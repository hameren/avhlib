module avh_kindpars
!  
  integer ,parameter :: kindi0 = selected_int_kind(2)
  integer ,parameter :: kindi1 = selected_int_kind(4)
  integer ,parameter :: kindi2 = selected_int_kind(9)
  integer ,parameter :: kindr1 = selected_real_kind(6)
  integer ,parameter :: kindr2 = selected_real_kind(15)
  integer ,parameter :: kindc1 = kindr1
  integer ,parameter :: kindc2 = kindr2
!
end module
