module avh_debug
  implicit none
  private
  public :: pickUp,giveBackI2,giveBackR2,giveBackC2,giveBackLL

  integer,parameter :: Nguess=100

  integer,allocatable,save :: lebal(:)
  logical,allocatable,save :: lFree(:)

  logical,allocatable,save :: storeLL(:)
  integer,allocatable,save :: offsetLL(:)
  integer            ,save :: usedLL,itemsLL

  integer,allocatable,save :: storeI2(:)
  integer,allocatable,save :: offsetI2(:)
  integer            ,save :: usedI2,itemsI2

  !(realknd2!),allocatable,save :: storeR2(:)
  integer     ,allocatable,save :: offsetR2(:)
  integer                 ,save :: usedR2,itemsR2

  !(complex2!),allocatable,save :: storeC2(:)
  integer     ,allocatable,save :: offsetC2(:)
  integer                 ,save :: usedC2,itemsC2

  interface pickUp
    module procedure pickUpI2,pickUpR2,pickUpC2,pickUpLL ! date: 03-11-2013
  end interface

contains

  subroutine pickUpC2( label ,xx )
!***********************************************************************
!***********************************************************************
  integer,intent(in) :: label
  !(complex2!),intent(in) :: xx(:)
  !(complex2!),allocatable :: tmp(:)
  integer,allocatable :: itmp(:)
  integer :: sizeS,sizeX
!
  if (.not.allocated(storeC2)) allocate(storeC2(1:Nguess))
  sizeX = size(xx,1)
  sizeS = size(storeC2,1)
  if (usedC2+sizeX.gt.sizeS) then
    allocate(tmp(1:sizeS))
    tmp = storeC2
    deallocate(storeC2)
    allocate(storeC2(1:sizeS+max(Nguess,sizeX)))
    storeC2(1:sizeS) = tmp(1:sizeS)
    deallocate(tmp)
  endif
!
  if (.not.allocated(offsetC2)) then
    allocate(offsetC2(1:Nguess))
    offsetC2(1) = 0
  endif
  sizeS = size(offsetC2,1)
  if (itemsC2+2.gt.sizeS) then
    allocate(itmp(1:sizeS))
    itmp = offsetC2
    deallocate(offsetC2)
    allocate(offsetC2(1:sizeS+Nguess))
    offsetC2(1:sizeS) = itmp(1:sizeS)
    deallocate(itmp)
  endif
!
  call newLabel( label )
!  
  if (lFree(label)) then
    lFree(label) = .false.
    itemsC2 = itemsC2+1
    lebal(label) = itemsC2
    storeC2(offsetC2(itemsC2)+1:offsetC2(itemsC2)+sizeX) = xx
    usedC2 = usedC2 + sizeX
    offsetC2(itemsC2+1) = offsetC2(itemsC2) + sizeX
  else
    storeC2( offsetC2(lebal(label))+1 : offsetC2(lebal(label)+1) ) = xx
  endif
  end subroutine

  function giveBackC2( label ) result(rslt)
!**********************************************************************
!**********************************************************************
  integer,intent(in) :: label
  !(complex2!) :: rslt(offsetC2(lebal(label)+1)-offsetC2(lebal(label)))
  rslt = storeC2( offsetC2(lebal(label))+1 : offsetC2(lebal(label)+1) )
  end function


  subroutine pickUpR2( label ,xx )
!***********************************************************************
!***********************************************************************
  integer,intent(in) :: label
  !(realknd2!),intent(in) :: xx(:)
  !(realknd2!),allocatable :: tmp(:)
  integer,allocatable :: itmp(:)
  integer :: sizeS,sizeX
!
  if (.not.allocated(storeR2)) allocate(storeR2(1:Nguess))
  sizeX = size(xx,1)
  sizeS = size(storeR2,1)
  if (usedR2+sizeX.gt.sizeS) then
    allocate(tmp(1:sizeS))
    tmp = storeR2
    deallocate(storeR2)
    allocate(storeR2(1:sizeS+max(Nguess,sizeX)))
    storeR2(1:sizeS) = tmp(1:sizeS)
    deallocate(tmp)
  endif
!
  if (.not.allocated(offsetR2)) then
    allocate(offsetR2(1:Nguess))
    offsetR2(1) = 0
  endif
  sizeS = size(offsetR2,1)
  if (itemsR2+2.gt.sizeS) then
    allocate(itmp(1:sizeS))
    itmp = offsetR2
    deallocate(offsetR2)
    allocate(offsetR2(1:sizeS+Nguess))
    offsetR2(1:sizeS) = itmp(1:sizeS)
    deallocate(itmp)
  endif
!
  call newLabel( label )
!  
  if (lFree(label)) then
    lFree(label) = .false.
    itemsR2 = itemsR2+1
    lebal(label) = itemsR2
    storeR2(offsetR2(itemsR2)+1:offsetR2(itemsR2)+sizeX) = xx
    usedR2 = usedR2 + sizeX
    offsetR2(itemsR2+1) = offsetR2(itemsR2) + sizeX
  else
    storeR2( offsetR2(lebal(label))+1 : offsetR2(lebal(label)+1) ) = xx
  endif
  end subroutine

  function giveBackR2( label ) result(rslt)
!**********************************************************************
!**********************************************************************
  integer,intent(in) :: label
  !(realknd2!) :: rslt(offsetR2(lebal(label)+1)-offsetR2(lebal(label)))
  rslt = storeR2( offsetR2(lebal(label))+1 : offsetR2(lebal(label)+1) )
  end function


  subroutine pickUpI2( label ,xx )
!***********************************************************************
!***********************************************************************
  integer,intent(in) :: label
  integer,intent(in) :: xx(:)
  integer,allocatable :: tmp(:)
  integer,allocatable :: itmp(:)
  integer :: sizeS,sizeX
!
  if (.not.allocated(storeI2)) allocate(storeI2(1:Nguess))
  sizeX = size(xx,1)
  sizeS = size(storeI2,1)
  if (usedI2+sizeX.gt.sizeS) then
    allocate(tmp(1:sizeS))
    tmp = storeI2
    deallocate(storeI2)
    allocate(storeI2(1:sizeS+max(Nguess,sizeX)))
    storeI2(1:sizeS) = tmp(1:sizeS)
    deallocate(tmp)
  endif
!
  if (.not.allocated(offsetI2)) then
    allocate(offsetI2(1:Nguess))
    offsetI2(1) = 0
  endif
  sizeS = size(offsetI2,1)
  if (itemsI2+2.gt.sizeS) then
    allocate(itmp(1:sizeS))
    itmp = offsetI2
    deallocate(offsetI2)
    allocate(offsetI2(1:sizeS+Nguess))
    offsetI2(1:sizeS) = itmp(1:sizeS)
    deallocate(itmp)
  endif
!
  call newLabel( label )
!  
  if (lFree(label)) then
    lFree(label) = .false.
    itemsI2 = itemsI2+1
    lebal(label) = itemsI2
    storeI2(offsetI2(itemsI2)+1:offsetI2(itemsI2)+sizeX) = xx
    usedI2 = usedI2 + sizeX
    offsetI2(itemsI2+1) = offsetI2(itemsI2) + sizeX
  else
    storeI2( offsetI2(lebal(label))+1 : offsetI2(lebal(label)+1) ) = xx
  endif
  end subroutine

  function giveBackI2( label ) result(rslt)
!**********************************************************************
!**********************************************************************
  integer,intent(in) :: label
  integer :: rslt(offsetI2(lebal(label)+1)-offsetI2(lebal(label)))
  rslt = storeI2( offsetI2(lebal(label))+1 : offsetI2(lebal(label)+1) )
  end function


  subroutine pickUpLL( label ,xx )
!***********************************************************************
!***********************************************************************
  integer,intent(in) :: label
  logical,intent(in) :: xx
  logical,allocatable :: tmp(:)
  integer,allocatable :: itmp(:)
  integer :: sizeS,sizeX
!
  if (.not.allocated(storeLL)) allocate(storeLL(1:Nguess))
  sizeX = 1
  sizeS = size(storeLL,1)
  if (usedLL+sizeX.gt.sizeS) then
    allocate(tmp(1:sizeS))
    tmp = storeLL
    deallocate(storeLL)
    allocate(storeLL(1:sizeS+max(Nguess,sizeX)))
    storeLL(1:sizeS) = tmp(1:sizeS)
    deallocate(tmp)
  endif
!
  if (.not.allocated(offsetLL)) then
    allocate(offsetLL(1:Nguess))
    offsetLL(1) = 0
  endif
  sizeS = size(offsetLL,1)
  if (itemsLL+2.gt.sizeS) then
    allocate(itmp(1:sizeS))
    itmp = offsetLL
    deallocate(offsetLL)
    allocate(offsetLL(1:sizeS+Nguess))
    offsetLL(1:sizeS) = itmp(1:sizeS)
    deallocate(itmp)
  endif
!
  call newLabel( label )
!  
  if (lFree(label)) then
    lFree(label) = .false.
    itemsLL = itemsLL+1
    lebal(label) = itemsLL
    storeLL(offsetLL(itemsLL)+1:offsetLL(itemsLL)+sizeX) = xx
    usedLL = usedLL + sizeX
    offsetLL(itemsLL+1) = offsetLL(itemsLL) + sizeX
  else
    storeLL( offsetLL(lebal(label))+1 : offsetLL(lebal(label)+1) ) = xx
  endif
  end subroutine

  function giveBackLL( label ) result(rslt)
!**********************************************************************
!**********************************************************************
  integer,intent(in) :: label
  logical :: rslt
  rslt = storeLL( offsetLL(lebal(label))+1 )
  end function


  subroutine newLabel( label )
!***********************************************************************
!***********************************************************************
  integer,intent(in) :: label
  integer,allocatable :: tmp(:)
  logical,allocatable :: lmp(:)
  integer :: lower,upper
!
  if (.not.allocated(lebal)) then
    allocate(lebal(-Nguess:Nguess))
    allocate(lFree(-Nguess:Nguess))
    lFree = .true.
  endif
  lower = lbound(lebal,1)
  upper = ubound(lebal,1)
!
  if (label.lt.lower.or.upper.lt.label) then
!
    allocate(tmp(lower:upper))
    tmp = lebal
    deallocate(lebal)
    allocate( lebal(min(label,lower):max(upper,label)) )
    lebal(lower:upper) = tmp(lower:upper)
    deallocate(tmp)
!
    allocate(lmp(lower:upper))
    lmp = lFree
    deallocate(lFree)
    allocate( lFree(min(label,lower):max(upper,label)) )
    lFree(lower:upper) = lmp(lower:upper)
    deallocate(lmp)
    lFree(label:lower) = .true.
    lFree(upper:label) = .true.
!
  endif
!
  end subroutine

   
end module


!program test
!  use avh_kindpars
!  use avh_ioUnits
!  use avh_ioTools
!  use avh_debug
!  implicit none
!  integer(kindi2) :: array(3),brray(4)
!  real(kindr2) :: xrray(3),yrray(4)
!  array = (/7,8,9/)
!  brray = (/11,12,13,14/)
!  xrray = (/4d0,5d0,6d0/)
!  yrray = (/7d0,8d0,9d0,-10d0/)
!  call pickUp( 201 ,array )
!  call pickUp(-117 ,yrray )
!  call pickUp(   9 ,brray )
!  call pickUp(  53 ,xrray )
!  write(*,*) prnt(giveBackI2(   9),3)
!  write(*,*) prnt(giveBackI2( 201),3)
!  write(*,*) prnt(giveBackR2(-117),3)
!  write(*,*) prnt(giveBackR2(  53),3)
!  write(*,*) giveBackR2(53)
!  call pickUp( 201 ,(/22,23,24/) )
!  write(*,*) giveBackI2(201)
!end program
