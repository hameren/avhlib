!******************************************************************************
! Function to print integer, real, and complex types onto character types.
!
! prnt(i)
!   prints integer i.
!   Space reserved to fit largest integer including a sign.
! prnt(i,j)
!   prints integer i.
!   Space reserved given by j+1.
!
! prnt(x)
!   prints real x in e-format.
!   No leading 0, instead a minus or a space.
!   The number of decimals is four more than significant.
! prnt(x,i)
!   the same as prnt(x), but with i decimals.
! prnt(x,i,j)
!   prints real x in f-format.
!   i+1 is the space reserved before the decimal point,
!   j is the number of decimals after the decimal point.
! prnt(x,i,j,y)
!   prints real x in f-format with an exponent given by
!   the exponent of real y represented as y1.y1y2...E<exponent>.
!   '|'//prnt(0.12d3,5,1,5.6d-1)//'|' ==> | 1200.0E-001|
!
! All calls can have an optional argument of type character.
!   If it is equal to 'lft' or 'left', then leading spaces are moved
!   to the end, to become trailing spaces (like the intrinsic ADJUSTL).
!     '|'//prnt( 12,4      )//'|' ==> |   12|
!     '|'//prnt(-12,4      )//'|' ==> |  -12|
!     '|'//prnt( 12,4,'lft')//'|' ==> |12   |
!     '|'//prnt(-12,4,'lft')//'|' ==> |-12  |
!   If it is '0', then leading spaces are replaced by '0', and put after
!   the possible minus-sign.
!     '|'//prnt( 12,4,'0')//'|' ==> |00012|
!     '|'//prnt(-12,4,'0')//'|' ==> |-0012|
!   If the first character is anything but '+', then leading spaces are
!   replaced by that character
!     '|'//prnt( 12,4,'_')//'|' ==> |___12|
!     '|'//prnt(-12,4,'_')//'|' ==> |__-12|
!   If the first character is '+', then the last space is replaced by a '+'
!   in case the number is positive. The next characters determine rules
!   as above:
!     '|'//prnt( 12,4,'+'   )//'|' ==> |  +12|
!     '|'//prnt(-12,4,'+'   )//'|' ==> |  -12|
!     '|'//prnt( 12,4,'+lft')//'|' ==> |+12  |
!     '|'//prnt(-12,4,'+lft')//'|' ==> |-12  |
!     '|'//prnt( 12,4,'+0'  )//'|' ==> |+0012|
!     '|'//prnt(-12,4,'+0'  )//'|' ==> |-0012|
!     '|'//prnt( 12,4,'+_'  )//'|' ==> |__+12|
!     '|'//prnt(-12,4,'+_'  )//'|' ==> |__-12|
!
! All calls can be performed with a complex as first input instead of a real.
!   z = cmplx( -12.345d0 ,6.7d0 ,kind(1d0) )
!   '|'//prnt(z,4,3    )//'|' ==> |(  -12.345,    6.700)|
!   '|'//prnt(z,4,3,'0')//'|' ==> |(-0012.345,00006.700)|
!   '|'//prnt(z,4,3,'+')//'|' ==> |(  -12.345,   +6.700)|
!    
! All calls can be performed with an array as the first input.
!   '|'//prnt((/-12.345d0,6.7d0/),4,3    )//'|' ==> |  -12.345     6.700|
!   '|'//prnt((/-12.345d0,6.7d0/),4,3,'0')//'|' ==> |-0012.345 00006.700|
!   '|'//prnt((/-12.345d0,6.7d0/),4,3,'+')//'|' ==> |  -12.345    +6.700|
!
! Un-comment the lines starting with ! followed by [QUAD=no if you have
! quadruple precision avialable. Add similar lines if you need other kinds
! not included.
! The initial values of nDig etc. inside the modules are "guesses",
! that are corrected if you <call init_prnt>.
!
!******************************************************************************


 module avh_prnt_aux
   implicit none
   private
   public :: put_symb
 contains
   subroutine put_symb(rslt,symb)
     character(*) ,intent(inout) :: rslt
     character(*) ,intent(in) :: symb
     integer :: jj
     if     (symb.eq.'+left'.or.symb.eq.'+lft') then
       jj = verify(rslt,' ')
       if (rslt(jj:jj).ne.'-') rslt(jj-1:jj-1) = '+'
       rslt = adjustl(rslt)
     elseif (symb.eq.'left'.or.symb.eq.'lft') then
       rslt = adjustl(rslt)
     elseif (symb.eq.'+0') then
       jj = verify(rslt,' ')
       if (rslt(jj:jj).eq.'-') then
         rslt(1:1)  = '-'
         rslt(2:jj) = repeat('0',jj-1)
       else
         rslt(1:1)  = '+'
         rslt(2:jj-1) = repeat('0',jj-2)
       endif
     elseif (symb.eq.'0') then
       jj = verify(rslt,' ')
       if (rslt(jj:jj).eq.'-') then
         rslt(1:1)  = '-'
         rslt(2:jj) = repeat('0',jj-1)
       else
         rslt(1:jj-1) = repeat('0',jj-1)
       endif
     elseif (symb.eq.'+') then
       jj = verify(rslt,' ')
       if (rslt(jj:jj).ne.'-') rslt(jj-1:jj-1) = '+'
     elseif (symb(1:1).eq.'+') then
       jj = verify(rslt,' ')
       if (rslt(jj:jj).ne.'-') then
         rslt(jj-1:jj-1) = '+'
         rslt(1:jj-2) = repeat(symb(2:2),jj-2)
       else
         rslt(1:jj-1) = repeat(symb(2:2),jj-1)
       endif
     else
       jj = verify(rslt,' ')
       rslt(1:jj-1) = repeat(symb(1:1),jj-1)
     endif
   end subroutine
 end module


 module avh_prnt_i0
   use avh_prnt_aux ;private ;public::init,prnt
   integer ,parameter :: kindIX=selected_int_kind(2)
   integer ,save :: nDig=10
   include 'intr_i.h90'
 end module


 module avh_prnt_i1
   use avh_prnt_aux ;private ;public::init,prnt
   integer ,parameter :: kindIX=selected_int_kind(4)
   integer ,save :: nDig=10
   include 'intr_i.h90'
 end module


 module avh_prnt_i2
   use avh_prnt_aux ;private ;public::init,prnt
   integer ,parameter :: kindIX=kind(1)
!   integer ,parameter :: kindIX=selected_int_kind(9)
   integer ,save :: nDig=10
   include 'intr_i.h90'
 end module


 module avh_prnt_r1
   use avh_prnt_aux ;private ;public::init,prnt
   integer ,parameter :: kindRX=kind(1.0)
!   integer ,parameter :: kindRX=selected_real_kind(6)
   integer ,save :: nDig=10,nExp=2,nLen=16
   include 'intr_r.h90'
 end module


 module avh_prnt_r2
   use avh_prnt_aux ;private ;public::init,prnt
   integer ,parameter :: kindRX=kind(1d0)
!   integer ,parameter :: kindRX=selected_real_kind(15)
   integer ,save :: nDig=19,nExp=3,nLen=26
   include 'intr_r.h90'
 end module


!{QUAD=yes
 module avh_prnt_r4                                    
   use avh_prnt_aux ;private ;public::init,prnt        
   integer ,parameter :: kindRX=kind(1q0) 
!   integer ,parameter :: kindRX=selected_real_kind(34) 
   integer ,save :: nDig=38,nExp=4,nLen=46             
   include 'intr_r.h90'                            
 end module                                            
!}QUAD


module avh_prnt
  use avh_prnt_i0 ,only: prnt
  use avh_prnt_i1 ,only: prnt
  use avh_prnt_i2 ,only: prnt
  use avh_prnt_r1 ,only: prnt
  use avh_prnt_r2 ,only: prnt
  use avh_prnt_r4 ,only: prnt !{QUAD=yes!}
  implicit none
  private
  public :: init_prnt,prnt
  logical ,save :: initd=.false.
contains
  subroutine init_prnt
    use avh_prnt_i0 ,only: init_i0=>init
    use avh_prnt_i1 ,only: init_i1=>init
    use avh_prnt_i2 ,only: init_i2=>init
    use avh_prnt_r1 ,only: init_r1=>init
    use avh_prnt_r2 ,only: init_r2=>init
    use avh_prnt_r4 ,only: init_r4=>init !{QUAD=yes!} 
    if (initd) return ;initd=.true.
    call init_i0
    call init_i1
    call init_i2
    call init_r1
    call init_r2
    call init_r4 !{QUAD=yes!}
  end subroutine
end module


