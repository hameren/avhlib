#!/usr/bin/env python
#begin header
import os,sys
pythonDir = ''
sys.path.append(pythonDir)
from avh import *
thisDir,packDir,avhDir = cr.dirsdown(2)
packageName = os.path.basename(thisDir)
buildDir = os.path.join(avhDir,'build')
#end header

date = '18-11-2014'

QPyes = False
for ii in range(0,len(sys.argv)):
    if sys.argv[ii]=='-QP': QPyes = True

lines = ['! From here the package "'+packageName+'", last edit: '+date+'\n']
cr.addfile(os.path.join(thisDir,'avh_prnt.f90'),lines,delPattern="^.*")

if QPyes:
    fpp.case('QUAD','yes',lines)
else:
    fpp.case('QUAD','no',lines)

fpp.incl(thisDir,lines)

cr.wfile(os.path.join(buildDir,packageName+'.f90'),lines)
