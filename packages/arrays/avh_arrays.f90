module avh_arrays
  use avh_iounits
  implicit none
  private
  public :: resize,enlarge,find_or_add

! Resize x to the new bounds. Anything that doesn't fit anymore is lost.
  interface resize
    module procedure resize1_i2,resize2_i2
    module procedure resize1_r2,resize2_r2
    module procedure resize1_c2,resize2_c2
  end interface

! Resize x to the maximum of the bounds it has and then new bounds.
  interface enlarge
    module procedure enlarge1_i2,enlarge2_i2
    module procedure enlarge1_r2,enlarge2_r2
    module procedure enlarge1_c2,enlarge2_c2
  end interface

! Find array in list and return position, add to list if not there.
  interface find_or_add
    module procedure find_or_add_i2,find_or_add_c2
  end interface

contains

  subroutine resize1_i2( xx ,l1,u1 )
  !(integer2!) &
  include 'resize1.h90'
  end subroutine 

  subroutine resize1_r2( xx ,l1,u1 )
  !(realknd2!) &
  include 'resize1.h90'
  end subroutine 

  subroutine resize1_c2( xx ,l1,u1 )
  !(complex2!) &
  include 'resize1.h90'
  end subroutine 

  subroutine resize2_i2( xx ,l1,u1 ,l2,u2 )
  !(integer2!) &
  include 'resize2.h90'
  end subroutine 

  subroutine resize2_r2( xx ,l1,u1 ,l2,u2 )
  !(realknd2!) &
  include 'resize2.h90'
  end subroutine 

  subroutine resize2_c2( xx ,l1,u1 ,l2,u2 )
  !(complex2!) &
  include 'resize2.h90'
  end subroutine 

  subroutine enlarge1_i2( xx ,l1,u1 )
  !(integer2!) &
  include 'enlarge1.h90'
  end subroutine 

  subroutine enlarge1_r2( xx ,l1,u1 )
  !(realknd2!) &
  include 'enlarge1.h90'
  end subroutine 

  subroutine enlarge1_c2( xx ,l1,u1 )
  !(complex2!) &
  include 'enlarge1.h90'
  end subroutine 

  subroutine enlarge2_i2( xx ,l1,u1 ,l2,u2 )
  !(integer2!) &
  include 'enlarge2.h90'
  end subroutine 

  subroutine enlarge2_r2( xx ,l1,u1 ,l2,u2 )
  !(realknd2!) &
  include 'enlarge2.h90'
  end subroutine 

  subroutine enlarge2_c2( xx ,l1,u1 ,l2,u2 )
  !(complex2!) &
  include 'enlarge2.h90'
  end subroutine 

  subroutine find_or_add_i2( ii ,xx ,yy )
  include 'find_or_add.h90'
    !execute xpnd: type | integer
  end subroutine

  subroutine find_or_add_c2( ii ,xx ,yy )
  include 'find_or_add.h90'
    !execute xpnd: type | !(complex2!)
  end subroutine

end module


