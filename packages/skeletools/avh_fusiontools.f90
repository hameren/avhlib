module avh_fusionTools
  !(usekindmod!)
  use avh_prnt
  use avh_ioUnits
  use avh_othercnst
  use !(cmplxCurrents_mod!)
  use avh_fermionLines
 
  implicit none
  private
  public :: interactionList_type,particleList_type
  public :: fusionRules_type,nonLocalRules_type
  public :: compatible_interface,updateRule_interface
  public :: vertexArgs_type

  type :: interaction_type
    integer,allocatable :: LegVal(:)
    integer :: CnstRef,Label
    procedure(vertex_interface),pointer,nopass :: Vertex
  end type
 
  type :: interactionList_type
    private
    integer :: Nleg=0,Nvertex=0
    type(interaction_type),allocatable :: Interaction(:)
  contains
    procedure :: add=>add_interaction
    procedure :: dealloc=>close_v
  end type

  type :: particle_type
    character(24) :: symbol=''
    integer :: Anti,fermionTyp
    procedure(propagator_interface),pointer,nopass :: Propagator
    procedure(   xternal_interface),pointer,nopass :: Xternal
  end type

  type :: particleList_type
    integer :: MinLabel=999999,MaxLabel=-999999
    integer :: symbLen=0
    type(particle_type),allocatable :: Particle(:)
  contains
    procedure :: add=>add_particle
    procedure :: dealloc=>close_p
    procedure :: set_shape
  end type
 
  type :: fusionRules_type
    integer :: Nleg=0,Nvertex=0,Nul=0,symbLen=0
    integer,allocatable :: Reference(:,:)
    integer,allocatable :: NfusionResults(:)
    integer,allocatable :: FusionResult(:,:)
    type(particle_type),allocatable :: Particle(:)
    type(interaction_type),allocatable :: Interaction(:)
    procedure(compatible_interface),pointer,nopass :: compatible
    procedure(updateRule_interface)   ,pointer :: Urule
    procedure(updateVertex_interface) ,pointer :: Uvertex
    procedure(updateCombine_interface),pointer :: Ucombine
    procedure(updateXternal_interface),pointer :: Uxternal
  contains
    procedure :: fill=>fill_fusionrules
    procedure :: dealloc=>close_f
    procedure :: prnt=>prnt_particle
  end type

  type :: nonLocalRules_type
    integer :: Nul=0
    procedure(nonLocalRule_interface),pointer,nopass :: Rule
    procedure(nonLocalAnti_interface),pointer,nopass :: Anti
  contains
    procedure :: fill=>fill_nonLocalRules
  end type

  type :: vertexArgs_type
    type(!(cmplxCurrent_type!)) :: u1,u2,u3,u4 ! off-shell currents
    type(!(cmplxMomentum_type!)) :: p0,p1,p2,p3,p4 ! momenta
    integer :: cPrim,cSecn ! primary and secondary coupling constants
    integer :: pPrim,pSecn ! primary and secondary permutation
    integer :: e(0:4),eRef ! primary current properties
    integer :: f(0:4),fRef ! secondary current properties
    integer :: Fsign
  end type

  abstract interface
    function vertex_interface( vtx ) result(rslt)
    use !(cmplxCurrents_mod!)
    import
    type(vertexArgs_type),intent(in) :: vtx
    type(!(cmplxCurrent_type!)) :: rslt
    end function
  end interface

  abstract interface
    subroutine propagator_interface( i ,rslt ,p )
    use !(cmplxCurrents_mod!)
    type(!(cmplxCurrent_type!)) :: rslt
    integer           ,intent(in) :: i
    type(!(cmplxMomentum_type!)),intent(in) :: p
    end subroutine
  end interface

  abstract interface
    function xternal_interface( x ) result(rslt)
    use !(cmplxCurrents_mod!)
    type(!(cmplxCurrent_type!)) :: rslt
    type(!(cmplxExternal_type!)),intent(in) :: x
    end function
  end interface

  abstract interface
    function compatible_interface(i,j) result(rslt)
    integer,intent(in) :: i,j
    logical :: rslt
    end function
  end interface
 
  abstract interface
    subroutine updateRule_interface( info ,p ,f ,ifus ,f0 ,discard )
    import
    class(fusionRules_type) :: info
    integer,intent(in   ) :: p(0:) ,f(1:)
    integer,intent(inout) :: ifus
    integer,intent(out  ) :: f0
    logical,intent(out  ) :: discard
    optional              :: f0,discard
    end subroutine
  end interface

  abstract interface
    subroutine updateVertex_interface( info ,vertexLabel,cnstRef ,f ,p )
    import
    class(fusionRules_type) :: info
    integer,intent(out) :: vertexLabel
    integer,intent(out) :: cnstRef
    integer,intent(in ) :: f(0:)
    logical,intent(in ) :: p(0:)
    end subroutine
  end interface

  abstract interface
    subroutine updateCombine_interface( info ,labl0,cnstRef0 ,labl1,cnstRef1 ,labl2,cnstRef2 )
    import
    class(fusionRules_type) :: info
    integer,intent(out) :: labl0
    integer,intent(out) :: cnstRef0
    integer,intent(in ) :: labl1,labl2
    integer,intent(in ) :: cnstRef1,cnstRef2
    end subroutine
  end interface

  abstract interface
    subroutine updateXternal_interface( info ,p ,f )
    import
    class(fusionRules_type) :: info
    logical,intent(inout) :: p(0:)
    integer,intent(in   ) :: f(0:)
    end subroutine
  end interface


  abstract interface
    subroutine nonLocalRule_interface( p ,f ,ifus ,f0 ,discard )
    integer,intent(in   ) :: p(0:) ,f(1:)
    integer,intent(inout) :: ifus
    integer,intent(out  ) :: f0
    logical,intent(out  ) :: discard
    optional              :: f0,discard
    end subroutine
  end interface

  abstract interface
    function nonLocalAnti_interface( f ) result(rslt)
    integer,intent(in) :: f
    integer :: rslt
    end function
  end interface


  interface enlarge
    module procedure enlarge_p1
  end interface

  logical,save :: initz=.true.
  !(realknd2!),save :: small

contains


  subroutine init
  initz = .false.
  small = 1d-12
!!                   123456789012345678901234567890123456789012345678901234567890123456789012
!  write(*,'(a72)') '########################################################################'
!  write(*,'(a72)') '#                      You are using Skeletools                        #'
!  write(*,'(a72)') '#                                                                      #'
!  write(*,'(a72)') '# author: Andreas van Hameren <hamerenREMOVETHIS@ifj.edu.pl>           #'
!  write(*,'(a72)') '#   date: 28-11-2013                                                   #'
!  write(*,'(a72)') '########################################################################'
  end subroutine


  subroutine close_f( f )
  class(fusionRules_type) :: f
  f%Nleg=0 ;f%Nvertex=0 ;f%Nul=0 ;f%symbLen=0
  if (allocated(f%Reference     )) deallocate(f%Reference     )
  if (allocated(f%NfusionResults)) deallocate(f%NfusionResults)
  if (allocated(f%FusionResult  )) deallocate(f%FusionResult  )
  if (allocated(f%Particle      )) deallocate(f%Particle      )
  if (allocated(f%Interaction   )) deallocate(f%Interaction   )
  end subroutine
 
  subroutine close_v( list )
  class(interactionList_type) :: list
  list%Nleg=0 ;list%Nvertex=0
  if (allocated(list%Interaction)) deallocate(list%Interaction)
  end subroutine

  subroutine close_p( list )
  class(particleList_type) :: list
  list%MinLabel=999999 ;list%MaxLabel=-999999 ;list%symbLen=0
  if (allocated(list%Particle)) deallocate(list%Particle)
  end subroutine


  subroutine enlarge_p1( xx ,l1,u1 )
  type(particle_type) &
  include '../arrays/enlarge1.h90'
  end subroutine
 

  subroutine add_interaction( list ,legVal ,cnstRef ,vertex ,label )
!******************************************************************************
!******************************************************************************
  class(interactionList_type) :: list
  integer                  ,intent(in) :: legVal(:)
  integer         ,optional,intent(in) :: cnstRef
  procedure(vertex_interface),optional :: vertex
  integer         ,optional,intent(in) :: label
  type(interaction_type),allocatable :: t_Interaction(:)
  integer ,parameter :: Nguess=64
  integer :: ii,sizeLegVal
  if (initz) call init
  if (list%Nvertex.eq.0) then
    list%Nleg = size(legVal,1)
    allocate(list%Interaction(1:Nguess))
  endif
!
  if (list%Nvertex+1.gt.size(list%Interaction,1)) then
    allocate(t_Interaction(lbound(list%Interaction,1):ubound(list%Interaction,1)))
    t_Interaction = list%Interaction
    deallocate(list%Interaction)
    allocate(list%Interaction(1:list%Nvertex+Nguess))
    list%Interaction(1:list%Nvertex) = t_Interaction(1:list%Nvertex)
    deallocate( t_Interaction )
  endif
!  
  list%Nvertex = list%Nvertex+1
  sizeLegVal = size(legVal,1)
  if (sizeLegVal.gt.list%Nleg) list%Nleg = sizeLegVal
  allocate(list%Interaction(list%Nvertex)%LegVal(1:sizeLegVal))
  list%Interaction(list%Nvertex)%LegVal = legVal
  if (present(vertex )) list%Interaction(list%Nvertex)%Vertex=>vertex
  if (present(label  )) list%Interaction(list%Nvertex)%Label=label
  if (present(cnstRef)) list%Interaction(list%Nvertex)%CnstRef=cnstRef
  end subroutine


 subroutine add_particle( list ,label ,symbol &
                         ,antiParticle ,fermionTyp ,propagator ,xternal )
!***********************************************************************
!***********************************************************************
  class(particleList_type) :: list
  integer     ,intent(in) :: label
  character(*),intent(in) :: symbol
  integer     ,intent(in),optional :: antiParticle,fermionTyp
  procedure(propagator_interface),optional :: propagator
  procedure(   xternal_interface),optional :: xternal
  if     (label.lt.list%MinLabel) then
    list%MinLabel = label
    call enlarge( list%Particle ,label,label+1 )
  elseif (label.gt.list%MaxLabel) then
    list%MaxLabel = label
    call enlarge( list%Particle ,label-1,label )
  endif
  if (list%symbLen.lt.len(symbol)) list%symbLen=len(symbol)
  list%Particle(label)%Symbol= adjustl(symbol)
  list%Particle(label)%Propagator => propagator
  list%Particle(label)%Xternal => xternal
  if (present(antiParticle)) then
    list%Particle(label)%Anti = antiParticle
  else
    list%Particle(label)%Anti = label
  endif
  if (present(fermionTyp)) then
    list%Particle(label)%FermionTyp = fermionTyp
  else
    list%Particle(label)%FermionTyp = bosTyp
  endif
  end subroutine

  subroutine set_shape( list ,minLabel,maxLabel )
!***********************************************************************
! This routine in principle does not have to be called,
! but may sometimes be required.
!***********************************************************************
  class(particleList_type) :: list
  type(particle_type),allocatable :: tmp(:)
  integer,intent(in) :: minLabel,maxLabel
  integer :: low,upp
  low = min(minLabel,list%minLabel)
  upp = max(maxLabel,list%maxLabel)
  if (low.lt.list%minLabel.or.upp.gt.list%maxLabel) then
    tmp = list%Particle
    deallocate(list%Particle)
    allocate(list%Particle(low:upp))
    list%Particle(list%minLabel:list%maxLabel) = &
              tmp(list%minLabel:list%maxLabel)
    list%minLabel = low
    list%maxLabel = upp
  endif
  end subroutine


  subroutine fill_fusionrules( f ,p ,v ,noCheck &
                              ,compatible &
                              ,rule,vertex,combine,xternal )
!******************************************************************************
! Inverts the array  v%LegVal(1:Nleg,1:Nvtx) , such that if
!   v%LegVal(1:Nleg,i) = (/l{1},l{2},..,l{Nleg}/)
! then
!   i = f%Reference(l{1},f%Reference(l{2},...,f%Reference(l{Nleg},0)...))
!
! Furthermore, it delivers all fusion rules, that is if
!   i = f%Reference(l{1},f%Reference(l{2},...,f%Reference(l{Nleg-1},0)...))
! then
!   nf = f%NfusionResults(i)
! is the number of vertices with values for Nleg-1 of its legs equal to
!   (/l{1},l{2},...,l{Nleg-1}/)
! and
!   antity(l{Nleg}) = f%FusionResult(j,i)
! is the antity value of the Nleg-th leg for the j-th member of these nf
! vertices. This antity value is defined by the input array <antity>
!
! The vertices are considere to be symmetric. Vertices that are equal up to a
! permutation of the legs are considered equal, and taken into acount only
! once. Leg values lower than "nul+1" are ignored. Any vertex "j" with
!   v%LegVal(i,j) < nul+1
! for "n" values of "i" is considered to be an (Nleg-n)-point vertex.
! The antity of any value smaller than <nul+1> should be <nul>
!******************************************************************************
  class(fusionRules_type),intent(out) :: f
  type(particleList_type)    :: p
  type(interactionList_type) :: v
  character(*),optional,intent(in) :: noCheck
  procedure(compatible_interface)    :: compatible
  procedure(updateRule_interface)    :: rule
  procedure(updateVertex_interface)  :: vertex
  procedure(updateCombine_interface) :: combine
  procedure(updateXternal_interface) :: xternal
  optional :: compatible,rule,vertex,combine,xternal
  integer,allocatable :: list(:,:),vv(:,:)
  integer :: nv(v%Nleg),stepl,stepv,maximum,absmin
  integer :: ii,jj,kk,ll(1:v%Nleg),nn,Ileg,Jleg,irhs,Ivtx
  logical :: doCheck
!
  call close_f( f )
!
  if (v%Nleg.eq.0) then
    if (errru.gt.0) write(errru,*) 'ERROR in fusionTools fill: ' &
      ,'vertices dont seem to be filled at all'
    stop
  endif
  if (v%Nvertex.eq.0) then
    if (errru.gt.0) write(errru,*) 'ERROR in fusionTools fill: ' &
      ,'no vertices'
    stop
  endif
!
  if (.not.allocated(p%Particle)) then
    if (errru.ge.0) write(errru,*) 'ERROR if avh_fusionTools fill_fusion: '&
      ,'Particle on input not allocated'
    stop
  endif
  f%symbLen = p%symbLen
  f%Nul = p%MinLabel-1
  allocate( f%Particle(f%Nul:p%MaxLabel) )
  f%Particle(p%MinLabel:p%MaxLabel) = p%Particle(p%MinLabel:p%MaxLabel)
  f%Particle(f%Nul)%Anti = f%Nul
!
  do ii=1,v%Nvertex
    jj = size(v%Interaction(ii)%LegVal,1)
    if (jj.eq.v%Nleg) cycle
    ll(1:jj) = v%Interaction(ii)%LegVal(1:jj)
    deallocate(v%Interaction(ii)%LegVal)
    allocate(v%Interaction(ii)%LegVal(1:v%Nleg))
    v%Interaction(ii)%LegVal(1:jj) = ll(1:jj)
    v%Interaction(ii)%LegVal(jj+1:v%Nleg) = f%Nul
  enddo
!
  f%Nleg = v%Nleg
  maximum = v%Interaction(1)%LegVal(1)
  absmin  = maximum
!
  stepl=v%Nvertex  ;stepv=v%Nvertex
  allocate( list(f%Nleg,stepl) ,vv(f%Nleg,stepv) )
  if (allocated(v%Interaction)) allocate( f%Interaction(1:v%Nvertex) )
!
  nn=0 ;nv=0
!
! List Ileg-point vertices seperately.
  doCheck = .true.
  if (present(noCheck)) then
    if (noCheck(1:3).eq.'yes') then
      doCheck = .false.
    else
      if (errru.gt.0) write(errru,*) 'ERROR in fusionTools fill: ' &
        ,'noCheck=',trim(noCheck),' not defined'
    endif
  endif
!
  if (doCheck) then
! Perform checking
    do Ivtx=1,v%Nvertex
      ll(1:f%Nleg) = v%Interaction(Ivtx)%LegVal(1:f%Nleg)
      call sort( ll(1:f%Nleg),f%Nleg )
      if (ll(1).gt.maximum) maximum = ll(1)
      if (ll(f%Nleg).lt.absmin) absmin = ll(f%Nleg)
      if (ll(1).le.f%Nul) then
        if (warnu.gt.0) write(warnu,*) 'WARNING from fusionTools fill: ' &
          ,'ignoring empty vertex'
        cycle
      endif
      jj = 0
      do ;jj=jj+1
        if (jj.gt.nn) then
          nn = nn+1; if (nn.gt.ubound(list,2)) call resize_l
          list(1:f%Nleg,nn) = ll(1:f%Nleg)
          if (allocated(f%Interaction)) f%Interaction(nn) = v%Interaction(Ivtx)
          if (allocated(f%Interaction(nn)%LegVal)) deallocate(f%Interaction(nn)%LegVal)
          do Ileg=f%Nleg,1,-1
            if (list(Ileg,nn).gt.f%Nul) then
              nv(Ileg) = nv(Ileg)+1 ;if (nv(Ileg).gt.ubound(vv,2)) call resize_v
              vv(Ileg,nv(Ileg)) = nn
              exit
            endif
          enddo
          exit
        endif
        if (all(list(1:f%Nleg,jj).eq.ll(1:f%Nleg))) then
          if (warnu.gt.0) write(warnu,*) 'WARNING from fusionTools fill: ' &
            ,'ignoring equal vertices'
          exit
        endif
      enddo
    enddo
  else
! No checking, "vertices" is expected to be properely ordered etc.
    do Ivtx=1,v%Nvertex
      ll(1:f%Nleg) = v%Interaction(Ivtx)%LegVal(1:f%Nleg)
      if (ll(1).gt.maximum) maximum = ll(1)
      if (ll(f%Nleg).lt.absmin) absmin = ll(f%Nleg)
      nn = nn+1; if (nn.gt.ubound(list,2)) call resize_l
      list(1:f%Nleg,nn) = ll(1:f%Nleg)
      if (allocated(f%Interaction)) f%Interaction(nn) = v%Interaction(Ivtx)
      if (allocated(f%Interaction(nn)%LegVal)) deallocate(f%Interaction(nn)%LegVal)
      do Ileg=f%Nleg,1,-1
        if (list(Ileg,nn).gt.f%Nul) then
          nv(Ileg) = nv(Ileg)+1 ;if (nv(Ileg).gt.ubound(vv,2)) call resize_v
          vv(Ileg,nv(Ileg)) = nn
          exit
        endif
      enddo
    enddo
  endif
!
  f%Nvertex = nn
!
! Increase the list with not yet listed (Ileg-1)-point vertices
! obtained by removing one leg of a Ileg-point vertex.
  do Ileg=f%Nleg,2,-1
    Jleg = Ileg-1
    do ii=1,nv(Ileg)
    do jj=1,Ileg
      ll(1 :jj-1) = list(   1:jj-1,vv(Ileg,ii))
      ll(jj:Jleg) = list(jj+1:Ileg,vv(Ileg,ii))
      kk = 0
      do ;kk=kk+1
        if (kk.gt.nv(Jleg)) then
          nn = nn+1 ;if (nn.gt.ubound(list,2)) call resize_l
          list(:,nn) = f%Nul
          list(1:Jleg,nn) = ll(1:Jleg)
          nv(Jleg) = nv(Jleg)+1 ;if (nv(Jleg).gt.ubound(vv,2)) call resize_v
          vv(Jleg,nv(Jleg)) = nn
          exit
        endif
        if (all(list(1:Jleg,vv(Jleg,kk)).eq.ll(1:Jleg))) exit
      enddo
    enddo
    enddo
  enddo
!
! Allocate f%Reference
  if (allocated(f%Reference)) deallocate(f%Reference)
  allocate( f%Reference(absmin:maximum,-1:nn) )
  f%Reference(absmin:maximum,-1:nn) = -1
  do ii=0,nn
    f%Reference(absmin:f%Nul,ii) = ii
  enddo
!
! Fill f%Reference
  do Ileg=1,f%Nleg
    do ii=1,nv(Ileg)
      ll(1:Ileg) = list(1:Ileg,vv(Ileg,ii))
      do jj=1,Ileg
        kk=ll(jj) ;ll(jj)=ll(1) ;ll(1)=kk
        irhs=0 ;do kk=2,Ileg ;irhs=f%Reference(ll(kk),irhs) ;enddo
        f%Reference(ll(1),irhs) = vv(Ileg,ii)
        kk=ll(jj) ;ll(jj)=ll(1) ;ll(1)=kk
      enddo
    enddo
  enddo
!
! Fill fusion rules
  deallocate( vv )
  allocate(f%NfusionResults( -1:nn))
  allocate(f%fusionResult(1:1,1:nn))
  f%NfusionResults = 0
!
  do Ivtx=1,f%Nvertex
    ll(1:f%Nleg) = list(1:f%Nleg,Ivtx)
    do Ileg=f%Nleg,1,-1 ;if (ll(Ileg).gt.f%Nul) exit ;enddo
    do jj=1,Ileg
      kk=ll(jj) ;ll(jj)=ll(1) ;ll(1)=kk
      irhs=0 ;do kk=2,Ileg ;irhs=f%Reference(ll(kk),irhs) ;enddo
      kk = 0
      do ;kk=kk+1
        if (kk.gt.f%NfusionResults(irhs)) then
          f%NfusionResults( irhs) = kk
          if (kk.gt.ubound(f%fusionResult,1)) call resize_f
          f%fusionResult(kk,irhs) = f%Particle(ll(1))%Anti
          exit
        endif
        if (f%fusionResult(kk,irhs).eq.f%Particle(ll(1))%Anti) exit
      enddo
      kk=ll(jj) ;ll(jj)=ll(1) ;ll(1)=kk
    enddo
  enddo
!
  if (present(rule)   ) f%Urule    => rule
  if (present(vertex) ) f%Uvertex  => vertex
  if (present(combine)) f%Ucombine => combine
  if (present(xternal)) f%Uxternal => xternal
  if (present(compatible)) f%compatible => compatible
!
  contains
!
    subroutine resize_l
    integer ,allocatable :: tmp(:,:)
    integer :: nsize
    nsize = ubound(list,2)
    allocate(tmp(f%Nleg,nsize)) ;tmp=list
    deallocate(list) ;allocate(list(f%Nleg,nsize+stepl))
    list(1:f%Nleg,1:nsize)=tmp(1:f%Nleg,1:nsize) ;deallocate(tmp)
    end subroutine
!
    subroutine resize_v
    integer ,allocatable :: tmp(:,:)
    integer :: nsize
    nsize = ubound(vv,2)
    allocate(tmp(f%Nleg,nsize)) ;tmp=vv
    deallocate(vv) ;allocate(vv(f%Nleg,nsize+stepv))
    vv(1:f%Nleg,1:nsize)=tmp(1:f%Nleg,1:nsize) ;deallocate(tmp)
    end subroutine
!
    subroutine resize_f
    integer ,allocatable :: tmp(:,:)
    integer :: nsize,nn
    nsize = ubound(f%fusionResult,1)
    nn    = ubound(f%fusionResult,2)
    allocate(tmp(nsize,nn)) ;tmp=f%fusionResult
    deallocate(f%fusionResult) ;allocate(f%fusionResult(1:nsize+1,1:nn))
    f%fusionResult(1:nsize,1:nn)=tmp(1:nsize,1:nn) ;deallocate(tmp)
    end subroutine
!
  end subroutine


  subroutine fill_nonLocalRules( f ,rule ,anti ,nul )
!******************************************************************************
!******************************************************************************
  class(nonLocalRules_type),intent(out) :: f
  procedure(nonLocalRule_interface) :: rule
  procedure(nonLocalAnti_interface) :: anti
  integer,intent(in) :: nul
  f%Rule => rule
  f%Anti => anti
  f%Nul = nul
  end subroutine


  function prnt_particle( f ,ii ) result(rslt)
!******************************************************************
!******************************************************************
  class(fusionRules_type) :: f
  integer,intent(in) :: ii
  character(f%symbLen) :: rslt
  rslt = f%Particle(ii)%symbol(1:f%symbLen)
  end function
  

  subroutine sort(yy,nn)
!******************************************************************
!******************************************************************
  integer ,intent(in) :: nn
  integer ,intent(inout) :: yy(nn)
  integer :: ll,ii,jj,ir,xxb,xx(nn)
  xx = yy
  if (nn.le.1) return
  ll = nn/2 + 1
  ir = nn
  do
    if (ll.gt.1) then 
      ll = ll - 1
      xxb = xx(ll)
    else
      xxb = xx(ir)
      xx(ir) = xx(1)
      ir = ir - 1
      if (ir.eq.1) then
        xx(1) = xxb
        exit
      endif
    endif
    ii = ll
    jj = ll + ll
    do while (jj.le.ir)
      if (jj.lt.ir) then
        if (xx(jj).lt.xx(jj+1)) jj = jj + 1
      endif
      if (xxb.lt.xx(jj)) then
        xx(ii) = xx(jj)
        ii = jj
        jj = jj + jj
      else
        jj = ir + 1
      endif
    enddo
    xx(ii) = xxb
  enddo
!
  do ii=1,nn
    yy(ii) = xx(nn-ii+1)
  enddo
!
  end subroutine


end module
 
 
