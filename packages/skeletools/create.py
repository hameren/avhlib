#!/usr/bin/env python
#begin header
import os,sys
pythonDir = ''
sys.path.append(pythonDir)
from avh import *
thisDir,packDir,avhDir = cr.dirsdown(2)
packageName = os.path.basename(thisDir)
buildDir = os.path.join(avhDir,'build')
#end header

date = '04-03-2021'

QPyes = False
for ii in range(0,len(sys.argv)):
    if sys.argv[ii]=='-QP': QPyes = True

lines = ['! From here the package "'+packageName+'", last edit: '+date+'\n']

cr.addfile(os.path.join(thisDir,'avh_fermionlines.f90'),lines,delPattern='^.*')

linesDP = []
cr.addfile(os.path.join(thisDir,'avh_fusiontools.f90'),linesDP,delPattern='^.*')
cr.addfile(os.path.join(thisDir,'avh_buildskel.f90'),linesDP,delPattern='^.*')
cr.addfile(os.path.join(thisDir,'avh_evalskel.f90'),linesDP,delPattern='^.*')
cr.addfile(os.path.join(thisDir,'avh_evalskel_t.f90'),linesDP,delPattern='^.*')
fpp.incl(thisDir,linesDP)
fpp.xpnd('cmplxCurrents_mod','avh_lorentz ,only: init_lorentz,lorentz_type,xternalArgs_type,oscPlus,contract,putToZero',linesDP)
fpp.xpnd('realknd2','real(kind(1d0))',linesDP)
fpp.xpnd('complex2','complex(kind(1d0))',linesDP)
lines = lines+linesDP

if QPyes:
    linesQP = []
    cr.addfile(os.path.join(thisDir,'avh_fusiontools.f90'),linesQP,delPattern='^.*')
    cr.addfile(os.path.join(thisDir,'avh_buildskel.f90'),linesQP,delPattern='^.*')
    cr.addfile(os.path.join(thisDir,'avh_evalskel.f90'),linesQP,delPattern='^.*')
    fpp.incl(thisDir,linesQP)
    fpp.xpnd('cmplxCurrents_mod','avh_qp_lorentz ,only: init_lorentz,lorentz_type,xternalArgs_type,oscPlus,contract,putToZero',linesDP)
    fpp.xpnd('realknd2','real(kind(1q0))',linesQP)
    fpp.xpnd('complex2','complex(kind(1q0))',linesQP)
    ed.replace_string('avh_mathcnst','avh_qp_mathcnst',linesQP)
    ed.replace_string('avh_lorentz','avh_qp_lorentz',linesQP)
    ed.replace_string('avh_fusionTools','avh_qp_fusionTools',linesQP)
    ed.replace_string('avh_buildskel','avh_qp_buildskel',linesQP)
    ed.replace_string('avh_evalskel','avh_qp_evalskel',linesQP)
    lines = lines+linesQP

cr.addfile(os.path.join(thisDir,'avh_ranxconf.f90'),lines,delPattern='^.*')

fpp.incl(thisDir,lines)
fpp.xpnd('cmplxCurrent_type','lorentz_type',lines)
fpp.xpnd('cmplxMomentum_type','lorentz_type',lines)
fpp.xpnd('cmplxExternal_type','xternalArgs_type',lines)
fpp.xpnd('initCmplxCurrents','call init_lorentz',lines)

cr.wfile(os.path.join(buildDir,packageName+'.f90'),lines)
