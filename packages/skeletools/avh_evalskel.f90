module avh_evalskel
  !(usekindmod!)
  use avh_iounits
  use avh_mathcnst
  use avh_othercnst
  use avh_doloops ,only: sort_3,sort_4,sort_5
  use !(cmplxCurrents_mod!)
  use avh_fusionTools
  use avh_buildskel
 
  implicit none
  private
  public :: evaluate

  integer,save :: NcurrentsMax=0
  type(!(cmplxCurrent_type!)),allocatable,save :: uu(:)
  logical,allocatable,save :: putProp(:)

  interface evaluate
    module procedure singleskel
    module procedure daughters
  end interface

  interface enlarge
    module procedure enlarge_l1,enlarge_u1,enlarge_u2
  end interface

  logical,save :: initz=.true.

contains

  subroutine init
  initz = .false.
  call init_mathcnst
  !(initCmplxCurrents!)
  end subroutine


  subroutine dealloc_work
  NcurrentsMax = 0
  if (allocated(uu)) deallocate( uu  )
  if (allocated(putProp)) deallocate( putProp  )
  end subroutine


  subroutine enlarge_c( Nnew )
  integer,intent(in) :: Nnew
  if (initz) call init
  NcurrentsMax = Nnew
  call enlarge( uu ,1,Nnew )
  call enlarge( putProp ,0,Nnew )
  end subroutine


  subroutine singleskel( incomplete ,rslt &
                        ,tBas ,fBas ,fAug ,xProperty &
                        ,momentum,pZero ,xCurrent )
!***********************************************************************
!***********************************************************************
  logical                           :: incomplete
  !(complex2!)                      :: rslt
  type(dsskeleton_type) ,intent(in) :: tBas
  type(fusionRules_type),intent(in) :: fBas,fAug
  integer               ,intent(in) :: xProperty(tBas%Nleaves+1)
  integer               ,intent(in) :: pZero
  type(!(cmplxMomentum_type!)),intent(in) :: momentum(0:pZero)
  type(!(cmplxCurrent_type!)) ,intent(in) :: xCurrent(tBas%Nleaves+1)
  type(!(cmplxCurrent_type!)) :: u0
  type(vertexArgs_type) :: vArg
  integer :: ivtx,Nxtrn,jvtx,pMod
  integer :: i0,i1,i2,i3,i4,p0,p1,p2,p3,p4
  integer :: j0,j1,j2,j3,j4
!
  call augment_skeleton( incomplete ,tBas ,fAug ,xProperty )
  if (incomplete) then
    rslt = 0
    return
  endif
!  call print_skeleton( tWork ,fAug ,tBas ,fBas ) !DEBUG
!
  Nxtrn = tBas%Nleaves+1
  pMod = pZero+1
!
  if (tWork%Ncurrents.gt.NcurrentsMax) call enlarge_c(tWork%Ncurrents)
!
  uu(1:tBas%Nleaves) = xCurrent(1:tBas%Nleaves)
!
  putProp(0:tWork%Nleaves) = .false.
  putProp(Nxtrn:tWork%Ncurrents) = .true.
  call putToZero( uu ,Nxtrn,tWork%Ncurrents )
!
  do ivtx=1,tWork%Nvertices
    jvtx = tWork%Vertex(ivtx)%Mother
    vArg%Fsign = tBas%Vertex(jvtx)%FermSign
    i0 = tWork%Vertex(ivtx)%Leg(0)
    i1 = tWork%Vertex(ivtx)%Leg(1)
    i2 = tWork%Vertex(ivtx)%Leg(2)
    vArg%f(0) = fAug%Particle(tWork%Current(i0)%Property)%Anti
    vArg%f(1) = tWork%Current(i1)%Property
    vArg%f(2) = tWork%Current(i2)%Property
    vArg%fRef = fAug%Reference(vArg%f(2),fAug%Reference(vArg%f(1),fAug%Reference(vArg%f(0),0)))
    j0 = tBas%Vertex(jvtx)%Leg(0)
    j1 = tBas%Vertex(jvtx)%Leg(1)
    j2 = tBas%Vertex(jvtx)%Leg(2)
    p0 = mod(tBas%Current(j0)%Mother,pMod)
    p1 = mod(tBas%Current(j1)%Mother,pMod)
    p2 = mod(tBas%Current(j2)%Mother,pMod)
    vArg%e(0) = fBas%Particle(tBas%Current(j0)%Property)%Anti
    vArg%e(1) = tBas%Current(j1)%Property
    vArg%e(2) = tBas%Current(j2)%Property
    vArg%eRef = fBas%Reference(vArg%e(2),fBas%Reference(vArg%e(1),fBas%Reference(vArg%e(0),0)))
    vArg%p0 = momentum(p0)
    vArg%p1 = momentum(p1)
    vArg%p2 = momentum(p2)
!
    if (putProp(i1)) then
      putProp(i1) = .false.                            
      call fBas%Particle(vArg%e(1))%propagator( vArg%e(1) ,uu(i1) ,momentum(p1) )
    endif                                                  
    if (putProp(i2)) then                                         
      putProp(i2) = .false.                                       
      call fBas%Particle(vArg%e(2))%propagator( vArg%e(2) ,uu(i2) ,momentum(p2) )
    endif
    vArg%u1 = uu(i1)
    vArg%u2 = uu(i2)
!
    i3 = tWork%Vertex(ivtx)%Leg(3)
    if (i3.eq.0) then
      vArg%cPrim = fBas%Interaction(vArg%eRef)%CnstRef
      vArg%cSecn = fAug%Interaction(vArg%fRef)%CnstRef
      vArg%pSecn = sort_3( vArg%f(0:2) )
      u0 = fBas%Interaction( vArg%eRef )%Vertex( vArg )
!
    else!(i3.ne.0)
      j3 = tBas%Vertex(jvtx)%Leg(3)
      p3 = mod(tBas%Current(j3)%Mother,pMod)
      vArg%f(3) = tWork%Current(i3)%Property
      vArg%e(3) = tBas%Current(j3)%Property
      vArg%eRef = fBas%Reference(vArg%e(3),vArg%eRef)
      vArg%fRef = fAug%Reference(vArg%f(3),vArg%fRef)
      vArg%p3 = momentum(p3)
!
      if (putProp(i3)) then
        putProp(i3) = .false.
        call fBas%Particle(vArg%e(3))%propagator( vArg%e(3) ,uu(i3) ,momentum(p3) )
      endif
      vArg%u3 = uu(i3)
!
      i4 = tWork%Vertex(ivtx)%Leg(4)
      if (i4.eq.0) then
        vArg%cPrim = fBas%Interaction(vArg%eRef)%CnstRef
        vArg%cSecn = fAug%Interaction(vArg%fRef)%CnstRef
        vArg%pSecn = sort_4( vArg%f(0:3) )
        u0 = fBas%Interaction( vArg%eRef )%Vertex( vArg )
!
      else!(i4.ne.0)
        j4 = tBas%Vertex(jvtx)%Leg(4)
        p4 = mod(tBas%Current(j4)%Mother,pMod)
        vArg%f(4) = tWork%Current(i4)%Property
        vArg%e(4) = tBas%Current(j4)%Property
        vArg%eRef = fBas%Reference(vArg%e(4),vArg%eRef)
        vArg%fRef = fAug%Reference(vArg%f(4),vArg%fRef)
        vArg%p4 = momentum(p4)
!       
        if (putProp(i4)) then
          putProp(i4) = .false.
          call fBas%Particle(vArg%e(4))%propagator( vArg%e(4) ,uu(i4) ,momentum(p4) )
        endif
        vArg%u4 = uu(i4)
!       i5.eq.0, no 6-point vertices
        vArg%cPrim = fBas%Interaction(vArg%eRef)%CnstRef
        vArg%cSecn = fAug%Interaction(vArg%fRef)%CnstRef
        vArg%pSecn = sort_5( vArg%f(0:4) )
        u0 = fBas%Interaction( vArg%eRef )%Vertex( vArg )
      endif
!
    endif
!
    uu(i0) = oscPlus( uu(i0) ,u0 )
!    write(*,*) i0,i1,i2,i3 ,ivtx!DEBUG
!    if (i3.ne.0) call print_lorentz(uu(i3),4) !DEBUG
!    call print_lorentz(uu(i2),4) !DEBUG
!    call print_lorentz(uu(i1),4) !DEBUG
!    call print_lorentz(u0   ,4) !DEBUG
!    call print_lorentz(uu(i0),4) !DEBUG
!
  enddo
!
  rslt = contract( xCurrent(Nxtrn) ,uu(i0) )
! call print_lorentz(xCurrent(Nxtrn),4) !DEBUG
! write(*,*) rslt !DEBUG
! write(*,*) 'Stopped in avh_amplitude'; stop !DEBUG
  end subroutine 
 
 
  subroutine daughters( rslt &
                       ,tBas ,fBas &
                       ,list &
                       ,momentum,pZero ,xCurrent )
!***********************************************************************
!***********************************************************************
  type(daughterList_type),intent(in) :: list
  !(complex2!)                       :: rslt(list%Ndaughters)
  type(dsskeleton_type) ,intent(in) :: tBas
  type(fusionRules_type),intent(in) :: fBas
  integer               ,intent(in) :: pZero
  type(!(cmplxMomentum_type!)),intent(in) :: momentum(0:pZero)
  type(!(cmplxCurrent_type!)) ,intent(in) :: xCurrent(tBas%Nleaves+1)
  type(!(cmplxCurrent_type!)) :: u0
  type(vertexArgs_type) :: vArg
  integer :: ivtx,Nxtrn,jvtx,pMod
  integer :: i0,i1,i2,i3,i4,p0,p1,p2,p3,p4
  integer :: jj
!
  Nxtrn = tBas%Nleaves+1
  pMod = pZero+1
!
  if (tBas%Ncurrents.gt.NcurrentsMax) call enlarge_c(tBas%Ncurrents)
!
  uu(1:tBas%Nleaves) = xCurrent(1:tBas%Nleaves)
!
  putProp(0:tBas%Nleaves) = .false.
!
  do jj=1,list%Ndaughters
!
    putProp(Nxtrn:tBas%Ncurrents) = .true.
    call putToZero( uu ,Nxtrn,tBas%Ncurrents )
!  
    do ivtx=1,list%daughter(jj)%Nvertices
      jvtx = list%daughter(jj)%MotherVertex(ivtx)
      vArg%Fsign = tBas%Vertex(jvtx)%FermSign
      i0 = tBas%Vertex(jvtx)%Leg(0)
      i1 = tBas%Vertex(jvtx)%Leg(1)
      i2 = tBas%Vertex(jvtx)%Leg(2)
      p0 = mod(tBas%Current(i0)%Mother,pMod)
      p1 = mod(tBas%Current(i1)%Mother,pMod)
      p2 = mod(tBas%Current(i2)%Mother,pMod)
      vArg%e(0) = fBas%Particle(tBas%Current(i0)%Property)%Anti
      vArg%e(1) = tBas%Current(i1)%Property
      vArg%e(2) = tBas%Current(i2)%Property
      vArg%eRef = fBas%Reference(vArg%e(2),fBas%Reference(vArg%e(1),fBas%Reference(vArg%e(0),0)))
      vArg%pSecn = 1
      vArg%p0 = momentum(p0)
      vArg%p1 = momentum(p1)
      vArg%p2 = momentum(p2)
!
      if (putProp(i1)) then
        putProp(i1) = .false.                            
        call fBas%Particle(vArg%e(1))%propagator( vArg%e(1) ,uu(i1) ,momentum(p1) )
      endif                                                  
      if (putProp(i2)) then                                         
        putProp(i2) = .false.                                       
        call fBas%Particle(vArg%e(2))%propagator( vArg%e(2) ,uu(i2) ,momentum(p2) )
      endif
      vArg%u1 = uu(i1)
      vArg%u2 = uu(i2)
!
      i3 = tBas%Vertex(jvtx)%Leg(3)
      if (i3.eq.0) then
        vArg%cPrim = fBas%Interaction(vArg%eRef)%CnstRef
        vArg%cSecn = list%daughter(jj)%CnstRef(ivtx)
        u0 = fBas%Interaction( vArg%eRef )%Vertex( vArg )
!  
      else!(i3.ne.0)
        p3 = mod(tBas%Current(i3)%Mother,pMod)
        vArg%e(3) = tBas%Current(i3)%Property
        vArg%eRef = fBas%Reference(vArg%e(3),vArg%eRef)
        vArg%p3 = momentum(p3)
!
        if (putProp(i3)) then
          putProp(i3) = .false.                            
          call fBas%Particle(vArg%e(3))%propagator( vArg%e(3) ,uu(i3) ,momentum(p3) )
        endif
        vArg%u3 = uu(i3)
!
        i4 = tBas%Vertex(jvtx)%Leg(4)
        if (i4.eq.0) then
          vArg%cPrim = fBas%Interaction(vArg%eRef)%CnstRef
          vArg%cSecn = list%daughter(jj)%CnstRef(ivtx)
          u0 = fBas%Interaction( vArg%eRef )%Vertex( vArg )
!
        else!(i4.ne.0)
          p4 = mod(tBas%Current(i4)%Mother,pMod)
          vArg%e(4) = tBas%Current(i4)%Property
          vArg%eRef = fBas%Reference(vArg%e(4),vArg%eRef)
          vArg%p4 = momentum(p4)
!         
          if (putProp(i4)) then
            putProp(i4) = .false.                            
            call fBas%Particle(vArg%e(4))%propagator( vArg%e(4) ,uu(i4) ,momentum(p4) )
          endif
          vArg%u4 = uu(i4)
!         i5.eq.0, no 6-point vertices
          vArg%cPrim = fBas%Interaction(vArg%eRef)%CnstRef
          vArg%cSecn = list%daughter(jj)%CnstRef(ivtx)
          u0 = fBas%Interaction( vArg%eRef )%Vertex( vArg )
        endif
!
      endif
!
      uu(i0) = oscPlus( uu(i0) ,u0 )
!      if (jj.eq.1) then !DEBUG
!      write(*,'(a15,5i4,a7,i6)') 'i0 i1 i2 i3 i4:',i0,i1,i2,i3,i4,', ivtx:',ivtx!DEBUG
!      if (i3.ne.0) then !DEBUG
!        call print_lorentz(uu(i3),4) !DEBUG
!        if (i4.ne.0) then !DEBUG
!          call print_lorentz(uu(i4),4) !DEBUG
!        endif !DEBUG
!      endif !DEBUG
!      call print_lorentz(uu(i2),4) !DEBUG
!      call print_lorentz(uu(i1),4) !DEBUG
!      call print_lorentz(u0   ,4) !DEBUG
!      call print_lorentz(uu(i0),4) !DEBUG
!      endif !DEBUG
!  
    enddo
!  
    rslt(jj) = contract( xCurrent(Nxtrn) ,uu(i0) )
! call print_lorentz(xCurrent(Nxtrn),4) !DEBUG
! write(*,*) rslt(jj) !DEBUG
! write(*,*) 'Stopped in avh_amplitude'; stop !DEBUG
!    if (.not.real(rslt(jj)).ge.0.and..not.real(rslt(jj)).le.0) stop !DEBUG
!
  enddo !jj=1,list%Ndaughters
  end subroutine 


  subroutine enlarge_l1( xx ,l1,u1 )
  logical &
  include '../arrays/enlarge1.h90'
  end subroutine

  subroutine enlarge_u1( xx ,l1,u1 )
  type(!(cmplxCurrent_type!)) &
  include '../arrays/enlarge1.h90'
  end subroutine

  subroutine enlarge_u2( xx ,l1,u1 ,l2,u2 )
  type(!(cmplxCurrent_type!)) &
  include '../arrays/enlarge2.h90'
  end subroutine

end module


