module avh_ranXconf
  !(usekindmod!)
  use avh_prnt
  use avh_kskeleton ,only: size_xternal
  implicit none
  private
  public :: ranXconf_type

  integer,parameter :: base(15)=[1,3,9,27,81,243,729,2187,6561,19683 &
                                 ,59049,177147,531441,1594323,4782969]

  type :: ranXconf_type
    integer :: Nxtrn,Nconf,Iconf=0,nbatch=0,ndat=0
    integer,allocatable :: conf(:),val(:)
    !(realknd2!),allocatable :: w(:),p(:)
  contains
    procedure :: exit=>x_exit
    procedure :: sum=>x_sum
    procedure :: fill=>x_fill
    procedure :: gnrt=>x_gnrt
    procedure :: wght=>x_wght
    procedure :: collect=>x_collect
    procedure :: digital=>x_digital
    procedure :: prnt_all=>x_prnt_all
  end type

  !(realknd2!),save :: small
  logical,save :: initz=.true.

contains


  subroutine init
  if (initz) initz=.false.
  small = 1d-16
  end subroutine


  subroutine x_prnt_all( x ,writeUnit )
  class(ranXconf_type) :: x
  integer,optional,intent(in) :: writeUnit
  integer :: ii,wUnit,xConfVal(size_xternal),jj
  wUnit = 6
  if (present(writeUnit)) wUnit = writeUnit
  do ii=1,x%Nconf
    call get_xConf( xConfVal(1:x%Nxtrn) ,x%Nxtrn ,x%conf(ii) )
    do jj=1,x%Nxtrn
      xConfVal(jj) = x%val(xConfVal(jj))
    enddo
    write(wUnit,'(a14,i5,a1,99i3)') 'ranXconf conf.',ii,':',xConfVal(1:x%Nxtrn)
  enddo
  end subroutine


  subroutine get_xConf( rslt ,Nxtrn ,conf )
!**********************************************************************
! valid for any base up to 9
!**********************************************************************
  integer,intent(in) :: Nxtrn,conf
  integer,intent(out) :: rslt(Nxtrn)
  integer :: jj,h0,h1,h2,h3,h4,h5,h6,h7,h8
  h0 = conf
  do jj=Nxtrn,1,-1
    h1 = h0-base(jj)
    if (h1.ge.0) then
      h2 = h1-base(jj)
      if (h2.ge.0) then
        h3 = h2-base(jj)
        if (h3.ge.0) then
          h4 = h3-base(jj)
          if (h4.ge.0) then
            h5 = h4-base(jj)
            if (h5.ge.0) then
              h6 = h5-base(jj)
              if (h6.ge.0) then
                h7 = h6-base(jj)
                if (h7.ge.0) then
                  h8 = h7-base(jj)
                  if (h8.ge.0) then
                    rslt(jj) = 9
                    h0 = h8
                  else
                    rslt(jj) = 8
                    h0 = h7
                  endif
                else
                  rslt(jj) = 7
                  h0 = h6
                endif
              else
                rslt(jj) = 6
                h0 = h5
              endif
            else
              rslt(jj) = 5
              h0 = h4
            endif
          else
            rslt(jj) = 4
            h0 = h3
          endif
        else
          rslt(jj) = 3
          h0 = h2
        endif
      else
        rslt(jj) = 2
        h0 = h1
      endif
    else
      rslt(jj) = 1
    endif
  enddo
  end subroutine


  subroutine x_fill( x ,NxDof ,val ,Nbatch ,batchFac ,option )
  class(ranXconf_type),intent(out) :: x
  integer,intent(in) :: NxDof(:),val(:)
  integer,optional,intent(in) :: Nbatch,batchFac
  character(*),optional,intent(in) :: option
  integer :: ii,jj
  if (initz) call init
  if (maxval(NxDof).gt.size(val)) then
    write(*,*) 'ERROR in ranxconf: more degrees of freedom than values provided'
    stop
  endif
  x%Nxtrn = size(NxDof,1)
  call resize_i( x%conf ,1,1 )
  x%Iconf = 0
  x%conf = 0
  x%Nconf = 1
  do ii=1,x%Nxtrn
    call resize_i( x%conf ,1,x%Nconf*NxDof(ii) )
    do jj=NxDof(ii)-1,0,-1
      x%conf( x%Nconf*jj+1 : x%Nconf*(jj+1) ) = jj*base(ii) + x%conf(1:x%Nconf)
    enddo
    x%Nconf = x%Nconf*NxDof(ii)
  enddo
  call resize_r2( x%w ,1,x%Nconf )
  call resize_r2( x%p ,0,x%Nconf+1 )
  x%p(0) = 0
  do ii=1,x%Nconf
    x%w(ii)= 0
    x%p(ii)= ii
  enddo
  if (.not.present(option)) then
    x%p = x%p/x%Nconf
  elseif (option.ne.'sum') then
    x%p = x%p/x%Nconf
  endif
  x%p(x%Nconf+1) = x%p(x%Nconf)+1
  x%Ndat = 0
  x%Nbatch = 128*x%Nconf
  call resize_i( x%val ,1,size(val) ) ;x%val = val
  if (present(batchFac)) x%Nbatch=x%Nbatch*batchFac
  if (present(Nbatch)) x%Nbatch=Nbatch
  end subroutine


  subroutine x_gnrt( x ,xConfVal ,rho )
  class(ranXconf_type) :: x
  integer,intent(out) :: xConfVal(x%Nxtrn)
  !(realknd2!),intent(in) :: rho
  integer :: i0,i1,ii
  i0 = 0
  i1 = x%Nconf
  do while (i1-i0.gt.1)
    ii = (i0+i1)/2
    if (rho.le.x%p(ii)) then
      i1 = ii
    else
      i0 = ii
    endif
  enddo
  x%Iconf = i1
  call get_xConf( xConfVal ,x%Nxtrn ,x%conf(x%Iconf) )
  do ii=1,x%Nxtrn
    xConfVal(ii) = x%val(xConfVal(ii))
  enddo
  end subroutine


  function x_wght( x ) result(rslt)
  class(ranXconf_type) :: x
  !(realknd2!) :: rslt
  rslt = 1/( x%p(x%Iconf) - x%p(x%Iconf-1) )
  end function


  subroutine x_collect( x ,wght )
  class(ranXconf_type) :: x
  !(realknd2!),intent(in) :: wght
  integer :: ii,jj
!
  if (x%Nbatch.le.0) return
!
! Make sure the weight is used only once
  ii = x%Iconf
  if (ii.eq.0) return
  x%Iconf = 0
!   
  if (wght.le.0) return
! Update the weights of the channels
  x%w(ii) = x%w(ii) + wght    ! entropy
! x%w(ii) = x%w(ii) + wght**2 ! variance
  x%Ndat = x%Ndat+1
! 
  if (x%Ndat.lt.x%Nbatch) return
  x%Ndat = 0
! Activate the updated bins for gnrt/wght
  do jj=1,x%Nconf
    x%p(jj) = x%p(jj-1) + x%w(jj)        ! entropy
!   x%p(jj) = x%p(jj-1) + dsqrt(x%w(jj)) ! variance
  enddo
  x%p(1:x%Nconf-1) = x%p(1:x%Nconf-1)/x%p(x%Nconf)
  x%p(x%Nconf) = 1
  end subroutine


  subroutine x_digital( x ,wght )
  class(ranXconf_type) :: x
  !(realknd2!),intent(in) :: wght
  !(realknd2!) :: thrs
  integer :: jj
!
  if (x%Nbatch.le.0) return
!
  if (wght.le.0) return
  x%w(x%Iconf) = x%w(x%Iconf) + wght
  x%Ndat = x%Ndat+1
! 
  if (x%Ndat.lt.x%Nbatch) return
  x%Ndat = 0
  thrs = small*maxval(x%w)
  do jj=1,x%Nconf
    x%p(jj) = x%p(jj-1)
    if (x%w(jj).gt.thrs) x%p(jj) = x%p(jj) + 1
  enddo
  x%p(1:x%Nconf-1) = x%p(1:x%Nconf-1)/x%p(x%Nconf)
  x%p(x%Nconf) = 1
  end subroutine


  function x_exit( x ,xConfVal ,wght ) result(rslt)
!***********************************************************************
!  amp = 0
!  do ;if (ranXconf%exit(xConfVal,wght)) exit
!    call evaluate_your_amplitude( tmp ,xConfVal )
!    call ranXconf%sum(tmp)
!    amp = amp + tmp*wght
!  enddo
!***********************************************************************
  class(ranXconf_type) :: x
  integer,intent(out) :: xConfVal(x%Nxtrn)
  !(realknd2!),intent(out) :: wght
  integer :: ii
  logical :: rslt
  x%Iconf = x%Iconf+1
  do while (x%p(x%Iconf)-x%p(x%Iconf-1).lt.small*x%p(x%Nconf))
    x%Iconf = x%Iconf+1
  enddo
  rslt = (x%Iconf.gt.x%Nconf)
  if (rslt) then
    x%Iconf = 0
  else
    call get_xConf( xConfVal ,x%Nxtrn , x%conf(x%Iconf) )
    do ii=1,x%Nxtrn
      xConfVal(ii) = x%val(xConfVal(ii))
    enddo
    wght = x%p(x%Iconf)-x%p(x%Iconf-1)
  endif
  end function

  subroutine x_sum( x ,wght )
  class(ranXconf_type) :: x
  !(realknd2!),intent(in) :: wght
  !(realknd2!) :: thrs
  integer :: jj,kk,ll
!
  if (x%Nbatch.le.0) return
!
  x%w(x%Iconf) = x%w(x%Iconf) + wght
!
  if (x%Iconf.eq.x%Nconf) x%Ndat = x%Ndat+1
! 
  if (x%Ndat.lt.x%Nbatch) return
  x%Ndat = 0
  x%Nbatch = 0
  thrs = small*maxval(x%w)
  do jj=1,x%Nconf
    x%p(jj) = x%p(jj-1)
    if (x%w(jj).gt.thrs) x%p(jj) = x%p(jj) + 1
  enddo
  x%p(x%Nconf+1) = x%p(x%Nconf)+1
  do jj=1,x%Nconf
    if (x%w(jj).le.thrs) cycle
    do kk=1,jj-1
      if (abs(x%w(jj)-x%w(kk)).le.thrs*1d8) then
        do ll=kk,jj-1
          x%p(ll) = x%p(ll)+1
        enddo
        exit
      endif
    enddo
  enddo
  jj = x%Nconf
  do
    if (jj.le.0) exit
    if (x%p(jj-1).ne.x%p(jj)) exit
    jj = jj-1
  enddo
  x%Nconf = jj
  x%p(x%Nconf+1) = x%p(x%Nconf)+1
  x%Iconf = x%Nconf
  end subroutine


  subroutine resize_i( xx ,l1,u1 )
  integer &
    ,allocatable :: xx(:) ,tt(:)
  intent(inout) :: xx
  integer,intent(in) :: l1,u1
  integer :: lb(1),ub(1)
  if (.not.allocated(xx)) then
    allocate(xx(l1:u1))
    return
  endif
  lb=lbound(xx) ;ub=ubound(xx)
  allocate(tt(lb(1):ub(1)))
  tt = xx
  deallocate(xx)
  allocate( xx(l1:u1) )
  lb(1)=max(l1,lb(1)) ;ub(1)=min(u1,ub(1))
  xx(lb(1):ub(1)) = tt(lb(1):ub(1))
  deallocate(tt)
  end subroutine 

  subroutine resize_r2( xx ,l1,u1 )
  !(realknd2!) &
    ,allocatable :: xx(:) ,tt(:)
  intent(inout) :: xx
  integer,intent(in) :: l1,u1
  integer :: lb(1),ub(1)
  if (.not.allocated(xx)) then
    allocate(xx(l1:u1))
    return
  endif
  lb=lbound(xx) ;ub=ubound(xx)
  allocate(tt(lb(1):ub(1)))
  tt = xx
  deallocate(xx)
  allocate( xx(l1:u1) )
  lb(1)=max(l1,lb(1)) ;ub(1)=min(u1,ub(1))
  xx(lb(1):ub(1)) = tt(lb(1):ub(1))
  deallocate(tt)
  end subroutine 

end module


