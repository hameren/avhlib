module avh_evalskel_t
  !(usekindmod!)
  use avh_iounits
  use avh_mathcnst
  use avh_othercnst
  use !(cmplxCurrents_mod!)
  use avh_fusionTools
  use avh_buildskel
 
  implicit none
  private
  public :: evaluate,evalskel_work_type

  type :: evalskel_work_type
    type(!(cmplxCurrent_type!)),allocatable :: uu(:)
    logical                    ,allocatable :: putProp(:)
  contains
    procedure :: alloc
  end type


contains


  subroutine alloc( work ,Nsize )
  class(evalskel_work_type) :: work
  integer,intent(in) :: Nsize
  if (allocated(work%uu     )) deallocate(work%uu)
  if (allocated(work%putProp)) deallocate(work%putProp)
  if (Nsize.ge.0) then
    allocate(work%uu(     1:Nsize))
    allocate(work%putProp(0:Nsize))
  endif
  end subroutine

 
  subroutine evaluate( rslt &
                      ,tBas ,fBas &
                      ,list &
                      ,momentum,pZero ,xCurrent ,work )
!***********************************************************************
! arrays in "work" should have size "tBas%Ncurrents"
!***********************************************************************
  type(daughterList_type),intent(in) :: list
  !(complex2!)                       :: rslt(list%Ndaughters)
  type(dsskeleton_type) ,intent(in) :: tBas
  type(fusionRules_type),intent(in) :: fBas
  integer               ,intent(in) :: pZero
  type(!(cmplxMomentum_type!)),intent(in) :: momentum(0:pZero)
  type(!(cmplxCurrent_type!)) ,intent(in) :: xCurrent(tBas%Nleaves+1)
  type(evalskel_work_type) ,intent(inout) :: work
  type(!(cmplxCurrent_type!)) :: u0
  type(vertexArgs_type) :: vArg
  integer :: ivtx,Nxtrn,jvtx,pMod,i0,i1,i2,i3,i4,p0,p1,p2,p3,p4,jj
  associate( uu=>work%uu ,putProp=>work%putProp )
!
  Nxtrn = tBas%Nleaves+1
  pMod = pZero+1
!
  uu(1:tBas%Nleaves) = xCurrent(1:tBas%Nleaves)
!
  putProp(0:tBas%Nleaves) = .false.
!
  do jj=1,list%Ndaughters
!
    putProp(Nxtrn:tBas%Ncurrents) = .true.
    call putToZero( uu ,Nxtrn,tBas%Ncurrents )
!  
    do ivtx=1,list%daughter(jj)%Nvertices
      jvtx = list%daughter(jj)%MotherVertex(ivtx)
      vArg%Fsign = tBas%Vertex(jvtx)%FermSign
      i0 = tBas%Vertex(jvtx)%Leg(0)
      i1 = tBas%Vertex(jvtx)%Leg(1)
      i2 = tBas%Vertex(jvtx)%Leg(2)
      p0 = mod(tBas%Current(i0)%Mother,pMod)
      p1 = mod(tBas%Current(i1)%Mother,pMod)
      p2 = mod(tBas%Current(i2)%Mother,pMod)
      vArg%e(0) = fBas%Particle(tBas%Current(i0)%Property)%Anti
      vArg%e(1) = tBas%Current(i1)%Property
      vArg%e(2) = tBas%Current(i2)%Property
      vArg%eRef = fBas%Reference(vArg%e(2),fBas%Reference(vArg%e(1),fBas%Reference(vArg%e(0),0)))
      vArg%pSecn = 1
      vArg%p0 = momentum(p0)
      vArg%p1 = momentum(p1)
      vArg%p2 = momentum(p2)
!
      if (putProp(i1)) then
        putProp(i1) = .false.                            
        call fBas%Particle(vArg%e(1))%propagator( vArg%e(1) ,uu(i1) ,momentum(p1) )
      endif                                                  
      if (putProp(i2)) then                                         
        putProp(i2) = .false.                                       
        call fBas%Particle(vArg%e(2))%propagator( vArg%e(2) ,uu(i2) ,momentum(p2) )
      endif
      vArg%u1 = uu(i1)
      vArg%u2 = uu(i2)
!
      i3 = tBas%Vertex(jvtx)%Leg(3)
      if (i3.eq.0) then
        vArg%cPrim = fBas%Interaction(vArg%eRef)%CnstRef
        vArg%cSecn = list%daughter(jj)%CnstRef(ivtx)
        u0 = fBas%Interaction( vArg%eRef )%Vertex( vArg )
!  
      else!(i3.ne.0)
        p3 = mod(tBas%Current(i3)%Mother,pMod)
        vArg%e(3) = tBas%Current(i3)%Property
        vArg%eRef = fBas%Reference(vArg%e(3),vArg%eRef)
        vArg%p3 = momentum(p3)
!
        if (putProp(i3)) then
          putProp(i3) = .false.                            
          call fBas%Particle(vArg%e(3))%propagator( vArg%e(3) ,uu(i3) ,momentum(p3) )
        endif
        vArg%u3 = uu(i3)
!
        i4 = tBas%Vertex(jvtx)%Leg(4)
        if (i4.eq.0) then
          vArg%cPrim = fBas%Interaction(vArg%eRef)%CnstRef
          vArg%cSecn = list%daughter(jj)%CnstRef(ivtx)
          u0 = fBas%Interaction( vArg%eRef )%Vertex( vArg )
!
        else!(i4.ne.0)
          p4 = mod(tBas%Current(i4)%Mother,pMod)
          vArg%e(4) = tBas%Current(i4)%Property
          vArg%eRef = fBas%Reference(vArg%e(4),vArg%eRef)
          vArg%p4 = momentum(p4)
!         
          if (putProp(i4)) then
            putProp(i4) = .false.                            
            call fBas%Particle(vArg%e(4))%propagator( vArg%e(4) ,uu(i4) ,momentum(p4) )
          endif
          vArg%u4 = uu(i4)
!         i5.eq.0, no 6-point vertices
          vArg%cPrim = fBas%Interaction(vArg%eRef)%CnstRef
          vArg%cSecn = list%daughter(jj)%CnstRef(ivtx)
          u0 = fBas%Interaction( vArg%eRef )%Vertex( vArg )
        endif
!
      endif
!
      uu(i0) = oscPlus( uu(i0) ,u0 )
!      if (jj.eq.1) then !DEBUG
!      write(*,'(a15,5i4,a7,i6)') 'i0 i1 i2 i3 i4:',i0,i1,i2,i3,i4,', ivtx:',ivtx!DEBUG
!      if (i3.ne.0) then !DEBUG
!        call print_lorentz(uu(i3),4) !DEBUG
!        if (i4.ne.0) then !DEBUG
!          call print_lorentz(uu(i4),4) !DEBUG
!        endif !DEBUG
!      endif !DEBUG
!      call print_lorentz(uu(i2),4) !DEBUG
!      call print_lorentz(uu(i1),4) !DEBUG
!      call print_lorentz(u0   ,4) !DEBUG
!      call print_lorentz(uu(i0),4) !DEBUG
!      endif !DEBUG
!  
    enddo
!  
    rslt(jj) = contract( xCurrent(Nxtrn) ,uu(i0) )
! call print_lorentz(xCurrent(Nxtrn),4) !DEBUG
! write(*,*) rslt(jj) !DEBUG
! write(*,*) 'Stopped in avh_amplitude'; stop !DEBUG
!    if (.not.real(rslt(jj)).ge.0.and..not.real(rslt(jj)).le.0) stop !DEBUG
!
  enddo !jj=1,list%Ndaughters
  end associate
  end subroutine 


 end module


