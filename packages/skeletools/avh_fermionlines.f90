module avh_fermionLines
  use avh_kskeleton, only: base

  implicit none
  private
  public :: bosTyp ,bgnTyp ,endTyp ,nulTyp ,is_fermion
  public :: fermionSign_type

  integer,parameter :: endTyp=1
  integer,parameter :: bosTyp=2
  integer,parameter :: bgnTyp=3
  integer,parameter :: nulTyp=4
  logical,parameter :: is_fermion(4)=(/.true.,.false.,.true.,.false./)

  type :: fermionSign_type
    private
    integer :: Nleaves=0
    logical,allocatable :: HasFermion(:,:) !(size_leaves,2**size_leaves)
    logical,allocatable :: CumulatOdd(:,:) !(size_leaves,2**size_leaves)
  contains
    procedure :: fill
    procedure :: sgn2
    procedure :: sgn3
    procedure :: sgn4
  end type


contains


  subroutine fill( x ,Nleaves ,isFerm )
!**********************************************************************
! Determines for each momentum p the logicals
!   x%HasFermion(i,p)=( mHat(i,p).eq.1 )
!   x%CumulatOdd(i,p)=odd( sum_{j=1}^{i-1}mHat(i,p) )
! with mHat as in
!   Eq.(15) of Draggiotis, Kleiss, Papadopoulos, hep-ph/0202201
!**********************************************************************
  class(fermionSign_type),intent(out) :: x
  integer,intent(in) :: Nleaves
  logical,intent(in) :: isFerm(Nleaves)
  logical :: finish,odd,bits(Nleaves)
  integer :: pp,ii,jj
  call resize( x%HasFermion ,1,Nleaves ,1,2**Nleaves )
  call resize( x%CumulatOdd ,1,Nleaves ,1,2**Nleaves )
  x%Nleaves = Nleaves
  pp = 0
  bits = .false.
  do ! loop over all bitstrings
! create next bitstring
    do ii=x%Nleaves,1,-1
      finish = bits(ii)
      if (finish) cycle
      pp=pp+base(ii) ;do jj=ii+1,x%Nleaves ;if (bits(jj)) pp=pp-base(jj) ;enddo
      bits(ii)=.true. ;bits(ii+1:x%Nleaves)=.false.
      exit
    enddo
    if (finish) exit
! operations given a bitstring
    odd = .false.
    x%CumulatOdd(1,pp) = .false.
    do ii=1,x%Nleaves-1
      if (bits(ii)) then
        x%HasFermion(ii,pp) = isFerm(ii)
        if (isFerm(ii)) odd = .not.odd
      else
        x%HasFermion(ii,pp) = .false.
      endif
      x%CumulatOdd(ii+1,pp) = odd
    enddo
    x%HasFermion(x%Nleaves,pp) = bits(x%Nleaves).and.isFerm(x%Nleaves)
  enddo
  end subroutine
 
  
  function sgn2( x ,p ) result(rslt)
  class(fermionSign_type),intent(in) :: x
  integer                ,intent(in) :: p(2)
  integer :: rslt,ii
  rslt = 1
  do ii=1,x%Nleaves
    if (x%HasFermion(ii,p(1)).and.x%CumulatOdd(ii,p(2))) rslt =-rslt
  enddo
  end function
  
  function sgn3( x ,p ) result(rslt)
  class(fermionSign_type),intent(in) :: x
  integer                ,intent(in) :: p(3)
  integer :: rslt,p5,ii
  rslt = 1
  p5 = p(2)+p(3)
  do ii=1,x%Nleaves
    if (x%HasFermion(ii,p(1)).and.x%CumulatOdd(ii,p5  )) rslt =-rslt
    if (x%HasFermion(ii,p(2)).and.x%CumulatOdd(ii,p(3))) rslt =-rslt
  enddo
  end function
  

  function sgn4( x ,p ) result(rslt)
  class(fermionSign_type),intent(in) :: x
  integer                ,intent(in) :: p(4)
  integer :: rslt,p7,p9,ii
  rslt = 1
  p7 = p(3)+p(4)
  p9 = p(2)+p7
  do ii=1,x%Nleaves
    if (x%HasFermion(ii,p(1)).and.x%CumulatOdd(ii,p9  )) rslt =-rslt
    if (x%HasFermion(ii,p(2)).and.x%CumulatOdd(ii,p7  )) rslt =-rslt
    if (x%HasFermion(ii,p(3)).and.x%CumulatOdd(ii,p(4))) rslt =-rslt
  enddo
  end function
  

  subroutine resize( xx ,l1,u1 ,l2,u2 )
  logical &
  include '../arrays/resize2.h90'
  end subroutine

end module


