module avh_buildskel
  use avh_prnt
  use avh_ioUnits
  use avh_othercnst
  use avh_doloops
  use avh_kskeleton
  use avh_fusionTools
  use avh_fermionLines
 
  implicit none
  private

! Propagate size_leaves from avh_kskeleton
  public :: size_leaves

  public :: dsskeleton_type
  public :: daughterList_type,daughter_type
  public :: fullTreeLevel,dress_skeleton,augment_skeleton,update_skeleton
  public :: print_skeleton
  public :: remove_internal_property
  public :: tWork

  type :: vertex_type
    integer :: Leg(0:4)=0,Mother,FermSign=1
  end type

  type :: current_type
    integer :: Mother,Property
  end type
 
  type :: dsskeleton_type
    integer :: Nleaves,Nvertices,Ncurrents,FirstRoot,pRoot
    integer,allocatable :: Process(:)
    integer,allocatable :: CurrentOfLeaf(:)
    type(vertex_type ),allocatable :: Vertex(:)
    type(current_type),allocatable :: Current(:)
  end type
 
  type :: daughter_type
    integer :: Label,Nvertices
    integer,allocatable :: MotherVertex(:)
    integer,allocatable :: CnstRef(:)
    integer,allocatable :: info(:,:)
  end type

  type :: daughterList_type
    integer :: Ndaughters=0
    logical :: added=.false.
    type(daughter_type),allocatable :: daughter(:)
  contains
    procedure :: add=>add_daughter
    procedure :: trim=>trim_daughterList
  end type

  logical,save :: allocWork=.true.
  integer,save :: NleavesMax=0,NverticesMax=0,NcurrentsMax=0
  integer,save :: NfusionsMax=0
  type(dsskeleton_type),save :: tWork
  integer,allocatable,save :: NnewOfOld(:),newOfOld(:,:),keep(:)
  logical,allocatable,save :: isThere(:)

  interface print_skeleton
    module procedure print_0,print_1,print_2
    module procedure print_daughters
  end interface

  interface update_skeleton
    module procedure update_1,update_2,update_3
  end interface

  interface resize
    module procedure resize_i1,resize_l1,resize_d1
  end interface

  interface enlarge
    module procedure enlarge_i2,enlarge_i1
    module procedure enlarge_v1,enlarge_c1,enlarge_l1
  end interface

  interface
  end interface
  
contains

  subroutine dealloc_work
  if (allocWork) return
  allocWork = .true.
  NleavesMax   = 0
  NverticesMax = 0
  NcurrentsMax = 0
  NfusionsMax  = 0
  deallocate( tWork%Process )
  deallocate( tWork%CurrentOfLeaf )
  deallocate( tWork%Current )
  deallocate( tWork%Vertex  )
  deallocate( keep          )
  deallocate( NnewOfOld     )
  deallocate(  newOfOld     )
  deallocate( isThere       )
  end subroutine

  subroutine alloc_work( Nleaves )
  integer,intent(in) :: Nleaves
  allocWork = .false.
  NleavesMax   = Nleaves
  NverticesMax = (4**Nleaves)/6
  NcurrentsMax = 2**Nleaves
  NfusionsMax  = 1
  allocate( tWork%Process(      1:NleavesMax+1) )
  allocate( tWork%CurrentOfLeaf(1:NleavesMax  ) )
  allocate( tWork%Vertex(1:NverticesMax) )
  allocate( keep(        1:NverticesMax) )
  allocate( tWork%Current(0:NcurrentsMax) )
  allocate( NnewOfOld(    0:NcurrentsMax) )
  allocate(  newOfOld(0:1,0:NcurrentsMax) )
  allocate( isThere(      0:NcurrentsMax) )
  end subroutine

  subroutine enlarge_l(Nnew)
  integer,intent(in) :: Nnew
  NleavesMax = Nnew
  call enlarge( tWork%Process ,1,Nnew+1 )
  call enlarge( tWork%CurrentOfLeaf ,1,Nnew )
  end subroutine

  subroutine enlarge_f(Nnew)
  integer,intent(in) :: Nnew
  NfusionsMax = Nnew
  call enlarge( newOfOld ,0,Nnew ,0,1 )
  end subroutine

  subroutine enlarge_c(Nnew)
  integer,intent(in) :: Nnew
  NcurrentsMax = Nnew
  call enlarge( tWork%Current ,0,Nnew )
  call enlarge( NnewOfOld ,0,Nnew )
  call enlarge( newOfOld ,0,1 ,0,Nnew )
  call enlarge( isThere ,0,Nnew )
  end subroutine

  subroutine enlarge_v(Nnew)
  integer,intent(in) :: Nnew
  NverticesMax = Nnew
  call enlarge( tWork%Vertex ,1,Nnew )
  call enlarge( keep ,1,Nnew )
  end subroutine

 
  subroutine dress_skeleton( incomplete ,t ,o ,f ,xProperty )
  logical               ,intent(out) :: incomplete
  type(dsskeleton_type) ,intent(out) :: t
  type(dsskeleton_type) ,intent(in ) :: o
  type(fusionRules_type),intent(in ) :: f
  integer               ,intent(in ) :: xProperty(o%Nleaves+1)
  include 'dress_skeleton.h90'
    !execute case: task | dress
  call copy_skeleton( t ,tWork )
  call put_fermionSign( t ,f )
  end subroutine

  subroutine augment_skeleton( incomplete ,o ,f ,xProperty )
  logical              ,intent(out) :: incomplete
  type(dsskeleton_type) ,intent(in) :: o
  type(fusionRules_type),intent(in) :: f
  integer               ,intent(in) :: xProperty(o%Nleaves+1)
  include 'dress_skeleton.h90'
    !execute case: task | augment
  end subroutine

  subroutine update_1( incomplete ,o ,f ,xProperty )
  logical              ,intent(out)   :: incomplete
  type(dsskeleton_type),intent(inout) :: o
  type(fusionRules_type),intent(in)   :: f
  integer              ,intent(in )   :: xProperty(o%Nleaves+1)
  logical :: keepProperty=.false.
  include 'dress_skeleton.h90'
    !execute case: task | update
  call copy_skeleton( o ,tWork )
  end subroutine

  subroutine update_2( incomplete ,o ,f ,xProperty ,keepProperty )
  logical              ,intent(out) :: incomplete
  type(dsskeleton_type),intent(in ) :: o
  type(fusionRules_type),intent(in) :: f
  integer              ,intent(in ) :: xProperty(o%Nleaves+1)
  logical              ,intent(in ) :: keepProperty
  include 'dress_skeleton.h90'
    !execute case: task | update
  end subroutine

  subroutine update_3( incomplete ,o ,f ,xProperty )
  logical              ,intent(out) :: incomplete
  type(dsskeleton_type),intent(inout) :: o
  type(nonLocalRules_type),intent(in) :: f
  integer                 ,intent(in) :: xProperty(o%Nleaves+1)
  include 'dress_skeleton.h90'
    !execute case: task | nonlocal
  call copy_skeleton( o ,tWork )
  end subroutine


  subroutine remove_internal_property( incomplete ,t ,property )
!***********************************************************************
! Remove vertices which have l.h.s. with property
!***********************************************************************
  logical               ,intent(  out) :: incomplete
  type(dsskeleton_type) ,intent(inout) :: t
  integer               ,intent(in   ) :: property(:)
  integer :: ii,jj
  logical :: remove
!
  tWork%Nvertices = 0
  do ii=1,t%FirstRoot-1
    remove = .false.
    do jj=1,size(property)
      if (t%Current(t%Vertex(ii)%Leg(0))%Property.eq.property(jj)) then
        remove = .true.
        exit
      endif
    enddo
    if (remove) cycle
    tWork%Nvertices = tWork%Nvertices+1
    tWork%Vertex(tWork%Nvertices) = t%Vertex(ii)
  enddo
  tWork%FirstRoot = tWork%Nvertices+1
  do ii=t%FirstRoot,t%Nvertices
    tWork%Nvertices = tWork%Nvertices+1
    tWork%Vertex(tWork%Nvertices) = t%Vertex(ii)
  enddo
!
  tWork%Ncurrents = t%Ncurrents
  tWork%Nleaves   = t%Nleaves
  tWork%CurrentOfLeaf(1:t%Nleaves) = tWork%CurrentOfLeaf(1:t%Nleaves)
  call make_consistent( incomplete ,tWork )
  if (incomplete) return
!
  t%Nvertices = tWork%Nvertices
  t%FirstRoot = tWork%FirstRoot
  deallocate( t%Vertex ) ;allocate( t%Vertex(1:t%Nvertices) )
  t%Vertex(1:t%Nvertices) = tWork%Vertex(1:t%Nvertices)
  end subroutine


  subroutine make_consistent( incomplete ,t )
!***********************************************************************
! Remove vertices with currents on the r.h.s. that are not on
! the l.h.s. of any earlier vertex. Repeat this.
!***********************************************************************
  logical,intent(out) :: incomplete
  type(dsskeleton_type),intent(inout) :: t
  integer :: newNvtx,ii
  do
    isThere(0:t%Ncurrents) = .false.
    isThere(0) = .true.
    do ii=1,t%Nleaves
      isThere(t%CurrentOfLeaf(ii)) = .true.
    enddo
    do ii=1,t%Nvertices
      isThere(t%Vertex(ii)%Leg(0)) = .true.
    enddo 
    newNvtx = 0
    do ii=1,t%Nvertices
      if (isThere(t%Vertex(ii)%Leg(1)).and.&
          isThere(t%Vertex(ii)%Leg(2)).and.&
          isThere(t%Vertex(ii)%Leg(3)).and.&
          isThere(t%Vertex(ii)%Leg(4))     ) then
        newNvtx = newNvtx+1
        keep(newNvtx) = ii
      endif
    enddo
!
    if (newNvtx.eq.t%Nvertices) exit
!  
    t%Nvertices = newNvtx
    incomplete = .true.
    do ii=1,t%Nvertices
      t%Vertex(ii) = t%Vertex(keep(ii))
      if (incomplete.and.keep(ii).ge.t%FirstRoot) then
        t%FirstRoot = ii
        incomplete = .false.
      endif
    enddo
    if (incomplete) return
  enddo
!
  incomplete = (t%Nvertices.le.0)
  end subroutine

  subroutine remove_dead_ends( t )
!***********************************************************************
! Find dead ends, ie, vertices with osc-s on the left-hand-side
! that are not on a right-hand-side
!***********************************************************************
  type(dsskeleton_type),intent(inout) :: t
  integer :: newNvtx,ii,jj
  isThere(0:t%Ncurrents) = .false.
  newNvtx = 0
  do ii=t%Nvertices,t%FirstRoot,-1
    isThere(t%Vertex(ii)%Leg(1)) = .true.
    isThere(t%Vertex(ii)%Leg(2)) = .true.
    if (t%Vertex(ii)%Leg(3).gt.0) then
      isThere(t%Vertex(ii)%Leg(3)) = .true.
      if (t%Vertex(ii)%Leg(4).gt.0) then
        isThere(t%Vertex(ii)%Leg(4)) = .true.
      endif
    endif
    newNvtx=newNvtx+1; keep(newNvtx)=ii
  enddo
  ii = t%FirstRoot-1
  do while (ii.ge.1)
    if (isThere(t%Vertex(ii)%Leg(0))) then
      isThere(t%Vertex(ii)%Leg(1)) = .true.
      isThere(t%Vertex(ii)%Leg(2)) = .true.
      if (t%Vertex(ii)%Leg(3).gt.0) then
        isThere(t%Vertex(ii)%Leg(3)) = .true.
        if (t%Vertex(ii)%Leg(4).gt.0) then
          isThere(t%Vertex(ii)%Leg(4)) = .true.
        endif
      endif
      newNvtx=newNvtx+1; keep(newNvtx)=ii
    endif
    ii = ii-1
  enddo
  t%Nvertices = newNvtx
  do ii=1,t%Nvertices
    jj = t%Nvertices-ii+1
    t%Vertex(ii) = t%Vertex(keep(jj))
  enddo
  end subroutine


  subroutine copy_skeleton( a ,f )
!***********************************************************************
!***********************************************************************
  type(dsskeleton_type),intent(out) :: a
  type(dsskeleton_type),intent(in)  :: f
  a%Nleaves   = f%Nleaves
  a%Nvertices = f%Nvertices
  a%Ncurrents = f%Ncurrents
  a%FirstRoot = f%FirstRoot
  a%pRoot     = f%pRoot
  if (allocated(a%Process      )) deallocate(a%Process)
  if (allocated(a%CurrentOfLeaf)) deallocate(a%CurrentOfLeaf)
  if (allocated(a%Vertex       )) deallocate(a%Vertex)
  if (allocated(a%Current      )) deallocate(a%Current)
  allocate( a%Process(1:f%Nleaves+1) )
  allocate( a%CurrentOfLeaf(1:a%Nleaves) )
  allocate( a%Vertex(1:a%Nvertices) )
  allocate( a%Current(0:a%Ncurrents) )
  a%Process(1:f%Nleaves+1)     = f%Process(1:f%Nleaves+1)
  a%CurrentOfLeaf(1:f%Nleaves) = f%CurrentOfLeaf(1:f%Nleaves)
  a%Vertex(1:a%Nvertices)      = f%Vertex(1:a%Nvertices)
  a%Current(0:a%Ncurrents)     = f%Current(0:a%Ncurrents)           
  end subroutine


  function fullTreeLevel( Nxtrn ,vertMult ) result(k)
!***********************************************************************
! Fill "k" with all kinematical vertices for tree-level amplitude
!***********************************************************************
  type(dsskeleton_type) :: k
  integer         ,intent(in) :: Nxtrn
  integer,optional,intent(in) :: vertMult
  integer :: ii,Nleaves,Ncurrents,Nvertices,maxVertMult
!
  maxVertMult = 4
  if (present(vertMult)) maxVertMult = vertMult
!
  Nleaves = Nxtrn-1
  Ncurrents = 2**Nleaves
!
  if     (maxVertMult.eq.3) then ;Nvertices=(3**Nleaves)/2
  elseif (maxVertMult.eq.4) then ;Nvertices=(4**Nleaves)/6
                            else ;Nvertices=(5**Nleaves+4**Nleaves)/24
  endif
!
  if (allocWork) call alloc_work( Nleaves )
  if (Nleaves  .gt.NleavesMax  ) call enlarge_l(Nleaves  )
  if (Ncurrents.gt.NcurrentsMax) call enlarge_c(Ncurrents)
  if (Nvertices.gt.NverticesMax) call enlarge_v(Nvertices)
!
  tWork%Nleaves = Nleaves
  tWork%CurrentOfLeaf(1:tWork%Nleaves) = base(1:tWork%Nleaves)
!
  if     (maxVertMult.eq.3) then ;call incl_kskeleton3( tWork%Nleaves )
  elseif (maxVertMult.eq.4) then ;call incl_kskeleton4( tWork%Nleaves )
                            else ;call incl_kskeleton5( tWork%Nleaves )
  endif
!
  tWork%pRoot = base(tWork%Nleaves+1)-1
  do ii=tWork%Nvertices,1,-1
    if (tWork%Vertex(ii)%Leg(0).lt.tWork%pRoot) exit
  enddo
  tWork%FirstRoot = ii+1
!
  call copy_skeleton( k ,tWork )
  end function

  subroutine incl_kskeleton5( Nxtrn )
  include '../kskeleton/kskeleton5.h90'
    !execute xpnd: array | tWork%Vertex(#2)%Leg(#1)
  tWork%Nvertices = Nvrtx
  end subroutine
  subroutine incl_kskeleton4( Nxtrn )
  include '../kskeleton/kskeleton4.h90'
    !execute xpnd: array | tWork%Vertex(#2)%Leg(#1)
  tWork%Nvertices = Nvrtx
  end subroutine
  subroutine incl_kskeleton3( Nxtrn )
  include '../kskeleton/kskeleton3.h90'
    !execute xpnd: array | tWork%Vertex(#2)%Leg(#1)
  tWork%Nvertices = Nvrtx
  end subroutine


  subroutine put_fermionsign( t ,f )
!***********************************************************************
!***********************************************************************
  type(dsskeleton_type),intent(inout) :: t
  type(fusionRules_type),intent(in) :: f
  logical :: isFerm(t%Nleaves)
  integer :: jj,i(4),p(4)
  type(fermionSign_type) :: x
  do jj=1,t%Nleaves
    isFerm(jj) = is_fermion(f%Particle(t%Process(jj))%FermionTyp)
  enddo
  call x%fill( t%Nleaves ,isFerm )
  do jj=1,t%Nvertices
    p(1:3) = t%Current(t%Vertex(jj)%Leg(1:3))%Mother
    i(1:2) = f%Particle(t%Current(t%Vertex(jj)%Leg(1:2))%Property)%FermionTyp
    if (p(3).eq.0) then
      t%Vertex(jj)%FermSign = x%sgn2(p( perm_a_few(1:2,sort_2(i(1:2))) ))
    else
      p(4) = t%Current(t%Vertex(jj)%Leg(4))%Mother
      i(3) = f%Particle(t%Current(t%Vertex(jj)%Leg(3))%Property)%FermionTyp
      if (p(4).eq.0) then
        t%Vertex(jj)%FermSign = x%sgn3(p (perm_a_few(1:3,sort_3(i(1:3))) ))
      else
        i(4) = f%Particle(t%Current(t%Vertex(jj)%Leg(4))%Property)%FermionTyp
        t%Vertex(jj)%FermSign = x%sgn4(p( perm_a_few(1:4,sort_4(i(1:4))) ))
      endif
    endif
  enddo
  end subroutine


  subroutine add_daughter( list ,m ,f ,xProperty ,label ,info )
!***********************************************************************
!***********************************************************************
  class(daughterList_type) :: list
  type(dsskeleton_type)  ,intent(in) :: m
  type(fusionRules_type)             :: f
  integer                ,intent(in) :: xProperty(m%Nleaves+1) ,label
  integer       ,optional,intent(in) :: info(:,:)
  logical :: incomplete,lclX(0:4)
  integer :: jj,ii,nn,newNvtx,kk,vertexLab,NdghtOld,cnstRef,lclF(0:4)
  integer,allocatable :: vertexLabel(:)
  logical,allocatable :: xternal(:)
!
  list%added = .false.
  NdghtOld = list%Ndaughters
!
! Dress motherTree with new property
  call update_skeleton( incomplete ,m ,f ,xProperty ,keepProperty=.true. )
!  call print_skeleton( tWork ) !DEBUG
  if (incomplete) return
!
! Make space for new daughter
  nn = list%Ndaughters+1
  call resize( list%daughter ,1,nn )
  call resize( list%daughter(nn)%MotherVertex ,1,tWork%Nvertices )
  call resize( list%daughter(nn)%CnstRef      ,1,tWork%Nvertices )
  call resize( vertexLabel  ,1,tWork%Nvertices )
  call resize( xternal ,0,tWork%Ncurrents )
  list%Ndaughters = nn
  list%daughter(nn)%Label = label
  list%daughter(nn)%Nvertices = tWork%Nvertices
  if (present(info)) then
    if (allocated(list%daughter(nn)%info)) deallocate(list%daughter(nn)%info)
    allocate(list%daughter(nn)%info(lbound(info,1):ubound(info,1),lbound(info,2):ubound(info,2)))
    list%daughter(nn)%info = info
  endif
!
! Determine which currents are effectively external (can be other than leaf and root).
  xternal(1:tWork%Ncurrents) = .false.
  xternal(tWork%CurrentOfLeaf(1:tWork%Nleaves)) = .true.
  do ii=tWork%FirstRoot,tWork%Nvertices
    xternal(tWork%Vertex(ii)%Leg(0)) = .true.
  enddo
  do ii=1,tWork%FirstRoot-1
    lclX(:) = xternal(tWork%Vertex(ii)%Leg(:))
    lclF(:) = tWork%Current(tWork%Vertex(ii)%Leg(:))%Property
    lclF(0) = f%Particle(lclF(0))%Anti
    call f%Uxternal( lclX ,lclF )
    xternal(tWork%Vertex(ii)%Leg(:)) = lclX(:)
  enddo
! Need to go backwards too, like root were leaf.
  do ii=tWork%Nvertices,1,-1
    lclX(:) = xternal(tWork%Vertex(ii)%Leg(:))
    lclF(:) = tWork%Current(tWork%Vertex(ii)%Leg(:))%Property
    lclF(0) = f%Particle(lclF(0))%Anti
    call f%Uxternal( lclX ,lclF )
    xternal(tWork%Vertex(ii)%Leg(:)) = lclX(:)
  enddo
!
! Put motherVertices
  do ii=1,tWork%Nvertices
    lclX(:) = xternal(tWork%Vertex(ii)%Leg(:))
    lclF(:) = tWork%Current(tWork%Vertex(ii)%Leg(:))%Property
    lclF(0) = f%Particle(lclF(0))%Anti
    call f%Uvertex( vertexLabel(ii) ,cnstRef ,lclF ,lclX )
    list%daughter(nn)%MotherVertex(ii) = tWork%Vertex(ii)%Mother
    list%daughter(nn)%CnstRef(ii) = cnstRef
  enddo
!
! Merge identical motherVertices
  ii = list%daughter(nn)%Nvertices
  do ;ii=ii-1 ;if (ii.le.0) exit
    if (list%daughter(nn)%MotherVertex(ii).eq.list%daughter(nn)%MotherVertex(ii+1)) then
      call f%Ucombine( vertexLab         ,cnstRef                         &
                      ,vertexLabel(ii  ) ,list%daughter(nn)%CnstRef(ii  ) &
                      ,vertexLabel(ii+1) ,list%daughter(nn)%CnstRef(ii+1) )
      if (vertexLab.le.0) then
        list%daughter(nn)%Nvertices = list%daughter(nn)%Nvertices-2
        do jj=ii,list%daughter(nn)%Nvertices
          list%daughter(nn)%MotherVertex(jj) = list%daughter(nn)%MotherVertex(jj+2)
          list%daughter(nn)%CnstRef(     jj) = list%daughter(nn)%CnstRef(     jj+2)
          vertexLabel(jj) = vertexLabel(jj+2)
        enddo
      else
        list%daughter(nn)%Nvertices = list%daughter(nn)%Nvertices-1
        list%daughter(nn)%CnstRef(ii) = cnstRef 
        vertexLabel(ii) = vertexLab
        do jj=ii+1,list%daughter(nn)%Nvertices
          list%daughter(nn)%MotherVertex(jj) = list%daughter(nn)%MotherVertex(jj+1)
          list%daughter(nn)%CnstRef(     jj) = list%daughter(nn)%CnstRef(     jj+1)
          vertexLabel(jj) = vertexLabel(jj+1)
        enddo
      endif
    endif
  enddo
!
! Remove vertices with currents on the r.h.s. that are not on
! the l.h.s. of any earlier vertex. Repeat this.
  do
    isThere(0:m%Ncurrents) = .false.
    isThere(0) = .true.
    do ii=1,m%Nleaves
      isThere(m%CurrentOfLeaf(ii)) = .true.
    enddo
    do ii=1,list%daughter(nn)%Nvertices
      jj = list%daughter(nn)%MotherVertex(ii)
      isThere(m%Vertex(jj)%Leg(0)) = .true.
    enddo 
    newNvtx = 0
    do ii=1,list%daughter(nn)%Nvertices
      jj = list%daughter(nn)%MotherVertex(ii)
      if (isThere(m%Vertex(jj)%Leg(1)).and.&
          isThere(m%Vertex(jj)%Leg(2)).and.&
          isThere(m%Vertex(jj)%Leg(3)).and.&
          isThere(m%Vertex(jj)%Leg(4))     ) then
        newNvtx = newNvtx+1
        keep(newNvtx) = ii
      endif
    enddo
!
    if (newNvtx.eq.list%daughter(nn)%Nvertices) exit
!  
    list%daughter(nn)%Nvertices = newNvtx
    do ii=1,list%daughter(nn)%Nvertices
      list%daughter(nn)%MotherVertex(ii) = list%daughter(nn)%MotherVertex(keep(ii))
      list%daughter(nn)%CnstRef(     ii) = list%daughter(nn)%CnstRef(     keep(ii))
    enddo
  enddo
!
! Discard daughter if skeleton is not complete
  ii = list%daughter(nn)%Nvertices
  if (ii.le.0) then
    list%Ndaughters = list%Ndaughters-1
    call resize( list%daughter ,1,list%Ndaughters )
  elseif (list%daughter(nn)%MotherVertex(ii).lt.m%FirstRoot) then
    list%Ndaughters = list%Ndaughters-1
    call resize( list%daughter ,1,list%Ndaughters )
  endif
!
  list%added = (list%Ndaughters.gt.NdghtOld)
!
!  if (list%added) then
!    write(*,'(a27,a38,i3,a1,999i1)') &
!          'MESSAGE from add_daughter: ' &
!         ,'vertex labels for daughter with label=' &
!         ,list%daughter(list%Ndaughters)%Label,':' &
!         ,vertexLabel(1:list%daughter(list%Ndaughters)%Nvertices)
!  endif
!
  end subroutine


  subroutine trim_daughterList( obj ,remove )
  class(daughterList_type) :: obj
  logical,intent(in) :: remove(:)
  integer :: ii
  ii = obj%Ndaughters
  do 
    if (remove(ii)) then
      obj%daughter(ii:obj%Ndaughters-1) = obj%daughter(ii+1:obj%Ndaughters)
      obj%Ndaughters = obj%Ndaughters-1
    endif
    ii = ii-1
    if (ii.le.0) exit
  enddo
  call resize_d1( obj%daughter ,1,obj%Ndaughters )
  end subroutine


  subroutine print_0( t ,iunit )
  type(dsskeleton_type) ,intent(in) :: t
  integer      ,optional,intent(in) :: iunit
  integer :: nunit,ii,jj,l(0:4),p(0:4),e(0:4)
  character(72) :: line
  nunit = stdout
  if (present(iunit)) nunit = iunit
  if (nunit.le.0) return
!
  write(nunit,*)
  line = 'Process:'
  do ii=1,t%Nleaves+1
    line = trim(line)//' '//trim(prnt(t%Process(ii),'lft'))
  enddo
  write(nunit,'(A)') line
  write(nunit,*)
!
  do ii=1,t%Nvertices
    l(0:4) = t%Vertex(ii)%Leg(0:4)
    do jj=0,4
      p(jj) = t%Current(l(jj))%Mother
      e(jj) = t%Current(l(jj))%Property
    enddo
    write(nunit,'(A)') prnt(ii,6)//': ' &
            //prnt(l(0),4)//'['//prnt(p(0),3)//' '//prnt(e(0),3)//']' &
    //' <--'//prnt(l(1),4)//'['//prnt(p(1),3)//' '//prnt(e(1),3)//']' &
            //prnt(l(2),4)//'['//prnt(p(2),3)//' '//prnt(e(2),3)//']' &
            //prnt(l(3),4)//'['//prnt(p(3),3)//' '//prnt(e(3),3)//']' &
            //prnt(l(4),4)//'['//prnt(p(4),3)//' '//prnt(e(4),3)//']' &
            //prnt(t%Vertex(ii)%FermSign,2)//' '//prnt(t%Vertex(ii)%Mother,6)
  enddo
  write(nunit,*)
  end subroutine

  subroutine print_1( t ,ft ,iunit )
  type(dsskeleton_type) ,intent(in) :: t
  type(fusionRules_type),intent(in) :: ft
  integer      ,optional,intent(in) :: iunit
  include 'print_skeleton.h90'
  end subroutine

  subroutine print_2( t,ft ,o,fo  ,iunit )
  type(dsskeleton_type) ,intent(in) :: t
  type(dsskeleton_type) ,intent(in) :: o
  type(fusionRules_type),intent(in) :: ft
  type(fusionRules_type),intent(in) :: fo
  integer      ,optional,intent(in) :: iunit
  include 'print_skeleton_2.h90'
  end subroutine

  subroutine print_daughters( list ,t ,ft ,iunit )
  type(daughterList_type),intent(in) :: list
  type(dsskeleton_type)  ,intent(in) :: t
  type(fusionRules_type) ,intent(in) :: ft
  integer       ,optional,intent(in) :: iunit
  include 'print_daughters.h90'
  end subroutine


  subroutine enlarge_i2( xx ,l1,u1 ,l2,u2 )
  integer &
  include '../arrays/enlarge2.h90'
  end subroutine

  subroutine enlarge_i1( xx ,l1,u1 )
  integer &
  include '../arrays/enlarge1.h90'
  end subroutine

  subroutine enlarge_l1( xx ,l1,u1 )
  logical &
  include '../arrays/enlarge1.h90'
  end subroutine

  subroutine enlarge_v1( xx ,l1,u1 )
  type(vertex_type) &
  include '../arrays/enlarge1.h90'
  end subroutine

  subroutine enlarge_c1( xx ,l1,u1 )
  type(current_type) &
  include '../arrays/enlarge1.h90'
  end subroutine

  subroutine resize_i1( xx ,l1,u1 )
  integer &
  include '../arrays/resize1.h90'
  end subroutine

  subroutine resize_l1( xx ,l1,u1 )
  logical &
  include '../arrays/resize1.h90'
  end subroutine

  subroutine resize_d1( xx ,l1,u1 )
  type(daughter_type) &
  include '../arrays/resize1.h90'
  end subroutine

end module


