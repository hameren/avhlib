module avh_othercnst
  !(usekindmod!)
  use avh_mathcnst
  implicit none
  private
  public :: dalloc_cnst ,get_ref,realcnst ,cplxcnst

  !(realknd2!),allocatable,save,protected :: realcnst(:,:)
  !(complex2!),allocatable,save,protected :: cplxcnst(:,:)

  interface get_ref
    module procedure get_ref_r,get_ref_c
    module procedure get_ref_r1,get_ref_r2,get_ref_r3
    module procedure get_ref_c1,get_ref_c2
    module procedure get_ref_i1,get_ref_i2,get_ref_i3
  end interface

contains

  subroutine dalloc_cnst
  if (allocated(realcnst)) deallocate(realcnst)
  if (allocated(cplxcnst)) deallocate(cplxcnst)
  end subroutine

  function get_ref_r( yy ) result(rslt)
  !(realknd2!),intent(in) :: yy(:)
  include 'get_or_add_r.h90'
  end function


  function get_ref_c( yy ) result(rslt)
  !(complex2!),intent(in) :: yy(:)
  include 'get_or_add_c.h90'
!  write(*,'(a9,i4,99e16.8)') 'cplxcnst:',rslt,yy !DEBUG
  end function


  function get_ref_i1( z1 ) result(rslt)
  integer,intent(in) :: z1
  integer :: rslt
  rslt = get_ref_c( [z1*cONE] )
  end function

  function get_ref_i2( z1,z2 ) result(rslt)
  integer,intent(in) :: z1,z2
  integer :: rslt
  rslt = get_ref_c( [z1*cONE,z2*cONE] )
  end function

  function get_ref_i3( z1,z2,z3 ) result(rslt)
  integer,intent(in) :: z1,z2,z3
  integer :: rslt
  rslt = get_ref_c( [z1*cONE,z2*cONE,z3*cONE] )
  end function

  function get_ref_r1( z1 ) result(rslt)
  !(realknd2!),intent(in) :: z1
  integer :: rslt
  rslt = get_ref_c( [z1*cONE] )
  end function

  function get_ref_r2( z1,z2 ) result(rslt)
  !(realknd2!),intent(in) :: z1,z2
  integer :: rslt
  rslt = get_ref_c( [z1*cONE,z2*cONE] )
  end function

  function get_ref_r3( z1,z2,z3 ) result(rslt)
  !(realknd2!),intent(in) :: z1,z2,z3
  integer :: rslt
  rslt = get_ref_c( [z1*cONE,z2*cONE,z3*cONE] )
  end function

  function get_ref_c1( z1 ) result(rslt)
  !(complex2!),intent(in) :: z1
  integer :: rslt
  rslt = get_ref_c( [z1] )
  end function

  function get_ref_c2( z1,z2 ) result(rslt)
  !(complex2!),intent(in) :: z1,z2
  integer :: rslt
  rslt = get_ref_c( [z1,z2] )
  end function

  function get_ref_c3( z1,z2,z3 ) result(rslt)
  !(complex2!),intent(in) :: z1,z2,z3
  integer :: rslt
  rslt = get_ref_c( [z1,z2,z3] )
  end function

end module


