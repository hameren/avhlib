module avh_mathcnst
  use avh_iounits
  !(usekindmod!)
  implicit none

  !(realknd2!) :: rZRO,rONE,rHLF,r1PI,r2PI,rSQRT2
  !(complex2!) :: cZRO,cONE,cIMA

  save :: rZRO,rONE,rHLF,r1PI,r2PI,rSQRT2
  save :: cZRO,cONE,cIMA

  protected :: rZRO,rONE,rHLF,r1PI,r2PI,rSQRT2
  protected :: cZRO,cONE,cIMA

  logical,private :: initd=.false.

contains

  subroutine init_mathcnst
  if (initd) return ;initd=.true.
  call avh_hello
  rZRO = 0
  rONE = 1
  rHLF = rONE/2
  r1PI = 4*atan(rONE)
  r2PI = 2*r1PI
  rSQRT2 = sqrt(2*rONE)
  cZRO = 0
  cONE = 1
  cIMA = cmplx(0,1,kind=kind(cIMA))
  end subroutine

end module


