#!/usr/bin/env python
#begin header
import os,sys
pythonDir = ''
sys.path.append(pythonDir)
from avh import *
thisDir,packDir,avhDir = cr.dirsdown(2)
packageName = os.path.basename(thisDir)
buildDir = os.path.join(avhDir,'build')
#end header

date = '10-12-2014'

QPyes = False
for ii in range(0,len(sys.argv)):
    if sys.argv[ii]=='-QP': QPyes = True

lines = ['! From here the package "'+packageName+'", last edit: '+date+'\n']

linesDP = []
cr.addfile(os.path.join(thisDir,'avh_mathcnst.f90'),linesDP,delPattern='^.*')
cr.addfile(os.path.join(thisDir,'avh_othercnst.f90'),linesDP,delPattern='^.*')
fpp.incl(thisDir,linesDP)
fpp.xpnd('realknd2','real(kind(1d0))',linesDP)
fpp.xpnd('complex2','complex(kind(1d0))',linesDP)
lines = lines+linesDP

if QPyes:
    linesQP = []
    cr.addfile(os.path.join(thisDir,'avh_mathcnst.f90'),linesQP,delPattern='^.*')
    cr.addfile(os.path.join(thisDir,'avh_othercnst.f90'),linesQP,delPattern='^.*')
    fpp.incl(thisDir,linesQP)
    fpp.xpnd('realknd2','real(kind(1q0))',linesQP)
    fpp.xpnd('complex2','complex(kind(1q0))',linesQP)
    ed.replace_string('avh_mathcnst','avh_qp_mathcnst',linesQP)
    ed.replace_string('avh_othercnst','avh_qp_othercnst',linesQP)
    lines = lines+linesQP

cr.wfile(os.path.join(buildDir,packageName+'.f90'),lines)
