#!/usr/bin/env python
#begin header
import os,sys
pythonDir = ''
sys.path.append(pythonDir)
from avh import *
thisDir,packDir,avhDir = cr.dirsdown(2)
packageName = os.path.basename(thisDir)
buildDir = os.path.join(avhDir,'build')
#end header

date = '14-02-2015'

lines = ['! From here the package "'+packageName+'", last edit: '+date+'\n']
cr.addfile(os.path.join(thisDir,'avh_spinors.f90'),lines,delPattern="^.*")

fpp.incl(thisDir,lines)
fpp.xpnd('fltknd','kind(1d0)',lines)
fpp.xpnd('realknd2','real(fltknd)',lines)
fpp.xpnd('complex2','complex(fltknd)',lines)

cr.wfile(os.path.join(buildDir,packageName+'.f90'),lines)
