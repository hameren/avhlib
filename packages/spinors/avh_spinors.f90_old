module avh_spinors
  use avh_mathcnst

  implicit none
  private
  public :: lightlike_type ,square
  public :: operator(*),operator(+),operator(-)

  integer,parameter :: l0=0,l1=2,l2=3,l3=1

  type :: Lsqr_type
    private
    !(complex2!) :: x1,x2
  end type
  type :: Rsqr_type
    private
    !(complex2!) :: x1,x2
  end type
  type :: Lang_type
    private
    !(complex2!) :: y1,y2
  end type
  type :: Rang_type
    private
    !(complex2!) :: y1,y2
  end type
  type :: slashed_type
    private
    !(complex2!) :: y1x1,y1x2,y2x1,y2x2
    !(complex2!) :: x1y1,x2y1,x1y2,x2y2 
  end type

  type :: lightlike_type
    type(Lsqr_type) :: Lsqr
    type(Rsqr_type) :: Rsqr
    type(Lang_type) :: Lang
    type(Rang_type) :: Rang
    type(slashed_type) :: slash
  contains
    procedure :: construct_ll_r
    procedure :: construct_ll_c
    procedure :: construct_ll_2
    generic :: construct=>construct_ll_r,construct_ll_c,construct_ll_2
    procedure :: shift_sqr
    procedure :: shift_ang
  end type

  interface operator (*)
    module procedure LsqrRsqr,LangRang,RsqrLang,RangLsqr
    module procedure LsqrSlash,LangSlash,SlashRsqr,SlashRang
    module procedure c_Lsqr,c_Rsqr,c_Lang,c_Rang,c_Slash
    module procedure Lsqr_c,Rsqr_c,Lang_c,Rang_c,Slash_c
  end interface
  interface operator (+)
    module procedure SlashPlusSlash
  end interface
  interface operator (-)
    module procedure SlashMinusSlash
  end interface

contains

  subroutine construct_ll_c( obj ,p )
  class(lightlike_type) :: obj
  !(complex2!),intent(in) :: p(0:3)
  include 'construct_ll.h90'
  end subroutine

  subroutine construct_ll_r( obj ,p )
  class(lightlike_type) :: obj
  !(realknd2!),intent(in) :: p(0:3)
  include 'construct_ll.h90'
  end subroutine

  subroutine construct_ll_2( obj ,qA,qS )
  class(lightlike_type) :: obj
  type(lightlike_type),intent(in) :: qA,qS
  obj%Lang%y1 = qA%Lang%y1
  obj%Lang%y2 = qA%Lang%y2
  obj%Rang%y1 = qA%Rang%y1
  obj%Rang%y2 = qA%Rang%y2
  obj%Lsqr%x1 = qS%Lsqr%x1
  obj%Lsqr%x2 = qS%Lsqr%x2
  obj%Rsqr%x1 = qS%Rsqr%x1
  obj%Rsqr%x2 = qS%Rsqr%x2
  obj%slash = obj%Rsqr*obj%Lang
  end subroutine
  

  function shift_sqr( obj ,zz,qq ) result(rslt)
  class(lightlike_type) :: obj
  !(complex2!),intent(in) :: zz
  type(lightlike_type),intent(in):: qq
  type(lightlike_type) :: rslt
  rslt%Lsqr%x1 = obj%Lsqr%x1 + zz*qq%Lsqr%x1
  rslt%Lsqr%x2 = obj%Lsqr%x2 + zz*qq%Lsqr%x2
  rslt%Rsqr%x1 =-rslt%Lsqr%x2
  rslt%Rsqr%x2 = rslt%Lsqr%x1
  rslt%Rang%y1 = obj%Rang%y1
  rslt%Rang%y2 = obj%Rang%y2
  rslt%Lang%y1 = obj%Lang%y1
  rslt%Lang%y2 = obj%Lang%y2
  rslt%slash = rslt%Rsqr*rslt%Lang
  end function

  function shift_ang( obj ,zz,qq ) result(rslt)
  class(lightlike_type) :: obj
  !(complex2!),intent(in) :: zz
  type(lightlike_type),intent(in):: qq
  type(lightlike_type) :: rslt
  rslt%Lang%y1 = obj%Lang%y1 + zz*qq%Lang%y1
  rslt%Lang%y2 = obj%Lang%y2 + zz*qq%Lang%y2
  rslt%Rang%y1 =-rslt%Lang%y2
  rslt%Rang%y2 = rslt%Lang%y1
  rslt%Lsqr%x1 = obj%Lsqr%x1
  rslt%Lsqr%x2 = obj%Lsqr%x2
  rslt%Rsqr%x1 = obj%Rsqr%x1
  rslt%Rsqr%x2 = obj%Rsqr%x2
  rslt%slash = rslt%Rsqr*rslt%Lang
  end function



  function LsqrRsqr( a,b ) result(c)
  type(Lsqr_type),intent(in) :: a
  type(Rsqr_type),intent(in) :: b
  !(complex2!) :: c
  c = a%x1*b%x1 + a%x2*b%x2
  end function

  function LangRang( a,b ) result(c)
  type(Lang_type),intent(in) :: a
  type(Rang_type),intent(in) :: b
  !(complex2!) :: c
  c = a%y1*b%y1 + a%y2*b%y2
  end function

  function RsqrLang( a,b ) result(c)
  type(Rsqr_type),intent(in) :: a
  type(Lang_type),intent(in) :: b
  type(slashed_type) :: c
  c%x1y1 = a%x1*b%y1
  c%x1y2 = a%x1*b%y2
  c%x2y1 = a%x2*b%y1
  c%x2y2 = a%x2*b%y2
  c%y1x1 = c%x2y2
  c%y1x2 =-c%x1y2
  c%y2x1 =-c%x2y1
  c%y2x2 = c%x1y1
  end function

  function RangLsqr( a,b ) result(c)
  type(Lang_type),intent(in) :: a
  type(Rsqr_type),intent(in) :: b
  type(slashed_type) :: c
  c%y1x1 = a%y1*b%x1
  c%y1x2 = a%y1*b%x2
  c%y2x1 = a%y2*b%x1
  c%y2x2 = a%y2*b%x2
  c%x1y1 = c%y2x2
  c%x1y2 =-c%y1x2
  c%x2y1 =-c%y2x1
  c%x2y2 = c%y1x1
  end function

  function LsqrSlash( a,b ) result(c)
  type(Lsqr_type),intent(in) :: a
  type(slashed_type),intent(in) :: b
  type(Lang_type) :: c
  c%y1 = a%x1*b%x1y1 + a%x2*b%x2y1
  c%y2 = a%x1*b%x1y2 + a%x2*b%x2y2
  end function

  function LangSlash( a,b ) result(c)
  type(Lang_type),intent(in) :: a
  type(slashed_type),intent(in) :: b
  type(Lsqr_type) :: c
  c%x1 = a%y1*b%y1x1 + a%y2*b%y2x1
  c%x2 = a%y1*b%y1x2 + a%y2*b%y2x2
  end function

  function SlashRsqr( a,b ) result(c)
  type(slashed_type),intent(in) :: a
  type(Rsqr_type),intent(in) :: b
  type(Rang_type) :: c
  c%y1 = a%y1x1*b%x1 + a%y1x2*b%x2
  c%y2 = a%y2x1*b%x1 + a%y2x2*b%x2
  end function

  function SlashRang( a,b ) result(c)
  type(slashed_type),intent(in) :: a
  type(Rang_type),intent(in) :: b
  type(Rsqr_type) :: c
  c%x1 = a%x1y1*b%y1 + a%x1y2*b%y2
  c%x2 = a%x2y1*b%y1 + a%x2y2*b%y2
  end function

  function SlashPlusSlash( a,b ) result(c)
  type(slashed_type),intent(in) :: a,b
  type(slashed_type) :: c
  c%x1y1 = a%x1y1+b%x1y1
  c%x1y2 = a%x1y2+b%x1y2
  c%x2y1 = a%x2y1+b%x2y1
  c%x2y2 = a%x2y2+b%x2y2
  c%y1x1 = c%x2y2
  c%y1x2 =-c%x1y2
  c%y2x1 =-c%x2y1
  c%y2x2 = c%x1y1
  end function
  
  function SlashMinusSlash( a,b ) result(c)
  type(slashed_type),intent(in) :: a,b
  type(slashed_type) :: c
  c%x1y1 = a%x1y1-b%x1y1
  c%x1y2 = a%x1y2-b%x1y2
  c%x2y1 = a%x2y1-b%x2y1
  c%x2y2 = a%x2y2-b%x2y2
  c%y1x1 = c%x2y2
  c%y1x2 =-c%x1y2
  c%y2x1 =-c%x2y1
  c%y2x2 = c%x1y1
  end function
  
  function c_Slash( a,b ) result(c)
  !(complex2!),intent(in) :: a
  type(slashed_type),intent(in) :: b
  type(slashed_type) :: c
  c%x1y1 = a*b%x1y1
  c%x1y2 = a*b%x1y2
  c%x2y1 = a*b%x2y1
  c%x2y2 = a*b%x2y2
  c%y1x1 = c%x2y2
  c%y1x2 =-c%x1y2
  c%y2x1 =-c%x2y1
  c%y2x2 = c%x1y1
  end function
  
  function Slash_c( b,a ) result(c)
  !(complex2!),intent(in) :: a
  type(slashed_type),intent(in) :: b
  type(slashed_type) :: c
  c%x1y1 = a*b%x1y1
  c%x1y2 = a*b%x1y2
  c%x2y1 = a*b%x2y1
  c%x2y2 = a*b%x2y2
  c%y1x1 = c%x2y2
  c%y1x2 =-c%x1y2
  c%y2x1 =-c%x2y1
  c%y2x2 = c%x1y1
  end function
  
  function c_Lsqr( a,b ) result(c)
  !(complex2!),intent(in) :: a
  type(Lsqr_type),intent(in) :: b
  type(Lsqr_type) :: c
  c%x1 = a*b%x1
  c%x2 = a*b%x2
  end function
  function Lsqr_c( b,a ) result(c)
  !(complex2!),intent(in) :: a
  type(Lsqr_type),intent(in) :: b
  type(Lsqr_type) :: c
  c%x1 = a*b%x1
  c%x2 = a*b%x2
  end function
  
  function c_Rsqr( a,b ) result(c)
  !(complex2!),intent(in) :: a
  type(Rsqr_type),intent(in) :: b
  type(Rsqr_type) :: c
  c%x1 = a*b%x1
  c%x2 = a*b%x2
  end function
  function Rsqr_c( b,a ) result(c)
  !(complex2!),intent(in) :: a
  type(Rsqr_type),intent(in) :: b
  type(Rsqr_type) :: c
  c%x1 = a*b%x1
  c%x2 = a*b%x2
  end function
  
  function c_Lang( a,b ) result(c)
  !(complex2!),intent(in) :: a
  type(Lang_type),intent(in) :: b
  type(Lang_type) :: c
  c%y1 = a*b%y1
  c%y2 = a*b%y2
  end function
  function Lang_c( b,a ) result(c)
  !(complex2!),intent(in) :: a
  type(Lang_type),intent(in) :: b
  type(Lang_type) :: c
  c%y1 = a*b%y1
  c%y2 = a*b%y2
  end function
  
  function c_Rang( a,b ) result(c)
  !(complex2!),intent(in) :: a
  type(Rang_type),intent(in) :: b
  type(Rang_type) :: c
  c%y1 = a*b%y1
  c%y2 = a*b%y2
  end function
  function Rang_c( b,a ) result(c)
  !(complex2!),intent(in) :: a
  type(Rang_type),intent(in) :: b
  type(Rang_type) :: c
  c%y1 = a*b%y1
  c%y2 = a*b%y2
  end function


  function square( list ) result(rslt)
  type(lightlike_type) :: list(:)
  !(complex2!) :: rslt ,aa,bb,cc,dd
  integer :: ii
  aa = 0
  bb = 0
  cc = 0
  dd = 0
  do ii=1,size(list)
    aa = aa + list(ii)%Slash%y1x1
    bb = bb + list(ii)%Slash%y2x2
    cc = cc + list(ii)%Slash%y1x2
    dd = dd + list(ii)%Slash%y2x1
  enddo
  rslt = aa*bb - cc*dd
  end function
  

end module


