module avh_spinors
  use avh_iounits

  implicit none
  private
  public :: fltknd,imag,init_spinors
  public :: lightlike_type ,slashed_type ,square ,twodot
  public :: lightlike_list_type
  public :: operator(*),operator(+),operator(-)

  integer,parameter :: fltknd=!(fltknd!)

  integer,parameter :: l0=0,l1=2,l2=3,l3=1

  logical,save :: initd=.false.
  !(complex2!),save,protected :: imag

  type :: Lsqr_type
    private
    !(complex2!) :: x1,x2
  end type
  type :: Rsqr_type
    private
    !(complex2!) :: x1,x2
  end type
  type :: Lang_type
    private
    !(complex2!) :: y1,y2
  end type
  type :: Rang_type
    private
    !(complex2!) :: y1,y2
  end type

  type :: slashed_type
    private
    !(complex2!) :: y1x1,y1x2,y2x1,y2x2
    !(complex2!) :: x1y1,x2y1,x1y2,x2y2 
  contains
    procedure :: construct=>construct_slashed
  end type

  type :: lightlike_type
    type(Lsqr_type) :: Lsqr
    type(Rsqr_type) :: Rsqr
    type(Lang_type) :: Lang
    type(Rang_type) :: Rang
    type(slashed_type) :: slash
  contains
    procedure :: construct_ll_r
    procedure :: construct_ll_c
    procedure :: construct_ll_2
    procedure :: construct_ll_slash
    generic :: construct=>construct_ll_r,construct_ll_c &
                         ,construct_ll_2,construct_ll_slash
    procedure :: shift_sqr
    procedure :: shift_ang
    procedure :: momentum
    procedure :: minus
    procedure :: print=>print_ll
  end type

  type :: lightlike_list_type
    type(lightlike_type),allocatable :: p(:)
  contains
    procedure :: alloc=>lll_alloc
    procedure :: construct=>lll_construct
    procedure :: lll_ang_ii
    procedure :: lll_ang_isi
    procedure :: lll_ang_issi
    procedure :: lll_sqr_ii
    procedure :: lll_sqr_isi
    procedure :: lll_sqr_issi
    generic :: ang=>lll_ang_ii,lll_ang_isi,lll_ang_issi
    generic :: sqr=>lll_sqr_ii,lll_sqr_isi,lll_sqr_issi
  end type

  interface operator (*)
    module procedure LsqrRsqr,LangRang,RsqrLang,RangLsqr
    module procedure LsqrSlash,LangSlash,SlashRsqr,SlashRang
    module procedure c_Lsqr,c_Rsqr,c_Lang,c_Rang,c_Slash
    module procedure Lsqr_c,Rsqr_c,Lang_c,Rang_c,Slash_c
    module procedure lightlike_c,c_lightlike
  end interface
  interface operator (+)
    module procedure plus_sl_sl,plus_sl_ll,plus_ll_sl,plus_ll_ll
  end interface
  interface operator (-)
    module procedure minus_sl_sl,minus_sl_ll,minus_ll_sl,minus_ll_ll
  end interface

  interface square
    module procedure square_ll,square_sl
  end interface

  interface twodot
    module procedure dot2_sl_sl,dot2_sl_ll,dot2_ll_sl,dot2_ll_ll
  end interface

  type(slashed_type),save,protected :: zeroSlash

contains


  subroutine init_spinors
  if (initd) return ;initd=.true.
  call avh_hello
  imag = cmplx(0,1,kind=kind(imag))
  call zeroSlash%construct( [0._fltknd,0._fltknd,0._fltknd,0._fltknd] )
  end subroutine


  subroutine print_ll( obj ,iunit )
  class(lightlike_type) :: obj
  integer,intent(in) :: iunit
  write(iunit,*) 'slash%y1x1',obj%slash%y1x1
  write(iunit,*) 'slash%y1x2',obj%slash%y1x2
  write(iunit,*) 'slash%y2x1',obj%slash%y2x1
  write(iunit,*) 'slash%y2x2',obj%slash%y2x2
  write(iunit,*) 'slash%x1y1',obj%slash%x1y1
  write(iunit,*) 'slash%x1y2',obj%slash%x1y2
  write(iunit,*) 'slash%x2y1',obj%slash%x2y1
  write(iunit,*) 'slash%x2y2',obj%slash%x2y2
  write(iunit,*) 'Lsqr%x1',obj%Lsqr%x1
  write(iunit,*) 'Lsqr%x2',obj%Lsqr%x2
  write(iunit,*) 'Rsqr%x1',obj%Rsqr%x1
  write(iunit,*) 'Rsqr%x2',obj%Rsqr%x2
  write(iunit,*) 'Rang%y1',obj%Rang%y1
  write(iunit,*) 'Rang%y2',obj%Rang%y2
  write(iunit,*) 'Lang%y1',obj%Lang%y1
  write(iunit,*) 'Lang%y2',obj%Lang%y2
  end subroutine


  subroutine construct_slashed( obj ,p )
  class(slashed_type) :: obj
  !(realknd2!),intent(in) :: p(0:3)
  !(complex2!) :: p2ima
  p2ima = p(l2)*imag
  obj%y1x1 = p(l0)+p(l3)
  obj%y1x2 = p(l1)-p2ima
  obj%y2x1 = p(l1)+p2ima
  obj%y2x2 = p(l0)-p(l3)
  obj%x1y1 = obj%y2x2
  obj%x1y2 =-obj%y1x2
  obj%x2y1 =-obj%y2x1
  obj%x2y2 = obj%y1x1
  end subroutine
  

  subroutine construct_ll_c( obj ,p )
  class(lightlike_type) :: obj
  !(complex2!),intent(in) :: p(0:3)
  include 'construct_ll.h90'
  end subroutine

  subroutine construct_ll_r( obj ,p )
  class(lightlike_type) :: obj
  !(realknd2!),intent(in) :: p(0:3)
  include 'construct_ll.h90'
  end subroutine

  subroutine construct_ll_2( obj ,qA,qS )
  class(lightlike_type) :: obj
  type(lightlike_type),intent(in) :: qA,qS
  obj%Lang = qA%Lang
  obj%Rang = qA%Rang
  obj%Lsqr = qS%Lsqr
  obj%Rsqr = qS%Rsqr
  obj%slash = obj%Rsqr*obj%Lang
  end subroutine

  subroutine construct_ll_slash( obj ,qSlash )
  class(lightlike_type) :: obj
  type(slashed_type),intent(in) :: qSlash
  !(complex2!) :: hh
  obj%slash = qSlash
  hh = sqrt(abs(obj%slash%y1x1))
  obj%Lsqr%x1 = obj%slash%y1x1/hh
  obj%Lsqr%x2 = obj%slash%y1x2/hh
  obj%Rsqr%x1 =-obj%Lsqr%x2
  obj%Rsqr%x2 = obj%Lsqr%x1
  obj%Rang%y1 = hh
  obj%Rang%y2 = obj%slash%y2x1*hh/obj%slash%y1x1
  obj%Lang%y1 =-obj%Rang%y2
  obj%Lang%y2 = hh
  end subroutine
  

  function shift_sqr( obj ,zz,qq ) result(rslt)
  class(lightlike_type) :: obj
  !(complex2!),intent(in) :: zz
  type(lightlike_type),intent(in):: qq
  type(lightlike_type) :: rslt
  !(complex2!) :: hh
  rslt%Lsqr%x1 = obj%Lsqr%x1 + zz*qq%Lsqr%x1
  rslt%Lsqr%x2 = obj%Lsqr%x2 + zz*qq%Lsqr%x2
  rslt%Rsqr%x1 =-rslt%Lsqr%x2
  rslt%Rsqr%x2 = rslt%Lsqr%x1
  rslt%Rang%y1 = obj%Rang%y1
  rslt%Rang%y2 = obj%Rang%y2
  rslt%Lang%y1 = obj%Lang%y1
  rslt%Lang%y2 = obj%Lang%y2
  rslt%slash = rslt%Rsqr*rslt%Lang
!
!  hh = sqrt(abs(rslt%slash%y1x1))
!  rslt%Lsqr%x1 = rslt%slash%y1x1/hh
!  rslt%Lsqr%x2 = rslt%slash%y1x2/hh
!  rslt%Rsqr%x1 =-rslt%Lsqr%x2
!  rslt%Rsqr%x2 = rslt%Lsqr%x1
!  rslt%Rang%y1 = hh
!  rslt%Rang%y2 = rslt%slash%y2x1*hh/rslt%slash%y1x1
!  rslt%Lang%y1 =-rslt%Rang%y2
!  rslt%Lang%y2 = hh
  end function

  function shift_ang( obj ,zz,qq ) result(rslt)
  class(lightlike_type) :: obj
  !(complex2!),intent(in) :: zz
  type(lightlike_type),intent(in):: qq
  type(lightlike_type) :: rslt
  !(complex2!) :: hh
  rslt%Rang%y1 = obj%Rang%y1 + zz*qq%Rang%y1
  rslt%Rang%y2 = obj%Rang%y2 + zz*qq%Rang%y2
  rslt%Lang%y1 =-rslt%Rang%y2
  rslt%Lang%y2 = rslt%Rang%y1
  rslt%Lsqr%x1 = obj%Lsqr%x1
  rslt%Lsqr%x2 = obj%Lsqr%x2
  rslt%Rsqr%x1 = obj%Rsqr%x1
  rslt%Rsqr%x2 = obj%Rsqr%x2
  rslt%slash = rslt%Rsqr*rslt%Lang
!
!  hh = sqrt(abs(rslt%slash%y1x1))
!  rslt%Lsqr%x1 = rslt%slash%y1x1/hh
!  rslt%Lsqr%x2 = rslt%slash%y1x2/hh
!  rslt%Rsqr%x1 =-rslt%Lsqr%x2
!  rslt%Rsqr%x2 = rslt%Lsqr%x1
!  rslt%Rang%y1 = hh
!  rslt%Rang%y2 = rslt%slash%y2x1*hh/rslt%slash%y1x1
!  rslt%Lang%y1 =-rslt%Rang%y2
!  rslt%Lang%y2 = hh
  end function

  function minus( obj ) result(qq)
  class(lightlike_type) :: obj
  type(lightlike_type) :: qq
  qq%slash%y1x1 =-obj%slash%y1x1
  qq%slash%y1x2 =-obj%slash%y1x2
  qq%slash%y2x1 =-obj%slash%y2x1
  qq%slash%y2x2 =-obj%slash%y2x2
  qq%slash%x1y1 =-obj%slash%x1y1
  qq%slash%x1y2 =-obj%slash%x1y2
  qq%slash%x2y1 =-obj%slash%x2y1
  qq%slash%x2y2 =-obj%slash%x2y2
  qq%Lsqr%x1 =-obj%Lsqr%x1
  qq%Lsqr%x2 =-obj%Lsqr%x2
  qq%Rsqr%x1 =-obj%Rsqr%x1
  qq%Rsqr%x2 =-obj%Rsqr%x2
  qq%Rang%y1 = obj%Rang%y1
  qq%Rang%y2 = obj%Rang%y2
  qq%Lang%y1 = obj%Lang%y1
  qq%Lang%y2 = obj%Lang%y2
  end function
  

  function LsqrRsqr( a,b ) result(c)
  type(Lsqr_type),intent(in) :: a
  type(Rsqr_type),intent(in) :: b
  !(complex2!) :: c
  c = a%x1*b%x1 + a%x2*b%x2
  end function

  function LangRang( a,b ) result(c)
  type(Lang_type),intent(in) :: a
  type(Rang_type),intent(in) :: b
  !(complex2!) :: c
  c = a%y1*b%y1 + a%y2*b%y2
  end function

  function RsqrLang( a,b ) result(c)
  type(Rsqr_type),intent(in) :: a
  type(Lang_type),intent(in) :: b
  type(slashed_type) :: c
  c%x1y1 = a%x1*b%y1
  c%x1y2 = a%x1*b%y2
  c%x2y1 = a%x2*b%y1
  c%x2y2 = a%x2*b%y2
  c%y1x1 = c%x2y2
  c%y1x2 =-c%x1y2
  c%y2x1 =-c%x2y1
  c%y2x2 = c%x1y1
  end function

  function RangLsqr( a,b ) result(c)
  type(Lang_type),intent(in) :: a
  type(Rsqr_type),intent(in) :: b
  type(slashed_type) :: c
  c%y1x1 = a%y1*b%x1
  c%y1x2 = a%y1*b%x2
  c%y2x1 = a%y2*b%x1
  c%y2x2 = a%y2*b%x2
  c%x1y1 = c%y2x2
  c%x1y2 =-c%y1x2
  c%x2y1 =-c%y2x1
  c%x2y2 = c%y1x1
  end function

  function LsqrSlash( a,b ) result(c)
  type(Lsqr_type),intent(in) :: a
  type(slashed_type),intent(in) :: b
  type(Lang_type) :: c
  c%y1 = a%x1*b%x1y1 + a%x2*b%x2y1
  c%y2 = a%x1*b%x1y2 + a%x2*b%x2y2
  end function

  function LangSlash( a,b ) result(c)
  type(Lang_type),intent(in) :: a
  type(slashed_type),intent(in) :: b
  type(Lsqr_type) :: c
  c%x1 = a%y1*b%y1x1 + a%y2*b%y2x1
  c%x2 = a%y1*b%y1x2 + a%y2*b%y2x2
  end function

  function SlashRsqr( a,b ) result(c)
  type(slashed_type),intent(in) :: a
  type(Rsqr_type),intent(in) :: b
  type(Rang_type) :: c
  c%y1 = a%y1x1*b%x1 + a%y1x2*b%x2
  c%y2 = a%y2x1*b%x1 + a%y2x2*b%x2
  end function

  function SlashRang( a,b ) result(c)
  type(slashed_type),intent(in) :: a
  type(Rang_type),intent(in) :: b
  type(Rsqr_type) :: c
  c%x1 = a%x1y1*b%y1 + a%x1y2*b%y2
  c%x2 = a%x2y1*b%y1 + a%x2y2*b%y2
  end function

  function plus_sl_sl( a,b ) result(c)
  type(slashed_type),intent(in) :: a,b
  type(slashed_type) :: c
  c%x1y1 = a%x1y1+b%x1y1
  c%x1y2 = a%x1y2+b%x1y2
  c%x2y1 = a%x2y1+b%x2y1
  c%x2y2 = a%x2y2+b%x2y2
  c%y1x1 = c%x2y2
  c%y1x2 =-c%x1y2
  c%y2x1 =-c%x2y1
  c%y2x2 = c%x1y1
  end function
  
  function minus_sl_sl( a,b ) result(c)
  type(slashed_type),intent(in) :: a,b
  type(slashed_type) :: c
  c%x1y1 = a%x1y1-b%x1y1
  c%x1y2 = a%x1y2-b%x1y2
  c%x2y1 = a%x2y1-b%x2y1
  c%x2y2 = a%x2y2-b%x2y2
  c%y1x1 = c%x2y2
  c%y1x2 =-c%x1y2
  c%y2x1 =-c%x2y1
  c%y2x2 = c%x1y1
  end function
  
  function plus_ll_ll( a,b ) result(c)
  type(lightlike_type),intent(in) :: a,b
  type(slashed_type) :: c
  c = plus_sl_sl( a%slash ,b%slash )
  end function
  
  function plus_ll_sl( a,b ) result(c)
  type(lightlike_type),intent(in) :: a
  type(slashed_type),intent(in) :: b
  type(slashed_type) :: c
  c = plus_sl_sl( a%slash ,b )
  end function
  
  function plus_sl_ll( a,b ) result(c)
  type(slashed_type),intent(in) :: a
  type(lightlike_type),intent(in) :: b
  type(slashed_type) :: c
  c = plus_sl_sl( a ,b%slash )
  end function
  
  function minus_ll_ll( a,b ) result(c)
  type(lightlike_type),intent(in) :: a,b
  type(slashed_type) :: c
  c = minus_sl_sl( a%slash ,b%slash )
  end function
  
  function minus_ll_sl( a,b ) result(c)
  type(lightlike_type),intent(in) :: a
  type(slashed_type),intent(in) :: b
  type(slashed_type) :: c
  c = minus_sl_sl( a%slash ,b )
  end function
  
  function minus_sl_ll( a,b ) result(c)
  type(slashed_type),intent(in) :: a
  type(lightlike_type),intent(in) :: b
  type(slashed_type) :: c
  c = minus_sl_sl( a ,b%slash )
  end function
  
  
  function c_Slash( a,b ) result(c)
  !(complex2!),intent(in) :: a
  type(slashed_type),intent(in) :: b
  type(slashed_type) :: c
  c%x1y1 = a*b%x1y1
  c%x1y2 = a*b%x1y2
  c%x2y1 = a*b%x2y1
  c%x2y2 = a*b%x2y2
  c%y1x1 = c%x2y2
  c%y1x2 =-c%x1y2
  c%y2x1 =-c%x2y1
  c%y2x2 = c%x1y1
  end function
  
  function Slash_c( b,a ) result(c)
  !(complex2!),intent(in) :: a
  type(slashed_type),intent(in) :: b
  type(slashed_type) :: c
  c%x1y1 = a*b%x1y1
  c%x1y2 = a*b%x1y2
  c%x2y1 = a*b%x2y1
  c%x2y2 = a*b%x2y2
  c%y1x1 = c%x2y2
  c%y1x2 =-c%x1y2
  c%y2x1 =-c%x2y1
  c%y2x2 = c%x1y1
  end function
  
  function c_lightlike( a,b ) result(c)
  !(complex2!),intent(in) :: a
  type(lightlike_type),intent(in) :: b
  type(slashed_type) :: c
  c%x1y1 = a*b%slash%x1y1
  c%x1y2 = a*b%slash%x1y2
  c%x2y1 = a*b%slash%x2y1
  c%x2y2 = a*b%slash%x2y2
  c%y1x1 = c%x2y2
  c%y1x2 =-c%x1y2
  c%y2x1 =-c%x2y1
  c%y2x2 = c%x1y1
  end function
  
  function lightlike_c( b,a ) result(c)
  !(complex2!),intent(in) :: a
  type(lightlike_type),intent(in) :: b
  type(slashed_type) :: c
  c%x1y1 = a*b%slash%x1y1
  c%x1y2 = a*b%slash%x1y2
  c%x2y1 = a*b%slash%x2y1
  c%x2y2 = a*b%slash%x2y2
  c%y1x1 = c%x2y2
  c%y1x2 =-c%x1y2
  c%y2x1 =-c%x2y1
  c%y2x2 = c%x1y1
  end function
  
  function c_Lsqr( a,b ) result(c)
  !(complex2!),intent(in) :: a
  type(Lsqr_type),intent(in) :: b
  type(Lsqr_type) :: c
  c%x1 = a*b%x1
  c%x2 = a*b%x2
  end function
  function Lsqr_c( b,a ) result(c)
  !(complex2!),intent(in) :: a
  type(Lsqr_type),intent(in) :: b
  type(Lsqr_type) :: c
  c%x1 = a*b%x1
  c%x2 = a*b%x2
  end function
  
  function c_Rsqr( a,b ) result(c)
  !(complex2!),intent(in) :: a
  type(Rsqr_type),intent(in) :: b
  type(Rsqr_type) :: c
  c%x1 = a*b%x1
  c%x2 = a*b%x2
  end function
  function Rsqr_c( b,a ) result(c)
  !(complex2!),intent(in) :: a
  type(Rsqr_type),intent(in) :: b
  type(Rsqr_type) :: c
  c%x1 = a*b%x1
  c%x2 = a*b%x2
  end function
  
  function c_Lang( a,b ) result(c)
  !(complex2!),intent(in) :: a
  type(Lang_type),intent(in) :: b
  type(Lang_type) :: c
  c%y1 = a*b%y1
  c%y2 = a*b%y2
  end function
  function Lang_c( b,a ) result(c)
  !(complex2!),intent(in) :: a
  type(Lang_type),intent(in) :: b
  type(Lang_type) :: c
  c%y1 = a*b%y1
  c%y2 = a*b%y2
  end function
  
  function c_Rang( a,b ) result(c)
  !(complex2!),intent(in) :: a
  type(Rang_type),intent(in) :: b
  type(Rang_type) :: c
  c%y1 = a*b%y1
  c%y2 = a*b%y2
  end function
  function Rang_c( b,a ) result(c)
  !(complex2!),intent(in) :: a
  type(Rang_type),intent(in) :: b
  type(Rang_type) :: c
  c%y1 = a*b%y1
  c%y2 = a*b%y2
  end function


  function momentum( obj ) result(rslt)
  class(lightlike_type) :: obj
  !(complex2!) :: rslt(0:3)
  rslt(l0) = ( obj%slash%y1x1 + obj%slash%y2x2 )/2
  rslt(l3) = ( obj%slash%y1x1 - obj%slash%y2x2 )/2
  rslt(l1) = ( obj%slash%y1x2 + obj%slash%y2x1 )/2
  rslt(l2) = ( obj%slash%y1x2 - obj%slash%y2x1 )/2*imag
  end function


  function square_ll( list ) result(rslt)
  type(lightlike_type) :: list(:)
  !(complex2!) :: rslt ,aa,bb,cc,dd
  integer :: ii
  aa = 0
  bb = 0
  cc = 0
  dd = 0
  do ii=1,size(list)
    aa = aa + list(ii)%Slash%y1x1
    bb = bb + list(ii)%Slash%y2x2
    cc = cc + list(ii)%Slash%y1x2
    dd = dd + list(ii)%Slash%y2x1
  enddo
  rslt = aa*bb - cc*dd
  end function

  function square_sl( a ) result(rslt)
  type(slashed_type),intent(in) :: a
  !(complex2!) :: rslt
  rslt = a%y1x1*a%y2x2 - a%y1x2*a%y2x1
  end function


  function dot2_sl_sl( a,b ) result(c)
  type(slashed_type),intent(in) :: a
  type(slashed_type),intent(in) :: b
  !(complex2!) :: c
  c = a%y1x1*b%y2x2 + b%y1x1*a%y2x2 &
    - a%y1x2*b%y2x1 - b%y1x2*a%y2x1
  end function
  
  function dot2_sl_ll( a,b ) result(c)
  type(slashed_type),intent(in) :: a
  type(lightlike_type),intent(in) :: b
  !(complex2!) :: c
  c = a%y1x1*b%slash%y2x2 + b%slash%y1x1*a%y2x2 &
    - a%y1x2*b%slash%y2x1 - b%slash%y1x2*a%y2x1
  end function
  
  function dot2_ll_sl( a,b ) result(c)
  type(slashed_type),intent(in) :: b
  type(lightlike_type),intent(in) :: a
  !(complex2!) :: c
  c = b%y1x1*a%slash%y2x2 + a%slash%y1x1*b%y2x2 &
    - b%y1x2*a%slash%y2x1 - a%slash%y1x2*b%y2x1
  end function
  
  function dot2_ll_ll( a,b ) result(c)
  type(lightlike_type),intent(in) :: a
  type(lightlike_type),intent(in) :: b
  !(complex2!) :: c
  c = a%slash%y1x1*b%slash%y2x2 + b%slash%y1x1*a%slash%y2x2 &
    - a%slash%y1x2*b%slash%y2x1 - b%slash%y1x2*a%slash%y2x1
  end function


  subroutine lll_alloc( obj ,n0,n1 )
  class(lightlike_list_type) :: obj
  integer,intent(in) :: n0,n1
  logical :: firstcall=.true.
  if (.not.initd) then
    if (errru.ge.0) write(*,*) 'ERROR in avh_spinors: ' &
      ,'you must call init_spinors'
    stop
  endif
  if (allocated(obj%p)) deallocate(obj%p)
  allocate(obj%p(n0:n1))
  end subroutine

  subroutine lll_construct( obj ,ii ,pp )
  class(lightlike_list_type) :: obj
  integer,intent(in) :: ii
  !(realknd2!),intent(in) :: pp(0:3)
  call obj%p(ii)%construct(pp)
  end subroutine

  function lll_ang_ii( obj ,i1,i2 ) result(rslt)
  class(lightlike_list_type) :: obj
  integer,intent(in) :: i1,i2
  !(complex2!) :: rslt
  rslt = obj%p(i1)%Lang*obj%p(i2)%Rang
  end function
  
  function lll_sqr_ii( obj ,i1,i2 ) result(rslt)
  class(lightlike_list_type) :: obj
  integer,intent(in) :: i1,i2
  !(complex2!) :: rslt
  rslt = obj%p(i1)%Lsqr*obj%p(i2)%Rsqr
  end function
  
  function lll_ang_isi( obj ,i1 ,kk ,i2 ) result(rslt)
  class(lightlike_list_type) :: obj
  type(slashed_type),intent(in) :: kk
  integer,intent(in) :: i1,i2
  !(complex2!) :: rslt
  rslt = obj%p(i1)%Lang*kk*obj%p(i2)%Rsqr
  end function
  
  function lll_sqr_isi( obj ,i1 ,kk ,i2 ) result(rslt)
  class(lightlike_list_type) :: obj
  type(slashed_type),intent(in) :: kk
  integer,intent(in) :: i1,i2
  !(complex2!) :: rslt
  rslt = obj%p(i1)%Lsqr*kk*obj%p(i2)%Rang
  end function
  
  function lll_ang_issi( obj ,i1,ka,kb,i2 ) result(rslt)
  class(lightlike_list_type) :: obj
  type(slashed_type),intent(in) :: ka,kb
  integer,intent(in) :: i1,i2
  !(complex2!) :: rslt
  rslt = obj%p(i1)%Lang*ka*kb*obj%p(i2)%Rang
  end function
  
  function lll_sqr_issi( obj ,i1,ka,kb,i2 ) result(rslt)
  class(lightlike_list_type) :: obj
  type(slashed_type),intent(in) :: ka,kb
  integer,intent(in) :: i1,i2
  !(complex2!) :: rslt
  rslt = obj%p(i1)%Lsqr*ka*kb*obj%p(i2)%Rsqr
  end function
  


end module


