module avh_ranvar
  !(usekindmod!)
  use avh_mathcnst
  implicit none
  private

  public :: init_ranvar
  public :: betaLike_type
  public :: ranvar03_gnrt

  type :: ranvar01_type
    private
    !(realknd2!) :: lam,x0,wl,wr,pl
  contains
    procedure :: init=>ranvar01_init
    procedure :: gnrt=>ranvar01_gnrt
  end type

  type :: betaLike_type
    private
    !(realknd2!) :: a,b,c,u
  contains
    procedure :: init=>betaLike_init
    procedure :: betaLike_gnrt
    procedure :: betaLike_gnrt_w
    generic :: gnrt=>betaLike_gnrt,betaLike_gnrt_w
    procedure :: betaLike_wght
    procedure :: betaLike_wght_r
    generic :: wght=>betaLike_wght,betaLike_wght_r
  end type

  !(realknd2!),private,save :: rC02

  logical,private :: initd=.false.

  interface ranvar03_gnrt
    module procedure ranvar03r_gnrt,ranvar03i_gnrt
  end interface


contains


  subroutine init_ranvar
  if (initd) return ;initd=.true.
  call init_mathcnst
  rC02 = 1+1/log(2*rONE)
  end subroutine


  subroutine ranvar01_init( obj ,lam,x0 )
!************************************************************************
! Generate x between 0 and 1 following the normalized and continuous
! version of the density
!*
!*          /  x^(-lam)  for   0 < x < x0
!*  f(x) = <
!*          \  x^(-1)    for  x0 < x < 1
!*
!************************************************************************
  class(ranvar01_type),intent(out) :: obj
  intent(in ) :: lam,x0
  !(realknd2!) :: lam,x0
  obj%lam = lam
  obj%x0  = x0
  obj%wr = (1-lam)/(1-(1-lam)*log(x0)) ! x^(-lam)
  obj%wl = obj%wr/x0**(1-lam)          ! x^(-1)
  obj%pl = obj%wl*x0**(1-lam)/(1-lam)
  end subroutine

  subroutine ranvar01_gnrt( obj ,xx,ww ,rho )
  class(ranvar01_type),intent(in) :: obj
  intent(in ) :: rho
  intent(out) :: xx,ww
  !(realknd2!) :: ww,xx,rho,hh
  if (rho.le.obj%pl) then
    hh = (1-obj%lam)*rho/obj%wl
    ww = hh**( obj%lam/(1-obj%lam) )
    xx = hh*ww
    ww = ww/obj%wl
!    xx = ((1-obj%lam)*rho/obj%wl)**(1/(1-obj%lam))
!    ww = xx**obj%lam/obj%wl
  else
    xx = obj%x0*exp((rho-obj%pl)/obj%wr)
    ww = xx/obj%wr
  endif
  end subroutine


  subroutine ranvar02_gnrt( xx,ww ,rho )
!***********************************************************************
! x*exp(-x)-like density on [0:infty), such that
!   1.05*f(x) > x*exp(-x)  forall x>0
!***********************************************************************
  intent(out) :: xx,ww
  intent(in ) :: rho
  !(realknd2!) :: xx,ww,rho,h1,h2,h3
  h1 = 1/rho      ! 1/rho
  h2 = h1-1       ! 1/rho-1
  h3 = 1
  h3 = h2**(h3/3) ! (1/rho-1)**(1/3)
  xx = rC02*log( 1 + h3*h3 )
  ww = 2*rC02*h1*h1/((h2+h3)*3)
  end subroutine

!  function ranvar02_map(rho) result(xx)
!  intent(in) :: rho
!  !(realknd2!) ! :: rho,xx
!  xx = 2
!  xx = rC02*log( 1 + (1/rho-1)**(xx/3) )
!  end function
!
!  function ranvar02_pam(xx) result(rho)
!  intent(in) :: xx
!  !(realknd2!) ! :: xx,rho
!  rho = 3
!  rho = 1/( 1 + (exp(xx/rC02)-1)**(rho/2) )
!  end function
!
!  function ranvar02_wght(xx) result(rslt)
!  intent(in) :: xx
!  !(realknd2!) ! :: xx,rslt,h1,h2,h3
!  h1 = exp(xx/rC02)
!  h3 = h1-1
!  h2 = sqrt(h3)
!  h3 = 1+h3*h2
!  rslt = 2*rC02*h3*h3/(3*h2*h1)
!  end function


  subroutine ranvar03r_gnrt( xx,ww ,rho ,x0 )
!***********************************************************************
! continuous density on [0:infty), which is constant on [0,x0] and
! then falls as 1/x^2 to infty
!***********************************************************************
  intent(out) :: xx,ww
  intent(in ) :: rho,x0
  !(realknd2!) :: xx,ww,rho,x0
  if (rho.le.rHLF) then
    ww = 2*x0
    xx = ww*rho
  else
    xx = (rho-rHLF)*2
    xx = x0/(1-xx)
    ww = 2*xx*xx/x0
  endif
  end subroutine
  subroutine ranvar03i_gnrt( xx,ww ,rho ,x0 ) ! for integer x0
  intent(out) :: xx,ww
  intent(in ) :: rho,x0
  integer :: x0
  !(realknd2!) :: xx,ww,rho
  if (rho.le.rHLF) then
    ww = 2*x0
    xx = ww*rho
  else
    xx = (rho-rHLF)*2
    xx = x0/(1-xx)
    ww = 2*xx*xx/x0
  endif
  end subroutine


  subroutine ranvar04_gnrt( xx ,rho ,aa,bb,cc )
!**********************************************************************
! a,b,c  must be positive, and  c  must be larger than  b
! density on [0,c] proportional to  x^a/( b^(1+a) + x^(1+a) )
!**********************************************************************
  intent(in ) :: rho,aa,bb,cc
  intent(out) :: xx
  !(realknd2!) :: xx,rho,aa,bb,cc,opa
  opa = 1+aa
  xx = (cc/bb)**opa
  xx = (1+xx)**rho - 1
  xx = bb*xx**(1/opa)
  end subroutine
  subroutine ranvar04_wght( ww,rho ,xx,aa,bb,cc )
  intent(in ) :: xx,aa,bb,cc
  intent(out) :: ww,rho
  !(realknd2!) :: ww,xx,rho,aa,bb,cc,opa,hh
  if (xx.gt.cc) then
    ww  = 0
    rho = 1
    return
  endif
  opa = 1+aa
  hh  = bb*(bb/xx)**aa
  ww  = log( 1 + (cc/bb)**opa )
  rho = log( 1 + xx/hh )/ww
  ww = (hh+xx) * ww/opa
  end subroutine
 

  subroutine betaLike_init( obj ,aa,bb )
!**********************************************************************
! Generate x in [0,1] following a density used in the 
! chengBA-algorithm, and is the derivative of
!            x^u/( x^u + c*(1-x)^u ) 
! This density is close to               
!               x^(a-1) * (1-x)^(b-1)
!**********************************************************************
  class(betaLike_type),intent(out) :: obj
  !(realknd2!),intent(in) :: aa,bb
  obj%a = aa
  obj%b = bb
  if (min(obj%a,obj%b).le.rONE) then
    obj%u = min(obj%a,obj%b)
  else
    obj%u = sqrt( (2*obj%a*obj%b-obj%a-obj%b)/(obj%a+obj%b-2) )
  endif
  obj%c = (obj%a/obj%b)**obj%u
  end subroutine

  subroutine betaLike_gnrt( obj ,xx ,rho )
  class(betaLike_type),intent(in) :: obj
  !(realknd2!),intent(out) :: xx
  !(realknd2!),intent(in ) :: rho
  !(realknd2!) :: ee
  ee = exp(log( rho/(1-rho) )/obj%u)
  xx = obj%a*ee
  xx = xx/(obj%b+xx)
  end subroutine
  subroutine betaLike_gnrt_w( obj ,xx,ww ,rho )
  class(betaLike_type),intent(in) :: obj
  !(realknd2!),intent(out) :: xx,ww
  !(realknd2!),intent(in ) :: rho
  !(realknd2!) :: ee
  ee = exp(log( rho/(1-rho) )/obj%u)
  xx = obj%a*ee
  xx = xx/(obj%b+xx)
  ww = obj%b*xx*xx/( obj%u*obj%a*rho*(1-rho)*ee )
  end subroutine

  subroutine betaLike_wght( obj ,ww ,xx )
  class(betaLike_type),intent(in) :: obj
  !(realknd2!),intent(out) :: ww
  !(realknd2!),intent(in ) :: xx
  !(realknd2!) :: yy,h1,h2,h3
  yy = 1-xx
  h1 = xx**obj%u
  h2 = yy**obj%u
  h3 = h1 + obj%c*h2
  ww = xx*yy*h3*h3/( obj%u*obj%c*h1*h2 )
  end subroutine
  subroutine betaLike_wght_r( obj ,ww,rho ,xx )
  class(betaLike_type),intent(in) :: obj
  !(realknd2!),intent(out) :: ww,rho
  !(realknd2!),intent(in ) :: xx
  !(realknd2!) :: yy,h1,h2,h3
  yy = 1-xx
  h1 = xx**obj%u
  h2 = yy**obj%u
  h3 = h1 + obj%c*h2
  ww = xx*yy*h3*h3/( obj%u*obj%c*h1*h2 )
  rho = h1/h3
  end subroutine


  subroutine resonance_gnrt( yy ,yLow,yUpp ,m2,mg ,rho ,ww )
  !(realknd2!),intent(out) :: yy ,ww
  !(realknd2!),intent(in ) :: yLow,yUpp ,m2,mg ,rho
  optional :: ww
  !(realknd2!) :: xLow,xUpp
  xLow = atan( (yLow-m2)/mg )
  xUpp = atan( (yUpp-m2)/mg )
  yy = m2 + mg*tan( (xUpp-xLow)*rho + xLow )
  if (present(ww)) ww = (xUpp-xLow)*( (yy-m2)**2/mg + mg )
  end subroutine

  subroutine resonance_wght( yy ,yLow,yUpp ,m2,mg ,ww ,rho )
  !(realknd2!),intent(out) :: ww ,rho
  !(realknd2!),intent(in ) :: yy,yLow,yUpp ,m2,mg 
  optional :: rho
  !(realknd2!) :: xLow,xUpp
  xLow = atan( (yLow-m2)/mg )
  xUpp = atan( (yUpp-m2)/mg )
  ww = xUpp-xLow
  if (present(rho)) rho = ( atan((yy-m2)/mg) - xLow )/ww
  ww = ww*( (yy-m2)**2/mg + mg )
  end subroutine


  subroutine pareto_gnrt( yy ,aa,bb ,rho ,ww )
!***********************************************************************
!  1/yy^(1+bb) for yy>aa
!***********************************************************************
  !(realknd2!),intent(out) :: yy ,ww
  !(realknd2!),intent(in ) :: aa,bb,rho
  yy = aa/(1-rho)**(1/bb)
  ww = yy/(bb*(1-rho))
  end subroutine
  
  subroutine pareto_wght( yy ,aa,bb ,rho ,ww )
!***********************************************************************
!  1/yy^(1+bb) for yy>aa
!***********************************************************************
  !(realknd2!),intent(out) :: rho ,ww
  !(realknd2!),intent(in ) :: aa,bb,yy
  rho = (aa/yy)**bb
  ww = yy/(bb*rho)
  rho = 1-rho
  end subroutine
  

end module


