module withTails_mod
  implicit none

  type :: withTails_type
  end type

contains

  subroutine withTails_init( obj)
  end subroutine



  subroutine withTails_xw( obj ,xx,rho ,left,body,right )
!***********************************************************************
! 
!***********************************************************************
  if     (rho.le.obj%pL) then
    obj%bin = 1
    yy = rho/obj%pL
    call left( xx,ww ,yy ,obj%xM,obj%xL ,obj%aL )
    obj%ww = ww/obj%pL
  elseif (rho.le.obj%pL+obj%pB) then
    obj%bin = 2
    yy = (rho-obj%pL)/obj%pB
    call body( xx,ww ,yy ,obj%xL,obj%xR ,obj%aB )
    obj%ww = ww/obj%pB
  else
    obj%bin = 3
    yy = (rho-obj%pL-obj%pB)/obj%pR
    call right( xx,ww ,yy ,obj%xR,obj%xP ,obj%aR )
    obj%ww = ww/obj%pR
  endif
  end function


  function withTail_w( obj ) result(ww)
  ww = obj%ww
  end function


  subroutine withTails_x( obj ,xx ,rho ,left,body,right )
!***********************************************************************
!***********************************************************************
  if     (rho.le.obj%pL) then
    obj%bin = 1
    yy = rho/obj%pL
    call left( xx ,yy ,obj%xM,obj%xL ,obj%aL )
  elseif (rho.le.obj%pL+obj%pB) then
    obj%bin = 2
    yy = (rho-obj%pL)/obj%pB
    call body( xx ,yy ,obj%xL,obj%xR ,obj%aB )
  else
    obj%bin = 3
    yy = (rho-obj%pL-obj%pB)/obj%pR
    call right( xx ,yy ,obj%xR,obj%xP ,obj%aR )
  endif
  end subroutine


  subroutine withTails_wr( obj ,ww,rho ,xx ,left,body,right )
  if     (xx.le.obj%xL) then
    obj%bin = 1
    call left( ww,yy ,xx ,obj%xM,obj%xL ,obj%aL )
    ww = ww/obj%pL
    rho = yy*obj%pL
  elseif (xx.le.obj%xR) then
    obj%bin = 2
    call body( ww,yy ,xx ,obj%xL,obj%xR ,obj%aB )
    ww = ww/obj%pB
    rho = yy*obj%pB + obj%pL
  else
    obj%bin = 3
    call right( ww,yy ,xx ,obj%xR,obj%xP ,obj%aR )
    ww = ww/obj%pR
    rho = yy*obj%pR + obj%pL + obj%pB
  endif
  end subroutine


  subroutine withTails_wOFx( obj ,ww ,xx ,left,body,right )
  if     (xx.le.obj%xL) then
    obj%bin = 1
    call left( ww ,xx ,obj%xM,obj%xL ,obj%aL )
    ww = ww/obj%pL
  elseif (xx.le.obj%xR) then
    obj%bin = 2
    call body( ww ,xx ,obj%xL,obj%xR ,obj%aB )
    ww = ww/obj%pB
  else
    obj%bin = 3
    call right( ww ,xx ,obj%xR,obj%xP ,obj%aR )
    ww = ww/obj%pR
  endif
  end subroutine


  subroutine withTails_collect( obj ,ww )
!
  if (obj%bin.le.0) return
  if (ww.le.rZRO) return
!
  obj%w(obj%bin) = obj%w(obj%bin) + ww
  obj%idat = obj%idat + 1
  obj%bin = 0
!
  if (obj%idat.lt.obj%ndat) return
  obj%idat = 0
!
  hh = obj%w(1)+obj%w(2)+obj%w(3)
  obj%pL = obj%w(1)/hh
  obj%pB = obj%w(2)/hh
  obj%pR = obj%w(3)/hh
!
  end subroutine


end module
