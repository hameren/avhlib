module avh_colormatrix
  use avh_doloops
  use avh_colorflow, only:Ncolor
  implicit none
  private
  public :: colormatrix_square ,colormatrix_single ,colormatrix_double

  integer,parameter :: gluon=0,quark=1,antiq=-1

  type :: dstring_type
    integer :: Ntot
    integer,allocatable :: upp(:),low(:)
    integer :: factor
  contains 
    procedure :: dalloc
    procedure :: alloc
    procedure :: connect
    procedure :: copy
    procedure :: eval
    procedure :: prnt
  end type

  abstract interface
    subroutine dc_interface
    end subroutine
  end interface

contains

  subroutine dalloc( obj )
    class(dstring_type) :: obj
    if (allocated(obj%upp)) deallocate(obj%upp)
    if (allocated(obj%low)) deallocate(obj%low)
    obj%Ntot = 0
    obj%factor = 0
  end subroutine
  
  subroutine alloc( obj ,nn )
    class(dstring_type) :: obj
    integer,intent(in) :: nn
    call obj%dalloc
    allocate(obj%upp(nn))
    allocate(obj%low(nn))
    obj%Ntot = nn
  end subroutine
  
  subroutine copy( obj ,xx )
    class(dstring_type),intent(in)  :: obj
    class(dstring_type),intent(out) :: xx
    call xx%alloc( obj%Ntot )
    xx%upp = obj%upp
    xx%low = obj%low
    xx%factor = obj%factor
  end subroutine
  
  subroutine connect( obj ,n1 ,n2,n3 )
    class(dstring_type)  :: obj
    integer,intent(in) :: n1,n2,n3
    obj%upp(n1)=n2 ;obj%low(n2)=n1
    obj%low(n1)=n3 ;obj%upp(n3)=n1
  end subroutine

  subroutine insertOne( obj ,ff ,ii,jj ,nn )
! jj corresponds to (i,j,k,l) in the following, while nn corresponds to (r,s)
! if ff=gluon, then replace
!    d(k,i)d(j,l)d(r,s)  with  d(k,i)d(r,l)d(j,s) - d(r,i)d(k,s)d(j,l)
! if ff=antiq, then replace
!    d(k,i)d(r,s)  with  -d(r,i)d(k,s) + d(k,i)d(r,s)/N
! if ff=quark then replace
!    d(j,l)d(r,s)  with  d(r,l)d(j,s) - d(j,l)d(r,s)/N
    type(dstring_type),intent(inout) :: obj(:)
    integer,intent(in) :: ff,ii,jj,nn
    integer :: siz,hh,kk,ll,uu
    siz = size(obj)/2
    do hh=1,siz
      associate( o=>obj(hh) ,t=>obj(siz+hh) )
      call o%copy(t)
      kk = o%upp(ii)
      ll = o%low(jj)
      uu = o%upp(nn)
      if     (ff.eq.quark) then
        o%low(uu)=ll ;o%upp(ll)=uu
        o%upp(nn)=jj ;o%low(jj)=nn
        t%factor =-t%factor/Ncolor(1)
      elseif (ff.eq.antiq) then
        o%low(uu)=ii ;o%upp(ii)=uu
        o%upp(nn)=kk ;o%low(kk)=nn
        o%factor =-o%factor
        t%factor = t%factor/Ncolor(1)
      else
        o%low(uu)=ll ;o%upp(ll)=uu
        o%upp(nn)=jj ;o%low(jj)=nn
        t%low(uu)=ii ;t%upp(ii)=uu
        t%upp(nn)=kk ;t%low(kk)=nn
        t%factor =-t%factor
      endif
      end associate
    enddo
  end subroutine

  subroutine insertTwo( obj ,ff ,ii,jj ,n1,n2 )
    type(dstring_type),intent(inout) :: obj(:)
    integer,intent(in) :: ff,ii,jj,n1,n2
    integer :: siz,hh,kk,ll,u1,u2
    siz = size(obj)/4
    do hh=1,siz
      associate( o0=>obj(hh) ,o1=>obj(siz+hh) &
                ,o2=>obj(2*siz+hh) ,o3=>obj(3*siz+hh) )
      call o0%copy(o1)
      call o0%copy(o2)
      call o0%copy(o3)
      kk = o0%upp(ii)
      ll = o0%low(jj)
      u1 = o0%upp(n1)
      u2 = o0%upp(n2)
      if     (ff.eq.quark) then
        o0%low(u1)=n2 ;o0%upp(n2)=u1
        o0%low(jj)=n1 ;o0%upp(n1)=jj
        o0%low(u2)=ll ;o0%upp(ll)=u2
        o1%low(u1)=ll ;o1%upp(ll)=u1
        o1%low(jj)=n1 ;o1%upp(n1)=jj
        o2%low(jj)=n2 ;o2%upp(n2)=jj
        o2%low(u2)=ll ;o2%upp(ll)=u2
        o1%factor =-o1%factor/Ncolor(1)
        o2%factor =-o2%factor/Ncolor(1)
        o3%factor = o3%factor/Ncolor(2)
      elseif (ff.eq.antiq) then
        o0%low(u1)=ii ;o0%upp(ii)=u1
        o0%low(u2)=n1 ;o0%upp(n1)=u2
        o0%low(kk)=n2 ;o0%upp(n2)=kk
        o1%low(u1)=ii ;o1%upp(ii)=u1
        o1%low(kk)=n1 ;o1%upp(n1)=kk
        o2%low(u2)=ii ;o2%upp(ii)=u2
        o2%low(kk)=n2 ;o2%upp(n2)=kk
        o1%factor =-o1%factor/Ncolor(1)
        o2%factor =-o2%factor/Ncolor(1)
        o3%factor = o3%factor/Ncolor(2)
      else
        o0%low(u1)=n2 ;o0%upp(n2)=u1
        o0%low(jj)=n1 ;o0%upp(n1)=jj
        o0%low(u2)=ll ;o0%upp(ll)=u2
!
        o3%low(u1)=ii ;o3%upp(ii)=u1
        o3%low(u2)=n1 ;o3%upp(n1)=u2
        o3%low(kk)=n2 ;o3%upp(n2)=kk
!
        o1%low(u2)=ii ;o1%upp(ii)=u2
        o1%low(u1)=ll ;o1%upp(ll)=u1
        o1%low(jj)=n1 ;o1%upp(n1)=jj
        o1%low(kk)=n2 ;o1%upp(n2)=kk
        o1%factor =-o1%factor
        o2%low(u1)=ii ;o2%upp(ii)=u1
        o2%low(kk)=n1 ;o2%upp(n1)=kk
        o2%low(jj)=n2 ;o2%upp(n2)=jj
        o2%low(u2)=ll ;o2%upp(ll)=u2
        o2%factor =-o2%factor
      endif
      end associate
    enddo
  end subroutine

  subroutine eval( obj )
    class(dstring_type) :: obj
    integer :: ii,jj,nn
    nn = obj%Ntot
    do while (nn.gt.0)
      ii = obj%upp(nn)
      jj = obj%low(nn)
      if (ii.eq.nn) then
        obj%factor = obj%factor*Ncolor(1)
      else
        obj%low(ii)=jj ;obj%upp(jj)=ii
      endif
      nn = nn-1
    enddo
  end subroutine
 
 
  subroutine colormatrix_square( Npair ,permList ,matrix )
    integer,intent(in) :: Npair,permList(:)
    integer,intent(out) :: matrix(size(permList),size(permList))
    integer :: pCnt,qCnt,hh,pp(13),qq(13),invq(13)
    type(dstring_type) :: d
    call d%alloc(Npair*2)
!
    if (Npair.eq.2.and.size(permList).eq.1) then
! General algorithm breaks for only two gluons.
      matrix = 8
!
    else
      do pCnt=1,size(permList) ;call permutation(pp,Npair,permList(pCnt))
      do qCnt=1,size(permList) ;call permutation(qq,Npair,permList(qCnt))
        do hh=1,Npair
          invq(qq(hh)) = hh
        enddo
        d%factor = Ncolor(0)
        do hh=1,Npair
          call d%connect( hh ,Npair+(Npair-hh)+1 ,Npair+(Npair-invq(pp(hh))+1) )
        enddo
        call d%eval
        matrix(pCnt,qCnt) = d%factor
      enddo
      enddo
!     
      call d%dalloc()
    endif
  end subroutine
 

  subroutine colormatrix_single( Npair ,corr ,flavor ,permList ,matrix )
    integer,intent(in) :: Npair ,corr(2) ,flavor(2) ,permList(:)
    integer,intent(out) :: matrix(size(permList),size(permList))
    integer :: pCnt,qCnt,hh,pp(13),qq(13),invq(13),invp(13),twoN
    type(dstring_type) :: d(4)
!
    if (Npair.eq.2.and.size(permList).eq.1) then
! General algorithm breaks for only two gluons.
      matrix =-162
!
    else
      twoN = Npair*2
      do hh=1,4
        call d(hh)%alloc(twoN+2)
      enddo
!     
      do pCnt=1,size(permList) ;call permutation(pp,Npair,permList(pCnt))
      do qCnt=1,size(permList) ;call permutation(qq,Npair,permList(qCnt))
        do hh=1,Npair
          invp(pp(hh)) = hh
          invq(qq(hh)) = hh
        enddo
        d(1)%factor = Ncolor(2)
        do hh=1,Npair
          call d(1)%connect( hh ,Npair+(Npair-hh)+1 ,Npair+(Npair-invq(pp(hh))+1) )
        enddo
        call d(1)%connect( twoN+1 ,twoN+2 ,twoN+2 )
        call insertOne( d(1:2) ,flavor(1) ,corr(1),invp(corr(1)) ,twoN+1 )
        call insertOne( d(1:4) ,flavor(2) ,corr(2),invp(corr(2)) ,twoN+2 )
        matrix(pCnt,qCnt) = 0
        do hh=1,4
          call d(hh)%eval
          matrix(pCnt,qCnt) = matrix(pCnt,qCnt) + d(hh)%factor
        enddo
        matrix(pCnt,qCnt) = matrix(pCnt,qCnt)/Ncolor(1)
      enddo
      enddo
!     
      do hh=1,4
        call d(hh)%dalloc()
      enddo
    endif
  end subroutine
 

  subroutine colormatrix_double( Npair ,corr ,flavor ,permList ,matrix )
    integer,intent(in) :: Npair ,corr(4) ,flavor(4) ,permList(:)
    integer,intent(out) :: matrix(size(permList),size(permList))
    integer :: pCnt,qCnt,hh,pp(13),qq(13),invq(13),invp(13),twoN
    integer :: ii,jj,kk,ll,fi,fj,fk,fl
    type(dstring_type) :: d(16)
!
    if (Npair.eq.2.and.size(permList).eq.1) then
! General algorithm breaks for only two gluons.
! Only the T1T2*T1T2 double correlator exists for this situation.
      matrix = 2916
!
    else
      twoN = Npair*2
      do hh=1,4
        call d(hh)%alloc(twoN+4)
      enddo
!    
      !ii=corr(1)   ;jj=corr(2)   ;kk=corr(3)   ;ll=corr(4)
      !fi=flavor(1) ;fj=flavor(2) ;fk=flavor(3) ;fl=flavor(4)
      ii=corr(3)   ;jj=corr(4)   ;kk=corr(1)   ;ll=corr(2)
      fi=flavor(3) ;fj=flavor(4) ;fk=flavor(1) ;fl=flavor(2)
!    
      if (ii.eq.kk.and.fi.eq.fk) then
        if (jj.eq.ll.and.fj.eq.fl) then
          call eval_dc(g1212)
        else
          call eval_dc(g1213)
        endif
      elseif (ii.eq.ll.and.fi.eq.fl) then
        hh=kk;kk=ll;ll=hh
        hh=fk;fk=fl;fl=hh
        if (jj.eq.ll.and.fj.eq.fl) then
          call eval_dc(g1212)
        else
          call eval_dc(g1213)
        endif
      elseif (jj.eq.kk.and.fj.eq.fk) then
        hh=ii;ii=jj;jj=hh
        hh=fi;fi=fj;fj=hh
        call eval_dc(g1213)
      elseif (jj.eq.ll.and.fj.eq.fl) then
        hh=ii;ii=jj;jj=hh
        hh=kk;kk=ll;ll=hh
        hh=fi;fi=fj;fj=hh
        hh=fk;fk=fl;fl=hh
        call eval_dc(g1213)
      else
        call eval_dc(g1234)
      endif
!    
      do hh=1,16
        call d(hh)%dalloc()
      enddo
    endif
!
  contains
!
    subroutine eval_dc(func)
      procedure(dc_interface) :: func
      do pCnt=1,size(permList) ;call permutation(pp,Npair,permList(pCnt))
      do qCnt=1,size(permList) ;call permutation(qq,Npair,permList(qCnt))
        do hh=1,Npair
          invp(pp(hh)) = hh
          invq(qq(hh)) = hh
        enddo
        d(1)%factor = Ncolor(4)
        do hh=1,Npair
          call d(1)%connect( hh ,Npair+(Npair-hh)+1 ,Npair+(Npair-invq(pp(hh))+1) )
        enddo
        call d(1)%connect( twoN+1 ,twoN+2 ,twoN+2 )
        call d(1)%connect( twoN+3 ,twoN+4 ,twoN+4 )
        call func
        matrix(pCnt,qCnt) = 0
        do hh=1,16
          call d(hh)%eval
          matrix(pCnt,qCnt) = matrix(pCnt,qCnt) + d(hh)%factor
        enddo
        matrix(pCnt,qCnt) = matrix(pCnt,qCnt)/Ncolor(2)
      enddo
      enddo
    end subroutine
 
    subroutine g1234
      call insertOne( d(1: 2) ,fi ,ii,invp(ii) ,twoN+1 )
      call insertOne( d(1: 4) ,fj ,jj,invp(jj) ,twoN+2 )
      call insertOne( d(1: 8) ,fk ,kk,invp(kk) ,twoN+3 )
      call insertOne( d(1:16) ,fl ,ll,invp(ll) ,twoN+4 )
    end subroutine
!
    subroutine g1213
      call insertTwo( d(1: 4) ,fi ,ii,invp(ii) ,twoN+1,twoN+3 )
      call insertOne( d(1: 8) ,fj ,jj,invp(jj) ,twoN+2 )
      call insertOne( d(1:16) ,fl ,ll,invp(ll) ,twoN+4 )
    end subroutine
!
    subroutine g1212
      call insertTwo( d(1: 4) ,fi ,ii,invp(ii) ,twoN+1,twoN+3 )
      call insertTwo( d(1:16) ,fj ,jj,invp(jj) ,twoN+2,twoN+4 )
    end subroutine
!
  end subroutine


  subroutine prnt( obj ,writeUnit )
    class(dstring_type) :: obj
    integer,intent(in),optional :: writeUnit
    integer :: wUnit,ii
    character(254) :: aa,bb,cc,dd,word
    wUnit = 6 ;if (present(writeUnit)) wUnit=writeUnit
    aa = ''
    bb = ''
    cc = ''
    dd = ''
    do ii=1,obj%Ntot
      write(word,'(i2)') ii          ;aa=trim(aa)//'|'//trim(word)
      write(word,'(i2)') obj%upp(ii) ;bb=trim(bb)//'|'//trim(word)
      write(word,'(i2)') obj%low(ii) ;cc=trim(cc)//'|'//trim(word)
      dd = trim(dd)//'==='
    enddo
    write(wUnit,'(A)') trim(aa)
    write(wUnit,'(A)') trim(dd)
    write(wUnit,'(A)') trim(bb)
    write(wUnit,'(A)') trim(cc)
  end subroutine
 
end module


