module avh_coloredlgn
  !(usekindmod!)
  use avh_iounits
  use avh_prnt
  use avh_doloops
  use avh_fusiontools
  use avh_buildskel
  use avh_colorflow
  use avh_fill_colorflow
  implicit none
  private

  public :: fill_colorsampling ,fill_cconnected 
  public :: fill_cc_amp
  public :: colorRules
  public :: get_NcolDof,commonCycles,fill_colorRepr
!
!  integer,parameter :: NcolPairMax=6

  type(fusionRules_type),save :: colorRules
  logical,save :: initz=.true.

  !(integer0!),allocatable,save :: repr_particle(:)

  interface fill_cc_amp
    module procedure fill_cc_amp_1,fill_cc_amp_2
  end interface


contains


  subroutine fill_colorRepr( n0,n1,array )
  integer,intent(in) :: n0,n1
  integer,intent(in) :: array(n0:n1)
  if (allocated(repr_particle)) deallocate(repr_particle)
  allocate( repr_particle(n0:n1) )
  repr_particle(n0:n1) = array(n0:n1)
  end subroutine

  function get_repr( nn,array ) result(rslt)
  integer,intent(in) :: nn
  integer,intent(in) :: array(1:nn)
  integer :: rslt(1:nn),ii
  do ii=1,nn
    rslt(ii) = repr_particle(array(ii))
  enddo
  end function


  subroutine fill_cconnected( dght ,matrix ,mthr ,printcc ,keepColcon )
!**********************************************************************
!**********************************************************************
  type(  dsskeleton_type),intent(in ) :: mthr
  type(daughterList_type),intent(out) :: dght
  !(integer0!),allocatable,intent(out) :: matrix(:,:)
  integer,optional,intent(in) :: printcc
  logical,optional,intent(in) :: keepColcon
  integer :: iPer(mthr%Nleaves+2),jPer(mthr%Nleaves+2)
  type(colorContent_type) :: content
  integer :: wUnit,ii,jj
  logical :: keepIt
!
  wUnit=0 ;if (present(printcc)) wUnit=printcc
  keepIt=.false. ;if (present(keepColcon)) keepIt=keepColcon
!
  if (keepIt) then
    call fill_cc_amp( content ,dght ,mthr ,wUnit ,keepIt )
  else
    call fill_cc_amp( content ,dght ,mthr ,wUnit )
  endif
!
  associate( Ndght=>dght%Ndaughters ,Npair=>content%Npair )
  if (allocated(matrix)) deallocate(matrix)
  allocate(matrix(1:Ndght,1:Ndght))
  do ii=1,Ndght ;call permutation(iPer,Npair,dght%daughter(ii)%Label)
  do jj=1,Ndght ;call permutation(jPer,Npair,dght%daughter(jj)%Label)
    matrix(ii,jj) = commonCycles(Npair,iPer,jPer)
  enddo
  enddo
  end associate
  end subroutine


  subroutine fill_cc_amp_1( content ,dght ,mthr ,wUnit ,keepColcon )
!**********************************************************************
! color correlated
!**********************************************************************
  type(colorContent_type),intent(out) :: content
  type(daughterList_type),intent(out) :: dght
  type(  dsskeleton_type),intent(in ) :: mthr
  logical       ,optional,intent(in ) :: keepColcon
  integer,intent(in) :: wUnit
!
  type(permut_type) :: permloop
  integer :: xColor(mthr%Nleaves+1)
  integer :: per(0:mthr%Nleaves+2)
  integer :: ii,jj,Nxtrn
  type(fusionRules_type) :: tmpRules
  logical :: keepIt
  integer :: colcon(2,mthr%Nleaves+1)
  associate(Npair=>content%Npair)
!
  Nxtrn = mthr%Nleaves+1
!
  call content%fill( Nxtrn ,get_repr(Nxtrn,mthr%Process) )
  call fill_colorflow( tmpRules ,max(1,Npair) ,updateRule=colorRule )
!
  keepIt=.false. ;if (present(keepColcon)) keepIt=keepColcon
!
!  if ( Npair.gt.NcolPairMax-1.and..not. &
!      (Npair.eq.NcolPairMax.and.content%Nadjoint.eq.NcolPairMax)) then
!    if (errru.ge.0) write(errru,*) 'ERROR in fill_cc_amp: ' &
!      ,' too many colored particles for color-connected approach'
!    stop
!  endif
!
  ii = 0
  call permloop%init(Npair,per(1:))
  do ;ii=ii+1
    per(0) = 0
    per(Npair+1) = per(1)
    do jj=1,Nxtrn
      colcon(1:2,jj) = [content%Ref(1,jj),per(content%Ref(2,jj))]
      xColor(jj) = map(colcon(1,jj),colcon(2,jj))
    enddo
    if (keepIt) then
      call dght%add( mthr ,tmpRules ,xColor ,ii ,colcon )
    else
      call dght%add( mthr ,tmpRules ,xColor ,ii )
    endif
    !if (Npair.eq.Nxtrn) ccAmp(tumrep(cc2perm(per(1:Npair),Npair),Npair)) = dght%Ndaughters
    !if (dght%added.and.wUnit.gt.0) write(wUnit,*) &
    !  'color connection',prnt(dght%Ndaughters,3),': ',prnt_color(Nxtrn,xColor) &
    !  ,prnt(dght%daughter(dght%Ndaughters)%Label,4)
  if (permloop%exit(per(1:))) exit
  enddo
!
  end associate
  end subroutine


  subroutine fill_cc_amp_2( dght ,mthr ,content )
!**********************************************************************
! color correlated
!**********************************************************************
  type(daughterList_type),intent(out) :: dght
  type(  dsskeleton_type),intent(in ) :: mthr
  type(colorContent_type),intent(in ) :: content
!
  type(permut_type) :: permloop
  integer :: xColor(mthr%Nleaves+1)
  integer :: per(0:mthr%Nleaves+2)
  integer :: ii,jj,Nxtrn
  type(fusionRules_type) :: tmpRules
  integer :: colcon(2,mthr%Nleaves+1)
!
  Nxtrn = mthr%Nleaves+1
!
  call fill_colorflow( tmpRules ,max(1,content%Npair) ,updateRule=colorRule )
!
  ii = 0
  call permloop%init(content%Npair,per(1:))
  do ;ii=ii+1
    per(0) = 0
    per(content%Npair+1) = per(1)
    do jj=1,Nxtrn
      colcon(1:2,jj) = [content%Ref(1,jj),per(content%Ref(2,jj))]
      xColor(jj) = map(colcon(1,jj),colcon(2,jj))
    enddo
    call dght%add( mthr ,tmpRules ,xColor ,ii ,colcon )
  if (permloop%exit(per(1:))) exit
  enddo
  end subroutine


  function perm2cc(per,nn) result(rslt)
! maps a permutation of gluons to a color connection
  integer,intent(in) :: nn
  integer,intent(in) :: per(nn)
  integer :: rslt(nn)
  integer :: ii,jj,rep(13)
  do ii=1,nn
    rep(per(ii)) = ii
  enddo
  do ii=1,nn
    jj = rep(ii)+1
    if (jj.gt.nn) jj = 1
    rslt(ii) = per(jj)
  enddo
  end function
  
  function cc2perm(per,nn) result(rslt)
! maps a color connection to a permutation of gluons
  integer,intent(in) :: nn
  integer,intent(in) :: per(nn)
  integer :: rslt(nn)
  integer :: ii,jj
  rslt(1) = 1
  do ii=2,nn
    rslt(ii) = per(rslt(ii-1))
  enddo
  do while (rslt(nn).ne.nn)
    jj = rslt(nn)
    rslt(2:nn) = rslt(1:nn-1)
    rslt(1) = jj
  enddo
  end function
  


  subroutine fill_colorsampling( content ,nn,process )
!**********************************************************************
! process in terms of particles
!**********************************************************************
  type(colorContent_type),intent(out) :: content
  integer                ,intent(in ) :: nn
  integer                ,intent(in ) :: process(1:nn)
  if (initz) then
    initz = .false.
    call fill_colorflow( colorRules ,Ncolor(1) ,compatible=colorcompat )
  endif
  call content%fill( nn ,get_repr(nn,process) )
  end subroutine


  function colorcompat(ii,jj) result(rslt)
!***********************************************************************
! Is true if the color representation associated with color value ii
! matches the color representation of particle jj
!***********************************************************************
  integer,intent(in) :: ii,jj
  logical :: rslt
  rslt = repr_colorval(ii).eq.repr_particle(jj)
  end function


  function get_NcolDof(ii) result(rslt)
  integer,intent(in) :: ii
  integer :: rslt
  select case(repr_particle(ii))
    case(adjoint) ;rslt=Ncolor(2)-1
    case(fundame) ;rslt=Ncolor(1)
    case(antifun) ;rslt=Ncolor(1)
    case default  ;rslt=1
  end select
  end function
  

  subroutine colorRule( obj ,p ,f ,ifus ,f0 ,discard )
  class(fusionRules_type) :: obj 
  integer,intent(in   ) :: p(0:) ,f(1:)
  integer,intent(inout) :: ifus
  integer,intent(out  ) :: f0
  logical,intent(out  ) :: discard
  optional              :: f0,discard
  integer :: irhs,ii
  irhs = 0
  do ii=1,size(f)
    irhs = obj%Reference(f(ii),irhs)
  enddo
  if (present(f0)) then
    f0 = obj%FusionResult(ifus,irhs)
    discard = repr_colorval(f0).ne.repr_particle(p(0))
  else
    ifus = obj%NfusionResults(irhs)
  endif
  end subroutine


  function commonCycles( nn,per1,per2 ) result(rslt)
  intent(in) :: nn,per1,per2
  integer :: nn,rslt
  integer :: per1(nn),per2(nn),pos(nn),i0,ii,jj
  logical :: free(nn+1)
  if (nn.le.0) then
    rslt = 0
    return
  endif
  do ii=1,nn ;pos(per2(ii))=ii ;enddo
  free = .true.
  rslt = 0
  i0 = 1
  do
    ii = i0
    free(ii) = .false.
    do
      jj = pos(per1(ii))
      if (jj.eq.i0) then
        rslt = rslt+1
        i0=0 ;do ;i0=i0+1 ;if (free(i0)) exit ;enddo
        if (i0.gt.nn) return
        exit
      else
        ii = jj
        free(ii) = .false.
      endif
    enddo
  enddo
  end function


end module


