module avh_colorflow
  !(usekindmod!)
  use avh_mathcnst
  use avh_ioUnits
  use avh_prnt
  use avh_doloops

  implicit none
  private
  public :: colorContent_type,init_colorflow
  public :: map,pam,Nmax,Ncolor
  public :: prnt_color
  public :: repr_colorval,singlet,fundame,antifun,adjoint

  integer,parameter :: Ncolor(0:14)=[1,3,9,27,81,243,729,2187,6561,19683 &
                                    ,59049,177147,531441,1594323,4782969]

  integer,parameter :: singlet=1,adjoint=2,fundame=3,antifun=4
 
  integer,parameter :: nul=0

  integer,parameter :: Nmax=12
  integer     ,save :: map(0:Nmax,0:Nmax)
  !(integer0!),save :: pam(1:2,nul:nul+(1+Nmax)**2)
  !(integer0!),save :: repr_colorval(nul:nul+(1+Nmax)**2)

  type :: colorContent_type
    integer :: Nxtrn,Npair,Nadjoint
    integer,allocatable :: Ref(:,:)
  contains
    procedure :: fill=>fill_content
  end type

  interface prnt_color
    module procedure prnt_color,prnt_color_2
  end interface

  logical,save :: initd=.false.

contains


  subroutine init_colorflow
  integer :: ii,jj,nn
  if (initd) return ;initd=.true.
! map color pairs on single integers, pam is the inverse
  pam =-1
  map = nul
  nn = nul
  nn=nn+1 ;map( 0, 0)=nn ;pam(1:2,nn)=[ 0, 0] ;repr_colorval(nn)=singlet
  do ii=1,Nmax
    nn=nn+1 ;map( 0,ii)=nn ;pam(1:2,nn)=[ 0,ii] ;repr_colorval(nn)=antifun
    nn=nn+1 ;map(ii, 0)=nn ;pam(1:2,nn)=[ii, 0] ;repr_colorval(nn)=fundame
    nn=nn+1 ;map(ii,ii)=nn ;pam(1:2,nn)=[ii,ii] ;repr_colorval(nn)=adjoint
    do jj=1,ii-1
      nn=nn+1 ;map(ii,jj)=nn ;pam(1:2,nn)=[ii,jj] ;repr_colorval(nn)=adjoint
      nn=nn+1 ;map(jj,ii)=nn ;pam(1:2,nn)=[jj,ii] ;repr_colorval(nn)=adjoint
    enddo
  enddo
  end subroutine


  function prnt_color( color ) result(rslt)
  integer,intent(in) :: color
  character(5) :: rslt
  integer :: kk,ll
  if (color.le.nul) then
    rslt = '     '
  else
    kk = pam(1,color)
    ll = pam(2,color)
    rslt = '('//trim(prnt(kk,'lft'))//','//trim(prnt(ll,'lft'))//')'
  endif
  end function

  function prnt_color_2( nn,color ) result(rslt)
  integer,intent(in) :: nn
  integer,intent(in) :: color(nn)
  character(5*nn) :: rslt
  integer :: ii
  rslt = ''
  do ii=1,nn
    rslt = trim(rslt)//prnt_color(color(ii))
  enddo
  end function


  subroutine fill_content( obj ,nn,process )
!**********************************************************************
! Determine the number of color pairs in process, so that
! given a list of color pairs color(1:2,1:obj%Npair), the color pair
! of i in process is  color(1,obj%Ref(1,i))  color(2,obj%Ref(2,i)) 
! It assumes that color(1:2,0)=0
!**********************************************************************
  class(colorContent_type),intent(out) :: obj
  integer,intent(in) :: nn
  integer,intent(in) :: process(1:nn)
  integer :: ii,j1,j2
!
  call init_colorflow
!
  obj%Nxtrn = nn
  allocate(obj%Ref(1:2,1:nn))
  obj%Ref = 0
  obj%Nadjoint = 0
  j1 = 0
  j2 = 0
  do ii=1,nn
    if     (process(ii).eq.adjoint) then
      j1=j1+1 ;obj%Ref(1,ii)=j1
      j2=j2+1 ;obj%Ref(2,ii)=j2
      obj%Nadjoint = obj%Nadjoint+1
    endif
  enddo
  do ii=1,nn
    if (process(ii).eq.fundame) then
      j1=j1+1 ;obj%Ref(1,ii)=j1
    endif
  enddo
  do ii=1,nn
    if (process(ii).eq.antifun) then
      j2=j2+1 ;obj%Ref(2,ii)=j2
    endif
  enddo
  if (j1.ne.j2) then
    if (errru.ge.0) write(errru,*) 'ERROR in avh_colorflow fill_content: '&
      ,'different number of colors and anti-colors'
    stop
  endif
  obj%Npair = j1
  end subroutine

 
end module


