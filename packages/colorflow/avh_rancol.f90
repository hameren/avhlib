module avh_ranCol
  use avh_mathcnst
  use avh_doloops
  use avh_colorflow
  implicit none
  private
  public :: ranCol_type

  type ranCol_type
    private
    !(realknd2!),allocatable :: p(:),w(:)
    integer :: Npar,N0,Nsym,Isym,nbatch,ndat
  contains
    procedure :: init=>init_rancol
    procedure :: wght=>wght_rancol
    procedure :: gnrt=>gnrt_rancol
    procedure :: collect=>collect_rancol
    procedure :: prnt=>prnt_rancol
  end type

  integer,parameter :: Ndim=Ncolor(1)
  integer,save :: table(1:Ndim,1:symmet_size(Nmax,Ndim))
  integer,save :: symf(        1:symmet_size(Nmax,Ndim))

  logical,save :: initz=.true.

contains


  subroutine init_ranCol( obj ,Npar ,Nbatch )
  class(ranCol_type) :: obj
  intent(in) :: Npar,Nbatch
  optional   :: Nbatch
  integer :: Npar,Nbatch ,ii,jj
!
  if (initz) then
    initz = .false.
    call symmet_table( Ndim,Nmax,table,symf=symf )
  endif
!
  obj%nbatch = 0
  if (present(Nbatch)) obj%nbatch = Nbatch
!
  obj%Npar = Npar
  obj%N0   = symmet_size(Npar-1,Ndim)
  obj%Nsym = symmet_size(Npar  ,Ndim)-obj%N0
  if (allocated(obj%p)) deallocate(obj%p)
  if (allocated(obj%w)) deallocate(obj%w)
  allocate( obj%p(0:obj%Nsym) )
  allocate( obj%w(1:obj%Nsym) )
!
  obj%p(0) = 0
  do ii=1,obj%Nsym
    obj%p(ii) = symf(ii+obj%N0)**2
    obj%p(ii) = obj%p(ii-1)+obj%p(ii) ! cumulative distribution
  enddo
  obj%p(1:obj%Nsym-1) = obj%p(1:obj%Nsym-1)/obj%p(obj%Nsym)
  obj%p(obj%Nsym) = 1
  obj%w(1:obj%Nsym) = 0
  obj%ndat = 0
  end subroutine

 
  subroutine gnrt_ranCol( obj ,rslt ,rho )
  class(ranCol_type) :: obj
  integer,intent(out) :: rslt(1:2,0:obj%Npar)
  !(realknd2!),intent(in) :: rho(3)
  integer :: i0,i1,ii,jj,per1(Nmax),per2(Nmax)
! generate random symmetric configuration (number of 1s,2s,3s)
  i0 = 0
  i1 = obj%Nsym
  do while (i1-i0.gt.1)
    ii = (i0+i1)/2
    if (rho(1).le.obj%p(ii)) then
      i1 = ii
    else
      i0 = ii
    endif
  enddo
  obj%Isym = i1
! Generate random permutations
  ii = 1+int(rho(2)*igamm(1+obj%Npar))
  call permutation( per1 ,obj%Npar, ii )
  ii = 1+int(rho(3)*igamm(1+obj%Npar))
  call permutation( per2 ,obj%Npar, ii )
! Distribute the 1,2,3s following the permutation
  i0 = 0
  do ii=1,Ndim
  do jj=1,table(ii,obj%Isym+obj%N0)
    i0 = i0+1
    rslt(1,per1(i0)) = ii
    rslt(2,per2(i0)) = ii
  enddo
  enddo
  rslt(1:2,0) = 0
  end subroutine


  function wght_ranCol(obj) result(rslt)
  class(ranCol_type) :: obj
  !(realknd2!) :: rslt
  rslt = symf(obj%Isym+obj%N0)**2 &
       /( obj%p(obj%Isym) - obj%p(obj%Isym-1) )
  end function


  subroutine collect_rancol( obj ,weight )
  class(ranCol_type) :: obj
  intent(in)   :: weight
  !(realknd2!) :: weight
  integer :: ii,jj
!
  if (obj%nbatch.le.0) return
!
! Make sure the weight is used only once
  ii = obj%Isym
  if (ii.eq.0) return
  obj%Isym = 0
!   
  if (weight.le.rZRO) return
! Update the weights of the channels
  obj%w(ii) = obj%w(ii) + weight    ! entropy
! obj%w(ii) = obj%w(ii) + weight**2 ! variance
  obj%ndat = obj%ndat+1
! 
  if (obj%ndat.lt.obj%nbatch) return
  obj%ndat = 0
! Activate the updated bins for gnrt/wght
  do jj=1,obj%Nsym
    obj%p(jj) = obj%p(jj-1) + obj%w(jj)        ! entropy
!   obj%p(jj) = obj%p(jj-1) + dsqrt(obj%w(jj)) ! variance
  enddo
  obj%p(1:obj%Nsym-1) = obj%p(1:obj%Nsym-1)/obj%p(obj%Nsym)
  obj%p(obj%Nsym) = 1
  end subroutine


  subroutine prnt_ranCol(obj)
  class(ranCol_type) :: obj
!  subroutine ranCol_print(obj) ;type(ranCol_type)::obj
  integer :: ii
  do ii=1,obj%Nsym
    write(*,*) ii,obj%p(ii)-obj%p(ii-1)
  enddo
  end subroutine


  subroutine jointwo( rslt ,cc,p1,p2,Nsize )
  integer,intent(in) :: Nsize
  integer,intent(out) :: rslt(1:2,0:Nsize-1,1:2)
  integer,intent(in) :: cc(1:2,0:Nsize),p1,p2
  if (cc(1,p1).eq.cc(2,p2)) then
    rslt(1:2, 0:p2-1   ,1) = cc(1:2,   0:p2-1)
    rslt(1:2,p2:Nsize-1,1) = cc(1:2,p2+1:Nsize)
    rslt(1,p1,1) = cc(2,p1)
    rslt(2,p1,1) = cc(1,p2)
  endif
  rslt(1:2,0,2) =-1
  if (cc(2,p1).eq.cc(1,p2)) then
    rslt(1:2, 0:p2-1   ,2) = cc(1:2,   0:p2-1)
    rslt(1:2,p2:Nsize-1,2) = cc(1:2,p2+1:Nsize)
    rslt(1,p1,1) = cc(2,p2)
    rslt(2,p1,1) = cc(1,p1)
  endif
  end subroutine


end module


