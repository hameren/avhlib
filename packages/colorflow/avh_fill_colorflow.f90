module avh_fill_colorflow
  !(usekindmod!)
  use avh_doloops
  use avh_mathcnst
  use avh_othercnst
  use avh_ioUnits
  use avh_prnt
  use avh_colorflow
  use avh_fusiontools

  implicit none
  private
  public :: fill_colorflow,tab_3g,tab_4g,tab_s3g,tab_s4g

  integer,parameter :: ggg=1,gggg=2,qqg=3,qqs=4,sss=5,ssss=6,qqa=7
  integer,parameter :: s2g=8,s3g=9,s4g=10
  integer,parameter :: nul=0

  integer,parameter :: tab_3g(6)=&
    [1,2,1,2,1,2]
  integer,parameter :: tab_4g(24)=&
    [1,2,3,4,5,6,4,3,2,1,6,5,1,2,3,4,5,6,4,3,2,1,6,5]
  integer,parameter :: tab_s3g(24)=&
    [1,1,1,2,2,2,1,2,1,2,1,2,1,2,2,2,1,1,1,1,2,2,2,1]
  integer,parameter :: tab_s4g(120)=&
    [1,1,1,2,2,2,1,2,3,4,5,6,3,4,4,4,3,3,5,5,6,6,6,5&
    ,1,2,3,4,5,6,4,3,2,1,6,5,1,2,3,4,5,6,4,3,2,1,6,5&
    ,4,3,2,1,6,5,2,1,1,1,2,2,6,6,5,5,5,6,4,4,4,3,3,3&
    ,1,2,2,2,1,1,3,3,4,4,4,3,5,5,5,6,6,6,5,6,1,2,3,4&
    ,4,4,3,3,3,4,2,2,2,1,1,1,2,1,6,5,4,3,6,5,5,5,6,6]

  !(realknd2!),save :: small

contains


  subroutine fill_colorflow( rules ,Nmult ,compatible ,updateRule )
!**********************************************************************
!**********************************************************************
  class(fusionRules_type),intent(out) :: rules
  integer                ,intent(in ) :: Nmult
  procedure(compatible_interface),optional :: compatible
  procedure(updateRule_interface),optional :: updateRule
  integer :: del(Nmult,Nmult)
  integer :: nn,i1,j1,i2,j2,i3,j3,i4,j4 ,k1,k2,k3,k4
  integer :: n1,n2,n3,n123,n231,n312,n321,n213,n132
  type(particleList_type) :: particles
  type(interactionList_type) :: vertices
  character(7) :: symbol
!
  call init_mathcnst
  call init_colorflow
  small = epsilon(small)*2048
!
  if (Nmult.gt.Nmax) then
    write(errru,*) 'ERROR in colorflow: ' &
     ,'increase the parameter Nmax to at least ',prnt(Nmult,'lft')
    stop
  endif
!
! Kronecker delta
  del = 0
  do i1=1,Nmult
    del(i1,i1) = 1
  enddo
!
! "particles"
  do i1=0,Nmult
  do j1=0,Nmult
    symbol = '('//trim(prnt(i1,'lft'))//','//trim(prnt(j1,'lft'))//')'
    call particles%add( map(i1,j1) ,trim(symbol) ,map(j1,i1) )
  enddo
  enddo
!
! quark-quark-gluon
  do i1=1,Nmult
  do j2=1,Nmult
  do i3=1,Nmult
  do j3=1,Nmult
    nn = Ncolor(1)*del(i1,j3)*del(i3,j2) - del(i1,j2)*del(i3,j3)
    if (nn.eq.0) cycle
    call vertices%add( [map(i1,0),map(0,j2),map(i3,j3)] ,get_ref(nn*rONE/Ncolor(1),rZRO,rZRO) ,label=qqg )
  enddo
  enddo
  enddo
  enddo
!
! quark-quark-singlet
  do i1=1,Nmult
  do j2=1,Nmult
    nn = del(i1,j2)
    if (nn.eq.0) cycle
    call vertices%add( [map(i1,0),map(0,j2),map(0,0)] ,get_ref(1,0,0) ,label=qqs)
  enddo
  enddo
!
! only singlet
  call vertices%add( [map(0,0),map(0,0),map(0,0)]          ,get_ref(1,0,0) ,label=sss )
  call vertices%add( [map(0,0),map(0,0),map(0,0),map(0,0)] ,get_ref(1,0,0) ,label=ssss )
!
! singlet-2gluon
  do k1=nul+1,nul+(1+Nmult)**2;i1=pam(1,k1);if(i1.eq.0)cycle;j1=pam(2,k1);if(j1.eq.0)cycle
  do k2=   k1,nul+(1+Nmult)**2;i2=pam(1,k2);if(i2.eq.0)cycle;j2=pam(2,k2);if(j2.eq.0)cycle
    nn = del(i1,j2)*del(i2,j1)
    if (nn.eq.0) cycle
    call vertices%add( [map(0,0),k1,k2] ,get_ref(nn,0,0) ,label=s2g )
  enddo
  enddo
!
! 3-gluon
  do k1=nul+1,nul+(1+Nmult)**2;i1=pam(1,k1);if(i1.eq.0)cycle;j1=pam(2,k1);if(j1.eq.0)cycle
  do k2=   k1,nul+(1+Nmult)**2;i2=pam(1,k2);if(i2.eq.0)cycle;j2=pam(2,k2);if(j2.eq.0)cycle
  do k3=   k2,nul+(1+Nmult)**2;i3=pam(1,k3);if(i3.eq.0)cycle;j3=pam(2,k3);if(j3.eq.0)cycle
    n1 = del(i1,j2)*del(i2,j3)*del(i3,j1)
    n2 = del(i2,j1)*del(i1,j3)*del(i3,j2)
    if (n1.eq.n2) cycle
    call vertices%add( [k1,k2,k3] ,get_ref(n1,n2,0) ,label=ggg )
    call vertices%add( [map(0,0),k1,k2,k3] ,get_ref(n1,n2,0) ,label=s3g )
  enddo
  enddo
  enddo
!
! 4-gluon
  do k1=nul+1,nul+(1+Nmult)**2;i1=pam(1,k1);if(i1.eq.0)cycle;j1=pam(2,k1);if(j1.eq.0)cycle
  do k2=   k1,nul+(1+Nmult)**2;i2=pam(1,k2);if(i2.eq.0)cycle;j2=pam(2,k2);if(j2.eq.0)cycle
  do k3=   k2,nul+(1+Nmult)**2;i3=pam(1,k3);if(i3.eq.0)cycle;j3=pam(2,k3);if(j3.eq.0)cycle
  do k4=   k3,nul+(1+Nmult)**2;i4=pam(1,k4);if(i4.eq.0)cycle;j4=pam(2,k4);if(j4.eq.0)cycle
    n123 = del(i1,j2)*del(i2,j3)*del(i3,j4)*del(i4,j1)
    n231 = del(i2,j3)*del(i3,j1)*del(i1,j4)*del(i4,j2)
    n312 = del(i3,j1)*del(i1,j2)*del(i2,j4)*del(i4,j3)
    n321 = del(i3,j2)*del(i2,j1)*del(i1,j4)*del(i4,j3)
    n213 = del(i2,j1)*del(i1,j3)*del(i3,j4)*del(i4,j2)
    n132 = del(i1,j3)*del(i3,j2)*del(i2,j4)*del(i4,j1)
    n1 =  -n123 -   n231 + 2*n312 -   n321 + 2*n213 -   n132 ! (14)(23)
    n2 = 2*n123 -   n231 -   n312 + 2*n321 -   n213 -   n132 ! (24)(13)
    n3 =  -n123 + 2*n231 -   n312 -   n321 -   n213 + 2*n132 ! (34)(12)
    if (n1.eq.0.and.n2.eq.0.and.n3.eq.0) cycle
    call vertices%add( [k1,k2,k3,k4] ,get_ref(n1,n2,n3) ,label=gggg )
    call vertices%add( [map(0,0),k1,k2,k3,k4] ,get_ref(n1,n2,n3) ,label=s4g )
  enddo
  enddo
  enddo
  enddo
!
  if (present(updateRule)) then
    if (present(compatible)) then
      call rules%fill( particles ,vertices &
                      ,vertex=get_vertex ,combine=combine_vertex ,xternal=is_external &
                      ,compatible=compatible ,rule=updateRule )
    else
      call rules%fill( particles ,vertices &
                      ,vertex=get_vertex ,combine=combine_vertex ,xternal=is_external &
                      ,rule=updateRule )
    endif
  elseif (present(compatible)) then
    call rules%fill( particles ,vertices &
                    ,vertex=get_vertex ,combine=combine_vertex ,xternal=is_external &
                    ,compatible=compatible )
  else
    call rules%fill( particles ,vertices &
                    ,vertex=get_vertex ,combine=combine_vertex ,xternal=is_external )
  endif
!
  end subroutine


  subroutine get_vertex( obj ,vertexLabel,cnstRef ,f,xtrn )
!***********************************************************************
! Get vertexLabel and coupling, and identify "photon"-vertex.
!***********************************************************************
  class(fusionRules_type) :: obj
  integer,intent(out  ) :: vertexLabel
  integer,intent(out  ) :: cnstRef
  integer,intent(in   ) :: f(0:)
  logical,intent(in   ) :: xtrn(0:)
  !(complex2!) :: coupling(3)
  integer :: vertRef,iPerm,cnstRefLoc
  vertRef = obj%Reference(f(2),obj%Reference(f(1),obj%Reference(f(0),0)))
  if (f(3).eq.nul) then
    if (obj%Interaction(vertRef)%Label.eq.ggg) then
      iPerm = tab_3g(sort_3(f(0:2)))
    else
      iPerm = 1
    endif
    cnstRefLoc = obj%Interaction(vertRef)%CnstRef
    coupling(1) = cplxcnst(perm_a_few(1,iPerm),cnstRefLoc)
    coupling(2) = cplxcnst(perm_a_few(2,iPerm),cnstRefLoc)
    coupling(3) = 0
  elseif (f(4).eq.nul) then          
    vertRef = obj%Reference(f(3),vertRef)     
    cnstRefLoc = obj%Interaction(vertRef)%CnstRef
    if (obj%Interaction(vertRef)%Label.eq.gggg) then
      iPerm = tab_4g(sort_4(f(0:3)))
      coupling(1) = cplxcnst(perm_a_few(1,iPerm),cnstRefLoc)
      coupling(2) = cplxcnst(perm_a_few(2,iPerm),cnstRefLoc)
      coupling(3) = cplxcnst(perm_a_few(3,iPerm),cnstRefLoc)
    else! label=s3g
      iPerm = tab_s3g(sort_4(f(0:3)))
      coupling(1) = cplxcnst(perm_a_few(1,iPerm),cnstRefLoc)
      coupling(2) = cplxcnst(perm_a_few(2,iPerm),cnstRefLoc)
      coupling(3) = 0
    endif
  else! label=s4g
    vertRef = obj%Reference(f(4),obj%Reference(f(3),vertRef))    
    cnstRefLoc = obj%Interaction(vertRef)%CnstRef
    iPerm = tab_s4g(sort_5(f(0:4)))
    coupling(1) = cplxcnst(perm_a_few(1,iPerm),cnstRefLoc)
    coupling(2) = cplxcnst(perm_a_few(2,iPerm),cnstRefLoc)
    coupling(3) = cplxcnst(perm_a_few(3,iPerm),cnstRefLoc)
  endif
  vertexLabel = obj%Interaction(vertRef)%Label
  if (vertexLabel.eq.qqg) then
    if (    (pam(1,f(0)).eq.0.and.pam(2,f(1)).eq.0.and.pam(2,f(0)).eq.pam(1,f(1)).and..not.xtrn(2)) &
        .or.(pam(2,f(0)).eq.0.and.pam(1,f(1)).eq.0.and.pam(1,f(0)).eq.pam(2,f(1)).and..not.xtrn(2)) &
        .or.(pam(1,f(0)).eq.0.and.pam(2,f(2)).eq.0.and.pam(2,f(0)).eq.pam(1,f(2)).and..not.xtrn(1)) &
        .or.(pam(2,f(0)).eq.0.and.pam(1,f(2)).eq.0.and.pam(1,f(0)).eq.pam(2,f(2)).and..not.xtrn(1)) &
        .or.(pam(1,f(0)).eq.pam(2,f(0)).and..not.xtrn(0)) ) then
      coupling = [-cIMA/sqrt(rONE*Ncolor(1)),cZRO,cZRO]
      vertexLabel = qqa
    endif
  endif
  cnstRef = get_ref( coupling )
  end subroutine


  subroutine combine_vertex( obj ,vLab0,cnstRef0 ,vLab1,cnstRef1 ,vLab2,cnstRef2 )
!***********************************************************************
!***********************************************************************
  class(fusionRules_type) :: obj
  integer,intent(out) :: vLab0
  integer,intent(out) :: cnstRef0
  integer,intent(in) :: vLab1,vLab2
  integer,intent(in) :: cnstRef1,cnstRef2
  !(complex2!) :: coup0(3),coup1(3),coup2(3)
  !(realknd2!) :: eps
  vLab0 = 0
  coup0(1:3) = 0
  coup1(1:3) = cplxcnst(1:3,cnstRef1)
  coup2(1:3) = cplxcnst(1:3,cnstRef2)
  if (vLab1.ne.vLab2) return
  if (vLab1.eq.qqa.or.vLab1.eq.s2g) then
!  if (vLab1.eq.qqa) then
    vLab0 = vLab1
    coup0 = coup1
    cnstRef0 = get_ref( coup0 )
    return
  endif
  eps = sum(abs(coup1(1:3))) + sum(abs(coup2(1:3)))
  eps = eps*small
  coup0(1:3) = coup1(1:3) + coup2(1:3)
  if (vLab1.eq.ggg.or.vLab1.eq.s3g) then
    if (abs(coup0(1)-coup0(2)).le.eps) return
  else
    if (abs(coup0(1)).le.eps.and. &
        abs(coup0(2)).le.eps.and. &
        abs(coup0(3)).le.eps      ) return
  endif
  vLab0 = vLab1
  cnstRef0 = get_ref( coup0 )
  end subroutine


  subroutine is_external( obj ,xtrn ,f )
!***********************************************************************
! Determine if currents are external.
! Besides leaves and the root, this is the case for "internal" currents
! that are the result of a gluon with only Higges attached.
! This routine can only change xtrn to true, not to false.
!***********************************************************************
  class(fusionRules_type) :: obj
  logical,intent(inout) :: xtrn(0:)
  integer,intent(in   ) :: f(0:)
  integer :: vertRef
  if (f(3).eq.nul) then
    vertRef = obj%Reference(f(2),obj%Reference(f(1),obj%Reference(f(0),0)))
    if (obj%Interaction(vertRef)%Label.eq.s2g) then
      if (all(pam(:,f(1)).eq.0).and.xtrn(2).or.all(pam(:,f(2)).eq.0).and.xtrn(1)) then
        xtrn(0) = .true.
      endif
      if (all(pam(:,f(2)).eq.0).and.xtrn(0).or.all(pam(:,f(0)).eq.0).and.xtrn(2)) then
        xtrn(1) = .true.
      endif
      if (all(pam(:,f(0)).eq.0).and.xtrn(1).or.all(pam(:,f(1)).eq.0).and.xtrn(0)) then
        xtrn(2) = .true.
      endif
    endif
  endif
  end subroutine

end module


