#!/usr/bin/env python
#begin header
import os,sys
pythonDir = ''
sys.path.append(pythonDir)
from avh import *
thisDir,packDir,avhDir = cr.dirsdown(2)
packageName = os.path.basename(thisDir)
buildDir = os.path.join(avhDir,'build')
#end header

date = '01-03-2021'

QPyes = False
for ii in range(0,len(sys.argv)):
    if sys.argv[ii]=='-QP': QPyes = True

lines = ['! From here the package "'+packageName+'", last edit: '+date+'\n']

cr.addfile(os.path.join(thisDir,'avh_colorflow.f90'),lines,delPattern="^.*")

linesDP = []
cr.addfile(os.path.join(thisDir,'avh_fill_colorflow.f90'),linesDP,delPattern="^.*")
cr.addfile(os.path.join(thisDir,'avh_coloredlgn.f90'),linesDP,delPattern="^.*")
cr.addfile(os.path.join(thisDir,'avh_colormatrix.f90'),linesDP,delPattern="^.*")
fpp.incl(thisDir,linesDP)
fpp.xpnd('realknd2','real(kind(1d0))',linesDP)
fpp.xpnd('complex2','complex(kind(1d0))',linesDP)
lines = lines+linesDP

if QPyes:
    linesQP = []
    cr.addfile(os.path.join(thisDir,'avh_fill_colorflow.f90'),linesQP,delPattern="^.*")
    cr.addfile(os.path.join(thisDir,'avh_coloredlgn.f90'),linesQP,delPattern="^.*")
    fpp.incl(thisDir,linesQP)
    fpp.xpnd('realknd2','real(kind(1q0))',linesQP)
    fpp.xpnd('complex2','complex(kind(1q0))',linesQP)
    ed.replace_string('avh_mathcnst','avh_qp_mathcnst',linesQP)
    ed.replace_string('avh_fusiontools','avh_qp_fusiontools',linesQP)
    ed.replace_string('avh_buildskel','avh_qp_buildskel',linesQP)
    ed.replace_string('avh_fill_colorflow','avh_qp_fill_colorflow',linesQP)
    ed.replace_string('avh_coloredlgn','avh_qp_coloredlgn',linesQP)
    lines = lines+linesQP

cr.addfile(os.path.join(thisDir,'avh_rancol.f90'),lines,delPattern="^.*")

fpp.incl(thisDir,lines)

cr.wfile(os.path.join(buildDir,packageName+'.f90'),lines)
