module avh_doloops
  implicit none
  private
  public :: permutation,tumrep,permfun
  public :: igamm,perm_a_few
  public :: sort_3,sort_4,sort_5

  integer,parameter :: igamm(1:13)=[1,1,2,6,24,120,720,5040,40320 &
                                ,362880,3628800,39916800,479001600]

  integer,parameter :: perm_a_few(5,120)=reshape( &
    [1,2,3,4,5 ,2,1,3,4,5 ,2,3,1,4,5 ,3,2,1,4,5 ,3,1,2,4,5 ,1,3,2,4,5 &
    ,2,3,4,1,5 ,3,2,4,1,5 ,3,4,2,1,5 ,4,3,2,1,5 ,4,2,3,1,5 ,2,4,3,1,5 &
    ,3,4,1,2,5 ,4,3,1,2,5 ,4,1,3,2,5 ,1,4,3,2,5 ,1,3,4,2,5 ,3,1,4,2,5 &
    ,4,1,2,3,5 ,1,4,2,3,5 ,1,2,4,3,5 ,2,1,4,3,5 ,2,4,1,3,5 ,4,2,1,3,5 &
    ,2,3,4,5,1 ,3,2,4,5,1 ,3,4,2,5,1 ,4,3,2,5,1 ,4,2,3,5,1 ,2,4,3,5,1 &
    ,3,4,5,2,1 ,4,3,5,2,1 ,4,5,3,2,1 ,5,4,3,2,1 ,5,3,4,2,1 ,3,5,4,2,1 &
    ,4,5,2,3,1 ,5,4,2,3,1 ,5,2,4,3,1 ,2,5,4,3,1 ,2,4,5,3,1 ,4,2,5,3,1 &
    ,5,2,3,4,1 ,2,5,3,4,1 ,2,3,5,4,1 ,3,2,5,4,1 ,3,5,2,4,1 ,5,3,2,4,1 &
    ,3,4,5,1,2 ,4,3,5,1,2 ,4,5,3,1,2 ,5,4,3,1,2 ,5,3,4,1,2 ,3,5,4,1,2 &
    ,4,5,1,3,2 ,5,4,1,3,2 ,5,1,4,3,2 ,1,5,4,3,2 ,1,4,5,3,2 ,4,1,5,3,2 &
    ,5,1,3,4,2 ,1,5,3,4,2 ,1,3,5,4,2 ,3,1,5,4,2 ,3,5,1,4,2 ,5,3,1,4,2 &
    ,1,3,4,5,2 ,3,1,4,5,2 ,3,4,1,5,2 ,4,3,1,5,2 ,4,1,3,5,2 ,1,4,3,5,2 &
    ,4,5,1,2,3 ,5,4,1,2,3 ,5,1,4,2,3 ,1,5,4,2,3 ,1,4,5,2,3 ,4,1,5,2,3 &
    ,5,1,2,4,3 ,1,5,2,4,3 ,1,2,5,4,3 ,2,1,5,4,3 ,2,5,1,4,3 ,5,2,1,4,3 &
    ,1,2,4,5,3 ,2,1,4,5,3 ,2,4,1,5,3 ,4,2,1,5,3 ,4,1,2,5,3 ,1,4,2,5,3 &
    ,2,4,5,1,3 ,4,2,5,1,3 ,4,5,2,1,3 ,5,4,2,1,3 ,5,2,4,1,3 ,2,5,4,1,3 &
    ,5,1,2,3,4 ,1,5,2,3,4 ,1,2,5,3,4 ,2,1,5,3,4 ,2,5,1,3,4 ,5,2,1,3,4 &
    ,1,2,3,5,4 ,2,1,3,5,4 ,2,3,1,5,4 ,3,2,1,5,4 ,3,1,2,5,4 ,1,3,2,5,4 &
    ,2,3,5,1,4 ,3,2,5,1,4 ,3,5,2,1,4 ,5,3,2,1,4 ,5,2,3,1,4 ,2,5,3,1,4 &
    ,3,5,1,2,4 ,5,3,1,2,4 ,5,1,3,2,4 ,1,5,3,2,4 ,1,3,5,2,4 ,3,1,5,2,4] &
    , [5,120] )


contains

  
  subroutine permutation( per,nn ,iper )
!********************************************************************
! Returns the  iper-th  permutation of nn objects, where the listing
! is such that  per(1:nn)  is the same in 
!   call permutation( per,nn   ,iper )
!   call permutation( per,nn+1 ,iper )
!   call permutation( per,nn+2 ,iper )
!   etc.
! For the complete list of permutations, use  permut_type
!********************************************************************
  intent(out) :: per
  intent(in ) :: nn,iper
  integer :: nn,iper
  integer :: per(nn),cyc(13),tmp(13),jper,ii,jj,kk
  integer,parameter :: unity(1:13)=(/1,2,3,4,5,6,7,8,9,10,11,12,13/)
  per(1:nn) = unity(1:nn)
!
  jper = iper-1
  do ii=nn,2,-1
    if (jper.lt.igamm(ii)) then
      cyc(ii) = 0
    else
      cyc(ii) = jper/igamm(ii)
      jper    = jper-cyc(ii)*igamm(ii)
    endif
  enddo
!
  do ii=nn,2,-1
    do jj=1,ii
      kk = jj-cyc(ii)
      if (kk.le.0) kk = kk+ii
      tmp(kk) = per(jj)
    enddo
    per(1:ii) = tmp(1:ii)
  enddo
  end subroutine

  function permfun(nn,iper) result(per)
  integer,intent(in) :: nn,iper
  integer :: per(nn)
  call permutation( per ,nn ,iper )
  end function

  function tumrep(per,nn) result(iper)
!********************************************************************
! iper=tumrep(per,nn)  is the integer that leads to  per  if used in
!    call permutation( per,nn ,iper )
!********************************************************************
  intent(in) :: per,nn
  integer :: nn,iper,ii,jj,kk
  integer :: per(nn),pmt(13),tmp(13),cyc(13),tmq(13),qmt(13)
!
  do ii=1,nn
    pmt(ii)      = per(ii)
    tmp(pmt(ii)) = ii
  enddo
  do ii=nn,2,-1
    cyc(ii) = pmt(ii)
    if (cyc(ii).eq.ii) cyc(ii) = 0
    do jj=1,ii
      kk = jj-cyc(ii)
      if (kk.le.0) kk = kk+ii
      tmq(kk) = tmp(jj)
      qmt(jj) = pmt(jj)-cyc(ii)
      if (qmt(jj).le.0) qmt(jj) = qmt(jj)+ii
    enddo
    do jj=1,ii
      tmp(jj) = tmq(jj)
      pmt(jj) = qmt(jj)
    enddo
  enddo
!
  iper = 1
  do ii=2,nn
    iper = iper + cyc(ii)*igamm(ii)
  enddo
  end function


  function sort_2(i) result(rslt)
!***********************************************************************
! Hardwired optimal decision tree.
!***********************************************************************
  integer,intent(in) :: i(2)
  integer :: rslt
  if(i(1).le.i(2))then
    rslt = 1
  else
    rslt = 2
  endif
  end function


  function sort_3(i) result(rslt)
!***********************************************************************
! Hardwired optimal decision tree.
!***********************************************************************
  integer,intent(in) :: i(3)
  integer :: rslt
  if(i(1).le.i(2))then;if(i(2).le.i(3))then
    rslt = 1 ! 123
  else;if(i(1).le.i(3))then
    rslt = 6 ! 132
  else
    rslt = 3 ! 231
  endif;endif;else;if(i(1).le.i(3))then
    rslt = 2 ! 213
  else if(i(2).le.i(3))then
    rslt = 5 ! 312
  else
    rslt = 4 ! 321
  endif;endif
  end function


  function sort_4(i) result(rslt)
!***********************************************************************
! Hardwired decision tree following insertion sort.
! Maximum number of comparisons is not optimal (6 instead of 5),
! but average number of comparisions is less. 
!***********************************************************************
  integer,intent(in) :: i(4)
  integer :: rslt
  !character(1) :: rslt
  if(i(1).le.i(2))then;if(i(2).le.i(3))then;if(i(3).le.i(4))then
    rslt = 1
  elseif(i(2).le.i(4))then
    rslt = 21
  elseif(i(1).le.i(4))then
    rslt = 17
  else
    rslt = 7
  endif;elseif(i(1).le.i(3))then;if(i(2).le.i(4))then
    rslt = 6
  elseif(i(3).le.i(4))then
    rslt = 20
  elseif(i(1).le.i(4))then
    rslt = 16
  else
    rslt = 12
  endif;else;if(i(2).le.i(4))then
    rslt = 3
  elseif(i(1).le.i(4))then
    rslt = 23
  elseif(i(3).le.i(4))then
    rslt = 13
  else
    rslt = 9
  endif;endif;else;if(i(1).le.i(3))then;if(i(3).le.i(4))then
    rslt = 2
  elseif(i(1).le.i(4))then
    rslt = 22
  elseif(i(2).le.i(4))then
    rslt = 18
  else
    rslt = 8
  endif;elseif(i(2).le.i(3))then;if(i(1).le.i(4))then
    rslt = 5
  elseif(i(3).le.i(4))then
    rslt = 19
  elseif(i(2).le.i(4))then
    rslt = 15
  else
    rslt = 11
  endif;else;if(i(1).le.i(4))then
    rslt = 4
  elseif(i(2).le.i(4))then
    rslt = 24
  elseif(i(3).le.i(4))then
    rslt = 14
  else
    rslt = 10
  endif;endif;endif
  end function


  function sort_5(i) result(rslt)
!***********************************************************************
! Hardwired decision tree following insertion sort.
! Maximum number of comparisons is not optimal (10 instead of 9),
! but average number of comparisions is less. 
!***********************************************************************
  integer,intent(in) :: i(5)
  integer :: rslt
  !character(4) :: rslt
  if(i(1).le.i(2))then;if(i(2).le.i(3))then;if(i(3).le.i(4))then;if(i(4).le.i(5))then
    rslt = 1
  elseif(i(3).le.i(5))then
    rslt = 103
  elseif(i(2).le.i(5))then
    rslt = 85
  elseif(i(1).le.i(5))then
    rslt = 67
  else
    rslt = 25
  endif;elseif(i(2).le.i(4))then;if(i(3).le.i(5))then
    rslt = 21
  elseif(i(4).le.i(5))then
    rslt = 99
  elseif(i(2).le.i(5))then
    rslt = 81
  elseif(i(1).le.i(5))then
    rslt = 63
  else
    rslt = 45
  endif;elseif(i(1).le.i(4))then;if(i(3).le.i(5))then
    rslt = 17
  elseif(i(2).le.i(5))then
    rslt = 119
  elseif(i(4).le.i(5))then
    rslt = 77
  elseif(i(1).le.i(5))then
    rslt = 59
  else
    rslt = 41
  endif;else;if(i(3).le.i(5))then
    rslt = 7
  elseif(i(2).le.i(5))then
    rslt = 109
  elseif(i(1).le.i(5))then
    rslt = 91
  elseif(i(4).le.i(5))then
    rslt = 49
  else
    rslt = 31
  endif;endif;elseif(i(1).le.i(3))then;if(i(2).le.i(4))then;if(i(4).le.i(5))then
    rslt = 6
  elseif(i(2).le.i(5))then
    rslt = 108
  elseif(i(3).le.i(5))then
    rslt = 90
  elseif(i(1).le.i(5))then
    rslt = 72
  else
    rslt = 30
  endif;elseif(i(3).le.i(4))then;if(i(2).le.i(5))then
    rslt = 20
  elseif(i(4).le.i(5))then
    rslt = 98
  elseif(i(3).le.i(5))then
    rslt = 80
  elseif(i(1).le.i(5))then
    rslt = 62
  else
    rslt = 44
  endif;elseif(i(1).le.i(4))then;if(i(2).le.i(5))then
    rslt = 16
  elseif(i(3).le.i(5))then
    rslt = 118
  elseif(i(4).le.i(5))then
    rslt = 76
  elseif(i(1).le.i(5))then
    rslt = 58
  else
    rslt = 40
  endif;else;if(i(2).le.i(5))then
    rslt = 12
  elseif(i(3).le.i(5))then
    rslt = 114
  elseif(i(1).le.i(5))then
    rslt = 96
  elseif(i(4).le.i(5))then
    rslt = 54
  else
    rslt = 36
  endif;endif;else;if(i(2).le.i(4))then;if(i(4).le.i(5))then
    rslt = 3
  elseif(i(2).le.i(5))then
    rslt = 105
  elseif(i(1).le.i(5))then
    rslt = 87
  elseif(i(3).le.i(5))then
    rslt = 69
  else
    rslt = 27
  endif;elseif(i(1).le.i(4))then;if(i(2).le.i(5))then
    rslt = 23
  elseif(i(4).le.i(5))then
    rslt = 101
  elseif(i(1).le.i(5))then
    rslt = 83
  elseif(i(3).le.i(5))then
    rslt = 65
  else
    rslt = 47
  endif;elseif(i(3).le.i(4))then;if(i(2).le.i(5))then
    rslt = 13
  elseif(i(1).le.i(5))then
    rslt = 115
  elseif(i(4).le.i(5))then
    rslt = 73
  elseif(i(3).le.i(5))then
    rslt = 55
  else
    rslt = 37
  endif;else;if(i(2).le.i(5))then
    rslt = 9
  elseif(i(1).le.i(5))then
    rslt = 111
  elseif(i(3).le.i(5))then
    rslt = 93
  elseif(i(4).le.i(5))then
    rslt = 51
  else
    rslt = 33
  endif;endif;endif;else;if(i(1).le.i(3))then;if(i(3).le.i(4))then;if(i(4).le.i(5))then
    rslt = 2
  elseif(i(3).le.i(5))then
    rslt = 104
  elseif(i(1).le.i(5))then
    rslt = 86
  elseif(i(2).le.i(5))then
    rslt = 68
  else
    rslt = 26
  endif;elseif(i(1).le.i(4))then;if(i(3).le.i(5))then
    rslt = 22
  elseif(i(4).le.i(5))then
    rslt = 100
  elseif(i(1).le.i(5))then
    rslt = 82
  elseif(i(2).le.i(5))then
    rslt = 64
  else
    rslt = 46
  endif;elseif(i(2).le.i(4))then;if(i(3).le.i(5))then
    rslt = 18
  elseif(i(1).le.i(5))then
    rslt = 120
  elseif(i(4).le.i(5))then
    rslt = 78
  elseif(i(2).le.i(5))then
    rslt = 60
  else
    rslt = 42
  endif;else;if(i(3).le.i(5))then
    rslt = 8
  elseif(i(1).le.i(5))then
    rslt = 110
  elseif(i(2).le.i(5))then
    rslt = 92
  elseif(i(4).le.i(5))then
    rslt = 50
  else
    rslt = 32
  endif;endif;elseif(i(2).le.i(3))then;if(i(1).le.i(4))then;if(i(4).le.i(5))then
    rslt = 5
  elseif(i(1).le.i(5))then
    rslt = 107
  elseif(i(3).le.i(5))then
    rslt = 89
  elseif(i(2).le.i(5))then
    rslt = 71
  else
    rslt = 29
  endif;elseif(i(3).le.i(4))then;if(i(1).le.i(5))then
    rslt = 19
  elseif(i(4).le.i(5))then
    rslt = 97
  elseif(i(3).le.i(5))then
    rslt = 79
  elseif(i(2).le.i(5))then
    rslt = 61
  else
    rslt = 43
  endif;elseif(i(2).le.i(4))then;if(i(1).le.i(5))then
    rslt = 15
  elseif(i(3).le.i(5))then
    rslt = 117
  elseif(i(4).le.i(5))then
    rslt = 75
  elseif(i(2).le.i(5))then
    rslt = 57
  else
    rslt = 39
  endif;else;if(i(1).le.i(5))then
    rslt = 11
  elseif(i(3).le.i(5))then
    rslt = 113
  elseif(i(2).le.i(5))then
    rslt = 95
  elseif(i(4).le.i(5))then
    rslt = 53
  else
    rslt = 35
  endif;endif;else;if(i(1).le.i(4))then;if(i(4).le.i(5))then
    rslt = 4
  elseif(i(1).le.i(5))then
    rslt = 106
  elseif(i(2).le.i(5))then
    rslt = 88
  elseif(i(3).le.i(5))then
    rslt = 70
  else
    rslt = 28
  endif;elseif(i(2).le.i(4))then;if(i(1).le.i(5))then
    rslt = 24
  elseif(i(4).le.i(5))then
    rslt = 102
  elseif(i(2).le.i(5))then
    rslt = 84
  elseif(i(3).le.i(5))then
    rslt = 66
  else
    rslt = 48
  endif;elseif(i(3).le.i(4))then;if(i(1).le.i(5))then
    rslt = 14
  elseif(i(2).le.i(5))then
    rslt = 116
  elseif(i(4).le.i(5))then
    rslt = 74
  elseif(i(3).le.i(5))then
    rslt = 56
  else
    rslt = 38
  endif;else;if(i(1).le.i(5))then
    rslt = 10
  elseif(i(2).le.i(5))then
    rslt = 112
  elseif(i(3).le.i(5))then
    rslt = 94
  elseif(i(4).le.i(5))then
    rslt = 52
  else
    rslt = 34
  endif;endif;endif;endif
  end function

  

end module


program testsort
  use avh_doloops
  implicit none
  integer,parameter :: nn=3
  integer :: ii,perm(5)
  do ii=1,igamm(nn+1)
    call permutation( perm ,nn ,ii )
    select case (nn)
      case (3) ;write(*,'(i4,1x,i1,1x,9i1)') ii,sort_3(perm),perm(1:nn)
      case (4) ;write(*,'(i4,1x,i2,1x,9i1)') ii,sort_4(perm),perm(1:nn)
      case (5) ;write(*,'(i4,1x,i3,1x,9i1)') ii,sort_5(perm),perm(1:nn)
    end select
  enddo
end program
