module avh_doloops
  implicit none
  private
  public :: indep_type,symmet_type,grepen_type,permut_type
  public :: kuijf_init,kuijf_exit
  public :: permutation,tumrep,permfun
  public :: igamm,perm_a_few,sort_2,sort_3,sort_4,sort_5
  public :: symmet_size,symmet_table

  type :: indep_type
    private
    integer :: n,var(13),low(13),upp(13)
  contains
    procedure :: indep_init_0
    procedure :: indep_init_1
    generic :: init=>indep_init_0,indep_init_1
    procedure :: exit=>indep_exit
  end type

  type :: symmet_type
    private
    integer :: n,var(13),upp
  contains
    procedure :: init=>symmet_init
    procedure :: exit=>symmet_exit
  end type

  type :: grepen_type
    private
    integer :: n,var(13),upp
  contains
    procedure :: init=>grepen_init
    procedure :: exit=>grepen_exit
  end type

  type :: permut_type
    private
    integer :: n,per(13),rep(13),cyc(13)
  contains
    procedure :: init=>permut_init
    procedure :: exit=>permut_exit
  end type

  integer,parameter :: igamm(1:13)=[1,1,2,6,24,120,720,5040,40320 &
                                ,362880,3628800,39916800,479001600]

! symmet_size(Nr,Nd) = -1 + (Nr+1)/1 * (Nr+2)/2 *...* (Nr+Nd)/Nd
  integer,parameter :: symmet_size(0:13,1:6)=&
    reshape( &
     [ 0,1, 2, 3,  4,  5,  6,   7,   8,   9,  10,   11,   12,   13 &
      ,0,2, 5, 9, 14, 20, 27,  35,  44,  54,  65,   77,   90,  104 &
      ,0,3, 9,19, 34, 55, 83, 119, 164, 219, 285,  363,  454,  559 &
      ,0,4,14,34, 69,125,209, 329, 494, 714,1000, 1364, 1819, 2379 &
      ,0,5,20,55,125,251,461, 791,1286,2001,3002, 4367, 6187, 8567 &
      ,0,6,27,83,209,461,923,1715,3002,5004,8007,12375,18563,27131] &
     ,[14,6] )

  integer,parameter :: perm_a_few(5,120)=reshape( &
    [1,2,3,4,5 ,2,1,3,4,5 ,2,3,1,4,5 ,3,2,1,4,5 ,3,1,2,4,5 ,1,3,2,4,5 &
    ,2,3,4,1,5 ,3,2,4,1,5 ,3,4,2,1,5 ,4,3,2,1,5 ,4,2,3,1,5 ,2,4,3,1,5 &
    ,3,4,1,2,5 ,4,3,1,2,5 ,4,1,3,2,5 ,1,4,3,2,5 ,1,3,4,2,5 ,3,1,4,2,5 &
    ,4,1,2,3,5 ,1,4,2,3,5 ,1,2,4,3,5 ,2,1,4,3,5 ,2,4,1,3,5 ,4,2,1,3,5 &
    ,2,3,4,5,1 ,3,2,4,5,1 ,3,4,2,5,1 ,4,3,2,5,1 ,4,2,3,5,1 ,2,4,3,5,1 &
    ,3,4,5,2,1 ,4,3,5,2,1 ,4,5,3,2,1 ,5,4,3,2,1 ,5,3,4,2,1 ,3,5,4,2,1 &
    ,4,5,2,3,1 ,5,4,2,3,1 ,5,2,4,3,1 ,2,5,4,3,1 ,2,4,5,3,1 ,4,2,5,3,1 &
    ,5,2,3,4,1 ,2,5,3,4,1 ,2,3,5,4,1 ,3,2,5,4,1 ,3,5,2,4,1 ,5,3,2,4,1 &
    ,3,4,5,1,2 ,4,3,5,1,2 ,4,5,3,1,2 ,5,4,3,1,2 ,5,3,4,1,2 ,3,5,4,1,2 &
    ,4,5,1,3,2 ,5,4,1,3,2 ,5,1,4,3,2 ,1,5,4,3,2 ,1,4,5,3,2 ,4,1,5,3,2 &
    ,5,1,3,4,2 ,1,5,3,4,2 ,1,3,5,4,2 ,3,1,5,4,2 ,3,5,1,4,2 ,5,3,1,4,2 &
    ,1,3,4,5,2 ,3,1,4,5,2 ,3,4,1,5,2 ,4,3,1,5,2 ,4,1,3,5,2 ,1,4,3,5,2 &
    ,4,5,1,2,3 ,5,4,1,2,3 ,5,1,4,2,3 ,1,5,4,2,3 ,1,4,5,2,3 ,4,1,5,2,3 &
    ,5,1,2,4,3 ,1,5,2,4,3 ,1,2,5,4,3 ,2,1,5,4,3 ,2,5,1,4,3 ,5,2,1,4,3 &
    ,1,2,4,5,3 ,2,1,4,5,3 ,2,4,1,5,3 ,4,2,1,5,3 ,4,1,2,5,3 ,1,4,2,5,3 &
    ,2,4,5,1,3 ,4,2,5,1,3 ,4,5,2,1,3 ,5,4,2,1,3 ,5,2,4,1,3 ,2,5,4,1,3 &
    ,5,1,2,3,4 ,1,5,2,3,4 ,1,2,5,3,4 ,2,1,5,3,4 ,2,5,1,3,4 ,5,2,1,3,4 &
    ,1,2,3,5,4 ,2,1,3,5,4 ,2,3,1,5,4 ,3,2,1,5,4 ,3,1,2,5,4 ,1,3,2,5,4 &
    ,2,3,5,1,4 ,3,2,5,1,4 ,3,5,2,1,4 ,5,3,2,1,4 ,5,2,3,1,4 ,2,5,3,1,4 &
    ,3,5,1,2,4 ,5,3,1,2,4 ,5,1,3,2,4 ,1,5,3,2,4 ,1,3,5,2,4 ,3,1,5,2,4] &
    , [5,120] )


  interface tumrep
    module procedure tumrep,tumrep_n
  end interface

contains

  subroutine indep_init_0( obj ,nn,var,low,upp )
!*******************************************************************
! Loop over all integer arrays  var(1:nn)
! where  var(j)  runs from  low(j)  to  upp(j)
! usage:
! 
!   call obj%init(nn,var,low,upp)       
!   do
!     write(6,*) var(1:nn)
!     if (obj%exit(var)) exit
!   enddo
! 
!*******************************************************************
  class(indep_type) :: obj
  intent(in ) :: nn,low,upp
  intent(out) :: var
  integer :: nn
  integer :: var(nn),low(nn),upp(nn)
  obj%n = nn
  obj%low(1:obj%n) = low(1:obj%n)
  obj%upp(1:obj%n) = upp(1:obj%n)
  obj%var(1:obj%n) = obj%low(1:obj%n)
  var(1:obj%n) = obj%var(1:obj%n)
  end subroutine 
!
  subroutine indep_init_1( obj ,nn,var,low,upp )
!!*******************************************************************
!! The same as init_0, but with the same limits for each dimension.
!!*******************************************************************
  class(indep_type) :: obj
  intent(in ) :: nn,low,upp
  intent(out) :: var
  integer :: nn
  integer :: var(nn),low,upp
  obj%n = nn
  obj%low(1:obj%n) = low
  obj%upp(1:obj%n) = upp
  obj%var(1:obj%n) = obj%low(1:obj%n)
  var(1:obj%n) = obj%var(1:obj%n)
  end subroutine
!
  function indep_exit( obj ,var ) result(rslt)
  class(indep_type) :: obj
  intent(out) :: var
  integer :: var(obj%n),jj
  logical :: rslt
  rslt = .true.
  do jj=obj%n,1,-1
    if (obj%var(jj).ge.obj%upp(jj)) cycle
    obj%var(jj) = obj%var(jj)+1
    obj%var(jj+1:obj%n) = obj%low(jj+1:obj%n)
    var(1:obj%n) = obj%var(1:obj%n)
    rslt = .false.
    exit
  enddo
  end function 


  subroutine symmet_init( obj ,nn,var,low,upp )
!*******************************************************************
! Loop over all integer arrays  var(1:nn)  restricted such that
!     var(1)<=var(2)<=...<=var(nn)
! where  var(j)  runs from  low  to  upp
! usage:
! 
!   call obj%init(nn,var,low,upp)       
!   do
!     write(6,*) var(1:nn)
!     if (obj%exit(var)) exit
!   enddo
! 
!*******************************************************************
  class(symmet_type) :: obj
  intent(in ) :: nn,low,upp
  intent(out) :: var
  integer :: nn
  integer :: var(nn),low,upp
  obj%n = nn
  obj%upp = upp
  obj%var(1:obj%n) = low
  var(1:obj%n) = obj%var(1:obj%n)
  end subroutine
!
  function symmet_exit( obj ,var ) result(rslt)
  class(symmet_type) :: obj
  intent(out) :: var
  integer :: var(obj%n),jj
  logical :: rslt
  rslt = .true.
  do jj=obj%n,1,-1
    if (obj%var(jj).ge.obj%upp) cycle
    obj%var(jj) = obj%var(jj)+1
    obj%var(jj+1:obj%n) = obj%var(jj)
    var(1:obj%n) = obj%var(1:obj%n)
    rslt = .false.
    exit
  enddo
  end function 
!
  subroutine symmet_table( Ndim,Nmax,table,elbat,symf )
!***********************************************************************
! Example for Ndim=3 and Nmax=2
! n=1: table(1:Ndim,1) = (/1,0,0/)  ;elbat(1,0) = 1
!      table(1:Ndim,2) = (/0,1,0/)  ;elbat(2,0) = 2
!      table(1:Ndim,3) = (/0,0,1/)  ;elbat(3,0) = 3
! n=2: table(1:Ndim,4) = (/2,0,0/)  ;elbat(1,elbat(1,0)) = 4
!      table(1:Ndim,5) = (/1,1,0/)  ;elbat(1,elbat(2,0)) = 5
!      table(1:Ndim,6) = (/1,0,1/)  ;elbat(1,elbat(3,0)) = 6
!      table(1:Ndim,7) = (/0,2,0/)  ;elbat(2,elbat(2,0)) = 7
!      table(1:Ndim,8) = (/0,1,1/)  ;elbat(2,elbat(3,0)) = 8
!      table(1:Ndim,9) = (/0,0,2/)  ;elbat(3,elbat(3,0)) = 9
!***********************************************************************
  intent(in ) :: Ndim,Nmax
  intent(out) :: table,elbat,symf
  optional :: elbat,symf
  integer :: Ndim,Nmax ,counter,ii,jj,nn
  integer :: var(Nmax),lab(Nmax)
  integer :: table(1:Ndim,1:symmet_size(Nmax  ,Ndim))
  integer :: elbat(1:Ndim,0:symmet_size(Nmax-1,Ndim))
  integer :: symf(        1:symmet_size(Nmax  ,Ndim))
  type(symmet_type) :: loop
!
  counter = 0
  do ii=1,Ndim
    counter = counter+1
    table(: ,counter) = 0
    table(ii,counter) = 1
    if (present(elbat)) elbat(ii,0) = counter
    if (present(symf)) symf(ii) = 1
  enddo
!
  do nn=2,Nmax
    call loop%init(nn,var,1,Ndim)
    do
      counter = counter+1
!
      table(:,counter) = 0
      do ii=1,nn
        table(var(ii),counter) = table(var(ii),counter) + 1
      enddo
!
      if (present(elbat)) then
        lab(:) = 0
        do ii=1,nn
        do jj=1,nn
          if (jj.ne.ii) lab(jj) = elbat(var(ii),lab(jj))
        enddo
        enddo
        do jj=1,nn
          elbat(var(jj),lab(jj)) = counter
        enddo
      endif
!
      if (present(symf)) then
        symf(counter) = igamm(1+nn)
        do jj=1,Ndim
          symf(counter) = symf(counter)/igamm(1+table(jj,counter))
        enddo
      endif
!
      if (loop%exit(var)) exit
    enddo
  enddo
!
  end subroutine


  subroutine grepen_init( obj ,nn ,var ,low,upp )
!*******************************************************************
! Loop over all integer arrays  var(1:nn)  restricted such that
!     var(1)<var(2)<...<var(nn)
! where  var(j)  runs from  low  to  upp
! usage:
! 
!   call obj%init(nn,var,low,upp)       
!   do
!     write(6,*) var(1:nn)
!     if (obj%exit(var)) exit
!   enddo
! 
!*******************************************************************
  class(grepen_type) :: obj
  intent(in ) :: nn,low,upp
  intent(out) :: var
  integer :: nn,ii
  integer :: var(nn),low,upp
  obj%n = nn
  obj%upp = upp
  do ii=1,obj%n
    obj%var(ii) = ii+low-1
  enddo
  var(1:obj%n) = obj%var(1:obj%n)
  end subroutine
!
  function grepen_exit(obj,var) result(rslt)
  class(grepen_type) :: obj
  intent(out) :: var
  integer :: var(obj%n),ii,jj
  logical :: rslt
  if (obj%var(obj%n).lt.obj%upp) then
    obj%var(obj%n) = obj%var(obj%n)+1
    var(1:obj%n) = obj%var(1:obj%n)
    rslt = .false.
    return
  endif
  do ii=obj%n-1,1,-1
  if (obj%var(ii).lt.obj%var(ii+1)-1) then
    obj%var(ii) = obj%var(ii)+1
    do jj=ii+1,obj%n
      obj%var(jj) = obj%var(ii)+(jj-ii)
    enddo
    var(1:obj%n) = obj%var(1:obj%n)
    rslt = .false.
    return
  endif
  enddo
  rslt = .true.
  end function


  subroutine kuijf_init( nn ,per,pos ,half )
!******************************************************************************
! Loop over the (nn-1)! non-cyclic permutations of per(2) to per(nn)
! Algorithm by H. Kuijf
! Usage:
!
!      call kuijf_init( nn ,per,pos )
!      do
!        write(6,*) (per(jj),jj=1,nn)
!        if (kuijf_exit( nn ,per,pos )) exit
!      enddo
!
!  per  should be at least of size  nn+1  and  pos  should be at
! least of size  3*nn+3 . The first  nn  entries of  pos  contain
! the inverse of  per :  per(pos(jj)) = jj 
! If optional argument  half='half'  is supplied, the loop only involves
! half of the permutations, those that are inequivalent under reflection.
! DO NOT CHANGE THE ARGUMENTS INSIDE THE LOOP.
!******************************************************************************
  integer ,intent(in) :: nn
  integer ,intent(out) :: per(nn+1),pos(3*nn+3)
  character(*),intent(in),optional :: half
  integer :: jj,n1,n2,n3
  n1 = nn+1
  n2 = n1+nn
  n3 = n2+nn+1
  do jj=1,nn
    per(jj) = jj
    pos(jj) = jj
    pos(n1+jj) = jj-2
    pos(n2+jj) = -1
  enddo
  per(nn+1) = 1
  pos(n3  ) = 0
  pos(n3+1) = 2
  if (present(half)) then
    if (half.eq.'half') pos(n3+1) = 3
  endif
  end subroutine
!
  function kuijf_exit( nn ,per,pos ) result(rslt)
  integer ,intent(in)    :: nn
  integer ,intent(inout) :: per(nn+1),pos(3*nn+3)
  logical :: rslt
  integer :: ii ,ic,hlp ,dir,n1,n2,n3
  n1 = nn+1
  n2 = n1+nn
  n3 = n2+nn+1
  ii = pos(n3)
  if (ii.gt.0) pos(n1+ii) = pos(n1+ii)-1
  ii = nn
  rslt = .true.
  do while (ii.gt.pos(n3+1))
!  do while (ii.gt.2)
!  do while (ii.gt.3)
    dir = pos(n2+ii)
    if (pos(n1+ii).gt.0) then
      ic           = pos(ii)
      hlp          = per(ic+dir)
      per(ic+dir)  = per(ic)
      per(ic)      = hlp
      pos(ii)      = ic+dir
      pos(per(ic)) = ic
      rslt = .false.
      pos(n3) = ii
      exit
    else
      pos(n1+ii) = ii-2
      pos(n2+ii) = -pos(n2+ii)
      ii = ii-1
    endif
  enddo
  end function

  
  subroutine permutation( per,nn ,iper )
!********************************************************************
! Returns the  iper-th  permutation of nn objects, where the listing
! is such that  per(1:nn)  is the same in 
!   call permutation( per,nn   ,iper )
!   call permutation( per,nn+1 ,iper )
!   call permutation( per,nn+2 ,iper )
!   etc.
! For the complete list of permutations, use  permut_type
!********************************************************************
  intent(out) :: per
  intent(in ) :: nn,iper
  integer :: nn,iper
  integer :: per(nn),cyc(13),tmp(13),jper,ii,jj,kk
  integer,parameter :: unity(1:13)=(/1,2,3,4,5,6,7,8,9,10,11,12,13/)
  per(1:nn) = unity(1:nn)
!
  jper = iper-1
  do ii=nn,2,-1
    if (jper.lt.igamm(ii)) then
      cyc(ii) = 0
    else
      cyc(ii) = jper/igamm(ii)
      jper    = jper-cyc(ii)*igamm(ii)
    endif
  enddo
!
  do ii=nn,2,-1
    do jj=1,ii
      kk = jj-cyc(ii)
      if (kk.le.0) kk = kk+ii
      tmp(kk) = per(jj)
    enddo
    per(1:ii) = tmp(1:ii)
  enddo
  end subroutine

  function permfun(nn,iper) result(per)
  integer,intent(in) :: nn,iper
  integer :: per(nn)
  call permutation( per ,nn ,iper )
  end function


  function tumrep_n(per,nn) result(iper)
!********************************************************************
! iper=tumrep(per,nn)  is the integer that leads to  per  if used in
!    call permutation( per,nn ,iper )
!********************************************************************
  integer,intent(in) :: nn
  integer,intent(in) :: per(nn)
  integer :: iper,ii,jj,kk
  integer :: pmt(13),tmp(13),cyc(13),tmq(13),qmt(13)
!
  do ii=1,nn
    pmt(ii)      = per(ii)
    tmp(pmt(ii)) = ii
  enddo
  do ii=nn,2,-1
    cyc(ii) = pmt(ii)
    if (cyc(ii).eq.ii) cyc(ii) = 0
    do jj=1,ii
      kk = jj-cyc(ii)
      if (kk.le.0) kk = kk+ii
      tmq(kk) = tmp(jj)
      qmt(jj) = pmt(jj)-cyc(ii)
      if (qmt(jj).le.0) qmt(jj) = qmt(jj)+ii
    enddo
    do jj=1,ii
      tmp(jj) = tmq(jj)
      pmt(jj) = qmt(jj)
    enddo
  enddo
!
  iper = 1
  do ii=2,nn
    iper = iper + cyc(ii)*igamm(ii)
  enddo
  end function


  function tumrep(per) result(iper)
!********************************************************************
! iper=tumrep(per,nn)  is the integer that leads to  per  if used in
!    call permutation( per,nn ,iper )
!********************************************************************
  integer,intent(in) :: per(:)
  integer :: nn,iper,ii,jj,kk
  integer :: pmt(13),tmp(13),cyc(13),tmq(13),qmt(13)
  nn = size(per)
!
  do ii=1,nn
    pmt(ii)      = per(ii)
    tmp(pmt(ii)) = ii
  enddo
  do ii=nn,2,-1
    cyc(ii) = pmt(ii)
    if (cyc(ii).eq.ii) cyc(ii) = 0
    do jj=1,ii
      kk = jj-cyc(ii)
      if (kk.le.0) kk = kk+ii
      tmq(kk) = tmp(jj)
      qmt(jj) = pmt(jj)-cyc(ii)
      if (qmt(jj).le.0) qmt(jj) = qmt(jj)+ii
    enddo
    do jj=1,ii
      tmp(jj) = tmq(jj)
      pmt(jj) = qmt(jj)
    enddo
  enddo
!
  iper = 1
  do ii=2,nn
    iper = iper + cyc(ii)*igamm(ii)
  enddo
  end function


  subroutine permut_init(obj,nn,per)
!********************************************************************
! The same listing as  subroutine permutation  but slower than
! Kuijf's algorithm
!
!   call obj%init(nn,per)
!   do
!     write(6,*) per(1:nn)
!     if (obj%exit(per)) exit
!   enddo
!
!********************************************************************
  intent(inout) :: obj
  intent(in   ) :: nn
  intent(out  ) :: per
  class(permut_type) :: obj
  integer :: nn,ii,per(:)
  obj%n = nn
  do ii=1,obj%n
    obj%per(ii) = ii
    obj%rep(ii) = ii
    obj%cyc(ii) = 0
  enddo
  obj%cyc(obj%n+1) = 0
  do ii=1,ubound(per,1)
    per(ii) = ii
  enddo
  end subroutine
!
  function permut_exit(obj,per) result(rslt)
  intent(inout) :: obj
  intent(out  ) :: per
  class(permut_type) :: obj
  logical :: rslt
  integer :: per(obj%n),ii,jj,kk,per1,rep1
!
  rslt = .false.
!
  ii = 1
  do
    jj = ii
    ii = ii+1
    if (obj%cyc(ii).lt.jj) then
      obj%cyc(2:jj) = 0
      obj%cyc(ii) = obj%cyc(ii) + 1
      exit
    endif
  enddo
!
  if (ii.gt.obj%n) then
    rslt = .true.
    return
  endif
!
  do jj=2,ii
    rep1 = obj%rep(obj%per(jj))
    do kk=jj-1,1,-1
      obj%rep(per(kk+1)) = obj%rep(obj%per(kk))
    enddo
    obj%rep(obj%per(1)) = rep1
    per1 = obj%per(1)
    do kk=2,jj
      obj%per(kk-1) = obj%per(kk)
    enddo
    obj%per(jj) = per1
  enddo
!
  per(1:obj%n) = obj%per(1:obj%n)
!
  end function


  function sort_2(i) result(rslt)
!***********************************************************************
! Hardwired optimal decision tree.
!***********************************************************************
  integer,intent(in) :: i(2)
  integer :: rslt
  if(i(1).le.i(2))then
    rslt = 1
  else
    rslt = 2
  endif
  end function


  function sort_3(i) result(rslt)
!***********************************************************************
! Hardwired optimal decision tree.
!***********************************************************************
  integer,intent(in) :: i(3)
  integer :: rslt
  if(i(1).le.i(2))then;if(i(2).le.i(3))then
    rslt = 1 ! 123
  else;if(i(1).le.i(3))then
    rslt = 6 ! 132
  else
    rslt = 3 ! 231
  endif;endif;else;if(i(1).le.i(3))then
    rslt = 2 ! 213
  else if(i(2).le.i(3))then
    rslt = 5 ! 312
  else
    rslt = 4 ! 321
  endif;endif
  end function


  function sort_4(i) result(rslt)
!***********************************************************************
! Hardwired decision tree following insertion sort.
! Maximum number of comparisons is not optimal (6 instead of 5),
! but average number of comparisions is less. 
!***********************************************************************
  integer,intent(in) :: i(4)
  integer :: rslt
  !character(1) :: rslt
  if(i(1).le.i(2))then;if(i(2).le.i(3))then;if(i(3).le.i(4))then
    rslt = 1
  elseif(i(2).le.i(4))then
    rslt = 21
  elseif(i(1).le.i(4))then
    rslt = 17
  else
    rslt = 7
  endif;elseif(i(1).le.i(3))then;if(i(2).le.i(4))then
    rslt = 6
  elseif(i(3).le.i(4))then
    rslt = 20
  elseif(i(1).le.i(4))then
    rslt = 16
  else
    rslt = 12
  endif;else;if(i(2).le.i(4))then
    rslt = 3
  elseif(i(1).le.i(4))then
    rslt = 23
  elseif(i(3).le.i(4))then
    rslt = 13
  else
    rslt = 9
  endif;endif;else;if(i(1).le.i(3))then;if(i(3).le.i(4))then
    rslt = 2
  elseif(i(1).le.i(4))then
    rslt = 22
  elseif(i(2).le.i(4))then
    rslt = 18
  else
    rslt = 8
  endif;elseif(i(2).le.i(3))then;if(i(1).le.i(4))then
    rslt = 5
  elseif(i(3).le.i(4))then
    rslt = 19
  elseif(i(2).le.i(4))then
    rslt = 15
  else
    rslt = 11
  endif;else;if(i(1).le.i(4))then
    rslt = 4
  elseif(i(2).le.i(4))then
    rslt = 24
  elseif(i(3).le.i(4))then
    rslt = 14
  else
    rslt = 10
  endif;endif;endif
  end function


  function sort_5(i) result(rslt)
!***********************************************************************
! Hardwired decision tree following insertion sort.
! Maximum number of comparisons is not optimal (10 instead of 9),
! but average number of comparisions is less. 
!***********************************************************************
  integer,intent(in) :: i(5)
  integer :: rslt
  !character(4) :: rslt
  if(i(1).le.i(2))then;if(i(2).le.i(3))then;if(i(3).le.i(4))then;if(i(4).le.i(5))then
    rslt = 1
  elseif(i(3).le.i(5))then
    rslt = 103
  elseif(i(2).le.i(5))then
    rslt = 85
  elseif(i(1).le.i(5))then
    rslt = 67
  else
    rslt = 25
  endif;elseif(i(2).le.i(4))then;if(i(3).le.i(5))then
    rslt = 21
  elseif(i(4).le.i(5))then
    rslt = 99
  elseif(i(2).le.i(5))then
    rslt = 81
  elseif(i(1).le.i(5))then
    rslt = 63
  else
    rslt = 45
  endif;elseif(i(1).le.i(4))then;if(i(3).le.i(5))then
    rslt = 17
  elseif(i(2).le.i(5))then
    rslt = 119
  elseif(i(4).le.i(5))then
    rslt = 77
  elseif(i(1).le.i(5))then
    rslt = 59
  else
    rslt = 41
  endif;else;if(i(3).le.i(5))then
    rslt = 7
  elseif(i(2).le.i(5))then
    rslt = 109
  elseif(i(1).le.i(5))then
    rslt = 91
  elseif(i(4).le.i(5))then
    rslt = 49
  else
    rslt = 31
  endif;endif;elseif(i(1).le.i(3))then;if(i(2).le.i(4))then;if(i(4).le.i(5))then
    rslt = 6
  elseif(i(2).le.i(5))then
    rslt = 108
  elseif(i(3).le.i(5))then
    rslt = 90
  elseif(i(1).le.i(5))then
    rslt = 72
  else
    rslt = 30
  endif;elseif(i(3).le.i(4))then;if(i(2).le.i(5))then
    rslt = 20
  elseif(i(4).le.i(5))then
    rslt = 98
  elseif(i(3).le.i(5))then
    rslt = 80
  elseif(i(1).le.i(5))then
    rslt = 62
  else
    rslt = 44
  endif;elseif(i(1).le.i(4))then;if(i(2).le.i(5))then
    rslt = 16
  elseif(i(3).le.i(5))then
    rslt = 118
  elseif(i(4).le.i(5))then
    rslt = 76
  elseif(i(1).le.i(5))then
    rslt = 58
  else
    rslt = 40
  endif;else;if(i(2).le.i(5))then
    rslt = 12
  elseif(i(3).le.i(5))then
    rslt = 114
  elseif(i(1).le.i(5))then
    rslt = 96
  elseif(i(4).le.i(5))then
    rslt = 54
  else
    rslt = 36
  endif;endif;else;if(i(2).le.i(4))then;if(i(4).le.i(5))then
    rslt = 3
  elseif(i(2).le.i(5))then
    rslt = 105
  elseif(i(1).le.i(5))then
    rslt = 87
  elseif(i(3).le.i(5))then
    rslt = 69
  else
    rslt = 27
  endif;elseif(i(1).le.i(4))then;if(i(2).le.i(5))then
    rslt = 23
  elseif(i(4).le.i(5))then
    rslt = 101
  elseif(i(1).le.i(5))then
    rslt = 83
  elseif(i(3).le.i(5))then
    rslt = 65
  else
    rslt = 47
  endif;elseif(i(3).le.i(4))then;if(i(2).le.i(5))then
    rslt = 13
  elseif(i(1).le.i(5))then
    rslt = 115
  elseif(i(4).le.i(5))then
    rslt = 73
  elseif(i(3).le.i(5))then
    rslt = 55
  else
    rslt = 37
  endif;else;if(i(2).le.i(5))then
    rslt = 9
  elseif(i(1).le.i(5))then
    rslt = 111
  elseif(i(3).le.i(5))then
    rslt = 93
  elseif(i(4).le.i(5))then
    rslt = 51
  else
    rslt = 33
  endif;endif;endif;else;if(i(1).le.i(3))then;if(i(3).le.i(4))then;if(i(4).le.i(5))then
    rslt = 2
  elseif(i(3).le.i(5))then
    rslt = 104
  elseif(i(1).le.i(5))then
    rslt = 86
  elseif(i(2).le.i(5))then
    rslt = 68
  else
    rslt = 26
  endif;elseif(i(1).le.i(4))then;if(i(3).le.i(5))then
    rslt = 22
  elseif(i(4).le.i(5))then
    rslt = 100
  elseif(i(1).le.i(5))then
    rslt = 82
  elseif(i(2).le.i(5))then
    rslt = 64
  else
    rslt = 46
  endif;elseif(i(2).le.i(4))then;if(i(3).le.i(5))then
    rslt = 18
  elseif(i(1).le.i(5))then
    rslt = 120
  elseif(i(4).le.i(5))then
    rslt = 78
  elseif(i(2).le.i(5))then
    rslt = 60
  else
    rslt = 42
  endif;else;if(i(3).le.i(5))then
    rslt = 8
  elseif(i(1).le.i(5))then
    rslt = 110
  elseif(i(2).le.i(5))then
    rslt = 92
  elseif(i(4).le.i(5))then
    rslt = 50
  else
    rslt = 32
  endif;endif;elseif(i(2).le.i(3))then;if(i(1).le.i(4))then;if(i(4).le.i(5))then
    rslt = 5
  elseif(i(1).le.i(5))then
    rslt = 107
  elseif(i(3).le.i(5))then
    rslt = 89
  elseif(i(2).le.i(5))then
    rslt = 71
  else
    rslt = 29
  endif;elseif(i(3).le.i(4))then;if(i(1).le.i(5))then
    rslt = 19
  elseif(i(4).le.i(5))then
    rslt = 97
  elseif(i(3).le.i(5))then
    rslt = 79
  elseif(i(2).le.i(5))then
    rslt = 61
  else
    rslt = 43
  endif;elseif(i(2).le.i(4))then;if(i(1).le.i(5))then
    rslt = 15
  elseif(i(3).le.i(5))then
    rslt = 117
  elseif(i(4).le.i(5))then
    rslt = 75
  elseif(i(2).le.i(5))then
    rslt = 57
  else
    rslt = 39
  endif;else;if(i(1).le.i(5))then
    rslt = 11
  elseif(i(3).le.i(5))then
    rslt = 113
  elseif(i(2).le.i(5))then
    rslt = 95
  elseif(i(4).le.i(5))then
    rslt = 53
  else
    rslt = 35
  endif;endif;else;if(i(1).le.i(4))then;if(i(4).le.i(5))then
    rslt = 4
  elseif(i(1).le.i(5))then
    rslt = 106
  elseif(i(2).le.i(5))then
    rslt = 88
  elseif(i(3).le.i(5))then
    rslt = 70
  else
    rslt = 28
  endif;elseif(i(2).le.i(4))then;if(i(1).le.i(5))then
    rslt = 24
  elseif(i(4).le.i(5))then
    rslt = 102
  elseif(i(2).le.i(5))then
    rslt = 84
  elseif(i(3).le.i(5))then
    rslt = 66
  else
    rslt = 48
  endif;elseif(i(3).le.i(4))then;if(i(1).le.i(5))then
    rslt = 14
  elseif(i(2).le.i(5))then
    rslt = 116
  elseif(i(4).le.i(5))then
    rslt = 74
  elseif(i(3).le.i(5))then
    rslt = 56
  else
    rslt = 38
  endif;else;if(i(1).le.i(5))then
    rslt = 10
  elseif(i(2).le.i(5))then
    rslt = 112
  elseif(i(3).le.i(5))then
    rslt = 94
  elseif(i(4).le.i(5))then
    rslt = 52
  else
    rslt = 34
  endif;endif;endif;endif
  end function

  
end module


