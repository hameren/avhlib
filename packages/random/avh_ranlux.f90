module avh_ranlux
  use avh_iounits
! Subtract-and-borrow random number generator proposed by
! Marsaglia and Zaman, implemented by F. James with the name
! RCARRY in 1991, and later improved by Martin Luescher
! in 1993 to produce "Luxury Pseudorandom Numbers".
! Fortran 77 coded by F. James, 1993
! Adapted to Fortran03 by A. van Hameren, 2013
  private
  public :: ranlux_type
      
  integer,parameter,private :: MAXLEV=4,LXDFLT=3
  integer,parameter,private :: TWOP12=4096.
  integer,parameter,private :: ITWO24=2**24,ICONS=2147483563,IGIGA=1000000000,JSDFLT=314159265

  type :: ranlux_type
    logical :: NOTYET=.true.
    integer :: I24=24, J24=10, LUXLEV=LXDFLT
    real    :: CARRY=0., TWOM24, TWOM12
    real    :: SEEDS(24)
    integer :: NSKIP, IN24=0, NEXT(24), KOUNT=0, MKOUNT=0, INSEED
    integer :: NDSKIP(0:MAXLEV)=(/0,24,73,199,365/)
    character(24) :: label='not specified'
  contains
    procedure :: gnrt=>RANLUX
    procedure :: init=>RLUXGO
    procedure :: in=>RLUXIN
    procedure :: ut=>RLUXUT
    procedure :: at=>RLUXAT
  end type

  logical,save,private :: initd=.false.

contains

  subroutine hello
  if (initd) return
  initd = .true.
  write(stdout,'(a72)') '########################################################################'
  write(stdout,'(a72)') '#          You are using RANLUX for random number generation           #'
  write(stdout,'(a72)') '# Please cite                                                          #'
  write(stdout,'(a72)') '#   F. James, Comput.Phys.Commun 79 (1994) 111-114,                    #'
  write(stdout,'(a72)') '#   Erratum-ibid. 97 (1996) 357                                        #'
  write(stdout,'(a72)') '# in publications with results obtained with the help of this program. #'
  write(stdout,'(a72)') '########################################################################'
  end subroutine

      subroutine RANLUX(obj,RVEC,LENV)
!         Subtract-and-borrow random number generator proposed by
!         Marsaglia and Zaman, implemented by F. James with the name
!         RCARRY in 1991, and later improved by Martin Luescher
!         in 1993 to produce "Luxury Pseudorandom Numbers".
!     Fortran 77 coded by F. James, 1993
!          
!       references: 
!  M. Luscher, Computer Physics Communications  79 (1994) 100
!  F. James, Computer Physics Communications 79 (1994) 111
!
!   LUXURY LEVELS.
!   ------ ------      The available luxury levels are:
!
!  level 0  (p=24): equivalent to the original RCARRY of Marsaglia
!           and Zaman, very long period, but fails many tests.
!  level 1  (p=48): considerable improvement in quality over level 0,
!           now passes the gap test, but still fails spectral test.
!  level 2  (p=97): passes all known tests, but theoretically still
!           defective.
!  level 3  (p=223): DEFAULT VALUE.  Any theoretically possible
!           correlations have very small chance of being observed.
!  level 4  (p=389): highest possible luxury, all 24 bits chaotic.
!
!!!! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!!!!  Calling sequences for RANLUX:                                  ++
!!!!      CALL RANLUX (RVEC, LEN)   returns a vector RVEC of LEN     ++
!!!!                   32-bit random floating point numbers between  ++
!!!!                   zero (not included) and one (also not incl.). ++
!!!!      CALL RLUXGO(LUX,INT,K1,K2) initializes the generator from  ++
!!!!               one 32-bit integer INT and sets Luxury Level LUX  ++
!!!!               which is integer between zero and MAXLEV, or if   ++
!!!!               LUX .GT. 24, it sets p=LUX directly.  K1 and K2   ++
!!!!               should be set to zero unless restarting at a break++ 
!!!!               point given by output of RLUXAT (see RLUXAT).     ++
!!!!      CALL RLUXAT(LUX,INT,K1,K2) gets the values of four integers++
!!!!               which can be used to restart the RANLUX generator ++
!!!!               at the current point by calling RLUXGO.  K1 and K2++
!!!!               specify how many numbers were generated since the ++
!!!!               initialization with LUX and INT.  The restarting  ++
!!!!               skips over  K1+K2*E9   numbers, so it can be long.++
!!!!   A more efficient but less convenient way of restarting is by: ++
!!!!      CALL RLUXIN(ISVEC)    restarts the generator from vector   ++
!!!!                   ISVEC of 25 32-bit integers (see RLUXUT)      ++
!!!!      CALL RLUXUT(ISVEC)    outputs the current values of the 25 ++
!!!!                 32-bit integer seeds, to be used for restarting ++
!!!!      ISVEC must be dimensioned 25 in the calling program        ++
!!!! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      class(ranlux_type) :: obj
      DIMENSION RVEC(LENV)
      DIMENSION ISEEDS(24)
!                               default
!  Luxury Level   0     1     2   *3*    4
!     DATA NDSKIP/0,   24,   73,  199,  365 /
!orresponds to p=24    48    97   223   389
!     time factor 1     2     3     6    10   on slow workstation
!                 1    1.5    2     3     5   on fast mainframe
!
!  NOTYET is .TRUE. if no initialization has been performed yet.
!              Default Initialization by Multiplicative Congruential
      IF (obj%NOTYET) THEN
         call hello
         obj%NOTYET = .FALSE.
         JSEED = JSDFLT  
         obj%INSEED = JSEED
         write(6,'(A)') 'MESSAGES from RANLUX instance '//trim(obj%label)//':'
         WRITE(6,'(A,I12)') ' RANLUX DEFAULT INITIALIZATION: ',JSEED
         obj%LUXLEV = LXDFLT
         obj%NSKIP = obj%NDSKIP(obj%LUXLEV)
         LP = obj%NSKIP + 24
         obj%IN24 = 0
         obj%KOUNT = 0
         obj%MKOUNT = 0
         WRITE(6,'(A,I2,A,I4)')  ' RANLUX DEFAULT LUXURY LEVEL =  ', &
              obj%LUXLEV,'      p =',LP
            obj%TWOM24 = 1.
         DO 25 I= 1, 24
            obj%TWOM24 = obj%TWOM24 * 0.5
         K = JSEED/53668
         JSEED = 40014*(JSEED-K*53668) -K*12211
         IF (JSEED .LT. 0)  JSEED = JSEED+ICONS
         ISEEDS(I) = MOD(JSEED,ITWO24)
   25    CONTINUE
         obj%TWOM12 = obj%TWOM24 * 4096.
         DO 50 I= 1,24
         obj%SEEDS(I) = REAL(ISEEDS(I))*obj%TWOM24
         obj%NEXT(I) = I-1
   50    CONTINUE
         obj%NEXT(1) = 24
         obj%I24 = 24
         obj%J24 = 10
         obj%CARRY = 0.
         IF (obj%SEEDS(24) .EQ. 0.) obj%CARRY = obj%TWOM24
      ENDIF
!
!          The Generator proper: "Subtract-with-borrow",
!          as proposed by Marsaglia and Zaman,
!          Florida State University, March, 1989
!
      DO 100 IVEC= 1, LENV
      UNI = obj%SEEDS(obj%J24) - obj%SEEDS(obj%I24) - obj%CARRY 
      IF (UNI .LT. 0.)  THEN
         UNI = UNI + 1.0
         obj%CARRY = obj%TWOM24
      ELSE
         obj%CARRY = 0.
      ENDIF
      obj%SEEDS(obj%I24) = UNI
      obj%I24 = obj%NEXT(obj%I24)
      obj%J24 = obj%NEXT(obj%J24)
      RVEC(IVEC) = UNI
!  small numbers (with less than 12 "significant" bits) are "padded".
      IF (UNI .LT. obj%TWOM12)  THEN
         RVEC(IVEC) = RVEC(IVEC) + obj%TWOM24*obj%SEEDS(obj%J24)
!        and zero is forbidden in case someone takes a logarithm
         IF (RVEC(IVEC) .EQ. 0.)  RVEC(IVEC) = obj%TWOM24*obj%TWOM24
      ENDIF
!        Skipping to luxury.  As proposed by Martin Luscher.
      obj%IN24 = obj%IN24 + 1
      IF (obj%IN24 .EQ. 24)  THEN
         obj%IN24 = 0
         obj%KOUNT = obj%KOUNT + obj%NSKIP
         DO 90 ISK= 1, obj%NSKIP
         UNI = obj%SEEDS(obj%J24) - obj%SEEDS(obj%I24) - obj%CARRY
         IF (UNI .LT. 0.)  THEN
            UNI = UNI + 1.0
            obj%CARRY = obj%TWOM24
         ELSE
            obj%CARRY = 0.
         ENDIF
         obj%SEEDS(obj%I24) = UNI
         obj%I24 = obj%NEXT(obj%I24)
         obj%J24 = obj%NEXT(obj%J24)
   90    CONTINUE
      ENDIF
  100 CONTINUE
      obj%KOUNT = obj%KOUNT + LENV
      IF (obj%KOUNT .GE. IGIGA)  THEN
         obj%MKOUNT = obj%MKOUNT + 1
         obj%KOUNT = obj%KOUNT - IGIGA
      ENDIF
      end subroutine
!
!           Entry to input and float integer seeds from previous run
      subroutine RLUXIN(obj,ISDEXT)
      class(ranlux_type) :: obj
      DIMENSION ISDEXT(25)
         obj%TWOM24 = 1.
         DO 195 I= 1, 24
         obj%NEXT(I) = I-1
!ANDRE  195    obj%TWOM24 = obj%TWOM24 * 0.5
         obj%TWOM24 = obj%TWOM24 * 0.5 !ANDRE
  195    continue                      !ANDRE
         obj%NEXT(1) = 24
         obj%TWOM12 = obj%TWOM24 * 4096.
      write(6,'(A)') 'MESSAGES from RANLUX instance '//trim(obj%label)//':'
      WRITE(6,'(A)') ' FULL INITIALIZATION OF RANLUX WITH 25 INTEGERS:'
      WRITE(6,'(5X,5I12)') ISDEXT
      DO 200 I= 1, 24
      obj%SEEDS(I) = REAL(ISDEXT(I))*obj%TWOM24
  200 CONTINUE
      obj%CARRY = 0.
      IF (ISDEXT(25) .LT. 0)  obj%CARRY = obj%TWOM24
      ISD = IABS(ISDEXT(25))
      obj%I24 = MOD(ISD,100)
      ISD = ISD/100
      obj%J24 = MOD(ISD,100)
      ISD = ISD/100
      obj%IN24 = MOD(ISD,100)
      ISD = ISD/100
      obj%LUXLEV = ISD
        IF (obj%LUXLEV .LE. MAXLEV) THEN
          obj%NSKIP = obj%NDSKIP(obj%LUXLEV)
          WRITE (6,'(A,I2)') ' RANLUX LUXURY LEVEL SET BY RLUXIN TO: ', &
                               obj%LUXLEV
        ELSE  IF (obj%LUXLEV .GE. 24) THEN
          obj%NSKIP = obj%LUXLEV - 24
          WRITE (6,'(A,I5)') ' RANLUX P-VALUE SET BY RLUXIN TO:',obj%LUXLEV
        ELSE
          obj%NSKIP = obj%NDSKIP(MAXLEV)
          WRITE (6,'(A,I5)') ' RANLUX ILLEGAL LUXURY RLUXIN: ',obj%LUXLEV
          obj%LUXLEV = MAXLEV
        ENDIF
      obj%INSEED = -1
      end subroutine
!
!                    Entry to ouput seeds as integers
      subroutine RLUXUT(obj,ISDEXT)
      class(ranlux_type) :: obj
      DIMENSION ISDEXT(25)
      DO 300 I= 1, 24
         ISDEXT(I) = INT(obj%SEEDS(I)*TWOP12*TWOP12)
  300 CONTINUE
      ISDEXT(25) = obj%I24 + 100*obj%J24 + 10000*obj%IN24 + 1000000*obj%LUXLEV
      IF (obj%CARRY .GT. 0.)  ISDEXT(25) = -ISDEXT(25)
      end subroutine
!
!                    Entry to output the "convenient" restart point
      subroutine RLUXAT(obj,LOUT,INOUT,K1,K2)
      class(ranlux_type) :: obj
      LOUT = obj%LUXLEV
      INOUT = obj%INSEED
      K1 = obj%KOUNT
      K2 = obj%MKOUNT
      end subroutine
!
!                    Entry to initialize from one or three integers
      subroutine RLUXGO(obj,LUX,INS,K1,K2,label)
      class(ranlux_type) :: obj
      character(*),intent(in),optional :: label
      DIMENSION ISEEDS(24)
      call hello
      if (present(label)) obj%label=adjustl(label)
      write(6,'(A)') 'MESSAGES from RANLUX instance '//trim(obj%label)//':'
         IF (LUX .LT. 0) THEN
            obj%LUXLEV = LXDFLT
         ELSE IF (LUX .LE. MAXLEV) THEN
            obj%LUXLEV = LUX
         ELSE IF (LUX .LT. 24 .OR. LUX .GT. 2000) THEN
            obj%LUXLEV = MAXLEV
            WRITE (6,'(A,I7)') ' RANLUX ILLEGAL LUXURY RLUXGO: ',LUX
         ELSE
            obj%LUXLEV = LUX
            DO 310 ILX= 0, MAXLEV
              IF (LUX .EQ. obj%NDSKIP(ILX)+24)  obj%LUXLEV = ILX
  310       CONTINUE
         ENDIF
      IF (obj%LUXLEV .LE. MAXLEV)  THEN
         obj%NSKIP = obj%NDSKIP(obj%LUXLEV)
         WRITE(6,'(A,I2,A,I4)') ' RANLUX LUXURY LEVEL SET BY RLUXGO :', &
              obj%LUXLEV,'     P=', obj%NSKIP+24
      ELSE
          obj%NSKIP = obj%LUXLEV - 24
          WRITE (6,'(A,I5)') ' RANLUX P-VALUE SET BY RLUXGO TO:',obj%LUXLEV
      ENDIF
      obj%IN24 = 0
      IF (INS .LT. 0)  WRITE (6,'(A)')    &
         ' Illegal initialization by RLUXGO, negative input seed'
      IF (INS .GT. 0)  THEN
        JSEED = INS
        WRITE(6,'(A,3I12)') ' RANLUX INITIALIZED BY RLUXGO FROM SEEDS', &
            JSEED, K1,K2
      ELSE
        JSEED = JSDFLT
        WRITE(6,'(A)')' RANLUX INITIALIZED BY RLUXGO FROM DEFAULT SEED'
      ENDIF
      obj%INSEED = JSEED
      obj%NOTYET = .FALSE.
      obj%TWOM24 = 1.
         DO 325 I= 1, 24
           obj%TWOM24 = obj%TWOM24 * 0.5
         K = JSEED/53668
         JSEED = 40014*(JSEED-K*53668) -K*12211
         IF (JSEED .LT. 0)  JSEED = JSEED+ICONS
         ISEEDS(I) = MOD(JSEED,ITWO24)
  325    CONTINUE
      obj%TWOM12 = obj%TWOM24 * 4096.
         DO 350 I= 1,24
         obj%SEEDS(I) = REAL(ISEEDS(I))*obj%TWOM24
         obj%NEXT(I) = I-1
  350    CONTINUE
      obj%NEXT(1) = 24
      obj%I24 = 24
      obj%J24 = 10
      obj%CARRY = 0.
      IF (obj%SEEDS(24) .EQ. 0.) obj%CARRY = obj%TWOM24
!        If restarting at a break point, skip K1 + IGIGA*K2
!        Note that this is the number of numbers delivered to
!        the user PLUS the number skipped (if luxury .GT. 0).
      obj%KOUNT = K1
      obj%MKOUNT = K2
      IF (K1+K2 .NE. 0)  THEN
        DO 500 IOUTER= 1, K2+1
          INNER = IGIGA
          IF (IOUTER .EQ. K2+1)  INNER = K1
          DO 450 ISK= 1, INNER
            UNI = obj%SEEDS(obj%J24) - obj%SEEDS(obj%I24) - obj%CARRY 
            IF (UNI .LT. 0.)  THEN
               UNI = UNI + 1.0
               obj%CARRY = obj%TWOM24
            ELSE
               obj%CARRY = 0.
            ENDIF
            obj%SEEDS(obj%I24) = UNI
            obj%I24 = obj%NEXT(obj%I24)
            obj%J24 = obj%NEXT(obj%J24)
  450     CONTINUE
  500   CONTINUE
!         Get the right value of obj%IN24 by direct calculation
        obj%IN24 = MOD(obj%KOUNT, obj%NSKIP+24)
        IF (obj%MKOUNT .GT. 0)  THEN
           IZIP = MOD(IGIGA, obj%NSKIP+24)
           IZIP2 = obj%MKOUNT*IZIP + obj%IN24
           obj%IN24 = MOD(IZIP2, obj%NSKIP+24)
        ENDIF
!       Now obj%IN24 had better be between zero and 23 inclusive
        IF (obj%IN24 .GT. 23) THEN
           WRITE (6,'(A/A,3I11,A,I5)')   &
          '  Error in RESTARTING with RLUXGO:','  The values', INS, &
           K1, K2, ' cannot occur at luxury level', obj%LUXLEV
           obj%IN24 = 0
        ENDIF
      ENDIF
      end subroutine

end module


