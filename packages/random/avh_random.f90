module avh_random
  !(usekindmod!)
  use avh_iounits
  use avh_ranlux
  implicit none
  private
  public :: rangen_init,rangen,ranfun

  interface rangen
    module procedure rangen0,rangen1
  end interface

  type(ranlux_type),save :: ranlux

contains

  subroutine rangen_init(seed)
  intent(in) :: seed
  integer :: seed
  call ranlux%init(-1,seed,0,0,label='avh_random static')
  end subroutine

  subroutine rangen0(xx)
  intent(out) :: xx
  !(realknd2!) :: xx
  real :: rr(1)
  call ranlux%gnrt(rr,1)
  xx = rr(1)
  end subroutine

  subroutine rangen1(xx,nn)
  integer,intent(in) :: nn
  intent(out) :: xx
  !(realknd2!) :: xx(nn)
  real :: rr(nn)
  call ranlux%gnrt(rr,nn)
  xx = rr
  end subroutine

  function ranfun() result(xx)
  !(realknd2!) :: xx
  real :: rr(1)
  call ranlux%gnrt(rr,1)
  xx = rr(1)
  end function
  

end module


