module avh_kinematics
  !(usekindmod!)
  use avh_iounits
  use avh_mathcnst
  use avh_doloops
  use avh_kskeleton
  use avh_prnt
  implicit none
  private
  public :: psPoint_type
  public :: realMom_type ,realMom_lambda
  public :: sort_big,sort_big2small,sort_small2big,order_big2small,symMat
  public :: process_type
  public :: momTree,update_momTree
  public :: operator(*),operator(+),operator(-)

  type :: process_type
    integer :: infi(-2:size_leaves-1)
    integer :: xtrn(1:size_leaves+1)
    integer :: Nfinst,Nxtrn
  contains
    procedure :: put=>process_put
    procedure :: complete_infi
    procedure :: complete_xtrn
    procedure :: abs=>process_abs
    procedure :: apply=>process_apply
    procedure :: print=>process_print
  end type


  type :: realMom_type
    logical :: Small
    !(realknd2!) :: E,V(3),M,S,AV,Tsq
  contains
    procedure :: complete=>realMom_complete
    procedure :: boost1
    procedure :: boost2
    generic :: boost=>boost1,boost2
    procedure :: tsoob
    procedure :: boost_sub
    procedure :: tsoob_sub
    procedure :: rotate_z
    procedure :: minus=>realMom_minus
    procedure :: prnt=>realMom_prnt
  end type

  interface operator (*)
    module procedure realMom_i_p,realMom_p_i,realMom_r_p,realMom_p_r
  end interface
  interface operator (+)
    module procedure realMom_add
  end interface
  interface operator (-)
    module procedure realMom_subtract
  end interface

  interface
    function process_interface(ii) result(rslt)
    integer,intent(in) :: ii
    integer :: rslt
    end function
  end interface


  type :: psPoint_type
    integer :: Nfinst=0,Nsize,AllFinal
    integer,allocatable :: b(:)
    type(realMom_type),allocatable :: p(:)
  contains
    procedure :: alloc=>psp_alloc
    procedure :: copy=>psp_copy
    procedure :: psp_put_a
    procedure :: psp_put_b
    generic :: put=>psp_put_a,psp_put_b
    procedure :: psp_put_inst0
    procedure :: psp_put_inst1
    generic :: put_inst=>psp_put_inst0,psp_put_inst1
    procedure :: put_masses=>psp_put_masses
    procedure :: complete=>psp_complete
    procedure :: finalize=>psp_finalize
    procedure :: lambda=>psp_lambda
    procedure :: psp_prnt
    procedure :: psp_prnt_list
    generic :: prnt=>psp_prnt,psp_prnt_list
    procedure :: write=>psp_write
    procedure :: psp_energy
    procedure :: psp_energy_list
    generic :: Energy=>psp_energy,psp_energy_list
    procedure :: psp_vector
    procedure :: psp_vector_list
    generic :: Vector=>psp_vector,psp_vector_list
    procedure :: psp_trans
    procedure :: psp_trans_list
    generic :: Trans=>psp_trans,psp_trans_list
    procedure :: psp_Etrans
    procedure :: psp_Etrans_list
    generic :: Etrans=>psp_Etrans,psp_Etrans_list
    procedure :: psp_square
    procedure :: psp_square_list
    generic :: Square=>psp_square,psp_square_list
    procedure :: psp_mass
    procedure :: psp_mass_list
    generic :: Mass=>psp_mass,psp_mass_list
    procedure :: psp_absvec
    procedure :: psp_absvec_list
    generic :: AbsVec=>psp_absvec,psp_absvec_list
    procedure :: psp_rapidity
    procedure :: psp_rapidity_list
    generic :: Rapidity=>psp_rapidity,psp_rapidity_list
    procedure :: psp_pseudoRap
    procedure :: psp_pseudoRap_list
    generic :: pseudoRap=>psp_pseudoRap,psp_pseudoRap_list
    procedure :: psp_plus
    procedure :: psp_plus_list
    generic :: plus=>psp_plus,psp_plus_list
    procedure :: psp_minus
    procedure :: psp_minus_list
    generic :: minus=>psp_minus,psp_minus_list
    procedure :: psp_theta
    procedure :: psp_theta_list
    generic :: theta=>psp_theta,psp_theta_list
    procedure :: psp_phi
    procedure :: psp_phi_list
    generic :: phi=>psp_phi,psp_phi_list
    procedure :: psp_deltaR
    procedure :: psp_deltaR_list
    generic :: deltaR=>psp_deltaR,psp_deltaR_list
    procedure :: psp_deltaPhi
    procedure :: psp_deltaPhi_list
    generic :: deltaPhi=>psp_deltaPhi,psp_deltaPhi_list
    procedure :: psp_angle
    procedure :: psp_angle_list
    generic :: angle=>psp_angle,psp_angle_list
    procedure :: frmt03=>psp_frmt03
    procedure :: fnst=>psp_fnst
    procedure :: inst=>psp_inst
  end type

  integer,parameter :: unity(-2:15)=&
    [-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]

  !(integer1!),save :: symMat(17,17)

  logical,save :: initz=.true.
  !(realknd2!),save :: small


contains


  subroutine init
  integer :: nn,ii,jj
  initz = .false.
  call init_mathcnst
  small = 1d-4
  nn = 0
  do ii=1,17
    nn = nn+1
    symMat(ii,ii) = nn
    do jj=ii-1,1,-1
      nn = nn+1
      symMat(ii,jj)=nn
      symMat(jj,ii)=nn
    enddo
  enddo
  end subroutine

  subroutine set_small( xx )
  !(realknd2!),intent(in) :: xx
  if (initz) call init
  small = abs(xx)
  end subroutine

  subroutine psp_alloc( obj ,Nfinst )
  class(psPoint_type),intent(out) :: obj
  integer,intent(in) :: Nfinst
  integer :: ii
  call update_momTree(Nfinst)
  obj%Nfinst = Nfinst
  obj%Nsize = base(Nfinst+3)-1
  obj%AllFinal = base(Nfinst+2)-2
  if (allocated(obj%p)) deallocate(obj%p)
  allocate( obj%p(0:obj%Nsize) )
  obj%p(       0 ) = realMom_zero()
  obj%p(obj%Nsize) = realMom_zero()
  if (allocated(obj%b)) deallocate(obj%b)
  allocate( obj%b(-2:obj%Nfinst) )
  obj%b(-1) = 1
  obj%b( 0) = 0
  obj%b( 1) = 2
  do ii=2,obj%Nfinst
    obj%b(ii) = 2*obj%b(ii-1)
  enddo
  obj%b(-2) = 2*obj%b(obj%Nfinst)
  end subroutine


  function realMom_prnt(obj,Ndec) result(rslt)
  class(realMom_type),intent(in) :: obj
  integer           ,intent(in) :: Ndec
  character(5*(8+Ndec)-1) :: rslt
  character(4*(8+Ndec)-1) :: gg
  character(   8+Ndec   ) :: hh
  gg = prnt([obj%E,obj%V(1),obj%V(2),obj%V(3)],Ndec)
  hh = prnt(obj%M,Ndec)
  if (obj%S.lt.rZRO) hh(1:1) = 'i'
  rslt = gg//' '//hh
  end function

  function psp_prnt(obj,ii,Ndec) result(rslt)
  class(psPoint_type),intent(in) :: obj
  integer           ,intent(in) :: ii,Ndec
  character(5*(8+Ndec)-1) :: rslt
  rslt = obj%p(ii)%prnt( Ndec )
  end function

  function psp_prnt_list(obj,list,Ndec) result(rslt)
  class(psPoint_type),intent(in) :: obj
  integer,intent(in) :: list(:),Ndec
  character(5*(8+Ndec)-1) :: rslt
  integer :: ii,arg
  arg = 0
  do ii=1,size(list,1)
    arg = arg+obj%b(list(ii))
  enddo
  rslt = obj%prnt(arg,Ndec)
  end function

  subroutine psp_write(obj,Ndecim,iunit,frmt)
  class(psPoint_type),intent(in) :: obj
  integer,intent(in),optional :: Ndecim,iunit
  character(*),intent(in),optional :: frmt
  integer :: ii,nunit,Ndec
!
  nunit = stdout
  if (present(iunit)) nunit=iunit
  Ndec = 15
  if (present(Ndecim)) Ndec=Ndecim
!
  if (.not.present(frmt)) then
    do ii=-2,obj%Nfinst
      if (ii.eq.0) cycle
      write(nunit,'(A)') obj%prnt([ii],Ndec)
    enddo
  else
    do ii=-2,obj%Nfinst
      if (ii.eq.0) cycle
      write(nunit,trim(frmt)) obj%p(obj%b(ii))%E,obj%p(obj%b(ii))%V
    enddo
  endif
!
  end subroutine


  subroutine psp_copy( obj ,ii ,psPoint ,jj )
!***********************************************************************
!***********************************************************************
  class(psPoint_type) :: obj,psPoint
  integer,intent(in) :: ii,jj
  obj%p(obj%b(ii)) = psPoint%p(psPoint%b(jj))
  end subroutine


  subroutine psp_put_a( obj ,ii ,pXtrn,sXtrn ,onShell )
!***********************************************************************
! Only uses the sign of pXtrn(0) if onShell=.true.,
!   Energy = sign( pXtrn(0) )*sqrt( sXtrn + pXtrn(1:3)**2 )
!***********************************************************************
  class(psPoint_type) :: obj
  integer,intent(in) :: ii
  !(realknd2!),intent(in) :: pXtrn(0:3),sXtrn
  logical,intent(in) :: onShell
  integer :: jj
!
  associate( q=>obj%p(obj%b(ii)) )
!
  q%V(1:3) = pXtrn(1:3)
  q%S = sXtrn
  q%Tsq = q%V(1)*q%V(1) + q%V(2)*q%V(2)
  q%AV = q%V(3)*q%V(3) + q%Tsq
  if (onShell) then
    q%E = q%AV + q%S
    if (q%E.lt.0) then
      if (errru.ge.0) write(errru,*) 'ERROR in avh_kinematics put_realMom: ' &
        ,'negative squared energy, taking absolute value'
      q%E =-q%E
    endif
    q%E = sign(sqrt(q%E),pXtrn(0))
  else
    q%E = pXtrn(0)
  endif
  q%M = abs(q%S)
  q%Small = (q%M.lt.small*q%AV)
  q%M = sqrt(q%M)
  q%AV = sqrt(q%AV)
!
  jj = obj%Nsize-obj%b(ii)
  obj%p(jj)%V =-q%V
  obj%p(jj)%E =-q%E
  obj%p(jj)%S = q%S
  obj%p(jj)%M = q%M  
  obj%p(jj)%Tsq = q%Tsq
  obj%p(jj)%AV = q%AV
  obj%p(jj)%Small = q%Small
!
  end associate
!
  end subroutine


  subroutine psp_put_b( obj ,ii ,pXtrn )
!***********************************************************************
!***********************************************************************
  class(psPoint_type) :: obj
  integer,intent(in) :: ii
  !(realknd2!),intent(in) :: pXtrn(0:3)
  !(realknd2!) :: av2
  integer :: jj
!
  associate( q=>obj%p(obj%b(ii)) )
!
  q%E = pXtrn(0)
  q%V(1:3) = pXtrn(1:3)
  q%Tsq = q%V(1)*q%V(1) + q%V(2)*q%V(2)
  av2 = q%V(3)*q%V(3) + q%Tsq
  q%AV = sqrt( av2 )
  q%S = (q%E-q%AV)*(q%E+q%AV)
  q%M = abs(q%S)
  q%Small = (q%M.lt.small*av2)
  q%M = sqrt(q%M)
!
  jj = obj%Nsize-obj%b(ii)
  obj%p(jj)%V =-q%V
  obj%p(jj)%E =-q%E
  obj%p(jj)%S = q%S
  obj%p(jj)%M = q%M  
  obj%p(jj)%Tsq = q%Tsq
  obj%p(jj)%AV = q%AV
  obj%p(jj)%Small = q%Small
!
  end associate
!
  end subroutine


  subroutine psp_put_inst0( obj ,pInst ,sInst ,onShell )
!***********************************************************************
!***********************************************************************
  class(psPoint_type) :: obj
  !(realknd2!),intent(in) :: pInst(0:3,-2:-1),sInst(-2:-1)
  logical,intent(in) :: onShell
  integer :: i1,i2
  call obj%put( -2 ,pInst(0:3,-2) ,sInst(-2) ,onShell )
  call obj%put( -1 ,pInst(0:3,-1) ,sInst(-1) ,onShell )
  i2 = obj%b(-2)
  i1 = obj%b(-1)
  obj%p(i1+i2) = realMom_add( obj%p(i1) ,obj%p(i2) )
  obj%p(obj%Nsize-i1-i2) = obj%p(i1+i2)%minus()
  end subroutine

  subroutine psp_put_inst1( obj ,psp )
  class(psPoint_type) :: obj
  type(psPoint_type) :: psp
  integer :: i1,i2,j1,j2
  i2 = obj%b(-2)  ;j2 = psp%b(-2)
  i1 = obj%b(-1)  ;j1 = psp%b(-1)
  obj%p(i2) = psp%p(j2)
  obj%p(i1) = psp%p(j1)
  obj%p(obj%Nsize-i2) = psp%p(psp%Nsize-j2)
  obj%p(obj%Nsize-i1) = psp%p(psp%Nsize-j1)
  obj%p(i1+i2) = psp%p(j1+j2)
  obj%p(obj%Nsize-i1-i2) = psp%p(psp%Nsize-j1-j2)
  end subroutine


  subroutine psp_put_masses( obj ,masses )
!***********************************************************************
!***********************************************************************
  class(psPoint_type),intent(inout) :: obj
  !(realknd2!),intent(in) :: masses(1:obj%Nfinst)
  integer :: ii
  do ii=1,obj%Nfinst
    obj%p(obj%b(ii))%M = masses(ii)
    obj%p(obj%b(ii))%S = masses(ii)*masses(ii)
  enddo
  end subroutine

  subroutine psp_complete( obj )
!***********************************************************************
!***********************************************************************
  class(psPoint_type) :: obj
  integer :: ii,i1,i2,nn
  do ii=-2,obj%Nfinst
    if (ii.eq.0) cycle
    obj%p(obj%Nsize-obj%b(ii)) = obj%p(obj%b(ii))%minus()
  enddo
  nn = obj%Nfinst+2
  do ii=1,momTree(nn)%N
    i1 = momTree(nn)%p1(ii)
    i2 = momTree(nn)%p2(ii)
    obj%p(i1+i2) = realMom_add( obj%p(i1),obj%p(i2) )
    obj%p(momTree(nn)%o-i1-i2) = obj%p(i1+i2)%minus()
  enddo
  end subroutine

  subroutine psp_finalize( obj )
!***********************************************************************
! finalize external momenta, and calculate all internal momenta
!***********************************************************************
  class(psPoint_type) :: obj
  !(realknd2!) :: sgn
  integer :: ii
  do ii=1,obj%Nfinst
  associate( q=>obj%p(obj%b(ii)) )
    q%Tsq = q%V(1)*q%V(1) + q%V(2)*q%V(2)
    q%AV = q%V(3)*q%V(3) + q%Tsq
    sgn = q%E
    q%E = q%AV + q%S
    if (q%E.lt.0) then
      if (errru.ge.0) write(errru,*) 'ERROR in avh_kinematics complete: ' &
        ,'negative squared energy, taking absolute value'
      q%E =-q%E
    endif
    q%E = sign(sqrt(q%E),sgn)
    q%Small = (q%S.lt.small*q%AV)
    q%AV = sqrt(q%AV)
  end associate
  enddo
  call obj%complete
  end subroutine


  function psp_Energy_list(obj,list) result(rslt)
    !(realknd2!) :: rslt
    include 'psp_function_list.h90'
    rslt = obj%p(arg)%E
  end function
  function psp_Vector_list(obj,list) result(rslt)
    !(realknd2!) :: rslt(3)
    include 'psp_function_list.h90'
    rslt = obj%p(arg)%V
  end function
  function psp_Trans_list(obj,list) result(rslt)
    !(realknd2!) :: rslt
    include 'psp_function_list.h90'
    rslt = sqrt(obj%p(arg)%Tsq)
  end function
  function psp_Etrans_list(obj,list) result(rslt)
    !(realknd2!) :: rslt
    include 'psp_function_list.h90'
    rslt = sqrt(obj%p(arg)%Tsq+obj%p(arg)%S)
  end function
  function psp_Square_list(obj,list) result(rslt)
    !(realknd2!) :: rslt
    include 'psp_function_list.h90'
    rslt = obj%p(arg)%S
  end function
  function psp_Mass_list(obj,list) result(rslt)
    !(realknd2!) :: rslt
    include 'psp_function_list.h90'
    rslt = obj%p(arg)%M
  end function
  function psp_AbsVec_list(obj,list) result(rslt)
    !(realknd2!) :: rslt
    include 'psp_function_list.h90'
    rslt = obj%p(arg)%AV
  end function
  function psp_rapidity_list(obj,list) result(rslt)
    !(realknd2!) :: rslt,mTsq
    include 'psp_function_list.h90'
    mTsq = obj%p(arg)%S + obj%p(arg)%Tsq
    if (mTsq.gt.rZRO) then
      rslt = obj%p(arg)%E + abs(obj%p(arg)%V(3))
      rslt = log( rslt*rslt/mTsq )/2
    else
      rslt = 99
    endif
    if (obj%p(arg)%V(3).lt.rZRO) rslt =-rslt
  end function
  function psp_pseudoRap_list(obj,list) result(rslt)
    !(realknd2!) :: rslt
    include 'psp_function_list.h90'
    if (obj%p(arg)%Tsq.gt.rZRO) then
      rslt = obj%p(arg)%AV + abs(obj%p(arg)%V(3))
      rslt = log( rslt*rslt/obj%p(arg)%Tsq )/2
    else
      rslt = 99
    endif
    if (obj%p(arg)%V(3).lt.rZRO) rslt =-rslt
  end function
  function psp_plus_list(obj,list) result(rslt)
    !(realknd2!) :: rslt
    include 'psp_function_list.h90'
    rslt = obj%p(arg)%E - obj%p(arg)%V(3)
  end function
  function psp_minus_list(obj,list) result(rslt)
    !(realknd2!) :: rslt
    include 'psp_function_list.h90'
    rslt = obj%p(arg)%E + obj%p(arg)%V(3)
  end function
  function psp_theta_list(obj,list) result(rslt)
    !(realknd2!) :: rslt
    include 'psp_function_list.h90'
    if (obj%p(arg)%AV.le.rZRO) then
      rslt = 0
    else
      rslt = acos(obj%p(arg)%V(3)/obj%p(arg)%AV)
      rslt = rslt/r1PI*180
    endif
  end function
  function psp_phi_list(obj,list) result(rslt)
    !(realknd2!) :: rslt
    include 'psp_function_list.h90'
    if (obj%p(arg)%Tsq.le.rZRO) then
      if (obj%p(arg)%V(3).ge.rZRO) then ;rslt = 0
                                        else ;rslt = r1PI
      endif
    else
      rslt = acos(obj%p(arg)%V(1)/sqrt(obj%p(arg)%Tsq))
      if (obj%p(arg)%V(2).lt.rZRO) rslt=r2PI-rslt
    endif
  end function
  function psp_deltaR_list(obj,list1,list2) result(rslt)
    !(realknd2!) :: rslt
    include 'psp_function_list2.h90'
    rslt = abs( obj%phi(arg1) - obj%phi(arg2) )
    rslt = min( rslt ,r2PI-rslt )
    rslt = sqrt( rslt*rslt + (obj%rapidity(arg1)-obj%rapidity(arg2))**2 )
  end function
  function psp_deltaPhi_list(obj,list1,list2) result(rslt)
    !(realknd2!) :: rslt
    include 'psp_function_list2.h90'
    rslt = abs( obj%phi(arg1) - obj%phi(arg2) )
    rslt = min( rslt ,r2PI-rslt )
  end function
  function psp_angle_list(obj,list1,list2) result(rslt)
    !(realknd2!) :: rslt
    include 'psp_function_list2.h90'
    if (obj%p(arg1)%AV.le.rZRO.or.obj%p(arg2)%AV.le.rZRO) then
      rslt = 0
    else
      rslt = ( obj%p(arg1)%V(1)*obj%p(arg2)%V(1) &
              +obj%p(arg1)%V(2)*obj%p(arg2)%V(2) &
              +obj%p(arg1)%V(3)*obj%p(arg2)%V(3) ) &
            /( obj%p(arg1)%AV  *obj%p(arg2)%AV )
      rslt = acos(rslt)/r1PI*180
    endif
  end function


  function psp_Energy(obj,arg) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt
    integer,intent(in) :: arg
    rslt = obj%p(arg)%E
  end function
  function psp_Vector(obj,arg) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt(3)
    integer,intent(in) :: arg
    rslt = obj%p(arg)%V
  end function
  function psp_Trans(obj,arg) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt
    integer,intent(in) :: arg
    rslt = sqrt(obj%p(arg)%Tsq)
  end function
  function psp_Etrans(obj,arg) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt
    integer,intent(in) :: arg
    rslt = sqrt(obj%p(arg)%Tsq+obj%p(arg)%S)
  end function
  function psp_Square(obj,arg) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt
    integer,intent(in) :: arg
    rslt = obj%p(arg)%S
  end function
  function psp_Mass(obj,arg) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt
    integer,intent(in) :: arg
    rslt = obj%p(arg)%M
  end function
  function psp_AbsVec(obj,arg) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt
    integer,intent(in) :: arg
    rslt = obj%p(arg)%AV
  end function
  function psp_rapidity(obj,arg) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt,mTsq
    integer,intent(in) :: arg
    mTsq = obj%p(arg)%S + obj%p(arg)%Tsq
    if (mTsq.gt.rZRO) then
      rslt = obj%p(arg)%E + abs(obj%p(arg)%V(3))
      rslt = log( rslt*rslt/mTsq )/2
    else
      rslt = 99
    endif
    if (obj%p(arg)%V(3).lt.rZRO) rslt =-rslt
  end function
  function psp_pseudoRap(obj,arg) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt
    integer,intent(in) :: arg
    if (obj%p(arg)%Tsq.gt.rZRO) then
      rslt = obj%p(arg)%AV + abs(obj%p(arg)%V(3))
      rslt = log( rslt*rslt/obj%p(arg)%Tsq )/2
    else
      rslt = 99
    endif
    if (obj%p(arg)%V(3).lt.rZRO) rslt =-rslt
  end function
  function psp_plus(obj,arg) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt
    integer,intent(in) :: arg
    rslt = obj%p(arg)%E - obj%p(arg)%V(3)
  end function
  function psp_minus(obj,arg) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt
    integer,intent(in) :: arg
    rslt = obj%p(arg)%E + obj%p(arg)%V(3)
  end function
  function psp_theta(obj,arg) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt
    integer,intent(in) :: arg
    if (obj%p(arg)%AV.le.rZRO) then
      rslt = 0
    else
      rslt = acos(obj%p(arg)%V(3)/obj%p(arg)%AV)
      rslt = rslt/r1PI*180
    endif
  end function
  function psp_phi(obj,arg) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt
    integer,intent(in) :: arg
    if (obj%p(arg)%Tsq.le.rZRO) then
      if (obj%p(arg)%V(3).ge.rZRO) then ;rslt = 0
                                   else ;rslt = r1PI
      endif
    else
      rslt = acos(obj%p(arg)%V(1)/sqrt(obj%p(arg)%Tsq))
      if (obj%p(arg)%V(2).lt.rZRO) rslt=r2PI-rslt
    endif
  end function
  function psp_deltaR(obj,arg1,arg2) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt
    integer,intent(in) :: arg1,arg2
    rslt = abs( obj%phi(arg1) - obj%phi(arg2) )
    rslt = min( rslt ,r2PI-rslt )
    rslt = sqrt( rslt*rslt + (obj%rapidity(arg1)-obj%rapidity(arg2))**2 )
  end function
  function psp_deltaPhi(obj,arg1,arg2) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt
    integer,intent(in) :: arg1,arg2
    rslt = abs( obj%phi(arg1) - obj%phi(arg2) )
    rslt = min( rslt ,r2PI-rslt )
  end function
  function psp_angle(obj,arg1,arg2) result(rslt)
    class(psPoint_type),intent(in) :: obj
    !(realknd2!) :: rslt
    integer,intent(in) :: arg1,arg2
    if (obj%p(arg1)%AV.le.rZRO.or.obj%p(arg2)%AV.le.rZRO) then
      rslt = 0
    else
      rslt = ( obj%p(arg1)%V(1)*obj%p(arg2)%V(1) &
              +obj%p(arg1)%V(2)*obj%p(arg2)%V(2) &
              +obj%p(arg1)%V(3)*obj%p(arg2)%V(3) ) &
            /( obj%p(arg1)%AV  *obj%p(arg2)%AV )
      rslt = acos(rslt)/r1PI*180
    endif
  end function


  function psp_frmt03(obj,ii) result(rslt)
  class(psPoint_type),intent(in) :: obj
  integer,intent(in) :: ii
  integer :: jj
  !(realknd2!) :: rslt(0:3)
  jj = obj%b(ii)
  rslt(0) = obj%p(jj)%E
  rslt(1:3) = obj%p(jj)%V(1:3)
  end function

  function psp_fnst(obj) result(rslt)
  class(psPoint_type),intent(in) :: obj
  integer :: ii,jj
  !(realknd2!) :: rslt(0:3,obj%Nfinst)
  do ii=1,obj%Nfinst
    jj = obj%b(ii)
    rslt(0  ,ii) = obj%p(jj)%E
    rslt(1:3,ii) = obj%p(jj)%V(1:3)
  enddo
  end function

  function psp_inst(obj) result(rslt)
  class(psPoint_type),intent(in) :: obj
  integer :: ii,jj
  !(realknd2!) :: rslt(0:3,-2:-1)
  do ii=-2,-1
    jj = obj%b(ii)
    rslt(0  ,ii) = obj%p(jj)%E
    rslt(1:3,ii) = obj%p(jj)%V(1:3)
  enddo
  end function




  function realMom_zero() result(q)
  type(realMom_type) :: q
  q%V = 0
  q%E = 0
  q%S = 0
  q%M = 0
  q%Tsq = 0
  q%AV = 0
  q%Small = .true.
  end function


  function realMom_minus(p) result(q)
  class(realMom_type),intent(in) :: p
  type(realMom_type) :: q
  q%V =-p%V
  q%E =-p%E
  q%S = p%S
  q%M = p%M  
  q%Tsq = p%Tsq
  q%AV = p%AV
  q%Small = p%Small
  end function


  function realMom_i_p(xx,p) result(r)
  type(realMom_type),intent(in) :: p
  integer,intent(in) :: xx
  type(realMom_type) :: r
  integer :: aa,x2
  include 'realMom_scamul.h90'
  end function
  
  function realMom_p_i(p,xx) result(r)
  type(realMom_type),intent(in) :: p
  integer,intent(in) :: xx
  type(realMom_type) :: r
  integer :: aa,x2
  include 'realMom_scamul.h90'
  end function
  
  function realMom_r_p(xx,p) result(r)
  type(realMom_type),intent(in) :: p
  !(realknd2!),intent(in) :: xx
  type(realMom_type) :: r
  !(realknd2!) :: aa,x2
  include 'realMom_scamul.h90'
  end function
  
  function realMom_p_r(p,xx) result(r)
  type(realMom_type),intent(in) :: p
  !(realknd2!),intent(in) :: xx
  type(realMom_type) :: r
  !(realknd2!) :: aa,x2
  include 'realMom_scamul.h90'
  end function
  
  function realMom_subtract(p,q) result(r)
!***********************************************************************
!***********************************************************************
  type(realMom_type),intent(in) :: p,q
  type(realMom_type) :: r
  r = realMom_add(p,realMom_minus(q))
  end function

  function realMom_add(p,q) result(r)
!***********************************************************************
!***********************************************************************
  type(realMom_type),intent(in) :: p,q
  type(realMom_type) :: r
  !(realknd2!) :: hh,pE,qE,pv(3),qv(3),V3V3
  r%V = p%V + q%V
  r%E = p%E + q%E
  r%Tsq = r%V(1)*r%V(1) + r%V(2)*r%V(2)
  V3V3 = r%V(3)*r%V(3)
  r%AV = sqrt( V3V3 + r%Tsq )
  if (p%S.eq.rZRO.and.q%S.eq.rZRO) then
    if (r%Tsq.gt.V3V3) then
      r%S = (r%E-r%AV)*(r%E+r%AV)
    else
      r%S = (r%E-r%V(3))*(r%E+r%V(3)) - r%Tsq
    endif
  elseif (p%Small.and.q%Small) then
    if (p%E.ge.rZRO) then ;pE= p%E ;pv= p%V
                     else ;pE=-p%E ;pv=-p%V
    endif
    if (q%E.ge.rZRO) then ;qE= q%E ;qv= q%V
                     else ;qE=-q%E ;qv=-q%V
    endif
    if (pv(3)*qv(3).le.rZRO) then
      hh = ( p%AV-pv(3) )*( q%AV+qv(3) ) &
         + ( p%AV+pv(3) )*( q%AV-qv(3) )
    else
      hh = ( p%AV+abs(pv(3)) )/( q%AV+abs(qv(3)) )
      hh = p%Tsq/hh + q%Tsq*hh
    endif
    r%S = hh - 2*( pv(1)*qv(1) + pv(2)*qv(2) )
    hh = ( pE+p%AV )/( qE+q%AV )
    if (p%E*q%E.ge.rZRO) then
      r%S = p%S + q%S + p%S/hh + q%S*hh + r%S
    else
      r%S = p%S + q%S - p%S/hh - q%S*hh - r%S
    endif
  else
    if (r%Tsq.gt.V3V3) then
      r%S = (r%E-r%AV)*(r%E+r%AV)
    else
      r%S = (r%E-r%V(3))*(r%E+r%V(3)) - r%Tsq
    endif
  endif
  r%M = abs(r%S)
  r%Small = (r%M.lt.small*r%AV*r%AV)
  r%M = sqrt(r%M)
  end function

 
  function psp_lambda(obj,i1,i2,i3) result(rslt)
!***********************************************************************
!  (p^2)^2 + (q^2)^2 + (r^2)^2 - 2*p^2*q^2 - 2*q^2*r^2 - 2*r^2*p^2
!***********************************************************************
  class(psPoint_type),intent(in) :: obj
  integer,intent(in) :: i1,i2,i3
  !(realknd2!) :: rslt
  rslt = realMom_lambda( obj%p(i1) ,obj%p(i2) ,obj%p(i3) )
  end function

  function realMom_lambda(p,q,r) result(rslt)
!***********************************************************************
!  (p^2)^2 + (q^2)^2 + (r^2)^2 - 2*p^2*q^2 - 2*q^2*r^2 - 2*r^2*p^2
!***********************************************************************
  type(realMom_type),intent(in) :: p,q,r
  !(realknd2!) :: rslt
  if (p%S.ge.rZRO) then
    if (q%S.ge.rZRO) then
      if (r%S.ge.rZRO) then ! +++
        if (q%M.ge.r%M) then
          rslt = ((p%M-q%M)-r%M) * (p%M+q%M+r%M) &
               * (p%M+(q%M-r%M)) * ((p%M-q%M)+r%M)
        else
          rslt = ((p%M-r%M)-q%M) * (p%M+q%M+r%M) &
               * (p%M+(q%M-r%M)) * ((p%M-q%M)+r%M)
        endif
      else ! ++-
        rslt = (p%M-q%M)*(p%M+q%M)
        rslt =  rslt*rslt  + r%S*( r%S - 2*(p%S+q%S) )
      endif
    else ! +-+
      if (r%S.ge.rZRO) then
        rslt = (p%M-r%M)*(p%M+r%M)
        rslt =  rslt*rslt + q%S*( q%S - 2*(p%S+r%S) )
      else ! +--
        rslt = (r%M-q%M)*(r%M+q%M)
        rslt =  rslt*rslt + p%S*( p%S - 2*(r%S+q%S) )
      endif
    endif
  else
    if (q%S.ge.rZRO) then
      if (r%S.ge.rZRO) then ! -++
        rslt = (r%M-q%M)*(r%M+q%M)
        rslt =  rslt*rslt + p%S*( p%S - 2*(r%S+q%S) )
      else ! -+-
        rslt = (p%M-r%M)*(p%M+r%M)
        rslt =  rslt*rslt + q%S*( q%S - 2*(p%S+r%S) )
      endif
    else ! --+
      if (r%S.ge.rZRO) then ! --+
        rslt = (p%M-q%M)*(p%M+q%M)
        rslt =  rslt*rslt + r%S*( r%S - 2*(p%S+q%S) )
      else ! ---
        if (q%M.ge.r%M) then
          rslt = ((p%M-q%M)-r%M) * (p%M+q%M+r%M) &
               * (p%M+(q%M-r%M)) * ((p%M-q%M)+r%M)
        else
          rslt = ((p%M-r%M)-q%M) * (p%M+q%M+r%M) &
               * (p%M+(q%M-r%M)) * ((p%M-q%M)+r%M)
        endif
      endif
    endif
  endif
  end function
          
  subroutine realMom_complete( p )
!********************************************************************
!********************************************************************
  class(realMom_type) :: p
  !(realknd2!) :: ee
  p%Tsq = p%V(1)*p%V(1) + p%V(2)*p%V(2)
  p%AV = p%V(3)*p%V(3) + p%Tsq
  ee = p%AV + p%S
  if (ee.lt.0) then
    if (errru.ge.0) write(errru,*) 'ERROR in avh_kinematics realMom_complete: ' &
      ,'negative squared energy, taking absolute value'
    ee =-ee
  endif
  p%E = sign(sqrt(ee),p%E)
  p%Small = (p%S.lt.small*p%AV)
  p%AV = sqrt(p%AV)
  end subroutine 

  function boost2( p ,q,k ,calc_square ) result(r)
!********************************************************************
!* apply on p the boost that transforms k to q assuming k^2=q^2
!*    g^mu_nu - 2*(q+k)^mu*(q+k)_nu/(q+k)^2 + 2*q^mu*k_nu/q^2
!* to get r
!********************************************************************
  class(realMom_type),intent(in) :: p
  type(realMom_type),intent(in) :: q,k
  logical,intent(in) :: calc_square
  type(realMom_type) :: r
  !(realknd2!) :: qk(0:3),sqk,bb,cc
  save :: qk,sqk
  r%M = p%M
  r%S = p%S
  if (calc_square) then
    qk(0) = q%E + k%E
    qk(1:3) = q%V(1:3) + k%V(1:3)
    sqk = (qk(0)-qk(3))*(qk(0)+qk(3)) - qk(1)*qk(1) - qk(2)*qk(2)
  endif
  bb = 2*( qk(0)*p%E - qk(1)*p%V(1) - qk(2)*p%V(2) - qk(3)*p%V(3) )/sqk
  cc = 2*( k%E*p%E - k%V(1)*p%V(1)- k%V(2)*p%V(2) - k%V(3)*p%V(3) )/q%S
  r%E      = p%E      - bb*qk(0)   + cc*q%E
  r%V(1:3) = p%V(1:3) - bb*qk(1:3) + cc*q%V(1:3)
  call r%complete
  end function

  function boost1( p ,q ) result(r)
!********************************************************************
!* apply on p the boost that boosts (mq,0,0,0) to q, to get r
!********************************************************************
  class(realMom_type),intent(in) :: p
  type(realMom_type),intent(in) :: q
  type(realMom_type) :: r
  !(realknd2!) :: bb
  r%M = p%M
  r%S = p%S
  r%E = ( p%E*q%E + p%V(1)*q%V(1) + p%V(2)*q%V(2) + p%V(3)*q%V(3) )/q%M
  bb = (p%E + r%E)/(q%E + q%M)
  r%V(1:3) = p%V(1:3) + bb*q%V(1:3)
  call r%complete
  end function

  subroutine boost_sub( q ,pE,p1,p2,p3 )
!********************************************************************
!* apply on p the boost that boosts (q%M,0,0,0) to q
!********************************************************************
  class(realMom_type),intent(in) :: q
  !(realknd2!) :: pE,p1,p2,p3
  !(realknd2!) :: aa,bb
  aa = ( pE*q%E + p1*q%V(1) + p2*q%V(2) + p3*q%V(3) )/q%M
  bb = (pE + aa)/(q%E + q%M)
  pE = aa
  p1 = p1 + bb*q%V(1)
  p2 = p2 + bb*q%V(2)
  p3 = p3 + bb*q%V(3)
  end subroutine

  function tsoob( p ,q ) result(r)
!********************************************************************
!* apply on p the boost that boosts q to (mq,0,0,0), to get r
!********************************************************************
  class(realMom_type),intent(in) :: p
  type(realMom_type),intent(in) :: q
  type(realMom_type) :: r
  !(realknd2!) :: bb
  r%M = p%M
  r%S = p%S
  r%E = ( p%E*q%E - p%V(1)*q%V(1) - p%V(2)*q%V(2) - p%V(3)*q%V(3) )/q%M
  bb = (p%E + r%E)/(q%E + q%M)
  r%V(1:3) = p%V(1:3) - bb*q%V(1:3)
  call r%complete
  end function

  subroutine tsoob_sub( q ,pE,p1,p2,p3 )
!********************************************************************
!* apply on p the boost that boosts q to (q%M,0,0,0)
!********************************************************************
  class(realMom_type),intent(in) :: q
  !(realknd2!) :: pE,p1,p2,p3
  !(realknd2!) :: aa,bb
  aa = ( pE*q%E - p1*q%V(1) - p2*q%V(2) - p3*q%V(3) )/q%M
  bb = (pE + aa)/(q%E + q%M)
  pE = aa
  p1 = p1 - bb*q%V(1)
  p2 = p2 - bb*q%V(2)
  p3 = p3 - bb*q%V(3)
  end subroutine

  function rotate_z( p, q ) result(r)
!********************************************************************
!* apply on  p%V  the inverse of the rotation
!* that rotates  q%V  to the z-axis, to get  r
!********************************************************************
  class(realMom_type),intent(in) :: p
  type(realMom_type),intent(in),optional :: q
  type(realMom_type) :: r
  !(realknd2!) :: stsp,stcp,ct,st,sp,cp,ctsp,ctcp
  save :: stsp,stcp,ct,st,sp,cp,ctsp,ctcp
  if (present(q)) then
    stsp = q%V(1)/q%AV
    stcp = q%V(2)/q%AV
    ct   = q%V(3)/q%AV
    st   = sqrt(stsp*stsp + stcp*stcp)
    if (st.ne.rZRO) then
      sp = stsp/st
      cp = stcp/st
    else
      sp = rZRO
      cp = rONE
    endif
    ctsp = ct*sp
    ctcp = ct*cp
  endif
  r%E = p%E
  r%M = p%M
  r%S = p%S
  r%AV = p%AV
  r%Small  = p%Small
  r%V(1) =  cp*p%V(1) + ctsp*p%V(2) + stsp*p%V(3)
  r%V(2) = -sp*p%V(1) + ctcp*p%V(2) + stcp*p%V(3)
  r%V(3) =            - st  *p%V(2) + ct  *p%V(3)
  r%Tsq = r%V(1)*r%V(1) + r%V(2)*r%V(2)
  end function


  subroutine sort_big( aa ,xx ,nn )
!******************************************************************
! sorts xx from large to small
! aa is the permutation along with it
!******************************************************************
  integer     ,intent(in   ) :: nn
  !(realknd2!),intent(inout) :: xx(nn)
  integer     ,intent(out  ) :: aa(nn)
  integer :: ll,ii,jj,ir,aab
  !(realknd2!) :: xxb
  do ii=1,nn
    aa(ii) = ii
  enddo
  if (nn.le.1) return
  ll = nn/2 + 1
  ir = nn
  do
    if (ll.gt.1) then 
      ll = ll - 1
      xxb = xx(ll)
      aab = aa(ll)
    else
      xxb = xx(ir)
      xx(ir) = xx(1)
      aab = aa(ir)
      aa(ir) = aa(1)
      ir = ir - 1
      if (ir.eq.1) then
        xx(1) = xxb
        aa(1) = aab
        exit
      endif
    endif
    ii = ll
    jj = ll + ll
    do while (jj.le.ir)
      if (jj.lt.ir) then
!        if (xx(jj).lt.xx(jj+1)) jj = jj + 1
        if (xx(jj).gt.xx(jj+1)) jj = jj + 1
      endif
!      if (xxb.lt.xx(jj)) then
      if (xxb.gt.xx(jj)) then
        xx(ii) = xx(jj)
        aa(ii) = aa(jj)
        ii = jj
        jj = jj + jj
      else
        jj = ir + 1
      endif
    enddo
    xx(ii) = xxb
    aa(ii) = aab
  enddo
!
  end subroutine


  subroutine order_big2small( ll ,aa )
  integer,intent(out) :: ll(:)
  !(realknd2!),intent(in) :: aa(:)
  integer :: ii,jj,nn,lb
  !(realknd2!) :: bb
  nn = size(aa,1)
  ll(1:nn) = unity(1:nn)
  do ii=nn-1,1,-1
    lb = ll(ii)
    bb = aa(lb)
    jj = ii+1
    do
      if (jj.gt.nn) exit
      if (aa(ll(jj)).le.bb) exit 
      ll(jj-1) = ll(jj)
      jj = jj+1
    enddo
    ll(jj-1) = lb
  enddo
  end subroutine

  subroutine sort_big2small( aa )
  real(kind(1d0)),intent(inout) :: aa(:)
  integer :: ii,jj,nn
  real(kind(1d0)) :: bb
  nn = size(aa,1)
  do ii=nn-1,1,-1
    bb = aa(ii)
    jj = ii+1
    do
      if (jj.gt.nn) exit
      if (aa(jj).le.bb) exit 
      aa(jj-1) = aa(jj)
      jj = jj+1
    enddo
    aa(jj-1) = bb
  enddo
  end subroutine

  subroutine sort_small2big( aa )
  real(kind(1d0)),intent(inout) :: aa(:)
  integer :: ii,jj,nn
  real(kind(1d0)) :: bb
  nn = size(aa,1)
  do ii=2,nn
    bb = aa(ii)
    jj = ii-1
    do
      if (jj.lt.1) exit
      if (aa(jj).le.bb) exit
      aa(jj+1) = aa(jj)
      jj = jj-1
    enddo
    aa(jj+1) = bb
  enddo
  end subroutine


!  subroutine process_put( proc ,vali,valf )
!  class(process_type) :: proc
!  integer,intent(in) :: vali(-2:-1),valf(:)
!  proc%Nfinst = ubound(valf,1)
!  proc%Nxtrn = proc%Nfinst+2
!  proc%infi(-2:-1) = vali(-2:-1)
!  proc%infi(1:proc%Nfinst) = valf(1:proc%Nfinst)
!  proc%xtrn(1)               = proc%infi(-1)
!  proc%xtrn(2:proc%Nfinst+1) = proc%infi(1:proc%Nfinst)
!  proc%xtrn(proc%Nfinst+2)   = proc%infi(-2)
!  end subroutine

  subroutine process_put( proc ,vali,valf ,anti )
  class(process_type) :: proc
  integer,intent(in) :: vali(-2:-1),valf(:)
  procedure(process_interface) :: anti
  proc%Nfinst = ubound(valf,1)
  proc%Nxtrn = proc%Nfinst+2
  proc%infi(-2:-1) = vali(-2:-1)
  proc%infi(1:proc%Nfinst)   = valf(1:proc%Nfinst)
  proc%xtrn(1)               = anti( proc%infi(-1) )
  proc%xtrn(2:proc%Nfinst+1) = proc%infi(1:proc%Nfinst)
  proc%xtrn(proc%Nfinst+2)   = anti( proc%infi(-2) )
  end subroutine

  subroutine complete_infi( proc )
! does not take into account change of sign
  class(process_type) :: proc
  proc%Nfinst = proc%Nxtrn-2
  proc%infi(-1)            = proc%xtrn(1)
  proc%infi(1:proc%Nfinst) = proc%xtrn(2:proc%Nfinst+1)
  proc%infi(-2)            = proc%xtrn(proc%Nfinst+2)
  end subroutine

  subroutine complete_xtrn( proc )
! does not take into account change of sign
  class(process_type) :: proc
  proc%Nxtrn = proc%Nfinst+2
  proc%xtrn(1)               = proc%infi(-1)
  proc%xtrn(2:proc%Nfinst+1) = proc%infi(1:proc%Nfinst)
  proc%xtrn(proc%Nfinst+2)   = proc%infi(-2)
  end subroutine

  function process_abs( proc ) result(rslt)
  class(process_type) :: proc
  type(process_type) :: rslt
  rslt%Nxtrn = proc%Nxtrn
  rslt%xtrn(1:proc%Nxtrn) = abs(proc%xtrn(1:proc%Nxtrn))
  rslt%Nfinst = proc%Nfinst
  rslt%infi(-2:proc%Nfinst) = abs(proc%infi(-2:proc%Nfinst))
  end function

  function process_apply( proc ,apply) result(rslt)
  class(process_type) :: proc
  type(process_type) :: rslt
  procedure(process_interface) :: apply
  integer :: ii
  rslt%Nxtrn = proc%Nxtrn
  do ii=1,proc%Nxtrn
    rslt%xtrn(ii) = apply( proc%xtrn(ii) )
  enddo
  rslt%Nfinst = proc%Nfinst
  rslt%infi(-2) = apply( proc%infi(-2) )
  rslt%infi(-1) = apply( proc%infi(-1) )
  do ii=1,proc%Nfinst
    rslt%infi(ii) = apply( proc%infi(ii) )
  enddo
  end function

  subroutine process_print( proc ,iunit )
  class(process_type) :: proc
  integer,intent(in),optional :: iunit
  character(144) :: aa
  integer :: nunit,ii
  nunit = stdout
  if (present(iunit)) nunit = iunit 
  aa = trim(adjustl(prnt(proc%infi(-2))))
  aa = trim(aa)//' '//trim(adjustl(prnt(proc%infi(-1))))
  aa = trim(aa)//' ->'
  do ii=1,proc%Nfinst
    aa = trim(aa)//' '//trim(adjustl(prnt(proc%infi(ii))))
  enddo
  write(nunit,*) trim(aa)
  end subroutine

end module


