module avh_kinematics
  use avh_iounits
  use avh_mathcnst
  use avh_doloops
  use avh_kskeleton
  use avh_prnt
  implicit none
  private
  public :: base ! propagate from avh_kskeleton
  public :: realMom_type,calc_all
  public :: put_realMom,zero_realMom,minus_realMom,add_realMom
  public :: lambda_realMom,prnt_realMom,complete_realMom

  interface calc_all
    module procedure calc_all,calc_all_new
  end interface

  type :: realMom_type
    logical :: Small
    !(realknd2!) :: Energy,VecCmp(3)
    !(realknd2!) :: Mass,MassSq,AbsVec,TranSq
  end type


  type :: momTree_type
    integer :: N,o
    !(integer1!),allocatable :: p1(:),p2(:)
  end type

  integer,parameter :: treeSize(1:15)= &
    (/1,1,1,6,10,35,56,154,246,627,1012,2497,4082,9893,16368/)

  type(momTree_type),allocatable,save :: momTree(:)

  logical,save :: initz=.true.
  integer,save :: NxtrnMax=0
  !(realknd2!),save :: small

contains


  subroutine init
  call init_mathcnst
  call init_kskeleton
  initz = .false.
  small = 1d-4
  end subroutine

  subroutine set_small( xx )
  !(realknd2!),intent(in) :: xx
  if (initz) call init
  small = abs(xx)
  end subroutine

  subroutine update_momTree( Nxtrn )
  integer,intent(in) :: Nxtrn
  integer :: ii
  if (initz) call init
  if (allocated(momTree)) then
    if (Nxtrn.le.NxtrnMax) then
      return
    else
      deallocate(momTree)
    endif
  endif
  NxtrnMax = Nxtrn
  allocate(momTree(1:NxtrnMax))
  do ii=4,Nxtrn
    call fill_tree( momTree(ii) ,ii )
  enddo
  end subroutine

  subroutine fill_tree( t ,Nxtrn )
  type(momTree_type),intent(out) :: t
  integer           ,intent(in)  :: Nxtrn
  integer :: Nvtx,ii,p1,kk,p2
  integer :: aa(Nxtrn)
  type(grepen_type) :: loopa
  allocate(t%p1(1:treeSize(Nxtrn)))
  allocate(t%p2(1:treeSize(Nxtrn)))
  t%o = base(Nxtrn+1)-1
  t%N = 0
  do ii=2,Nxtrn/2
    call grepen_init(loopa,ii,aa,1,Nxtrn)
    do
      p1 = 0
      kk = 0
      do ;kk=kk+1
        p1 = p1 + base(aa(kk))
        if (kk.eq.ii/2) exit
      enddo
      p2 = 0
      do ;kk=kk+1
        p2 = p2 + base(aa(kk))
        if (kk.eq.ii) exit
      enddo
      t%N = t%N+1
      t%p2(t%N) = p2
      t%p1(t%N) = p1
      if (grepen_exit(loopa,aa)) exit
    enddo
  enddo
  end subroutine


  function prnt_realMom(p,Ndec) result(rslt)
  type(realMom_type),intent(in) :: p
  integer           ,intent(in) :: Ndec
  character(5*(8+Ndec)-1) :: rslt
  character(4*(8+Ndec)-1) :: gg
  character(   8+Ndec   ) :: hh
  gg = prnt((/p%Energy,p%VecCmp(1),p%VecCmp(2),p%VecCmp(3)/),Ndec)
  hh = prnt(p%Mass,Ndec)
  if (p%MassSq.lt.rZRO) hh(1:1) = 'i'
  rslt = gg//' '//hh
  end function


  subroutine calc_all( p ,Nfinst ,pXtrn,sXtrn )
  integer,intent(in) :: Nfinst
  type(realMom_type),intent(out) :: p(base(Nfinst+3)-1)
  !(realknd2!),intent(in) :: pXtrn(0:3,-2:Nfinst),sXtrn(-2:Nfinst)
  integer :: ii,oo,i1,i2,nn
!
  nn = Nfinst+2
!
  if (nn.gt.NxtrnMax) call update_momTree(nn)
!
  oo = momTree(nn)%o
  p(   base(nn)) = put_realMom( pXtrn(:,-2) ,sXtrn(-2) ,.false. )
  p(oo-base(nn)) = minus_realMom( p(base(nn)) )
  p(   base( 1)) = put_realMom( pXtrn(:,-1) ,sXtrn(-1) ,.false. )
  p(oo-base( 1)) = minus_realMom( p(base( 1)) )
  do ii=1,Nfinst
    p(   base(1+ii)) = put_realMom( pXtrn(:,ii) ,sXtrn(ii) ,.true. )
    p(oo-base(1+ii)) = minus_realMom( p(base(1+ii)) )
  enddo
  do ii=1,momTree(nn)%N
    i1 = momTree(nn)%p1(ii)
    i2 = momTree(nn)%p2(ii)
    p(   i1+i2) = add_realMom( p(i1),p(i2) )
    p(oo-i1-i2) = minus_realMom( p(i1+i2) )
  enddo
  p(base(Nfinst+3)-1) = zero_realMom()
!
  end subroutine


  subroutine calc_all_new( p ,Nfinst )
  integer,intent(in) :: Nfinst
  type(realMom_type),intent(inout) :: p(base(Nfinst+3)-1)
  integer :: ii,oo,i1,i2,nn
!
  nn = Nfinst+2
!
  if (nn.gt.NxtrnMax) call update_momTree(nn)
!
  oo = momTree(nn)%o
  p(oo-base(nn)) = minus_realMom( p(base(nn)) )
  p(oo-base( 1)) = minus_realMom( p(base( 1)) )
  do ii=1,Nfinst
    p(oo-base(1+ii)) = minus_realMom( p(base(1+ii)) )
  enddo
  do ii=1,momTree(nn)%N
    i1 = momTree(nn)%p1(ii)
    i2 = momTree(nn)%p2(ii)
    p(   i1+i2) = add_realMom( p(i1),p(i2) )
    p(oo-i1-i2) = minus_realMom( p(i1+i2) )
  enddo
  p(base(Nfinst+3)-1) = zero_realMom()
!
  end subroutine


  function put_realMom( pXtrn,sXtrn ,onShell ) result(q)
!***********************************************************************
! Only uses the sign of pXtrn(0) if onShell=.true.,
!   Energy = sign( pXtrn(0) )*sqrt( sXtrn + pXtrn(1:3)**2 )
!***********************************************************************
  !(realknd2!),intent(in) :: pXtrn(0:3),sXtrn
  logical,intent(in) :: onShell
  type(realMom_type) :: q
  q%VecCmp(1:3) = pXtrn(1:3)
  q%MassSq = sXtrn
  q%TranSq = q%VecCmp(1)*q%VecCmp(1) + q%VecCmp(2)*q%VecCmp(2)
  q%AbsVec = q%VecCmp(3)*q%VecCmp(3) + q%TranSq
  if (onShell) then
    q%Energy = q%AbsVec + q%MassSq
    if (q%Energy.lt.0) then
      if (errru.ge.0) write(errru,*) 'ERROR in avh_kinematics put_realMom: ' &
        ,'negative squared energy, taking absolute value'
      q%Energy =-q%Energy
    endif
    q%Energy = sign(sqrt(q%Energy),pXtrn(0))
  else
    q%Energy = pXtrn(0)
  endif
  q%Mass  = abs(q%MassSq)
  q%Small = (q%Mass.lt.small*q%AbsVec)
  q%Mass   = sqrt(q%Mass)
  q%AbsVec = sqrt(q%AbsVec)
  end function


  subroutine complete_realMom( q ,onShell )
!***********************************************************************
!***********************************************************************
  type(realMom_type),intent(inout) :: q
  logical,intent(in) :: onShell
  !(realknd2!) :: sgn
  q%TranSq = q%VecCmp(1)*q%VecCmp(1) + q%VecCmp(2)*q%VecCmp(2)
  q%AbsVec = q%VecCmp(3)*q%VecCmp(3) + q%TranSq
  if (onShell) then
    sgn = q%Energy
    q%Energy = q%AbsVec + q%MassSq
    if (q%Energy.lt.0) then
      if (errru.ge.0) write(errru,*) 'ERROR in avh_kinematics put_realMom: ' &
        ,'negative squared energy, taking absolute value'
      q%Energy =-q%Energy
    endif
    q%Energy = sign(sqrt(q%Energy),sgn)
  endif
  q%Small = (q%MassSq.lt.small*q%AbsVec)
  q%AbsVec = sqrt(q%AbsVec)
  end subroutine


  function zero_realMom() result(q)
  type(realMom_type) :: q
  q%VecCmp = 0
  q%Energy = 0
  q%MassSq = 0
  q%Mass   = 0
  q%TranSq = 0
  q%AbsVec = 0
  q%Small  = .true.
  end function


  function minus_realMom(p) result(q)
  type(realMom_type),intent(in) :: p
  type(realMom_type) :: q
  q%VecCmp =-p%VecCmp
  q%Energy =-p%Energy
  q%MassSq = p%MassSq
  q%Mass   = p%Mass  
  q%TranSq = p%TranSq
  q%AbsVec = p%AbsVec
  q%Small  = p%Small
  end function


  function add_realMom(p,q) result(r)
!***********************************************************************
!***********************************************************************
  type(realMom_type),intent(in) :: p,q
  type(realMom_type) :: r
  !(realknd2!) :: hh,pE,qE,pv(3),qv(3)
  r%VecCmp = p%VecCmp + q%VecCmp
  r%Energy = p%Energy + q%Energy
  r%TranSq = r%VecCmp(1)*r%VecCmp(1) + r%VecCmp(2)*r%VecCmp(2)
  r%AbsVec = sqrt( r%VecCmp(3)*r%VecCmp(3) + r%TranSq )
  if (p%MassSq.eq.rZRO.and.q%MassSq.eq.rZRO) then
    r%MassSq = (r%Energy-r%AbsVec)*(r%Energy+r%AbsVec)
  elseif (p%Small.and.q%Small) then
    if (p%Energy.ge.rZRO) then ;pE= p%Energy ;pv= p%VecCmp
                          else ;pE=-p%Energy ;pv=-p%VecCmp
    endif
    if (q%Energy.ge.rZRO) then ;qE= q%Energy ;qv= q%VecCmp
                          else ;qE=-q%Energy ;qv=-q%VecCmp
    endif
    if (pv(3)*qv(3).le.rZRO) then
      hh = ( p%AbsVec-pv(3) )*( q%AbsVec+qv(3) ) &
         + ( p%AbsVec+pv(3) )*( q%AbsVec-qv(3) )
    else
      hh = ( p%AbsVec+abs(pv(3)) )/( q%AbsVec+abs(qv(3)) )
      hh = p%TranSq/hh + q%TranSq*hh
    endif
    r%MassSq = hh - 2*( pv(1)*qv(1) + pv(2)*qv(2) )
    hh = ( pE+p%AbsVec )/( qE+q%AbsVec )
    if (p%Energy*q%Energy.ge.rZRO) then
      r%MassSq = p%MassSq + q%MassSq + p%MassSq/hh + q%MassSq*hh + r%MassSq
    else
      r%MassSq = p%MassSq + q%MassSq - p%MassSq/hh - q%MassSq*hh - r%MassSq
    endif
  else
    r%MassSq = (r%Energy-r%AbsVec)*(r%Energy+r%AbsVec)
  endif
  r%Mass = abs(r%MassSq)
  r%Small = (r%Mass.lt.small*r%AbsVec*r%AbsVec)
  r%Mass = sqrt(r%Mass)
  end function

 
  function lambda_realMom(p,q,r) result(rslt)
!***********************************************************************
!  (p^2)^2 + (q^2)^2 + (r^2)^2 - 2*p^2*q^2 - 2*q^2*r^2 - 2*r^2*p^2
!***********************************************************************
  type(realMom_type),intent(in) :: p,q,r
  !(realknd2!) :: rslt
  if (p%MassSq.ge.rZRO) then
    if (q%MassSq.ge.rZRO) then
      if (r%MassSq.ge.rZRO) then ! +++
        rslt = (p%Mass-q%Mass-r%Mass)*(p%Mass+q%Mass+r%Mass) &
             * (p%Mass+q%Mass-r%Mass)*(p%Mass-q%Mass+r%Mass)
      else ! ++-
        rslt = (p%Mass-q%Mass)*(p%Mass+q%Mass)
        rslt =  rslt*rslt &
             + r%MassSq*( r%MassSq - 2*(p%MassSq+q%MassSq) )
      endif
    else ! +-+
      if (r%MassSq.ge.rZRO) then
        rslt = (p%Mass-r%Mass)*(p%Mass+r%Mass)
        rslt =  rslt*rslt &
             + q%MassSq*( q%MassSq - 2*(p%MassSq+r%MassSq) )
      else ! +--
        rslt = (r%Mass-q%Mass)*(r%Mass+q%Mass)
        rslt =  rslt*rslt &
             + p%MassSq*( p%MassSq - 2*(r%MassSq+q%MassSq) )
      endif
    endif
  else
    if (q%MassSq.ge.rZRO) then
      if (r%MassSq.ge.rZRO) then ! -++
        rslt = (r%Mass-q%Mass)*(r%Mass+q%Mass)
        rslt =  rslt*rslt &
             + p%MassSq*( p%MassSq - 2*(r%MassSq+q%MassSq) )
      else ! -+-
        rslt = (p%Mass-r%Mass)*(p%Mass+r%Mass)
        rslt =  rslt*rslt &
             + q%MassSq*( q%MassSq - 2*(p%MassSq+r%MassSq) )
      endif
    else ! --+
      if (r%MassSq.ge.rZRO) then ! --+
        rslt = (p%Mass-q%Mass)*(p%Mass+q%Mass)
        rslt =  rslt*rslt &
             + r%MassSq*( r%MassSq - 2*(p%MassSq+q%MassSq) )
      else ! ---
        rslt = (p%Mass-q%Mass-r%Mass)*(p%Mass+q%Mass+r%Mass) &
             * (p%Mass+q%Mass-r%Mass)*(p%Mass-q%Mass+r%Mass)
      endif
    endif
  endif
  end function
            
end module


