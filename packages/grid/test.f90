program test_grid
  use avh_grid_cpp
  implicit none
  integer :: nev,iev
  real(kind(1d0)) :: xx,ff,s1(10),s2(10)

  call rng_init(seed=372643)
  call init(id=4,nbatch=400,nchmax=200)
  call init(id=7,nbatch=400,nchmax=200)

  nev = 100000

  s1 = 0
  s2 = 0
  do iev=1,nev
    xx = rng_gnrt()
      ff = 11*xx**10
      s1(1) = s1(1) + ff
      s2(1) = s2(1) + ff*ff
    xx = gnrt(id=4,rho=rng_gnrt())
      ff = 11*xx**10 * wght(id=4)
      s1(4) = s1(4) + ff
      s2(4) = s2(4) + ff*ff
      call collect(id=4,weight=ff)
    xx = gnrt(id=7,rho=rng_gnrt())
      ff = 2*xx * wght(id=7)
      s1(7) = s1(7) + ff
      s2(7) = s2(7) + ff*ff
      call collect(id=7,weight=ff)
  enddo

  call plot(id=4,writeUnit=21)
  call plot(id=7,writeUnit=21)
  call dump(id=4,writeUnit=21)
  write(*,'(2f14.8)') s1(1)/nev ,error(s1(1),s2(1),nev)
  write(*,'(2f14.8)') s1(4)/nev ,error(s1(4),s2(4),nev)
  write(*,'(2f14.8)') s1(7)/nev ,error(s1(7),s2(7),nev)

  call load(id=4,readUnit=21)
  s1 = 0
  s2 = 0
  do iev=1,nev
    xx = gnrt(id=4,rho=rng_gnrt())
    ff = 11*xx**10 * wght(id=4)
    s1(4) = s1(4) + ff
    s2(4) = s2(4) + ff*ff
  enddo
  write(*,'(2f14.8)') s1(4)/nev ,error(s1(4),s2(4),nev)

contains

  function error(aa,bb,nn) result(rslt)
  real(kind(1d0)),intent(in) :: aa,bb
  integer,intent(in) :: nn
  real(kind(1d0)) :: rslt
  rslt = sqrt( (bb/nev-(aa/nev)**2)/(nev-1) )
  end function

end program
