module avh_grid
  !(usekindmod!)
  use avh_mathcnst
  use avh_iounits
  use avh_ranlux

  implicit none
  private
  public :: grid_type

! Character lenght label
  integer,parameter :: lablen=8

  type :: grid_type
    private
    !(realknd2!),allocatable :: xri(:),wch(:),vch(:),sch(:)
    !(realknd2!)      :: frac,nrm,ww,Low,Vol,nTot,wTot,maxRat
    integer           :: idat,ndat,nch,nchmax
    integer           :: ic
    logical           :: merg
    character(lablen) :: lbl
  contains
    procedure :: init
    procedure :: restart
    procedure :: dalloc
    procedure :: collect
    procedure :: plot
    procedure :: map
    procedure :: pam
    procedure :: get_nch
    procedure :: gnrt_0
    procedure :: gnrt_lu
    procedure :: wght_0
    procedure :: wght_lu
    generic :: gnrt=>gnrt_0,gnrt_lu
    generic :: wght=>wght_0,wght_lu
    procedure :: stop_optimization
    procedure :: dump
    procedure :: load
  end type

  logical,save :: initz=.true.
  type(ranlux_type),save :: rng

contains


  subroutine init( obj ,nbatch,nchmax,label,maxRat ,frac ,yLow,yUpp )
!********************************************************************
!* nbatch = number of events to be collected before adaptation step
!* nchmax = the maximal number of bins/channels
!*   frac = fraction of the number of channels/bins to be merged in
!*          an adaptation step
!*  label = a character label for the grid
!*   yLow = lower border of integration space
!*   yUpp = upper border of integration space
!* yLow and yUpp only influence grid_map and grid_pam
!********************************************************************
  class(grid_type) :: obj
  integer     ,intent(in) :: nbatch,nchmax,maxRat
  !(realknd2!),intent(in) :: frac,yLow,yUpp
  character(*),intent(in) :: label
  optional :: nbatch,nchmax,label,frac,yLow,yUpp,maxRat
  integer :: ii
!
  if (initz) then
    initz = .false.
    call init_mathcnst
    call rng%init(-1,731204,0,0,label='avh_grid static')
  endif
!
  obj%lbl  = ''    ;if(present(label )) obj%lbl=adjustl(label)
  obj%ndat = 1000  ;if(present(nbatch)) obj%ndat=nbatch
  obj%nchmax = 200 ;if(present(nchmax)) obj%nchmax=nchmax
  obj%frac = (2*rONE)/(2*obj%nchmax-1)
                    if(present(frac  )) obj%frac=frac
  obj%Low = 0      ;if(present(yLow  )) obj%Low=yLow
  obj%Vol = 1      ;if(present(yUpp  )) obj%Vol=yUpp-yLow
  obj%maxRat = 1d99;if(present(maxRat)) obj%maxRat=maxRat
  obj%idat = 0
  obj%nrm = 1
  obj%nch = 1
  obj%ic = 0
  obj%merg = .false.
  if (obj%nchmax.le.1) then
    if (errru.gt.0) write(errru,*) 'ERROR in grid_init: ' &
      ,'nchmax should be at least 2'
    stop
  endif
  call obj%dalloc
  allocate( obj%xri(0:obj%nchmax) )
  allocate( obj%wch(1:obj%nchmax) )
  allocate( obj%vch(1:obj%nchmax) )
  allocate( obj%sch(0:obj%nchmax) )
  obj%xri(0) = 0
  obj%sch(0) = 0
  do ii=1,obj%nch
    obj%xri(ii) = (ii*rONE)/obj%nch
    obj%wch(ii) = 0
    obj%vch(ii) = 0
    obj%sch(ii) = (ii*rONE)/obj%nch
  enddo
  obj%nTot = 0
  obj%wTot = 0
  end subroutine

  subroutine restart( obj )
!********************************************************************
!********************************************************************
  class(grid_type) :: obj
  integer :: ii
  obj%idat = 0
  obj%nrm = 1
  obj%nch = 1
  obj%ic = 0
  obj%merg = .false.
  obj%xri(0) = 0
  obj%sch(0) = 0
  do ii=1,obj%nch
    obj%xri(ii) = (ii*rONE)/obj%nch
    obj%wch(ii) = 0
    obj%vch(ii) = 0
    obj%sch(ii) = (ii*rONE)/obj%nch
  enddo
  obj%nTot = 0
  obj%wTot = 0
  end subroutine

  subroutine dalloc( obj )
!********************************************************************
!********************************************************************
  class(grid_type) :: obj
  if (allocated(obj%xri)) deallocate( obj%xri )
  if (allocated(obj%wch)) deallocate( obj%wch )
  if (allocated(obj%vch)) deallocate( obj%vch )
  if (allocated(obj%sch)) deallocate( obj%sch )
  end subroutine

  subroutine stop_optimization( obj )
!********************************************************************
!********************************************************************
  class(grid_type) :: obj
  obj%ndat =-1
  if (allocated(obj%wch)) deallocate(obj%wch)
  if (allocated(obj%vch)) deallocate(obj%vch)
  end subroutine

  subroutine dump( obj ,wUnit ,fileName )
!********************************************************************
!********************************************************************
  class(grid_type) :: obj
  integer,intent(in),optional :: wUnit
  character(*),intent(in),optional :: fileName
  integer :: iunit
  integer :: ii
  iunit = deffile
  if (present(wUnit)) iunit = wUnit
  if (iunit.le.0) return
  if (present(fileName)) then
    open(unit=iunit,file=trim(fileName),status='replace',form='unformatted')
  else
    open(unit=iunit,file='grid_'//trim(obj%lbl)//'.dump',status='replace' &
        ,form='unformatted')
  endif
  write(iunit) obj%lbl
  write(iunit) obj%idat,obj%ndat,obj%nch,obj%nchmax,obj%ic,obj%merg
  write(iunit) obj%frac,obj%nrm,obj%ww,obj%Low,obj%Vol,obj%nTot,obj%wTot
  write(iunit) obj%xri
  write(iunit) obj%sch
  if (allocated(obj%wch)) then
    write(iunit) 1 !'allocated'
    write(iunit) obj%wch
    write(iunit) obj%vch
  else
    write(iunit) 0 !'not_allocated'
  endif
  close(iunit)
  end subroutine
  
  subroutine load( obj ,filename ,rUnit )
!********************************************************************
!********************************************************************
  class(grid_type) :: obj
  character(*),intent(in) :: filename
  integer,intent(in),optional :: rUnit
  integer :: iunit,allocd
  iunit = deffile
  if (present(rUnit)) iunit = rUnit
  if (iunit.le.0) return
!
  if (initz) then
    initz = .false.
    call init_mathcnst
    call rng%init(-1,731204,0,0,label='avh_grid static')
  endif
!
  open(unit=iunit,file=trim(filename),status='old' &
      ,form='unformatted')
  read(iunit) obj%lbl
  read(iunit) obj%idat,obj%ndat,obj%nch,obj%nchmax,obj%ic,obj%merg
  read(iunit) obj%frac,obj%nrm,obj%ww,obj%Low,obj%Vol,obj%nTot,obj%wTot
  if (allocated(obj%xri)) deallocate(obj%xri)
  if (allocated(obj%sch)) deallocate(obj%sch)
  if (allocated(obj%wch)) deallocate(obj%wch)
  if (allocated(obj%vch)) deallocate(obj%vch)
  allocate(obj%xri(0:obj%Nchmax))
  allocate(obj%sch(0:obj%Nchmax))
  read(iunit) obj%xri
  read(iunit) obj%sch
  read(iunit) allocd
  if (allocd.eq.1) then
    allocate(obj%wch(1:obj%Nchmax))
    allocate(obj%vch(1:obj%Nchmax))
    read(iunit) obj%wch
    read(iunit) obj%vch
  endif
  close(iunit)
  end subroutine
  

  function gnrt_0( obj ,rho ) result(xx)
!********************************************************************
!********************************************************************
  class(grid_type) :: obj
  !(realknd2!),intent(in) :: rho
  !(realknd2!) :: xx
  integer :: i0,i1,ii
  i0 = 0
  i1 = obj%nch
  do while (i1-i0.gt.1)
    ii = (i0+i1)/2
    if (rho.le.obj%sch(ii)) then
      i1 = ii
    else
      i0 = ii
    endif
  enddo
  ii = i1
  obj%ic = ii
  obj%ww = ( obj%xri(ii) - obj%xri(ii-1) ) &
         / ( obj%sch(ii) - obj%sch(ii-1) )
  xx = obj%ww*( rho - obj%sch(ii-1) ) + obj%xri(ii-1)
  end function

  function wght_0( obj ,xx ) result(weight)
!********************************************************************
!* Don't put xx to use the latest generated, and stored, channel
!********************************************************************
  class(grid_type) :: obj
  intent(in) :: xx
  optional :: xx
  !(realknd2!) :: weight,xx
  integer :: ii
  if (present(xx)) then
    call findch( obj ,ii ,xx )
    if (ii.eq.0) then
      if (errru.gt.0) write(errru,*) 'ERROR in grid_wght: ' &
        ,'no channel found for x=',xx,', putting weight=0'
      obj%ww = 0
    else
      obj%ic = ii
      obj%ww = ( obj%xri(ii) - obj%xri(ii-1) ) &
             / ( obj%sch(ii) - obj%sch(ii-1) )
    endif
  endif
  weight = obj%ww
  end function

  subroutine collect( obj ,weight ,xx ,option )
!********************************************************************
!* xx is optional input
!********************************************************************
  class(grid_type) :: obj
  intent(in) :: weight,xx,option
  optional :: xx,option
  !(realknd2!) :: weight,xx,ww,hh
  integer :: ii,jj,option,opt
  logical :: exit_routine
!
  if (obj%ndat.le.0) return
!
  opt = 0
  if (present(option)) opt=option
!
! Find the bin into which xx belongs.
! If xx outside interval, use the latest active (by gnrt or wght) bin.
  if (present(xx)) then
    call findch( obj ,ii ,xx )
  else
    ii = obj%ic
  endif
!
! Make sure the weight coming with a channel is used only once
  if (ii.eq.0) return
  obj%ic = 0
!
! Update the weights of the bins
  !if (weight.ne.rZRO) then
    ww = weight
    hh = obj%maxRat*obj%wTot
    if (obj%nTot*ww.gt.hh) ww = hh/obj%nTot 
    obj%vch(ii) = obj%vch(ii) + rONE
    obj%wch(ii) = obj%wch(ii) + ww    ! entropy
!   obj%wch(ii) = obj%wch(ii) + ww**2 ! variance
    obj%idat = obj%idat+1
    obj%merg = .true.
    obj%nTot = obj%nTot + rONE
    obj%wTot = obj%wTot + weight
  !endif
!
! Adapt the bins
  exit_routine = .true.
  if(obj%idat.eq.obj%ndat) then
    obj%idat = 0
    if (maxval(obj%wch(1:obj%nch)).gt.rZRO) then
      call splitch( obj )
      exit_routine = .false.
    endif
  elseif (obj%idat.eq.obj%ndat/2.and.obj%merg) then
    if (maxval(obj%wch(1:obj%nch)).gt.rZRO) then
      call mergech( obj )
      obj%merg = .false.
      exit_routine = .false.
    endif
  endif
!
  if (exit_routine) return
  if (maxval(obj%wch).le.rZRO) return
!
! Activate the updated bins for gnrt/wght
  select case (opt)
    case default
      do jj=1,obj%nch
        obj%sch(jj) = obj%sch(jj-1) + obj%wch(jj)
!       obj%sch(jj) = obj%sch(jj-1) + sqrt(obj%wch(jj)) ! variance
      enddo
    case (1)
      do jj=1,obj%nch
        obj%sch(jj) = obj%sch(jj-1) + obj%wch(jj)/max(rONE,obj%vch(jj))
      enddo
  end select
  do jj=1,obj%nch
    obj%sch(jj) = obj%sch(jj)/obj%sch(obj%nch)
  enddo
  end subroutine


  function gnrt_lu( obj ,rho ,xlow,xupp ) result(xx)
!********************************************************************
! generate xx with  0 < xlow < xx < xupp < 1
!********************************************************************
  class(grid_type) :: obj
  !(realknd2!),intent(in) :: rho,xlow,xupp
  !(realknd2!) :: xx
  include 'gnrt_wght.h90'
    !execute case: task | gnrt
  end function

  function wght_lu( obj ,xx ,xlow,xupp ) result(weight)
!********************************************************************
! Weight factor coming with  grid_gnrt_lu
!********************************************************************
  class(grid_type) :: obj
  !(realknd2!),intent(in) :: xx,xlow,xupp
  !(realknd2!) :: weight
  include 'gnrt_wght.h90'
    !execute case: task | wght
  end function


  subroutine map( obj ,yy ,yLow,yUpp ,rho ,weight )
!********************************************************************
! From  rho, which must satisfy  yLow < rho < yUpp,
! construct  yy  with  yLow < yy < yUpp.
! yLow and yUpp  must satisfy  yLow_init < yLow < yUpp < yUpp_init
!********************************************************************
  class(grid_type) :: obj
  !(realknd2!),intent(  out) :: yy,weight
  !(realknd2!),intent(in   ) :: yLow,yUpp,rho
  optional :: weight
  include 'map_pam.h90'
    !execute case: task | map
  end subroutine

  subroutine pam( obj ,yy ,yLow,yUpp ,rho ,weight )
!********************************************************************
! The opposite of grid_map
!********************************************************************
  class(grid_type) :: obj
  !(realknd2!),intent(in   ) :: yy,yLow,yUpp
  !(realknd2!),intent(  out) :: rho,weight
  include 'map_pam.h90'
    !execute case: task | pam
  end subroutine


  subroutine splitch( obj )
!********************************************************************
!********************************************************************
  type(grid_type) :: obj
  !(realknd2!) :: ineff,ineff_old,wchtmp,wchmax
  integer :: nch,ii,lch(obj%nchmax),nsame
  real :: rr(1)
!
  ineff_old = huge(ineff_old)
  do while (obj%nch.lt.obj%nchmax)
    nch = obj%nch
    wchmax = -1
    do ii=1,nch
      wchtmp = obj%wch(ii)
      if     (wchtmp.gt.wchmax) then
        wchmax = wchtmp
        lch(1) = ii
        nsame = 1
      elseif (wchtmp.eq.wchmax) then
        nsame = nsame+1
        lch(nsame) = ii
      endif
    enddo
!  
    ineff = wchmax*nch
    if (ineff.ge.ineff_old) then
      exit
    else
      ineff_old = ineff
    endif
!  
    if (nsame.gt.1) then
      call rng%gnrt(rr,1)
      ii = lch(1+int(nsame*rr(1)))
    else
      ii = lch(1)
    endif
!
    if (obj%xri(ii)-obj%xri(ii-1).le.epsilon(obj%xri(ii))) exit
!  
    obj%xri(ii+1:nch+1) = obj%xri(ii:nch)
    obj%wch(ii+1:nch+1) = obj%wch(ii:nch)
    obj%vch(ii+1:nch+1) = obj%vch(ii:nch)
    obj%nch = nch+1
    obj%xri(ii)   = ( obj%xri(ii-1)+obj%xri(ii) )/2
    obj%wch(ii)   = obj%wch(ii)/2
    obj%wch(ii+1) = obj%wch(ii)
    obj%vch(ii)   = obj%vch(ii)/2
    obj%vch(ii+1) = obj%vch(ii)
  enddo
!
  end subroutine


  subroutine mergech( obj )
!********************************************************************
!********************************************************************
  type(grid_type) :: obj
  !(realknd2!) :: wchtmp,wchmin
  integer :: nch,ii,lch(obj%nchmax),nsame,newnch
  real :: rr(1)
!
  newnch = 1+int( (1-obj%frac)*obj%nch )
  do while (obj%nch.gt.newnch)
    nch = obj%nch
    wchmin = huge(wchmin)
    do ii=1,nch-1
      wchtmp = obj%xri(ii+1)-obj%xri(ii-1)
      if     (wchtmp.lt.wchmin) then
        wchmin = wchtmp
        lch(1) = ii
        nsame = 1
      elseif (wchtmp.eq.wchmin) then
        nsame = nsame+1
        lch(nsame) = ii
      endif
    enddo
!  
    if (nsame.gt.1) then
      call rng%gnrt(rr,1)
      ii = lch(1+int(nsame*rr(1)))
    else
      ii = lch(1)
    endif
!
    obj%xri(ii) = obj%xri(ii+1)
    obj%wch(ii) = obj%wch(ii)+obj%wch(ii+1)
    obj%vch(ii) = obj%vch(ii)+obj%vch(ii+1)
    obj%xri(ii+1:nch-1) = obj%xri(ii+2:nch)
    obj%wch(ii+1:nch-1) = obj%wch(ii+2:nch)
    obj%vch(ii+1:nch-1) = obj%vch(ii+2:nch)
    obj%nch = nch-1
  enddo
!
  end subroutine


  subroutine findch( obj ,ii ,xx )
!********************************************************************
!********************************************************************
  intent(in   ) :: obj ,xx
  intent(  out) :: ii
  type(grid_type) :: obj
  integer :: ii
  !(realknd2!) :: xx
  integer i0,i1
!
  if (xx.lt.rZRO.or.xx.gt.rONE) then
    ii = 0
    return
  endif
!
  i0 = 0
  i1 = obj%nch
  do while (i1-i0.gt.1)
    ii = (i0+i1)/2
    if (xx.le.obj%xri(ii)) then
      i1 = ii
    else
      i0 = ii
    endif
  enddo
  ii = i1
  end subroutine


  subroutine plot( obj ,wUnit ,fileName )
!********************************************************************
!********************************************************************
  class(grid_type),intent(in) :: obj
  integer,intent(in),optional :: wUnit
  character(*),intent(in),optional :: fileName
  integer :: iunit
  real(kind(1d0)) :: h1,h2,h3,h4
  integer :: ii
  iunit = deffile
  if (present(wUnit)) iunit = wUnit
  if (iunit.le.0) return
  if (obj%nchmax.le.1) return
  if (present(fileName)) then
    open(unit=iunit,file=trim(fileName),status='replace')
  else
    open(unit=iunit,file='grid_'//trim(obj%lbl)//'.plot',status='replace')
  endif
  do ii=1,obj%nch
    h1 = ( obj%sch(ii)-obj%sch(ii-1) )/( obj%xri(ii)-obj%xri(ii-1) )
    h2 = obj%sch(ii)
    h3 = obj%xri(ii-1)*obj%Vol + obj%Low
    h4 = obj%xri(ii  )*obj%Vol + obj%Low
    write(iunit,'(99e16.8)') h3,0d0,0d0
    write(iunit,'(99e16.8)') h3,h1,h2
    write(iunit,'(99e16.8)') h4,h1,h2
    write(iunit,'(99e16.8)') h4,0d0,0d0
  enddo
  close( unit=iunit )
  end subroutine


  function get_nch( obj ) result(rslt)
!********************************************************************
!********************************************************************
  class(grid_type),intent(in) :: obj
  integer :: rslt
  rslt = obj%nch
  end function

end module


