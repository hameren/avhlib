module avh_grid_cpp
  use avh_prnt
  use avh_ranlux
  use avh_grid

  implicit none
  private
  public :: init ,gnrt ,gnrtsec ,wght ,collect ,load ,dump ,plot ,finalize
  public :: rng_init ,rng_gnrt

  integer :: Ngrids=0
  type(grid_type),allocatable,save :: list(:),tmpList(:)

  type(ranlux_type),save :: ranlux

contains
  
  subroutine init( id ,nbatch ,nchmax )
  integer,intent(out) :: id
  integer,intent(in ) :: nbatch,nchmax
  Ngrids = Ngrids+1
  call increase_list
  id = Ngrids
  call tmpList(id)%init( nbatch ,nchmax ,prnt(id,4,'0') )
  end subroutine

  subroutine load( id ,readUnit )
  integer,intent(in) :: id ,readUnit
  if (id.gt.Ngrids) Ngrids = id
  call increase_list
  call tmpList(id)%load( 'grid_'//prnt(id,4,'0')//'.dump' ,readUnit )
  end subroutine

  subroutine finalize
  allocate(list(1:Ngrids))
  list(1:Ngrids) = tmpList(1:Ngrids)
  deallocate(tmpList)
  Ngrids = 0
  end subroutine

  subroutine dump( id ,writeUnit )
  integer,intent(in) :: id ,writeUnit
  call list(id)%dump( writeUnit )
  end subroutine

  function gnrt( id ,rho ) result(rslt)
  integer,intent(in) :: id
  real(kind(1d0)),intent(in) :: rho
  real(kind(1d0)) :: rslt
  rslt = list(id)%gnrt( rho )
  end function

  function gnrtsec( id ,rho ,xLow,xUpp ) result(rslt)
  integer,intent(in) :: id
  real(kind(1d0)),intent(in) :: rho ,xLow,xUpp
  real(kind(1d0)) :: rslt
  rslt = list(id)%gnrt( rho ,xLow,xUpp )
  end function

  function wght( id ) result(rslt)
  integer,intent(in) :: id
  real(kind(1d0)) :: rslt
  rslt = list(id)%wght()
  end function

  subroutine collect( id ,weight )
  integer,intent(in) :: id
  real(kind(1d0)),intent(in) :: weight
  call list(id)%collect( weight ) 
  end subroutine

  subroutine plot( id ,writeUnit )
  integer,intent(in) :: id ,writeUnit
  call list(id)%plot( writeUnit )
  end subroutine


!  function grid_wghtx( id ,xx ) result(rslt)
!  integer,intent(in) :: id
!  real(kind(1d0)),intent(in) :: xx
!  real(kind(1d0)) :: rslt
!  rslt = list(id)%wght( xx )
!  end function
! 
!  subroutine grid_collectx( id ,ww ,xx )
!  integer,intent(in) :: id
!  real(kind(1d0)),intent(in) :: ww,xx
!  call list(id)%collect( ww,xx ) 
!  end subroutine


  subroutine rng_init(seed)
  intent(in) :: seed
  integer :: seed
  call ranlux%init(-1,seed,0,0,label='avh_grid_cpp static')
  end subroutine

  function rng_gnrt() result(xx)
  real(kind(1d0)) :: xx
  real :: rr(1)
  call ranlux%gnrt(rr,1)
  xx = rr(1)
  end function

!***********************************************************************
! Private stuff from here...
!***********************************************************************

  subroutine increase_list
  integer :: nn
  if (.not.allocated(tmpList)) allocate(tmpList(2))
  nn = size(tmpList,1)
  if (Ngrids.gt.nn) then
    allocate(list(1:nn))
    list(1:nn) = tmpList(1:nn)
    deallocate(tmpList)
    allocate(tmpList(1:Ngrids*2))
    tmpList(1:nn) = list(1:nn)
    deallocate(list)
  endif
  end subroutine

end module
