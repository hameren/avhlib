// GRID remaps numbers on the unit interval [0,1]. It does not
// generate them, but takes numbers on [0,1] as input (GRID_gnrt).

// GRID does provide a random number generator (RNG_gnrt), which uses
// RANLUX.

// It is possible to remap numbers on a section of [0,1] (GRID_gnrtsec).
// The input that is to be remapped is expected to be in [0,1], and the
// weight is such that the density on the section is normalized.
// Collected weights contribute to building the grid on the whole [0,1].
// E.g. for kinematical variables whose limits change event by event
// the whole [0,1] may correspond to the absolute limits, while sections
// are generated event by event, dependening on the value of already
// generated variables.

// GRID does use a random number generator internally, to build the grid.

#define GRID_init     __avh_grid_cpp_init   
#define GRID_load     __avh_grid_cpp_load   
#define GRID_dump     __avh_grid_cpp_dump   
#define GRID_finalize __avh_grid_cpp_finalize   
#define GRID_gnrt     __avh_grid_cpp_gnrt   
#define GRID_gnrtsec  __avh_grid_cpp_gnrtsec  
#define GRID_wght     __avh_grid_cpp_wght   
#define GRID_collect  __avh_grid_cpp_collect
#define GRID_plot     __avh_grid_cpp_plot   

#define RNG_init      __avh_grid_cpp_rng_init   
#define RNG_gnrt      __avh_grid_cpp_rng_gnrt   

extern "C" {
              //( id   ,nbatch ,nchmax )
void   GRID_init( int* ,int*   ,int*   );

              //( id   ,readUnit )
void   GRID_load( int* ,int*     );

              //( id   ,writeUnit )
void   GRID_dump( int* ,int*      );

void   GRID_finalize(void);

              //( id   ,randomNumber )  
double GRID_gnrt( int* ,double*      );   

                 //( id   ,randomNumber ,lowLim  ,uppLim  )  
double GRID_gnrtsec( int* ,double*      ,double* ,double* );

              //( id   )
double GRID_wght( int* );

                 //( id   ,weight  )
void   GRID_collect( int* ,double* );

              //( id   ,writeUnit )
void   GRID_plot( int* ,int*      );

             //( seed )
void   RNG_init( int* );

double RNG_gnrt();   

}//extern "C"
