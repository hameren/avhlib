#!/usr/bin/env python
#begin header
import os,sys
pythonDir = ''
sys.path.append(pythonDir)
from avh import *
thisDir,packDir,avhDir = cr.dirsdown(2)
packageName = os.path.basename(thisDir)
buildDir = os.path.join(avhDir,'build')
#end header

date = '30-10-2018'

lines = ['! From here the package "'+packageName+'", last edit: '+date+'\n']
cr.addfile(os.path.join(thisDir,'avh_bivinp.f90'),lines,delPattern='^.*')
cr.addfile(os.path.join(thisDir,'avh_trivinp.f90'),lines,delPattern='^.*')

fpp.incl(thisDir,lines)

cr.wfile(os.path.join(buildDir,packageName+'.f90'),lines)
