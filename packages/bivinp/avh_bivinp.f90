module avh_bivinp
  !(usekindmod!)
  use avh_iounits
  implicit none
  private
  public :: bivinp_type

  type :: bivinp_type
    private
    integer :: Nx,Ny
    !(realknd2!),allocatable :: x(:),y(:),z(:,:)
    !(realknd2!) :: zMin,zMax
  contains
    procedure :: read_file
    procedure :: evaluate
  end type

  !(realknd2!),parameter :: small=1d-14
  !(realknd2!),parameter :: prec=1d-8

  interface resize
    module procedure resize1,resize2! date: 03-11-2023
  end interface

  interface enlarge_stp
    module procedure enlarge1_stp,enlarge2_stp! date: 03-11-2023
  end interface

contains

  function equal(xx,yy) result(rslt)
  !(realknd2!),intent(in) :: xx,yy
  logical :: rslt
  if     (xx.eq.0) then ;rslt=(abs(yy).lt.small)
  elseif (yy.eq.0) then ;rslt=(abs(xx).lt.small)
                   else ;rslt=(abs(1-xx/yy).lt.small)
  endif
  end function

  function notEqual(xx,yy) result(rslt)
  !(realknd2!),intent(in) :: xx,yy
  logical :: rslt
  if     (xx.eq.0) then ;rslt=(abs(yy).ge.small)
  elseif (yy.eq.0) then ;rslt=(abs(xx).ge.small)
                   else ;rslt=(abs(1-xx/yy).ge.small)
  endif
  end function


  subroutine read_file( obj ,filename ,readUnit )
!***********************************************************************
! The file must have the shape
!   x1 y1 z11
!   x1 y2 z12
!   ...
!   x1 yn z1n
!   x2 y1 z21
!   x2 y2 z22
!   ...
! where  x1<x2<...<xm  and  y1<y2<...<yn
!***********************************************************************
  class(bivinp_type) :: obj
  character(*)     ,intent(in ) :: filename
  integer,optional ,intent(in ) :: readUnit
  !(realknd2!) :: xx,yy,zz
  integer :: runit,ny
  integer,parameter :: nStep=100
  runit = deffile
  if (present(readUnit)) runit = readUnit
  obj%Ny = -1
  obj%zMin = huge(obj%zMin)
  obj%zMax = tiny(obj%zMax)
  open(runit,file=filename)
    read(runit,*,end=111) xx,yy,zz
  close(runit)
  obj%Nx = 1
  call enlarge_stp( obj%x ,1,obj%Nx ,0,nStep )
  obj%x(obj%Nx) = xx
  ny = 0
  open(runit,file=filename)
  do
    do
      read(runit,*,end=111) xx,yy,zz
      call enlarge_stp( obj%z ,1,obj%Nx,1,ny+1 ,0,nStep,0,nStep )
      obj%z(obj%Nx,ny+1) = zz
      if (obj%zMin.gt.zz) obj%zMin = zz
      if (obj%zMax.lt.zz) obj%zMax = zz
      if (notEqual(obj%x(obj%Nx),xx)) exit
      ny = ny+1
      if (obj%Ny.eq.-1) then
        call enlarge_stp( obj%y ,1,ny ,0,nStep )
        obj%y(ny) = yy
        if (ny.gt.1) then
          if (obj%y(ny).le.obj%y(ny-1)) call error
        endif
      else
        if (notEqual(obj%y(ny),yy)) call error
      endif
    enddo
    if (obj%Ny.eq.-1) then
      obj%Ny = ny
    else
      if (obj%Ny.ne.ny) call error
    endif
    ny = 1
    if (notEqual(obj%y(ny),yy)) call error
    obj%Nx = obj%Nx+1
    call enlarge_stp( obj%x ,1,obj%Nx ,0,nStep )
    obj%x(obj%Nx) = xx
    obj%z(obj%Nx,1) = zz
    if (obj%Nx.gt.1) then
      if (obj%x(obj%Nx).le.obj%x(obj%Nx-1)) call error
    endif
  enddo
  111 continue
  close(runit)
  call resize( obj%x ,1,obj%Nx )
  call resize( obj%y ,1,obj%Ny )
  call resize( obj%z ,1,obj%Nx ,1,obj%Ny )
! Perform thinning
  call thinning( obj )
!
  contains
!
    subroutine error
    if (errru.ge.0) write(errru,*) 'ERROR in avh_bivinp read_file: ' &
      ,'file ',filename,' inconsistent'
    stop
    end subroutine
!
  end subroutine


  subroutine thinning( obj )
!***********************************************************************
! Throw out rows and collumns for which all z-values are just as well
! interpolated.
!***********************************************************************
  type(bivinp_type),intent(inout) :: obj
  !(realknd2!) :: ff,maxDiff,thrs
  integer :: ii,jj,i0,i1,j0,j1
  thrs = abs(obj%zMax-obj%zMin)*prec
  ii = 1
  do ;ii=ii+1 ;if(ii.ge.obj%Nx)exit
    i0 = ii-1
    i1 = ii+1
    maxDiff = 0
    do jj=1,obj%Ny
      ff = interpolate( obj%z(i0,jj),obj%z(i0,jj),obj%z(i1,jj),obj%z(i1,jj) &
                       ,obj%x(i0),obj%x(i1) ,obj%y(jj),obj%y(jj) &
                       ,obj%x(ii),obj%y(jj) )
      ff = abs( ff - obj%z(ii,jj) )
      if (ff.gt.maxDiff) maxDiff = ff
    enddo
    if (maxDiff.ge.thrs) cycle
    obj%x(ii:obj%Nx-1) = obj%x(ii+1:obj%Nx)
    obj%z(ii:obj%Nx-1,1:obj%Ny) = obj%z(ii+1:obj%Nx,1:obj%Ny)
    obj%Nx = obj%Nx-1
    ii = ii-1
  enddo
  jj = 1
  do ;jj=jj+1 ;if(jj.ge.obj%Ny)exit
    j0 = jj-1
    j1 = jj+1
    maxDiff = 0
    do ii=1,obj%Nx
      ff = interpolate( obj%z(ii,j0),obj%z(ii,j1),obj%z(ii,j0),obj%z(ii,j1) &
                       ,obj%x(ii),obj%x(ii) ,obj%y(j0),obj%y(j1) &
                       ,obj%x(ii),obj%y(jj) )
      ff = abs( ff - obj%z(ii,jj) )
      if (ff.gt.maxDiff) maxDiff = ff
    enddo
    if (maxDiff.ge.thrs) cycle
    obj%y(jj:obj%Ny-1) = obj%y(jj+1:obj%Ny)
    obj%z(1:obj%Nx,jj:obj%Ny-1) = obj%z(1:obj%Nx,jj+1:obj%Ny)
    obj%Ny = obj%Ny-1
    jj = jj-1
  enddo
  call resize( obj%x ,1,obj%Nx )
  call resize( obj%y ,1,obj%Ny )
  call resize( obj%z ,1,obj%Nx ,1,obj%Ny )
!  do ii=1,obj%Nx !DEBUG
!  do jj=1,obj%Ny !DEBUG
!    write(*,*) obj%x(ii),obj%y(jj),obj%z(ii,jj) !DEBUG
!  enddo !DEBUG
!  enddo !DEBUG
  end subroutine


  function interpolate( z00,z01,z10,z11 ,x0,x1 ,y0,y1 ,xx,yy ) result(rslt)
  !(realknd2!),intent(in) :: z00,z01,z10,z11 ,x0,x1 ,y0,y1 ,xx,yy
  !(realknd2!) :: rslt 
  if (equal(x0,x1)) then
    if (equal(y0,y1)) then
      rslt = z00
    else
      rslt = ( z00*(y1-yy) + z01*(yy-y0) )/(y1-y0)
    endif
  elseif (equal(y0,y1)) then
    rslt = ( z00*(x1-xx) + z10*(xx-x0) )/(x1-x0)
  else
    rslt = ( z00*(x1-xx)*(y1-yy) + z01*(x1-xx)*(yy-y0)   &
           + z10*(xx-x0)*(y1-yy) + z11*(xx-x0)*(yy-y0) ) &
         / ( (x1-x0)*(y1-y0) )
  endif
  end function 


  subroutine find_interval( i0,i1 ,n0,n1,ordered ,xx ) 
  integer,intent(in) :: n0,n1
  !(realknd2!),intent(in) :: ordered(n0:n1),xx
  integer,intent(out) :: i0,i1
  integer :: ii
  if     (xx.lt.ordered(n0)) then         ;i1=n0-1;return
  elseif (xx.eq.ordered(n0)) then ;i0=n0  ;i1=n0  ;return
  elseif (xx.eq.ordered(n1)) then ;i0=n1  ;i1=n1  ;return
  elseif (xx.gt.ordered(n1)) then ;i0=n1+1        ;return
  endif
  i0 = n0
  i1 = n1
  do while(i1-i0.gt.1)
    ii = (i0+i1)/2
    if (xx.lt.ordered(ii)) then
      i1 = ii
    elseif (xx.gt.ordered(ii)) then
      i0 = ii
    else
      i0 = ii
      i1 = ii
      exit
    endif
  enddo
  end subroutine
  

  function evaluate( obj ,xx,yy ) result(rslt)
  class(bivinp_type),intent(in) :: obj
  !(realknd2!),intent(in) :: xx,yy
  !(realknd2!) :: rslt 
  integer :: i0,i1,j0,j1
  call find_interval( i0,i1 ,1,obj%Nx,obj%x ,xx )
  if (i1.lt.1.or.i0.gt.obj%Nx) then
    rslt = 0
    return
  endif
  call find_interval( j0,j1 ,1,obj%Ny,obj%y ,yy )
  if (j1.lt.1.or.j0.gt.obj%Ny) then
    rslt = 0
    return
  endif
  rslt = interpolate( obj%z(i0,j0),obj%z(i0,j1),obj%z(i1,j0),obj%z(i1,j1) &
                     ,obj%x(i0),obj%x(i1) ,obj%y(j0),obj%y(j1) &
                     ,xx,yy )
  end function 


  subroutine resize1( xx ,l1,u1 )
  !(realknd2!) &
  include '../arrays/resize1.h90'
  end subroutine

  subroutine resize2( xx ,l1,u1 ,l2,u2 )
  !(realknd2!) &
  include '../arrays/resize2.h90'
  end subroutine

  subroutine enlarge1_stp( xx ,l1,u1 ,l1stp,u1stp )
  !(realknd2!) &
  include '../arrays/enlarge1stp.h90'
  end subroutine

  subroutine enlarge2_stp( xx ,l1,u1,l2,u2 ,l1stp,u1stp,l2stp,u2stp )
  !(realknd2!) &
  include '../arrays/enlarge2stp.h90'
  end subroutine

end module
