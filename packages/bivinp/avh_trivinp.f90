module avh_trivinp
  !(usekindmod!)
  use avh_iounits
  use avh_mathcnst
  implicit none
  private
  public :: trivinp_type
  public :: operator(+),operator(*)

  type :: trivinp_type
    !private
    integer :: Nx,Ny,Nz
    !(realknd1!),allocatable :: x(:),y(:),z(:),h(:,:,:)
    !(realknd2!),allocatable :: sx(:),sy(:),sz(:)
    integer :: Ndim,map(3)
  contains
    procedure :: dalloc
    procedure :: read_file
    procedure :: evaluate1
    procedure :: evaluate2
    procedure :: evaluate3
    generic :: evaluate=>evaluate1,evaluate2,evaluate3
    procedure :: interpolate1
    procedure :: interpolate2
    procedure :: interpolate3
    generic :: interpolate=>interpolate1,interpolate2,interpolate3
    procedure :: gnrt_x
    procedure :: gnrt_y
    procedure :: gnrt_z
    procedure :: plot_x
    procedure :: plot_y
    procedure :: dalloc_gridval
    procedure :: create1
    procedure :: create2
    generic :: create=>create1,create2
    procedure :: write_file
  end type

  abstract interface
    function xfunc_1d(xx) result(rslt)
    !(realknd2!),intent(in) :: xx
    !(realknd2!) :: rslt
    end function
  end interface

  abstract interface
    function xfunc_2d(xx,yy) result(rslt)
    !(realknd2!),intent(in) :: xx,yy
    !(realknd2!) :: rslt
    end function
  end interface

  interface operator (+)
    module procedure trivinp_add
  end interface
  interface operator (*)
    module procedure trivinp_r_t,trivinp_t_r
  end interface

  !(realknd1!),parameter :: small=1e-6


contains


  function equal(xx,yy) result(rslt)
  !(realknd1!),intent(in) :: xx,yy
  logical :: rslt
  if     (xx.eq.rZRO) then ;rslt=(abs(yy).lt.small)
  elseif (yy.eq.rZRO) then ;rslt=(abs(xx).lt.small)
                      else ;rslt=(abs(1-xx/yy).lt.small)
  endif
  end function

  function notEqual(xx,yy) result(rslt)
  !(realknd1!),intent(in) :: xx,yy
  logical :: rslt
  if     (xx.eq.rZRO) then ;rslt=(abs(yy).ge.small)
  elseif (yy.eq.rZRO) then ;rslt=(abs(xx).ge.small)
                      else ;rslt=(abs(1-xx/yy).ge.small)
  endif
  end function


  subroutine find_interval( i0,i1 ,n0,n1,ordered ,xx ) 
  integer,intent(in) :: n0,n1
  !(realknd1!),intent(in) :: ordered(n0:n1),xx
  integer,intent(out) :: i0,i1
  integer :: ii
  if     (xx.lt.ordered(n0)) then         ;i1=n0-1
  elseif (xx.eq.ordered(n0)) then ;i0=n0  ;i1=n0  
  elseif (xx.eq.ordered(n1)) then ;i0=n1  ;i1=n1  
  elseif (xx.gt.ordered(n1)) then ;i0=n1+1        
  else
    i0 = n0
    i1 = n1
    do while(i1-i0.gt.1)
      ii = (i0+i1)/2
      if (xx.lt.ordered(ii)) then
        i1 = ii
      elseif (xx.gt.ordered(ii)) then
        i0 = ii
      else
        i0 = ii
        i1 = ii
        exit
      endif
    enddo
  endif
  end subroutine
  

  subroutine dalloc( obj )
  class(trivinp_type) :: obj
  if (allocated(obj%x )) deallocate(obj%x )
  if (allocated(obj%y )) deallocate(obj%y )
  if (allocated(obj%z )) deallocate(obj%z )
  if (allocated(obj%h )) deallocate(obj%h )
  if (allocated(obj%sx)) deallocate(obj%sx)
  if (allocated(obj%sy)) deallocate(obj%sy)
  if (allocated(obj%sz)) deallocate(obj%sz)
  end subroutine
  

  subroutine dalloc_gridval( obj )
  class(trivinp_type) :: obj
  if (allocated(obj%h)) deallocate(obj%h)
  end subroutine
  

  subroutine read_file( obj ,filename ,readUnit )
  class(trivinp_type) :: obj
  character(*)     ,intent(in)  :: filename
  integer,optional ,intent(in)  :: readUnit
  integer :: runit,ii,nn,ncollumn
  character(144) :: line
  character(1) :: tab
  runit = deffile
  if (present(readUnit)) runit = readUnit
  write(messu,*) 'MESSAGE from avh_trivinp: loading '//filename
!
! determine dimension
  tab = achar(9)
  open(runit,file=filename,status='old')
  read(runit,'(A)',end=111) line
  nn = len_trim(line)
  ncollumn = 0
  ii = 0
  do ;ii=ii+1 ;if(ii.gt.nn) exit
    if (line(ii:ii).ne.' '.and.line(ii:ii).ne.tab) then
      ncollumn = ncollumn+1
      do ;ii=ii+1 ;if(ii.gt.nn) exit
        if (line(ii:ii).eq.' '.or.line(ii:ii).eq.tab) exit
      enddo
    endif
  enddo
  111 continue
  close(runit)
!
  obj%Ndim = ncollumn-1
!
  select case (obj%Ndim)
  case (1) ;call read_file1( obj ,filename ,runit )
  case (2) ;call read_file2( obj ,filename ,runit )
  case (3) ;call read_file3( obj ,filename ,runit )
  end select
!
  end subroutine


  subroutine read_file1( obj ,filename ,runit )
  class(trivinp_type) :: obj
  character(*),intent(in) :: filename
  integer     ,intent(in) :: runit
  !(realknd1!) :: xx,hh,x1
  integer :: nx
  open(runit,file=filename,status='old')
  do
    read(runit,*,end=222) xx,hh
    nx = nx+1
  enddo
  222 continue
  close(runit)
  if (allocated(obj%x)) deallocate(obj%x)
  if (allocated(obj%y)) deallocate(obj%y)
  if (allocated(obj%z)) deallocate(obj%z)
  if (allocated(obj%h)) deallocate(obj%h)
  if (allocated(obj%sx)) deallocate(obj%sx)
  if (allocated(obj%sy)) deallocate(obj%sy)
  if (allocated(obj%sz)) deallocate(obj%sz)
  allocate(obj%x(nx))
  allocate(obj%h(nx,1,1))
  obj%Nx = 0
  obj%Ny = 1
  obj%Nz = 1
  open(runit,file=filename,status='old')
  do
    read(runit,*,end=333) xx,hh
    nx = nx+1
    call append(obj%x,obj%Nx,nx,xx,filename)
    obj%h(nx,1,1) = hh
  enddo
  333 continue
  close (runit)
  call fill_marg1( obj )
  obj%map = [1,1,1]
  end subroutine


  subroutine read_file2( obj ,filename ,runit )
  class(trivinp_type) :: obj
  character(*),intent(in) :: filename
  integer     ,intent(in) :: runit
  !(realknd1!) :: xx,yy,hh,x1,y1
  integer :: nx,ny,nn
  open(runit,file=filename,status='old')
  read(runit,*,end=111) x1,y1,hh
  111 continue
  close(runit)
  open(runit,file=filename,status='old')
  do
    read(runit,*,end=222) xx,yy,hh
    if (equal(yy,y1)) then
      ny = 0
      if (equal(xx,x1)) then
        nx = 0
        nn = 0
      endif
      nx = nx+1
    endif
    ny = ny+1
    nn = nn+1
  enddo
  222 continue
  close(runit)
  if (nn.ne.nx*ny) call error(filename)
  if (allocated(obj%x)) deallocate(obj%x)
  if (allocated(obj%y)) deallocate(obj%y)
  if (allocated(obj%z)) deallocate(obj%z)
  if (allocated(obj%h)) deallocate(obj%h)
  if (allocated(obj%sx)) deallocate(obj%sx)
  if (allocated(obj%sy)) deallocate(obj%sy)
  if (allocated(obj%sz)) deallocate(obj%sz)
  allocate(obj%x(nx))
  allocate(obj%y(ny))
  allocate(obj%h(nx,ny,1))
  obj%Nx = 0
  obj%Ny = 0
  obj%Nz = 1
  open(runit,file=filename,status='old')
  do
    read(runit,*,end=333) xx,yy,hh
    if (equal(yy,y1)) then
      ny = 0
      if (equal(xx,x1)) then
        nx = 0
      endif
      nx = nx+1
      call append(obj%x,obj%Nx,nx,xx,filename)
    endif
    ny = ny+1
    call append(obj%y,obj%Ny,ny,yy,filename)
    obj%h(nx,ny,1) = hh
  enddo
  333 continue
  close (runit)
  call fill_marg2( obj ,[.false.,.true.] )
  obj%map = [1,1,1]
  end subroutine


  subroutine read_file3( obj ,filename ,runit )
  class(trivinp_type) :: obj
  character(*),intent(in) :: filename
  integer     ,intent(in) :: runit 
  !(realknd1!) :: xx,yy,zz,hh,x1,y1,z1
  integer :: nx,ny,nz,nn
  open(runit,file=filename,status='old')
  read(runit,*,end=111) x1,y1,z1,hh
  111 continue
  close(runit)
  open(runit,file=filename,status='old')
  do
    read(runit,*,end=222) xx,yy,zz,hh
    if (equal(zz,z1)) then
      nz = 0
      if (equal(yy,y1)) then
        ny = 0
        if (equal(xx,x1)) then
          nx = 0
          nn = 0
        endif
        nx = nx+1
      endif
      ny = ny+1
    endif
    nz = nz+1
    nn = nn+1
  enddo
  222 continue
  close(runit)
  if (nn.ne.nx*ny*nz) call error(filename)
  if (allocated(obj%x)) deallocate(obj%x)
  if (allocated(obj%y)) deallocate(obj%y)
  if (allocated(obj%z)) deallocate(obj%z)
  if (allocated(obj%h)) deallocate(obj%h)
  if (allocated(obj%sx)) deallocate(obj%sx)
  if (allocated(obj%sy)) deallocate(obj%sy)
  if (allocated(obj%sz)) deallocate(obj%sz)
  allocate(obj%x(nx))
  allocate(obj%y(ny))
  allocate(obj%z(nz))
  allocate(obj%h(nx,ny,nz))
  open(runit,file=filename,status='old')
  obj%Nx = 0
  obj%Ny = 0
  obj%Nz = 0
  do
    read(runit,*,end=333) xx,yy,zz,hh
    if (equal(zz,z1)) then
      nz = 0
      if (equal(yy,y1)) then
        ny = 0
        if (equal(xx,x1)) then
          nx = 0
        endif
        nx = nx+1
        call append(obj%x,obj%Nx,nx,xx,filename)
      endif
      ny = ny+1
      call append(obj%y,obj%Ny,ny,yy,filename)
    endif
    nz = nz+1
    call append(obj%z,obj%Nz,nz,zz,filename)
    obj%h(nx,ny,nz) = hh
  enddo
  333 continue
  close (runit)
  call fill_marg3( obj ,[.false.,.true.,.false.] )
  obj%map = [1,1,1]
  end subroutine


  subroutine append(aa,na,ii,uu,filename)
  !(realknd1!) :: aa(:)
  integer,intent(inout) :: na
  integer,intent(in) :: ii
  !(realknd1!),intent(in) :: uu
  character(*),intent(in) :: filename
  if (ii.gt.na) then
    na = ii
    aa(ii) = uu
  elseif (notEqual(aa(ii),uu)) then
    call error(filename)
  endif
  end subroutine

  subroutine error(filename)
  character(*),intent(in) :: filename
  if (errru.ge.0) write(errru,*) 'ERROR in avh_trivinp: ' &
    ,'file ',filename,' inconsistent'
  stop
  end subroutine



  function interpolate1( obj ,i0,i1 ,xx ) result(rslt)
  class(trivinp_type),intent(in) :: obj
  integer,intent(in) :: i0,i1
  !(realknd1!),intent(in) :: xx
  !(realknd1!) :: rslt 
  associate( x0=>obj%x(i0),x1=>obj%x(i1) &
            ,h0=>obj%h(i0,1,1),h1=>obj%h(i1,1,1) )
  if (equal(x0,x1)) then
    rslt = h0
  else
    rslt = ( (x1-xx)*h0 + (xx-x0)*h1 )/(x1-x0)
  endif
  end associate
  end function 

  function interpolate2( obj ,i0,i1 ,j0,j1 ,xx,yy ) result(rslt)
  class(trivinp_type),intent(in) :: obj
  integer,intent(in) :: i0,i1 ,j0,j1
  !(realknd1!),intent(in) :: xx,yy
  !(realknd1!) :: rslt 
  associate( x0=>obj%x(i0),x1=>obj%x(i1) &
            ,y0=>obj%y(j0),y1=>obj%y(j1) &
            ,h00=>obj%h(i0,j0,1),h01=>obj%h(i0,j1,1) &
            ,h10=>obj%h(i1,j0,1),h11=>obj%h(i1,j1,1) )
  if (equal(y0,y1)) then
    if(equal(x0,x1)) then
      rslt = h00
    else
      rslt = ( (x1-xx)*h00 + (xx-x0)*h10 )/(x1-x0)
    endif
  else
    if (equal(x0,x1)) then
      rslt = ( (y1-yy)*h00 + (yy-y0)*h01 )/(y1-y0)
    else
      rslt = ( (y1-yy)*( (x1-xx)*h00+(xx-x0)*h10 ) &
              +(yy-y0)*( (x1-xx)*h01+(xx-x0)*h11 ) &
             ) / ( (y1-y0)*(x1-x0) )
    endif
  endif
  end associate
  end function 

  function interpolate3( obj ,i0,i1 ,j0,j1 ,k0,k1 ,xx,yy,zz ) result(rslt)
  class(trivinp_type),intent(in) :: obj
  integer,intent(in) :: i0,i1 ,j0,j1 ,k0,k1
  !(realknd1!),intent(in) :: xx,yy,zz
  !(realknd1!) :: rslt 
  associate( x0=>obj%x(i0),x1=>obj%x(i1) &
            ,y0=>obj%y(j0),y1=>obj%y(j1) &
            ,z0=>obj%z(k0),z1=>obj%z(k1) &
            ,h000=>obj%h(i0,j0,k0),h001=>obj%h(i0,j0,k1) &
            ,h010=>obj%h(i0,j1,k0),h011=>obj%h(i0,j1,k1) &
            ,h100=>obj%h(i1,j0,k0),h101=>obj%h(i1,j0,k1) &
            ,h110=>obj%h(i1,j1,k0),h111=>obj%h(i1,j1,k1) )
  if (equal(z0,z1)) then
    if (equal(y0,y1)) then
      if(equal(x0,x1)) then
        rslt = h000
      else
        rslt = ( (x1-xx)*h000 + (xx-x0)*h100 )/(x1-x0)
      endif
    else
      if (equal(x0,x1)) then
        rslt = ( (y1-yy)*h000 + (yy-y0)*h010 )/(y1-y0)
      else
        rslt = ( (y1-yy)*( (x1-xx)*h000+(xx-x0)*h100 ) &
                +(yy-y0)*( (x1-xx)*h010+(xx-x0)*h110 ) &
               ) / ( (y1-y0)*(x1-x0) )
      endif
    endif
  else
    if (equal(y0,y1)) then
      if(equal(x0,x1)) then
        rslt = ( (z1-zz)*h000 + (zz-z0)*h001 )/(z1-z0)
      else
        rslt = ( (z1-zz)*( (x1-xx)*h000+(xx-x0)*h100 ) &
                +(zz-z0)*( (x1-xx)*h001+(xx-x0)*h101 ) &
               ) / ( (z1-z0)*(x1-x0) )
      endif
    else
      if (equal(x0,x1)) then
        rslt = ( (z1-zz)*( (y1-yy)*h000+(yy-y0)*h010 ) &
                +(zz-z0)*( (y1-yy)*h001+(yy-y0)*h011 ) &
               ) / ( (z1-z0)*(y1-y0) )
      else
        rslt = ( (z1-zz)*( (y1-yy)*( (x1-xx)*h000+(xx-x0)*h100 )   &
                          +(yy-y0)*( (x1-xx)*h010+(xx-x0)*h110 ) ) &
                +(zz-z0)*( (y1-yy)*( (x1-xx)*h001+(xx-x0)*h101 )   &
                          +(yy-y0)*( (x1-xx)*h011+(xx-x0)*h111 ) ) &
               ) / ( (z1-z0)*(y1-y0)*(x1-x0) )
      endif
    endif
  endif
  end associate
  end function 


  function evaluate1( obj ,dx ) result(rslt)
  class(trivinp_type),intent(in) :: obj
  !(realknd2!),intent(in) :: dx
  !(realknd1!) :: xx,rslt 
  integer :: i0,i1
  xx = dx
  rslt = 0
  call find_interval( i0,i1 ,1,obj%Nx,obj%x ,xx )
  if (i1.lt.1.or.i0.gt.obj%Nx) return
  rslt = obj%interpolate( i0,i1 ,xx )
  end function 

  function evaluate2( obj ,dx,dy ) result(rslt)
  class(trivinp_type),intent(in) :: obj
  !(realknd2!),intent(in) :: dx,dy
  !(realknd1!) :: xx,yy,rslt 
  integer :: i0,i1,j0,j1
  xx = dx
  yy = dy
  rslt = 0
  call find_interval( i0,i1 ,1,obj%Nx,obj%x ,xx )
  if (i1.lt.1.or.i0.gt.obj%Nx) return
  call find_interval( j0,j1 ,1,obj%Ny,obj%y ,yy )
  if (j1.lt.1.or.j0.gt.obj%Ny) return
  rslt = obj%interpolate( i0,i1 ,j0,j1 ,xx,yy )
  end function 

  function evaluate3( obj ,dx,dy,dz ) result(rslt)
  class(trivinp_type),intent(in) :: obj
  !(realknd2!),intent(in) :: dx,dy,dz
  !(realknd1!) :: xx,yy,zz,rslt 
  integer :: i0,i1,j0,j1,k0,k1
  xx = dx
  yy = dy
  zz = dz
  rslt = 0
  call find_interval( i0,i1 ,1,obj%Nx,obj%x ,xx )
  if (i1.lt.1.or.i0.gt.obj%Nx) return
  call find_interval( j0,j1 ,1,obj%Ny,obj%y ,yy )
  if (j1.lt.1.or.j0.gt.obj%Ny) return
  call find_interval( k0,k1 ,1,obj%Nz,obj%z ,zz )
  if (k1.lt.1.or.k0.gt.obj%Nz) return
  rslt = obj%interpolate( i0,i1 ,j0,j1 ,k0,k1 ,xx,yy,zz )
  end function 


  subroutine fill_marg1( obj )
  class(trivinp_type) :: obj
  integer :: ii,jj
  allocate( obj%sx(0:obj%Nx-1) )
  associate( ss=>obj%sx ,xx=>obj%x ,Nx=>obj%Nx )
  ss(0) = 0
  do ii=1,Nx-1
    ss(ii) = ss(ii-1) + ( xx(ii+1)-xx(ii) )*( obj%h(ii,1,1) + obj%h(ii+1,1,1) )
  enddo
  ss(1:Nx-2) = ss(1:Nx-2)/ss(Nx-1)
  ss(Nx-1) = 1
! Find first maximum of density starting from the right,
! and make density constant from there to the left. 
  call flatleft( ss ,xx ,Nx )
  end associate
  end subroutine


  subroutine fill_marg2( obj ,dmn )
  class(trivinp_type) :: obj
  logical,intent(in) :: dmn(2)
  if (dmn(1)) call fill_s( 1 ,obj%sx ,obj%x,obj%Nx, obj%y,obj%Ny )
  if (dmn(2)) call fill_s( 2 ,obj%sy ,obj%y,obj%Ny, obj%x,obj%Nx )
!
  contains
!
    subroutine fill_s( dn ,ss ,xx,Nx ,yy,Ny )
    !(realknd2!),allocatable,intent(out) :: ss(:)
    integer,intent(in) :: Nx,Ny
    !(realknd1!),intent(in) :: xx(Nx),yy(Ny)
    integer,intent(in) :: dn
    !(realknd1!) :: aa
    integer :: ii,jj
    allocate( ss(0:Nx-1) )
    ss(0) = 0
    do ii=1,Nx-1
      aa = 0
      do jj=1,Ny-1
        aa = aa + ( yy(jj+1)-yy(jj) )*celsum(dn,ii,jj)
      enddo
      ss(ii) = ss(ii-1) + aa*( xx(ii+1)-xx(ii) )
    enddo
    ss(1:Nx-2) = ss(1:Nx-2)/ss(Nx-1)
    ss(Nx-1) = 1
! Find first maximum of density starting from the right,
! and make density constant from there to the left. 
    call flatleft( ss ,xx ,Nx )
    end subroutine
!
    function celsum(dn,ii,jj) result(rslt)
    integer,intent(in) :: dn,ii,jj
    !(realknd2!) :: rslt
    if (dn.eq.1) then
      rslt = obj%h(ii  ,jj,1) + obj%h(ii  ,jj+1,1) &
           + obj%h(ii+1,jj,1) + obj%h(ii+1,jj+1,1)
    elseif (dn.eq.2) then
      rslt = obj%h(jj  ,ii,1) + obj%h(jj  ,ii+1,1) &
           + obj%h(jj+1,ii,1) + obj%h(jj+1,ii+1,1)
    endif
    end function
!
  end subroutine


  subroutine fill_marg3( obj ,dmn )
  class(trivinp_type) :: obj
  logical,intent(in) :: dmn(3)
  if (dmn(1)) call fill_s( 1 ,obj%sx ,obj%x,obj%Nx, obj%y,obj%Ny ,obj%z,obj%Nz )
  if (dmn(2)) call fill_s( 2 ,obj%sy ,obj%y,obj%Ny, obj%z,obj%Nz ,obj%x,obj%Nx )
  if (dmn(3)) call fill_s( 3 ,obj%sz ,obj%z,obj%Nz, obj%x,obj%Nx ,obj%y,obj%Ny )
!
  contains
!
    subroutine fill_s( dn ,ss ,xx,Nx ,yy,Ny ,zz,Nz )
    !(realknd2!),allocatable,intent(out) :: ss(:)
    integer,intent(in) :: Nx,Ny,Nz
    !(realknd1!),intent(in) :: xx(Nx),yy(Ny),zz(Nz)
    integer,intent(in) :: dn
    !(realknd1!) :: aa,bb
    integer :: ii,jj,kk
    allocate( ss(0:Nx-1) )
    ss(0) = 0
    do ii=1,Nx-1
      aa = 0
      do jj=1,Ny-1
        bb = 0
        do kk=1,Nz-1
          bb = bb + ( zz(kk+1)-zz(kk) )*celsum(dn,ii,jj,kk)
        enddo
        aa = aa + bb*( yy(jj+1)-yy(jj) )
      enddo
      ss(ii) = ss(ii-1) + aa*( xx(ii+1)-xx(ii) )
    enddo
    ss(1:Nx-2) = ss(1:Nx-2)/ss(Nx-1)
    ss(Nx-1) = 1
! Find first maximum of density starting from the right,
! and make density constant from there to the left. 
    call flatleft( ss ,xx ,Nx )
    end subroutine
!
    function celsum(dn,ii,jj,kk) result(rslt)
    integer,intent(in) :: dn,ii,jj,kk
    !(realknd2!) :: rslt
    if (dn.eq.1) then
      rslt = obj%h(ii  ,jj  ,kk) + obj%h(ii  ,jj  ,kk+1) &
           + obj%h(ii  ,jj+1,kk) + obj%h(ii  ,jj+1,kk+1) &
           + obj%h(ii+1,jj  ,kk) + obj%h(ii+1,jj  ,kk+1) &
           + obj%h(ii+1,jj+1,kk) + obj%h(ii+1,jj+1,kk+1)
    elseif (dn.eq.2) then
      rslt = obj%h(kk  ,ii  ,jj) + obj%h(kk  ,ii  ,jj+1) &
           + obj%h(kk  ,ii+1,jj) + obj%h(kk  ,ii+1,jj+1) &
           + obj%h(kk+1,ii  ,jj) + obj%h(kk+1,ii  ,jj+1) &
           + obj%h(kk+1,ii+1,jj) + obj%h(kk+1,ii+1,jj+1)
    elseif (dn.eq.3) then
      rslt = obj%h(jj  ,kk  ,ii) + obj%h(jj  ,kk  ,ii+1) &
           + obj%h(jj  ,kk+1,ii) + obj%h(jj  ,kk+1,ii+1) &
           + obj%h(jj+1,kk  ,ii) + obj%h(jj+1,kk  ,ii+1) &
           + obj%h(jj+1,kk+1,ii) + obj%h(jj+1,kk+1,ii+1)
    endif
    end function
!
  end subroutine


  subroutine flatleft( ss ,xx ,nn )
  integer,intent(in) :: nn
  !(realknd1!),intent(in) :: xx(1:nn)
  !(realknd2!),intent(inout) :: ss(0:nn-1)
  !(realknd2!) :: aa,bb
  integer :: ii,jj
  ii = nn
  do ;ii=ii-1 ;if(ii.le.1)exit
    if ( (ss(ii-1)-ss(ii-2))/(xx(ii  )-xx(ii-1)) .lt. &
         (ss(ii  )-ss(ii-1))/(xx(ii+1)-xx(ii  )) ) exit
  enddo
  aa = ss(ii)
  bb = (ss(ii)-ss(ii-1))/(xx(ii+1)-xx(ii))
  do jj=1,ii
    ss(jj) = ss(jj-1) + (xx(jj+1)-xx(jj))*bb
  enddo
  ss(ii+1:nn-1) = ss(ii+1:nn-1) + (ss(ii)-aa)
  ss(1:nn-2) = ss(1:nn-2)/ss(nn-1)
  ss(nn-1) = 1
  end subroutine


  subroutine gnrt_x( obj ,ww ,rslt ,rho )
  class(trivinp_type) :: obj
  !(realknd2!),intent(out) :: rslt,ww
  !(realknd2!),intent(in)  :: rho
  integer :: i0,i1,ii
  associate( NN=>obj%Nx ,ss=>obj%sx ,xx=>obj%x ,map=>obj%map(1) )
  include 'gnrt_marg.h90'
  end associate
  end subroutine

  subroutine gnrt_y( obj ,ww ,rslt ,rho )
  class(trivinp_type) :: obj
  !(realknd2!),intent(out) :: rslt,ww
  !(realknd2!),intent(in ) :: rho
  integer :: i0,i1,ii
  associate( NN=>obj%Ny ,ss=>obj%sy ,xx=>obj%y ,map=>obj%map(2) )
  include 'gnrt_marg.h90'
  end associate
  end subroutine

  subroutine gnrt_z( obj ,ww ,rslt ,rho )
  class(trivinp_type) :: obj
  !(realknd2!),intent(out) :: rslt,ww
  !(realknd2!),intent(in ) :: rho
  integer :: i0,i1,ii
  associate( NN=>obj%Nz ,ss=>obj%sz ,xx=>obj%z ,map=>obj%map(3) )
  include 'gnrt_marg.h90'
  end associate
  end subroutine

  
  subroutine plot_x( obj ,filename ,writeUnit )
  class(trivinp_type) :: obj
  character(*),intent(in) :: filename
  integer,intent(in),optional :: writeUnit
  integer :: wUnit,ii
  wUnit = deffile
  if (present(writeUnit)) wUnit = writeUnit
  open(wUnit,file=trim(filename),status='replace')
  do ii=1,obj%Nx
    write(wUnit,*) ii,obj%x(ii) ,obj%h(ii,1,1)
  enddo
  close(wUnit)
  end subroutine

  subroutine plot_y( obj ,filename ,writeUnit )
  class(trivinp_type) :: obj
  character(*),intent(in) :: filename
  integer,intent(in),optional :: writeUnit
  integer :: wUnit,ii
  wUnit = deffile
  if (present(writeUnit)) wUnit = writeUnit
  open(wUnit,file=trim(filename),status='replace')
  do ii=1,obj%Ny-1
    write(wUnit,*) obj%y(ii) &
                  ,(obj%sy(ii)-obj%sy(ii-1))/(obj%y(ii+1)-obj%y(ii))
  enddo
  close(wUnit)
  end subroutine


  function trivinp_add( aa ,bb ) result(cc)
  type(trivinp_type),intent(in) :: aa,bb
  type(trivinp_type) :: cc
  if (allocated(cc%x )) deallocate(cc%x )
  if (allocated(cc%y )) deallocate(cc%y )
  if (allocated(cc%z )) deallocate(cc%z )
  if (allocated(cc%h )) deallocate(cc%h )
  if (allocated(cc%sx)) deallocate(cc%sx)
  if (allocated(cc%sy)) deallocate(cc%sy)
  if (allocated(cc%sz)) deallocate(cc%sz)
  if (aa%Nx.ne.bb%Nx) call error('aa%x and bb%x inconsistent')
  if (aa%Ny.ne.bb%Ny) call error('aa%y and bb%y inconsistent')
  if (aa%Nz.ne.bb%Nz) call error('aa%z and bb%z inconsistent')
  cc%Nx = aa%Nx
  cc%Ny = aa%Ny
  cc%Nz = aa%Nz
  if (allocated(aa%x)) then
    if (any(aa%x.ne.bb%x)) call error('aa%x and bb%x inconsistent') 
    cc%x = aa%x
  endif
  if (allocated(aa%y)) then
    if (any(aa%y.ne.bb%y)) call error('aa%y and bb%y inconsistent') 
    cc%y = aa%y
  endif
  if (allocated(aa%z)) then
    if (any(aa%z.ne.bb%z)) call error('aa%z and bb%z inconsistent') 
    cc%z = aa%z
  endif
  cc%h = aa%h + bb%h
  if     (.not.allocated(cc%y)) then ;call fill_marg1( cc )
  elseif (.not.allocated(cc%z)) then ;call fill_marg2( cc ,[.false.,.true.] )
  else                               ;call fill_marg3( cc ,[.false.,.true.,.false.] )
  endif
!
  contains
!
    subroutine error( message )
    character(*),intent(in) :: message
    if (errru.ge.0) write(errru,*) 'ERROR in trivinp_add: '//trim(message)
    stop
    end subroutine
!
  end function
  

  function trivinp_r_t( rr ,aa ) result(bb)
  type(trivinp_type),intent(in) :: aa
  !(realknd2!),intent(in) :: rr
  type(trivinp_type) :: bb
  bb = aa
  bb%h = rr*bb%h
  end function

  function trivinp_t_r( aa ,rr) result(bb)
  type(trivinp_type),intent(in) :: aa
  !(realknd2!),intent(in) :: rr
  type(trivinp_type) :: bb
  bb = aa
  bb%h = rr*bb%h
  end function


  subroutine thinning( inFile ,outFile ,rwUnit )
  character(*)     ,intent(in) :: inFile,outFile
  integer,optional ,intent(in) :: rwUnit
  type(trivinp_type) :: obj
  integer :: iunit,ix,iy,iz
  logical :: keep
  iunit = deffile
  if (present(rwUnit)) iunit = rwUnit
  call obj%read_file( inFile ,iunit )
  open(unit=iunit,file='removed.dat',status='replace')
    call thin_dim( 3 ,obj%Nz ,obj%Nx ,obj%Ny )
    call thin_dim( 1 ,obj%Nx ,obj%Ny ,obj%Nz )
    call thin_dim( 2 ,obj%Ny ,obj%Nx ,obj%Nz )
  close(iunit)
  open(unit=iunit,file=trim(outFile),status='new')
    do ix=1,obj%Nx
    do iy=1,obj%Ny
    do iz=1,obj%Nz
      write(rwUnit,*) obj%x(ix),obj%y(iy),obj%z(iz),obj%h(ix,iy,iz)
    enddo
    enddo
    enddo
  close(iunit)
!
  contains
!
    subroutine thin_dim( dn ,Ni ,Nj ,Nk )
    integer,intent(in) :: dn,Nj,Nk
    integer,intent(inout) :: Ni
    integer :: ii,jj,kk
    logical :: keep
    ii = 2
    do while (ii.lt.Ni)
      keep = .false.
      do jj=1,Nj
        do kk=1,Nk
          if (jj.gt.1.and.kk.gt.1) then
            keep = keep.or.compare( dn ,ii ,jj-1,jj ,kk-1,kk )
            if (keep) exit
          endif
          if (jj.lt.Nj.and.kk.gt.1) then
            keep = keep.or.compare( dn ,ii ,jj,jj+1 ,kk-1,kk )
            if (keep) exit
          endif
          if (jj.gt.1.and.kk.lt.Nk) then
            keep = keep.or.compare( dn ,ii ,jj-1,jj ,kk,kk+1 )
            if (keep) exit
          endif
          if (jj.lt.Nj.and.kk.lt.Nk) then
            keep = keep.or.compare( dn ,ii ,jj,jj+1 ,kk,kk+1 )
            if (keep) exit
          endif
        enddo
        if (keep) exit
      enddo
      if (keep) then
        ii = ii+1
      else
        call shift( dn ,ii ,Ni ,Nj ,Nk )
        Ni = Ni-1
      endif
    enddo
    end subroutine
!
    subroutine shift( dn ,ii ,Ni ,Nj ,Nk )
    integer,intent(in) :: dn,ii,Ni,Nj,Nk
    if (dn.eq.1) then
      write(iunit,*) 'x=',obj%x(ii)
      obj%x( ii:Ni-1 ) = obj%x( ii+1:Ni )
      obj%h( ii:Ni-1 ,1:Nj ,1:Nk ) = obj%h( ii+1:Ni ,1:Nj ,1:Nk )
    elseif (dn.eq.2) then
      write(iunit,*) 'y=',obj%y(ii)
      obj%y( ii:Ni-1 ) = obj%y( ii+1:Ni )
      obj%h( 1:Nj ,ii:Ni-1 ,1:Nk ) = obj%h( 1:Nj ,ii+1:Ni ,1:Nk )
    elseif (dn.eq.3) then
      write(iunit,*) 'z=',obj%z(ii)
      obj%z( ii:Ni-1 ) = obj%z( ii+1:Ni )
      obj%h( 1:Nj ,1:Nk ,ii:Ni-1 ) = obj%h( 1:Nj ,1:Nk ,ii+1:Ni )
    endif
    end subroutine
!
    function compare( dn ,ii ,j0,j1 ,k0,k1 ) result(rslt)
    integer,intent(in) :: dn ,ii ,j0,j1 ,k0,k1
    logical :: rslt
    !(realknd1!) :: xx,yy,zz,aa,bb
    rslt = .false.
    if (dn.eq.1) then
      yy = (obj%y(j0)+obj%y(j1))/2
      zz = (obj%z(k0)+obj%z(k1))/2
      xx = (obj%x(ii-1)+obj%x(ii))/2
        aa = obj%interpolate( ii-1,ii   ,j0,j1 ,k0,k1 ,xx,yy,zz )
        bb = obj%interpolate( ii-1,ii+1 ,j0,j1 ,k0,k1 ,xx,yy,zz )
        rslt = rslt.or.h_notEqual(aa,bb)
      xx = (obj%x(ii)+obj%x(ii+1))/2
        aa = obj%interpolate( ii  ,ii+1 ,j0,j1 ,k0,k1 ,xx,yy,zz )
        bb = obj%interpolate( ii-1,ii+1 ,j0,j1 ,k0,k1 ,xx,yy,zz )
        rslt = rslt.or.h_notEqual(aa,bb)
    elseif (dn.eq.2) then
      xx = (obj%x(j0)+obj%x(j1))/2
      zz = (obj%z(k0)+obj%z(k1))/2
      yy = (obj%y(ii-1)+obj%y(ii))/2
        aa = obj%interpolate( j0,j1 ,ii-1,ii   ,k0,k1 ,xx,yy,zz )
        bb = obj%interpolate( j0,j1 ,ii-1,ii+1 ,k0,k1 ,xx,yy,zz )
        rslt = rslt.or.h_notEqual(aa,bb)
      yy = (obj%y(ii)+obj%y(ii+1))/2
        aa = obj%interpolate( j0,j1 ,ii  ,ii+1 ,k0,k1 ,xx,yy,zz )
        bb = obj%interpolate( j0,j1 ,ii-1,ii+1 ,k0,k1 ,xx,yy,zz )
        rslt = rslt.or.h_notEqual(aa,bb)
    elseif (dn.eq.3) then
      xx = (obj%x(j0)+obj%x(j1))/2
      yy = (obj%y(k0)+obj%y(k1))/2
      zz = (obj%z(ii-1)+obj%z(ii))/2
        aa = obj%interpolate( j0,j1 ,k0,k1 ,ii-1,ii   ,xx,yy,zz )
        bb = obj%interpolate( j0,j1 ,k0,k1 ,ii-1,ii+1 ,xx,yy,zz )
        rslt = rslt.or.h_notEqual(aa,bb)
      zz = (obj%y(ii)+obj%y(ii+1))/2
        aa = obj%interpolate( j0,j1 ,k0,k1 ,ii  ,ii+1 ,xx,yy,zz )
        bb = obj%interpolate( j0,j1 ,k0,k1 ,ii-1,ii+1 ,xx,yy,zz )
        rslt = rslt.or.h_notEqual(aa,bb)
    endif
    end function
!
    function h_notEqual(xx,yy) result(rslt)
    !(realknd1!),intent(in) :: xx,yy
    !(realknd1!),parameter :: small=1e-6
    logical :: rslt
    if     (xx.eq.0) then ;rslt=(yy.ne.0)
    elseif (yy.eq.0) then ;rslt=(xx.ne.0)
                     else ;rslt=(abs(1-xx/yy).ge.small)
    endif
    end function
!
  end subroutine


  subroutine create1( obj ,xfunc ,xmin,xmax,Nmax )
  class(trivinp_type) :: obj
  procedure(xfunc_1d) :: xfunc
  !(realknd2!),intent(in) :: xmin,xmax
  integer,intent(in) :: Nmax
  integer :: Npos,iMax,ii
  integer,allocatable :: pos(:)
  !(realknd2!),allocatable :: xval(:),hval(:),crit(:)
  !(realknd2!) :: h01,h12,d01,d12
  obj%Ndim = 1
  obj%Nx = Nmax
  obj%Ny = 1
  obj%Nz = 1
  Npos = 3
  allocate(pos(obj%Nx))
  allocate(crit(obj%Nx))
  allocate(xval(obj%Nx))
  allocate(hval(obj%Nx))
  pos(1) = 1
  pos(2) = 3
  pos(3) = 2
  xval(pos(1)) = xmin
  xval(pos(2)) = (xmin+xmax)/2
  xval(pos(3)) = xmax
  hval(pos(1)) = xfunc(xval(pos(1)))
  hval(pos(2)) = xfunc(xval(pos(2)))
  hval(pos(3)) = xfunc(xval(pos(3)))
  crit(pos(1)) = -1
  call evalCrit(2)
  crit(pos(3)) = -1
  do ;if(Npos.ge.obj%Nx)exit
    iMax = maxloc(crit(pos(1:Npos)),1)
    Npos = Npos+1
    pos(iMax+2:Npos) = pos(iMax+1:Npos-1)
    d01 = xval(pos(iMax  ))-xval(pos(iMax-1))
    d12 = xval(pos(iMax+1))-xval(pos(iMax  ))
    if (d01.gt.d12) then
      pos(iMax+1) = pos(iMax)
      pos(iMax  ) = Npos
      ii = iMax 
    elseif (d12.gt.d01) then
      pos(iMax+1) = Npos
      ii = iMax+1 
    else
      h01 = abs(hval(pos(iMax  ))-hval(pos(iMax-1)))
      h12 = abs(hval(pos(iMax+1))-hval(pos(iMax  )))
      if (h01.gt.h12) then
        pos(iMax+1) = pos(iMax)
        pos(iMax  ) = Npos
        ii = iMax 
      else
        pos(iMax+1) = Npos
        ii = iMax+1 
      endif
    endif
    xval(pos(ii)) = (xval(pos(ii+1))+xval(pos(ii-1)))/2
    hval(pos(ii)) = xfunc(xval(pos(ii)))
    call evalCrit(ii-1)
    call evalCrit(ii  )
    call evalCrit(ii+1)
  enddo
  call dalloc(obj)
  allocate(obj%x(obj%Nx))
  allocate(obj%h(obj%Nx,1,1))
  obj%x(1:obj%Nx    ) = xval(pos(1:obj%Nx))
  obj%h(1:obj%Nx,1,1) = hval(pos(1:obj%Nx))
  contains
    subroutine evalCrit(ii)
    integer,intent(in) :: ii
    if (ii.eq.1.or.ii.eq.Npos) return
    crit(pos(ii)) = ( hval(pos(ii+1))*(xval(pos(ii))-xval(pos(ii-1))) &
                    + hval(pos(ii-1))*(xval(pos(ii+1))-xval(pos(ii))) ) &
                    /( xval(pos(ii+1))-xval(pos(ii-1)) )
    crit(pos(ii)) = abs(crit(pos(ii))-hval(pos(ii)))   
    end subroutine
  end subroutine
  
  
  
  subroutine create2( obj ,xfunc ,xmin,xmax,Nxmax ,ymin,ymax,Nymax )
  class(trivinp_type) :: obj
  procedure(xfunc_2d) :: xfunc
  !(realknd2!),intent(in) :: xmin,xmax,ymin,ymax
  integer,intent(in) :: Nxmax,Nymax
  integer :: Nxpos,Nypos,iMax,ii,xmlt,ymlt,jj,xcnt,ycnt
  integer,allocatable :: xpos(:),ypos(:)
  !(realknd2!),allocatable :: xval(:),yval(:),hval(:,:),xcrt(:),ycrt(:)
  !(realknd2!) :: h01,h12,d01,d12
  obj%Ndim = 2
  if (Nxmax.ge.Nymax) then
    xmlt = Nxmax/Nymax
    ymlt = 1
    obj%Nx = 3+xmlt*Nymax
    obj%Ny = 3+Nymax
  else
    xmlt = 1
    ymlt = Nymax/Nxmax
    obj%Nx = 3+Nxmax
    obj%Ny = 3+ymlt*Nxmax
  endif
  obj%Nz = 1
  Nxpos = 3 ;Nypos = 3
  allocate(xpos(1:obj%Nx),ypos(1:obj%Ny))
  allocate(xcrt(1:obj%Nx),ycrt(1:obj%Ny))
  allocate(xval(1:obj%Nx),yval(1:obj%Ny))
  allocate(hval(1:obj%Nx,1:obj%Ny))
  xpos(1) = 1  ;ypos(1) = 1
  xpos(2) = 3  ;ypos(2) = 3
  xpos(3) = 2  ;ypos(3) = 2
  xval(xpos(1)) = xmin           ;yval(ypos(1)) = ymin
  xval(xpos(2)) = (xmin+xmax)/2  ;yval(ypos(2)) = (ymin+ymax)/2
  xval(xpos(3)) = xmax           ;yval(ypos(3)) = ymax         
  do ii=1,3
  do jj=1,3
    hval(xpos(ii),ypos(jj)) = xfunc(xval(xpos(ii)),yval(ypos(jj)))
  enddo
  enddo
  xcrt(xpos(1)) = -1  ;ycrt(ypos(1)) = -1
  call evalXcrt(2)    ;call evalYcrt(2)
  xcrt(xpos(3)) = -1  ;ycrt(ypos(3)) = -1
  do ;if(Nxpos.ge.obj%Nx.and.Nypos.ge.obj%Ny)exit
    do xcnt=1,xmlt
      iMax = maxloc(xcrt(xpos(1:Nxpos)),1)
      Nxpos = Nxpos+1
      xpos(iMax+2:Nxpos) = xpos(iMax+1:Nxpos-1)
      d01 = xval(xpos(iMax  ))-xval(xpos(iMax-1))
      d12 = xval(xpos(iMax+1))-xval(xpos(iMax  ))
      if (d01.gt.d12) then
        xpos(iMax+1) = xpos(iMax)
        xpos(iMax  ) = Nxpos
        ii = iMax 
      elseif (d12.gt.d01) then
        xpos(iMax+1) = Nxpos
        ii = iMax+1 
      else
        h01 = 0
        h12 = 0
        do jj=1,Nypos
          h01 = h01 + abs(hval(xpos(iMax  ),ypos(jj))-hval(xpos(iMax-1),ypos(jj)))
          h12 = h12 + abs(hval(xpos(iMax+1),ypos(jj))-hval(xpos(iMax  ),ypos(jj)))
        enddo
        if (h01.gt.h12) then
          xpos(iMax+1) = xpos(iMax)
          xpos(iMax  ) = Nxpos
          ii = iMax 
        else
          xpos(iMax+1) = Nxpos
          ii = iMax+1 
        endif
      endif
      xval(xpos(ii)) = (xval(xpos(ii+1))+xval(xpos(ii-1)))/2
      do jj=1,Nypos
        hval(xpos(ii),ypos(jj)) = xfunc(xval(xpos(ii)),yval(ypos(jj)))
      enddo
      call evalXcrt(ii-1)
      call evalXcrt(ii  )
      call evalXcrt(ii+1)
    enddo
    do ycnt=1,ymlt
      iMax = maxloc(ycrt(ypos(1:Nypos)),1)
      Nypos = Nypos+1
      ypos(iMax+2:Nypos) = ypos(iMax+1:Nypos-1)
      d01 = yval(ypos(iMax  ))-yval(ypos(iMax-1))
      d12 = yval(ypos(iMax+1))-yval(ypos(iMax  ))
      if (d01.gt.d12) then
        ypos(iMax+1) = ypos(iMax)
        ypos(iMax  ) = Nypos
        ii = iMax 
      elseif (d12.gt.d01) then
        ypos(iMax+1) = Nypos
        ii = iMax+1 
      else
        h01 = 0
        h12 = 0
        do jj=1,Nxpos
          h01 = h01 + abs(hval(xpos(jj),ypos(iMax  ))-hval(xpos(jj),ypos(iMax-1)))
          h12 = h12 + abs(hval(xpos(jj),ypos(iMax+1))-hval(xpos(jj),ypos(iMax  )))
        enddo
        if (h01.gt.h12) then
          ypos(iMax+1) = ypos(iMax)
          ypos(iMax  ) = Nypos
          ii = iMax 
        else
          ypos(iMax+1) = Nypos
          ii = iMax+1 
        endif
      endif
      yval(ypos(ii)) = (yval(ypos(ii+1))+yval(ypos(ii-1)))/2
      do jj=1,Nxpos
        hval(xpos(jj),ypos(ii)) = xfunc(xval(xpos(jj)),yval(ypos(ii)))
      enddo
      call evalYcrt(ii-1)
      call evalYcrt(ii  )
      call evalYcrt(ii+1)
    enddo
  enddo
  call dalloc(obj)
  allocate(obj%x(1:obj%Nx),obj%y(1:obj%Ny))
  allocate(obj%h(1:obj%Nx,1:obj%Ny,1))
  obj%x(1:obj%Nx) = xval(xpos(1:obj%Nx))
  obj%y(1:obj%Ny) = yval(ypos(1:obj%Ny))
  obj%h(1:obj%Nx,1:obj%Ny,1) = hval(xpos(1:obj%Nx),ypos(1:obj%Ny))
  contains
    subroutine evalXcrt(ii)
    integer,intent(in) :: ii
    integer :: jj
    !(realknd2!) :: hh
    if (ii.eq.1.or.ii.eq.Nxpos) return
    xcrt(xpos(ii)) = 0
    do jj=1,Nypos
      hh = ( hval(xpos(ii+1),ypos(jj))*(xval(xpos(ii))-xval(xpos(ii-1))) &
           + hval(xpos(ii-1),ypos(jj))*(xval(xpos(ii+1))-xval(xpos(ii))) ) &
           /( xval(xpos(ii+1))-xval(xpos(ii-1)) )
      hh = abs(hh-hval(xpos(ii),ypos(jj)))
      xcrt(xpos(ii)) = xcrt(xpos(ii)) + hh  
    enddo
    end subroutine
    subroutine evalYcrt(ii)
    integer,intent(in) :: ii
    integer :: jj
    !(realknd2!) :: hh
    if (ii.eq.1.or.ii.eq.Nypos) return
    ycrt(ypos(ii)) = 0
    do jj=1,Nxpos
      hh = ( hval(xpos(jj),ypos(ii+1))*(yval(ypos(ii))-yval(ypos(ii-1))) &
           + hval(xpos(jj),ypos(ii-1))*(yval(ypos(ii+1))-yval(ypos(ii))) ) &
           /( yval(ypos(ii+1))-yval(ypos(ii-1)) )
      hh = abs(hh-hval(xpos(jj),ypos(ii)))
      ycrt(ypos(ii)) = ycrt(ypos(ii)) + hh  
    enddo
    end subroutine
  end subroutine
  

  subroutine write_file( obj ,filename ,writeUnit )
  class(trivinp_type) :: obj
  character(*)     ,intent(in)  :: filename
  integer,optional ,intent(in)  :: writeUnit
  integer :: wUnit,ii,jj,kk
  wUnit = deffile
  if (present(writeUnit)) wUnit = writeUnit
  open(wUnit,file=filename,status='replace')
  select case (obj%Ndim)
  case(1)
    do ii=1,obj%Nx
      write(wUnit,'(2e16.8)') obj%x(ii),obj%h(ii,1,1)
    enddo
  case(2)
    do ii=1,obj%Nx
    do jj=1,obj%Ny
      write(wUnit,'(3e16.8)') obj%x(ii),obj%y(jj),obj%h(ii,jj,1)
    enddo
    enddo
  case(3)
    do ii=1,obj%Nx
    do jj=1,obj%Ny
    do kk=1,obj%Nz
      write(wUnit,'(4e16.8)') obj%x(ii),obj%y(jj),obj%z(kk),obj%h(ii,jj,kk)
    enddo
    enddo
    enddo
  end select
  close(wUnit)
  end subroutine

  
  


end module


