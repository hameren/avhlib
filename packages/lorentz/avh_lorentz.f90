module avh_lorentz
  !(usekindmod!)
  use avh_mathcnst
  use avh_iounits
  use avh_prnt

  implicit none
  private
  public :: lorentz_type,init_lorentz
  public :: momDef,xVector,xSpinor,justConvert,vecD2vec,momD2vec
  public :: momD2mom,momSqr,KangFmom,KsqrFmom,BangFmom,BsqrFmom
  public :: braFket,ketFbra,justReturn,scaMul,vecFmom,ketFmom
  public :: putToZero,oscPlus,contract
  public :: planarMomenta,gluonCurrents,print_lorentz
  public :: xScalar
  public :: conj,ellBasis,print_4mom,print_4real
  public :: xternalArgs_type
  public :: gggVertex,ggggVertex,ffvVertex,vvvvVertex
  public :: sssVertex,ssssVertex,ffsVertex,vvsVertex,vvssVertex
  public :: s2gVertex,s3gVertex,s4gVertex
  public :: gPropagator,vPropagator,fPropagator,sPropagator
  public :: ePropagator
  public :: WARDTEST,XDEFINED,POLARANG,USEAUXMOM
  public :: back2realmom,back2cmplxmom

  integer,parameter :: WARDTEST=9,XDEFINED=8,POLARANG=7,USEAUXMOM=6

  integer ,parameter :: momSiz=4,vecSiz=4,spiSiz=4
  integer ,parameter :: oscSiz=4 ! maximum of the ones above
  integer ,parameter :: typSiz=3

  type :: lorentz_type
    private
    !(complex2!) :: c(1:oscsiz)   ! complex components
    !(complex2!) :: s             ! complex scalar
    !(integer0!) :: Typ(1:typSiz)
  end type
  
  type :: xternalArgs_type
    type(lorentz_type) :: momentum,auxVec
    integer :: identity,helicity
    !(realknd2!) :: polarangle
  end type

  !(integer0!),parameter :: nulTyp=0
  !(integer0!),parameter :: momTyp=1,vecTyp=2,braTyp=4,ketTyp=8,scaTyp=16
  !(integer0!),parameter :: angTyp=1,sqrTyp=2,plsTyp=4,minTyp=8

  integer,parameter :: vecPLS=1 ,vecMIN=2 ,vecTRA=3 ,vecTSR=4
  integer,parameter :: vec11 =1 ,vec22 =2 ,vec21 =3 ,vec12 =4

  integer,parameter :: momPLS=1 ,momMIN=2 ,momTRA=3 ,momTSR=4
  integer,parameter :: mom11 =1 ,mom22 =2 ,mom21 =3 ,mom12 =4

  integer,parameter :: Bsqr1=1     ,Bsqr2=2     ,Bang1=3     ,Bang2=4
  integer,parameter :: Ksqr2=Bsqr1 ,Ksqr1=Bsqr2 ,Kang2=Bang1 ,Kang1=Bang2

  interface justConvert
    module procedure justConvert0,justConvert1
  end interface
  interface scaMul
    module procedure scaMul_r,scaMul_c
  end interface
  interface momDef
    module procedure momDef_r,momDef_c ,momDef_rr
    module procedure momDef_ri,momDef_ci,momDef_rri,momDef_rrri
  end interface
  interface vecFmom
    module procedure vecFmom_0,vecFmom_1,vecFmom_2
  end interface
  interface xVector
    module procedure xVector_a,xVector_b,xVector_c
  end interface
  interface xSpinor
    module procedure xSpinor_a,xSpinor_b
  end interface
  interface xScalar
    module procedure xScalar_0,xScalar_r,xScalar_c
  end interface

  logical :: initd=.false.

contains


  subroutine print_lorentz( aa ,ndec ,iunit )
  intent(in) :: aa,ndec,iunit
  optional :: iunit
  type(lorentz_type) :: aa
  integer :: ndec,iunit,nunit
  nunit=stdout ;if(present(iunit))nunit=iunit
  if (nunit.le.0) return
  select case (aa%Typ(1))
    case default ;write(nunit,*) 'WARNING from print_lorentz: type does not exist'
    case (scaTyp)
      select case (aa%Typ(2))
        case (nulTyp) ;write(nunit,*) 'sr  '//prnt( aa%c(1),ndec )
        case (plsTyp) ;write(nunit,*) 'sr+ '//prnt( aa%c(1),ndec )
        case (minTyp) ;write(nunit,*) 'sr- '//prnt( aa%c(1),ndec )
      end select
    case (vecTyp) ;write(nunit,*) 'vec '//prnt( aa%c(1:vecSiz),ndec )
    case (momTyp) ;write(nunit,*) 'mom '//prnt( aa%c(1:momSiz),ndec )
    case (braTyp)
      select case (aa%Typ(2))
        case (nulTyp) ;write(nunit,*) 'bra '//prnt( aa%c(1:spiSiz),ndec )
        case (angTyp) ;write(nunit,*) '< | '//prnt( aa%c(1:spiSiz),ndec ) 
        case (sqrTyp) ;write(nunit,*) '[ | '//prnt( aa%c(1:spiSiz),ndec ) 
      end select
    case (ketTyp)
      select case (aa%Typ(2))
        case (nulTyp) ;write(nunit,*) 'ket '//prnt( aa%c(1:spiSiz),ndec )
        case (angTyp) ;write(nunit,*) '| > '//prnt( aa%c(1:spiSiz),ndec ) 
        case (sqrTyp) ;write(nunit,*) '| ] '//prnt( aa%c(1:spiSiz),ndec ) 
      end select
  end select
  end subroutine

  subroutine print_4mom( aa ,ndec ,iunit )
  intent(in) :: aa,ndec,iunit
  optional :: iunit
  type(lorentz_type) :: aa
  integer :: ndec,iunit,nunit
  nunit=stdout ;if(present(iunit))nunit=iunit
  if (nunit.le.0) return
  select case(aa%Typ(1))
    case default ;write(nunit,*) 'WARNING from print_4mom: type does not exist'
    case (scaTyp) ;return
    case (braTyp) ;return
    case (ketTyp) ;return
    case (vecTyp)
      write(nunit,*) 'vec '//prnt((aa%c(momPLS)+aa%c(momMIN))/2,ndec ) &
                      //' '//prnt((aa%c(momTRA)+aa%c(momTSR))/2,ndec ) &
                      //' '//prnt((aa%c(momTRA)-aa%c(momTSR))/2/cIMA,ndec ) &
                      //' '//prnt((aa%c(momPLS)-aa%c(momMIN))/2,ndec )
    case (momTyp)
      write(nunit,*) 'mom '//prnt((aa%c(momPLS)+aa%c(momMIN))/2,ndec ) &
                      //' '//prnt((aa%c(momTRA)+aa%c(momTSR))/2,ndec ) &
                      //' '//prnt((aa%c(momTRA)-aa%c(momTSR))/2/cIMA,ndec ) &
                      //' '//prnt((aa%c(momPLS)-aa%c(momMIN))/2,ndec )
  end select
  end subroutine

  subroutine print_4real( aa ,ndec ,iunit )
  intent(in) :: aa,ndec,iunit
  optional :: iunit
  type(lorentz_type) :: aa
  integer :: ndec,iunit,nunit
  nunit=stdout ;if(present(iunit))nunit=iunit
  if (nunit.le.0) return
  select case(aa%Typ(1))
    case default ;write(nunit,*) 'WARNING from print_4real: type does not exist'
    case (scaTyp) ;return
    case (braTyp) ;return
    case (ketTyp) ;return
    case (vecTyp)
      write(nunit,*) 'vec '//prnt(real((aa%c(momPLS)+aa%c(momMIN))/2     ),ndec ) &
                      //' '//prnt(real((aa%c(momTRA)+aa%c(momTSR))/2     ),ndec ) &
                      //' '//prnt(real((aa%c(momTRA)-aa%c(momTSR))/2/cIMA),ndec ) &
                      //' '//prnt(real((aa%c(momPLS)-aa%c(momMIN))/2     ),ndec )
    case (momTyp)
      write(nunit,*) 'mom '//prnt(real((aa%c(momPLS)+aa%c(momMIN))/2     ),ndec ) &
                      //' '//prnt(real((aa%c(momTRA)+aa%c(momTSR))/2     ),ndec ) &
                      //' '//prnt(real((aa%c(momTRA)-aa%c(momTSR))/2/cIMA),ndec ) &
                      //' '//prnt(real((aa%c(momPLS)-aa%c(momMIN))/2     ),ndec )
  end select
  end subroutine


  subroutine init_lorentz
  integer :: ii,jj
  if (initd) return ;initd=.true.
  call init_mathcnst
  end subroutine


  function conj(aa) result(bb)
  intent(in) :: aa
  type(lorentz_type) :: aa,bb
  bb%c(vecPLS) = conjg(aa%c(vecPLS))
  bb%c(vecMIN) = conjg(aa%c(vecMIN))
  bb%c(vecTRA) = conjg(aa%c(vecTSR))
  bb%c(vecTSR) = conjg(aa%c(vecTRA))
  bb%s   = conjg(aa%s)
  bb%Typ = aa%Typ
  end function


  subroutine putToZero( aa ,nlow,nupp )
  integer,intent(in) :: nlow,nupp
  type(lorentz_type) :: aa(1:nupp)
  integer :: ii
  do ii=nlow,nupp
    aa(ii)%c = 0
    aa(ii)%s = 0
    aa(ii)%Typ = [nulTyp,nulTyp,nulTyp]
  enddo
  end subroutine

  function oscPlus( aa,bb ) result(cc)
  intent(in) :: aa,bb
  type(lorentz_type) :: aa,bb,cc
  cc%c = aa%c + bb%c
  cc%Typ = aa%Typ
  if (cc%Typ(1).eq.nulTyp) then
     cc%Typ = bb%Typ
  elseif (aa%Typ(2).ne.bb%Typ(2)) then
     cc%Typ(2) = nulTyp
  endif
  end function


  function scaMul_r( aa,bb ) result(cc)
  intent(in) :: aa,bb
  !(realknd2!) :: aa
  type(lorentz_type) :: bb,cc
  cc%c = aa * bb%c
  cc%Typ = bb%Typ
  end function

  function scaMul_c( aa,bb ) result(cc)
  intent(in) :: aa,bb
  !(complex2!) :: aa
  type(lorentz_type) :: bb,cc
  cc%c = aa * bb%c
  cc%Typ = bb%Typ
  end function


  function justReturn( qq ) result(aa)
!************************************************
!************************************************
  !(complex2!) :: aa(4)
  type(lorentz_type),intent(in) :: qq
  aa = qq%c
  end function


  function justConvert0( cPLS,cMIN,cTRA,cTSR ) result(qq)
!************************************************
!************************************************
  !(complex2!),intent(in) :: cPLS,cMIN,cTRA,cTSR
  type(lorentz_type) :: qq
  qq%c(momPLS) = cPLS
  qq%c(momMIN) = cMIN
  qq%c(momTRA) = cTRA
  qq%c(momTSR) = cTSR
  end function

  function justConvert1( aa ) result(qq)
!************************************************
!************************************************
  !(complex2!),intent(in) :: aa(4)
  type(lorentz_type) :: qq
  qq%c(momPLS) = aa(1)
  qq%c(momMIN) = aa(2)
  qq%c(momTRA) = aa(3)
  qq%c(momTSR) = aa(4)
  end function


  function momDef_rr( p0,p2,p3,p1,ss ) result(qq)
!************************************************
!* Rotated input
!************************************************
  !(realknd2!),intent(in) :: p0,p1,p2,p3,ss
  type(lorentz_type) :: qq
  !(complex2!) :: p2i
  p2i = p2*cIMA
  qq%c(momPLS) = p0+p3
  qq%c(momMIN) = p0-p3
  qq%c(momTRA) = p1+p2i
  qq%c(momTSR) = p1-p2i
  qq%s = ss
  qq%Typ = [momTyp,nulTyp,nulTyp]
  end function

  function momDef_r( p0,p2,p3,p1 ) result(qq)
!************************************************
!* Rotated input
!************************************************
  intent(in) :: p0,p1,p2,p3
  !(realknd2!) :: p0,p1,p2,p3
  type(lorentz_type) :: qq
  !(complex2!) :: p2i
  p2i = p2*cIMA
  qq%c(momPLS) = p0+p3
  qq%c(momMIN) = p0-p3
  qq%c(momTRA) = p1+p2i
  qq%c(momTSR) = p1-p2i
  qq%Typ = [momTyp,nulTyp,nulTyp]
  end function

  function momDef_c( p0,p2,p3,p1 ) result(qq)
!************************************************
!* Rotated input
!************************************************
  intent(in) :: p0,p1,p2,p3
  !(complex2!) :: p0,p1,p2,p3,p2i
  type(lorentz_type) :: qq
  p2i = p2*cIMA
  qq%c(momPLS) = p0+p3
  qq%c(momMIN) = p0-p3
  qq%c(momTRA) = p1+p2i
  qq%c(momTSR) = p1-p2i
  qq%Typ = [momTyp,nulTyp,nulTyp]
  end function

  function momDef_ri( p ,l ) result(qq)
  intent(in) :: p,l
  !(realknd2!) :: p(0:3)
  integer :: l(1:3)
  type(lorentz_type) :: qq
  !(complex2!) :: p2i
  p2i = p(l(2))*cIMA
  qq%c(momPLS) = p(0)+p(l(3))
  qq%c(momMIN) = p(0)-p(l(3))
  qq%c(momTRA) = p(l(1))+p2i
  qq%c(momTSR) = p(l(1))-p2i
  qq%Typ = [momTyp,nulTyp,nulTyp]
  end function

  function momDef_ci( p ,l ) result(qq)
  intent(in) :: p,l
  !(complex2!) :: p(0:3),p2i
  integer :: l(1:3)
  type(lorentz_type) :: qq
  p2i = p(l(2))*cIMA
  qq%c(momPLS) = p(0)+p(l(3))
  qq%c(momMIN) = p(0)-p(l(3))
  qq%c(momTRA) = p(l(1))+p2i
  qq%c(momTSR) = p(l(1))-p2i
  qq%Typ = [momTyp,nulTyp,nulTyp]
  end function

  function momDef_rri( p ,s ,l ) result(qq)
  intent(in) :: p,s,l
  !(realknd2!) :: p(0:3),s
  integer :: l(1:3)
  type(lorentz_type) :: qq
  !(complex2!) :: p2i
  p2i = p(l(2))*cIMA
  qq%c(momPLS) = p(0)+p(l(3))
  qq%c(momMIN) = p(0)-p(l(3))
  qq%c(momTRA) = p(l(1))+p2i
  qq%c(momTSR) = p(l(1))-p2i
  qq%s = s
  qq%Typ = [momTyp,nulTyp,nulTyp]
  end function

  function momDef_rrri( E ,p ,s ,l ) result(qq)
  intent(in) :: E,p,s,l
  !(realknd2!) :: E,p(3),s
  integer :: l(1:3)
  type(lorentz_type) :: qq
  !(complex2!) :: p2i
  p2i = p(l(2))*cIMA
  qq%c(momPLS) = E+p(l(3))
  qq%c(momMIN) = E-p(l(3))
  qq%c(momTRA) = p(l(1))+p2i
  qq%c(momTSR) = p(l(1))-p2i
  qq%s = s
  qq%Typ = [momTyp,nulTyp,nulTyp]
  end function


   function back2realmom( qq ,vecPerm ) result(p)
!************************************************
!************************************************
  type(lorentz_type),intent(in) :: qq
  integer,intent(in) :: vecPerm(1:3)
  !(realknd2!) :: p(0:3)
  p(        0 ) = ( qq%c(momPLS) + qq%c(momMIN) )/2
  p(vecPerm(3)) = ( qq%c(momPLS) - qq%c(momMIN) )/2
  p(vecPerm(1)) = ( qq%c(momTRA) + qq%c(momTSR) )/2
  p(vecPerm(2)) = ( qq%c(momTRA) - qq%c(momTSR) )/(2*cIMA)
  end function

  function back2cmplxmom( qq ,vecPerm ) result(p)
!************************************************
!************************************************
  type(lorentz_type),intent(in) :: qq
  integer,intent(in) :: vecPerm(1:3)
  !(complex2!) :: p(0:3)
  p(        0 ) = ( qq%c(momPLS) + qq%c(momMIN) )/2
  p(vecPerm(3)) = ( qq%c(momPLS) - qq%c(momMIN) )/2
  p(vecPerm(1)) = ( qq%c(momTRA) + qq%c(momTSR) )/2
  p(vecPerm(2)) = ( qq%c(momTRA) - qq%c(momTSR) )/(2*cIMA)
  end function


  function contract( aa,bb ) result(rslt)
  intent(in) :: aa,bb
  type(lorentz_type) :: aa,bb
  !(complex2!) :: rslt
  if     (aa%Typ(1).eq.momTyp) then
    if     (bb%Typ(1).eq.momTyp) then ;rslt=momD2mom(aa,bb)/2
    elseif (bb%Typ(1).eq.vecTyp) then ;rslt=momD2vec(aa,bb)/2
    endif
  elseif (aa%Typ(1).eq.vecTyp) then
    if     (bb%Typ(1).eq.momTyp) then ;rslt=momD2vec(bb,aa)/2
    elseif (bb%Typ(1).eq.vecTyp) then ;rslt=momD2mom(aa,bb)/2
    endif
  elseif (aa%Typ(1).eq.braTyp) then ;rslt = braDket(aa,bb)
  elseif (aa%Typ(1).eq.ketTyp) then ;rslt = braDket(bb,aa)
  else
    rslt = aa%c(1)*bb%c(1)
!  elseif (aa%Typ.eq.scaTyp) then ;rslt = aa%c(1)*bb%c(1)
!  elseif (aa%Typ.eq.eBraTyp) then ;rslt = aa%c(1)*bb%c(1)
!  elseif (aa%Typ.eq.eKetTyp) then ;rslt = aa%c(1)*bb%c(1)
  endif
  end function


  function vecD2vec( aa,bb ) result(rslt)
  intent(in) :: aa,bb
  type(lorentz_type) :: aa,bb
  !(complex2!) :: rslt
  rslt = aa%c(vecPLS)*bb%c(vecMIN) - aa%c(vecTRA)*bb%c(vecTSR) &
       + bb%c(vecPLS)*aa%c(vecMIN) - bb%c(vecTRA)*aa%c(vecTSR)
  end function

  function momSqr( aa ) result(rslt)
  intent(in) :: aa
  type(lorentz_type) :: aa
  !(complex2!) :: rslt
  rslt = aa%c(momPLS)*aa%c(momMIN) - aa%c(momTRA)*aa%c(momTSR)
  end function

  function momD2mom( aa,bb ) result(rslt)
  intent(in) :: aa,bb
  type(lorentz_type) :: aa,bb
  !(complex2!) :: rslt
  rslt = aa%c(momPLS)*bb%c(momMIN) - aa%c(momTRA)*bb%c(momTSR) &
       + bb%c(momPLS)*aa%c(momMIN) - bb%c(momTRA)*aa%c(momTSR)
  end function

  function momD2vec( aa,bb ) result(rslt)
  intent(in) :: aa,bb
  type(lorentz_type) :: aa,bb
  !(complex2!) :: rslt
  rslt = aa%c(momPLS)*bb%c(vecMIN) - aa%c(momTRA)*bb%c(vecTSR) &
       + bb%c(vecPLS)*aa%c(momMIN) - bb%c(vecTRA)*aa%c(momTSR)
  end function

  function braDket( aa,bb ) result(rslt)
  intent(in) :: aa,bb
  type(lorentz_type) :: aa,bb
  !(complex2!) :: rslt
  if     (aa%Typ(2).eq.angTyp.or.bb%Typ(2).eq.angTyp) then
    rslt = aa%c(Bang1)*bb%c(Kang1) + aa%c(Bang2)*bb%c(Kang2)
  elseif (aa%Typ(2).eq.sqrTyp.or.bb%Typ(2).eq.sqrTyp) then
    rslt = aa%c(Bsqr1)*bb%c(Ksqr1) + aa%c(Bsqr2)*bb%c(Ksqr2)
  else
    rslt = aa%c(Bsqr1)*bb%c(Ksqr1) + aa%c(Bsqr2)*bb%c(Ksqr2) &
         + aa%c(Bang1)*bb%c(Kang1) + aa%c(Bang2)*bb%c(Kang2)
  endif
  end function

  function vecFmom_0( aa ) result(bb)
  intent(in) :: aa
  type(lorentz_type) :: aa,bb
  bb%c(1:vecSiz) = aa%c(1:vecSiz)
  bb%Typ = [vecTyp,nulTyp,nulTyp]
  end function
  function vecFmom_1( aa,zz ) result(bb)
  intent(in) :: aa,zz
  type(lorentz_type) :: aa,bb
  !(complex2!) :: zz
  bb%c(1:vecSiz) = aa%c(1:vecSiz)*zz
  bb%Typ = [vecTyp,nulTyp,nulTyp]
  end function
  function vecFmom_2( a1,z1 ,a2,z2 ) result(bb)
  intent(in) :: a1,a2,z1,z2
  type(lorentz_type) :: a1,a2,bb
  !(complex2!) :: z1,z2
  bb%c(1:vecSiz) = a1%c(1:vecSiz)*z1 + a2%c(1:vecSiz)*z2
  bb%Typ = [vecTyp,nulTyp,nulTyp]
  end function


  function ketFmom( pp ) result(uu)
!*******************************************************************
!* 
!*         / Ksqr1 \          1        / -p1+i*p2 \
!*   |p] = |       | = --------------- |          |
!*         \ Ksqr2 /    sqrt(|p0+p3|)  \   p0+p3  /
!* 
!*         / Kang1 \   |p0+p3|/(p0+p3) /  p0+p3  \
!*   |p> = |       | = --------------- |         |
!*         \ Kang2 /    sqrt(|p0+p3|)  \ p1+i*p2 /
!* 
!* 
!*         / Bsqr1 \         1         /  p0+p3  \
!*   [p| = |       | = --------------- |         |
!*         \ Bsqr2 /   sqrt(|p0+p3|)   \ p1-i*p2 /
!* 
!*         / Bang1 \   |p0+p3|/(p0+p3) / -p1-i*p2 \
!*   <p| = |       | = --------------- |          |
!*         \ Bang2 /   sqrt(|p0+p3|)   \  p0+p3   /
!* 
!*******************************************************************
  intent(in) :: pp
  type(lorentz_type) :: pp,uu
  !(complex2!) :: expiphi
  !(realknd2!) :: aPLS,aMIN
  aPLS = abs(pp%c(momPLS))
  if (aPLS.le.RZRO) then
    aMIN = abs(pp%c(momMIN))
    if (aMIN.le.RZRO) then
      uu%c(1:spisiz) = 0
    else ! consider momentum to be result of p1->0,p2=0
      aMIN = sqrt(aMIN)
      uu%c(Ksqr1) =-pp%c(momMIN)/aMIN
      uu%c(Ksqr2) = 0
      uu%c(Kang1) = 0
      uu%c(Kang2) = aMIN
    endif
  else
    expiphi = aPLS/pp%c(momPLS)
    aPLS = 1/sqrt( aPLS )
    uu%c(Ksqr1) =-pp%c(momTSR)*aPLS
    uu%c(Ksqr2) = pp%c(momPLS)*aPLS
    uu%c(Kang1) = uu%c(Ksqr2 )*expiphi
    uu%c(Kang2) = pp%c(momTRA)*expiphi*aPLS
  endif
  uu%Typ = [ketTyp,nulTyp,nulTyp]
  end function

  function braFmom( pp ) result(uu)
  intent(in) :: pp
  type(lorentz_type) :: pp,uu
  !(complex2!) :: expiphi
  !(realknd2!) :: aPLS,aMIN
  aPLS = abs(pp%c(momPLS))
  if (aPLS.le.RZRO) then
    aMIN = abs(pp%c(momMIN))
    if (aMIN.le.RZRO) then
      uu%c(1:spisiz) = 0
    else ! consider momentum to be result of p1->0,p2=0
      aMIN = sqrt(aMIN)
      uu%c(Bsqr1) = 0
      uu%c(Bsqr2) = pp%c(momMIN)/aMIN
      uu%c(Bang1) =-aMIN
      uu%c(Bang2) = 0
    endif
  else
    expiphi = aPLS/pp%c(momPLS)
    aPLS = 1/sqrt( aPLS )
    uu%c(Bsqr1) =  pp%c(momPLS)*aPLS
    uu%c(Bsqr2) =  pp%c(momTSR)*aPLS
    uu%c(Bang1) = -pp%c(momTRA)*expiphi*aPLS
    uu%c(Bang2) =  uu%c(Bsqr1 )*expiphi
  endif
  uu%Typ = [braTyp,nulTyp,nulTyp]
  end function

  function KsqrFmom( pp ) result(uu)
  intent(in) :: pp
  type(lorentz_type) :: pp,uu
  !(realknd2!) :: aPLS,aMIN
  aPLS = abs(pp%c(momPLS))
  if (aPLS.le.RZRO) then
    aMIN = abs(pp%c(momMIN))
    if (aMIN.le.RZRO) then
      uu%c(1:spisiz) = 0
    else ! consider momentum to be result of p1->0,p2=0
      aMIN = sqrt(aMIN)
      uu%c(Ksqr1) =-pp%c(momMIN)/aMIN
      uu%c(Ksqr2) = 0
      uu%c(Kang1) = 0
      uu%c(Kang2) = 0
    endif
  else
    aPLS = 1/sqrt( aPLS )
    uu%c(Ksqr1) =-pp%c(momTSR)*aPLS
    uu%c(Ksqr2) = pp%c(momPLS)*aPLS
    uu%c(Kang1) = 0
    uu%c(Kang2) = 0
  endif
  uu%Typ = [ketTyp,sqrTyp,nulTyp]
  end function

  function BsqrFmom( pp ) result(uu)
  intent(in) :: pp
  type(lorentz_type) :: pp,uu
  !(realknd2!) :: aPLS,aMIN
  aPLS = abs(pp%c(momPLS))
  if (aPLS.le.RZRO) then
    aMIN = abs(pp%c(momMIN))
    if (aMIN.le.RZRO) then
      uu%c(1:spisiz) = 0
    else ! consider momentum to be result of p1->0,p2=0
      aMIN = sqrt(aMIN)
      uu%c(Bsqr1) = 0
      uu%c(Bsqr2) = pp%c(momMIN)/aMIN
      uu%c(Bang1) = 0
      uu%c(Bang2) = 0
    endif
  else
    aPLS = 1/sqrt( aPLS )
    uu%c(Bsqr1) = pp%c(momPLS)*aPLS
    uu%c(Bsqr2) = pp%c(momTSR)*aPLS
    uu%c(Bang1) = 0
    uu%c(Bang2) = 0
  endif
  uu%Typ = [braTyp,sqrTyp,nulTyp]
  end function

  function KangFmom( pp ) result(uu)
  intent(in) :: pp
  type(lorentz_type) :: pp,uu
  !(complex2!) :: zz
  !(realknd2!) :: aPLS,aMIN
  aPLS = abs(pp%c(momPLS))
  if (aPLS.le.RZRO) then
    aMIN = abs(pp%c(momMIN))
    if (aMIN.le.RZRO) then
      uu%c(1:spisiz) = 0
    else ! consider momentum to be result of p1->0,p2=0
      aMIN = sqrt(aMIN)
      uu%c(Ksqr1) = 0
      uu%c(Ksqr2) = 0
      uu%c(Kang1) = 0
      uu%c(Kang2) = aMIN
    endif
  else
    zz = sqrt(aPLS)/pp%c(momPLS)
    uu%c(Ksqr1) = 0
    uu%c(Ksqr2) = 0
    uu%c(Kang1) = pp%c(momPLS)*zz
    uu%c(Kang2) = pp%c(momTRA)*zz
  endif
  uu%Typ = [ketTyp,angTyp,nulTyp]
  end function

  function BangFmom( pp ) result(uu)
  intent(in) :: pp
  type(lorentz_type) :: pp,uu
  !(complex2!) :: zz
  !(realknd2!) :: aPLS,aMIN
  aPLS = abs(pp%c(momPLS))
  if (aPLS.le.RZRO) then
    aMIN = abs(pp%c(momMIN))
    if (aMIN.le.RZRO) then
      uu%c(1:spisiz) = 0
    else ! consider momentum to be result of p1->0,p2=0
      aMIN = sqrt(aMIN)
      uu%c(Bsqr1) = 0
      uu%c(Bsqr2) = 0
      uu%c(Bang1) =-aMIN
      uu%c(Bang2) = 0
    endif
  else
    zz = sqrt(aPLS)/pp%c(momPLS)
    uu%c(Bsqr1) = 0
    uu%c(Bsqr2) = 0
    uu%c(Bang1) =-pp%c(momTRA)*zz
    uu%c(Bang2) = pp%c(momPLS)*zz
  endif
  uu%Typ = [braTyp,angTyp,nulTyp]
  end function

  function braFket( vv ) result(uu)
  intent(in) :: vv
  type(lorentz_type) :: vv,uu
  uu%c(Bsqr1) = vv%c(Ksqr2)
  uu%c(Bsqr2) =-vv%c(Ksqr1)
  uu%c(Bang1) =-vv%c(Kang2)
  uu%c(Bang2) = vv%c(Kang1)
  uu%Typ(1) = braTyp
  uu%Typ(2:typSiz) = vv%Typ(2:typSiz)
  end function

  function ketFbra( vv ) result(uu)
  intent(in) :: vv
  type(lorentz_type) :: vv,uu
  uu%c(Ksqr1) =-vv%c(Bsqr2)
  uu%c(Ksqr2) = vv%c(Bsqr1)
  uu%c(Kang1) = vv%c(Bang2)
  uu%c(Kang2) =-vv%c(Bang1)
  uu%Typ(1) = ketTyp
  uu%Typ(2:typSiz) = vv%Typ(2:typSiz)
  end function


  subroutine ellBasis( l1,l2,l3,l4  )
!********************************************************************
! Input l1 and l2 should be light-like with l1.l2=/=0
! Output is  l3 = <l1+|gamma^mu|l2+> / 2
!            l4 = <l2+|gamma^mu|l1+> / 2
!********************************************************************
  intent(in ) :: l1,l2
  intent(out) :: l3,l4
  type(lorentz_type) :: l1,l2,l3,l4,uu,vv
  uu = braFmom(l1)
  vv = ketFmom(l2)
  l4%c(vec11) = vv%c(Kang1) * uu%c(Bsqr1)
  l4%c(vec12) = vv%c(Kang1) * uu%c(Bsqr2)
  l4%c(vec21) = vv%c(Kang2) * uu%c(Bsqr1)
  l4%c(vec22) = vv%c(Kang2) * uu%c(Bsqr2)
  l4%Typ = [momTyp,nulTyp,nulTyp]
  uu = ketFbra(uu)
  vv = braFket(vv)
  l3%c(vec11) = uu%c(Kang1) * vv%c(Bsqr1)
  l3%c(vec12) = uu%c(Kang1) * vv%c(Bsqr2)
  l3%c(vec21) = uu%c(Kang2) * vv%c(Bsqr1)
  l3%c(vec22) = uu%c(Kang2) * vv%c(Bsqr2)
  l3%Typ = [momTyp,nulTyp,nulTyp]
  end subroutine

  
  function xScalar_0() result(ee)
!*******************************************************************
!********************************************************************
  type(lorentz_type) :: ee
  ee%c(1) = cONE
  ee%c(2:oscSiz) = cZRO
  ee%Typ = [scaTyp,nulTyp,nulTyp]
  end function

  function xScalar_r(zz) result(ee)
!*******************************************************************
!********************************************************************
  intent(in) :: zz
  !(realknd2!) :: zz
  type(lorentz_type) :: ee
  ee%c(1) = zz
  ee%c(2:oscSiz) = cZRO
  ee%Typ = [scaTyp,nulTyp,nulTyp]
  end function

  function xScalar_c(zz) result(ee)
!*******************************************************************
!********************************************************************
  intent(in) :: zz
  !(complex2!) :: zz
  type(lorentz_type) :: ee
  ee%c(1) = zz
  ee%c(2:oscSiz) = cZRO
  ee%Typ = [scaTyp,nulTyp,nulTyp]
  end function


  function xVector_a( pp ,qq ,hel ,rho ) result(ee)
!*******************************************************************
!* Fills "ee" with polarization vector having helicity "hel"
!* constructed following Kleiss-Stirling from momentum "pp" and
!* reference momentum "qq"
!********************************************************************
  intent(in) :: pp,qq,hel,rho
  type(lorentz_type) :: pp,qq,ee,uu,vv
  integer :: hel
  !(realknd2!) :: rho
  !(complex2!) :: zz,expon
  select case (hel)
  case (+1)
    uu = braFmom(pp)
    vv = KangFmom(qq)
    zz =-rSQRT2/( uu%c(Bang1)*vv%c(Kang1) + uu%c(Bang2)*vv%c(Kang2) )
    ee%c(vec11) = vv%c(Kang1) * uu%c(Bsqr1) * zz
    ee%c(vec12) = vv%c(Kang1) * uu%c(Bsqr2) * zz
    ee%c(vec21) = vv%c(Kang2) * uu%c(Bsqr1) * zz
    ee%c(vec22) = vv%c(Kang2) * uu%c(Bsqr2) * zz
    ee%Typ = [vecTyp,nulTyp,nulTyp]
  case (-1)
    uu = ketFmom(pp)
    vv = BsqrFmom(qq)
    zz =-rSQRT2/( vv%c(Bsqr1)*uu%c(Ksqr1) + vv%c(Bsqr2)*uu%c(Ksqr2) )
    ee%c(vec11) = uu%c(Kang1) * vv%c(Bsqr1) * zz
    ee%c(vec12) = uu%c(Kang1) * vv%c(Bsqr2) * zz
    ee%c(vec21) = uu%c(Kang2) * vv%c(Bsqr1) * zz
    ee%c(vec22) = uu%c(Kang2) * vv%c(Bsqr2) * zz
    ee%Typ = [vecTyp,nulTyp,nulTyp]
  case (POLARANG)
    expon = exp(cIMA*r1PI*rho)
    uu = braFmom(pp)
    vv = ketFmom(qq)
    zz =-rSQRT2/( uu%c(Bang1)*vv%c(Kang1) + uu%c(Bang2)*vv%c(Kang2) )
    zz = zz*expon
    ee%c(vec11) = vv%c(Kang1) * uu%c(Bsqr1) * zz
    ee%c(vec12) = vv%c(Kang1) * uu%c(Bsqr2) * zz
    ee%c(vec21) = vv%c(Kang2) * uu%c(Bsqr1) * zz
    ee%c(vec22) = vv%c(Kang2) * uu%c(Bsqr2) * zz
    zz = rSQRT2/( uu%c(Bsqr1)*vv%c(Ksqr1) + uu%c(Bsqr2)*vv%c(Ksqr2) )
    zz = zz/expon
    ee%c(vec11) = ee%c(vec11) + vv%c(Ksqr2) * uu%c(Bang2) * zz
    ee%c(vec12) = ee%c(vec12) - vv%c(Ksqr1) * uu%c(Bang2) * zz
    ee%c(vec21) = ee%c(vec21) - vv%c(Ksqr2) * uu%c(Bang1) * zz
    ee%c(vec22) = ee%c(vec22) + vv%c(Ksqr1) * uu%c(Bang1) * zz
    ee%Typ = [vecTyp,nulTyp,nulTyp]
  case (WARDTEST)
    zz = 2/( pp%c(momPLS) + pp%c(momMIN) )
    ee = vecFmom(pp,zz)
  case (XDEFINED)
    ee = vecFmom(qq)
  end select
  end function

  function xVector_b( pp,qq,q2,hel ) result(ee)
!*******************************************************************
!* For nonzero q2=qq.qq
!********************************************************************
  intent(in) :: pp,qq,q2,hel
  type(lorentz_type) :: pp,qq,ee,uu,vv
  integer :: hel
  !(complex2!) :: zz,q2
  zz = pp%c(momPLS)*qq%c(momMIN) - pp%c(momTRA)*qq%c(momTSR) &
     + qq%c(momPLS)*pp%c(momMIN) - qq%c(momTRA)*pp%c(momTSR)
  zz = q2/zz
  vv%c(1:momSiz) = qq%c(1:momSiz) - pp%c(1:momSiz)*zz
  vv%Typ = [momTyp,nulTyp,nulTyp]
  select case (hel)
  case (+1)
    uu = braFmom(pp)
    vv = KangFmom(vv)
    zz =-rSQRT2/( uu%c(Bang1)*vv%c(Kang1) + uu%c(Bang2)*vv%c(Kang2) )
    ee%c(vec11) = vv%c(Kang1) * uu%c(Bsqr1) * zz
    ee%c(vec12) = vv%c(Kang1) * uu%c(Bsqr2) * zz
    ee%c(vec21) = vv%c(Kang2) * uu%c(Bsqr1) * zz
    ee%c(vec22) = vv%c(Kang2) * uu%c(Bsqr2) * zz
    ee%Typ = [vecTyp,nulTyp,nulTyp]
  case (-1)
    uu = ketFmom(pp)
    vv = BsqrFmom(vv)
    zz =-rSQRT2/( vv%c(Bsqr1)*uu%c(Ksqr1) + vv%c(Bsqr2)*uu%c(Ksqr2) )
    ee%c(vec11) = uu%c(Kang1) * vv%c(Bsqr1) * zz
    ee%c(vec12) = uu%c(Kang1) * vv%c(Bsqr2) * zz
    ee%c(vec21) = uu%c(Kang2) * vv%c(Bsqr1) * zz
    ee%c(vec22) = uu%c(Kang2) * vv%c(Bsqr2) * zz
    ee%Typ = [vecTyp,nulTyp,nulTyp]
  case (WARDTEST)
    zz = 2/( pp%c(momPLS) + pp%c(momMIN) )
    ee = vecFmom(pp,zz)
  case (XDEFINED)
    ee = vecFmom(qq)
  end select
  end function

  function xVector_c( pp ,mm ,qq ,hel ,rho ) result(ee)
!*******************************************************************
! Massive external vector
!********************************************************************
  intent(in) :: pp,qq,mm,hel,rho
  type(lorentz_type) :: pp,qq,ee
  !(realknd2!) :: mm,rho
  !(complex2!) :: hh,pT,pZ,pA,expon
  integer :: hel
  if (mm.eq.rZRO) then
    ee = xVector_a( pp ,qq ,hel ,rho )
    return
  endif
  ee%Typ = [vecTyp,nulTyp,nulTyp]
  pT = pp%c(momTRA)*pp%c(momTSR)
  pZ = (pp%c(momPLS)-pp%c(momMIN))/2
  pA = sqrt(pT+pZ*pZ)
  pT = sqrt(pT)
  select case (hel)
  case (-1) ;ee%c = eMin()
  case ( 1) ;ee%c = ePls()
  case ( 0) ;ee%c = eZero()
  case (POLARANG)
    expon = exp(cIMA*r1PI*rho)
    ee%c = eMin()/expon + expon*ePls() + (expon*expon*expon)*eZero()
  case (WARDTEST)
    hh = 2/( pp%c(momPLS) + pp%c(momMIN) )
    ee = vecFmom(pp,hh)
  case (XDEFINED)
    ee = vecFmom(qq)
  end select
  contains
    function ePls() result(rslt)
    !(complex2!) :: rslt(vecSiz),hh
    hh = 1/(pA*pT*rSQRT2)
    rslt(momPLS) =-pT/(pA*rSQRT2)
    rslt(momMIN) =-rslt(momPLS)
    rslt(momTRA) = pp%c(momTRA)*( pA+pZ)*hh
    rslt(momTSR) = pp%c(momTSR)*(-pA+pZ)*hh
    end function
    function eMin() result(rslt)
    !(complex2!) :: rslt(vecSiz),hh
    hh = 1/(pA*pT*rSQRT2)
    rslt(momPLS) = pT/(pA*rSQRT2)
    rslt(momMIN) =-rslt(momPLS)
    rslt(momTRA) = pp%c(momTRA)*( pA-pZ)*hh
    rslt(momTSR) = pp%c(momTSR)*(-pA-pZ)*hh
    end function
    function eZero() result(rslt)
    !(complex2!) :: rslt(vecSiz),hh,qA,qZ
    hh = (pp%c(momPLS)+pp%c(momMIN))/(2*pA*mm)
    qA = pA/mm
    qZ = pZ*hh
    rslt(momPLS) = qA+qZ
    rslt(momMIN) = qA-qZ
    rslt(momTRA) = pp%c(momTRA)*hh
    rslt(momTSR) = pp%c(momTSR)*hh
    end function
  end function


  function xSpinor_a( pp ,qq ,hel ,negIsKet ,rho ) result(uu)
!**************************************************************
! Massless external spinor
! returns a ket if negIsKet<=-1, else it returns a bra.
!      particle:  bra with p0>0  or  ket with p0<0
! anti-particle:  bra with p0<0  or  ket with p0>0
!**************************************************************
  intent(in) :: pp,hel,qq,negIsKet,rho
  type(lorentz_type) :: pp,qq,uu
  integer :: hel
  integer :: negIsKet
  !(realknd2!) :: rho
  !(complex2!) :: expon
  if     (hel.eq.-1) then
    uu = BangFmom(pp)
    if (negIsKet.le.-1) uu = ketFbra(uu)
  elseif (hel.eq. 1) then
    uu = BsqrFmom(pp)
    if (negIsKet.le.-1) uu = ketFbra(uu)
  elseif (hel.eq.POLARANG) then
    expon = exp(cIMA*r1PI*rho)
    uu = braFmom(pp)
    uu%c(Bang1) = uu%c(Bang1)*expon
    uu%c(Bang2) = uu%c(Bang2)*expon
    uu%c(Bsqr1) = uu%c(Bsqr1)/expon
    uu%c(Bsqr2) = uu%c(Bsqr2)/expon
    if (negIsKet.le.-1) uu = ketFbra(uu)
  elseif (hel.eq.XDEFINED) then
    uu%c(Ksqr1) = qq%c(Ksqr1)
    uu%c(Ksqr2) = qq%c(Ksqr2)
    uu%c(Kang1) = qq%c(Kang1)
    uu%c(Kang2) = qq%c(Kang2)
    uu%Typ = [ketTyp,nulTyp,nulTyp]
    if (negIsKet.ge.1) uu = braFket(uu)
  elseif (hel.eq.-USEAUXMOM) then
    uu = BangFmom(qq)
    if (negIsKet.le.-1) uu = ketFbra(uu)
  elseif (hel.eq. USEAUXMOM) then
    uu = BsqrFmom(qq)
    if (negIsKet.le.-1) uu = ketFbra(uu)
  else
    uu%c(1:spiSiz) = 0
    uu%Typ = [nulTyp,nulTyp,nulTyp]
  endif
  end function

  function xSpinor_b( pp ,mm,qq ,hel ,negIsKet ,rho ) result(uu)
!**************************************************************
! Massive external spinor
! Auxiliary momentum qq should be massless with pp.qq=/=0
!**************************************************************
  intent(in) :: pp,qq,mm,hel,negIsKet,rho
  type(lorentz_type) :: pp,qq,uu,pv,pw,vv,ww
  !(realknd2!) :: mm,rho
  !(complex2!) :: zz,expon
  integer :: hel,negIsKet
  if (mm.eq.rZRO) then
    uu = xSpinor_a( pp ,qq ,hel ,negIsKet ,rho )
    return
  endif
  zz = mm*mm/momD2mom(pp,qq)
  pw%Typ=[momTyp,nulTyp,nulTyp] ;pw%c(1:momSiz) = qq%c(1:momSiz)*zz
  pv%Typ=[momTyp,nulTyp,nulTyp] ;pv%c(1:momSiz) = pp%c(1:momSiz)-pw%c(1:momSiz)
  if (hel.eq.POLARANG) then
    expon = exp(cIMA*r1PI*rho)
    vv = braFmom(pv) 
    ww = braFmom(pw)
    zz = ( vv%c(Bang1)*ww%c(Bang2) - ww%c(Bang1)*vv%c(Bang2) )/mm
    uu%c(Bang1) = vv%c(Bang1)*expon
    uu%c(Bang2) = vv%c(Bang2)*expon
    uu%c(Bsqr1) = ww%c(Bsqr1)*zz*expon
    uu%c(Bsqr2) = ww%c(Bsqr2)*zz*expon
    zz = ( ww%c(Bsqr1)*vv%c(Bsqr2) - vv%c(Bsqr1)*ww%c(Bsqr2) )/mm
    uu%c(Bang1) = uu%c(Bang1) + ww%c(Bang1)*zz/expon
    uu%c(Bang2) = uu%c(Bang2) + ww%c(Bang2)*zz/expon
    uu%c(Bsqr1) = uu%c(Bsqr1) + vv%c(Bsqr1)/expon
    uu%c(Bsqr2) = uu%c(Bsqr2) + vv%c(Bsqr2)/expon
    uu%Typ = [braTyp,nulTyp,nulTyp]
    if (negIsKet.le.-1) uu = ketFbra(uu)
  elseif (hel.eq.XDEFINED) then
    uu%c(Ksqr1) = qq%c(Ksqr1)
    uu%c(Ksqr2) = qq%c(Ksqr2)
    uu%c(Kang1) = qq%c(Kang1)
    uu%c(Kang2) = qq%c(Kang2)
    uu%Typ = [ketTyp,nulTyp,nulTyp]
    if (negIsKet.ge.1) uu = braFket(uu)
  !elseif (hel*negIsKet.lt.0) then
  elseif (hel.lt.0) then
    vv = BangFmom(pv) 
    ww = braFmom(pw)
    zz = ( vv%c(Bang1)*ww%c(Bang2) - ww%c(Bang1)*vv%c(Bang2) )/mm ! <v|w> / m
    uu%c(Bang1) = vv%c(Bang1)
    uu%c(Bang2) = vv%c(Bang2)
    uu%c(Bsqr1) = zz*ww%c(Bsqr1)
    uu%c(Bsqr2) = zz*ww%c(Bsqr2)
    uu%Typ = [braTyp,nulTyp,nulTyp]
    if (negIsKet.le.-1) uu = ketFbra(uu)
  !elseif (hel*negIsKet.gt.0) then
  elseif (hel.gt.0) then
    vv = BsqrFmom(pv) 
    ww = braFmom(pw)
    zz = ( ww%c(Bsqr1)*vv%c(Bsqr2) - vv%c(Bsqr1)*ww%c(Bsqr2) )/mm ! [v|w] / m
    uu%c(Bsqr1) = vv%c(Bsqr1)
    uu%c(Bsqr2) = vv%c(Bsqr2)
    uu%c(Bang1) = zz*ww%c(Bang1)
    uu%c(Bang2) = zz*ww%c(Bang2)
    uu%Typ = [braTyp,nulTyp,nulTyp]
    if (negIsKet.le.-1) uu = ketFbra(uu)
  else
    uu%c(1:spiSiz) = 0
    uu%Typ = 0
  endif
  end function


!**************************************************************
  include 'ffvVertex.h90'
  include 'ffsVertex.h90'
!**************************************************************


  function gggVertex( p0 ,p1,e1 ,p2,e2 ,zz ) result(e0)
!**************************************************************
! Lorentz-part of the triple-gluon vertex
!**************************************************************
  intent(in) :: p0,p1,p2,e1,e2,zz
  type(lorentz_type) :: p0,p1,p2,e1,e2,e0
  !(complex2!) :: zz,p20(momSiz),p01(momSiz),e1e2,e1p20,e2p01,yy
  p20(momPLS)=p2%c(momPLS)+p0%c(momPLS); p01(momPLS)=p0%c(momPLS)+p1%c(momPLS)
  p20(momMIN)=p2%c(momMIN)+p0%c(momMIN); p01(momMIN)=p0%c(momMIN)+p1%c(momMIN)
  p20(momTRA)=p2%c(momTRA)+p0%c(momTRA); p01(momTRA)=p0%c(momTRA)+p1%c(momTRA)
  p20(momTSR)=p2%c(momTSR)+p0%c(momTSR); p01(momTSR)=p0%c(momTSR)+p1%c(momTSR)
  e1e2  = e1%c(vecPLS)*e2%c(vecMIN) - e1%c(vecTRA)*e2%c(vecTSR) &
        + e2%c(vecPLS)*e1%c(vecMIN) - e2%c(vecTRA)*e1%c(vecTSR) !/2
  e1p20 = e1%c(vecPLS)* p20(vecMIN) - e1%c(vecTRA)* p20(vecTSR) &
        +  p20(vecPLS)*e1%c(vecMIN) -  p20(vecTRA)*e1%c(vecTSR) !/2
  e2p01 = e2%c(vecPLS)* p01(vecMIN) - e2%c(vecTRA)* p01(vecTSR) &
        +  p01(vecPLS)*e2%c(vecMIN) -  p01(vecTRA)*e2%c(vecTSR) !/2
  e0%c(vecPLS) = (p1%c(vecPLS)-p2%c(vecPLS))*e1e2 + e2%c(vecPLS)*e1p20 - e1%c(vecPLS)*e2p01
  e0%c(vecMIN) = (p1%c(vecMIN)-p2%c(vecMIN))*e1e2 + e2%c(vecMIN)*e1p20 - e1%c(vecMIN)*e2p01
  e0%c(vecTRA) = (p1%c(vecTRA)-p2%c(vecTRA))*e1e2 + e2%c(vecTRA)*e1p20 - e1%c(vecTRA)*e2p01
  e0%c(vecTSR) = (p1%c(vecTSR)-p2%c(vecTSR))*e1e2 + e2%c(vecTSR)*e1p20 - e1%c(vecTSR)*e2p01
  yy = zz/2 ! here /2 for dot products
  e0%c(vecPLS) = e0%c(vecPLS)*yy
  e0%c(vecMIN) = e0%c(vecMIN)*yy
  e0%c(vecTRA) = e0%c(vecTRA)*yy
  e0%c(vecTSR) = e0%c(vecTSR)*yy
  e0%Typ = [vecTyp,nulTyp,nulTyp]
  end function

  function ggggVertex( e1,e2,e3 ,z1,z2,z3 ) result(e0)
!**************************************************************
! e0(nu) = e3(nu)*e1.e2*z1 + e2(nu)*e3.e1*z2 + e1(nu)*e2.e3*z3
!**************************************************************
  intent(in) :: e1,e2,e3,z1,z2,z3
  type(lorentz_type) :: e1,e2,e3,e0
  !(complex2!) :: z1,z2,z3,zz
  e0%c(vecPLS) = 0
  e0%c(vecMIN) = 0
  e0%c(vecTRA) = 0
  e0%c(vecTSR) = 0
  if (z3.ne.cZRO) then
    zz = e2%c(vecPLS)*e3%c(vecMIN) - e2%c(vecTRA)*e3%c(vecTSR) &
       + e3%c(vecPLS)*e2%c(vecMIN) - e3%c(vecTRA)*e2%c(vecTSR) !/2
    zz = zz*z3/2
    e0%c(vecPLS) = e0%c(vecPLS) + e1%c(vecPLS)*zz
    e0%c(vecMIN) = e0%c(vecMIN) + e1%c(vecMIN)*zz
    e0%c(vecTRA) = e0%c(vecTRA) + e1%c(vecTRA)*zz
    e0%c(vecTSR) = e0%c(vecTSR) + e1%c(vecTSR)*zz
  endif
  if (z2.ne.cZRO) then
    zz = e3%c(vecPLS)*e1%c(vecMIN) - e3%c(vecTRA)*e1%c(vecTSR) &
       + e1%c(vecPLS)*e3%c(vecMIN) - e1%c(vecTRA)*e3%c(vecTSR) !/2
    zz = zz*z2/2
    e0%c(vecPLS) = e0%c(vecPLS) + e2%c(vecPLS)*zz
    e0%c(vecMIN) = e0%c(vecMIN) + e2%c(vecMIN)*zz
    e0%c(vecTRA) = e0%c(vecTRA) + e2%c(vecTRA)*zz
    e0%c(vecTSR) = e0%c(vecTSR) + e2%c(vecTSR)*zz
  endif
  if (z1.ne.cZRO) then
    zz = e1%c(vecPLS)*e2%c(vecMIN) - e1%c(vecTRA)*e2%c(vecTSR) &
       + e2%c(vecPLS)*e1%c(vecMIN) - e2%c(vecTRA)*e1%c(vecTSR) !/2
    zz = zz*z1/2
    e0%c(vecPLS) = e0%c(vecPLS) + e3%c(vecPLS)*zz
    e0%c(vecMIN) = e0%c(vecMIN) + e3%c(vecMIN)*zz
    e0%c(vecTRA) = e0%c(vecTRA) + e3%c(vecTRA)*zz
    e0%c(vecTSR) = e0%c(vecTSR) + e3%c(vecTSR)*zz
  endif
  e0%Typ = [vecTyp,nulTyp,nulTyp]
  end function

  function vvvvVertex( e1,e2,e3 ,zz ) result(e0)
!**************************************************************
! e0(nu) = zz*( 2*e3(nu)*e1.e2 - e2(nu)*e3.e1 - e1(nu)*e2.e3 )
!**************************************************************
  intent(in) :: e1,e2,e3,zz
  type(lorentz_type) :: e1,e2,e3,e0
  !(complex2!) :: yy,zz
  e0%c(vecPLS) = 0
  e0%c(vecMIN) = 0
  e0%c(vecTRA) = 0
  e0%c(vecTSR) = 0
  yy = e2%c(vecPLS)*e3%c(vecMIN) - e2%c(vecTRA)*e3%c(vecTSR) &
     + e3%c(vecPLS)*e2%c(vecMIN) - e3%c(vecTRA)*e2%c(vecTSR) !/2
  yy =-yy*zz/2
  e0%c(vecPLS) = e0%c(vecPLS) + e1%c(vecPLS)*yy
  e0%c(vecMIN) = e0%c(vecMIN) + e1%c(vecMIN)*yy
  e0%c(vecTRA) = e0%c(vecTRA) + e1%c(vecTRA)*yy
  e0%c(vecTSR) = e0%c(vecTSR) + e1%c(vecTSR)*yy
  yy = e3%c(vecPLS)*e1%c(vecMIN) - e3%c(vecTRA)*e1%c(vecTSR) &
     + e1%c(vecPLS)*e3%c(vecMIN) - e1%c(vecTRA)*e3%c(vecTSR) !/2
  yy =-yy*zz/2
  e0%c(vecPLS) = e0%c(vecPLS) + e2%c(vecPLS)*yy
  e0%c(vecMIN) = e0%c(vecMIN) + e2%c(vecMIN)*yy
  e0%c(vecTRA) = e0%c(vecTRA) + e2%c(vecTRA)*yy
  e0%c(vecTSR) = e0%c(vecTSR) + e2%c(vecTSR)*yy
  yy = e1%c(vecPLS)*e2%c(vecMIN) - e1%c(vecTRA)*e2%c(vecTSR) &
     + e2%c(vecPLS)*e1%c(vecMIN) - e2%c(vecTRA)*e1%c(vecTSR) !/2
  yy = yy*zz
  e0%c(vecPLS) = e0%c(vecPLS) + e3%c(vecPLS)*yy
  e0%c(vecMIN) = e0%c(vecMIN) + e3%c(vecMIN)*yy
  e0%c(vecTRA) = e0%c(vecTRA) + e3%c(vecTRA)*yy
  e0%c(vecTSR) = e0%c(vecTSR) + e3%c(vecTSR)*yy
  e0%Typ = [vecTyp,nulTyp,nulTyp]
  end function


  function vvsVertex( e1,e2 ,zz ) result(e0)
!**************************************************************
!**************************************************************
  intent(in) :: e1,e2,zz
  type(lorentz_type) :: e1,e2,e0
  !(complex2!) :: yy,zz
  if (e1%Typ(1).eq.vecTyp) then
    if (e2%Typ(1).eq.vecTyp) then
      yy = e1%c(vecPLS)*e2%c(vecMIN) - e1%c(vecTRA)*e2%c(vecTSR) &
         + e2%c(vecPLS)*e1%c(vecMIN) - e2%c(vecTRA)*e1%c(vecTSR) !/2
      e0%c(1) = zz*yy/2
      e0%Typ = [scaTyp,nulTyp,nulTyp]
    else
      e0%c(1:vecSiz) = (zz*e2%c(1))*e1%c(1:vecSiz)
      e0%Typ = [vecTyp,nulTyp,nulTyp]
    endif
  else
    e0%c(1:vecSiz) = (zz*e1%c(1))*e2%c(1:vecSiz)
    e0%Typ = [vecTyp,nulTyp,nulTyp]
  endif
  end function

  function vvssVertex( e1,e2,e3 ,zz ) result(e0)
!**************************************************************
!**************************************************************
  intent(in) :: e1,e2,e3,zz
  type(lorentz_type) :: e1,e2,e3,e0
  !(complex2!) :: yy,zz
  if (e1%Typ(1).eq.vecTyp) then
    if (e2%Typ(1).eq.vecTyp) then
      yy = e1%c(vecPLS)*e2%c(vecMIN) - e1%c(vecTRA)*e2%c(vecTSR) &
         + e2%c(vecPLS)*e1%c(vecMIN) - e2%c(vecTRA)*e1%c(vecTSR) !/2
      e0%c(1) = e3%c(1)*zz*yy/2
      e0%Typ = [scaTyp,nulTyp,nulTyp]
    elseif (e3%Typ(1).eq.vecTyp) then
      yy = e1%c(vecPLS)*e3%c(vecMIN) - e1%c(vecTRA)*e3%c(vecTSR) &
         + e3%c(vecPLS)*e1%c(vecMIN) - e3%c(vecTRA)*e1%c(vecTSR) !/2
      e0%c(1) = e2%c(1)*zz*yy/2
      e0%Typ = [scaTyp,nulTyp,nulTyp]
    else
      e0%c(1:vecSiz) = (zz*e2%c(1)*e3%c(1))*e1%c(1:vecSiz)
      e0%Typ = [vecTyp,nulTyp,nulTyp]
    endif
  elseif (e2%Typ(1).eq.vecTyp) then
    if (e3%Typ(1).eq.vecTyp) then
      yy = e2%c(vecPLS)*e3%c(vecMIN) - e2%c(vecTRA)*e3%c(vecTSR) &
         + e3%c(vecPLS)*e2%c(vecMIN) - e3%c(vecTRA)*e2%c(vecTSR) !/2
      e0%c(1) = e1%c(1)*zz*yy/2
      e0%Typ = [scaTyp,nulTyp,nulTyp]
    else
      e0%c(1:vecSiz) = (zz*e1%c(1)*e3%c(1))*e2%c(1:vecSiz)
      e0%Typ = [vecTyp,nulTyp,nulTyp]
    endif
  else
    e0%c(1:vecSiz) = (zz*e1%c(1)*e2%c(1))*e3%c(1:vecSiz)
    e0%Typ = [vecTyp,nulTyp,nulTyp]
  endif
  end function

  function sssVertex( e1,e2 ,zz ) result(e0)
!**************************************************************
!**************************************************************
  intent(in) :: e1,e2,zz
  type(lorentz_type) :: e1,e2,e0
  !(complex2!) :: zz
  e0%c(1) = zz * e1%c(1) * e2%c(1)
  e0%Typ = [scaTyp,nulTyp,nulTyp]
  end function

  function ssssVertex( e1,e2,e3 ,zz ) result(e0)
!**************************************************************
!**************************************************************
  intent(in) :: e1,e2,e3,zz
  type(lorentz_type) :: e1,e2,e3,e0
  !(complex2!) :: zz
  e0%c(1) = zz * e1%c(1) * e2%c(1) * e3%c(1)
  e0%Typ = [scaTyp,nulTyp,nulTyp]
  end function

  function s2gVertex( p0 ,p1,e1 ,p2,e2 ,zz ) result(e0)
!**************************************************************
!**************************************************************
  intent(in) :: p0,p1,e1,p2,e2,zz
  type(lorentz_type) :: p1,e1,p2,e2,e0,p0
  !(complex2!) :: uu,vv,yy,zz
  if     (e1%Typ(1).eq.scaTyp) then
    uu = p0%c(momPLS)*p2%c(momMIN) - p0%c(momTRA)*p2%c(momTSR) &
       + p2%c(momPLS)*p0%c(momMIN) - p2%c(momTRA)*p0%c(momTSR) !/2
    vv = p0%c(momPLS)*e2%c(vecMIN) - p0%c(momTRA)*e2%c(vecTSR) &
       + e2%c(vecPLS)*p0%c(momMIN) - e2%c(vecTRA)*p0%c(momTSR) !/2
    yy = zz*e1%c(1)/2
    e0 = vecFmom(p2,vv*yy)
    e0%c(1:vecSiz) = e0%c(1:vecSiz) - (uu*yy)*e2%c(1:vecSiz)
  elseif (e2%Typ(1).eq.scaTyp) then
    uu = p0%c(momPLS)*p1%c(momMIN) - p0%c(momTRA)*p1%c(momTSR) &
       + p1%c(momPLS)*p0%c(momMIN) - p1%c(momTRA)*p0%c(momTSR) !/2
    vv = p0%c(momPLS)*e1%c(vecMIN) - p0%c(momTRA)*e1%c(vecTSR) &
       + e1%c(vecPLS)*p0%c(momMIN) - e1%c(vecTRA)*p0%c(momTSR) !/2
    yy = zz*e2%c(1)/2
    e0 = vecFmom(p1,vv*yy)
    e0%c(1:vecSiz) = e0%c(1:vecSiz) - (uu*yy)*e1%c(1:vecSiz)
  else
    e0%Typ = [scaTyp,nulTyp,nulTyp]
    uu = p1%c(momPLS)*p2%c(momMIN) - p1%c(momTRA)*p2%c(momTSR) &
       + p2%c(momPLS)*p1%c(momMIN) - p2%c(momTRA)*p1%c(momTSR) !/2
    vv = e1%c(vecPLS)*e2%c(vecMIN) - e1%c(vecTRA)*e2%c(vecTSR) &
       + e2%c(vecPLS)*e1%c(vecMIN) - e2%c(vecTRA)*e1%c(vecTSR) !/2
    e0%c(1) = uu*vv
    uu = e1%c(vecPLS)*p2%c(momMIN) - e1%c(vecTRA)*p2%c(momTSR) &
       + p2%c(momPLS)*e1%c(vecMIN) - p2%c(momTRA)*e1%c(vecTSR) !/2
    vv = p1%c(momPLS)*e2%c(vecMIN) - p1%c(momTRA)*e2%c(vecTSR) &
       + e2%c(vecPLS)*p1%c(momMIN) - e2%c(vecTRA)*p1%c(momTSR) !/2
    e0%c(1) = (e0%c(1) - uu*vv)*zz/4
  endif
  end function

  function s3gVertex( p0 ,p1,e1 ,p2,e2 ,p3,e3 ,zz ) result(e0)
!**************************************************************
!**************************************************************
  intent(in) :: p0,p1,e1,p2,e2,p3,e3,zz
  type(lorentz_type) :: p0,p1,e1,p2,e2,p3,e3,e0,pp
  !(complex2!) :: xx,yy,zz
  if     (e1%Typ(1).eq.scaTyp) then
    e0 = gggVertex( p0 ,p2,e2 ,p3,e3 ,zz*e1%c(1) )
  elseif (e2%Typ(1).eq.scaTyp) then
    e0 = gggVertex( p0 ,p1,e1 ,p3,e3 ,zz*e2%c(1) )
  elseif (e3%Typ(1).eq.scaTyp) then
    e0 = gggVertex( p0 ,p1,e1 ,p2,e2 ,zz*e3%c(1) )
  else
    e0%Typ = [scaTyp,nulTyp,nulTyp]
    pp%c(1:momSiz) = p1%c(1:momSiz)-p2%c(1:momSiz)
      xx = pp%c(momPLS)*e3%c(vecMIN) - pp%c(momTRA)*e3%c(vecTSR) &
         + e3%c(vecPLS)*pp%c(momMIN) - e3%c(vecTRA)*pp%c(momTSR) !/2
      yy = e1%c(vecPLS)*e2%c(vecMIN) - e1%c(vecTRA)*e2%c(vecTSR) &
         + e2%c(vecPLS)*e1%c(vecMIN) - e2%c(vecTRA)*e1%c(vecTSR) !/2
      e0%c(1) = xx*yy
    pp%c(1:momSiz) = p2%c(1:momSiz)-p3%c(1:momSiz)
      xx = pp%c(momPLS)*e1%c(vecMIN) - pp%c(momTRA)*e1%c(vecTSR) &
         + e1%c(vecPLS)*pp%c(momMIN) - e1%c(vecTRA)*pp%c(momTSR) !/2
      yy = e2%c(vecPLS)*e3%c(vecMIN) - e2%c(vecTRA)*e3%c(vecTSR) &
         + e3%c(vecPLS)*e2%c(vecMIN) - e3%c(vecTRA)*e2%c(vecTSR) !/2
      e0%c(1) = e0%c(1) + xx*yy
    pp%c(1:momSiz) = p3%c(1:momSiz)-p1%c(1:momSiz)
      xx = pp%c(momPLS)*e2%c(vecMIN) - pp%c(momTRA)*e2%c(vecTSR) &
         + e2%c(vecPLS)*pp%c(momMIN) - e2%c(vecTRA)*pp%c(momTSR) !/2
      yy = e3%c(vecPLS)*e1%c(vecMIN) - e3%c(vecTRA)*e1%c(vecTSR) &
         + e1%c(vecPLS)*e3%c(vecMIN) - e1%c(vecTRA)*e3%c(vecTSR) !/2
      e0%c(1) = e0%c(1) + xx*yy
    e0%c(1) = e0%c(1)*zz/4
  endif
  end function

  function s4gVertex( e1,e2,e3,e4 ,z1,z2,z3 ) result(e0)
!**************************************************************
!**************************************************************
  intent(in) :: e1,e2,e3,e4,z1,z2,z3
  type(lorentz_type) :: e1,e2,e3,e4,e0
  !(complex2!) :: xx,yy,z1,z2,z3
  if     (e1%Typ(1).eq.scaTyp) then
    !write(*,*) 'A' !DEBUG
    e0 = ggggVertex( e2,e3,e4 ,z1*e1%c(1) ,z2*e1%c(1) ,z3*e1%c(1) )
  elseif (e2%Typ(1).eq.scaTyp) then
    !write(*,*) 'B' !DEBUG
    e0 = ggggVertex( e1,e3,e4 ,z1*e2%c(1) ,z2*e2%c(1) ,z3*e2%c(1) )
  elseif (e3%Typ(1).eq.scaTyp) then
    !write(*,*) 'C' !DEBUG
    e0 = ggggVertex( e1,e2,e4 ,z1*e3%c(1) ,z2*e3%c(1) ,z3*e3%c(1) )
  elseif (e4%Typ(1).eq.scaTyp) then
    !write(*,*) 'D' !DEBUG
    e0 = ggggVertex( e1,e2,e3 ,z1*e4%c(1) ,z2*e4%c(1) ,z3*e4%c(1) )
  else
    e0%Typ = [scaTyp,nulTyp,nulTyp]
    e0%c(1) = cZRO
    if (z1.ne.cZRO) then
      xx = e1%c(vecPLS)*e2%c(vecMIN) - e1%c(vecTRA)*e2%c(vecTSR) &
         + e2%c(vecPLS)*e1%c(vecMIN) - e2%c(vecTRA)*e1%c(vecTSR) !/2
      yy = e3%c(vecPLS)*e4%c(vecMIN) - e3%c(vecTRA)*e4%c(vecTSR) &
         + e4%c(vecPLS)*e3%c(vecMIN) - e4%c(vecTRA)*e3%c(vecTSR) !/2
      e0%c(1) = e0%c(1) + xx*yy*z3
    endif
    if (z2.ne.cZRO) then
      xx = e1%c(vecPLS)*e3%c(vecMIN) - e1%c(vecTRA)*e3%c(vecTSR) &
         + e3%c(vecPLS)*e1%c(vecMIN) - e3%c(vecTRA)*e1%c(vecTSR) !/2
      yy = e2%c(vecPLS)*e4%c(vecMIN) - e2%c(vecTRA)*e4%c(vecTSR) &
         + e4%c(vecPLS)*e2%c(vecMIN) - e4%c(vecTRA)*e2%c(vecTSR) !/2
      e0%c(1) = e0%c(1) + xx*yy*z2
    endif
    if (z3.ne.cZRO) then
      xx = e2%c(vecPLS)*e3%c(vecMIN) - e2%c(vecTRA)*e3%c(vecTSR) &
         + e3%c(vecPLS)*e2%c(vecMIN) - e3%c(vecTRA)*e2%c(vecTSR) !/2
      yy = e1%c(vecPLS)*e4%c(vecMIN) - e1%c(vecTRA)*e4%c(vecTSR) &
         + e4%c(vecPLS)*e1%c(vecMIN) - e4%c(vecTRA)*e1%c(vecTSR) !/2
      e0%c(1) = e0%c(1) + xx*yy*z1
    endif
    e0%c(1) = e0%c(1)/4
  endif
  end function


!**************************************************************
  include 'fPropagator.h90'
!**************************************************************

  subroutine ePropagator( uu ,pp ,nn )
!**************************************************************
! Eikonal quark propagator  slash(n)/(2p.n) 
!**************************************************************
  type(lorentz_type) :: uu
  type(lorentz_type),intent(in) :: pp,nn
  type(lorentz_type) :: vv
  !(complex2!) :: zz
  zz = pp%c(momPLS)*nn%c(momMIN) - pp%c(momTRA)*nn%c(momTSR) &
     + nn%c(momPLS)*pp%c(momMIN) - nn%c(momTRA)*pp%c(momTSR) !/2
  zz = cIMA/zz
  select case(uu%Typ(1))
  case (braTyp)
    select case (uu%Typ(2))
    case (sqrTyp)
      uu%Typ(2) = angTyp
      uu%c(Bang1) = ( uu%c(Bsqr1)*nn%c(mom22) - uu%c(Bsqr2)*nn%c(mom21) )*zz
      uu%c(Bang2) = (-uu%c(Bsqr1)*nn%c(mom12) + uu%c(Bsqr2)*nn%c(mom11) )*zz
      uu%c(Bsqr1) = 0
      uu%c(Bsqr2) = 0
    case (angTyp)
      uu%Typ(2) = sqrTyp
      uu%c(Bsqr1) = ( uu%c(Bang1)*nn%c(mom11) + uu%c(Bang2)*nn%c(mom21) )*zz
      uu%c(Bsqr2) = ( uu%c(Bang1)*nn%c(mom12) + uu%c(Bang2)*nn%c(mom22) )*zz
      uu%c(Bang1) = 0
      uu%c(Bang2) = 0
    case (nulTyp)
      uu%Typ(2) = nulTyp
      vv%c(Bsqr1) = uu%c(Bang1)*nn%c(mom11) + uu%c(Bang2)*nn%c(mom21)
      vv%c(Bsqr2) = uu%c(Bang1)*nn%c(mom12) + uu%c(Bang2)*nn%c(mom22)
      vv%c(Bang1) = uu%c(Bsqr1)*nn%c(mom22) - uu%c(Bsqr2)*nn%c(mom21)
      vv%c(Bang2) =-uu%c(Bsqr1)*nn%c(mom12) + uu%c(Bsqr2)*nn%c(mom11)
      uu%c(1:spiSiz) = vv%c(1:spiSiz)*zz
    end select
  case (ketTyp)
    select case (uu%Typ(2))
    case (angTyp)
      uu%Typ(2) = sqrTyp
      uu%c(Ksqr1) =-( nn%c(mom22)*uu%c(Kang1) - nn%c(mom12)*uu%c(Kang2) )*zz
      uu%c(Ksqr2) =-(-nn%c(mom21)*uu%c(Kang1) + nn%c(mom11)*uu%c(Kang2) )*zz
      uu%c(Kang1) = 0
      uu%c(Kang2) = 0
    case (sqrTyp)
      uu%Typ(2) = angTyp
      uu%c(Kang1) =-( nn%c(mom11)*uu%c(Ksqr1) + nn%c(mom12)*uu%c(Ksqr2) )*zz
      uu%c(Kang2) =-( nn%c(mom21)*uu%c(Ksqr1) + nn%c(mom22)*uu%c(Ksqr2) )*zz
      uu%c(Ksqr1) = 0
      uu%c(Ksqr2) = 0
    case (nulTyp)
      uu%Typ(2) = nulTyp
      vv%c(Ksqr1) = nn%c(mom22)*uu%c(Kang1) - nn%c(mom12)*uu%c(Kang2)
      vv%c(Ksqr2) =-nn%c(mom21)*uu%c(Kang1) + nn%c(mom11)*uu%c(Kang2)
      vv%c(Kang1) = nn%c(mom11)*uu%c(Ksqr1) + nn%c(mom12)*uu%c(Ksqr2)
      vv%c(Kang2) = nn%c(mom21)*uu%c(Ksqr1) + nn%c(mom22)*uu%c(Ksqr2)
      uu%c(1:spiSiz) = vv%c(1:spiSiz)*(-zz)
    end select
  end select
  end subroutine


  subroutine gPropagator( uu ,pp ,nn ,xx )
!**************************************************************
! Massless vector boson.
! nn is gauge vector, xx is gauge parameter
! xx>=1 will always result in the Feynman gauge
!**************************************************************
  type(lorentz_type) :: uu
  type(lorentz_type),intent(in) :: pp,nn
  !(realknd2!),intent(in) :: xx
  !(complex2!) :: pn,pu,nu,cTerms(4)
  !(realknd2!) :: rTerms(4)
  if (xx.lt.rONE) then ! non-Feynman gauge
    cTerms(1) = pp%c(momPLS)*nn%c(momMIN)
    cTerms(2) = nn%c(momPLS)*pp%c(momMIN)
    cTerms(3) = pp%c(momTRA)*nn%c(momTSR)
    cTerms(4) = nn%c(momTRA)*pp%c(momTSR)
    rTerms = abs(cTerms)
    pn = cTerms(1)+cTerms(2)-cTerms(3)-cTerms(4) !/2
!    pn = pp%c(momPLS)*nn%c(momMIN) - pp%c(momTRA)*nn%c(momTSR) &
!       + nn%c(momPLS)*pp%c(momMIN) - nn%c(momTRA)*pp%c(momTSR) !/2
!    if (pn.eq.cZRO) then
    if (maxval(rTerms).eq.rZRO) then ! covariant gauge
      pu = pp%c(momPLS)*uu%c(vecMIN) - pp%c(momTRA)*uu%c(vecTSR) &
         + uu%c(vecPLS)*pp%c(momMIN) - uu%c(vecTRA)*pp%c(momTSR) !/2
      uu%c(1:vecSiz) = uu%c(1:vecSiz) &
                     - pp%c(1:vecSiz)*( (1-xx)*pu/(2*pp%s) )
    elseif (abs(pn).gt.1e-9*maxval(rTerms)) then ! axial gauge
      pu = pp%c(momPLS)*uu%c(vecMIN) - pp%c(momTRA)*uu%c(vecTSR) &
         + uu%c(vecPLS)*pp%c(momMIN) - uu%c(vecTRA)*pp%c(momTSR) !/2
      nu = nn%c(momPLS)*uu%c(vecMIN) - nn%c(momTRA)*uu%c(vecTSR) &
         + uu%c(vecPLS)*nn%c(momMIN) - uu%c(vecTRA)*nn%c(momTSR) !/2
      uu%c(1:vecSiz) = uu%c(1:vecSiz) &
                     + pp%c(1:vecSiz)*(( 2*(nn%s+xx*pp%s)*pu/pn - nu )/pn) &
                     - nn%c(1:vecSiz)*( pu/pn )
    endif
  endif
  uu%c(1:vecSiz) = uu%c(1:vecSiz)*(-cIMA/pp%s)
  end subroutine


  subroutine vPropagator( uu ,pp ,mm ,xx )
!**************************************************************
! Massive vector boson.
! mm is complex mass, xx is gauge parameter
! xx>1 is interpreted as xx=infinity
!**************************************************************
  type(lorentz_type) :: uu
  type(lorentz_type),intent(in) :: pp
  !(complex2!),intent(in) :: mm
  !(realknd2!),intent(in) :: xx
  !(complex2!) :: mSqr,pu
  mSqr = mm*mm
  if (xx.ne.rONE) then
    pu = pp%c(momPLS)*uu%c(vecMIN) - pp%c(momTRA)*uu%c(vecTSR) &
       + uu%c(vecPLS)*pp%c(momMIN) - uu%c(vecTRA)*pp%c(momTSR) !/2
    pu = pu/2
    if (xx.lt.rONE) then
      uu%c(1:vecSiz) = uu%c(1:vecSiz) &
                     - pp%c(1:vecSiz)*( pu*(1-xx)/(pp%s-xx*mSqr) )
    else
      uu%c(1:vecSiz) = uu%c(1:vecSiz) &
                     - pp%c(1:vecSiz)*( pu/mSqr )
    endif
  endif
  uu%c(1:vecSiz) = uu%c(1:vecSiz)*( -cIMA/(pp%s-mSqr) )
  end subroutine


  subroutine sPropagator( uu ,pp ,mm )
!**************************************************************
!**************************************************************
  type(lorentz_type) :: uu
  type(lorentz_type),intent(in) :: pp
  !(complex2!),intent(in) :: mm
  uu%c(1) = uu%c(1)*( cIMA/(pp%s-mm*mm) )
  end subroutine
  


  subroutine planarMomenta( pp ,nn )
!**************************************************************
! Given a list of external momenta pp(ii,ii), calculate all
! internal momenta in the planar, ordered, configuration.
!  pp(n1,n2) = pp(n1,n1) + pp(n1+1,n1+1) + ... + pp(n2,n2)
!**************************************************************
  intent(inout) :: pp
  intent(in)    :: nn
  integer :: nn,ii,jj,n1,n2
  type(lorentz_type) :: pp(:,:)
  if (minval(ubound(pp)).lt.nn) then
    write(*,*) 'ERROR in planarMomenta: array too small'
    stop
  endif
  do ii=1,nn-1 ! ii=Ngluons-1
  do n1=1,nn-ii
    n2 = n1+ii
    jj = (n1+n2)/2
    pp(n1,n2)%c(1:momSiz) = pp(n1,jj)%c(1:momSiz) + pp(jj+1,n2)%c(1:momSiz)
    pp(n1,n2)%s = momSqr(pp(n1,n2))
    pp(n1,n2)%Typ = [momTyp,nulTyp,nulTyp]
  enddo
  enddo
  end subroutine


  subroutine gluonCurrents( aa ,pp ,nn ,gaugvec,gaugDen )
!**************************************************************
! Calculate all planar, ordered, purely gluonic currents.
!  aa(n1,n2) = current( gluon(n1),gluon(n1+1),...,gluon(n2) )
! On input, aa(ii,ii) should contain polvec(ii)
!**************************************************************
  intent(inout) :: aa
  intent(in)    :: pp,nn,gaugvec,gaugDen
  optional :: gaugvec,gaugDen
  integer :: nn,ii,n1,n2,j1,j2
  !(complex2!) :: z2,z3,z4,vv,va,pa,vp
  type(lorentz_type) :: aa(:,:),pp(:,:),gaugvec,gaugDen,tt
  logical :: axial
  if (minval(ubound(aa)).lt.nn) then
    write(*,*) 'ERROR in gluonCurrents: array too small'
    stop
  endif
! Couplings
  z2 = cIMA
  z3 = cIMA/rSQRT2 
  z4 = cIMA/2
! Gauge vector
  axial = .false.
  if (present(gaugvec)) then
    do ii=1,vecSiz ;axial=(axial.or.gaugvec%c(ii).ne.0) ;enddo
    vv = 2*momSqr(gaugvec)
  endif
! Calculate off-shell currents with more than 1 gluon
  do ii=1,nn-1 ! ii=Ngluons-1
  do n1=1,nn-ii
    n2 = n1+ii
! Initialize current
    aa(n1,n2)%c(1:vecSiz) = 0
    aa(n1,n2)%Typ = [vecTyp,nulTyp,nulTyp]
! Contribution 3-point vertex
    do j1=n1,n2-1
      tt = gggVertex( pp(n1,n2) ,pp(n1,j1),aa(n1,j1) ,pp(j1+1,n2),aa(j1+1,n2) ,z3 )
      aa(n1,n2)%c(1:vecSiz) = aa(n1,n2)%c(1:vecSiz) + tt%c(1:vecSiz)
    enddo
! Contribution 4-point vertex
    do j1=n1,n2-2
    do j2=j1+1,n2-1
      tt = ggggVertex( aa(n1,j1),aa(j1+1,j2),aa(j2+1,n2) ,-z4,2*z4,-z4 ) 
      aa(n1,n2)%c(1:vecSiz) = aa(n1,n2)%c(1:vecSiz) + tt%c(1:vecSiz)
    enddo
    enddo
! Multiply by propagator (except when all gluons are included)
    if (n1.ne.1.or.n2.ne.nn) then
      if (axial) then
        pa = momD2vec(pp(n1,n2),aa(n1,n2))
        va = momD2vec(gaugvec,aa(n1,n2))
        if (present(gaugDen)) then
          vp = momD2mom(gaugDen,pp(n1,n2))
        else
          vp = momD2mom(gaugvec,pp(n1,n2))
        endif
        pa = pa/vp
        va = va/vp
!        write(*,*) 'vv',prnt(vv) !DEBUG
!        write(*,*) 'vp',prnt(vp) !DEBUG
!        write(*,*) 'pa',prnt(pa) !DEBUG
!        write(*,*) 'va',prnt(va) !DEBUG
        tt = vecFmom( pp(n1,n2),va-pa*vv/vp,gaugvec,pa )
        aa(n1,n2)%c(1:vecSiz) = aa(n1,n2)%c(1:vecSiz) - tt%c(1:vecSiz)
      endif
      aa(n1,n2)%c(1:vecSiz) = aa(n1,n2)%c(1:vecSiz)*(-z2/pp(n1,n2)%s)
    endif
  enddo
  enddo
  end subroutine

end module


