  function ffvVertex( vv ,uu ,zmin,zpls ) result(ww)
!**************************************************************
! slash(p)*( zpls*(1-g5)/2 + zmin*(1+g5)/2 ) | u >
! slash(p)*(V+A*g5) | u >
! zpls=V-A , zmin=V+A
!**************************************************************
  intent(in) :: uu,vv,zpls,zmin
  optional :: zpls
  type(lorentz_type) :: uu,vv,ww
  !(complex2!) :: zpls,zmin ,ymin,ypls
  logical :: zplsPresent
!
  zplsPresent = present(zpls)
  if (zplsPresent) then
    ymin = zmin
    ypls = zpls
  else
    ymin = zmin
    ypls = ymin
  endif
!
  ww%Typ(3) = nulTyp
!
  if (vv%Typ(1).eq.vecTyp) then
    select case (uu%Typ(1))
      case (ketTyp)
        ww%Typ(1) = ketTyp
        select case (uu%Typ(2))
          case (angTyp)
            ww%Typ(2) = sqrTyp
            ww%c(Ksqr1) = ( vv%c(vec22)*uu%c(Kang1) - vv%c(vec12)*uu%c(Kang2) )*ypls
            ww%c(Ksqr2) = (-vv%c(vec21)*uu%c(Kang1) + vv%c(vec11)*uu%c(Kang2) )*ypls
            ww%c(Kang1) = 0
            ww%c(Kang2) = 0
          case (sqrTyp)
            ww%Typ(2) = angTyp
            ww%c(Ksqr1) = 0
            ww%c(Ksqr2) = 0
            ww%c(Kang1) = ( vv%c(vec11)*uu%c(Ksqr1) + vv%c(vec12)*uu%c(Ksqr2) )*ymin
            ww%c(Kang2) = ( vv%c(vec21)*uu%c(Ksqr1) + vv%c(vec22)*uu%c(Ksqr2) )*ymin
          case default
            ww%Typ(2) = nulTyp
            ww%c(Ksqr1) = ( vv%c(vec22)*uu%c(Kang1) - vv%c(vec12)*uu%c(Kang2) )*ypls
            ww%c(Ksqr2) = (-vv%c(vec21)*uu%c(Kang1) + vv%c(vec11)*uu%c(Kang2) )*ypls
            ww%c(Kang1) = ( vv%c(vec11)*uu%c(Ksqr1) + vv%c(vec12)*uu%c(Ksqr2) )*ymin
            ww%c(Kang2) = ( vv%c(vec21)*uu%c(Ksqr1) + vv%c(vec22)*uu%c(Ksqr2) )*ymin
        end select
      case (braTyp)
        ww%Typ(1) = braTyp
        select case (uu%Typ(2))
          case (sqrTyp)
            ww%Typ(2) = angTyp
            ww%c(Bsqr1) = 0
            ww%c(Bsqr2) = 0
            ww%c(Bang1) = ( uu%c(Bsqr1)*vv%c(vec22) - uu%c(Bsqr2)*vv%c(vec21) )*ypls
            ww%c(Bang2) = (-uu%c(Bsqr1)*vv%c(vec12) + uu%c(Bsqr2)*vv%c(vec11) )*ypls
          case (angTyp)
            ww%Typ(2) = sqrTyp
            ww%c(Bsqr1) = ( uu%c(Bang1)*vv%c(vec11) + uu%c(Bang2)*vv%c(vec21) )*ymin
            ww%c(Bsqr2) = ( uu%c(Bang1)*vv%c(vec12) + uu%c(Bang2)*vv%c(vec22) )*ymin
            ww%c(Bang1) = 0
            ww%c(Bang2) = 0
          case default
            ww%Typ(2) = nulTyp
            ww%c(Bsqr1) = ( uu%c(Bang1)*vv%c(vec11) + uu%c(Bang2)*vv%c(vec21) )*ymin
            ww%c(Bsqr2) = ( uu%c(Bang1)*vv%c(vec12) + uu%c(Bang2)*vv%c(vec22) )*ymin
            ww%c(Bang1) = ( uu%c(Bsqr1)*vv%c(vec22) - uu%c(Bsqr2)*vv%c(vec21) )*ypls
            ww%c(Bang2) = (-uu%c(Bsqr1)*vv%c(vec12) + uu%c(Bsqr2)*vv%c(vec11) )*ypls
        end select
      case default
        if (errru.ge.0) write(errru,*) 'ERROR in avh_lorentz ffvVertex',prnt(vv%Typ,3),prnt(uu%Typ,3)
        stop
    end select
!
  elseif (uu%Typ(1).eq.vecTyp) then
    select case (vv%Typ(1))
      case (ketTyp)
        ww%Typ(1) = ketTyp
        select case (vv%Typ(2))
          case (angTyp)
            ww%Typ(2) = sqrTyp
            ww%c(Ksqr1) = ( uu%c(vec22)*vv%c(Kang1) - uu%c(vec12)*vv%c(Kang2) )*ypls
            ww%c(Ksqr2) = (-uu%c(vec21)*vv%c(Kang1) + uu%c(vec11)*vv%c(Kang2) )*ypls
            ww%c(Kang1) = 0
            ww%c(Kang2) = 0
          case (sqrTyp)
            ww%Typ(2) = angTyp
            ww%c(Ksqr1) = 0
            ww%c(Ksqr2) = 0
            ww%c(Kang1) = ( uu%c(vec11)*vv%c(Ksqr1) + uu%c(vec12)*vv%c(Ksqr2) )*ymin
            ww%c(Kang2) = ( uu%c(vec21)*vv%c(Ksqr1) + uu%c(vec22)*vv%c(Ksqr2) )*ymin
          case default
            ww%Typ(2) = nulTyp
            ww%c(Ksqr1) = ( uu%c(vec22)*vv%c(Kang1) - uu%c(vec12)*vv%c(Kang2) )*ypls
            ww%c(Ksqr2) = (-uu%c(vec21)*vv%c(Kang1) + uu%c(vec11)*vv%c(Kang2) )*ypls
            ww%c(Kang1) = ( uu%c(vec11)*vv%c(Ksqr1) + uu%c(vec12)*vv%c(Ksqr2) )*ymin
            ww%c(Kang2) = ( uu%c(vec21)*vv%c(Ksqr1) + uu%c(vec22)*vv%c(Ksqr2) )*ymin
        end select
      case (braTyp)
        ww%Typ(1) = braTyp
        select case (vv%Typ(2))
          case (sqrTyp)
            ww%Typ(2) = angTyp
            ww%c(Bsqr1) = 0
            ww%c(Bsqr2) = 0
            ww%c(Bang1) = ( vv%c(Bsqr1)*uu%c(vec22) - vv%c(Bsqr2)*uu%c(vec21) )*ypls
            ww%c(Bang2) = (-vv%c(Bsqr1)*uu%c(vec12) + vv%c(Bsqr2)*uu%c(vec11) )*ypls
          case (angTyp)
            ww%Typ(2) = sqrTyp
            ww%c(Bsqr1) = ( vv%c(Bang1)*uu%c(vec11) + vv%c(Bang2)*uu%c(vec21) )*ymin
            ww%c(Bsqr2) = ( vv%c(Bang1)*uu%c(vec12) + vv%c(Bang2)*uu%c(vec22) )*ymin
            ww%c(Bang1) = 0
            ww%c(Bang2) = 0
          case default
            ww%Typ(2) = nulTyp
            ww%c(Bsqr1) = ( vv%c(Bang1)*uu%c(vec11) + vv%c(Bang2)*uu%c(vec21) )*ymin
            ww%c(Bsqr2) = ( vv%c(Bang1)*uu%c(vec12) + vv%c(Bang2)*uu%c(vec22) )*ymin
            ww%c(Bang1) = ( vv%c(Bsqr1)*uu%c(vec22) - vv%c(Bsqr2)*uu%c(vec21) )*ypls
            ww%c(Bang2) = (-vv%c(Bsqr1)*uu%c(vec12) + vv%c(Bsqr2)*uu%c(vec11) )*ypls
        end select
      case default
        if (errru.ge.0) write(errru,*) 'ERROR in avh_lorentz ffvVertex',prnt(vv%Typ,3),prnt(uu%Typ,3)
        stop
    end select
!
  elseif (uu%Typ(1).eq.ketTyp) then
    ww%Typ(1:2) = [vecTyp,nulTyp]
    if     (uu%Typ(2).eq.angTyp.or.vv%Typ(2).eq.sqrTyp) then
      ypls = 2*ypls
      ww%c(vec11) =  uu%c(Kang1)*vv%c(Bsqr1)*ypls
      ww%c(vec12) =  uu%c(Kang1)*vv%c(Bsqr2)*ypls
      ww%c(vec21) =  uu%c(Kang2)*vv%c(Bsqr1)*ypls
      ww%c(vec22) =  uu%c(Kang2)*vv%c(Bsqr2)*ypls
    elseif (uu%Typ(2).eq.sqrTyp.or.vv%Typ(2).eq.angTyp) then
      ymin = 2*ymin
      ww%c(vec11) =  uu%c(Ksqr2)*vv%c(Bang2)*ymin
      ww%c(vec12) = -uu%c(Ksqr1)*vv%c(Bang2)*ymin
      ww%c(vec21) = -uu%c(Ksqr2)*vv%c(Bang1)*ymin
      ww%c(vec22) =  uu%c(Ksqr1)*vv%c(Bang1)*ymin
    elseif (zplsPresent) then
      ypls = 2*ypls  ;ymin = 2*ymin
      ww%c(vec11) = uu%c(Kang1)*vv%c(Bsqr1)*ypls + uu%c(Ksqr2)*vv%c(Bang2)*ymin
      ww%c(vec12) = uu%c(Kang1)*vv%c(Bsqr2)*ypls - uu%c(Ksqr1)*vv%c(Bang2)*ymin
      ww%c(vec21) = uu%c(Kang2)*vv%c(Bsqr1)*ypls - uu%c(Ksqr2)*vv%c(Bang1)*ymin
      ww%c(vec22) = uu%c(Kang2)*vv%c(Bsqr2)*ypls + uu%c(Ksqr1)*vv%c(Bang1)*ymin
    else
      ymin = 2*ymin
      ww%c(vec11) = ( uu%c(Kang1)*vv%c(Bsqr1) + uu%c(Ksqr2)*vv%c(Bang2) )*ymin
      ww%c(vec12) = ( uu%c(Kang1)*vv%c(Bsqr2) - uu%c(Ksqr1)*vv%c(Bang2) )*ymin
      ww%c(vec21) = ( uu%c(Kang2)*vv%c(Bsqr1) - uu%c(Ksqr2)*vv%c(Bang1) )*ymin
      ww%c(vec22) = ( uu%c(Kang2)*vv%c(Bsqr2) + uu%c(Ksqr1)*vv%c(Bang1) )*ymin
    endif
!
  else
    ww%Typ(1:2) = [vecTyp,nulTyp]
    if     (vv%Typ(2).eq.angTyp.or.uu%Typ(2).eq.sqrTyp) then
      ypls = 2*ypls
      ww%c(vec11) =  vv%c(Kang1)*uu%c(Bsqr1)*ypls
      ww%c(vec12) =  vv%c(Kang1)*uu%c(Bsqr2)*ypls
      ww%c(vec21) =  vv%c(Kang2)*uu%c(Bsqr1)*ypls
      ww%c(vec22) =  vv%c(Kang2)*uu%c(Bsqr2)*ypls
    elseif (vv%Typ(2).eq.sqrTyp.or.uu%Typ(2).eq.angTyp) then
      ymin = 2*ymin
      ww%c(vec11) =  vv%c(Ksqr2)*uu%c(Bang2)*ymin
      ww%c(vec12) = -vv%c(Ksqr1)*uu%c(Bang2)*ymin
      ww%c(vec21) = -vv%c(Ksqr2)*uu%c(Bang1)*ymin
      ww%c(vec22) =  vv%c(Ksqr1)*uu%c(Bang1)*ymin
    elseif (zplsPresent) then
      ypls = 2*ypls  ;ymin = 2*ymin
      ww%c(vec11) = vv%c(Kang1)*uu%c(Bsqr1)*ypls + vv%c(Ksqr2)*uu%c(Bang2)*ymin
      ww%c(vec12) = vv%c(Kang1)*uu%c(Bsqr2)*ypls - vv%c(Ksqr1)*uu%c(Bang2)*ymin
      ww%c(vec21) = vv%c(Kang2)*uu%c(Bsqr1)*ypls - vv%c(Ksqr2)*uu%c(Bang1)*ymin
      ww%c(vec22) = vv%c(Kang2)*uu%c(Bsqr2)*ypls + vv%c(Ksqr1)*uu%c(Bang1)*ymin
    else
      ymin = 2*ymin
      ww%c(vec11) = ( vv%c(Kang1)*uu%c(Bsqr1) + vv%c(Ksqr2)*uu%c(Bang2) )*ymin
      ww%c(vec12) = ( vv%c(Kang1)*uu%c(Bsqr2) - vv%c(Ksqr1)*uu%c(Bang2) )*ymin
      ww%c(vec21) = ( vv%c(Kang2)*uu%c(Bsqr1) - vv%c(Ksqr2)*uu%c(Bang1) )*ymin
      ww%c(vec22) = ( vv%c(Kang2)*uu%c(Bsqr2) + vv%c(Ksqr1)*uu%c(Bang1) )*ymin
    endif
!
  endif
  end function
