module avh_lorentz
  !(usekindmod!)
  use avh_mathcnst
  use avh_iounits
  use avh_prnt

  implicit none
  private
  public :: lorentz_type,init_lorentz
  public :: momDef,xVector,xSpinor 
  public :: putToZero,oscPlus,contract
  public :: planarMomenta,gluonCurrents,print_lorentz
  public :: xScalar
  public :: conj,ellBasis,print_4mom,print_4real
  public :: xternalArgs_type
  public :: gggVertex,ggggVertex,ffvVertex
  public :: gPropagator,vPropagator,fPropagator,sPropagator
  public :: ePropagator

  integer ,parameter :: momSiz=4,vecSiz=4,spiSiz=4
  integer ,parameter :: oscSiz=4 ! maximum of the ones above
  integer ,parameter :: typSiz=3

  type :: lorentz_type
    private
    !(complex2!) :: c(1:oscsiz)   ! complex components
    !(complex2!) :: s             ! complex scalar
    !(integer0!) :: Typ(1:typSiz)
  end type
  
  type :: xternalArgs_type
    type(lorentz_type) :: auxVec
    integer :: helicity
  end type

  !(integer0!),parameter :: nulTyp=0
  !(integer0!),parameter :: momTyp=1,vecTyp=2,braTyp=4,ketTyp=8,scaTyp=16
  !(integer0!),parameter :: angTyp=1,sqrTyp=2,plsTyp=4,minTyp=8

  integer,parameter :: vec0p3=1 ,vec0m3=2 ,vec1p2=3 ,vec1m2=4
  integer,parameter :: vec11 =1 ,vec22 =2 ,vec21 =3 ,vec12 =4

  integer,parameter :: mom0p3=1 ,mom0m3=2 ,mom1p2=3 ,mom1m2=4
  integer,parameter :: mom11 =1 ,mom22 =2 ,mom21 =3 ,mom12 =4

  integer,parameter :: Bsqr1=1     ,Bsqr2=2     ,Bang1=3     ,Bang2=4
  integer,parameter :: Ksqr2=Bsqr1 ,Ksqr1=Bsqr2 ,Kang2=Bang1 ,Kang1=Bang2

  interface momDef
    module procedure momDef_r,momDef_c ,momDef_rr
    module procedure momDef_ri,momDef_ci,momDef_rri
  end interface
  interface vecFmom
    module procedure vecFmom_0,vecFmom_1,vecFmom_2
  end interface
  interface xVector
    module procedure xVector_a,xVector_b,xVector_c
  end interface
  interface xSpinor
    module procedure xSpinor_a,xSpinor_b
  end interface
  interface xScalar
    module procedure xScalar_0,xScalar_r,xScalar_c
  end interface

  logical :: initd=.false.

contains


  subroutine print_lorentz( aa ,ndec ,iunit )
  intent(in) :: aa,ndec,iunit
  optional :: iunit
  type(lorentz_type) :: aa
  integer :: ndec,iunit,nunit
  nunit=stdout ;if(present(iunit))nunit=iunit
  if (nunit.le.0) return
  select case (aa%Typ(1))
    case default ;write(nunit,*) 'WARNING from print_lorentz: type does not exist'
    case (scaTyp)
      select case (aa%Typ(2))
        case (nulTyp) ;write(nunit,*) 'sr  '//prnt( aa%c(1),ndec )
        case (plsTyp) ;write(nunit,*) 'sr+ '//prnt( aa%c(1),ndec )
        case (minTyp) ;write(nunit,*) 'sr- '//prnt( aa%c(1),ndec )
      end select
    case (vecTyp) ;write(nunit,*) 'vec '//prnt( aa%c(1:vecSiz),ndec )
    case (momTyp) ;write(nunit,*) 'mom '//prnt( aa%c(1:momSiz),ndec )
    case (braTyp)
      select case (aa%Typ(2))
        case (nulTyp) ;write(nunit,*) 'bra '//prnt( aa%c(1:spiSiz),ndec )
        case (angTyp) ;write(nunit,*) '< | '//prnt( aa%c(1:spiSiz),ndec ) 
        case (sqrTyp) ;write(nunit,*) '[ | '//prnt( aa%c(1:spiSiz),ndec ) 
      end select
    case (ketTyp)
      select case (aa%Typ(2))
        case (nulTyp) ;write(nunit,*) 'ket '//prnt( aa%c(1:spiSiz),ndec )
        case (angTyp) ;write(nunit,*) '| > '//prnt( aa%c(1:spiSiz),ndec ) 
        case (sqrTyp) ;write(nunit,*) '| ] '//prnt( aa%c(1:spiSiz),ndec ) 
      end select
  end select
  end subroutine

  subroutine print_4mom( aa ,ndec ,iunit )
  intent(in) :: aa,ndec,iunit
  optional :: iunit
  type(lorentz_type) :: aa
  integer :: ndec,iunit,nunit
  nunit=stdout ;if(present(iunit))nunit=iunit
  if (nunit.le.0) return
  select case(aa%Typ(1))
    case default ;write(nunit,*) 'WARNING from print_4mom: type does not exist'
    case (scaTyp) ;return
    case (braTyp) ;return
    case (ketTyp) ;return
    case (vecTyp)
      write(nunit,*) 'vec '//prnt((aa%c(mom0p3)+aa%c(mom0m3))/2,ndec ) &
                      //' '//prnt((aa%c(mom1p2)+aa%c(mom1m2))/2,ndec ) &
                      //' '//prnt((aa%c(mom1p2)-aa%c(mom1m2))/2/cIMA,ndec ) &
                      //' '//prnt((aa%c(mom0p3)-aa%c(mom0m3))/2,ndec )
    case (momTyp)
      write(nunit,*) 'mom '//prnt((aa%c(mom0p3)+aa%c(mom0m3))/2,ndec ) &
                      //' '//prnt((aa%c(mom1p2)+aa%c(mom1m2))/2,ndec ) &
                      //' '//prnt((aa%c(mom1p2)-aa%c(mom1m2))/2/cIMA,ndec ) &
                      //' '//prnt((aa%c(mom0p3)-aa%c(mom0m3))/2,ndec )
  end select
  end subroutine

  subroutine print_4real( aa ,ndec ,iunit )
  intent(in) :: aa,ndec,iunit
  optional :: iunit
  type(lorentz_type) :: aa
  integer :: ndec,iunit,nunit
  nunit=stdout ;if(present(iunit))nunit=iunit
  if (nunit.le.0) return
  select case(aa%Typ(1))
    case default ;write(nunit,*) 'WARNING from print_4real: type does not exist'
    case (scaTyp) ;return
    case (braTyp) ;return
    case (ketTyp) ;return
    case (vecTyp)
      write(nunit,*) 'vec '//prnt(real((aa%c(mom0p3)+aa%c(mom0m3))/2     ),ndec ) &
                      //' '//prnt(real((aa%c(mom1p2)+aa%c(mom1m2))/2     ),ndec ) &
                      //' '//prnt(real((aa%c(mom1p2)-aa%c(mom1m2))/2/cIMA),ndec ) &
                      //' '//prnt(real((aa%c(mom0p3)-aa%c(mom0m3))/2     ),ndec )
    case (momTyp)
      write(nunit,*) 'mom '//prnt(real((aa%c(mom0p3)+aa%c(mom0m3))/2     ),ndec ) &
                      //' '//prnt(real((aa%c(mom1p2)+aa%c(mom1m2))/2     ),ndec ) &
                      //' '//prnt(real((aa%c(mom1p2)-aa%c(mom1m2))/2/cIMA),ndec ) &
                      //' '//prnt(real((aa%c(mom0p3)-aa%c(mom0m3))/2     ),ndec )
  end select
  end subroutine


  subroutine init_lorentz
  integer :: ii,jj
  if (initd) return ;initd=.true.
  call init_mathcnst
  end subroutine


  function conj(aa) result(bb)
  intent(in) :: aa
  type(lorentz_type) :: aa,bb
  bb%c(vec0p3) = conjg(aa%c(vec0p3))
  bb%c(vec0m3) = conjg(aa%c(vec0m3))
  bb%c(vec1p2) = conjg(aa%c(vec1m2))
  bb%c(vec1m2) = conjg(aa%c(vec1p2))
  bb%s   = conjg(aa%s)
  bb%Typ = aa%Typ
  end function


  subroutine putToZero( aa ,nlow,nupp )
  integer,intent(in) :: nlow,nupp
  type(lorentz_type) :: aa(1:nupp)
  integer :: ii
  do ii=nlow,nupp
    aa(ii)%c = 0
    aa(ii)%s = 0
    aa(ii)%Typ = [nulTyp,nulTyp,nulTyp]
  enddo
  end subroutine

  function oscPlus( aa,bb ) result(cc)
  intent(in) :: aa,bb
  type(lorentz_type) :: aa,bb,cc
  cc%c = aa%c + bb%c
  cc%Typ = aa%Typ
  if (cc%Typ(1).eq.nulTyp) then
     cc%Typ = bb%Typ
  elseif (aa%Typ(2).ne.bb%Typ(2)) then
     cc%Typ(2) = nulTyp
  endif
  end function

  function momDef_rr( p0,p2,p3,p1,ss ) result(qq)
!************************************************
!* Rotated input
!************************************************
  !(realknd2!),intent(in) :: p0,p1,p2,p3,ss
  type(lorentz_type) :: qq
  !(complex2!) :: p2i
  p2i = p2*cIMA
  qq%c(mom0p3) = p0+p3
  qq%c(mom0m3) = p0-p3
  qq%c(mom1p2) = p1+p2i
  qq%c(mom1m2) = p1-p2i
  qq%s = ss
  qq%Typ = [momTyp,nulTyp,nulTyp]
  end function

  function momDef_r( p0,p2,p3,p1 ) result(qq)
!************************************************
!* Rotated input
!************************************************
  intent(in) :: p0,p1,p2,p3
  !(realknd2!) :: p0,p1,p2,p3
  type(lorentz_type) :: qq
  !(complex2!) :: p2i
  p2i = p2*cIMA
  qq%c(mom0p3) = p0+p3
  qq%c(mom0m3) = p0-p3
  qq%c(mom1p2) = p1+p2i
  qq%c(mom1m2) = p1-p2i
  qq%Typ = [momTyp,nulTyp,nulTyp]
  end function

  function momDef_c( p0,p2,p3,p1 ) result(qq)
!************************************************
!* Rotated input
!************************************************
  intent(in) :: p0,p1,p2,p3
  !(complex2!) :: p0,p1,p2,p3,p2i
  type(lorentz_type) :: qq
  p2i = p2*cIMA
  qq%c(mom0p3) = p0+p3
  qq%c(mom0m3) = p0-p3
  qq%c(mom1p2) = p1+p2i
  qq%c(mom1m2) = p1-p2i
  qq%Typ = [momTyp,nulTyp,nulTyp]
  end function

  function momDef_ri( p ,l ) result(qq)
!************************************************
!************************************************
  intent(in) :: p,l
  !(realknd2!) :: p(0:3)
  integer :: l(1:3)
  type(lorentz_type) :: qq
  !(complex2!) :: p2i
  p2i = p(l(2))*cIMA
  qq%c(mom0p3) = p(0)+p(l(3))
  qq%c(mom0m3) = p(0)-p(l(3))
  qq%c(mom1p2) = p(l(1))+p2i
  qq%c(mom1m2) = p(l(1))-p2i
  qq%Typ = [momTyp,nulTyp,nulTyp]
  end function

  function momDef_rri( p ,s ,l ) result(qq)
!************************************************
!************************************************
  intent(in) :: p,s,l
  !(realknd2!) :: p(0:3),s
  integer :: l(1:3)
  type(lorentz_type) :: qq
  !(complex2!) :: p2i
  p2i = p(l(2))*cIMA
  qq%c(mom0p3) = p(0)+p(l(3))
  qq%c(mom0m3) = p(0)-p(l(3))
  qq%c(mom1p2) = p(l(1))+p2i
  qq%c(mom1m2) = p(l(1))-p2i
  qq%s = s
  qq%Typ = [momTyp,nulTyp,nulTyp]
  end function

  function momDef_ci( p ,l ) result(qq)
!************************************************
!************************************************
  intent(in) :: p,l
  !(complex2!) :: p(0:3),p2i
  integer :: l(1:3)
  type(lorentz_type) :: qq
  p2i = p(l(2))*cIMA
  qq%c(mom0p3) = p(0)+p(l(3))
  qq%c(mom0m3) = p(0)-p(l(3))
  qq%c(mom1p2) = p(l(1))+p2i
  qq%c(mom1m2) = p(l(1))-p2i
  qq%Typ = [momTyp,nulTyp,nulTyp]
  end function


  function contract( aa,bb ) result(rslt)
  intent(in) :: aa,bb
  type(lorentz_type) :: aa,bb
  !(complex2!) :: rslt
  if     (aa%Typ(1).eq.momTyp) then
    if     (bb%Typ(1).eq.momTyp) then ;rslt=momD2mom(aa,bb)/2
    elseif (bb%Typ(1).eq.vecTyp) then ;rslt=momD2vec(aa,bb)/2
    endif
  elseif (aa%Typ(1).eq.vecTyp) then
    if     (bb%Typ(1).eq.momTyp) then ;rslt=momD2vec(bb,aa)/2
    elseif (bb%Typ(1).eq.vecTyp) then ;rslt=momD2mom(aa,bb)/2
    endif
  elseif (aa%Typ(1).eq.braTyp) then ;rslt = braDket(aa,bb)
  elseif (aa%Typ(1).eq.ketTyp) then ;rslt = braDket(bb,aa)
  else
    rslt = aa%c(1)*bb%c(1)
!  elseif (aa%Typ.eq.scaTyp) then ;rslt = aa%c(1)*bb%c(1)
!  elseif (aa%Typ.eq.eBraTyp) then ;rslt = aa%c(1)*bb%c(1)
!  elseif (aa%Typ.eq.eKetTyp) then ;rslt = aa%c(1)*bb%c(1)
  endif
  end function


  function vecD2vec( aa,bb ) result(rslt)
  intent(in) :: aa,bb
  type(lorentz_type) :: aa,bb
  !(complex2!) :: rslt
  rslt = aa%c(vec0p3)*bb%c(vec0m3) - aa%c(vec1p2)*bb%c(vec1m2) &
       + bb%c(vec0p3)*aa%c(vec0m3) - bb%c(vec1p2)*aa%c(vec1m2)
  end function

  function momSqr( aa ) result(rslt)
  intent(in) :: aa
  type(lorentz_type) :: aa
  !(complex2!) :: rslt
  rslt = aa%c(mom0p3)*aa%c(mom0m3) - aa%c(mom1p2)*aa%c(mom1m2)
  end function

  function momD2mom( aa,bb ) result(rslt)
  intent(in) :: aa,bb
  type(lorentz_type) :: aa,bb
  !(complex2!) :: rslt
  rslt = aa%c(mom0p3)*bb%c(mom0m3) - aa%c(mom1p2)*bb%c(mom1m2) &
       + bb%c(mom0p3)*aa%c(mom0m3) - bb%c(mom1p2)*aa%c(mom1m2)
  end function

  function momD2vec( aa,bb ) result(rslt)
  intent(in) :: aa,bb
  type(lorentz_type) :: aa,bb
  !(complex2!) :: rslt
  rslt = aa%c(mom0p3)*bb%c(vec0m3) - aa%c(mom1p2)*bb%c(vec1m2) &
       + bb%c(vec0p3)*aa%c(mom0m3) - bb%c(vec1p2)*aa%c(mom1m2)
  end function

  function braDket( aa,bb ) result(rslt)
  intent(in) :: aa,bb
  type(lorentz_type) :: aa,bb
  !(complex2!) :: rslt
  if     (aa%Typ(2).eq.angTyp.or.bb%Typ(2).eq.angTyp) then
    rslt = aa%c(Bang1)*bb%c(Kang1) + aa%c(Bang2)*bb%c(Kang2)
  elseif (aa%Typ(2).eq.sqrTyp.or.bb%Typ(2).eq.sqrTyp) then
    rslt = aa%c(Bsqr1)*bb%c(Ksqr1) + aa%c(Bsqr2)*bb%c(Ksqr2)
  else
    rslt = aa%c(Bsqr1)*bb%c(Ksqr1) + aa%c(Bsqr2)*bb%c(Ksqr2) &
         + aa%c(Bang1)*bb%c(Kang1) + aa%c(Bang2)*bb%c(Kang2)
  endif
  end function

  function vecFmom_0( aa ) result(bb)
  intent(in) :: aa
  type(lorentz_type) :: aa,bb
  bb%c(1:vecSiz) = aa%c(1:vecSiz)
  bb%Typ = [vecTyp,nulTyp,nulTyp]
  end function
  function vecFmom_1( aa,zz ) result(bb)
  intent(in) :: aa,zz
  type(lorentz_type) :: aa,bb
  !(complex2!) :: zz
  bb%c(1:vecSiz) = aa%c(1:vecSiz)*zz
  bb%Typ = [vecTyp,nulTyp,nulTyp]
  end function
  function vecFmom_2( a1,z1 ,a2,z2 ) result(bb)
  intent(in) :: a1,a2,z1,z2
  type(lorentz_type) :: a1,a2,bb
  !(complex2!) :: z1,z2
  bb%c(1:vecSiz) = a1%c(1:vecSiz)*z1 + a2%c(1:vecSiz)*z2
  bb%Typ = [vecTyp,nulTyp,nulTyp]
  end function


  function ketFmom( pp ) result(uu)
!*******************************************************************
!* 
!*         / Ksqr1 \          1        / -p1+i*p2 \
!*   |p] = |       | = --------------- |          |
!*         \ Ksqr2 /    sqrt(|p0+p3|)  \   p0+p3  /
!* 
!*         / Kang1 \   |p0+p3|/(p0+p3) /  p0+p3  \
!*   |p> = |       | = --------------- |         |
!*         \ Kang2 /    sqrt(|p0+p3|)  \ p1+i*p2 /
!* 
!* 
!*         / Bsqr1 \         1         /  p0+p3  \
!*   [p| = |       | = --------------- |         |
!*         \ Bsqr2 /   sqrt(|p0+p3|)   \ p1-i*p2 /
!* 
!*         / Bang1 \   |p0+p3|/(p0+p3) / -p1-i*p2 \
!*   <p| = |       | = --------------- |          |
!*         \ Bang2 /   sqrt(|p0+p3|)   \  p0+p3   /
!* 
!*******************************************************************
  intent(in) :: pp
  type(lorentz_type) :: pp,uu
  !(complex2!) :: expiphi
  !(realknd2!) :: a0p3,a0m3
  a0p3 = abs(pp%c(mom0p3))
  if (a0p3.le.RZRO) then
    a0m3 = abs(pp%c(mom0m3))
    if (a0m3.le.RZRO) then
      uu%c(1:spisiz) = 0
    else ! consider momentum to be result of p1->0,p2=0
      a0m3 = sqrt(a0m3)
      uu%c(Ksqr1) =-pp%c(mom0m3)/a0m3
      uu%c(Ksqr2) = 0
      uu%c(Kang1) = 0
      uu%c(Kang2) = a0m3
    endif
  else
    expiphi = a0p3/pp%c(mom0p3)
    a0p3 = 1/sqrt( a0p3 )
    uu%c(Ksqr1) =-pp%c(mom1m2)*a0p3
    uu%c(Ksqr2) = pp%c(mom0p3)*a0p3
    uu%c(Kang1) = uu%c(Ksqr2 )*expiphi
    uu%c(Kang2) = pp%c(mom1p2)*expiphi*a0p3
  endif
  uu%Typ = [ketTyp,nulTyp,nulTyp]
  end function

  function braFmom( pp ) result(uu)
  intent(in) :: pp
  type(lorentz_type) :: pp,uu
  !(complex2!) :: expiphi
  !(realknd2!) :: a0p3,a0m3
  a0p3 = abs(pp%c(mom0p3))
  if (a0p3.le.RZRO) then
    a0m3 = abs(pp%c(mom0m3))
    if (a0m3.le.RZRO) then
      uu%c(1:spisiz) = 0
    else ! consider momentum to be result of p1->0,p2=0
      a0m3 = sqrt(a0m3)
      uu%c(Bsqr1) = 0
      uu%c(Bsqr2) = pp%c(mom0m3)/a0m3
      uu%c(Bang1) =-a0m3
      uu%c(Bang2) = 0
    endif
  else
    expiphi = a0p3/pp%c(mom0p3)
    a0p3 = 1/sqrt( a0p3 )
    uu%c(Bsqr1) =  pp%c(mom0p3)*a0p3
    uu%c(Bsqr2) =  pp%c(mom1m2)*a0p3
    uu%c(Bang1) = -pp%c(mom1p2)*expiphi*a0p3
    uu%c(Bang2) =  uu%c(Bsqr1 )*expiphi
  endif
  uu%Typ = [braTyp,nulTyp,nulTyp]
  end function

  function KsqrFmom( pp ) result(uu)
  intent(in) :: pp
  type(lorentz_type) :: pp,uu
  !(realknd2!) :: a0p3,a0m3
  a0p3 = abs(pp%c(mom0p3))
  if (a0p3.le.RZRO) then
    a0m3 = abs(pp%c(mom0m3))
    if (a0m3.le.RZRO) then
      uu%c(1:spisiz) = 0
    else ! consider momentum to be result of p1->0,p2=0
      a0m3 = sqrt(a0m3)
      uu%c(Ksqr1) =-pp%c(mom0m3)/a0m3
      uu%c(Ksqr2) = 0
      uu%c(Kang1) = 0
      uu%c(Kang2) = 0
    endif
  else
    a0p3 = 1/sqrt( a0p3 )
    uu%c(Ksqr1) =-pp%c(mom1m2)*a0p3
    uu%c(Ksqr2) = pp%c(mom0p3)*a0p3
    uu%c(Kang1) = 0
    uu%c(Kang2) = 0
  endif
  uu%Typ = [ketTyp,sqrTyp,nulTyp]
  end function

  function BsqrFmom( pp ) result(uu)
  intent(in) :: pp
  type(lorentz_type) :: pp,uu
  !(realknd2!) :: a0p3,a0m3
  a0p3 = abs(pp%c(mom0p3))
  if (a0p3.le.RZRO) then
    a0m3 = abs(pp%c(mom0m3))
    if (a0m3.le.RZRO) then
      uu%c(1:spisiz) = 0
    else ! consider momentum to be result of p1->0,p2=0
      a0m3 = sqrt(a0m3)
      uu%c(Bsqr1) = 0
      uu%c(Bsqr2) = pp%c(mom0m3)/a0m3
      uu%c(Bang1) = 0
      uu%c(Bang2) = 0
    endif
  else
    a0p3 = 1/sqrt( a0p3 )
    uu%c(Bsqr1) = pp%c(mom0p3)*a0p3
    uu%c(Bsqr2) = pp%c(mom1m2)*a0p3
    uu%c(Bang1) = 0
    uu%c(Bang2) = 0
  endif
  uu%Typ = [braTyp,sqrTyp,nulTyp]
  end function

  function KangFmom( pp ) result(uu)
  intent(in) :: pp
  type(lorentz_type) :: pp,uu
  !(complex2!) :: zz
  !(realknd2!) :: a0p3,a0m3
  a0p3 = abs(pp%c(mom0p3))
  if (a0p3.le.RZRO) then
    a0m3 = abs(pp%c(mom0m3))
    if (a0m3.le.RZRO) then
      uu%c(1:spisiz) = 0
    else ! consider momentum to be result of p1->0,p2=0
      a0m3 = sqrt(a0m3)
      uu%c(Ksqr1) = 0
      uu%c(Ksqr2) = 0
      uu%c(Kang1) = 0
      uu%c(Kang2) = a0m3
    endif
  else
    zz = sqrt(a0p3)/pp%c(mom0p3)
    uu%c(Ksqr1) = 0
    uu%c(Ksqr2) = 0
    uu%c(Kang1) = pp%c(mom0p3)*zz
    uu%c(Kang2) = pp%c(mom1p2)*zz
  endif
  uu%Typ = [ketTyp,angTyp,nulTyp]
  end function

  function BangFmom( pp ) result(uu)
  intent(in) :: pp
  type(lorentz_type) :: pp,uu
  !(complex2!) :: zz
  !(realknd2!) :: a0p3,a0m3
  a0p3 = abs(pp%c(mom0p3))
  if (a0p3.le.RZRO) then
    a0m3 = abs(pp%c(mom0m3))
    if (a0m3.le.RZRO) then
      uu%c(1:spisiz) = 0
    else ! consider momentum to be result of p1->0,p2=0
      a0m3 = sqrt(a0m3)
      uu%c(Bsqr1) = 0
      uu%c(Bsqr2) = 0
      uu%c(Bang1) =-a0m3
      uu%c(Bang2) = 0
    endif
  else
    zz = sqrt(a0p3)/pp%c(mom0p3)
    uu%c(Bsqr1) = 0
    uu%c(Bsqr2) = 0
    uu%c(Bang1) =-pp%c(mom1p2)*zz
    uu%c(Bang2) = pp%c(mom0p3)*zz
  endif
  uu%Typ = [braTyp,angTyp,nulTyp]
  end function

  function braFket( vv ) result(uu)
  intent(in) :: vv
  type(lorentz_type) :: vv,uu
  uu%c(Bsqr1) = vv%c(Ksqr2)
  uu%c(Bsqr2) =-vv%c(Ksqr1)
  uu%c(Bang1) =-vv%c(Kang2)
  uu%c(Bang2) = vv%c(Kang1)
  uu%Typ(1) = braTyp
  uu%Typ(2:typSiz) = vv%Typ(2:typSiz)
  end function

  function ketFbra( vv ) result(uu)
  intent(in) :: vv
  type(lorentz_type) :: vv,uu
  uu%c(Ksqr1) =-vv%c(Bsqr2)
  uu%c(Ksqr2) = vv%c(Bsqr1)
  uu%c(Kang1) = vv%c(Bang2)
  uu%c(Kang2) =-vv%c(Bang1)
  uu%Typ(1) = ketTyp
  uu%Typ(2:typSiz) = vv%Typ(2:typSiz)
  end function


  subroutine ellBasis( l1,l2,l3,l4  )
!********************************************************************
! Input l1 and l2 should be light-like with l1.l2=/=0
! Output is  l3 = <l1+|gamma^mu|l2+> / 2
!            l4 = <l2+|gamma^mu|l1+> / 2
!********************************************************************
  intent(in ) :: l1,l2
  intent(out) :: l3,l4
  type(lorentz_type) :: l1,l2,l3,l4,uu,vv
  uu = braFmom(l1)
  vv = ketFmom(l2)
  l4%c(vec11) = vv%c(Kang1) * uu%c(Bsqr1)
  l4%c(vec12) = vv%c(Kang1) * uu%c(Bsqr2)
  l4%c(vec21) = vv%c(Kang2) * uu%c(Bsqr1)
  l4%c(vec22) = vv%c(Kang2) * uu%c(Bsqr2)
  l4%Typ = [momTyp,nulTyp,nulTyp]
  uu = ketFbra(uu)
  vv = braFket(vv)
  l3%c(vec11) = uu%c(Kang1) * vv%c(Bsqr1)
  l3%c(vec12) = uu%c(Kang1) * vv%c(Bsqr2)
  l3%c(vec21) = uu%c(Kang2) * vv%c(Bsqr1)
  l3%c(vec22) = uu%c(Kang2) * vv%c(Bsqr2)
  l3%Typ = [momTyp,nulTyp,nulTyp]
  end subroutine

  
  function xScalar_0() result(ee)
!*******************************************************************
!********************************************************************
  type(lorentz_type) :: ee
  ee%c(1) = cONE
  ee%c(2:oscSiz) = cZRO
  ee%Typ = [scaTyp,nulTyp,nulTyp]
  end function

  function xScalar_r(zz) result(ee)
!*******************************************************************
!********************************************************************
  intent(in) :: zz
  !(realknd2!) :: zz
  type(lorentz_type) :: ee
  ee%c(1) = zz
  ee%c(2:oscSiz) = cZRO
  ee%Typ = [scaTyp,nulTyp,nulTyp]
  end function

  function xScalar_c(zz) result(ee)
!*******************************************************************
!********************************************************************
  intent(in) :: zz
  !(complex2!) :: zz
  type(lorentz_type) :: ee
  ee%c(1) = zz
  ee%c(2:oscSiz) = cZRO
  ee%Typ = [scaTyp,nulTyp,nulTyp]
  end function


  function xVector_a( pp,qq,hel ) result(ee)
!*******************************************************************
!* Fills "ee" with polarization vector having helicity "hel"
!* constructed following Kleiss-Stirling from momentum "pp" and
!* reference momentum "qq"
!********************************************************************
  intent(in) :: pp,qq,hel
  type(lorentz_type) :: pp,qq,ee,uu,vv
  integer :: hel
  !(complex2!) :: zz
  if     (hel.eq. 1) then
    uu = braFmom(pp)
    vv = KangFmom(qq)
    zz = -rSQRT2/( uu%c(Bang1)*vv%c(Kang1) + uu%c(Bang2)*vv%c(Kang2) )
    ee%c(vec11) = vv%c(Kang1) * uu%c(Bsqr1) * zz
    ee%c(vec12) = vv%c(Kang1) * uu%c(Bsqr2) * zz
    ee%c(vec21) = vv%c(Kang2) * uu%c(Bsqr1) * zz
    ee%c(vec22) = vv%c(Kang2) * uu%c(Bsqr2) * zz
    ee%Typ = [vecTyp,nulTyp,nulTyp]
  elseif (hel.eq.-1) then
    uu = ketFmom(pp)
    vv = BsqrFmom(qq)
    zz = -rSQRT2/( vv%c(Bsqr1)*uu%c(Ksqr1) + vv%c(Bsqr2)*uu%c(Ksqr2) )
    ee%c(vec11) = uu%c(Kang1) * vv%c(Bsqr1) * zz
    ee%c(vec12) = uu%c(Kang1) * vv%c(Bsqr2) * zz
    ee%c(vec21) = uu%c(Kang2) * vv%c(Bsqr1) * zz
    ee%c(vec22) = uu%c(Kang2) * vv%c(Bsqr2) * zz
    ee%Typ = [vecTyp,nulTyp,nulTyp]
  elseif (hel.eq.10.or.hel.eq.9) then ! e=p/p0 for gauge check
    zz = 2/( pp%c(mom0p3) + pp%c(mom0m3) )
    ee = vecFmom(pp,zz)
  else
    ee = vecFmom(qq)
!  else ! e=q/||q|| for off-shell legs in HEF
!    zz = qq%c(mom0p3) + qq%c(mom0m3)
!    zz = 1/sqrt( zz*zz/2 - momSqr(qq) ) ! Euclidean length
!    ee = vecFmom(qq,zz)
  endif
  end function

  function xVector_b( pp,qq,q2,hel ) result(ee)
!*******************************************************************
!* For nonzero q2=qq.qq
!********************************************************************
  intent(in) :: pp,qq,q2,hel
  type(lorentz_type) :: pp,qq,ee,uu,vv
  integer :: hel
  !(complex2!) :: zz,q2
  zz = pp%c(mom0p3)*qq%c(mom0m3) - pp%c(mom1p2)*qq%c(mom1m2) &
     + qq%c(mom0p3)*pp%c(mom0m3) - qq%c(mom1p2)*pp%c(mom1m2)
  zz = q2/zz
  vv%c(1:momSiz) = qq%c(1:momSiz) - pp%c(1:momSiz)*zz
  vv%Typ = [momTyp,nulTyp,nulTyp]
  if     (hel.eq. 1) then
    uu = braFmom(pp)
    vv = KangFmom(vv)
    zz = -rSQRT2/( uu%c(Bang1)*vv%c(Kang1) + uu%c(Bang2)*vv%c(Kang2) )
    ee%c(vec11) = vv%c(Kang1) * uu%c(Bsqr1) * zz
    ee%c(vec12) = vv%c(Kang1) * uu%c(Bsqr2) * zz
    ee%c(vec21) = vv%c(Kang2) * uu%c(Bsqr1) * zz
    ee%c(vec22) = vv%c(Kang2) * uu%c(Bsqr2) * zz
    ee%Typ = [vecTyp,nulTyp,nulTyp]
  elseif (hel.eq.-1) then
    uu = ketFmom(pp)
    vv = BsqrFmom(vv)
    zz = -rSQRT2/( vv%c(Bsqr1)*uu%c(Ksqr1) + vv%c(Bsqr2)*uu%c(Ksqr2) )
    ee%c(vec11) = uu%c(Kang1) * vv%c(Bsqr1) * zz
    ee%c(vec12) = uu%c(Kang1) * vv%c(Bsqr2) * zz
    ee%c(vec21) = uu%c(Kang2) * vv%c(Bsqr1) * zz
    ee%c(vec22) = uu%c(Kang2) * vv%c(Bsqr2) * zz
    ee%Typ = [vecTyp,nulTyp,nulTyp]
  elseif (hel.eq.10.or.hel.eq.9) then ! e=p/p0 for gauge check
    zz = 2/( pp%c(mom0p3) + pp%c(mom0m3) )
    ee = vecFmom(pp,zz)
  else
    ee = vecFmom(qq)
!  else ! e=q/||q|| for off-shell legs in HEF
!    zz = 1/sqrt(abs(q2))
!    ee = vecFmom(qq,zz)
  endif
  end function

  function xVector_c( pp,mm,qq,hel ) result(ee)
!*******************************************************************
! Massive external vector
!********************************************************************
  intent(in) :: pp,qq,mm,hel
  type(lorentz_type) :: pp,qq,ee,pv,pw,vv,ww
  !(realknd2!) :: mm
  !(complex2!) :: zz
  integer :: hel
  if (mm.eq.rZRO) then
    ee = xVector_a( pp,qq,hel )
    return
  endif
  zz = pp%c(mom0p3)*qq%c(mom0m3) - pp%c(mom1p2)*qq%c(mom1m2) &
     + qq%c(mom0p3)*pp%c(mom0m3) - qq%c(mom1p2)*pp%c(mom1m2)
  zz = mm*mm/zz
  pw%Typ=[momTyp,nulTyp,nulTyp] ;pw%c(1:momSiz) = qq%c(1:momSiz)*zz
  pv%Typ=[momTyp,nulTyp,nulTyp] ;pv%c(1:momSiz) = pp%c(1:momSiz)-pw%c(1:momSiz)
  if (abs(hel).eq.1) then
    ee = xVector_a( pv,pw,hel )
  elseif (hel.eq.0) then
    vv = BsqrFmom(pv) 
    ww = BangFmom(pw)
    zz = 1/(vv%c(Bsqr1)*ww%c(Ksqr1) + vv%c(Bsqr2)*ww%c(Ksqr2)) !1 / < v+|w-> 
    pv%c(1:momSiz) = pv%c(1:momSiz)-pw%c(1:momSiz)
    ee = vecFmom(pv,zz)
  else
    ee = xVector_a( pp,qq,hel )
  endif
  end function


  function xSpinor_a( pp ,hel ,negIsKet ) result(uu)
!**************************************************************
! Massless external spinor
! returns a ket if negIsKet<=-1, else it returns a bra.
!      particle:  bra with p0>0  or  ket with p0<0
! anti-particle:  bra with p0<0  or  ket with p0>0
!**************************************************************
  intent(in) :: pp,hel,negIsKet
  type(lorentz_type) :: pp,uu
  integer :: hel
  integer :: negIsKet
!
!  if (negIsKet.gt.0) then
!    if     (hel.gt.0) then ;uu=BsqrFmom(pp)
!    elseif (hel.lt.0) then ;uu=BangFmom(pp)
!    else
!    endif
!  elseif (negIsKet.lt.0) then
!    if     (hel.gt.0) then ;uu=KangFmom(pp)
!    elseif (hel.lt.0) then ;uu=KsqrFmom(pp)
!    else
!    endif
!
  if     (hel*negIsKet.lt.0) then ;uu = BangFmom(pp)
  elseif (hel*negIsKet.gt.0) then ;uu = BsqrFmom(pp)
  else
    uu%c(1:spiSiz) = 0
    uu%Typ = [nulTyp,nulTyp,nulTyp]
    return
  endif
  if (negIsKet.le.-1) uu = ketFbra(uu)
  end function

  function xSpinor_b( pp ,mm,qq ,hel ,negIsKet ) result(uu)
!**************************************************************
! Massive external spinor
! Auxiliary momentum qq should be massless with pp.qq=/=0
!**************************************************************
  intent(in) :: pp,qq,mm,hel,negIsKet
  type(lorentz_type) :: pp,qq,uu,pv,pw,vv,ww
  !(realknd2!) :: mm
  !(complex2!) :: zz
  integer :: hel,negIsKet
  if (mm.eq.rZRO) then
    uu = xSpinor_a( pp ,hel ,negIsKet )
    return
  endif
  zz = mm*mm/momD2mom(pp,qq)
  pw%Typ=[momTyp,nulTyp,nulTyp] ;pw%c(1:momSiz) = qq%c(1:momSiz)*zz
  pv%Typ=[momTyp,nulTyp,nulTyp] ;pv%c(1:momSiz) = pp%c(1:momSiz)-pw%c(1:momSiz)
  if     (hel*negIsKet.lt.0) then
    vv = BangFmom(pv) 
    ww = braFmom(pw)
    zz = ( vv%c(Bang1)*ww%c(Bang2) - ww%c(Bang1)*vv%c(Bang2) )/mm ! <v|w> / m
    uu%c(Bang1) = vv%c(Bang1)
    uu%c(Bang2) = vv%c(Bang2)
    uu%c(Bsqr1) = zz*ww%c(Bsqr1)
    uu%c(Bsqr2) = zz*ww%c(Bsqr2)
    uu%Typ = [braTyp,nulTyp,nulTyp]
  elseif (hel*negIsKet.gt.0) then
    vv = BsqrFmom(pv) 
    ww = braFmom(pw)
    zz = ( ww%c(Bsqr1)*vv%c(Bsqr2) - vv%c(Bsqr1)*ww%c(Bsqr2) )/mm ! [v|w] / m
    uu%c(Bsqr1) = vv%c(Bsqr1)
    uu%c(Bsqr2) = vv%c(Bsqr2)
    uu%c(Bang1) = zz*ww%c(Bang1)
    uu%c(Bang2) = zz*ww%c(Bang2)
    uu%Typ = [braTyp,nulTyp,nulTyp]
  else
    uu%c(1:spiSiz) = 0
    uu%Typ = 0
    return
  endif
  if (negIsKet.le.-1) uu = ketFbra(uu)
  end function


!**************************************************************
  include 'ffvVertex.h90'
  include 'ffsVertex.h90'
!**************************************************************


  function gggVertex( p0 ,p1,e1 ,p2,e2 ,zz ) result(e0)
!**************************************************************
! Lorentz-part of the triple-gluon vertex
!**************************************************************
  intent(in) :: p0,p1,p2,e1,e2,zz
  type(lorentz_type) :: p0,p1,p2,e1,e2,e0
  !(complex2!) :: zz,p20(momSiz),p01(momSiz),e1e2,e1p20,e2p01,yy
  p20(mom0p3)=p2%c(mom0p3)+p0%c(mom0p3); p01(mom0p3)=p0%c(mom0p3)+p1%c(mom0p3)
  p20(mom0m3)=p2%c(mom0m3)+p0%c(mom0m3); p01(mom0m3)=p0%c(mom0m3)+p1%c(mom0m3)
  p20(mom1p2)=p2%c(mom1p2)+p0%c(mom1p2); p01(mom1p2)=p0%c(mom1p2)+p1%c(mom1p2)
  p20(mom1m2)=p2%c(mom1m2)+p0%c(mom1m2); p01(mom1m2)=p0%c(mom1m2)+p1%c(mom1m2)
  e1e2  = e1%c(vec0p3)*e2%c(vec0m3) - e1%c(vec1p2)*e2%c(vec1m2) &
        + e2%c(vec0p3)*e1%c(vec0m3) - e2%c(vec1p2)*e1%c(vec1m2) !/2
  e1p20 = e1%c(vec0p3)* p20(vec0m3) - e1%c(vec1p2)* p20(vec1m2) &
        +  p20(vec0p3)*e1%c(vec0m3) -  p20(vec1p2)*e1%c(vec1m2) !/2
  e2p01 = e2%c(vec0p3)* p01(vec0m3) - e2%c(vec1p2)* p01(vec1m2) &
        +  p01(vec0p3)*e2%c(vec0m3) -  p01(vec1p2)*e2%c(vec1m2) !/2
  e0%c(vec0p3) = (p1%c(vec0p3)-p2%c(vec0p3))*e1e2 + e2%c(vec0p3)*e1p20 - e1%c(vec0p3)*e2p01
  e0%c(vec0m3) = (p1%c(vec0m3)-p2%c(vec0m3))*e1e2 + e2%c(vec0m3)*e1p20 - e1%c(vec0m3)*e2p01
  e0%c(vec1p2) = (p1%c(vec1p2)-p2%c(vec1p2))*e1e2 + e2%c(vec1p2)*e1p20 - e1%c(vec1p2)*e2p01
  e0%c(vec1m2) = (p1%c(vec1m2)-p2%c(vec1m2))*e1e2 + e2%c(vec1m2)*e1p20 - e1%c(vec1m2)*e2p01
  yy = zz/2 ! here /2 for dot products
  e0%c(vec0p3) = e0%c(vec0p3)*yy
  e0%c(vec0m3) = e0%c(vec0m3)*yy
  e0%c(vec1p2) = e0%c(vec1p2)*yy
  e0%c(vec1m2) = e0%c(vec1m2)*yy
  e0%Typ = [vecTyp,nulTyp,nulTyp]
  end function

  function ggggVertex( e1,e2,e3 ,z1,z2,z3 ) result(e0)
!**************************************************************
! e0(nu) = e3(nu)*e1.e2*z1 + e2(nu)*e3.e1*z2 + e1(nu)*e2.e3*z3
!**************************************************************
  intent(in) :: e1,e2,e3,z1,z2,z3
  type(lorentz_type) :: e1,e2,e3,e0
  !(complex2!) :: z1,z2,z3,zz
  e0%c(vec0p3) = 0
  e0%c(vec0m3) = 0
  e0%c(vec1p2) = 0
  e0%c(vec1m2) = 0
  if (z3.ne.cZRO) then
    zz = e2%c(vec0p3)*e3%c(vec0m3) - e2%c(vec1p2)*e3%c(vec1m2) &
       + e3%c(vec0p3)*e2%c(vec0m3) - e3%c(vec1p2)*e2%c(vec1m2) !/2
    zz = zz*z3/2
    e0%c(vec0p3) = e0%c(vec0p3) + e1%c(vec0p3)*zz
    e0%c(vec0m3) = e0%c(vec0m3) + e1%c(vec0m3)*zz
    e0%c(vec1p2) = e0%c(vec1p2) + e1%c(vec1p2)*zz
    e0%c(vec1m2) = e0%c(vec1m2) + e1%c(vec1m2)*zz
  endif
  if (z2.ne.cZRO) then
    zz = e3%c(vec0p3)*e1%c(vec0m3) - e3%c(vec1p2)*e1%c(vec1m2) &
       + e1%c(vec0p3)*e3%c(vec0m3) - e1%c(vec1p2)*e3%c(vec1m2) !/2
    zz = zz*z2/2
    e0%c(vec0p3) = e0%c(vec0p3) + e2%c(vec0p3)*zz
    e0%c(vec0m3) = e0%c(vec0m3) + e2%c(vec0m3)*zz
    e0%c(vec1p2) = e0%c(vec1p2) + e2%c(vec1p2)*zz
    e0%c(vec1m2) = e0%c(vec1m2) + e2%c(vec1m2)*zz
  endif
  if (z1.ne.cZRO) then
    zz = e1%c(vec0p3)*e2%c(vec0m3) - e1%c(vec1p2)*e2%c(vec1m2) &
       + e2%c(vec0p3)*e1%c(vec0m3) - e2%c(vec1p2)*e1%c(vec1m2) !/2
    zz = zz*z1/2
    e0%c(vec0p3) = e0%c(vec0p3) + e3%c(vec0p3)*zz
    e0%c(vec0m3) = e0%c(vec0m3) + e3%c(vec0m3)*zz
    e0%c(vec1p2) = e0%c(vec1p2) + e3%c(vec1p2)*zz
    e0%c(vec1m2) = e0%c(vec1m2) + e3%c(vec1m2)*zz
  endif
  e0%Typ = [vecTyp,nulTyp,nulTyp]
  end function


!**************************************************************
  include 'fPropagator.h90'
!**************************************************************

  subroutine ePropagator( uu ,pp ,nn )
!**************************************************************
! Eikonal quark propagator  slash(n)/(2p.n) 
!**************************************************************
  type(lorentz_type) :: uu
  type(lorentz_type),intent(in) :: pp,nn
  type(lorentz_type) :: vv
  !(complex2!) :: zz
  zz = pp%c(mom0p3)*nn%c(mom0m3) - pp%c(mom1p2)*nn%c(mom1m2) &
     + nn%c(mom0p3)*pp%c(mom0m3) - nn%c(mom1p2)*pp%c(mom1m2) !/2
  zz = cIMA/zz
  select case(uu%Typ(1))
  case (braTyp)
    select case (uu%Typ(2))
    case (sqrTyp)
      uu%Typ(2) = angTyp
      uu%c(Bang1) = ( uu%c(Bsqr1)*nn%c(mom22) - uu%c(Bsqr2)*nn%c(mom21) )*zz
      uu%c(Bang2) = (-uu%c(Bsqr1)*nn%c(mom12) + uu%c(Bsqr2)*nn%c(mom11) )*zz
      uu%c(Bsqr1) = 0
      uu%c(Bsqr2) = 0
    case (angTyp)
      uu%Typ(2) = sqrTyp
      uu%c(Bsqr1) = ( uu%c(Bang1)*nn%c(mom11) + uu%c(Bang2)*nn%c(mom21) )*zz
      uu%c(Bsqr2) = ( uu%c(Bang1)*nn%c(mom12) + uu%c(Bang2)*nn%c(mom22) )*zz
      uu%c(Bang1) = 0
      uu%c(Bang2) = 0
    case (nulTyp)
      uu%Typ(2) = nulTyp
      vv%c(Bsqr1) = uu%c(Bang1)*nn%c(mom11) + uu%c(Bang2)*nn%c(mom21)
      vv%c(Bsqr2) = uu%c(Bang1)*nn%c(mom12) + uu%c(Bang2)*nn%c(mom22)
      vv%c(Bang1) = uu%c(Bsqr1)*nn%c(mom22) - uu%c(Bsqr2)*nn%c(mom21)
      vv%c(Bang2) =-uu%c(Bsqr1)*nn%c(mom12) + uu%c(Bsqr2)*nn%c(mom11)
      uu%c(1:spiSiz) = vv%c(1:spiSiz)*zz
    end select
  case (ketTyp)
    select case (uu%Typ(2))
    case (angTyp)
      uu%Typ(2) = sqrTyp
      uu%c(Ksqr1) =-( nn%c(mom22)*uu%c(Kang1) - nn%c(mom12)*uu%c(Kang2) )*zz
      uu%c(Ksqr2) =-(-nn%c(mom21)*uu%c(Kang1) + nn%c(mom11)*uu%c(Kang2) )*zz
      uu%c(Kang1) = 0
      uu%c(Kang2) = 0
    case (sqrTyp)
      uu%Typ(2) = angTyp
      uu%c(Kang1) =-( nn%c(mom11)*uu%c(Ksqr1) + nn%c(mom12)*uu%c(Ksqr2) )*zz
      uu%c(Kang2) =-( nn%c(mom21)*uu%c(Ksqr1) + nn%c(mom22)*uu%c(Ksqr2) )*zz
      uu%c(Ksqr1) = 0
      uu%c(Ksqr2) = 0
    case (nulTyp)
      uu%Typ(2) = nulTyp
      vv%c(Ksqr1) = nn%c(mom22)*uu%c(Kang1) - nn%c(mom12)*uu%c(Kang2)
      vv%c(Ksqr2) =-nn%c(mom21)*uu%c(Kang1) + nn%c(mom11)*uu%c(Kang2)
      vv%c(Kang1) = nn%c(mom11)*uu%c(Ksqr1) + nn%c(mom12)*uu%c(Ksqr2)
      vv%c(Kang2) = nn%c(mom21)*uu%c(Ksqr1) + nn%c(mom22)*uu%c(Ksqr2)
      uu%c(1:spiSiz) = vv%c(1:spiSiz)*(-zz)
    end select
  end select
  end subroutine


  subroutine gPropagator( uu ,pp ,nn ,xx )
!**************************************************************
! Massless vector boson.
! nn is gauge vector, xx is gauge parameter
!**************************************************************
  type(lorentz_type) :: uu
  type(lorentz_type),intent(in) :: pp,nn
  !(realknd2!),intent(in) :: xx
  !(complex2!) :: pn,pu,nu
  pn = pp%c(mom0p3)*nn%c(mom0m3) - pp%c(mom1p2)*nn%c(mom1m2) &
     + nn%c(mom0p3)*pp%c(mom0m3) - nn%c(mom1p2)*pp%c(mom1m2) !/2
  if (pn.eq.cZRO) then
    if (xx.lt.rONE) then
      pu = pp%c(mom0p3)*uu%c(vec0m3) - pp%c(mom1p2)*uu%c(vec1m2) &
         + uu%c(vec0p3)*pp%c(mom0m3) - uu%c(vec1p2)*pp%c(mom1m2) !/2
      uu%c(1:vecSiz) = uu%c(1:vecSiz) &
                     - pp%c(1:vecSiz)*( (1-xx)*pu/(2*pp%s) )
    endif
  else
    pu = pp%c(mom0p3)*uu%c(vec0m3) - pp%c(mom1p2)*uu%c(vec1m2) &
       + uu%c(vec0p3)*pp%c(mom0m3) - uu%c(vec1p2)*pp%c(mom1m2) !/2
    nu = nn%c(mom0p3)*uu%c(vec0m3) - nn%c(mom1p2)*uu%c(vec1m2) &
       + uu%c(vec0p3)*nn%c(mom0m3) - uu%c(vec1p2)*nn%c(mom1m2) !/2
    uu%c(1:vecSiz) = uu%c(1:vecSiz) &
                   + pp%c(1:vecSiz)*(( 2*(nn%s+xx*pp%s)*pu/pn - nu )/pn) &
                   - nn%c(1:vecSiz)*( pu/pn )
  endif
  uu%c(1:vecSiz) = uu%c(1:vecSiz)*(-cIMA/pp%s)
  end subroutine


  subroutine vPropagator( uu ,pp ,mm ,xx )
!**************************************************************
! Massive vector boson.
! mm is complex mass, xx is gauge parameter
! xx>1 is interpreted as xx=infinity
!**************************************************************
  type(lorentz_type) :: uu
  type(lorentz_type),intent(in) :: pp
  !(complex2!),intent(in) :: mm
  !(realknd2!),intent(in) :: xx
  !(complex2!) :: mSqr,pu
  mSqr = mm*mm
  if (xx.ne.rONE) then
    pu = pp%c(mom0p3)*uu%c(vec0m3) - pp%c(mom1p2)*uu%c(vec1m2) &
       + uu%c(vec0p3)*pp%c(mom0m3) - uu%c(vec1p2)*pp%c(mom1m2) !/2
    pu = pu/2
    if (xx.lt.rONE) then
      uu%c(1:vecSiz) = uu%c(1:vecSiz) &
                     - pp%c(1:vecSiz)*( pu*(1-xx)/(pp%s-xx*mSqr) )
    else
      uu%c(1:vecSiz) = uu%c(1:vecSiz) &
                     - pp%c(1:vecSiz)*( pu/mSqr )
    endif
  endif
  uu%c(1:vecSiz) = uu%c(1:vecSiz)*( -cIMA/(pp%s-mSqr) )
  end subroutine


  subroutine sPropagator( uu ,pp )
!**************************************************************
!**************************************************************
  type(lorentz_type) :: uu
  type(lorentz_type),intent(in) :: pp
  !(complex2!) :: pn
  uu%c(1) = uu%c(1)*( cIMA/pp%s )
  end subroutine
  


  subroutine planarMomenta( pp ,nn )
!**************************************************************
! Given a list of external momenta pp(ii,ii), calculate all
! internal momenta in the planar, ordered, configuration.
!  pp(n1,n2) = pp(n1,n1) + pp(n1+1,n1+1) + ... + pp(n2,n2)
!**************************************************************
  intent(inout) :: pp
  intent(in)    :: nn
  integer :: nn,ii,jj,n1,n2
  type(lorentz_type) :: pp(:,:)
  if (minval(ubound(pp)).lt.nn) then
    write(*,*) 'ERROR in planarMomenta: array too small'
    stop
  endif
  do ii=1,nn-1 ! ii=Ngluons-1
  do n1=1,nn-ii
    n2 = n1+ii
    jj = (n1+n2)/2
    pp(n1,n2)%c(1:momSiz) = pp(n1,jj)%c(1:momSiz) + pp(jj+1,n2)%c(1:momSiz)
    pp(n1,n2)%s = momSqr(pp(n1,n2))
    pp(n1,n2)%Typ = [momTyp,nulTyp,nulTyp]
  enddo
  enddo
  end subroutine


  subroutine gluonCurrents( aa ,pp ,nn ,gaugvec )
!**************************************************************
! Calculate all planar, ordered, purely gluonic currents.
!  aa(n1,n2) = current( gluon(n1),gluon(n1+1),...,gluon(n2) )
! On input, aa(ii,ii) should contain polvec(ii)
!**************************************************************
  intent(inout) :: aa
  intent(in)    :: pp,nn,gaugvec
  optional :: gaugvec
  integer :: nn,ii,n1,n2,j1,j2
  !(complex2!) :: z2,z3,z4,vv,va,pa,vp
  type(lorentz_type) :: aa(:,:),pp(:,:),gaugvec,tt
  logical :: axial
  if (minval(ubound(aa)).lt.nn) then
    write(*,*) 'ERROR in gluonCurrents: array too small'
    stop
  endif
! Couplings
  z2 = cIMA
  z3 = cIMA/rSQRT2 
  z4 = cIMA/2
! Gauge vector
  axial = .false.
  if (present(gaugvec)) then
    do ii=1,vecSiz ;axial=(axial.or.gaugvec%c(ii).ne.0) ;enddo
    vv = 2*momSqr(gaugvec)
  endif
! Calculate off-shell currents with more than 1 gluon
  do ii=1,nn-1 ! ii=Ngluons-1
  do n1=1,nn-ii
    n2 = n1+ii
! Initialize current
    aa(n1,n2)%c(1:vecSiz) = 0
    aa(n1,n2)%Typ = [vecTyp,nulTyp,nulTyp]
! Contribution 3-point vertex
    do j1=n1,n2-1
      tt = gggVertex( pp(n1,n2) ,pp(n1,j1),aa(n1,j1) ,pp(j1+1,n2),aa(j1+1,n2) ,z3 )
      aa(n1,n2)%c(1:vecSiz) = aa(n1,n2)%c(1:vecSiz) + tt%c(1:vecSiz)
    enddo
! Contribution 4-point vertex
    do j1=n1,n2-2
    do j2=j1+1,n2-1
      tt = ggggVertex( aa(n1,j1),aa(j1+1,j2),aa(j2+1,n2) ,-z4,2*z4,-z4 ) 
      aa(n1,n2)%c(1:vecSiz) = aa(n1,n2)%c(1:vecSiz) + tt%c(1:vecSiz)
    enddo
    enddo
! Multiply by propagator (except when all gluons are included)
    if (n1.ne.1.or.n2.ne.nn) then
      if (axial) then
        pa = momD2vec(pp(n1,n2),aa(n1,n2))
        va = momD2vec(gaugvec,aa(n1,n2))
        vp = momD2mom(gaugvec,pp(n1,n2))
        pa = pa/vp
        va = va/vp
!        write(*,*) 'vv',prnt(vv) !DEBUG
!        write(*,*) 'vp',prnt(vp) !DEBUG
!        write(*,*) 'pa',prnt(pa) !DEBUG
!        write(*,*) 'va',prnt(va) !DEBUG
        tt = vecFmom( pp(n1,n2),va-pa*vv/vp,gaugvec,pa )
        aa(n1,n2)%c(1:vecSiz) = aa(n1,n2)%c(1:vecSiz) - tt%c(1:vecSiz)
      endif
      aa(n1,n2)%c(1:vecSiz) = aa(n1,n2)%c(1:vecSiz)*(-z2/pp(n1,n2)%s)
    endif
  enddo
  enddo
  end subroutine

end module


