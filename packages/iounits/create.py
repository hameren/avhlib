#!/usr/bin/env python
#begin header
import os,sys
pythonDir = ''
sys.path.append(pythonDir)
from avh import *
thisDir,packDir,avhDir = cr.dirsdown(2)
packageName = os.path.basename(thisDir)
buildDir = os.path.join(avhDir,'build')
#end header

date = '10-07-2018'

lines = ['! From here the package "'+packageName+'", last edit: '+date+'\n']
cr.addfile(os.path.join(thisDir,'avh_iounits.f90'),lines,delPattern='^.*')

fpp.xpnd('pathsepr',os.path.sep,lines)

cr.wfile(os.path.join(buildDir,packageName+'.f90'),lines)
