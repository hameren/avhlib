module avh_iounits
  implicit none

  character(128),protected :: path_tbldir=&
  '!(path_tbldir!)'
 
  character(1),parameter :: pathsepr='!(pathsepr!)'
 
  integer,parameter :: stdin=5
  integer,parameter :: stdout=6
  integer,protected :: errru=6
  integer,protected :: warnu=6
  integer,protected :: messu=6
  integer,protected :: deffile=21
  integer,protected :: banner=stdout

  logical,save,private :: initd=.false.

contains

  subroutine avh_hello
  if (initd) return
  initd = .true.
  if (banner.ge.0) then
    write(banner,'(a72)') '########################################################################'
    write(banner,'(a72)') '# You are using the packages                                           #'
!IncludingTheModulesBeforeThisLine
    write(banner,'(a72)') '# from                                                                 #'
    write(banner,'(a72)') '#                         A Very Handy LIBrary                         #'
!IncludeProgramNameBeforeThisLine
    write(banner,'(a72)') '#                                                                      #'
    write(banner,'(a72)') '# author: Andreas van Hameren <hamerenREMOVETHIS@ifj.edu.pl>           #'
    write(banner,'(a72)') '#   date: 2018-10-07                                                   #'
!TheFirstReferenceIsNotYetHere
!IncludingReferencesBeforeThisLine
    write(banner,'(a72)') '########################################################################'
  endif
!
!  if (messu.ge.0) then
!    write(messu,'(A)') " MESSAGE from avhlib: path_tbldir="//trim(path_tbldir)
!  endif
  end subroutine

  subroutine set_unit( message ,nunit )
!**********************************************************************
!**********************************************************************
  character(*) ,intent(in) :: message
  integer      ,intent(in) :: nunit
  select case (message)
    case ('message') ;messu=nunit
    case ('warning') ;warnu=nunit
    case ('error'  ) ;errru=nunit
    case ('deffile') ;deffile=nunit
    case ('banner')  ;banner=nunit
    case default
      messu  = nunit
      warnu  = nunit
      errru  = nunit
  end select
  end subroutine


  function get_path( fileName ,option ) result(rslt)
  character(*),intent(in) :: fileName
  character(*),intent(in),optional :: option
  character(144) :: rslt,optVal
  optVal = 'file'
  if (present(option)) optVal = option
  select case (fileName)
  case ('avhlib.tbl')
    select case (trim(optVal))
    case ('dir'     ) ;rslt = trim(path_tbldir)
    case ('file'    ) ;rslt = trim(path_tbldir)//'avhlib.tbl'
    case ('pathName') ;rslt = 'path_tbldir'
    end select
  end select
  end function

  subroutine check_file( fileName )
  character(*),intent(in) :: fileName
  logical :: file_exists
  inquire( file=trim(get_path(fileName)), exist=file_exists )
  if (file_exists) return
  write(stdout,'(A)') "ERROR in avhlib:"
  write(stdout,'(A)') "  the file "//trim(get_path(fileName))
  write(stdout,'(A)') "  does not seem to exist. You can set the path to the directory"
  write(stdout,'(A)') "  where this file is with"
  write(stdout,'(A)') "    call set_path('"//trim(get_path(fileName,'pathName'))//"',yourPath)"
  write(stdout,'(A)') "  where for example  yourPath='/home/user/project/files/'"
  write(stdout,'(A)') "  It must include the final delimiter (the final slash in this example)!"
  write(stdout,*)
  stop
  end subroutine

  subroutine set_path(pathName,pathVal)
  character(*),intent(in) :: pathName,pathVal
  select case(pathName)
  case ('path_tbldir') ;path_tbldir = pathVal
  end select
  end subroutine

end module


