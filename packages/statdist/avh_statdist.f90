!!HEADER!!
!!
!! Copyright (C) 2011 Andreas van Hameren <hamerenREMOVETHIS@ifj.edu.pl>. 
!!
!! This file is part of StatDist.
!!
!! StatDist is free software: you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! StatDist is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with StatDist.  If not, see <http://www.gnu.org/licenses/>.
!!
!*******************************************************************************
! Usage:
!
!   use avh_StatDist
!  
!   type(StatDist_type) :: obj
!  
!   call obj%init(label='example')
!
!   do Iev=1,Nev
!     [generate data point x with weight w]
!     call obj%collect(x,w)
!   enddo
!  
!   write(*,*) 'result:',obj%ave(),' +/-',obj%sig()
!   call obj%dump()
!   call obj%plot(Nbins=100)
!
! Use gnuplot
!   gnuplot> plot "example.dist" w l
! for the distribution itself, and
!   gnuplot> plot "example.dist" u 3:4 w l
! for the distribution mapped on the unit interval.
!
! Histograms can be created with  histogram.f90  using dumped files.
!
! Optional input for subroutine StatDist_init:
!        label: character label for the distribution.
!        Ninit: number of events spent to determine the mean and the width
!               as measures of the position of the body of the distribution.
!               The default is 1000. The value of mean=m and width=w are used
!               to map (-infty,infty) on [0,1] via
!                 x -> y = 1/2/( 1 + |x-m|/w ) ;if (x>m) y=1-y ,
!                 y <- x = m + w*(y-1/2)/min(y,1-y) .
!        Nbins: number of bins into which [0,1] is divided to store the
!               distribution. The default is 2^10. The actual number is the
!               power of 2 closest to the number on input.
!         mean: instead of spending Ninit events, the mean and the width
!        width: can be given as input.
!     positive: logical. If put to .true., the distribution is assumed to be
!               zero on the negative axis. The bin with value-zero events is
!               not plotted in the histogram.
!       buffer: logical. If put to .true., the first Ninit events are stored,
!               and included in the distribution.
!
! Optional input for subroutine StatDist_plot:
!        iUnit: unit to which the histogram is written. The default is 21.
!        Nbins: number of bins in the plot. The default is the number of
!               bins given to StatDist_init.
!       option: by default, for each bin, the following four coordinates are
!               written to the file: the left border with zero height
!                                    the left border with bin height
!                                    the right border with bin height
!                                    the right border with zero height
!               The other options are:
!                 option=1: the middle with bin height
!                 option=2: the left border with bin height
!                 option=3: the right border with bin height
!                 option=4: the left and the right border with bin height
!    extension: characters appended to the label of the distribution to form
!               the file name to which the histogram is written.
!               The default is ".dist".
!
! Other routines:
!
!   subroutine obj%clean()
!     re-initialize all statistical quantities (so not Ninit, Nbins etc.).
!
!   function obj%integral(aa,bb)
!     returns the unnormalized integral of the distribution between aa and bb
!   
!   function obj%lTail(frac)
!     returns x such that  integral(-infty,x)=frac*integral(-infty,infty)
!                  
!   function obj%rTail(frac)
!     returns x such that  integral(x,infty)=frac*integral(-infty,infty)
!
!   subroutine obj%dump( iUnit ,extension )
!     dumps the distribution to a file.
!     The optional input iUnit is the unit to which the file is written.
!     The default is 21.
!     Optional input extension is appended to the label of the distribution
!     to form the file name. The default is ".dump".
!
!   subroutine obj%load( iUnit ,filename )
!     loads the contents of filename to obj.
!     Optional input iUnit is the unit from which the file is read.
!     The default is 21.
!
!   function obj%Nev()
!     returns the number of collected events.
!
!   function obj%ave()
!     returns the weighted average of the events.
!
!   function obj%sig()
!     returns the square-root of the (unbiased estimate of the) variance of
!     the weighted average of the events.
!      
!   function obj%NvalZero()
!     if positive=.true. in subroutine obj%init, this function returns
!     the total weight of the events with value zero.
!  
!   function obj%exp()
!     returns the (unbiased estimate of the) expectation value of the
!     distribution. It is equal to obj%ave(obj).
!  
!   function obj%var()
!     returns the (unbiased estimate of the) variance of the distribution.
!
! Notes:
!   For the functions  obj%ave(),obj%var(),obj%sig()  the following
!   definitions are used, which lead to the usual definitions for weight=1
!   events: Let us denote
!     W1 = sum( w_i     , i=1,Nevents )
!     W2 = sum( w_i*w_i , i=1,Nevents )
!     WV = W1*W1 - W2
!     X1 = sum( w_i*x_i     , i=1,Nevents )
!     X2 = sum( w_i*x_i*x_i , i=1,Nevents )
!     XV = W1*X2 - X1*X1
!   Then
!     StatDist_ave = X1/W1
!     StatDist_var = XV/WV
!     StatDist_sig = sqrt( XV/WV * W2/W1^2 )
!     
!   The quantity XV is not calculated as W1*X2 - X1*X1, but determined
!   cumulatively, following the recursive formula
!     XV{N+1} = ( (W1{N}+w{N+1})*XV{N} + w{N+1}*(X1{N}-x{N+1}*W1{N})^2 )/W1{N}
!   This way, possible cancellations happen for "linear quantities" X1-xNew*W1,
!   instead of for "quadratic quantities".
!
!   Also the quantity WV is calculated cumulatively, via the recursive formula
!     WV{N+1} = WV{N} + 2*w{N+1}*W1{N}
!
!*******************************************************************************

module avh_StatDist
  !(usekindmod!)
  use avh_mathcnst
  use avh_ioUnits
  use avh_ioTools
  use avh_prnt

  implicit none
  private
  public :: StatDist_type ,histogram_type
!2d  public :: StatDi2d_type ,histogr2d_type
  public :: operator(/),operator(*),operator(+),operator(-)

  integer ,parameter :: NbinDef=2**10
  integer ,parameter :: NinitDef=1000
  integer ,parameter :: labLen=24

  type :: StatDist_type
    private
    character(labLen) :: label=''
    logical      :: inactive=.true.
    !(realknd2!) :: Ninit=NinitDef
    logical      :: initPhase=.true.
    !(realknd2!) :: Npositv=0
    !(realknd2!) :: NnonPos=0
    !(realknd2!) :: Xzero=0
    !(realknd2!) :: W1=0
    !(realknd2!) :: W2=0
    !(realknd2!) :: WV=0
    !(realknd2!) :: X1=0
    !(realknd2!) :: XV=0
    !(realknd2!) :: e=0
    !(realknd2!) :: s=0
    !(realknd2!) :: Xmin=huge(1d0)
    !(realknd2!) :: Xmax=-huge(1d0)
    logical      :: NewXmin=.false.
    logical      :: NewXmax=.false.
    logical      :: positive=.false.
    integer      :: Nbin=NbinDef
    !(realknd2!) :: Norm=0
    !(realknd2!) ,allocatable :: BinVal(:,:)
    logical      :: Buffer=.false.
    !(realknd2!) ,allocatable :: xBuffer(:),wBuffer(:)
  contains
    procedure :: dump
    procedure :: load
    procedure :: clean
    procedure :: close
    procedure :: init
    procedure :: collect
    procedure :: NvalZero
    procedure :: exp
    procedure :: var
    procedure :: newMax
    procedure :: Nev
    procedure :: ave
    procedure :: sig
    procedure :: relErr
    procedure :: lTail
    procedure :: rTail
    procedure :: normal_integral
    procedure :: weighted_integral
    generic :: integral=>normal_integral,weighted_integral
    procedure :: plot
    procedure :: print
    procedure :: initializing
    procedure :: sum0
    procedure :: sum1
    procedure :: sum2
    procedure :: getMax
    procedure :: getParE
    procedure :: getParS
    procedure :: get_Norm
  end type

!2d  type :: statdi2d_type
!2d    private
!2d    type(statdist_type) :: sd(2)
!2d    logical      :: inactive=.true.
!2d    integer      :: Nbin(2)=256
!2d    !(realknd2!) ,allocatable :: BinVal(:,:,:)
!2d  contains
!2d    procedure :: dump=>    s2d_dump
!2d    procedure :: load=>    s2d_load
!2d    procedure :: clean=>   s2d_clean
!2d    procedure :: close=>   s2d_close
!2d    procedure :: init=>    s2d_init
!2d    procedure :: collect=> s2d_collect
!2d    procedure :: integral=>s2d_integral
!2d  end type

  type :: histogram_type
    private
    integer :: Nbins
    real(kind(1d0)),allocatable :: left(:),right(:),height(:),error(:)
  contains
    procedure :: alloc=>histogram_alloc
    procedure :: histogram_fill
    procedure :: histogram_fill_func
    generic :: fill=>histogram_fill,histogram_fill_func
    procedure :: histogram_setbins_e
    procedure :: histogram_setbins_d
    generic :: setbins=>histogram_setbins_e,histogram_setbins_d
    procedure :: optimal=>histogram_optimal
    procedure :: getbins=>histogram_getbins
    procedure :: write=>histogram_write
    procedure :: regularize=>histogram_regularize
    procedure :: normalize=>histogram_normalize
    procedure :: merge=>histogram_merge
    procedure :: norm=>histogram_norm
  end type

!2d  type :: histogr2d_type
!2d    private
!2d    integer :: Nbins(2)
!2d    real(kind(1d0)),allocatable :: left(:),right(:),low(:),upp(:)
!2d    real(kind(1d0)),allocatable :: height(:,:),error(:,:)
!2d  contains
!2d    procedure :: alloc=>histogr2d_alloc
!2d    procedure :: fill=>histogr2d_fill
!2d    procedure :: write=>histogr2d_write
!2d  end type

  interface operator (/)
    module procedure histogram_divide
    module procedure histogram_div_rh,histogram_div_hr
!2d    module procedure histogr2d_divide
!2d    module procedure histogr2d_div_rh,histogr2d_div_hr
  end interface
  interface operator (*)
    module procedure histogram_mlt_rh,histogram_mlt_hr
!2d    module procedure histogr2d_mlt_rh,histogr2d_mlt_hr
  end interface
  interface operator (+)
    module procedure histogram_add
!2d    module procedure histogr2d_add
  end interface
  interface operator (-)
    module procedure histogram_subtract
!2d    module procedure histogr2d_subtract
  end interface

  interface
    function integral_interface(xx) result(rslt)
    !(realknd2!),intent(in) :: xx
    !(realknd2!) :: rslt
    end function
  end interface

  logical,save :: initz=.true.

contains

  subroutine dump( obj ,iUnit ,extension ,prefix )
!**********************************************************************
! prefix must end with the slash/backslash
!**********************************************************************
  class(StatDist_type) :: obj
  integer       ,optional ,intent(in) :: iUnit
  character(*)  ,optional ,intent(in) :: extension ,prefix
  character(labLen+100) :: filename
  integer :: nunit
  if (obj%inactive) return
  if (present(iUnit)) then ;nunit=iUnit
  else                     ;nunit=deffile ;endif
  if (present(extension)) then
    if (present(prefix)) then
      filename = trim(prefix)//trim(obj%label)//trim(adjustl(extension))
    else
      filename = trim(obj%label)//trim(adjustl(extension))
    endif
  else
    if (present(prefix)) then
      filename = trim(prefix)//trim(obj%label)//'.dump'
    else
      filename = trim(obj%label)//'.dump'
    endif
  endif
  open(nunit,file=filename,status='replace')
  write(nunit,*) obj%label
  write(nunit,*) obj%inactive
  write(nunit,*) obj%Ninit
  write(nunit,*) obj%initPhase
  write(nunit,*) obj%Npositv
  write(nunit,*) obj%NnonPos
  write(nunit,*) obj%Xzero
  write(nunit,*) obj%W1
  write(nunit,*) obj%W2
  write(nunit,*) obj%WV
  write(nunit,*) obj%X1
  write(nunit,*) obj%XV
  write(nunit,*) obj%e
  write(nunit,*) obj%s
  write(nunit,*) obj%Xmin
  write(nunit,*) obj%Xmax
  write(nunit,*) obj%positive
  write(nunit,*) obj%Nbin
  write(nunit,*) obj%Norm
  write(nunit,*) obj%BinVal(1,0:obj%Nbin)
  write(nunit,*) obj%BinVal(2,0:obj%Nbin)
  close(nunit)
  end subroutine

  subroutine load( obj ,iUnit ,filename )
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  integer       ,optional ,intent(in) :: iUnit
  character(*)            ,intent(in) :: filename
  integer nunit
  if (initz) then
    initz = .false.
    call init_mathcnst
  endif
  if (present(iUnit)) then ;nunit=iUnit
  else                     ;nunit=deffile ;endif
  open(nunit,file=filename,status='old')
  read(nunit,*) obj%label      ! ;write(*,*) obj%label    
  read(nunit,*) obj%inactive   ! ;write(*,*) obj%inactive 
  read(nunit,*) obj%Ninit      ! ;write(*,*) obj%Ninit    
  read(nunit,*) obj%initPhase  ! ;write(*,*) obj%initPhase
  read(nunit,*) obj%Npositv    ! ;write(*,*) obj%Npositv  
  read(nunit,*) obj%NnonPos    ! ;write(*,*) obj%NnonPos  
  read(nunit,*) obj%Xzero      ! ;write(*,*) obj%Xzero    
  read(nunit,*) obj%W1         ! ;write(*,*) obj%W1       
  read(nunit,*) obj%W2         ! ;write(*,*) obj%W2       
  read(nunit,*) obj%WV         ! ;write(*,*) obj%WV       
  read(nunit,*) obj%X1         ! ;write(*,*) obj%X1       
  read(nunit,*) obj%XV         ! ;write(*,*) obj%XV       
  read(nunit,*) obj%e          ! ;write(*,*) obj%e        
  read(nunit,*) obj%s          ! ;write(*,*) obj%s        
  read(nunit,*) obj%Xmin       ! ;write(*,*) obj%Xmin     
  read(nunit,*) obj%Xmax       ! ;write(*,*) obj%Xmax     
  read(nunit,*) obj%positive   ! ;write(*,*) obj%positive 
  read(nunit,*) obj%Nbin       ! ;write(*,*) obj%Nbin     
  read(nunit,*) obj%Norm       ! ;write(*,*) obj%Norm     
  allocate(obj%BinVal(1:2,0:obj%Nbin))
  read(nunit,*) obj%BinVal(1,0:obj%Nbin)
  read(nunit,*) obj%BinVal(2,0:obj%Nbin)
  close(nunit)
  end subroutine


  subroutine clean( obj )
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  obj%initPhase =.true.
  obj%Npositv =0
  obj%NnonPos =0
  obj%Xzero   =0
  obj%W1      =0
  obj%W2      =0
  obj%WV      =0
  obj%X1      =0
  obj%XV      =0
  obj%e       =0
  obj%s       =0
  obj%Xmin = huge(rONE)
  obj%Xmax =-huge(rONE)
  obj%NewXmin = .false.
  obj%NewXmax = .false.
  obj%Norm = 0
  if (allocated(obj%BinVal)) obj%BinVal=0
  if (allocated(obj%xBuffer)) deallocate(obj%xBuffer)
  if (allocated(obj%wBuffer)) deallocate(obj%wBuffer)
  end subroutine


  subroutine close( obj )
  class(StatDist_type) :: obj
  obj%label=''
  obj%inactive=.true.
  obj%Ninit=NinitDef
  obj%initPhase=.true.
  obj%Npositv=0
  obj%NnonPos=0
  obj%Xzero=0
  obj%W1=0
  obj%W2=0
  obj%WV=0
  obj%X1=0
  obj%XV=0
  obj%e=0
  obj%s=0
  obj%Xmin= huge(obj%Xmin)
  obj%Xmax=-huge(obj%Xmax)
  obj%NewXmin = .false.
  obj%NewXmax = .false.
  obj%positive=.false.
  obj%Nbin=NbinDef
  obj%Norm=0
  obj%Buffer=.false.
  if (allocated(obj%BinVal)) deallocate(obj%BinVal)
  if (allocated(obj%xBuffer)) deallocate(obj%xBuffer)
  if (allocated(obj%wBuffer)) deallocate(obj%wBuffer)
  end subroutine


  subroutine init( obj,label,Ninit,Nbins,mean,width,positive,buffer )
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  character(*),optional ,intent(in) :: label
  integer     ,optional ,intent(in) :: Nbins,Ninit
  !(realknd2!),optional ,intent(in) :: mean,width
  logical     ,optional ,intent(in) :: positive,buffer
  integer :: ii
!
  if (initz) then
    initz = .false.
    call init_mathcnst
  endif
!
  obj%inactive = .false.
!
  if (present(label)) obj%label = adjustl(label)
!
  if (present(Ninit)) then
    if (Ninit.gt.0) obj%Ninit=Ninit
  endif
!
  if (present(Nbins)) then
    if (0.lt.Nbins) then
      ii=1 ;do ;ii=ii*2 ;if (Nbins/ii.le.1) exit ;enddo ;ii=ii*2
      if (abs(Nbins-ii).gt.abs(Nbins-ii/2)) then ;obj%Nbin=ii/2
      else                                       ;obj%Nbin=ii ;endif
    else
      obj%Nbin = NbinDef
    endif
  else
    obj%Nbin = NbinDef
  endif
!
  if (present(buffer)) then
    if (buffer) obj%buffer=.true.
  else
    obj%buffer=.false.
  endif
!
  if (present(mean).or.present(width)) then
    if (.not.present(mean).or..not.present(width)) then
      if (errru.ge.0) write(errru,*) 'ERROR in StatDist_init: ' &
        ,'mean and width must BOTH be specified, or BOTH not be specified'
      stop
    endif
    if (present(Ninit)) then
      if (warnu.ge.0) write(warnu,*) 'WARNING from StatDist_init: ' &
        ,'realize that Ninit is overruled by specifying mean,width'
    endif
    if (present(buffer)) then
      if (buffer) then
        if (warnu.ge.0) write(warnu,*) 'WARNING from StatDist_init: ' &
          ,'realize that buffer is overruled by specifying mean,width'
      endif
    endif
    if (width.le.rZRO) then
      if (errru.ge.0) write(errru,*) 'ERROR in StatDist_init: ' &
        ,'width must be positive'
      stop
    endif
    obj%e = mean
    obj%s = width
    obj%Ninit = 0
    obj%Buffer = .false.
  endif
!
  if (present(positive)) then
    if (positive) obj%positive=.true.
  else
    obj%positive=.false.
  endif
!
  if (allocated(obj%BinVal)) deallocate(obj%BinVal)
!
  call obj%clean()
  end subroutine


  subroutine collect( obj ,xx ,ww_in )
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!)        ,intent(in   ) :: xx,ww_in
  optional :: ww_in
  !(realknd2!) :: ww,yy,vv,yRight
  integer :: i0,i1,ii,jj
!
  if (obj%inactive) return
!
  if (.not.allocated(obj%BinVal)) then
    allocate(obj%BinVal(1:2,0:obj%Nbin))
    obj%BinVal = 0
  endif
!
  if (obj%Buffer) then
    if (.not.allocated(obj%xBuffer)) then
      allocate(obj%xBuffer(1:int(obj%Ninit))) ;obj%xBuffer=0
      allocate(obj%wBuffer(1:int(obj%Ninit))) ;obj%wBuffer=0
    endif
  endif
!
  if (present(ww_in)) then
    if (ww_in.le.rZRO) then
      obj%NnonPos = obj%NnonPos+1
      if (ww_in.eq.rZRO) return
    endif
    ww = ww_in
  else 
    ww = 1
  endif
!
  obj%Npositv = obj%Npositv+1
  if (xx.eq.rZRO) obj%Xzero = obj%Xzero+ww
  if (obj%W1.eq.rZRO) then
    obj%W1 = ww
    obj%W2 = ww*ww
    obj%WV = 0
    obj%X1 = ww*xx
    obj%XV = 0
  else
    obj%XV = ( (obj%W1+ww)*obj%XV + ww*(obj%X1-xx*obj%W1)**2 )/obj%W1
    obj%X1 = obj%X1 + ww*xx
    obj%WV = obj%WV + 2*ww*obj%W1
    obj%W1 = obj%W1 + ww
    obj%W2 = obj%W2 + ww*ww
  endif
!
  obj%NewXmin = (xx.lt.obj%Xmin) ;if (obj%NewXmin) obj%Xmin=xx
  obj%NewXmax = (xx.gt.obj%Xmax) ;if (obj%NewXmax) obj%Xmax=xx
!
  if (obj%initPhase) then
    if (obj%Buffer) then
      obj%xBuffer(int(obj%Npositv)) = xx
      obj%wBuffer(int(obj%Npositv)) = ww
    endif
    if (obj%Npositv.eq.obj%Ninit) then
      obj%initPhase = .false.
      obj%e = obj%X1/obj%W1
      obj%s = obj%XV/obj%WV
      if (obj%s.le.rZRO) then
        if (warnu.gt.0) write(warnu,*) 'WARNING from StatDist_collect for ' &
          ,trim(obj%label),': non-positive variance, increasing Ninit'
        obj%Ninit = 2*obj%Ninit
        obj%initPhase = .true.
        !obj%inactive = .true.
        return
      endif
      obj%s = sqrt(obj%s)
      if (obj%positive) then
        if (obj%e.lt.rZRO) then
          if (warnu.gt.0) write(warnu,*) 'WARNING from StatDist_collect for ' &
            ,trim(obj%label),': negative expectation value for positive quantity, ' &
            ,'increasing Ninit'
          obj%Ninit = 2*obj%Ninit
          obj%initPhase = .true.
          !obj%inactive = .true.
          return
        endif
      endif
      if (obj%Buffer) then
        do jj=1,int(obj%Ninit)
          call findBin( obj ,ii,yy ,obj%xBuffer(jj) )
          obj%BinVal(1,ii) = obj%BinVal(1,ii)+obj%wBuffer(jj)
          obj%BinVal(2,ii) = obj%BinVal(2,ii)+obj%wBuffer(jj)**2
          obj%Norm = obj%Norm+obj%wBuffer(jj)
        enddo
        deallocate(obj%xBuffer,obj%wBuffer)
        allocate(obj%xBuffer(1:0)) ! allocate with size zero
      else
        obj%Npositv = 0
        obj%NnonPos = 0
        obj%Xzero = 0
        obj%W1 = 0
        obj%W2 = 0
        obj%WV = 0
        obj%X1 = 0
        obj%XV = 0
!        obj%Xmin = huge(obj%Xmin)
!        obj%Xmax =-huge(obj%Xmax)
      endif
    endif
  else
    call findBin( obj ,ii,yy ,xx )
    obj%BinVal(1,ii) = obj%BinVal(1,ii)+ww
    obj%BinVal(2,ii) = obj%BinVal(2,ii)+ww*ww
    obj%Norm = obj%Norm+ww
  endif
!
  end subroutine


  function getParE( obj ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!) :: rslt
  rslt = obj%e
  end function

  function getParS( obj ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!) :: rslt
  rslt = obj%s
  end function

  function initializing( obj ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  logical :: rslt
  rslt = obj%initPhase
  end function

  function NvalZero( obj ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!) :: rslt
  rslt = obj%Xzero
  end function

  function get_Norm( obj ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!) :: rslt
  rslt = obj%Norm
  end function

  function exp( obj ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!) :: rslt
  if (obj%W1.ne.rZRO) then
    rslt = obj%X1/obj%W1
  else
    rslt = tiny(rONE)
  endif
  end function

  function var( obj ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!) :: rslt
  if (obj%WV.ne.rZRO) then
    rslt = obj%XV/obj%WV
  else
    rslt = tiny(rONE)
  endif
  end function

  function getMax( obj ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!) :: rslt
  rslt = obj%Xmax
  end function

  function newMax( obj ,xMax ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!)       ,intent(out) :: xMax
  logical :: rslt
  rslt = obj%NewXmax.and..not.obj%initPhase
  xMax = obj%Xmax
  end function

  function Nev( obj ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!) :: rslt
  rslt = obj%Npositv + obj%NnonPos
  end function

  function ave( obj ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!) :: rslt
  if (obj%W1.ne.rZRO) then
    rslt = obj%X1/obj%W1
  else
    rslt = tiny(rONE)
  endif
  end function

  function sig( obj ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!) :: rslt
  rslt = obj%WV*obj%W1**2
  if (rslt.ne.rZRO) then
    rslt = obj%XV*obj%W2/rslt
    if (rslt.gt.rZRO) then
      rslt = sqrt(rslt)
    else
      rslt = tiny(rONE)
    endif
  else
    rslt = tiny(rONE)
  endif
  end function

  function relErr( obj ) result(rslt)
!**********************************************************************
! sig/ave
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!) :: rslt
  rslt = obj%WV*obj%X1**2
  if (rslt.ne.rZRO) then
    rslt = obj%XV*obj%W2/rslt
    if (rslt.gt.rZRO) then
      rslt = sqrt(rslt)
    else
      rslt = tiny(rONE)
    endif
  else
    rslt = tiny(rONE)
  endif
  end function

  function sum0( obj ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!) :: rslt
  rslt = obj%W1
  end function

  function sum1( obj ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!) :: rslt
  rslt = obj%X1
  end function

  function sum2( obj ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!) :: rslt
  if (obj%W1.ne.rZRO) then
    rslt = (obj%XV+obj%X1**2)/obj%W1
  else
    rslt = tiny(rONE)
  endif
  end function


  function lTail( obj ,frac ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!)          ,intent(in) :: frac
  !(realknd2!) :: rslt ,volume,binSum,yy
  integer :: ii
  if (obj%initPhase) then
    rslt = obj%Xmin
    return
  endif
  volume = frac*obj%Norm
  binSum = 0
  do ii=0,obj%Nbin
    binSum = binSum + obj%binVal(1,ii)
    if (binSum.ge.volume) exit
  enddo
  ii = min(ii,obj%Nbin)
  yy = ii + (volume-binSum)/obj%binVal(1,ii)
  yy = max(yy,rZRO)
  yy = yy/obj%Nbin
  rslt = obj%e + obj%s*(yy-rHLF)/min(yy,1-yy)
  end function


  function rTail( obj ,frac ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!)          ,intent(in) :: frac
  !(realknd2!) :: rslt ,volume,binSum,yy
  integer :: ii
  if (obj%initPhase) then
    rslt = obj%Xmax
    return
  endif
  volume = frac*obj%Norm
  binSum = 0
  do ii=obj%Nbin,0,-1
    binSum = binSum + obj%binVal(1,ii)
    if (binSum.ge.volume) exit
  enddo
  yy = (ii-1) - (volume-binSum)/obj%binVal(1,ii)
  yy = max(yy,rZRO)
  yy = yy/obj%Nbin
  rslt = obj%e + obj%s*(yy-rHLF)/min(yy,1-yy)
  end function


  function normal_integral( obj ,aa,bb ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!)          ,intent(in) :: aa,bb
  !(realknd2!) :: rslt(2) ,ta,tb,ya,yb ,left(2),bulk(2),right(2)
  integer :: ia,ib
  if (obj%initPhase) then
    rslt = 0
    return
  endif
!
  if (bb.le.aa) then ;rslt=0 ;return ;endif
!
  if (obj%positive.and.aa.lt.rZRO) then
    if     (bb.lt.rZRO) then
      rslt = 0
      return
    elseif (bb.eq.rZRO) then
      rslt(:) = obj%BinVal(:,0)
      return
    else
      ia = 0
      left(:) = obj%BinVal(:,ia)
    endif
  else
    call findBin( obj ,ia,ya ,aa )
    left(:) = ( ia - ya*obj%Nbin )*obj%BinVal(:,ia)
  endif
  call findBin( obj ,ib,yb ,bb )
!
  bulk(1) = sum( obj%BinVal(1,ia+1:ib-1) )
  bulk(2) = sum( obj%BinVal(2,ia+1:ib-1) )
  right(:) = ( yb*obj%Nbin - (ib-1) )*obj%BinVal(:,ib)
!
  rslt = (left+bulk+right)
!
  end function


  function weighted_integral( obj ,aa,bb ,func ,option ) result(rslt)
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  !(realknd2!),intent(in) :: aa,bb
  procedure(integral_interface) :: func
  integer,intent(in) :: option
  !(realknd2!) :: rslt(2) ,yy,ff,xLeft,xRght,rNbin,ya,yb
  integer :: ia,ib,ii
  if (obj%initPhase) then
    rslt = 0
    return
  endif
!
  if (bb.le.aa) then ;rslt=0 ;return ;endif
!
  rNbin = obj%Nbin
!
  rslt = 0
!
  call findBin( obj ,ia,ya ,aa )
  call findBin( obj ,ib,yb ,bb )
!
  select case (option)
!
  case default ! function evaluated at middle of y-bins, in [0,1]
    if (obj%BinVal(2,ia).gt.rZRO) then
      yy = ( ya + ia/rNbin )/2
      ff = func( obj%e + obj%s*(yy-rHLF)/min(yy,1-yy) )
      rslt(1) = rslt(1) + ( ia - ya*rNbin )*obj%BinVal(1,ia)*ff
      rslt(2) = rslt(2) + ( ia - ya*rNbin )*obj%BinVal(2,ia)*ff*ff
    endif
!  
    do ii=ia+1,ib-1
      if (obj%BinVal(2,ii).gt.rZRO) then
        yy = (ii-rHLF)/rNbin
        ff = func( obj%e + obj%s*(yy-rHLF)/min(yy,1-yy) )
        rslt(1) = rslt(1) + obj%BinVal(1,ii)*ff
        rslt(2) = rslt(2) + obj%BinVal(2,ii)*ff*ff
      endif
    enddo
!  
    if (obj%BinVal(2,ib).gt.rZRO) then
      yy = ( (ib-1)/rNbin + yb )/2
      ff = func( obj%e + obj%s*(yy-rHLF)/min(yy,1-yy) )
      rslt(1) = rslt(1) + ( yb*rNbin - (ib-1) )*obj%BinVal(1,ib)*ff
      rslt(2) = rslt(2) + ( yb*rNbin - (ib-1) )*obj%BinVal(2,ib)*ff*ff
    endif
!
  case (1) ! function evaluated at middle of x-bins, in (-infty,infty)
    xLeft = aa
    yy = ia/rNbin
    xRght = obj%e + obj%s*(yy-rHLF)/min(yy,1-yy)
    if (obj%BinVal(2,ia).gt.rZRO) then
      ff = func((xLeft+xRght)/2)
      rslt(1) = rslt(1) + ( ia - ya*rNbin )*obj%BinVal(1,ia)*ff
      rslt(2) = rslt(2) + ( ia - ya*rNbin )*obj%BinVal(2,ia)*ff*ff
    endif
!  
    do ii=ia+1,ib-1
      xLeft = xRght
      yy = ii/rNbin
      xRght = obj%e + obj%s*(yy-rHLF)/min(yy,1-yy)
      if (obj%BinVal(2,ii).gt.rZRO) then
        ff = func((xLeft+xRght)/2)
        rslt(1) = rslt(1) + obj%BinVal(1,ii)*ff
        rslt(2) = rslt(2) + obj%BinVal(2,ii)*ff*ff
      endif
    enddo
!  
    xLeft = xRght
    xRght = bb
    if (obj%BinVal(2,ib).gt.rZRO) then
      ff = func((xLeft+xRght)/2)
      rslt(1) = rslt(1) + ( yb*rNbin - (ib-1) )*obj%BinVal(1,ib)*ff
      rslt(2) = rslt(2) + ( yb*rNbin - (ib-1) )*obj%BinVal(2,ib)*ff*ff
    endif
!
  end select
  end function


  subroutine plot( obj ,iUnit ,Nbins ,option ,extension )
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  integer     ,optional ,intent(in) :: iUnit,Nbins,option
  character(*),optional ,intent(in) :: extension
  character(labLen+100) :: filename
  integer :: nUnit,NbinPlot,ii,ja,jb,choice
  !(realknd2!) :: aa,bb,height,xa,xb,xHeight
!
  if (obj%inactive) return
!
  if (present(iUnit)) then ;nUnit=iUnit
  else                     ;nUnit=deffile ;endif
!
  if (present(Nbins)) then
    if (0.lt.Nbins) then
      ii=1 ;do ;ii=ii*2 ;if (Nbins/ii.le.1) exit ;enddo ;ii=ii*2
      if (abs(Nbins-ii).gt.abs(Nbins-ii/2)) then ;NbinPlot=ii/2
      else                                       ;NbinPlot=ii ;endif
    else
      NbinPlot = obj%Nbin
    endif
  else
    NbinPlot = obj%Nbin
  endif
!
  if (present(option)) then ;choice=option
  else                      ;choice=0 ;endif
!
  if (present(extension)) then
    filename = trim(obj%label)//trim(adjustl(extension))
  else
    filename = trim(obj%label)//'.dist'
  endif
  open(nunit,file=filename,status='replace')
  write(nunit,'(A)') '# Collumn 3:4 contain the distribution of collumn 1:2 mapped to the'
  write(nunit,'(A)') '# unit interval via  x -> y = 1/2/( 1 + |x-m|/w ) ;if (x>m) y=1-y'
  write(nunit,'(a10,e23.16,a4,e23.16)') '# with  m=',obj%e,', w=',obj%s
  if (choice.eq.4) write(nunit,'(3e15.8)') rZRO,rZRO
!
  jb=0
  bb=0
!
  do ii=1,NbinPlot
    ja = jb
    jb = (ii*obj%Nbin)/NbinPlot
    aa = bb
    bb = ii*rONE/NbinPlot
    xa = obj%e + obj%s*(aa-rHLF)/min(aa,1-aa) ;xa=max(xa,obj%Xmin)
    xb = obj%e + obj%s*(bb-rHLF)/min(bb,1-bb) ;xb=min(xb,obj%Xmax)
    if (xa.ge.xb) cycle
    height  = sum(obj%BinVal(1,ja+1:jb))/obj%Norm
    if (abs(height).lt.1.d-99) height=0
    xHeight = height/(xb-xa)
    height  = height*NbinPlot
    if     (choice.eq.1) then
      write(nunit,'(4e15.8)') (xa+xb)/2,xHeight,(aa+bb)/2,height
    elseif (choice.eq.2) then
      write(nunit,'(4e15.8)') xa,xHeight ,aa,height
    elseif (choice.eq.3) then           
      write(nunit,'(4e15.8)') xb,xHeight ,bb,height
    elseif (choice.eq.4) then           
      write(nunit,'(4e15.8)') xa,xHeight ,aa,height
      write(nunit,'(4e15.8)') xb,xHeight ,bb,height
    else                                           
      write(nunit,'(4e15.8)') xa,rZRO    ,aa,rZRO  
      write(nunit,'(4e15.8)') xa,xHeight ,aa,height
      write(nunit,'(4e15.8)') xb,xHeight ,bb,height
      write(nunit,'(4e15.8)') xb,rZRO    ,bb,rZRO  
    endif
  enddo
!
  if (choice.eq.4) write(nunit,'(3e15.8)') rONE,rZRO
  close(nunit)
!
  end subroutine


  subroutine findBin( obj ,ii,yy ,xx )
!**********************************************************************
!**********************************************************************
  class(StatDist_type),intent(in ) :: obj
!  type(StatDist_type),intent(in ) :: obj
  integer            ,intent(out) :: ii
  !(realknd2!)       ,intent(out) :: yy
  !(realknd2!)       ,intent(in ) :: xx
  yy = 1/(1+abs(xx-obj%e)/obj%s)/2 ;if (xx.gt.obj%e) yy=1-yy
  if (obj%positive.and.xx.le.rZRO) then ;ii=0 ;return ;endif
  ii = 1+int(yy*obj%Nbin)  
  if (ii.gt.obj%Nbin) ii=obj%Nbin
  end subroutine


  subroutine notANumber(xx,comment)
  !(realknd2!) ,intent(in) :: xx
  character(*) ,intent(in) :: comment
  if (.not.xx.le.rZRO.and..not.xx.ge.rZRO) then
    write(*,*) comment,'  ',xx
    stop
  endif
  end subroutine

  subroutine print( obj ,iUnit )
!**********************************************************************
!**********************************************************************
  class(StatDist_type) :: obj
  integer       ,optional ,intent(in) :: iUnit
  integer :: nunit
  nunit = stdout
  if (present(iUnit)) nunit=iUnit
  write(nunit,*) 'label    ',obj%label
  write(nunit,*) 'inactive ',obj%inactive
  write(nunit,*) 'Ninit    ',obj%Ninit
  write(nunit,*) 'initPhase',obj%initPhase
  write(nunit,*) 'Npositv  ',obj%Npositv
  write(nunit,*) 'NnonPos  ',obj%NnonPos
  write(nunit,*) 'Xzero    ',obj%Xzero
  write(nunit,*) 'W1       ',obj%W1
  write(nunit,*) 'W2       ',obj%W2
  write(nunit,*) 'WV       ',obj%WV
  write(nunit,*) 'X1       ',obj%X1
  write(nunit,*) 'XV       ',obj%XV
  write(nunit,*) 'e        ',obj%e
  write(nunit,*) 's        ',obj%s
  write(nunit,*) 'Xmin     ',obj%Xmin
  write(nunit,*) 'Xmax     ',obj%Xmax
  write(nunit,*) 'positive ',obj%positive
  write(nunit,*) 'Nbin     ',obj%Nbin
  write(nunit,*) 'Norm     ',obj%Norm     
  end subroutine


  subroutine histogram_alloc( hst ,Nbins )
!***********************************************************************
!***********************************************************************
  class(histogram_type),intent(out) :: hst
  integer,intent(in) :: Nbins
  hst%Nbins = Nbins
  allocate( hst%left(  Nbins) )
  allocate( hst%right( Nbins) )
  allocate( hst%height(Nbins) )
  allocate( hst%error( Nbins) )
  end subroutine
  
  subroutine histogram_dalloc( hst )
  class(histogram_type),intent(out) :: hst
  hst%Nbins = 0
  if (allocated(hst%left  )) deallocate( hst%left   )
  if (allocated(hst%right )) deallocate( hst%right  )
  if (allocated(hst%height)) deallocate( hst%height )
  if (allocated(hst%error )) deallocate( hst%error  )
  end subroutine
  

  subroutine histogram_fill( hst ,inputFiles &
                 ,leftBorder ,rightBorder ,numberOfBins &
                 ,overallFactor ,shift ,schaal )
!***********************************************************************
! inputFiles is a single character string that may contain several
! space-separated file names.
!***********************************************************************
  class(histogram_type),intent(out) :: hst
  character(*)         ,intent(in) :: inputFiles
  real(kind(1d0))      ,intent(in) :: leftBorder,rightBorder
  integer              ,intent(in) :: numberOfBins
  real(kind(1d0)),optional,intent(in) :: overallFactor,shift,schaal
  type(StatDist_type),allocatable :: obj(:)
  real(kind(1d0)) :: aa,bb,cc,dd,step,Norm,Ntot,hh(2),factor
  integer :: nFiles,iFile,iBin
  type(charLine_type) :: files
!
  factor = 1
  if (present(overallFactor)) factor = overallFactor
  cc = 0
  if (present(shift)) cc = shift
  dd = 1
  if (present(schaal)) dd = schaal
!
  files = put_charLine( inputFiles )
  nFiles = files%Nwords()
  allocate( obj(1:nFiles) )
!
  do iFile=1,nFiles
    call obj(iFile)%load( iUnit=deffile ,filename=files%word(iFile) )
  enddo
!  
  Ntot = 0
  Norm = 0
  do iFile=1,nFiles
    Ntot = Ntot + obj(iFile)%Nev()
    Norm = Norm + obj(iFile)%get_Norm()
  enddo
  Norm = Norm/Ntot
! 
  step = (rightBorder-leftBorder)/numberOfBins
!
  call hst%alloc( numberOfBins )
! 
  do iBin=1,numberOfBins
    aa = leftBorder + step*(iBin-1)
    bb = aa + step
    hh = 0
    if (dd.gt.0) then
      do iFile=1,nFiles
        hh = hh + obj(iFile)%integral((aa-cc)/dd,(bb-cc)/dd)
      enddo
    else
      do iFile=1,nFiles
        hh = hh + obj(iFile)%integral((bb-cc)/dd,(aa-cc)/dd)
      enddo
    endif
    hh(1) = hh(1)/Ntot
    hh(2) = hh(2)/Ntot - hh(1)**2
    if (hh(2).le.0.or.Ntot.le.1) then ;hh(2) = abs( hh(1) )
                                 else ;hh(2) = sqrt( hh(2)/(Ntot-1) )
    endif
    hh = hh/step
    if (factor.eq.0) then
      hh = hh/Norm
    else
      hh = hh*factor
    endif
    hst%left(  iBin) = aa
    hst%right( iBin) = bb
    hst%height(iBin) = hh(1)
    hst%error( iBin) = hh(2)
  enddo
!
  end subroutine


  subroutine histogram_fill_func( hst ,inputFiles &
                 ,leftBorder ,rightBorder ,numberOfBins &
                 ,weightFunction )
!***********************************************************************
! inputFiles is a single character string that may contain several
! space-separated file names.
!***********************************************************************
  class(histogram_type),intent(out) :: hst
  character(*)         ,intent(in) :: inputFiles
  real(kind(1d0))      ,intent(in) :: leftBorder,rightBorder
  integer              ,intent(in) :: numberOfBins
  procedure(integral_interface) :: weightFunction
  type(StatDist_type),allocatable :: obj(:)
  real(kind(1d0)) :: aa,bb,step,Ntot,hh(2)
  integer :: nFiles,iFile,iBin,intOption
  type(charLine_type) :: files
!
  intOption=0 !;if (present(option)) intOption=option
!
  files = put_charLine( inputFiles )
  nFiles = files%Nwords()
  allocate( obj(1:nFiles) )
!
  do iFile=1,nFiles
    call obj(iFile)%load( iUnit=deffile ,filename=files%word(iFile) )
  enddo
!  
  Ntot = 0
  do iFile=1,nFiles
    Ntot = Ntot + obj(iFile)%Nev()
  enddo
! 
  step = (rightBorder-leftBorder)/numberOfBins
!
  call hst%alloc( numberOfBins )
! 
  do iBin=1,numberOfBins
    aa = leftBorder + step*(iBin-1)
    bb = aa + step
    hh = 0
    do iFile=1,nFiles
      hh = hh + obj(iFile)%integral(aa,bb,weightFunction,intOption)
    enddo
    hh(1) = hh(1)/Ntot
    hh(2) = hh(2)/Ntot - hh(1)**2
    if (hh(2).le.0.or.Ntot.le.1) then ;hh(2) = abs( hh(1) )
                                 else ;hh(2) = sqrt( hh(2)/(Ntot-1) )
    endif
    hh = hh/step
    hst%left(  iBin) = aa
    hst%right( iBin) = bb
    hst%height(iBin) = hh(1)
    hst%error( iBin) = hh(2)
  enddo
!
  end subroutine


  subroutine histogram_setbins_e( hst ,inputFiles ,bins )
!***********************************************************************
! inputFiles is a single character string that may contain several
! space-separated file names.
! bins is an array containing left-and right borders of bins, eg.
!   bins = [ 0.5,0.7 ,0.7,0.8 ,0.8,1.3 ]
!***********************************************************************
  real(kind(1e0)),intent(in) :: bins(:)
  include 'histogram_setbins.h90'
  end subroutine

  subroutine histogram_setbins_d( hst ,inputFiles ,bins )
  real(kind(1d0)),intent(in) :: bins(:)
  include 'histogram_setbins.h90'
  end subroutine

  
  function histogram_getbins( hst ) result(bins)
!***********************************************************************
!***********************************************************************
  class(histogram_type),intent(in) :: hst
  real(kind(1d0)) :: bins(2*hst%Nbins)
  integer :: ii
  do ii=1,hst%Nbins
    bins(2*ii-1) = hst%left( ii)
    bins(2*ii  ) = hst%right(ii)
  enddo
  end function


  subroutine histogram_optimal( hst ,inputFiles &
                 ,leftBorder ,rightBorder ,numberOfBins &
                 ,alpha ,alpha1 ,alpha2 )
!***********************************************************************
!***********************************************************************
  class(histogram_type),intent(out) :: hst
  character(*)         ,intent(in) :: inputFiles
  real(kind(1d0))      ,intent(in) :: leftBorder,rightBorder
  integer              ,intent(in) :: numberOfBins
  real(kind(1d0)),optional,intent(in) :: alpha1,alpha2,alpha
  type(StatDist_type),allocatable :: obj(:)
  real(kind(1d0)) :: Norm,Ntot,hh(2),minError,measure,binMin,binWidth
  real(kind(1d0)) :: p1,p2,p3
  integer :: nFiles,iFile,iBin,jBin,Nbins
  type(charLine_type) :: files
!
  p1 = 1d0   ;if (present(alpha1)) p1=alpha1
  p2 = 1d0   ;if (present(alpha2)) p2=alpha2
  p3 = 0.5d0 ;if (present(alpha )) p3=alpha
!
  files = put_charLine( inputFiles )
  nFiles = files%Nwords()
  allocate( obj(1:nFiles) )
!
  do iFile=1,nFiles
    call obj(iFile)%load( iUnit=deffile ,filename=files%word(iFile) )
  enddo
!  
  Ntot = 0
  Norm = 0
  do iFile=1,nFiles
    Ntot = Ntot + obj(iFile)%Nev()
    Norm = Norm + obj(iFile)%get_Norm()
  enddo
  Norm = Norm/Ntot
!
  call hst%alloc( numberOfBins )
!
  binMin = 0d0 !(rightBorder-leftBorder)/(numberOfBins*8)
! 
  Nbins = 1
  call fillBin(1,leftBorder,rightBorder)
  do
    minError = huge(minError)
    do iBin=1,Nbins
      binWidth = hst%right(iBin)-hst%left(iBin)
      measure = hst%error(iBin)**p1 &
              / max(abs(hst%height(iBin)),hst%error(iBin))**p2 &
              / binWidth**p3
      if (measure.lt.minError.and.binWidth.gt.binMin) then
        minError = measure
        jBin = iBin
      endif
    enddo
    hst%left(  jBin+1:Nbins+1) = hst%left(  jBin:Nbins)
    hst%right( jBin+1:Nbins+1) = hst%right( jBin:Nbins)
    hst%height(jBin+1:Nbins+1) = hst%height(jBin:Nbins)
    hst%error( jBin+1:Nbins+1) = hst%error( jBin:Nbins)
    hst%right(jBin) = ( hst%left(jBin) + hst%right(jBin) )/2
    hst%left(jBin+1) = hst%right(jBin)
    call fillBin( jBin   ,hst%left(jBin  ) ,hst%right(jBin  ) )
    call fillBin( jBin+1 ,hst%left(jBin+1) ,hst%right(jBin+1) )
    Nbins = Nbins+1
    if (Nbins.eq.numberOfBins) exit
  enddo
!
  contains
!
    subroutine fillBin(ii,aa,bb)
    integer,intent(in) :: ii
    !(realknd2!),intent(in) :: aa,bb
    hh = 0
    do iFile=1,nFiles
      hh = hh + obj(iFile)%integral(aa,bb)
    enddo
    hh(1) = hh(1)/Ntot
    hh(2) = hh(2)/Ntot - hh(1)**2
    if (hh(2).le.0.or.Ntot.le.1) then ;hh(2) = abs( hh(1) )
                                 else ;hh(2) = sqrt( hh(2)/(Ntot-1) )
    endif
    hh = hh/(bb-aa)
    hst%left(  ii) = aa
    hst%right( ii) = bb
    hst%height(ii) = hh(1)
    hst%error( ii) = hh(2)
    end subroutine
!
  end subroutine


  subroutine histogram_write( hst ,outputFile ,frmt ,shift )
!***********************************************************************
!***********************************************************************
  class(histogram_type),intent(in) :: hst
  character(*)         ,intent(in) :: outputFile ,frmt
  !(realknd2!),optional,intent(in) :: shift
  !(realknd2!) :: dlt
  integer :: ii
!
  dlt = 0
  if (present(shift)) dlt = shift
!
  open(deffile,file=outputFile,status='replace')
!
  select case (frmt)
!
    case('middle') ! midpoint
      do ii=1,hst%Nbins
        write(deffile,'(A)') prnt([(hst%left(ii)+hst%right(ii))/2+dlt,hst%height(ii),hst%error(ii)],8)
      enddo
!  
    case('left-border') ! left border
      do ii=1,hst%Nbins
        write(deffile,'(A)') prnt([hst%left(ii)+dlt,hst%height(ii),hst%error(ii)],8)
      enddo
!  
    case('right-border') ! right border
      do ii=1,hst%Nbins
        write(deffile,'(A)') prnt([hst%right(ii)+dlt,hst%height(ii),hst%error(ii)],8)
      enddo
!  
    case('steps') ! left border and right border
      write(deffile,'(A)') prnt([hst%left(1)+dlt,0d0,0d0],8)
      do ii=1,hst%Nbins
        write(deffile,'(A)') prnt([hst%left( ii)+dlt,hst%height(ii),hst%error(ii)],8)
        write(deffile,'(A)') prnt([hst%right(ii)+dlt,hst%height(ii),hst%error(ii)],8)
      enddo
      write(deffile,'(A)') prnt([hst%right(hst%Nbins)+dlt,0d0,0d0],8)
!  
    case('heights') ! left border and right border
      do ii=1,hst%Nbins
        write(deffile,'(A)') prnt([hst%left( ii)+dlt,hst%height(ii),hst%error(ii)],8)
        write(deffile,'(A)') prnt([hst%right(ii)+dlt,hst%height(ii),hst%error(ii)],8)
        write(deffile,'(A)')
      enddo
!  
    case('left-right') ! left-right, one line
      do ii=1,hst%Nbins
        write(deffile,'(A)') prnt([hst%left(ii)+dlt,hst%right(ii)+dlt,hst%height(ii),hst%error(ii)],8)
      enddo
!  
    case default ! complete bin
      do ii=1,hst%Nbins
        write(deffile,'(A)') prnt([hst%left( ii)+dlt,0d0,0d0],8)
        write(deffile,'(A)') prnt([hst%left( ii)+dlt,hst%height(ii),hst%error(ii)],8)
        write(deffile,'(A)') prnt([hst%right(ii)+dlt,hst%height(ii),hst%error(ii)],8)
        write(deffile,'(A)') prnt([hst%right(ii)+dlt,0d0,0d0],8)
      enddo
!
  end select
! 
  close(deffile)
!
  end subroutine


  subroutine histogram_regularize( hst ,thrs )
!***********************************************************************
!***********************************************************************
  class(histogram_type),intent(inout) :: hst
  !(realknd2!),optional,intent(in   ) :: thrs
  integer :: ii
  !(realknd2!) :: minHeight
  if (present(thrs)) then
    minHeight = thrs
  else
    minHeight = huge(minHeight)
    do ii=1,hst%Nbins
      if (hst%height(ii).eq.rZRO) cycle
      if (hst%height(ii).lt.minHeight) minHeight=hst%height(ii)
    enddo
  endif
  do ii=1,hst%Nbins
    if (hst%height(ii).gt.minHeight) cycle
    hst%height(ii) = minHeight
  enddo
  end subroutine
  

  function histogram_compatible( hst1 ,hst2 ) result(hst3)
!***********************************************************************
!***********************************************************************
  type(histogram_type),intent(in) :: hst1 ,hst2
  type(histogram_type) :: hst3
  integer :: ii
  if (hst1%Nbins.ne.hst2%Nbins) then
    write(*,*) 'ERROR in statdist histogram: number of bins not equal'
    stop
  endif
  do ii=1,hst1%Nbins
    if (hst1%left(ii).ne.hst2%left(ii).or.hst1%right(ii).ne.hst2%right(ii)) then
      write(*,*) 'ERROR in statdist histogram: histograms not compatible'
      stop
    endif
  enddo
  call hst3%alloc( hst1%Nbins )
  hst3%left  = hst1%left
  hst3%right = hst1%right
  end function


  function histogram_divide( hst1 ,hst2 ) result(hst3)
!***********************************************************************
!***********************************************************************
  type(histogram_type),intent(in) :: hst1 ,hst2
  type(histogram_type) :: hst3
  integer :: ii
  hst3 = histogram_compatible( hst1 ,hst2 )
  do ii=1,hst1%Nbins
    if (hst2%height(ii).eq.0) then
      if (hst1%height(ii).eq.0) then
        hst3%height(ii) = 0
        hst3%error( ii) = 0
      else
        hst3%height(ii) = sign(9d99,hst1%height(ii))
        hst3%error( ii) = 9d99
      endif
    else
      hst3%height(ii) = hst1%height(ii)/hst2%height(ii)
    endif
    hst3%error(ii) = sqrt( (hst1%error(ii)/hst2%height(ii))**2 &
                          +(hst2%error(ii)*hst1%height(ii)/hst2%height(ii)**2)**2 )
  enddo
  end function

  
  function histogram_add( hst1 ,hst2 ) result(hst3)
!***********************************************************************
!***********************************************************************
  type(histogram_type),intent(in) :: hst1 ,hst2
  type(histogram_type) :: hst3
  integer :: ii
  hst3 = histogram_compatible( hst1 ,hst2 )
  do ii=1,hst1%Nbins
    hst3%height(ii) = hst1%height(ii) + hst2%height(ii)
    hst3%error(ii) = sqrt( hst1%error(ii)**2 + hst2%error(ii)**2 )
  enddo
  end function
  
  function histogram_subtract( hst1 ,hst2 ) result(hst3)
!***********************************************************************
!***********************************************************************
  type(histogram_type),intent(in) :: hst1 ,hst2
  type(histogram_type) :: hst3
  integer :: ii
  hst3 = histogram_compatible( hst1 ,hst2 )
  do ii=1,hst1%Nbins
    hst3%height(ii) = hst1%height(ii) - hst2%height(ii)
    hst3%error(ii) = sqrt( hst1%error(ii)**2 + hst2%error(ii)**2 )
  enddo
  end function
  
  function histogram_mlt_rh( xx ,hst1 ) result(hst3)
  real(kind(1d0))     ,intent(in) :: xx
  type(histogram_type),intent(in) :: hst1
  type(histogram_type) :: hst3
  integer :: ii
  call hst3%alloc( hst1%Nbins )
  do ii=1,hst1%Nbins
    hst3%left( ii) = hst1%left( ii)
    hst3%right(ii) = hst1%right(ii)
    hst3%height(ii) = hst1%height(ii)*xx
    hst3%error(ii) = hst1%error(ii)*abs(xx)
  enddo
  end function
  
  function histogram_mlt_hr( hst1 ,xx ) result(hst3)
  real(kind(1d0))     ,intent(in) :: xx
  type(histogram_type),intent(in) :: hst1
  type(histogram_type) :: hst3
  integer :: ii
  call hst3%alloc( hst1%Nbins )
  do ii=1,hst1%Nbins
    hst3%left( ii) = hst1%left( ii)
    hst3%right(ii) = hst1%right(ii)
    hst3%height(ii) = hst1%height(ii)*xx
    hst3%error(ii) = hst1%error(ii)*abs(xx)
  enddo
  end function
  
  function histogram_div_rh( xx ,hst1 ) result(hst3)
  real(kind(1d0))     ,intent(in) :: xx
  type(histogram_type),intent(in) :: hst1
  type(histogram_type) :: hst3
  integer :: ii
  call hst3%alloc( hst1%Nbins )
  do ii=1,hst1%Nbins
    hst3%left( ii) = hst1%left( ii)
    hst3%right(ii) = hst1%right(ii)
    hst3%height(ii) = hst1%height(ii)/xx
    hst3%error(ii) = hst1%error(ii)/abs(xx)
  enddo
  end function
  
  function histogram_div_hr( hst1 ,xx ) result(hst3)
  real(kind(1d0))     ,intent(in) :: xx
  type(histogram_type),intent(in) :: hst1
  type(histogram_type) :: hst3
  integer :: ii
  call hst3%alloc( hst1%Nbins )
  do ii=1,hst1%Nbins
    hst3%left( ii) = hst1%left( ii)
    hst3%right(ii) = hst1%right(ii)
    hst3%height(ii) = hst1%height(ii)/xx
    hst3%error(ii) = hst1%error(ii)/abs(xx)
  enddo
  end function
  

  function histogram_norm( obj ) result(rslt)
  class(histogram_type),intent(inout) :: obj
  real(kind(1d0)) :: rslt
  integer :: ii
  rslt = 0
  do ii=1,obj%Nbins
    rslt = rslt + (obj%right(ii)-obj%left(ii))*obj%height(ii)
  enddo
  end function 
  
  subroutine histogram_normalize( obj )
  class(histogram_type),intent(inout) :: obj
  real(kind(1d0)) :: norm
  integer :: ii
  norm = 0
  do ii=1,obj%Nbins
    norm = norm + (obj%right(ii)-obj%left(ii))*obj%height(ii)
  enddo
  obj%height = obj%height/norm
  obj%error  = obj%error/norm
  end subroutine
  
 
  subroutine histogram_merge( hst ,list ,outputFile ,frmt )
  class(histogram_type),intent(in) :: hst
  type(histogram_type) ,intent(in) :: list(:)
  character(*)         ,intent(in) :: outputFile ,frmt
  character(16*(2+2*8)) :: line
  integer :: jj,ii
  do jj=1,size(list,1)
    if (hst%Nbins.ne.list(jj)%Nbins) call error
    do ii=1,hst%Nbins
      if (hst%left( ii).ne.list(jj)%left( ii)) call error
      if (hst%right(ii).ne.list(jj)%right(ii)) call error
    enddo
  enddo
!
  open(deffile,file=outputFile,status='replace')
!
  select case (frmt)
!
    case('middle') ! midpoint
      do ii=1,hst%Nbins
        line = prnt([(hst%left(ii)+hst%right(ii))/2,hst%height(ii),hst%error(ii)],8)
        do jj=1,size(list,1)
          line = trim(line)//' '//prnt([list(jj)%height(ii),list(jj)%error(ii)],8)
        enddo
        write(deffile,'(A)') trim(line)
      enddo
!  
    case('left-right') ! left-right, one line
      do ii=1,hst%Nbins
        line = prnt([hst%left(ii),hst%right(ii),hst%height(ii),hst%error(ii)],8)
        do jj=1,size(list,1)
          line = trim(line)//' '//prnt([list(jj)%height(ii),list(jj)%error(ii)],8)
        enddo
        write(deffile,'(A)') trim(line)
      enddo
!  
  end select
! 
  close(deffile)
!
  contains
    subroutine error
    if (errru.ge.0) write(errru,*) 'ERROR in histogram_merge: ' &
      ,'histrograms not compatible'
    stop
    end subroutine
  end subroutine
 
!2d  include 'statdi2d.h90'
  

end module


