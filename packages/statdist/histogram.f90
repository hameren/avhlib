!!HEADER!!
!!
!! Copyright (C) 2011 Andreas van Hameren <hamerenREMOVETHIS@ifj.edu.pl>. 
!!
!! This file is part of StatDist.
!!
!! StatDist is free software: you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! StatDist is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with StatDist.  If not, see <http://www.gnu.org/licenses/>.
!!
!******************************************************************************
!begin example input file
!!
!! Lines starting with an exclamation mark are skipped.
!!
!    xLeft =-8d0
!   xRight = 8d0
!    nBins = 32
!  iFormat = 0
!   nFiles = 3
! dumpFile = EtaJet1.dump 1d0
! dumpFile = EtaJet2.dump 2d0
! dumpFile = EtaJet3.dump 1d0
!  outFile = EtaJet.hist
!!
!end example input file

!option iFormat=1: midpoint     of each bin, one line
!option iFormat=2: left border  of each bin, one line
!option iFormat=3: right border of each bin, one line
!option iFormat=4: both borders of each bin, two lines
!option iFormat=rest: complete bin, four lines 

program histogram
  use avh_kindpars
  use avh_mathcnst
  use avh_ioUnits
  use avh_ioTools
  use avh_prnt
  use avh_StatDist
  implicit none
!
  integer      ,parameter :: lineLen=72
  character(8) ,parameter :: writeFrmt='(A)'
  integer      ,parameter :: NfileMax=11
!
  type(StatDist_type) :: obj(NfileMax)
  real(kindr2)        :: weight(NfileMax)
  real(kindr2) :: xLeft,xRight,aa,bb,hh,step,Norm,Ntot,Wtot,factor
  character(lineLen) :: line,outFile
  integer :: nFiles,iFile,nBins,iBin,iFormat,iOutput,nn
  integer :: iFrst(lineLen),iLast(lineLen)
  logical :: eof
!
  call init_mathcnst
!
  iOutput = 0
  do
    iOutput = iOutput+1
    outFile = 'histout'//prnt(iOutput,'lft')
!
!   Read input file
    call nextLine(line,iFrst,iLast,nn,'!',' =');if(line.eq.'#end')return
    read(line(iFrst(nn-4):iLast(nn-4)),*) xLeft
    read(line(iFrst(nn-3):iLast(nn-3)),*) xRight
    read(line(iFrst(nn-2):iLast(nn-2)),*) nBins
    read(line(iFrst(nn-1):iLast(nn-1)),*) factor
    read(line(iFrst(nn-0):iLast(nn-0)),*) iFormat
    nFiles = 0
    do
      call nextLine(line,iFrst,iLast,nn,'!',' =');if(line.eq.'#')exit
      nFiles = nFiles+1
      call obj(nFiles)%load( iUnit=deffile &
                         ,filename=line(iFrst(nn-1):iLast(nn-1)) )
      read(line(iFrst(nn):iLast(nn)),*) weight(nFiles)
    enddo
!  
!   Open output file
    open(deffile,file=outFile,status='replace')
    if (iFormat.eq.4) write(deffile,writeFrmt) prnt((/xLeft,rZRO/),8)
!  
    Ntot = 0
    Wtot = 0
    Norm = 0
    do iFile=1,nFiles
      Ntot = Ntot + obj(iFile)%Nev()
      Wtot = Wtot + weight(iFile)
      Norm = Norm + weight(iFile)*obj(iFile)%get_Norm()
    enddo
    Norm = Norm/Ntot
!  
    step = (xRight-xLeft)/nBins
!  
!   Loop over bins 
    do iBin=1,nBins
      aa = xLeft + step*(iBin-1)
      bb = aa + step
      hh = 0
      do iFile=1,nFiles
        hh = hh + weight(iFile)*obj(iFile)%integral(aa,bb)
      enddo
      hh = hh/Ntot
      hh = hh/step
      if (factor.eq.rZRO) then
        hh = hh/Norm
      else
        hh = hh*factor
      endif
      if     (iFormat.eq.1) then ! midpoint
        write(deffile,writeFrmt) prnt((/(aa+bb)/2,hh/),8)
      elseif (iFormat.eq.2) then ! left border
        write(deffile,writeFrmt) prnt((/aa,hh/),8)
      elseif (iFormat.eq.3) then ! right border
        write(deffile,writeFrmt) prnt((/bb,hh/),8)
      elseif (iFormat.eq.4) then ! left border and right border
        write(deffile,writeFrmt) prnt((/aa,hh/),8)
        write(deffile,writeFrmt) prnt((/bb,hh/),8)
      elseif (iFormat.eq.5) then ! left-right, one line
        write(deffile,writeFrmt) prnt((/aa,bb,hh/),8)
      else                       ! complete bin
        write(deffile,writeFrmt) prnt((/aa,rZRO/),8)
        write(deffile,writeFrmt) prnt((/aa,hh  /),8)
        write(deffile,writeFrmt) prnt((/bb,hh  /),8)
        write(deffile,writeFrmt) prnt((/bb,rZRO/),8)
      endif
    enddo
!  
    if (iFormat.eq.4) write(deffile,writeFrmt) prnt((/xRight,rZRO/),8)
!  
!   Close output file
    close(deffile)
!
  enddo
!
end program
