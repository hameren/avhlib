#!/usr/bin/env python
import sys
avhdir = '/home/others/hameren/fortran/avh_src-06'
sys.path.append(avhdir)
import avh_fpp,avh_cr

locdir = avhdir+'/statdist/'
lines = []
avh_cr.addavh(avhdir+'/kindpars',lines)
avh_cr.addavh(avhdir+'/mathcnst',lines)
avh_cr.addavh(avhdir+'/iounits',lines)
avh_cr.addavh(avhdir+'/iotools',lines)
avh_cr.addavh(avhdir+'/prnt',lines)
avh_cr.addavh(avhdir+'/statdist',lines)
avh_cr.addfile(locdir+'histogram.f90',lines)

avh_fpp.xpnd('realknd2','real(kind(1d0))',lines)
avh_fpp.xpnd('complex2','complex(kind(1d0))',lines)

avh_cr.wfile('tmp.f90',lines,verbose=1)
avh_cr.execute(['gfortran','tmp.f90'],verbose=1)
avh_cr.execute(['mv','a.out','histfiles'])
