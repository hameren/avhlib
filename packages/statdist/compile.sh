#!/bin/sh
avh=/home/hameren/fortran/avh_src-04
FC=gfortran

$FC \
-o histfiles \
$avh/kindpars/bott.f90 \
$avh/mathcnst/bott.f90 \
$avh/iounits/bott.f90 \
$avh/iotools/bott.f90 \
$avh/prnt/bott.f90 \
$avh/statdist/bott.f90 \
$avh/statdist/histogram.f90

rm *.mod
