module avh_kaleu_cs
  use avh_kaleu_units
  use avh_mathcnst
  use avh_prnt
  use avh_kaleu_base
  use avh_kaleu_model
  use avh_kaleu_mulcha, only: rng_interface
  use avh_kaleu_splvar !{splvar=both!}
  use avh_grid
  use avh_kinematics
  use avh_kskeleton, only: base
  use avh_kaleu_ns, only: mch_type
  use avh_kaleu_stats

  implicit none
  private
  public :: kaleu_cs_type

  type :: kaleu_cs_type
    private
    integer :: Ndip=0,Nfinst
    integer          ,allocatable :: i(:),j(:),k(:)
    type(kaleu_base_type) ,allocatable :: kaleu(:)
    type(splvar_type),allocatable :: splv(:) !{splvar=both!}
    type(grid_type)  ,allocatable :: grid(:)
    type(mch_type) :: mch
    integer,allocatable :: list(:)
    integer :: Idip=-1 ,idat=0 ,sourceDip=0
    !(realknd2!) :: loopDns
    logical :: wght_not_calcd=.true.
    procedure(rng_interface),pointer,nopass :: rng
  contains
    procedure :: init
    procedure :: gnrt
    procedure :: exit
    procedure :: wght
    procedure :: adapt
    procedure :: dalloc
    procedure :: plotgrids
  end type


contains


  subroutine dalloc( obj )
!**********************************************************************
!**********************************************************************
  class(kaleu_cs_type) :: obj
  integer :: ii
  call obj%mch%dalloc
  do ii=0,obj%Ndip
    call obj%kaleu(ii)%dalloc
  enddo
  if (allocated( obj%i )    ) deallocate( obj%i )
  if (allocated( obj%j )    ) deallocate( obj%j )
  if (allocated( obj%k )    ) deallocate( obj%k )
  if (allocated( obj%kaleu )) deallocate( obj%kaleu )
  if (allocated( obj%splv ) ) deallocate( obj%splv ) !{splvar=both!}
  if (allocated( obj%grid ) ) deallocate( obj%grid )
  if (allocated( obj%list ) ) deallocate( obj%list )
  obj%Ndip = 0
  obj%Idip =-1
  obj%idat = 0
  obj%wght_not_calcd = .true.
  end subroutine


  subroutine init( obj ,mdl ,stats ,process ,Ecm ,Ecut ,rcg,rng ,dip,gluon )
!**********************************************************************
!* dip(1,l)=i, dip(2,l)=j, dip(3,l)=k
!**********************************************************************
  class(kaleu_cs_type) :: obj
  type(kaleu_model_type),intent(inout) :: mdl
  type(kaleu_stats_type),intent(in) :: stats
  type(process_type),intent(in) :: process
  !(realknd2!),intent(in) :: Ecm,Ecut
  procedure(rng_interface) :: rcg,rng
  integer,intent(in) :: dip(:,:),gluon ! expects dip(1:3,1:Ndip)
  type(process_type) :: tmpproc
  integer      :: ll,mm,nn,Nfinst
  logical      :: cancel
  character(8) :: lbl
  character(2) ,parameter :: symb(-2:9) = &
      (/'-2','-1','+0','+1','+2','+3','+4','+5','+6','+7','+8','+9'/)
  character(160) :: line
!
  call kaleu_hello
!
  obj%rng => rng
!
  if (size(dip,1).ne.3) then
    if (errru.ge.0) write(errru,*) 'ERROR in Kaleu CS init: '&
      ,'array dip has the wrong shape'
    stop
  endif
  obj%Ndip = size(dip,2)
!
  allocate( obj%i(0:obj%Ndip) )
  allocate( obj%j(0:obj%Ndip) )
  allocate( obj%k(0:obj%Ndip) )
  allocate( obj%kaleu(0:obj%Ndip) )
  allocate( obj%list(0:obj%Ndip) )
  obj%i = 0
  obj%j = 0
  obj%k = 0
!
! The (n+1)-channel
  call obj%kaleu(0)%init( mdl ,process ,Ecm ,Ecut ,rcg,rng &
                         ,stats ,cancel=cancel ,label='00' )
  if (cancel) then
    if (errru.ge.0) write(errru,*) 'ERROR in Kaleu CS: process not possible'
    stop
  endif
!
! Put dipped channels and find processes
  Nfinst = process%Nfinst
  nn = 0
  ll = 0
  do ;nn=nn+1 ;if(nn.gt.obj%Ndip)exit
    ll = ll+1
    obj%list(nn) = ll
    obj%i(nn) = dip(1,ll)
    obj%j(nn) = dip(2,ll)
    obj%k(nn) = dip(3,ll)
!
    if (obj%i(nn).lt.0.and. &
        mdl%mass(mdl%getRef(process%infi(obj%j(nn)))).ne.rZRO) then
      if (warnu.gt.0) write(warnu,*) 'WARNING from Kaleu CS: ' &
                        ,'massive radiant from initial state, '&
                        ,'not added to the list of channels'
      nn = nn-1
      obj%Ndip = obj%Ndip-1
      cycle
    endif
!
    tmpproc = process
    if     (process%infi(obj%i(nn)).eq.gluon) then
      tmpproc%infi(obj%i(nn)) = process%infi(obj%j(nn))
    elseif (process%infi(obj%j(nn)).eq.gluon) then
      tmpproc%infi(obj%i(nn)) = process%infi(obj%i(nn))
    else
      tmpproc%infi(obj%i(nn)) = gluon
    endif
    tmpproc%infi(obj%j(nn):Nfinst-1) = tmpproc%infi(obj%j(nn)+1:Nfinst)
    tmpproc%Nfinst = tmpproc%Nfinst-1
    call tmpproc%complete_xtrn
!
    call obj%kaleu(nn)%init( mdl ,tmpproc ,Ecm ,Ecut ,rcg,rng &
                           ,stats  ,cancel=cancel,label=prnt(nn,1,'0') )
    if (cancel) then
      if (warnu.gt.0) write(warnu,*) 'WARNING from Kaleu CS: ' &
        ,'process not possible, not adding it to the list of channels'
      call obj%kaleu(nn)%dalloc
      nn = nn-1
      obj%Ndip = obj%Ndip-1
      cycle
    endif
!
    if (nsUnit.gt.0) then
      line = 'MESSAGE from Kaleu CS:'//prnt(nn,2)//' =' &
        //' '//prnt(obj%i(nn),2)//mdl%symbol(mdl%getRef(process%infi(obj%i(nn)))) &
        //' '//prnt(obj%j(nn),1)//mdl%symbol(mdl%getRef(process%infi(obj%j(nn)))) & 
        //' '//prnt(obj%k(nn),1)//mdl%symbol(mdl%getRef(process%infi(obj%k(nn)))) 
      line = trim(line)//' , '//mdl%symbol(mdl%getRef(tmpproc%infi(-2)))        &
                         //' '//mdl%symbol(mdl%getRef(tmpproc%infi(-1)))//' ->'
      do mm=1,Nfinst-1
        line = trim(line)//' '//mdl%symbol(mdl%getRef(tmpproc%infi(mm)))
      enddo
      write(nsUnit,*) trim(line)
    endif
  enddo
!
! Initialize adaptive channels
  call obj%mch%init( 0,obj%Ndip ,rcg ,stats )
!
! Initialize grids
  allocate( obj%grid(1:2*obj%Ndip) )
  allocate( obj%splv(1:2*obj%Ndip) ) !{splvar=both!}
  lbl = 'D     '
  do nn=1,obj%Ndip
    lbl(2:8) = symb(obj%i(nn))//symb(obj%j(nn))//symb(obj%k(nn))//'x'
    call obj%grid(2*nn-1)%init( stats%Nbatch ,200 ,label=lbl )
    call obj%splv(2*nn-1)%init( Nbatch=stats%Nbatch ,label=lbl ) !{splvar=both!}
    lbl(8:8) = 'z'
    call obj%grid(2*nn  )%init( stats%Nbatch ,200 ,label=lbl )
    call obj%splv(2*nn  )%init( Nbatch=stats%Nbatch ,label=lbl ) !{splvar=both!}
  enddo
  end subroutine


  subroutine gnrt( obj ,discard ,psp1,psp ,pInst )
!*********************************************************************
!*********************************************************************
  class(kaleu_cs_type) :: obj
  type(psPoint_type),intent(inout) :: psp1,psp
  logical     ,intent(  out) :: discard
  !(realknd2!),intent(in   ) :: pInst(0:3,-2:-1)
  !(realknd2!) :: xx,zz,pTmp(0:3,-2:-1)
  integer      :: Idip,ii,jj,kk,aa
!
  call obj%mch%gnrt( Idip )
  obj%sourceDip = Idip
!
  if (Idip.eq.0) then
    call obj%kaleu(0)%gnrt( discard ,psp1 ,pInst ,[rZRO,rZRO] )
    if (discard) return
  else
    call obj%kaleu(0)%put_masses_to( psp1 )
    call psp1%put_inst( pInst ,[rZRO,rZRO] ,.true. )
    ii = obj%i(Idip)
    jj = obj%j(Idip)
    kk = obj%k(Idip)
    xx = obj%grid(2*Idip-1)%gnrt( obj%rng() )
    zz = obj%grid(2*Idip  )%gnrt( obj%rng() )
!{splvar=both
    xx = obj%splv(2*Idip-1)%gnrt( xx )
    zz = obj%splv(2*Idip  )%gnrt( zz )
!}splvar=both
    if ( ii.gt.0.and.kk.gt.0 ) then
      call obj%kaleu(Idip)%gnrt( discard ,psp ,pInst ,[rZRO,rZRO] )
      if (discard) return
      call gnrt_finfin( discard ,psp1,psp ,xx,zz,obj%rng() ,ii,jj,kk )
      if (discard) return
    elseif ( ii.gt.0.and.kk.lt.0 .or. ii.lt.0.and.kk.gt.0 ) then
      if (ii.gt.0) then ;aa=ii;ii=kk;kk=aa ;endif
      pTmp = pInst
      pTmp(0:3,ii) = (1-xx)*pTmp(0:3,ii) ! soft limit for x->0, not x->1
      call obj%kaleu(Idip)%gnrt( discard ,psp ,pTmp ,[rZRO,rZRO] )
      if (discard) return
      call gnrt_infin( discard ,psp1,psp ,xx,zz,obj%rng() ,ii,jj,kk )
      if (discard) return
    else
      zz = zz*xx
      xx = 1-xx
      pTmp = pInst
      pTmp(0:3,ii) = xx*pTmp(0:3,ii)
      call obj%kaleu(Idip)%gnrt( discard ,psp ,pTmp ,[rZRO,rZRO] )
      if (discard) return
      call gnrt_inin( discard ,psp1,psp ,xx,zz,obj%rng() ,ii,jj )
      if (discard) return
    endif
    call psp1%complete
  endif
!
  discard = .false.
!
  end subroutine 


  function exit( obj ,psp1 ,psp ,Ilist ,discard ,chWght ,chDnst ) result(rslt)
!*********************************************************************
! Usage:
!                   input       output
!                     |       /   |    \
!                     V      V    V     V
!  do ;if (obj%exit( psp1 ,psp ,Ilist ,discard )) exit
!    if (discard) cycle
!    if (Ilist.eq.0) then
!      value = amplitude_Nplus1( psp1 )
!      etc...
!    else
!      value = amplitude_N( Ilist ,psp )
!      etc...
!    endif
!  enddo
!
!  weight = obj%wght( psp1,psp )
!
! DO NOT CHANGE psp1 INSIDE THE LOOP
!*********************************************************************
  class(kaleu_cs_type) :: obj
  type(psPoint_type),intent(in   ):: psp1
  type(psPoint_type),intent(inout):: psp
  integer,intent(out) :: Ilist
  logical,intent(out) :: discard
  !(realknd2!),intent(out),optional :: chWght,chDnst
  logical :: rslt
  !(realknd2!) :: xx,zz,w1,w2,wx,wz,x0,z0
  integer :: Idip
!
  if (obj%Idip.lt.0) then
    obj%loopDns = 0
    obj%Idip =-1
  endif
!
  obj%Idip = obj%Idip+1
  Idip = obj%Idip
  discard = .true.
!
  rslt = (Idip.gt.obj%Ndip)
  if (rslt) then
    obj%wght_not_calcd = .false.
    obj%Idip =-1
    return
  endif
!
  Ilist = obj%list(Idip)
  obj%mch%dns(Idip) = 0
! 
  if (Idip.eq.0) then
    w1 = obj%kaleu(0)%wght( psp1 )
    if (w1.eq.rZRO) return
    obj%mch%dns(Idip) = 1/w1
  else
    call obj%kaleu(Idip)%put_masses_to( psp )
    if (obj%i(Idip).gt.0) then
      if (obj%k(Idip).gt.0) then
        call wght_finfin( w1 ,psp1,psp ,xx,zz ,obj%i(Idip),obj%j(Idip),obj%k(Idip) )
      else
        call wght_infin( w1 ,psp1,psp ,xx,zz ,obj%k(Idip),obj%j(Idip),obj%i(Idip) )
      endif
    else
      if (obj%k(Idip).gt.0) then
        call wght_infin( w1 ,psp1,psp ,xx,zz ,obj%i(Idip),obj%j(Idip),obj%k(Idip) )
      else
        call wght_inin( w1 ,psp1,psp ,xx,zz ,obj%i(Idip),obj%j(Idip) )
        xx = 1-xx
        zz = zz/xx
      endif
    endif
    if (w1.eq.rZRO) return
    if (xx.le.rZRO.or.rONE.le.xx) return
    if (zz.le.rZRO.or.rONE.le.zz) return
    call psp%complete
    w2 = obj%kaleu(Idip)%wght( psp )
    if (w2.eq.rZRO) return
!{splvar=both
    call obj%splv(2*Idip-1)%wght( wx ,x0 ,xx )
    call obj%splv(2*Idip  )%wght( wz ,z0 ,zz )
    wx = wx * obj%grid(2*Idip-1)%wght( x0 )
    wz = wz * obj%grid(2*Idip  )%wght( z0 )
!}splvar=both
!{splvar=grid
    wx = obj%grid(2*Idip-1)%wght( xx )
    wz = obj%grid(2*Idip  )%wght( zz )
!}splvar=grid
    obj%mch%dns(Idip) = 1/(w1*w2*wx*wz)
  endif
! 
  obj%loopDns = obj%loopDns + obj%mch%wch(Idip)*obj%mch%dns(Idip)
!
  if (present(chWght)) chWght = obj%mch%wch(Idip)
  if (present(chDnst)) chDnst = obj%mch%dns(Idip)
!
  discard = .false.
!
  end function


  function wght( obj ,psp1,psp ) result(weight)
!*********************************************************************
!*********************************************************************
  class(kaleu_cs_type) :: obj
  type(psPoint_type),intent(in   ) :: psp1
  type(psPoint_type),intent(inout) :: psp
  !(realknd2!) :: weight
  integer :: Ilist
  logical :: discard
!
  if (obj%wght_not_calcd) then
    do ;if (obj%exit( psp1 ,psp ,Ilist ,discard )) exit
      if (discard) cycle
    enddo
  endif
!
  if (obj%loopDns.eq.rZRO) then
    weight = 0
  else
    weight = 1/obj%loopDns
  endif
  obj%wght_not_calcd = .true.
!
  end function


  subroutine adapt( obj ,stats )
!*********************************************************************
!*********************************************************************
  class(kaleu_cs_type) :: obj
  include 'kaleu_ns_adapt.h90'
  end subroutine


  subroutine plotgrids( obj ,iunit )
!*********************************************************************
!*********************************************************************
  class(kaleu_cs_type) :: obj
  integer,intent(in) :: iunit
  integer :: ii
  if (iunit.le.0) return
  do ii=1,obj%Ndip
    call obj%splv(2*ii-1)%plot( iunit ) !{splvar=both!}
    call obj%splv(2*ii  )%plot( iunit ) !{splvar=both!}
    call obj%grid(2*ii-1)%plot( iunit )
    call obj%grid(2*ii  )%plot( iunit )
  enddo
  end subroutine


!*********************************************************************
!*********************************************************************


  subroutine gnrt_finfin( discard ,psp1,psp ,xx,zz,phi ,ii,jj,kk )
!*********************************************************************
!*********************************************************************
  logical           ,intent(inout) :: discard
  type(psPoint_type),intent(inout) :: psp1
  type(psPoint_type),intent(in   ) :: psp
  integer      ,intent(in) :: ii,jj,kk
  !(realknd2!) ,intent(in) :: xx,zz,phi
  !(realknd2!) :: hh,vk,vi,zmin,zdif,yy &
                 ,pipj2,pipk2,pjpk2,abspj,abspk,cosjk,sinjk
  integer :: ll,it,kt
!
  it = ii
  if (it.gt.jj) it=it-1
  kt = kk
  if (kt.gt.jj) kt=kt-1
  associate(  pi=>psp1%p(psp1%b(ii)) &
            , pj=>psp1%p(psp1%b(jj)) &
            , pk=>psp1%p(psp1%b(kk)) &
            ,pij=>psp1%p(psp1%b(ii)+psp1%b(jj)) &
            ,  Q=>psp%p(psp%b(it)+psp%b(kt)) &
            ,pkt=>psp%p(psp%b(kt)) )
!
  discard = .false.
!
  pi%S = pi%M**2
  pj%S = pj%M**2
  pk%S = pk%M**2
!
  hh = ( Q%M - pk%M )*( Q%M + pk%M )
  pij%S = xx*( Q%M - pk%M )**2 + (1-xx)*( pi%M + pj%M )**2
  pij%M = sqrt(abs(pij%S))
  vk = realMom_lambda(Q,pk,pij)
  if (vk.lt.rZRO) then
    !if (errru.gt.0) write(errru,*) 'ERROR in Kaleu CS gnrt_final: ' &
    !  ,'vk<0, discard event',Q%M,pk%M,pij%M-pi%M-pj%M
    discard = .true.
    return
  endif
  vk = sqrt( vk )/( hh - pij%S )
  vi = realMom_lambda(pij,pi,pj)
  if (vi.lt.rZRO) then
    !if (errru.gt.0) write(errru,*) 'ERROR in Kaleu CS gnrt_final: ' &
    !  ,'vi<0, discard event'
    discard = .true.
    return
  endif
  vi = sqrt( vi )/( pij%S + (pi%S - pj%S) )
  zmin = ( pij%S + (pi%S - pj%S) )/( 2*pij%S )
  zdif = zmin*vi*vk
  zmin = zmin-zdif
  zdif = 2*zdif
  yy = zdif*zz + zmin
!
  pipj2 = pij%S - pi%S - pj%S
  pipk2 = yy*( hh - pij%S )
  pjpk2 = (1-yy)*( hh - pij%S )
  pi%E = ( hh - pjpk2 + (pi%S - pj%S) )/(2*Q%M)
  pj%E = ( hh - pipk2 - (pi%S - pj%S) )/(2*Q%M)
  pk%E = ( Q%S + pk%S - pipj2 - pi%S - pj%S )/(2*Q%M)
  abspj = ( pj%E - pj%M )*( pj%E + pj%M )
  abspk = ( pk%E - pk%M )*( pk%E + pk%M )
  if (abspj.lt.rZRO.or.abspk.lt.rZRO) then
    if (errru.gt.0) write(errru,*) 'ERROR in Kaleu CS gnrt_final: ' &
      ,'abspj or abspk <0, discard event'
    discard = .true.
    return
  endif
  abspj = sqrt(abspj)
  abspk = sqrt(abspk)
  cosjk = ( 2*pj%E*pk%E - pjpk2 )/(2*abspk)
  sinjk = (abspj+cosjk)*(abspj-cosjk)
  if (sinjk.lt.rZRO) then
    if (errru.gt.0) write(errru,*) 'ERROR in Kaleu CS gnrt_final: ' &
      ,'|cosjk| >1, discard event'
    discard = .true.
    return
  endif
  sinjk = sqrt(sinjk)
  pj%V(1:3) = [ sinjk*cos(r2PI*phi) ,sinjk*sin(r2PI*phi) ,cosjk ]
  pk%V(1:3) = [ rZRO ,rZRO ,abspk ]
  pi%V(1:3) = -pk%V(1:3) - pj%V(1:3)
  pi = pi%rotate_z(pkt%tsoob(Q))
  pj = pj%rotate_z()
  pk = pk%rotate_z()
  pi = pi%boost( Q )
  pj = pj%boost( Q )
  pk = pk%boost( Q )
!
  if (psp%Nfinst.ge.3) then
    do ll=1,jj-1
      if (ll.eq.ii) cycle
      if (ll.eq.kk) cycle
      psp1%p(psp1%b(ll)) = psp%p(psp%b(ll))
    enddo
    do ll=jj+1,psp1%Nfinst
      if (ll.eq.ii) cycle
      if (ll.eq.kk) cycle
      psp1%p(psp1%b(ll)) = psp%p(psp%b(ll-1))
    enddo
  endif
!
  end associate
  end subroutine


  subroutine wght_finfin( weight ,psp1,psp ,xx,zz ,ii,jj,kk )
!*********************************************************************
!*********************************************************************
  !(realknd2!),intent(out) :: weight ,xx,zz
  type(psPoint_type),intent(in   ) :: psp1
  type(psPoint_type),intent(inout) :: psp
  integer,intent(in) :: ii,jj,kk
  !(realknd2!) :: vol,lam,vk,vi,rr
  integer :: it,kt,ll
  logical :: update
!
  weight = rZRO
!
  it = ii
  if (it.gt.jj) it=it-1
  kt = kk
  if (kt.gt.jj) kt=kt-1
!
  associate(   Q=>psp1%p(psp1%b(ii)+psp1%b(jj)+psp1%b(kk)) &
             ,pi=>psp1%p(psp1%b(ii)) &
             ,pj=>psp1%p(psp1%b(jj)) &
             ,pk=>psp1%p(psp1%b(kk)) &
            ,pij=>psp1%p(psp1%b(ii)+psp1%b(jj)) &
            ,pik=>psp1%p(psp1%b(ii)+psp1%b(kk)) &
            ,pit=>psp%p(psp%b(it)) &
            ,pkt=>psp%p(psp%b(kt)) )
!
  pit%S = pit%M**2
  pkt%S = pkt%M**2
!
  lam = realMom_lambda(Q,pit,pk)
  if (lam.le.rZRO) then
    if (errru.ge.0) write(errru,*) 'ERROR in Kaleu CS wght_final: ' &
      ,'lam<0, putting weight=0'
    return
  endif
  vk = realMom_lambda(Q,pij,pk)
  if (vk.le.rZRO) then
    if (errru.ge.0) write(errru,*) 'ERROR in Kaleu CS wght_final: ' &
      ,'vk<0, putting weight=0'
    return
  endif
  vi = realMom_lambda(pij,pi,pj)
  if (vi.le.rZRO) then
    if (errru.ge.0) write(errru,*) 'ERROR in Kaleu CS wght_final: ' &
      ,'vi<0, putting weight=0'
    return
  endif
!
  vol = ((Q%M-pk%M)-pi%M-pj%M)*((Q%M-pk%M)+pi%M+pj%M)
  xx = ( pij%M - pi%M - pj%M )*( pij%M + pi%M + pj%M )/vol
!
  zz = ( (pik%M-pk%M)*(pik%M+pk%M) - pi%S )*( pij%S*2 ) &
     - ( (pij%M-pj%M)*(pij%M+pj%M) + pi%S )*( (Q%M-pk%M)*(Q%M+pk%M) - pij%S )
  zz = rHLF + zz/sqrt( 4*vk*vi )
!
  rr = sqrt(lam/vk)
  weight = r2PI*sqrt(vi)*vol/(4*rr*pij%S) ! extra factor (2pi)^3
!
  call psp%put_inst( psp1 )
!
  pkt%V = rr*pk%V + ( (Q%S+pk%S)*(1-rr) - (pit%S-rr*pij%S) )/(2*Q%S)*Q%V
  call pkt%complete
  pit%V = Q%V - pkt%V
  call pit%complete
!
  if (psp%Nfinst.ge.3) then
    do ll=1,jj-1
      if (ll.eq.it) cycle
      if (ll.eq.kt) cycle
      psp%p(psp%b(ll)) = psp1%p(psp1%b(ll))
    enddo
    do ll=jj,psp%Nfinst
      if (ll.eq.it) cycle
      if (ll.eq.kt) cycle
      psp%p(psp%b(ll)) = psp1%p(psp1%b(ll+1))
    enddo
  endif
!
  end associate
  end subroutine


  subroutine gnrt_infin( discard ,psp1,psp ,xx,zz,phi ,aa,jj,kk )
!********************************************************************
!* Soft limit when x->0, not x->1
!********************************************************************
  logical           ,intent(inout) :: discard
  type(psPoint_type),intent(inout) :: psp1
  type(psPoint_type),intent(in   ) :: psp
  integer      ,intent(in) :: aa,jj,kk
  !(realknd2!) ,intent(in) :: xx,zz,phi
  !(realknd2!) :: yy,abspk,cosKa,sinKa,pktpa,hh
  integer :: ll,kt
!
  discard = .false.
!
  kt = kk
  if (kt.gt.jj) kt=kt-1
!
  associate( pa=>psp1%p(psp1%Nsize-psp1%b(aa)) & ! -p_a, so with positive energy
            ,pj=>psp1%p(psp1%b(jj)) &
            ,pk=>psp1%p(psp1%b(kk)) &
            ,PP=>psp1%p(psp1%b(jj)+psp1%b(kk)) &
            ,pkt=>psp%p(psp%b(kt)) )
!
  pj%S = pj%M**2
  pk%S = pk%M**2
  PP = pkt + xx*pa
  pktpa = pkt%E*pa%E - pkt%V(1)*pa%V(1) - pkt%V(2)*pa%V(2) - pkt%V(3)*pa%V(3)
!
  hh = pk%S/(2*pktpa)
  yy = ( xx*zz + hh )/(xx+hh)
!
  pa = pa%tsoob(PP)
  abspk = realMom_lambda(PP,pk,pj)
  if (abspk.lt.rZRO) then
    !if (errru.gt.0) write(errru,*) 'ERROR in Kaleu CS gnrt_initial: ' &
    !  ,'abspk^2<0, discard event',PP%S,pk%S,pj%S
    discard = .true.
    return
  endif
  abspk = abspk/(4*PP%S)
  pk%E = sqrt( abspk + pk%S )
  abspk = sqrt( abspk )
  cosKa = pk%E - yy*pktpa/pa%E
  sinKa = (abspk+cosKa)*(abspk-cosKa)
  if (sinKa.lt.rZRO) then
    !if (errru.gt.0) write(errru,*) 'ERROR in Kaleu CS gnrt_initial: ' &
    !  ,'|cosKa| >1, discard event'
    discard = .true.
    return
  endif
  sinKa = sqrt(sinKa)
  pk%V = [ sinKa*cos(r2PI*phi) ,sinKa*sin(r2PI*phi) ,cosKa ]
  pj%E = PP%M - pk%E
  pj%V(1:3) =-pk%V(1:3)
  pk = pk%rotate_z(pa)
  pj = pj%rotate_z()
  pk = pk%boost( PP )
  pj = pj%boost( PP )
!
  do ll=1,jj-1
    if (ll.eq.kk) cycle
    psp1%p(psp1%b(ll)) = psp%p(psp%b(ll))
  enddo
  do ll=jj+1,psp1%Nfinst
    if (ll.eq.kk) cycle
    psp1%p(psp1%b(ll)) = psp%p(psp%b(ll-1))
  enddo
!
  end associate
  end subroutine


  subroutine wght_infin( weight ,psp1,psp ,xx,zz ,aa,jj,kk )
!********************************************************************
!* Soft limit when x->0, not x->1
!********************************************************************
  !(realknd2!)      ,intent(out)   :: weight ,xx,zz
  type(psPoint_type),intent(in   ) :: psp1
  type(psPoint_type),intent(inout) :: psp
  integer      ,intent(in)    :: aa,jj,kk
  !(realknd2!) :: hh,pInst(0:3,-2:-1),zmin,pjpk2
  integer :: ll,kt
!
  weight = rZRO
!
  kt = kk
  if (kt.gt.jj) kt=kt-1
!
  associate( pa=>psp1%p(psp1%b(aa)) & ! p_a, with negative energy
            ,pj=>psp1%p(psp1%b(jj)) &
            ,pk=>psp1%p(psp1%b(kk)) &
            ,PP=>psp1%p(psp1%b(jj)+psp1%b(kk)) &
            ,pka=>psp1%p(psp1%b(kk)+psp1%b(aa)) & ! pk+pa
            , Q=>psp1%p(psp1%b(aa)+psp1%b(jj)+psp1%b(kk)) )
!
  pjpk2 = (PP%M-pk%M)*(PP%M+pk%M)
  hh = Q%S-PP%S
  xx =-pjpk2/hh
  zz = (pka%S-pk%S)/hh
  hh = abs(Q%S-pk%S)
  zmin = (1-xx)*pk%S/hh
  zmin = zmin/(xx+zmin)
  zz = (zz-zmin)/(1-zmin)
  weight = r2PI*hh*(1-zmin)/(4*(1-xx)) ! extra factor (2pi)^3
!
  pInst(  0,-2) = psp1%p(psp1%b(-2))%E
  pInst(1:3,-2) = psp1%p(psp1%b(-2))%V(1:3)
  pInst(  0,-1) = psp1%p(psp1%b(-1))%E
  pInst(1:3,-1) = psp1%p(psp1%b(-1))%V(1:3)
  pInst(:,aa) = (1-xx)*pInst(:,aa) 
  call psp%put_inst( pInst ,[rZRO,rZRO] ,.true. )
  psp%p(psp%b(kt)) = Q - psp%p(psp%b(aa))
!
  do ll=1,jj-1
    if (ll.eq.kt) cycle
    psp%p(psp%b(ll)) = psp1%p(psp1%b(ll))
  enddo
  do ll=jj,psp%Nfinst
    if (ll.eq.kt) cycle
    psp%p(psp%b(ll)) = psp1%p(psp1%b(ll+1))
  enddo
!
  end associate
  end subroutine


  subroutine gnrt_inin( discard ,psp1,psp ,xx,zz,phi ,aa,jj )
!********************************************************************
!********************************************************************
  logical           ,intent(inout) :: discard
  type(psPoint_type),intent(inout) :: psp1
  type(psPoint_type),intent(in   ) :: psp
  integer      ,intent(in) :: aa,jj
  !(realknd2!) ,intent(in) :: xx,zz,phi
  !(realknd2!) :: cosja,sinja
  integer :: ll
  logical :: update
!
  discard = .false.
!
  associate( pj=>psp1%p(psp1%b(jj)) &
            , Q=>psp1%p(psp1%AllFinal) & ! -p_a-p_b should be defined
            , K=>psp1%p(psp1%AllFinal-psp1%b(jj)) &
            ,Kt=>psp%p(psp%AllFinal) )
!
  pj%S = pj%M**2
  K%S = Kt%S
  K%M = Kt%M
!
  pj%E = (1-xx)*Q%S/(2*Q%M)
  cosja = pj%E - zz*Q%M
  sinja = (pj%E-cosja)*(pj%E+cosja)
  if (sinja.lt.rZRO) then
    !if (errru.gt.0) write(errru,*) 'ERROR in Kaleu CS gnrt_inin: ' &
    !  ,'|cosja| >1, discard event'
    discard = .true.
    return
  endif
  sinja = sqrt(sinja)
  pj%V(1:3) = [ sinja*cos(r2PI*phi) ,sinja*sin(r2PI*phi) ,cosja ]
! "pj(0:3)" is momentum in restframe of "Q" with "Lpa=L(Q->rest)pa"
! positive along the z-axis. Notice that "Q" and "pa" are already along
! the z-axis, so the rotating "Lpa" out of the z-axis is 1 or -1.
  if (psp1%p(psp1%b(aa))%V(3).gt.rZRO) pj%V =-pj%V ! pa = -pp(,aa) positive energy
  pj = pj%boost( Q )
  K = Q - pj
!
  update = .true.
  do ll=1,jj-1
    psp1%p(psp1%b(ll)) = psp%p(psp%b(ll))%boost( K ,Kt ,update )
    update = .false.
  enddo
  do ll=jj+1,psp1%Nfinst
    psp1%p(psp1%b(ll)) = psp%p(psp%b(ll-1))%boost( K ,Kt ,update )
    update = .false.
  enddo
!
  end associate
  end subroutine


  subroutine wght_inin( weight ,psp1,psp ,xx,zz ,aa,jj )
!********************************************************************
!********************************************************************
  !(realknd2!)      ,intent(out)   :: weight ,xx,zz
  type(psPoint_type),intent(in   ) :: psp1
  type(psPoint_type),intent(inout) :: psp
  integer      ,intent(in)    :: aa,jj
  !(realknd2!) :: pInst(0:3,-2:-1)
  integer :: ll
  logical :: update
!
  weight = rZRO
!
  associate(  pj=>psp1%p(psp1%b(jj)) &
            ,  Q=>psp1%p(psp1%AllFinal) &
            ,  K=>psp1%p(psp1%AllFinal-psp1%b(jj)) &
            ,paj=>psp1%p(psp1%b(aa)+psp1%b(jj)) & ! pa+pj
            ,Kt=>psp%p(psp%AllFinal) )
!
  xx = K%S/Q%S
  zz =-paj%S/Q%S
  weight = r2PI*K%S*(1-xx)/(4*xx) ! extra factor (2pi)^3
!
  pInst(  0,-2) = psp1%p(psp1%b(-2))%E
  pInst(1:3,-2) = psp1%p(psp1%b(-2))%V(1:3)
  pInst(  0,-1) = psp1%p(psp1%b(-1))%E
  pInst(1:3,-1) = psp1%p(psp1%b(-1))%V(1:3)
  pInst(:,aa) = xx*pInst(:,aa) 
  call psp%put_inst( pInst ,[rZRO,rZRO] ,.true. )
!
  update = .true.
  do ll=1,jj-1
    psp%p(psp%b(ll)) = psp1%p(psp1%b(ll))%boost( Kt ,K ,update )
    update = .false.
  enddo
  do ll=jj,psp%Nfinst
    psp%p(psp%b(ll)) = psp1%p(psp1%b(ll+1))%boost( Kt ,K ,update )
    update = .false.
  enddo
!
  end associate
  end subroutine


end module


