module avh_kaleu_splvar
  use avh_iounits
  use avh_mathcnst
  use avh_prnt
  implicit none

  private
  public :: splvar_type

  integer,private,parameter :: lablen=8 !LABLEN

  type :: splvar_type ! m=minus,l=left,r=right,p=plus
    private
    !(realknd2!) :: x,a
    !(realknd2!) :: el,er
    !(realknd2!) :: sl,sr
    !(realknd2!) :: pl,pr
    !(realknd2!) :: dl,ol,dr,or
    !(realknd2!) :: pm,pp
    !(realknd2!) :: wm,wl,wr,wp
    !(realknd2!) :: emin,emax,cutoff
    integer         :: idat=0,nbatch=0
    character(lablen) :: lbl=''
  contains
    procedure :: init
    procedure :: gnrt
    procedure :: wght
    procedure :: collect
    procedure :: plot
  end type

contains

  subroutine init( obj ,label ,aa ,nbatch ,emin ,emax ,cutoff )
!**********************************************************************
!* Automatically adapts to integrand of the form
!*
!*           / wl*x^(el-1)     0 < x < a
!*   f(x) = < 
!*           \ wr*(1-x)^(er-1) a < x < 1
!*
!* for arbitray unknown (but positive) parameters  wl,el,wr,er
!* Takes  a=0.5  if this routine is not called.
!* Below  cutoff  and above  1-cutoff, the density is constant,
!* such that it is continuous at  x=cutoff  and at  x=1-cutoff
!**********************************************************************
  class(splvar_type) :: obj
  !(realknd2!),intent(in) :: aa,emin,emax,cutoff
  character(*),intent(in) :: label
  integer,intent(in) :: nbatch
  optional :: label,aa,nbatch,emin,emax,cutoff
!
  obj%lbl=''      ;if(present(label))  obj%lbl=adjustl(label)
  obj%a=rHLF      ;if(present(aa))     obj%a=aa
  obj%nbatch=1000 ;if(present(nbatch)) obj%nbatch=nbatch
  obj%emin=1d-2   ;if(present(emin))   obj%emin=emin
  obj%emax=2.5d0  ;if(present(emax))   obj%emax=emax
  obj%cutoff=1d-6 ;if(present(cutoff)) obj%cutoff=cutoff
!
  obj%x=-rONE ;obj%a=rHLF
  obj%el=rZRO                              ;obj%er=rZRO
  obj%sl=rZRO                              ;obj%sr=rZRO
  obj%pl=rHLF-obj%cutoff                   ;obj%pr=rHLF-obj%cutoff
  obj%dl=rHLF-obj%cutoff;obj%ol=obj%cutoff ;obj%dr=rHLF-obj%cutoff;obj%or=obj%cutoff
  obj%pm=obj%cutoff;obj%pp=obj%cutoff
  obj%wm=rZRO;obj%wl=rZRO;obj%wr=rZRO;obj%wp=rZRO
!
  if (obj%a.le.obj%cutoff.or.rONE-obj%cutoff.le.obj%a) then
    if (errru.gt.0) write(errru,*) 'ERROR in Kaleu splvar_init: ' &
      ,'aa=',trim(prnt(obj%a,7)),', while it should be ' &
      ,'between ',trim(prnt(obj%cutoff,7)),' ' &
      ,'and ',trim(prnt(rONE-obj%cutoff,7))
    stop
  endif
  obj%dl =      obj%a-obj%cutoff
  obj%dr = rONE-obj%a-obj%cutoff
  end subroutine

  function gnrt( obj ,rho ) result(xx)
!**********************************************************************
!**********************************************************************
  class(splvar_type) :: obj
  !(realknd2!),intent(in)  :: rho
  !(realknd2!) :: xx
  if     (rho.le.obj%pm) then
    xx = rho/obj%pm
    xx = obj%cutoff*xx
!
  elseif (rho.le.obj%pm+obj%pl) then
    xx = ( rho - obj%pm )/obj%pl
    if (obj%el.gt.rZRO) then
      xx = ( obj%dl*xx + obj%ol )**(rONE/obj%el)
    else
      xx = (obj%a-obj%cutoff) * xx + obj%cutoff
    endif
!
  elseif (rho.lt.rONE-obj%pp) then
    xx = ( (rho-obj%pl) - obj%pm )/obj%pr
    xx = rONE-xx
    if (obj%er.gt.rZRO) then
      xx = ( obj%dr*xx + obj%or )**(rONE/obj%er)
    else
      xx = (rONE-obj%a-obj%cutoff) * xx + obj%cutoff
    endif
    xx = rONE-xx
!
  else
!    xx = (rONE-rho)/obj%pp
!    xx = rONE - obj%cutoff*xx
    xx = obj%cutoff/obj%pp
    xx = (rONE-xx) + xx*rho
!
  endif
  obj%x = xx
  end function 

  subroutine wght( obj ,ww,rho ,xx )
!**********************************************************************
!**********************************************************************
  class(splvar_type) :: obj
  !(realknd2!),intent(out) :: ww,rho
  !(realknd2!),intent(in)  :: xx
  if (xx.le.rZRO.or.rONE.le.xx) then
    ww = rZRO
    rho = rZRO
    return
  endif
  obj%x = xx
!
  if     (obj%x.le.obj%cutoff) then
    ww = obj%cutoff/obj%pm
    rho = xx/ww
!
  elseif (obj%x.le.obj%a) then
    if (obj%el.gt.rZRO) then
!      ww = obj%dl/obj%el * xx**(rONE-obj%el)
      rho = xx**obj%el
      ww = obj%dl/obj%el * xx/rho
      rho = ( rho-obj%ol )/obj%dl
    else
      ww = obj%a-obj%cutoff
      rho = ( xx-obj%cutoff )/ww
    endif
    ww = ww/obj%pl
    rho = obj%pl*rho + obj%pm
!
  elseif (obj%x.lt.rONE-obj%cutoff) then
    if (obj%er.gt.rZRO) then
!      ww = obj%dr/obj%er * (rONE-xx)**(rONE-obj%er)
      rho = rONE-xx
      rho = rho**obj%er
      ww = obj%dr/obj%er * (rONE-xx)/rho
      rho = ( rho-obj%or )/obj%dr
    else
      ww = rONE-obj%a-obj%cutoff
      rho = ( rONE-xx-obj%cutoff )/ww
    endif
    ww = ww/obj%pr
    rho = obj%pr*(rONE-rho) + obj%pm + obj%pl
!
  else
    ww = obj%cutoff/obj%pp
!    rho = rONE - (rONE-xx)/ww
    rho = (rONE-rONE/ww) + xx/ww
!
  endif 
  end subroutine

  subroutine collect( obj ,ww )
!**********************************************************************
!**********************************************************************
  class(splvar_type) :: obj
  !(realknd2!),intent(in) :: ww
  !(realknd2!) :: hh
!
  if (ww.le.rZRO.or.obj%x.le.rZRO.or.obj%x.ge.rONE) return
!
  if     (obj%x.le.obj%cutoff) then
    obj%wm = obj%wm + ww
  elseif (obj%x.le.obj%a) then
    obj%wl = obj%wl + ww
    hh = obj%a/obj%x
    if (hh.gt.rZRO) obj%sl = obj%sl + ww*log( hh ) 
  elseif (obj%x.lt.rONE-obj%cutoff) then
    obj%wr = obj%wr + ww
    hh = (rONE-obj%a)/(rONE-obj%x)
    if (hh.gt.rZRO) obj%sr = obj%sr + ww*log( hh ) 
  else
    obj%wp = obj%wp + ww
  endif
  obj%idat = obj%idat+1
  obj%x = -rONE ! to make sure x is used only once
!
  if (mod(obj%idat,obj%nbatch).eq.0) then
    if (obj%sl.ne.rZRO) obj%el = obj%wl/obj%sl
    if (obj%sr.ne.rZRO) obj%er = obj%wr/obj%sr
    if (obj%el.le.obj%emin) obj%el = obj%emin
    if (obj%er.le.obj%emin) obj%er = obj%emin
    if (obj%el.ge.obj%emax) obj%el = obj%emax
    if (obj%er.ge.obj%emax) obj%er = obj%emax
!
    obj%ol = obj%cutoff**obj%el
    obj%dl = obj%a**obj%el - obj%ol
    obj%or = obj%cutoff**obj%er
    obj%dr = (rONE-obj%a)**obj%er - obj%or
!
!    hh = rZRO
    hh = obj%wm+obj%wp
    hh = hh + obj%wl+obj%wr
    if (hh.gt.rZRO) then
!      obj%pl = (obj%wm+obj%wl)/hh
!      obj%pr = (obj%wr+obj%wp)/hh
      obj%pl = obj%wl/hh
      obj%pr = obj%wr/hh
      obj%pm = obj%pl * obj%el/obj%dl * obj%cutoff**obj%el ! continuity
      obj%pp = obj%pr * obj%er/obj%dr * obj%cutoff**obj%er ! continuity
      hh = obj%pm+obj%pp
      hh = hh + obj%pl+obj%pr
      obj%pm = obj%pm/hh
      obj%pl = obj%pl/hh
      obj%pr = obj%pr/hh
      obj%pp = obj%pp/hh
    endif
  endif
  end subroutine

  subroutine plot( obj ,iunit )
!**********************************************************************
!**********************************************************************
  class(splvar_type),intent(in) :: obj
  integer           ,intent(in) :: iunit
  character(lablen+3) :: filename
  character(15) :: pl,el,pr,er,aa,cl,cr,o0,o1,pm,pp
  if (iunit.le.0) return
  filename = 'spl'//trim(obj%lbl)
  open( unit=iunit, file=filename, status="replace" )
  write(iunit,*) '# soft cut-off    :',obj%cutoff
  write(iunit,*) '# border          :',obj%a
  write(iunit,*) '# exponents       :',obj%el,obj%er
  write(iunit,*) '# relative weights:',obj%pl,obj%pr
  write(iunit,*) '# weights beyond cut-offs:',obj%pm,obj%pp
  write(pl,'(e15.8)') obj%pl*obj%el/obj%dl
  write(el,'(e15.8)') obj%el-rONE
  write(pr,'(e15.8)') obj%pr*obj%er/obj%dr
  write(er,'(e15.8)') obj%er-rONE
  write(aa,'(e15.8)') obj%a
  write(cl,'(e15.8)') obj%cutoff
  write(cr,'(e15.8)') rONE-obj%cutoff
  write(o0,'(e15.8)') rZRO
  write(o1,'(e15.8)') rONE
  write(pm,'(e15.8)') obj%pm/obj%cutoff
  write(pp,'(e15.8)') obj%pp/obj%cutoff
  write(iunit,*) 'theta(x) = (1+sgn(x))/2'
  write(iunit,*) 'bin(x,y,z) = theta(x-y)*theta(z-x)'
  write(iunit,*) 'fm(x) = bin(x,',o0,',',cl,')*',pm
  write(iunit,*) 'fl(x) = bin(x,',cl,',',aa,')*',pl,'*   x **(',el,')'
  write(iunit,*) 'fr(x) = bin(x,',aa,',',cr,')*',pr,'*(1-x)**(',er,')'
  write(iunit,*) 'fp(x) = bin(x,',cr,',',o1,')*',pp
  write(iunit,*) 'plot [0:1] fm(x)+fl(x)+fr(x)+fp(x)'
  close( unit=iunit )
  end subroutine

end module


!! program test
!! use !(rngmodule!)
!!   use avh_kaleu_kinds
!!   use avh_kaleu_splvar
!!   use avh_kaleu_grid
!!   implicit none
!!   type(splvar_type) :: splvar
!!   type(grid_type) :: grid
!!   integer ,parameter :: nev = 50000
!!   !(realknd2!) ,parameter :: aa=0.3_kindr2
!!   !(realknd2!) ,parameter :: wl=0.2_kindr2,wr=0.8_kindr2
!!   !(realknd2!) ,parameter :: el=0.41_kindr2,er=0.25_kindr2
!!   !(realknd2!) :: s0=rZRO,s1=rZRO,s2=rZRO
!!   !(realknd2!) :: ww,xx,h1,h2,rho,cutoff=5*R1M9
!!   integer :: iev
!! !
!!   call splvar%init( 0.3_kindr2 ,'Example0' )
!!   call grid%init( 100,100,rZRO ,'Example0' )
!! !
!!   do iev=1,nev
!!     call rangen( rho )
!!     call splvar%gnrt( xx ,rho )
!!     call splvar%gnrt( ww,h1 ,xx )
!!     if (abs(rONE-rho/h1).gt.R1M12) write(6,*) abs(rho/h1),rho !DEBUG
!! !    call rangen( xx )
!! !    ww = rONE
!!     if (xx.le.cutoff.or.rONE-cutoff.le.xx) then
!!       ww = rZRO
!!     elseif (xx.le.aa) then
!!       ww = ww * wl * xx**(el-rONE) 
!!     else
!!       ww = ww * wr * (rONE-xx)**(er-rONE)
!!     endif
!!     call splvar%collect( ww )
!!     s0 = s0 + rONE
!!     s1 = s1 + ww
!!     s2 = s2 + ww*ww
!!     call grid%collect( ww ,xx )
!!   enddo
!!   call splvar%plot( 21 )
!!   s1 = s1/s0
!!   s2 = sqrt( ( s2/s0-s1*s1 )/( s0-rONE ) )
!!   write(6,*) 'result:',s1,s2,s2/s1
!! !
!!   ww = wl * (      aa**(el)-cutoff**(el))/el &
!!      + wr * ((rONE-aa)**(er)-cutoff**(er))/er
!!   write(6,*) 'result:',ww
!! !
!!   call grid%plot( 21 )
!! !
!!   do iev=1,100
!!     xx = (rONE*iev)/100
!!     if (xx.le.aa) then
!!       h2 = wl * xx**(el-rONE) 
!!     else
!!       h2 = wr * (rONE-xx)**(er-rONE)
!!     endif
!!     write(21,*) xx,h2/ww
!!   enddo
!! !
!! end program
