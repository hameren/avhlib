module avh_kaleu_stats
  use avh_mathcnst
  use avh_prnt
  use avh_iotools
  use avh_kaleu_units

  implicit none
  private
  public :: kaleu_stats_type ,rng_interface ,kaleu_unweight

  abstract interface
    function rng_interface() result(rslt)
    !(realknd2!) :: rslt
    end function
  end interface 

  type :: kaleu_stats_type
!
    integer :: Nnegative=0,Nzero=0,Npositive=0,NnonZero=0,Ntot=0
    integer :: Nevents=0
    logical :: negativeWeight=.false.,zeroWeight=.false.
    logical :: positiveWeight=.false.,nonZeroWeight=.false.
    logical :: NaN=.false.,unweight=.true.
    !(realknd2!) :: weight,absWeight,sqrWeight
!
    !(realknd2!) :: W1pos,W2pos,W1neg,W2neg
    !(realknd2!),allocatable :: Wlist(:)
    !(realknd2!) :: Nnext,Nmessage=20
    !(realknd2!) :: optAvePos,optSigPos,optAveNeg,optSigNeg
    logical :: firstWrite=.true.,accepted=.false.
    character(144) :: eventFile='eventFile'
    integer :: writeUnit=71,Nlist=1024
!
    integer :: Nbatch,Nstep,NbatchGrid,NstepGrid,Noptim
    !(realknd2!) :: ChThrs
    logical :: optimPhase=.true.,restart=.false.
!
    procedure(rng_interface),pointer,nopass :: rng
!
  contains
!
    procedure :: init
    procedure :: collect
    procedure :: update
    procedure :: average
    procedure :: print_result
    procedure :: finish
!
  end type


contains

  
  subroutine init( obj &
                  ,ChThrs,Nbatch,Nstep,NbatchGrid,NstepGrid,Noptim &
                  ,rng ,writeUnit ,eventFile ,Nmessage ,Nlist &
                 )
  class(kaleu_stats_type) :: obj
  procedure(rng_interface),optional :: rng
  integer,intent(in),optional :: writeUnit,Nmessage,Nlist
  character(*),intent(in),optional :: eventFile
  !(realknd2!),intent(in),optional :: ChThrs
  integer,intent(in),optional :: Nbatch,Nstep,NbatchGrid,NstepGrid,Noptim
!
  obj%unweight = present(rng)
!
  obj%Nnegative=0
  obj%Nzero=0
  obj%Npositive=0
  obj%NnonZero=0
  obj%Ntot=0
  obj%Nevents=0
  obj%negativeWeight=.false.
  obj%zeroWeight=.false.
  obj%positiveWeight=.false.
  obj%nonZeroWeight=.false.
!
  obj%W1pos=0 ;obj%W1neg=0;
  obj%W2pos=0 ;obj%W2neg=0;
  obj%optAvePos=0 ;obj%optAveNeg=0
  obj%optSigPos=0 ;obj%optSigNeg=0
  if (present(writeUnit)) obj%writeUnit=writeUnit
  if (present(eventFile)) obj%eventFile=adjustl(eventFile)
  if (present(Nmessage )) obj%Nmessage=Nmessage
  if (present(Nlist    )) obj%Nlist=Nlist
  if (allocated(obj%Wlist)) deallocate(obj%Wlist)
  if (obj%unweight) then
    obj%rng => rng
    allocate(obj%Wlist(1:obj%Nlist)) ;obj%Wlist=0
    open(obj%writeUnit,file=trim(obj%eventFile)//'.info',status='new')
    if (inpUnit.gt.0) write(inpUnit,*) 'MESSAGE from Kaleu stats: ' &
      ,'eventFile = ',trim(obj%eventFile)
  endif
  obj%firstWrite = .true.
  obj%accepted = .false.
  obj%Nnext = obj%Nmessage**2
!
  obj%optimPhase = .true.
  obj%restart    = .false.
  obj%ChThrs    =1d-3   ;if (present(ChThrs    )) obj%ChThrs    =ChThrs
  obj%Noptim    =10000  ;if (present(Noptim    )) obj%Noptim    =Noptim
  obj%Nbatch    =10000  ;if (present(Nbatch    )) obj%Nbatch    =Nbatch
  obj%Nstep     =10     ;if (present(Nstep     )) obj%Nstep     =Nstep
  obj%NbatchGrid=1000   ;if (present(NbatchGrid)) obj%NbatchGrid=NbatchGrid
  obj%NstepGrid =100    ;if (present(NstepGrid )) obj%NstepGrid =NstepGrid
  obj%Noptim = max(0,obj%Noptim &
                  ,obj%Nbatch*obj%Nstep &
                  ,obj%NbatchGrid*obj%NstepGrid )
  obj%Nbatch = obj%Noptim/max(1,obj%Nstep)
  obj%NbatchGrid = obj%Noptim/max(1,obj%NstepGrid)
  if (obj%Nstep.le.0) obj%Nbatch=-1
  if (obj%NstepGrid.le.0) obj%NbatchGrid=-1
  if (inpUnit.gt.0) write(inpUnit,*) 'MESSAGE from Kaleu stats: ' &
    ,'ChThrs = ',prnt(obj%ChThrs,'lft')
  if (inpUnit.gt.0) write(inpUnit,*) 'MESSAGE from Kaleu stats: ' &
      ,'Nbatch = ',trim(prnt(obj%Nbatch,'lft')) &
    ,' , Nstep = ',trim(prnt(obj%Nstep,'lft'))
  if (inpUnit.gt.0) write(inpUnit,*) 'MESSAGE from Kaleu stats: ' &
      ,'NbatchGrid = ',trim(prnt(obj%NbatchGrid,'lft')) &
    ,' , NstepGrid = ',trim(prnt(obj%NstepGrid,'lft'))
  if (inpUnit.gt.0) write(inpUnit,*) 'MESSAGE from Kaleu stats: ' &
    ,'Nopt = ',prnt(obj%Noptim,'lft')
!
  end subroutine


  subroutine collect( obj ,weight )
  class(kaleu_stats_type) :: obj
  !(realknd2!),intent(in) :: weight
  if (weight.gt.rZRO) then
    obj%NaN = .false.
    obj%weight = weight
    obj%absWeight = abs(obj%weight)
    obj%sqrWeight = obj%weight*obj%weight
    obj%Ntot = obj%Ntot+1
    obj%Npositive = obj%Npositive+1
    obj%NnonZero  = obj%NnonZero+1
    obj%negativeWeight = .false.
    obj%positiveWeight = .true.
    obj%zeroWeight     = .false.
    obj%nonZeroWeight  = .true.
    obj%W1pos = obj%W1pos+obj%absWeight
    obj%W2pos = obj%W2pos+obj%sqrWeight
  elseif (weight.eq.rZRO) then
    obj%NaN = .false.
    obj%weight = rZRO
    obj%absWeight = rZRO
    obj%sqrWeight = rZRO
    obj%Ntot = obj%Ntot+1
    obj%Nzero = obj%Nzero+1
    obj%negativeWeight = .false.
    obj%positiveWeight = .false.
    obj%zeroWeight     = .true.
    obj%nonZeroWeight  = .false.
    return
  elseif (weight.lt.rZRO) then
    obj%NaN = .false.
    obj%weight = weight
    obj%absWeight = abs(obj%weight)
    obj%sqrWeight = obj%weight*obj%weight
    obj%Ntot = obj%Ntot+1
    obj%Nnegative = obj%Nnegative+1
    obj%NnonZero  = obj%NnonZero+1
    obj%negativeWeight = .true.
    obj%positiveWeight = .false.
    obj%zeroWeight     = .false.
    obj%nonZeroWeight  = .true.
    obj%W1neg = obj%W1neg+obj%absWeight
    obj%W2neg = obj%W2neg+obj%sqrWeight
  else
    obj%NaN = .true.
    obj%weight = rZRO
    obj%absWeight = rZRO
    obj%sqrWeight = rZRO
    obj%negativeWeight = .false.
    obj%positiveWeight = .false.
    obj%zeroWeight     = .false.
    obj%nonZeroWeight  = .false.
    if (errru.ge.0) write(errru,*) 'ERROR in Kaleu stats: weight=',weight
    return
  endif
  end subroutine


  subroutine update( obj ,efficiency )
  class(kaleu_stats_type) :: obj
  !(realknd2!),intent(in),optional :: efficiency
  !(realknd2!) :: Wevent,ave
  integer :: i0,i1,ii
!
  obj%restart = .false.
  obj%accepted = .false.
!
  if (obj%NnonZero.eq.obj%Nnext) then
    obj%Nnext = (sqrt(obj%Nnext)+obj%Nmessage)**2
    if (progUnit.gt.0) then
      write(progUnit,*) 'MESSAGE from Kaleu stats: Ntot = ' &
                       ,adjustl(print_whole(rONE*obj%Ntot))
      write(progUnit,*) 'MESSAGE from Kaleu stats: +',obj%print_result(8)
      if (obj%Nnegative.ne.0) &
      write(progUnit,*) 'MESSAGE from Kaleu stats: -',obj%print_result(8,-1)
    endif
  endif
!
  if (obj%optimPhase.and.obj%NnonZero.eq.obj%Noptim) then
!
    obj%optimPhase = .false.
    obj%restart = .true.
    if (progUnit.gt.0) then
      write(progUnit,*) 'MESSAGE from Kaleu stats: ' &
                       ,'#################################################'
      write(progUnit,*) 'MESSAGE from Kaleu stats: Ntot = ' &
                       ,adjustl(print_whole(rONE*obj%Ntot))
      write(progUnit,*) 'MESSAGE from Kaleu stats: +',obj%print_result(8)
      if (obj%Nnegative.ne.0) &
      write(progUnit,*) 'MESSAGE from Kaleu stats: -',obj%print_result(8,-1)
      write(progUnit,*) 'MESSAGE from Kaleu stats: stopping optimization,' &
                                                ,' re-starting collection'
      write(progUnit,*) 'MESSAGE from Kaleu stats: ' &
                       ,'#################################################'
    endif
    call average( obj ,obj%optAvePos ,obj%optSigPos ,obj%optAveNeg ,obj%optSigNeg )
    obj%Nnext = obj%Nmessage**2
    obj%Nnegative=0
    obj%Nzero=0
    obj%Npositive=0
    obj%NnonZero=0
    obj%Ntot=0
    obj%W1pos=0 ;obj%W1neg=0;
    obj%W2pos=0 ;obj%W2neg=0;
!
  elseif (.not.obj%optimPhase.and.obj%unweight) then
!
    if (obj%Wlist(1).lt.obj%absWeight) then
      if (obj%Wlist(obj%Nlist).lt.obj%absWeight) then
        obj%Wlist(1:obj%Nlist-1) = obj%Wlist(2:obj%Nlist)
        obj%Wlist(obj%Nlist) = obj%absWeight
      else
        i0 = 1
        i1 = obj%Nlist
        do while (i1-i0.gt.1)
          ii = (i0+i1)/2
          if (obj%absWeight.le.obj%Wlist(ii)) then
            i1 = ii
          else
            i0 = ii
          endif
        enddo
        obj%Wlist(1:i0-1) = obj%Wlist(2:i0)
        obj%Wlist(i0) = obj%absWeight
      endif
    endif
!
    if (obj%Ntot.eq.0) then
      ave = 0
    else
      ave = (obj%W1pos+obj%W1neg)/obj%Ntot
    endif
!
    if (present(efficiency)) then
      Wevent = min(obj%Wlist(obj%Nlist),ave/efficiency)
    else
      Wevent = min(obj%Wlist(obj%Nlist),ave*1024)
    endif
    obj%accepted = (Wevent.le.obj%absWeight)
    if (obj%accepted) then
      Wevent = obj%absWeight
    else
      obj%accepted = (obj%rng()*Wevent.le.obj%absWeight)
    endif
!  
    if (obj%accepted) then
      obj%Nevents = obj%Nevents+1
      if (obj%firstWrite) then
        obj%firstWrite = .false.
        close(obj%writeUnit)
        open(obj%writeUnit,file=trim(obj%eventFile),status='new')
      endif
      write(obj%writeUnit,'(a16,e24.16,i10,4e23.16)') 'avh_kaleu_weight' &
        ,sign(Wevent,obj%weight) &
        ,obj%Ntot,obj%W1pos,obj%W2pos,obj%W1neg,obj%W2neg
    endif

  endif
!
  end subroutine


  subroutine average( obj ,avePos ,sigPos ,aveNeg ,sigNeg )
  class(kaleu_stats_type) :: obj
  !(realknd2!),intent(out) :: avePos,sigPos
  !(realknd2!),intent(out),optional :: aveNeg,sigNeg
  call average_h( avePos ,sigPos ,obj%Ntot ,obj%W1pos ,obj%W2pos )
  if (present(aveNeg)) then
    call average_h( aveNeg ,sigNeg ,obj%Ntot ,obj%W1neg ,obj%W2neg )
  endif
  end subroutine


  subroutine average_h( ave ,sig ,Ntot ,W1tot ,W2tot )
  !(realknd2!),intent(out) :: ave,sig
  !(realknd2!),intent(in) :: W1tot,W2tot
  integer,intent(in) :: Ntot
  if (Ntot.le.0.or.W1tot.le.rZRO) then
    ave = 0
    sig = 0
  elseif (Ntot.le.1) then
    ave = W1tot
    sig = ave
  else
    ave = W1tot/Ntot
    sig = W2tot/Ntot - ave*ave
    if (sig.le.rZRO) then
      sig = ave
    else
      sig = sqrt( sig/(Ntot-1) )
    endif
  endif
  end subroutine


  function print_result( obj ,nDec ,negative ) result(line)
  class(kaleu_stats_type),intent(in) :: obj
  integer,intent(in) :: nDec
  integer,intent(in),optional :: negative
  character(33+2*nDec) :: line
  integer :: i1,i2
  !(realknd2!) :: xx,yy,rr,tt,w0
  character(33+2*nDec) :: aa,bb,cc,ee
  if (present(negative)) then
    if (negative.eq.-1) then
      call average_h( xx,yy ,obj%Ntot ,obj%W1neg ,obj%W2neg )
      w0 = obj%Nnegative
    else
      call average_h( xx,yy ,obj%Ntot ,obj%W1pos ,obj%W2pos )
      w0 = obj%Npositive
    endif
  else
    call average_h( xx,yy ,obj%Ntot ,obj%W1pos ,obj%W2pos )
    w0 = obj%Npositive
  endif
  rr=0 ;if (xx.ne.rZRO) rr=yy/xx
  write(aa,'(e23.16)') xx
  ee(1:2) = '1.'
  ee(3:6) = aa(20:23)
  read(ee(1:6),*) tt
  write(bb,'(f18.16)') abs(yy)/tt
  write(cc,'(f15.12)') abs(rr*100)
  line = print_whole(w0)//' ('//aa(3:3+nDec)//'+/-'//bb(2:2+nDec)//')'//ee(3:6) &
       //' '//cc(1:6)//'%'
  end function

  function print_whole( xx ) result(rslt)
  !(realknd2!),intent(in) :: xx
  character(24) :: aa
  character(12) :: rslt
  write(aa,'(f13.0)') xx
  if (aa(9:9).ne.' ') aa(1:9) = aa(2:9)//','
  if (aa(5:5).ne.' ') aa(1:5) = aa(2:5)//','
  rslt = aa(1:12)
  end function
  
  
   

  subroutine finish( obj )
  class(kaleu_stats_type) :: obj
  !(realknd2!) :: avePos,sigPos,rerPos,aveNeg,sigNeg,rerNeg,sigDif,rerDif
  !(realknd2!) :: eps,Wsum
  integer :: ii,jj
!
  close(obj%writeUnit)
!
  if (progUnit.gt.0) then
    write(progUnit,*) 'MESSAGE from Kaleu stats: ' &
                     ,'#################################################'
    write(progUnit,*) 'MESSAGE from Kaleu stats: Ntot = ' &
                     ,adjustl(print_whole(rONE*obj%Ntot))
    write(progUnit,*) 'MESSAGE from Kaleu stats: +',obj%print_result(8)
    if (obj%Nnegative.ne.0) &
    write(progUnit,*) 'MESSAGE from Kaleu stats: -',obj%print_result(8,-1)
    write(progUnit,*) 'MESSAGE from Kaleu stats: ' &
                     ,'#################################################'
  endif
!
  call obj%average( avePos,sigPos ,aveNeg,sigNeg )
  rerPos=avePos ;if (rerPos.ne.rZRO) rerPos=sigPos/rerPos
  rerNeg=aveNeg ;if (rerNeg.ne.rZRO) rerNeg=sigNeg/rerNeg
  sigDif=sqrt( sigPos**2 + sigNeg**2 + 2*avePos*aveNeg/(obj%Ntot-1) )
  rerDif=abs(avePos-aveNeg) ;if (rerDif.ne.rZRO) rerDif=sigDif/rerDif
!
  open(obj%writeUnit,file=trim(obj%eventFile)//'.info',status='old',position='append')
  write(obj%writeUnit,'(a28,e24.16,a4,e23.16,f8.4,a1)') &
    'avh_kaleu average positive =',avePos,' +/-',sigPos,100*rerPos,'%'
  write(obj%writeUnit,'(a28,e24.16,a4,e23.16,f8.4,a1)') &
    'avh_kaleu average negative =',aveNeg,' +/-',sigNeg,100*rerNeg,'%'
  write(obj%writeUnit,'(a28,e24.16,a4,e23.16,f8.4,a1)') &
    'avh_kaleu average differnc =',avePos-aveNeg,' +/-',sigDif,100*rerDif,'%'
  write(obj%writeUnit,'(a25,i10,2e23.16)') &
    'avh_kaleu sums positive =',obj%Npositive,obj%W1pos,obj%W2pos
  write(obj%writeUnit,'(a25,i10,2e23.16)') &
    'avh_kaleu sums negative =',obj%Nnegative,obj%W1neg,obj%W2neg
  write(obj%writeUnit,'(a30,i10)') &
    'avh_kaleu Nevents total     = ',obj%Ntot
  write(obj%writeUnit,'(a30,i10)') &
    'avh_kaleu Nevents vanishing = ',obj%Nzero
  write(obj%writeUnit,'(a30,i10)') &
    'avh_kaleu Nevents eventFile = ',obj%Nevents
!
  Wsum = 0
  do ii=obj%Nlist,1,-1
    jj = obj%Nlist-ii
    eps = ( Wsum - jj*obj%Wlist(ii) )/abs(obj%W1pos-obj%W1neg)
    if (eps.gt.rerDif) exit
    Wsum = Wsum + obj%Wlist(ii)
    write(obj%writeUnit,'(a16,i5,2e23.16)') &
      'avh_kaleu tail =',jj+1,obj%Wlist(ii),eps
  enddo
  close(obj%writeUnit)
  end subroutine


  subroutine kaleu_unweight( rng ,inFiles ,outFile ,error ,freeUnits )
!***********************************************************************
! inFiles is a single character string that may contain several
! space-separated file names.
!
!program unweight
!  use avh_random
!  use avh_kaleu_stats
!  call kaleu_unweight( ranfun &
!                      ,inFiles='events1' &
!                            //' events2' & 
!                            //' events3' & 
!                      ,outFile='unwtdEvents' )
!end program
!
!***********************************************************************
  procedure(rng_interface) :: rng
  character(*),intent(in) :: inFiles,outFile
  !(realknd2!),intent(in),optional :: error
  integer,intent(in),optional :: freeUnits(3)
  type(charLine_type),allocatable :: lines(:)
  type(charLine_type) :: input
  character(maxLineLen) :: line
  !(realknd2!),allocatable :: s1pos(:),s2pos(:)
  !(realknd2!),allocatable :: s1neg(:),s2neg(:),NtotFile(:)
  !(realknd2!),allocatable :: Wlist(:,:),eList(:,:)
  integer,allocatable :: Nlist(:)
  integer :: ii,Nfiles,jj,kk,NacPos,NacNeg,NacPosTot,NacNegTot
  integer :: rUnit,pUnit,nUnit
  !(realknd2!) :: ave,sig,Wmax,Wtmp,eTmp,Ntot,relErr
  !(realknd2!) :: s1totp,s2totp,s1totn,s2totn
  rUnit = 21
  pUnit = 22
  nUnit = 23
  if (present(freeUnits)) then
    rUnit = freeUnits(1)
    pUnit = freeUnits(2)
    nUnit = freeUnits(3)
  endif
  input = put_charLine( inFiles )
  Nfiles = input%Nwords()
  allocate(s1pos(Nfiles),s2pos(Nfiles),Nlist(Nfiles))
  allocate(s1neg(Nfiles),s2neg(Nfiles),NtotFile(Nfiles))
  Nlist = 0
  do ii=1,Nfiles
    open(rUnit,file=input%word(ii)//'.info',status='old')
    call read_charLines( rUnit ,lines )
    do jj=1,ubound(lines,1)
      if (lines(jj)%Nwords().le.1) cycle
      select case (lines(jj)%word(1,3))
        case ('avh_kaleu sums positive')
          line=lines(jj)%word(6,7) ;read(line,*) s1pos(ii),s2pos(ii)
        case ('avh_kaleu sums negative')
          line=lines(jj)%word(6,7) ;read(line,*) s1neg(ii),s2neg(ii)
        case ('avh_kaleu Nevents total')
          line=lines(jj)%word(5) ;read(line,*) NtotFile(ii)
        case ('avh_kaleu tail =')
          line=lines(jj)%word(4,6) ;read(line,*) kk,Wtmp,eTmp
          call enlarge_r2( Wlist, 1,kk ,1,Nfiles )
          call enlarge_r2( eList, 1,kk ,1,Nfiles )
          Wlist(kk,ii) = Wtmp
          eList(kk,ii) = eTmp
          if (Nlist(ii).lt.kk) Nlist(ii) = kk
      end select
    enddo
    close(rUnit)
  enddo
  s1totp = sum(s1pos(1:Nfiles))
  s2totp = sum(s2pos(1:Nfiles))
  s1totn = sum(s1neg(1:Nfiles))
  s2totn = sum(s2neg(1:Nfiles))
  Ntot = sum(NtotFile(1:Nfiles))
  ave = abs(s1totp-s1totn)/Ntot
  sig = sqrt( ((s2totp+s2totn)/Ntot-ave*ave)/(Ntot-1) )
  if (present(error)) then
    relErr = error
  else
    relErr = sig/ave
  endif
  NacPosTot = 0
  NacNegTot = 0
  open(pUnit,file=trim(outFile)//'.pos',status='new')
  open(nUnit,file=trim(outFile)//'.neg',status='new')
  do ii=1,Nfiles
    do kk=1,Nlist(ii)
      if (eList(kk,ii).gt.relErr) exit
    enddo
    if (messu.gt.0) write(messu,*) 'Including events from ',input%word(ii)
    if (messu.gt.0) write(messu,*) '    with wMax(',prnt(kk-1,2),')=',prnt(Wlist(kk-1,ii),8)
    open(rUnit,file=input%word(ii),status='old')
    call unweight_h( rng ,rUnit ,pUnit ,nUnit ,Wlist(kk-1,ii) ,NacPos,NacNeg )
    if (messu.gt.0) write(messu,*) '    accepted',NacPos+NacNeg,' events'
    NacPosTot = NacPosTot + NacPos
    NacNegTot = NacNegTot + NacNeg
    close(rUnit)
  enddo
  if (messu.gt.0) write(messu,*) 'Accepted',NacPosTot+NacNegTot,' events in total'
  close(pUnit)
  close(nUnit)
  open(pUnit,file=trim(outFile)//'.info',status='new')
    write(pUnit,'(a19,3i12)') &
      'avh_kaleu Nevents =',NacPosTot+NacNegTot,NacPosTot,NacNegTot
    write(pUnit,'(a19,e24.16,a4,e23.16,f8.4,a1)') &
      'avh_kaleu average =',(s1totp-s1totn)/Ntot,' +/-',sig,100*sig/ave,'%'
    write(pUnit,'(a16,5e23.16)') &
      'avh_kaleu sums =',Ntot,s1totp,s2totp,s1totn,s2totn
    write(pUnit,'(A)') '#'
    write(pUnit,'(A)') '# Below the content of the .info files coming with ' &
                           //'the event files from'
    write(pUnit,'(A)') '# which '//trim(outFile)//' was created.'
    write(pUnit,'(A)') '#'
    do ii=1,Nfiles
      open(rUnit,file=input%word(ii)//'.info',status='old')
      call read_charLines( rUnit ,lines )
      write(pUnit,'(A)') '# '//input%word(ii)//'.info'
      do jj=1,ubound(lines,1)
        if (lines(jj)%word(1,1,11).eq.'avh_kaleu') cycle
        write(pUnit,'(A)') lines(jj)%prnt()
      enddo
    enddo
  close(pUnit)
  end subroutine

  
  subroutine unweight_h( rng ,rUnit ,pUnit ,nUnit ,wMax ,NacPos,NacNeg )
!***********************************************************************
!***********************************************************************
  procedure(rng_interface) :: rng
  integer,intent(in) :: rUnit ,pUnit,nUnit
  !(realknd2!),intent(in) :: wMax
  integer,intent(out) :: NacPos,NacNeg
  character(maxLineLen),allocatable :: lines(:),tmp(:)
  !(realknd2!) :: weight,nextWeight
  integer :: jj,kk,Nlines
!
  allocate(lines(1))
  read(rUnit,'(A)') lines(1)
  if (lines(1)(1:16).ne.'avh_kaleu_weight') then
    write(errru,*) 'ERROR in kaleu_unweight: ' &
      ,'this does not seem to be an unweightable file.'
    return
  endif
  read(lines(1)(18:40),*) weight
!
  NacPos = 0
  NacNeg = 0
  Nlines = 0
  do
    jj = 0
    do ;jj=jj+1
      if (jj.gt.Nlines) then
        allocate(tmp(Nlines))
        tmp = lines
        deallocate(lines)
        allocate(lines(jj))
        lines(1:Nlines) = tmp(1:Nlines)
        Nlines = jj
        deallocate(tmp)
      endif
      read(rUnit,'(A)',end=111) lines(jj)
      if (lines(jj)(1:16).eq.'avh_kaleu_weight') then
        read(lines(jj)(18:40),*) nextWeight
        exit
      endif
    enddo
    if (wMax.le.abs(weight)) then
      if (weight.ge.rZRO) then
        NacPos = NacPos+1
        do kk=1,jj-1
          write(pUnit,'(A)') trim(lines(kk))
        enddo
      else
        NacNeg = NacNeg+1
        do kk=1,jj-1
          write(nUnit,'(A)') trim(lines(kk))
        enddo
      endif
    else
      if (rng()*wMax.le.abs(weight)) then
        if (weight.ge.rZRO) then
          NacPos = NacPos+1
          do kk=1,jj-1
            write(pUnit,'(A)') trim(lines(kk))
          enddo
        else
          NacNeg = NacNeg+1
          do kk=1,jj-1
            write(nUnit,'(A)') trim(lines(kk))
          enddo
        endif
      endif
    endif
    weight = nextWeight
  enddo
  111 continue
!
  end subroutine


  subroutine enlarge_r2( xx ,l1,u1 ,l2,u2 )
  !(realknd2!) &
  include '../arrays/enlarge2.h90'
  end subroutine


end module


