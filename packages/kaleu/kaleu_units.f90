module avh_kaleu_units
  use avh_iounits
  implicit none

  integer,save,protected :: inpUnit=0
  integer,save,protected :: treeUnit=0
  integer,save,protected :: sizeUnit=0
  integer,save,protected :: gnrtUnit=0
  integer,save,protected :: modelUnit=0
  integer,save,protected :: mchUnit=0
  integer,save,protected :: nsUnit=0
  integer,save,protected :: progUnit=0

  logical,private,save :: initd=.false.

contains

  subroutine kaleu_hello
  use avh_mathcnst
  if (initd) return
  initd = .true.
  call init_mathcnst
!  write(*,'(a72)') '########################################################################'
!  write(*,'(a72)') '#                       You are using Kaleu-2.0                        #'
!  write(*,'(a72)') '#                                                                      #'
!  write(*,'(a72)') '#                     for phase space generation                       #'
!  write(*,'(a72)') '#                                                                      #'
!  write(*,'(a72)') '# author: Andreas van Hameren <hamerenREMOVETHIS@ifj.edu.pl>           #'
!  write(*,'(a72)') '#   date: 28-11-2013                                                   #'
!  write(*,'(a72)') '#                                                                      #'
!  write(*,'(a72)') '# Please cite                                                          #'
!  write(*,'(a72)') '#    A. van Hameren, arXiv:1003.4953 [hep-ph]                          #'
!  write(*,'(a72)') '# in publications with results obtained with the help of this program. #'
!  write(*,'(a72)') '########################################################################'
  end subroutine
 
  subroutine kaleu_set_unit( message ,nunit )
!**********************************************************************
!**********************************************************************
  character(*) ,intent(in) :: message
  integer      ,intent(in) :: nunit
  select case (message)
  case ('all')
        inpUnit=nunit
        mchUnit=nunit
        treeUnit=nunit
        modelUnit=nunit
        nsUnit=nunit
        progUnit=nunit
  case('NS','ns','CS','cs')
        nsUnit=nunit
  case('input')
        inpUnit=nunit
  case('mch')
        mchUnit=nunit
  case('tree','skeleton')
        treeUnit=nunit
  case('size')
        sizeUnit=nunit
  case('gnrt')
        gnrtUnit=nunit
  case('model')
        modelUnit=nunit
  case('progress')
        progUnit=nunit
  case default
        if (errru.ge.0) write(errru,*) 'ERROR in Kaleu_set_unit: ' &
          ,'message not defined, setting nothing'
  end select
  end subroutine

end module


