module avh_kaleu_geninv
  !(usekindmod!)
  use avh_mathcnst
  use avh_iounits
  use avh_grid
  implicit none
  private
  public :: geninv_type

  type :: geninv_type
    !(realknd2!) :: Low,Vol
    !(realknd2!) :: a,b
    type(grid_type) :: grid
    procedure(init_interface),pointer :: init
    procedure(gnrt_interface),pointer :: gnrt
    procedure(wght_interface),pointer :: wght
  contains
    procedure :: init_universal
    procedure :: set
    procedure :: collect
    procedure :: dalloc
    procedure :: plot
  end type

  abstract interface
    subroutine init_interface( obj ,sLow,sUpp ,params )
    import
    class(geninv_type) :: obj
    !(realknd2!),intent(in) :: sLow,sUpp
    !(realknd2!),intent(in),optional :: params(:)
    end subroutine
  end interface

  abstract interface
    function gnrt_interface( obj ,rho ,sLow,sUpp ) result(ss)
    import
    class(geninv_type) :: obj
    !(realknd2!),intent(in) :: rho,sLow,sUpp
    !(realknd2!) :: ss
    end function
  end interface

  abstract interface
    subroutine wght_interface( obj ,ss ,sLow,sUpp ,ww )
    import
    class(geninv_type) :: obj
    !(realknd2!),intent(in ) :: ss,sLow,sUpp
    !(realknd2!),intent(out) :: ww
    end subroutine
  end interface


contains


  subroutine init_universal( obj ,nbatch,nchmax ,label ,fileName,readUnit )
  class(geninv_type) :: obj
  integer,intent(in),optional :: nbatch,nchmax,readUnit
  character(*),intent(in),optional :: label,fileName
  integer :: Nb,Nc
  character(12) :: ll
  if (present(fileName)) then
    call obj%grid%load( fileName ,readUnit )
  else
    Nb=1000 ;if(present(nbatch)) Nb=nbatch
    Nc= 200 ;if(present(nchmax)) Nc=nchmax
    ll=''   ;if(present(label )) ll=label
    if (Nc.lt.2) Nc=2
    call obj%grid%init( nbatch=Nb ,nchmax=Nc ,label=ll )
  endif
  end subroutine


  subroutine set( obj ,distType )
  class(geninv_type) :: obj
  character(*),intent(in) :: distType
  select case(distType)
    case('trivial')
      obj%init => trivial_init
      obj%gnrt => trivial_gnrt
      obj%wght => trivial_wght
    case('resonance')
      !obj%init => trivial_init
      !obj%gnrt => trivial_gnrt
      !obj%wght => trivial_wght
      obj%init => resonance_init
      obj%gnrt => resonance_gnrt
      obj%wght => resonance_wght
    case('oneOverSqrt')
      obj%init => oneOverSqrt_init
      obj%gnrt => oneOverSqrt_gnrt
      obj%wght => oneOverSqrt_wght
    case('oneOverX')
      obj%init => oneOverX_init
      obj%gnrt => oneOverX_gnrt
      obj%wght => oneOverX_wght
  end select
  end subroutine

  subroutine collect( obj ,ww )
  class(geninv_type) :: obj
  !(realknd2!),intent(in) :: ww
  call obj%grid%collect( ww ,option=1 )
  end subroutine

  subroutine dalloc( obj )
  class(geninv_type) :: obj
  call obj%grid%dalloc
  end subroutine

  subroutine plot( obj ,wUnit )
  class(geninv_type) :: obj
  integer,intent(in) :: wUnit
  call obj%grid%plot( wUnit )
  end subroutine


!***********************************************************************
!***********************************************************************
  subroutine trivial_init &
  include 'kaleu_geninv_init.h90'
  obj%Low = sLow
  obj%Vol = sUpp - sLow
  end subroutine

  function trivial_gnrt( obj ,rho ,sLow,sUpp ) result(ss)
  include 'kaleu_geninv_gnrt.h90'
  xLow = (sLow-obj%Low)/obj%Vol
  xUpp = (sUpp-obj%Low)/obj%Vol
  xx = obj%grid%gnrt( rho ,xLow,xUpp )
  if (xx.lt.rZRO) return
  ss = obj%Vol*xx + obj%Low
  end function

  subroutine trivial_wght( obj ,ss ,sLow,sUpp ,ww )
  include 'kaleu_geninv_wght.h90'
  xLow = (sLow-obj%Low)/obj%Vol
  xUpp = (sUpp-obj%Low)/obj%Vol
  xx   = (ss  -obj%Low)/obj%Vol
  ww = obj%Vol * obj%grid%wght( xx ,xLow,xUpp )
  end subroutine


!***********************************************************************
!***********************************************************************
  subroutine resonance_init &
  include 'kaleu_geninv_init.h90'
  obj%a = params(1)**2        !mass*mass
  obj%b = params(1)*params(2) !mass*width
  obj%Low = atan( (sLow-obj%a)/obj%b )
  obj%Vol = atan( (sUpp-obj%a)/obj%b ) - obj%Low
  end subroutine

  function resonance_gnrt( obj ,rho ,sLow,sUpp ) result(ss)
  include 'kaleu_geninv_gnrt.h90'
  xLow = ( atan((sLow-obj%a)/obj%b)-obj%Low ) / obj%Vol
  xUpp = ( atan((sUpp-obj%a)/obj%b)-obj%Low ) / obj%Vol
  xx = obj%grid%gnrt( rho ,xLow,xUpp )
  if (xx.lt.rZRO) return
  ss = obj%a + obj%b*tan( obj%Vol*xx + obj%Low )
  end function

  subroutine resonance_wght( obj ,ss ,sLow,sUpp ,ww )
  include 'kaleu_geninv_wght.h90'
  xLow = ( atan((sLow-obj%a)/obj%b)-obj%Low ) / obj%Vol
  xUpp = ( atan((sUpp-obj%a)/obj%b)-obj%Low ) / obj%Vol
  xx   = ( atan((ss  -obj%a)/obj%b)-obj%Low ) / obj%Vol
  ww = (ss-obj%a)**2/obj%b + obj%b
  ww = ww * obj%Vol * obj%grid%wght( xx ,xLow,xUpp )
  end subroutine


!***********************************************************************
!***********************************************************************
  subroutine oneOverSqrt_init &
  include 'kaleu_geninv_init.h90'
  obj%b = abs(params(1)) !boundary between flat and 1/sqrt, in GeV
  obj%a = obj%b*obj%b
  obj%Low = oneOverSqrt_h( sLow ,obj%a,obj%b )
  obj%Vol = oneOverSqrt_h( sUpp ,obj%a,obj%b ) - obj%Low
  end subroutine

  function oneOverSqrt_h( ss,aa,bb ) result(xx)
  !(realknd2!),intent(in) :: ss,aa,bb
  !(realknd2!) :: xx
  if     (ss.lt.-aa  ) then ;xx = bb - 2*sqrt(-ss)
  elseif (ss.lt. rZRO) then ;xx = ss/bb
  elseif (ss.lt. aa  ) then ;xx = ss/bb
                       else ;xx = 2*sqrt(ss) - bb
  endif
  end function

  function oneOverSqrt_gnrt( obj ,rho ,sLow,sUpp ) result(ss)
  include 'kaleu_geninv_gnrt.h90'
  xLow = ( oneOverSqrt_h( sLow ,obj%a,obj%b ) - obj%Low )/obj%Vol
  xUpp = ( oneOverSqrt_h( sUpp ,obj%a,obj%b ) - obj%Low )/obj%Vol
  xx = obj%grid%gnrt( rho ,xLow,xUpp )
  if (xx.lt.rZRO) return
  xx = obj%b*( obj%Vol*xx + obj%Low )
  if     (xx.lt.-obj%a) then ;ss =-(obj%b-xx/obj%b)**2/4
  elseif (xx.lt. rZRO ) then ;ss = xx
  elseif (xx.lt. obj%a) then ;ss = xx
                        else ;ss = (xx/obj%b+obj%b)**2/4
  endif
  end function

  subroutine oneOverSqrt_wght( obj ,ss ,sLow,sUpp ,ww )
  include 'kaleu_geninv_wght.h90'
  xLow = ( oneOverSqrt_h( sLow ,obj%a,obj%b ) - obj%Low )/obj%Vol
  xUpp = ( oneOverSqrt_h( sUpp ,obj%a,obj%b ) - obj%Low )/obj%Vol
  if     (ss.lt.-obj%a) then ;ww=sqrt(-ss) ;xx= obj%b - 2*ww 
  elseif (ss.lt. rZRO ) then ;ww=obj%b     ;xx= ss/obj%b
  elseif (ss.lt. obj%a) then ;ww=obj%b     ;xx= ss/obj%b
                        else ;ww=sqrt( ss) ;xx= 2*ww - obj%b
  endif
  xx   = (xx-obj%Low)/obj%Vol
  ww = ww * obj%Vol * obj%grid%wght( xx ,xLow,xUpp )
  end subroutine


!***********************************************************************
!***********************************************************************
  subroutine oneOverX_init &
  include 'kaleu_geninv_init.h90'
  obj%a = params(1)**2 !boundary between flat and 1/x, in GeV^2
  obj%Low = oneOverX_h(sLow,obj%a)
  obj%Vol = oneOverX_h(sUpp,obj%a) - obj%Low
  end subroutine

  function oneOverX_h( ss,aa ) result(xx)
  !(realknd2!),intent(in) :: ss,aa
  !(realknd2!) :: xx
  if     (ss.lt.-aa  ) then ;xx =-1-log(-ss/aa)
  elseif (ss.lt. rZRO) then ;xx = ss/aa
  elseif (ss.lt. aa  ) then ;xx = ss/aa
                       else ;xx = 1+log( ss/aa)
  endif
  end function

  function oneOverX_gnrt( obj ,rho ,sLow,sUpp ) result(ss)
  include 'kaleu_geninv_gnrt.h90'
  xLow = ( oneOverX_h(sLow,obj%a) - obj%Low )/obj%Vol
  xUpp = ( oneOverX_h(sUpp,obj%a) - obj%Low )/obj%Vol
  xx = obj%grid%gnrt( rho ,xLow,xUpp )
  if (xx.lt.rZRO) return
  xx = obj%Vol*xx + obj%Low
  if     (xx.lt.-rONE) then ;ss =-obj%a*exp(-xx-1)
  elseif (xx.lt. rZRO) then ;ss = xx*obj%a
  elseif (xx.lt. rONE) then ;ss = xx*obj%a
                       else ;ss = obj%a*exp( xx-1)
  endif
  end function

  subroutine oneOverX_wght( obj ,ss ,sLow,sUpp ,ww )
  include 'kaleu_geninv_wght.h90'
  xLow = ( oneOverX_h(sLow,obj%a) - obj%Low )/obj%Vol
  xUpp = ( oneOverX_h(sUpp,obj%a) - obj%Low )/obj%Vol
  if     (ss.lt.-obj%a) then ;ww=-ss    ;xx=-1-log(-ss/obj%a)
  elseif (ss.lt. rZRO ) then ;ww= obj%a ;xx= ss/obj%a
  elseif (ss.lt. obj%a) then ;ww= obj%a ;xx= ss/obj%a
                        else ;ww= ss    ;xx= 1+log( ss/obj%a)
  endif
  xx = (xx-obj%Low)/obj%Vol
  ww = ww * obj%Vol * obj%grid%wght( xx ,xLow,xUpp )
  end subroutine

end module


