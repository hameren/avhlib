module avh_kaleu_model
  !(usekindmod!)
  use avh_mathcnst
  use avh_prnt
  use avh_kaleu_units
  implicit none
  private
  public :: kaleu_model_type
 
  integer,parameter :: symbLen=2

  type :: particle_type
    integer      :: label
    !(realknd2!) :: mass ,width
    logical      :: haswidth=.false. ,nowidth =.false.
    character(symbLen) :: symb='0'
  end type
 
  type :: kaleu_model_type
    private
!
    type(particle_type),allocatable :: particle(:) ! particle
    integer :: Nparticle = 0 ! number of particles
!
    integer,allocatable :: leg(:,:) ! vertex legs
    integer :: Nvertex = 0 ! number of vertices
    logical :: printed=.false.
!
    integer,allocatable :: ref(:)
    logical :: vertexStarted=.false.
!
  contains
    procedure :: getRef
    procedure :: addParticle
    procedure :: addVertex
    procedure :: fillFusion
    procedure :: Npart
    procedure :: mass
    procedure :: width
    procedure :: symbol
    procedure :: hasWidth
    procedure :: noWidth
    procedure :: label
    procedure :: process
!
  end type

  interface enlarge
    module procedure enlarge_i1,enlarge_p1,enlarge_i2
  end interface

contains
 

  function getRef( m ,ii ) result(rslt)
  class(kaleu_model_type) :: m
  integer,intent(in) :: ii
  integer :: rslt
  rslt = m%ref(ii)
  end function

 
  subroutine addParticle( m ,label ,symb,mass,width )
!**********************************************************************
!**********************************************************************
  class(kaleu_model_type) :: m
  integer     ,intent(in) :: label
  character(*),intent(in) :: symb
  !(realknd2!),intent(in) :: mass,width
  integer :: nn,ii
!
  call kaleu_hello
!
  if (m%vertexStarted) then
    if (errru.ge.0) write(errru,*) 'ERROR in Kaleu addparticle: ' &
      ,'all particles must have been added before adding vertices.'
    stop
  endif
!
  do ii=1,m%Nparticle
    if (m%particle(ii)%label.eq.label.and.&
        m%particle(ii)%symb .eq.symb .and.&
        m%particle(ii)%mass .eq.mass .and.&
        m%particle(ii)%width.eq.width     ) return
  enddo
!
  m%Nparticle = m%Nparticle+1
  nn = m%Nparticle
  call enlarge( m%ref ,label,label )
  call enlarge( m%particle ,0,nn ,0,10 )
!
  m%ref(label) = nn
  m%particle(nn)%label = label
  m%particle(nn)%symb  = adjustl(symb)
  m%particle(nn)%mass  = mass
  m%particle(nn)%width = width
!
  if     (mass.gt.rZRO.and.width.gt.rZRO) then
    m%particle(nn)%haswidth = .true.
  elseif (mass.ge.rZRO) then
    m%particle(nn)%nowidth = .true.
  else
    if (errru.gt.0) write(errru,*) 'ERROR in Kaleu addparticle: ' &
      ,'particle with label ',trim(prnt(label,'lft')) &
      ,' has mass=',trim(prnt(mass,7)),' < 0'
    stop
  endif
  end subroutine

 
  subroutine addVertex( m ,i1,i2,i3 )
!**********************************************************************
!**********************************************************************
  class(kaleu_model_type) :: m
  integer,intent(in)    :: i1,i2,i3
  integer :: ii,j1,j2,j3,ll,uu
!
  m%vertexStarted= .true.
  call resize( m%particle ,0,m%Nparticle )
!
  ll = lbound(m%ref,1)
  uu = ubound(m%ref,1)
  if (i1.lt.ll.or.uu.lt.i1.or. &
      i2.lt.ll.or.uu.lt.i2.or. &
      i3.lt.ll.or.uu.lt.i3     ) then
    if (errru.ge.0) write(errru,*) 'ERROR in Kaleu addvertex: ' &
      ,'value for vertex leg not encountered in addparticle.'
    stop
  endif
!
  j1 = m%ref(i1)
  j2 = m%ref(i2)
  j3 = m%ref(i3)
!
  if (j1.le.0.or.m%Nparticle.lt.j1.or. &
      j2.le.0.or.m%Nparticle.lt.j2.or. &
      j3.le.0.or.m%Nparticle.lt.j3     ) then
    if (errru.ge.0) write(errru,*) 'ERROR in Kaleu addvertex: ' &
      ,'value for vertex leg not encountered in addparticle.'
    stop
  endif
!
  do ii=1,m%Nvertex
    if (m%leg(1,ii).eq.j1.and.m%leg(2,ii).eq.j2.and.m%leg(3,ii).eq.j3.or. &
        m%leg(1,ii).eq.j2.and.m%leg(2,ii).eq.j3.and.m%leg(3,ii).eq.j1.or. &
        m%leg(1,ii).eq.j3.and.m%leg(2,ii).eq.j1.and.m%leg(3,ii).eq.j2.or. &
        m%leg(1,ii).eq.j3.and.m%leg(2,ii).eq.j2.and.m%leg(3,ii).eq.j1.or. &
        m%leg(1,ii).eq.j2.and.m%leg(2,ii).eq.j1.and.m%leg(3,ii).eq.j3.or. &
        m%leg(1,ii).eq.j1.and.m%leg(2,ii).eq.j3.and.m%leg(3,ii).eq.j2    )&
      return
  enddo
!
  m%Nvertex = m%Nvertex+1
  call enlarge( m%leg ,1,3 ,1,m%Nvertex ,0,0 ,0,100 )
!
  m%leg(1,m%Nvertex) = j1
  m%leg(2,m%Nvertex) = j2
  m%leg(3,m%Nvertex) = j3
  end subroutine
 
 
  function Npart( model ) result(rslt)
!********************************************************************
!* The number of particles in model
!********************************************************************
  class(kaleu_model_type),intent(in) :: model
  integer :: rslt
  rslt = model%Nparticle
  end function
 
  function symbol( model ,ii ) result(rslt)
  class(kaleu_model_type) :: model
  integer,intent(in) :: ii
  character(len_trim(model%particle(ii)%symb)) :: rslt
  rslt = model%particle(ii)%symb
  end function
 
  function mass( model ,ii ) result(rslt)
  class(kaleu_model_type) :: model
  integer,intent(in) :: ii
  !(realknd2!) :: rslt
  rslt = model%particle(ii)%mass
  end function
 
  function label( model ,ii ) result(rslt)
  class(kaleu_model_type) :: model
  integer,intent(in) :: ii
  integer :: rslt
  rslt = model%particle(ii)%label
  end function
 
  function width( model ,ii ) result(rslt)
  class(kaleu_model_type) :: model
  integer,intent(in) :: ii
  !(realknd2!) :: rslt
  rslt = model%particle(ii)%width
  end function
 
  function noWidth( model ,ii ) result(rslt)
  class(kaleu_model_type) :: model
  integer,intent(in) :: ii
  logical :: rslt
  rslt = model%particle(ii)%nowidth
  end function
 
  function hasWidth( model ,ii ) result(rslt)
  class(kaleu_model_type) :: model
  integer,intent(in) :: ii
  logical :: rslt
  rslt = model%particle(ii)%haswidth
  end function

  function process( model ,Nfinst,proc ) result(rslt)
  class(kaleu_model_type) :: model
  integer,intent(in) :: Nfinst
  integer,intent(in) :: proc(-2:Nfinst)
  character((2+Nfinst)*(1+symbLen)+2) :: rslt
  integer :: ii
  rslt = trim(model%particle(proc(-2))%symb) &
  //' '//trim(model%particle(proc(-1))%symb)//' ->'
  do ii=1,nfinst
    rslt = trim(rslt)//' '//trim(model%particle(proc(ii))%symb)
  enddo
  end function 
 
 
  subroutine fillFusion( m ,fusion )
!********************************************************************
!********************************************************************
  class(kaleu_model_type) :: m
  integer,allocatable ,intent(out  ) :: fusion(:,:,:)
  integer :: i1,i2,i3,j1,j2,j3,ii,jj,nn
!
  m%vertexStarted = .false.
  allocate( fusion(1:m%Nparticle,1:m%Nparticle,0:m%Nparticle) )
  fusion = 0
!
  if (modelUnit.gt.0.and..not.m%printed) then
    m%printed = .true.
    write(modelUnit,*) 'MESSAGE from Kaleu model:' &
                  ,' here follows the list of particles'
    do ii=1,m%Nparticle
      write(modelUnit,*) 'MESSAGE from Kaleu model:   ' &
                 ,trim(m%particle(ii)%symb) &
                 ,' mass=',trim(prnt(m%particle(ii)%mass,7,'lft')) &
                 ,' width=',trim(prnt(m%particle(ii)%width,7,'lft'))
    enddo
    write(modelUnit,*) 'MESSAGE from Kaleu model:' &
                  ,' here follows the list of vertices'
    do ii=1,m%Nvertex/2
      jj = ii+m%Nvertex/2
      i1=m%leg(1,ii); i2=m%leg(2,ii); i3=m%leg(3,ii)
      j1=m%leg(1,jj); j2=m%leg(2,jj); j3=m%leg(3,jj)
      write(modelUnit,*) 'MESSAGE from Kaleu model:   ' &
                    ,'(',trim(m%particle(i1)%symb) &
                    ,',',trim(m%particle(i2)%symb) &
                    ,',',trim(m%particle(i3)%symb),') ' &
                    ,'(',trim(m%particle(j1)%symb) &
                    ,',',trim(m%particle(j2)%symb) &
                    ,',',trim(m%particle(j3)%symb),')'
    enddo
    ii = m%Nvertex
    if (ii.ne.ii/2*2) then
      i1=m%leg(1,ii); i2=m%leg(2,ii); i3=m%leg(3,ii)
      write(modelUnit,*) 'MESSAGE from Kaleu model:   ' &
                    ,'       ' &
                    ,'(',trim(m%particle(i1)%symb) &
                    ,',',trim(m%particle(i2)%symb) &
                    ,',',trim(m%particle(i3)%symb),')'
    endif
  endif
!
! Fill fusion
  do ii=1,m%Nvertex
    i1 = m%leg(1,ii)
    i2 = m%leg(2,ii)
    i3 = m%leg(3,ii)
    if     (i1.eq.i2.and.i2.eq.i3) then
      nn = fusion( i1,i2 ,0 ) + 1
      fusion( i1,i2 ,0  ) = nn
      fusion( i1,i2 ,nn ) = i3
    elseif (i1.eq.i2.and.i2.ne.i3) then
      nn = fusion( i1,i2 ,0 ) + 1
      fusion( i1,i2 ,0  ) = nn
      fusion( i1,i2 ,nn ) = i3
      nn = fusion( i1,i3 ,0 ) + 1
      fusion( i1,i3 ,0  ) = nn
      fusion( i1,i3 ,nn ) = i2
      fusion( i3,i1 ,0  ) = nn
      fusion( i3,i1 ,nn ) = i2
    elseif (i3.eq.i1.and.i1.ne.i2) then
      nn = fusion( i3,i1 ,0 ) + 1
      fusion( i3,i1 ,0  ) = nn
      fusion( i3,i1 ,nn ) = i2
      nn = fusion( i3,i2 ,0 ) + 1
      fusion( i3,i2 ,0  ) = nn
      fusion( i3,i2 ,nn ) = i1
      fusion( i2,i3 ,0  ) = nn
      fusion( i2,i3 ,nn ) = i1
    elseif (i2.eq.i3.and.i3.ne.i1) then
      nn = fusion( i2,i3 ,0 ) + 1
      fusion( i2,i3 ,0  ) = nn
      fusion( i2,i3 ,nn ) = i1
      nn = fusion( i2,i1 ,0 ) + 1
      fusion( i2,i1 ,0  ) = nn
      fusion( i2,i1 ,nn ) = i3
      fusion( i1,i2 ,0  ) = nn
      fusion( i1,i2 ,nn ) = i3
    else
      nn = fusion( i1,i2 ,0 ) + 1
      fusion( i1,i2 ,0  ) = nn
      fusion( i1,i2 ,nn ) = i3
      fusion( i2,i1 ,0  ) = nn
      fusion( i2,i1 ,nn ) = i3
      nn = fusion( i3,i1 ,0 ) + 1
      fusion( i3,i1 ,0  ) = nn
      fusion( i3,i1 ,nn ) = i2
      fusion( i1,i3 ,0  ) = nn
      fusion( i1,i3 ,nn ) = i2
      nn = fusion( i2,i3 ,0 ) + 1
      fusion( i2,i3 ,0  ) = nn
      fusion( i2,i3 ,nn ) = i1
      fusion( i3,i2 ,0  ) = nn
      fusion( i3,i2 ,nn ) = i1
    endif
  enddo
!
  end subroutine
 

  subroutine enlarge_i1( xx ,l1,u1 )
  integer &
  include '../arrays/enlarge1.h90'
  end subroutine

  subroutine resize( xx ,l1,u1 )
  type(particle_type) &
  include '../arrays/resize1.h90'
  end subroutine

  subroutine enlarge_p1( xx ,l1,u1 ,l1stp,u1stp )
  type(particle_type) &
  include '../arrays/enlarge1stp.h90'
  end subroutine

  subroutine enlarge_i2( xx ,l1,u1 ,l2,u2 ,l1stp,u1stp ,l2stp,u2stp )
  integer &
  include '../arrays/enlarge2stp.h90'
  end subroutine

end module


