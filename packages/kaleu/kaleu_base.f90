module avh_kaleu_base
  !(usekindmod!)
  use avh_mathcnst
  use avh_prnt
  use avh_kskeleton
  use avh_kinematics
  use avh_kaleu_units
  use avh_kaleu_model
  use avh_kaleu_tree
  use avh_kaleu_geninv
  use avh_kaleu_mulcha
  use avh_kaleu_stats
  use avh_random
!
  implicit none
  private ! everything is private, except the following list
  public :: kaleu_hello    !propagate from avh_kaleu_units
  public :: kaleu_set_unit !propagate from avh_kaleu_units
  public :: kaleu_base_type
 
  type :: vertex_type
    private
    integer :: typ ,i0,i1,i2 ,iz=0 ,j0,j1,j2 ,o1,o2,ot
    logical :: sch1=.false.,sch2=.false.
  end type
 
  type :: kaleu_base_type
    private
    type(mulcha_type) :: mch
    type(vertex_type),allocatable :: vtx(:)
    type(geninv_type),allocatable :: gnv(:)
    !(realknd2!),allocatable:: xMass(:)
    !(realknd2!) :: ecm
    integer :: n1=0,Nfinst=0,Nxtrn=0
    character(24) :: label
    procedure(rng_interface),pointer,nopass :: rng
  contains
    procedure :: init1
    procedure :: init2
    generic :: init=>init1,init2
    procedure :: gnrt
    procedure :: wght
    procedure :: adapt_grids
    procedure :: adapt_channels
    procedure :: adapt
    procedure :: dalloc
    procedure :: plotgrids
    procedure :: put_masses_to
    procedure :: dump
  end type

contains
 
 
  subroutine init1( obj ,mdl ,process ,Ecm ,Ecut ,rcg,rng &
                       ,stats ,cancel ,label ,fileName ,readUnit )
!**********************************************************************
! rng_interface for input "rcg" and "rng" is defined in kaleu_mulcha
!**********************************************************************
  type(process_type),intent(in) :: process
  integer :: pss(-2:process%Nfinst)
!
  include 'kaleu_base_init_A.h90'
!
! Put process
  obj%Nfinst = process%Nfinst
  if (obj%Nfinst.le.1) then
    if (present(cancel)) cancel = .false.
    return
  endif
  obj%Nxtrn  = obj%Nfinst+2
  obj%ecm = ecm
!
  do ii=-2,obj%Nfinst
    if (ii.eq.0) cycle
    pss(ii) = mdl%getRef(process%infi(ii))
  enddo
!
  include 'kaleu_base_init_B.h90'
!
  end subroutine


  subroutine init2( obj ,mdl ,Nfinst,process ,Ecm ,Ecut ,rcg,rng &
                       ,stats ,cancel ,label ,fileName ,readUnit )
  integer,intent(in) :: Nfinst
  integer,intent(in) :: process(-2:Nfinst)
  integer :: pss(-2:Nfinst)
!
  include 'kaleu_base_init_A.h90'
!
! Put process
  obj%Nfinst = Nfinst
  if (obj%Nfinst.le.1) then
    if (present(cancel)) cancel = .false.
    return
  endif
  obj%Nxtrn = obj%Nfinst+2
  obj%ecm = ecm
!
  do ii=-2,obj%Nfinst
    if (ii.eq.0) cycle
    pss(ii) = mdl%getRef(process(ii))
  enddo
!
  include 'kaleu_base_init_B.h90'
!
  end subroutine


  subroutine gnrt( obj ,discard ,psp ,pInst,sInst ,mFins )
!*********************************************************************
!*********************************************************************
  class(kaleu_base_type) :: obj
  type(psPoint_type) :: psp
  logical     ,intent(  out) :: discard
  !(realknd2!),intent(in   ) :: pInst(0:3,-2:-1),sInst(-2:-1)
  !(realknd2!),intent(in),optional :: mFins(1:obj%Nfinst)
  integer :: nn,ii,vrtx(obj%mch%n1)
!
  discard = .false.
!
  if (obj%Nfinst.gt.1) then
!
!   Put inst-momenta
    call psp%put_inst( pInst,sInst ,.false. )
    if (present(mFins)) then
      call psp%put_masses( mFins(1:obj%Nfinst) )
    else
      call psp%put_masses( obj%xMass(1:obj%Nfinst) )
    endif
!  
!   Generate a graph, ie, a list of vertices
    nn = 0
    ii = obj%mch%ov(0,obj%mch%nv)
    call obj%mch%vrtx( vrtx,nn ,ii ) ! recursive routine!
!  
!   Go through the vertices of the graph
    do ii=1,nn
      call vertex_gnrt( obj ,vrtx(ii) ,psp ,discard )
      if (discard) return
    enddo
!  
!   Complete external and internal momenta
    call psp%finalize
!
  else
!
    call psp%put(-2 ,             pInst(:,-2) ,sInst(-2) ,.true. )
    call psp%put(-1 , pInst(:,-1)             ,sInst(-1) ,.true. )
    if (present(mFins)) then
      call psp%put( 1 ,-pInst(:,-1)-pInst(:,-2) ,mFins(1)**2 ,.true. )
    else
      call psp%put( 1 ,-pInst(:,-1)-pInst(:,-2) ,obj%xMass(1)**2 ,.true. )
    endif
!
  endif
!
  end subroutine
 
 
  function wght( obj ,psp ) result( weight )
!*********************************************************************
!*********************************************************************
  class(kaleu_base_type) :: obj
  type(psPoint_type),intent(in) :: psp
  !(realknd2!) :: weight
  !(realknd2!) :: dd(obj%mch%no),ww
  integer :: ii,i0,i1,i2,i3
  if (obj%Nfinst.gt.1) then
    weight = rZRO
    dd( 1:obj%mch%n1 ) = rONE            ! obj%mch%no may be too large,
    dd( obj%mch%n1+1:obj%mch%no ) = rZRO ! but not too small
    if (obj%mch%yes) then
      do ii=1,obj%mch%nv
        if (obj%mch%wtv(ii).eq.rZRO) cycle
        obj%mch%dns(ii) = rZRO
        i0 = obj%mch%ov(0,ii)
        i1 = obj%mch%ov(1,ii)
        i2 = obj%mch%ov(2,ii)
        i3 = obj%mch%ov(3,ii)
        if (i3.ne.0) i1 = i3 ! specific T-channel
        call vertex_wght( obj ,ii ,ww ,psp )
        if (ww.eq.rZRO) cycle !return
        obj%mch%dns(ii) = dd(i1)*dd(i2)/ww
        dd(i0) = dd(i0) + obj%mch%wtv(ii)*obj%mch%dns(ii)
      enddo
    else
      do ii=1,obj%mch%nv
        i0 = obj%mch%ov(0,ii)
        i1 = obj%mch%ov(1,ii)
        i2 = obj%mch%ov(2,ii)
        i3 = obj%mch%ov(3,ii)
        if (i3.ne.0) i1 = i3 ! specific T-channel
        call vertex_wght( obj ,ii ,ww ,psp )
        if (ww.eq.rZRO) cycle !return
        dd(i0) = dd(i0) + dd(i1)*dd(i2)/ww/obj%mch%vo(i0,0)
      enddo
    endif
    weight = dd( obj%mch%ov(0,obj%mch%nv) )
    if (weight.ne.rZRO) weight = rONE/weight
  else
    weight = rONE
  endif
  end function
 
 
  subroutine adapt_grids( obj ,stats )
!*********************************************************************
!*********************************************************************
  class(kaleu_base_type) :: obj
  type(kaleu_stats_type),intent(in) :: stats
  integer :: ii
  if (stats%zeroWeight) return
  do ii=1,ubound(obj%gnv,1)
    call obj%gnv(ii)%collect( stats%absWeight )
  enddo
  end subroutine
 
  subroutine adapt_channels( obj ,stats )
  class(kaleu_base_type) :: obj
  type(kaleu_stats_type),intent(in) :: stats
  integer :: nrem,jrem(obj%mch%nv)
  call obj%mch%adapt( stats ,jrem,nrem )
  end subroutine
 
  subroutine adapt( obj ,stats )
  class(kaleu_base_type) :: obj
  type(kaleu_stats_type),intent(in) :: stats
  if (obj%Nfinst.le.1) return
  if (stats%NnonZero.le.stats%Noptim) then
    call obj%adapt_channels( stats )
    call obj%adapt_grids( stats )
  endif
  end subroutine
 
 
  subroutine dalloc( obj )
!*********************************************************************
!*********************************************************************
  class(kaleu_base_type) :: obj
  if (allocated(obj%xMass)) deallocate( obj%xMass )
  if (allocated(obj%vtx)) deallocate( obj%vtx )
  if (allocated(obj%gnv)) deallocate( obj%gnv )
  call obj%mch%dalloc
  end subroutine
 
 
  subroutine plotgrids( obj ,iunit )
!*********************************************************************
!*********************************************************************
  class(kaleu_base_type),intent(in) :: obj
  integer        ,intent(in) :: iunit
  integer :: ii
  if (obj%Nfinst.le.1) return
  do ii=1,ubound(obj%gnv,1)
    call obj%gnv(ii)%plot( iunit )
  enddo
  end subroutine
 
 
  subroutine dump( obj ,fileName ,writeUnit )
!*********************************************************************
!*********************************************************************
  class(kaleu_base_type),intent(in) :: obj
  character(*),intent(in) :: fileName
  integer,intent(in),optional :: writeUnit
  integer :: ii,iunit
  iunit = deffile
  if (present(writeUnit)) iunit=writeUnit
  if (writeUnit.le.0) return
  if (obj%Nfinst.le.1) return
  call obj%mch%dump( writeUnit ,trim(fileName)//'mch.kaleudump' )
  do ii=1,ubound(obj%gnv,1)
    call obj%gnv(ii)%grid%dump( writeUnit &
           ,trim(fileName)//'gnv'//prnt(ii,2,'0')//'.kaleudump' )
  enddo
  end subroutine
 
 
  subroutine put_masses_to( obj ,psp )
!*********************************************************************
!*********************************************************************
  class(kaleu_base_type) :: obj
  type(psPoint_type) :: psp
  integer :: ii
  do ii=-2,obj%Nfinst
    if (ii.eq.0) cycle
    psp%p(psp%b(ii))%M = obj%xMass(ii)
  enddo
  end subroutine
 

  subroutine vertex_init( obj ,mdl ,tree ,Ecm ,Ecut ,nbatch ,fileName ,readUnit )
!********************************************************************
!********************************************************************
  class(kaleu_base_type),intent(inout) :: obj
  type(kaleu_model_type),intent(in   ) :: mdl
  type(tree_type) ,intent(in  )  :: tree
  !(realknd2!)    ,intent(in  )  :: Ecm,Ecut
  integer  ,intent(in),optional  :: nbatch
  character(*),intent(in),optional :: fileName
  integer,intent(in),optional :: readUnit
  integer :: PandF(1: 2**tree%n1-1,  0:tree%nfl )
  integer :: justP(1:(2**tree%n1-1)*(1+tree%nfl))
  integer :: justF(1:(2**tree%n1-1)*(1+tree%nfl))
  !(realknd2!) :: sLow,sUpp
  integer :: ii,f1,f2,f3,nn,kk,fl
!
  PandF = 0
  nn = 0
!
  if (allocated(obj%vtx)) deallocate(obj%vtx)
  allocate( obj%vtx(1:tree%nv) )
  do ii=1,tree%nv
    obj%vtx(ii)%typ = tree%typ(ii)
    obj%vtx(ii)%iz  = tree%zax(ii)
    obj%vtx(ii)%i0 = tree%po( tree%ov(0,ii) )
    obj%vtx(ii)%i1 = tree%po( tree%ov(1,ii) )
    obj%vtx(ii)%i2 = tree%po( tree%ov(2,ii) )
    f1 = tree%fo( tree%ov(1,ii) )
    f2 = tree%fo( tree%ov(2,ii) )
    f3 = tree%fo( tree%ov(3,ii) )
    if     (obj%vtx(ii)%typ.eq.0) then
      obj%vtx(ii)%j0 = obj%vtx(ii)%i0
      obj%vtx(ii)%j1 = obj%vtx(ii)%i1
      obj%vtx(ii)%j2 = obj%vtx(ii)%i2
    elseif (obj%vtx(ii)%typ.eq.2) then ! s-channel vertex
      obj%vtx(ii)%j0 = obj%vtx(ii)%i0
      obj%vtx(ii)%j1 = obj%vtx(ii)%i1
      obj%vtx(ii)%j2 = obj%vtx(ii)%i2
      obj%vtx(ii)%sch1 = (kskeleton_esab(obj%vtx(ii)%j1).eq.0)
      obj%vtx(ii)%sch2 = (kskeleton_esab(obj%vtx(ii)%j2).eq.0)
      if ( obj%vtx(ii)%sch1 .and. PandF(obj%vtx(ii)%j1,f1).eq.0 ) then
        nn = nn+1
        PandF(obj%vtx(ii)%j1,f1) = nn
        justP(nn) = obj%vtx(ii)%j1
        justF(nn) = f1
      endif
      if ( obj%vtx(ii)%sch2 .and. PandF(obj%vtx(ii)%j2,f2).eq.0 ) then
        nn = nn+1
        PandF(obj%vtx(ii)%j2,f2) = nn
        justP(nn) = obj%vtx(ii)%j2
        justF(nn) = f2
      endif
      obj%vtx(ii)%o1 = PandF(obj%vtx(ii)%j1,f1)
      obj%vtx(ii)%o2 = PandF(obj%vtx(ii)%j2,f2)
    elseif (obj%vtx(ii)%typ.eq.1) then ! t-variable is s(j1+iz)=s(j1+1)=s(i1)
      obj%vtx(ii)%j0 = obj%vtx(ii)%i0-1
      obj%vtx(ii)%j1 = obj%vtx(ii)%i1-1
      obj%vtx(ii)%j2 = obj%vtx(ii)%i2
      obj%vtx(ii)%sch1 = (kskeleton_esab(obj%vtx(ii)%j1).eq.0)
      obj%vtx(ii)%sch2 = (kskeleton_esab(obj%vtx(ii)%j2).eq.0)
      if ( obj%vtx(ii)%sch1 .and. PandF(obj%vtx(ii)%j1,f3).eq.0 ) then
        nn = nn+1
        PandF(obj%vtx(ii)%j1,f3) = nn
        justP(nn) = obj%vtx(ii)%j1
        justF(nn) = f3
      endif
      if ( obj%vtx(ii)%sch2 .and. PandF(obj%vtx(ii)%j2,f2).eq.0 ) then
        nn = nn+1
        PandF(obj%vtx(ii)%j2,f2) = nn
        justP(nn) = obj%vtx(ii)%j2
        justF(nn) = f2
      endif
      if (PandF(obj%vtx(ii)%i1,f1).eq.0) then
        nn = nn+1
        PandF(obj%vtx(ii)%i1,f1) = nn
        justP(nn) = obj%vtx(ii)%i1
        justF(nn) = f1
      endif
      obj%vtx(ii)%o1 = PandF(obj%vtx(ii)%j1,f3)
      obj%vtx(ii)%o2 = PandF(obj%vtx(ii)%j2,f2)
      obj%vtx(ii)%ot = PandF(obj%vtx(ii)%i1,f1)
    endif
  enddo
!
  if (allocated(obj%gnv)) deallocate(obj%gnv)
  allocate( obj%gnv(1:nn) )
  do ii=1,nn
    kk = justP(ii)
    fl = justF(ii)
    if (mod(kk,2).eq.0) then ;sLow= 0       ;sUpp= Ecm*Ecm
                        else ;sLow=-Ecm*Ecm ;sUpp= 0
    endif
    if (present(fileName)) then
      call obj%gnv(ii)%init_universal( readUnit=readUnit &
             ,fileName=trim(fileName)//'gnv'//prnt(ii,2,'0')//'.kaleudump' )
    else
      call obj%gnv(ii)%init_universal( nbatch=nbatch &
             ,label=prnt(kk,2,'0')//'_'//mdl%symbol(fl) )
    endif
    if     (mdl%hasWidth(fl)) then
      call obj%gnv(ii)%set('resonance')
      call obj%gnv(ii)%init( sLow,sUpp ,params=[mdl%mass(fl),mdl%width(fl)] )
    elseif (mdl%noWidth(fl)) then
      call obj%gnv(ii)%set('oneOverX')
      call obj%gnv(ii)%init( sLow,sUpp ,params=[Ecut] )
    else
      call obj%gnv(ii)%set('trivial')
      call obj%gnv(ii)%init( sLow,sUpp )
    endif
  enddo
!
  end subroutine


  subroutine vertex_gnrt( obj ,iv ,psp ,discard )
!********************************************************************
!********************************************************************
  class(kaleu_base_type),intent(inout) :: obj
  integer          ,intent(in   ) :: iv
  type(psPoint_type),intent(inout) :: psp
  logical         ,intent(  out) :: discard
  integer :: jj
  !(realknd2!) :: lamo,phi,ct,st,tmin,tmax,Ez,E1,hz,h1
  !(realknd2!) :: pz1,pz2,pz3,p0,p1,p2,p3,absz,abs1
!
  associate(   v=>obj%vtx(iv) &
            ,Pi0=>psp%p(obj%vtx(iv)%i0) &
            ,Pi1=>psp%p(obj%vtx(iv)%i1) &
            ,Pi2=>psp%p(obj%vtx(iv)%i2) &
            ,Pj0=>psp%p(obj%vtx(iv)%j0) &
            ,Pj1=>psp%p(obj%vtx(iv)%j1) &
            ,Pj2=>psp%p(obj%vtx(iv)%j2) &
            ,Piz=>psp%p(obj%vtx(iv)%iz) )
!
  discard = .false.
!
  if (v%typ.eq.0) then
!No generation, only happens as first vertex for pure S-channel
    Pi2%E = Pi0%E - Pi1%E
    Pi2%V = Pi0%V - Pi1%V
!
  elseif (v%iz.gt.0) then
    call gnrt_s1s2( obj ,iv ,psp ,discard )
    if (discard) return
    if (Pj0%M.lt.Pj1%M+Pj2%M) then
      if (gnrtUnit.gt.0) write(gnrtUnit,*) 'WARNING from Kaleu vertex_gnrt T-chan: ' &
        ,'m0-m1-m2 < 0, discard event.',Pj0%M,Pj1%M,Pj2%M
      discard = .true.
      return
    endif
    if (Piz%E.ge.rZRO) then
      Ez  = Piz%E
      pz1 = Piz%V(1)
      pz2 = Piz%V(2)
      pz3 = Piz%V(3)
    else
      Ez  =-Piz%E
      pz1 =-Piz%V(1)
      pz2 =-Piz%V(2)
      pz3 =-Piz%V(3)
    endif
    call Pj0%tsoob_sub( Ez,pz1,pz2,pz3 )
    absz = sqrt(pz1**2 + pz2**2 + pz3**2)
    h1 = psp%lambda( v%j0 ,v%j1 ,v%j2 )
    if (h1.lt.rZRO) then
      if (gnrtUnit.gt.0) write(gnrtUnit,*) 'WARNING from Kaleu vertex_gnrt T-chan: ' &
        ,'lam =',h1,', discard event.' 
      discard = .true.
      return
    endif
    abs1 = sqrt(h1)/( 2*Pj0%M )
    E1 = sqrt( abs1*abs1 + Pj1%S )
    if (v%iz.eq.1.or.v%iz.eq.base(obj%Nxtrn)-1) then
      hz = Piz%S/(Ez+absz) ! Ez-absz
      h1 = Pj1%S/(E1+abs1) ! E1-abs1
      tmin = ( h1 - (Ez+absz) )*( (E1+abs1) - hz        )
      tmax = ( h1 - hz        )*( (E1+abs1) - (Ez+absz) )
      tmax =-abs(tmax)
    else
    endif
    jj = v%j1+v%iz
    if (jj.gt.base(obj%Nxtrn)) jj = v%iz-v%j1
    psp%p(jj)%S = obj%gnv(v%ot)%gnrt( obj%rng() ,tmin,tmax )
    if (psp%p(jj)%S.eq.rZRO) then
      discard = .true.
      return
    endif
    phi = r2PI*obj%rng()
!Given the variables, construct the momenta 
    if (v%iz.eq.1.or.v%iz.eq.base(obj%Nxtrn)-1) then
      ct = ( psp%p(jj)%S - Pj1%S - Piz%S + 2*Ez*E1 )/(2*absz)
      !ct = ( psp%p(jj)%S -2*abs1*absz - tmin )/(2*absz)
    else
      ct = (-psp%p(jj)%S + Pj1%S + Piz%S + 2*Ez*E1 )/(2*absz)
    endif
    st = (abs1+ct)*(abs1-ct)
    if (st.lt.rZRO) then
      if (gnrtUnit.gt.0) write(gnrtUnit,*) 'WARNING from Kaleu vertex_gnrt T-chan: ' &
        ,'1-ct^2 =',st/abs1**2,', discard event.'
      discard = .true.
      return
    endif
    st = sqrt(st)
    p1 = st*sin(phi)
    p2 = st*cos(phi)
    p3 = ct
    call rot3( p1,p2,p3 ,pz1,pz2,pz3,absz )
    call Pj0%boost_sub( E1,p1,p2,p3 )
    Pj1%E    = E1
    Pj1%V(1) = p1
    Pj1%V(2) = p2
    Pj1%V(3) = p3
    Pj2%E = Pj0%E - Pj1%E
    Pj2%V = Pj0%V - Pj1%V
    if (v%i0.ne.v%j0) then
      if (v%j1.eq.v%i2) then
        Pi1%E = Pj2%E + psp%p(1)%E
        Pi1%V = Pj2%V + psp%p(1)%V
      else
        Pi1%E = Pj1%E + psp%p(1)%E
        Pi1%V = Pj1%V + psp%p(1)%V
      endif
      if (v%typ.ne.1) then
        Pi1%S = (Pi1%E-Pi1%V(3))*(Pi1%E+Pi1%V(3)) - Pi1%V(1)**2 - Pi1%V(2)**2
      endif
    endif
!
  else
!S-channel generation (without correlated cos(theta))
    call gnrt_s1s2( obj ,iv ,psp ,discard )
    if (discard) return
    if (Pj0%M.lt.Pj1%M+Pj2%M) then
      if (gnrtUnit.gt.0) write(gnrtUnit,*) 'WARNING from Kaleu vertex_gnrt S-chan: ' &
        ,'m0-m1-m2 < 0, discard event.' 
      discard = .true.
      return
    endif
    lamo = psp%lambda( v%j0 ,v%j1 ,v%j2 )
    if (lamo.lt.rZRO) then
      if (gnrtUnit.gt.0) write(gnrtUnit,*) 'WARNING from Kaleu vertex_gnrt S-chan: ' &
        ,'lamo =',lamo,', discard event.'
      discard = .true.
      return
    endif
    lamo = sqrt(lamo)
    ct = 2*obj%rng()
    st = sqrt((2-ct)*ct)
    ct = ct-1
    phi = r2PI*obj%rng()
!Given the variables, construct the momenta 
    abs1 = lamo/(2*Pj0%M)
    p0 = sqrt( Pj1%S + abs1*abs1 )
    ct = abs1*ct
    st = abs1*st
    p1 = st*sin(phi)
    p2 = st*cos(phi)
    p3 = ct
    call Pj0%boost_sub( p0,p1,p2,p3 )
    Pi1%E    = p0
    Pi1%V(1) = p1
    Pi1%V(2) = p2
    Pi1%V(3) = p3
    Pi2%E = Pi0%E - Pi1%E
    Pi2%V = Pi0%V - Pi1%V
!
  endif
!
  end associate
!
  end subroutine

 
  subroutine vertex_wght( obj ,iv ,weight ,psp )
!********************************************************************
!********************************************************************
  class(kaleu_base_type),intent(inout) :: obj
  integer          ,intent(in   ) :: iv
  !(realknd2!)    ,intent(  out) :: weight
  type(psPoint_type),intent(in) :: psp
  integer :: jj
  !(realknd2!) :: lamo,ww,tmin,tmax,absz,abs1,h1,hz,E1,Ez,pz1,pz2,pz3
!
  associate( v=>obj%vtx(iv) )
!
  if (v%typ.eq.0) then
! No generation, no weight factor
    weight = rONE
!
  elseif (v%iz.gt.0) then
    call wght_s1s2( obj ,iv ,weight ,psp )
    if (weight.eq.rZRO) return
    h1 = psp%lambda( v%j0 ,v%j1 ,v%j2 )
    if (h1.lt.rZRO) then
      if (warnu.gt.0) write(warnu,*) 'WARNING from Kaleu vertex_wght T-chan: ' &
        ,'h1 =',h1,', returning weight=0'
      weight = rZRO
      return
    endif
    abs1 = sqrt(h1)/( 2*psp%p(v%j0)%M )
    E1 = sqrt( abs1*abs1 + psp%p(v%j1)%S )
!
!    hz = psp%lambda( v%j0+v%iz ,v%j0 ,v%iz )
!    absz = sqrt(hz)/( 2*psp%p(v%j0)%M )
!    if (hz.lt.rZRO) then
!      if (warnu.gt.0) write(warnu,*) 'WARNING from Kaleu vertex_wght T-chan: ' &
!        ,'hz =',hz,', returning weight=0'
!      weight = rZRO
!      return
!    endif
!    Ez = sqrt(( absz - psp%p(v%iz)%M )*( absz + psp%p(v%iz)%M ))
!
    if (psp%p(v%iz)%E.ge.rZRO) then
      Ez  = psp%p(v%iz)%E
      pz1 = psp%p(v%iz)%V(1)
      pz2 = psp%p(v%iz)%V(2)
      pz3 = psp%p(v%iz)%V(3)
    else
      Ez  =-psp%p(v%iz)%E
      pz1 =-psp%p(v%iz)%V(1)
      pz2 =-psp%p(v%iz)%V(2)
      pz3 =-psp%p(v%iz)%V(3)
    endif
    call psp%p(obj%vtx(iv)%j0)%tsoob_sub( Ez,pz1,pz2,pz3 )
    absz = sqrt(pz1**2 + pz2**2 + pz3**2)
!
    if (v%iz.eq.1.or.v%iz.eq.base(obj%Nxtrn)-1) then
      hz = psp%p(v%iz)%S/(Ez+absz) ! Ez-absz
      h1 = psp%p(v%j1)%S/(E1+abs1) ! E1-abs1
      tmin = ( h1-(Ez+absz) )*( (E1+abs1)-hz        )
      tmax = ( h1-hz        )*( (E1+abs1)-(Ez+absz) )
      tmax =-abs(tmax)
    else
    endif
    jj = v%j1+v%iz
    if (jj.gt.base(obj%Nxtrn)) jj = v%iz-v%j1
    call obj%gnv(v%ot)%wght( psp%p(jj)%S ,tmin,tmax ,ww )
    weight = weight * ww * r2PI / (8*psp%p(v%j0)%M*absz)
!
  else
! S-channel generation (without correlated cos(theta))
    call wght_s1s2( obj ,iv ,weight ,psp )
    if (weight.eq.rZRO) return
    lamo = psp%lambda( v%j0, v%j1 ,v%j2 )
    if (lamo.lt.rZRO) then
      if (warnu.gt.0) write(warnu,*) 'WARNING from Kaleu vertex_wght S-chan: ' &
        ,'lamo =',lamo,', returning weight=0'
      weight = rZRO
      return
    endif
    lamo = sqrt(lamo)
    weight = weight * r2PI * lamo / (4*psp%p(v%j0)%S)
!
  endif
!
  end associate
!
  end subroutine
 
 
  subroutine gnrt_s1s2( obj ,iv ,psp ,discard )
!********************************************************************
!* Given ss(i0), generate ss(i1) and ss(i2).
!* i0,i1,i2  should be even and correspond to positive invariants.
!********************************************************************
  class(kaleu_base_type),intent(inout) :: obj
  integer          ,intent(in   ) :: iv
  type(psPoint_type),intent(inout) :: psp
  logical         ,intent(  out) :: discard
  !(realknd2!) :: sLow,sUpp
!
  associate( v=>obj%vtx(iv) )
!
  discard = .true.
  sLow = 0
  sUpp = psp%p(v%j0)%S
  if (sUpp.le.sLow) return
  if (v%sch1.and.v%sch2) then
    psp%p(v%j1)%S = obj%gnv(v%o1)%gnrt( obj%rng() ,sLow,sUpp )
      if (psp%p(v%j1)%S.eq.rZRO) return
    psp%p(v%j1)%M = sqrt(psp%p(v%j1)%S)
    sUpp = ( psp%p(v%j0)%M - psp%p(v%j1)%M )**2
      if (sUpp.le.sLow) return
    psp%p(v%j2)%S = obj%gnv(v%o2)%gnrt( obj%rng() ,sLow,sUpp )
      if (psp%p(v%j2)%S.eq.rZRO) return
    psp%p(v%j2)%M = sqrt(psp%p(v%j2)%S)
  elseif (v%sch1) then
    psp%p(v%j1)%S = obj%gnv(v%o1)%gnrt( obj%rng() ,sLow,sUpp )
      if (psp%p(v%j1)%S.eq.rZRO) return
    psp%p(v%j1)%M = sqrt(psp%p(v%j1)%S)
  elseif (v%sch2) then
    psp%p(v%j2)%S = obj%gnv(v%o2)%gnrt( obj%rng() ,sLow,sUpp )
      if (psp%p(v%j2)%S.eq.rZRO) return
    psp%p(v%j2)%M = sqrt(psp%p(v%j2)%S)
  endif
  discard = .false.
!
  end associate
!
  end subroutine

  subroutine wght_s1s2( obj ,iv ,weight ,psp )
!********************************************************************
!* Given ss(i0), generate ss(i1) and ss(i2).
!* i0,i1,i2  should be even and correspond to positive invariants.
!********************************************************************
  class(kaleu_base_type),intent(inout) :: obj
  integer          ,intent(in   ) :: iv
  !(realknd2!)    ,intent(  out) :: weight
  type(psPoint_type),intent(in) :: psp
  !(realknd2!) :: sLow,sUpp,w1,w2
!
  associate( v=>obj%vtx(iv) )
!
  weight = 0
  sLow = 0
  sUpp = psp%p(v%j0)%S
  if (sUpp.le.sLow) return
  if (v%sch1.and.v%sch2) then
    call obj%gnv(v%o1)%wght( psp%p(v%j1)%S ,sLow,sUpp ,w1 )
    sUpp = ( psp%p(v%j0)%M - psp%p(v%j1)%M )**2
    if (sUpp.le.sLow) return
    call obj%gnv(v%o2)%wght( psp%p(v%j2)%S ,sLow,sUpp ,w2 )
    weight = w1*w2
  elseif (v%sch1) then
    call obj%gnv(v%o1)%wght( psp%p(v%j1)%S ,sLow,sUpp ,w1 )
    weight = w1
  elseif (v%sch2) then
    call obj%gnv(v%o2)%wght( psp%p(v%j2)%S ,sLow,sUpp ,w2 )
    weight = w2
  else
    weight = 1
  endif
!
  end associate
!
  end subroutine

 
  subroutine rot3( v1,v2,v3 ,x1,x2,x3,lx )
!********************************************************************
!* apply on (v1,v2,v3) the inverse of the rotation
!* that rotates (x1,x2,x3) to the 3-axis
!* lx is the length of (x1,x2,x3)
!********************************************************************
  !(realknd2!) :: v1,v2,v3 ,lx,x1,x2,x3  ,h1,h2,h3 &
                 ,stsp,stcp,ct,st,sp,cp,ctsp,ctcp
!  lx = sqrt( x1*x1 + x2*x2 + x3*x3 )
  stsp = x1/lx
  stcp = x2/lx
  ct   = x3/lx
  st   = sqrt(stsp*stsp + stcp*stcp)
  if (st.ne.rZRO) then
    sp = stsp/st
    cp = stcp/st
  else
    sp = rZRO
    cp = rONE
  endif
  ctsp = ct*sp
  ctcp = ct*cp
  h1 = v1
  h2 = v2
  h3 = v3
  v1 =  cp*h1 + ctsp*h2 + stsp*h3
  v2 = -sp*h1 + ctcp*h2 + stcp*h3
  v3 =        - st  *h2 + ct  *h3
  end subroutine
 
  subroutine tor3( v1,v2,v3 ,x1,x2,x3,lx )
!********************************************************************
!* apply on (v1,v2,v3) the rotation
!* that rotates (x1,x2,x3) to the 3-axis
!* lx is the length of (x1,x2,x3)
!********************************************************************
  !(realknd2!) :: v1,v2,v3 ,lx,x1,x2,x3  ,h1,h2,h3 &
                 ,stsp,stcp,ct,st,sp,cp,ctsp,ctcp
!  lx = sqrt( x1*x1 + x2*x2 + x3*x3 )
  stsp = x1/lx
  stcp = x2/lx
  ct   = x3/lx
  st   = sqrt(stsp*stsp + stcp*stcp)
  if (st.ne.rZRO) then
    sp = stsp/st
    cp = stcp/st
  else
    sp = rZRO
    cp = rONE
  endif
  ctsp = ct*sp
  ctcp = ct*cp
  h1 = v1
  h2 = v2
  h3 = v3
  v1 =   cp*h1 -   sp*h2
  v2 = ctsp*h1 + ctcp*h2 - st*h3
  v3 = stsp*h1 + stcp*h2 + ct*h3
  end subroutine
 


end module


