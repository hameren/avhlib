module avh_kaleu_mulcha
  !(usekindmod!)
  use avh_mathcnst
  use avh_prnt
  use avh_kaleu_units
  use avh_kaleu_tree ,only : tree_type
  use avh_kaleu_stats
  use avh_random
  implicit none

  private
  public :: mulcha_type,rng_interface

  type :: mulcha_type 
    logical :: yes = .false.
    integer ,allocatable :: ov(:,:),vo(:,:)
    logical ,allocatable :: frst1(:)
    integer              :: n1,nv,no
    !(realknd2!) ,allocatable :: wtv(:),ave(:),dns(:)
    integer                   :: idat,ntot,istp
    procedure(rng_interface),pointer,nopass :: rng
  contains
    procedure :: init
    procedure :: init_adapt
    procedure :: dalloc
    procedure :: adapt
    procedure :: vrtx
    procedure :: dump
  end type


contains


  subroutine init( obj ,rng, tree ,fileName ,readUnit )
!********************************************************************
!********************************************************************
  class(mulcha_type),intent(out) :: obj
  procedure(rng_interface) :: rng
  type(tree_type),intent(in),optional  :: tree
  character(*),intent(in),optional :: fileName
  integer,intent(in),optional :: readUnit
  integer :: nn,ii,jj,iv,iunit
  !(realknd2!) :: wtv0
!
  obj%rng => rng
!
 if (present(tree)) then 
    obj%n1 = tree%n1
    obj%nv = tree%nv
    obj%no = tree%no
!  
    allocate( obj%ov(0:3,1:obj%nv) )
    nn = maxval( tree%vo(:,0) )
    allocate( obj%vo(0:obj%no,0:nn) )
    allocate( obj%frst1(1:obj%nv) )
    obj%ov(0:3,1:obj%nv)  = tree%ov(0:3,1:obj%nv)
    obj%vo(0:obj%no,0:nn) = tree%vo(0:obj%no,0:nn)
    obj%frst1(1:obj%nv)   = tree%frst1(1:obj%nv)
!
    obj%yes = .true.
    allocate( obj%wtv(1:obj%nv) )
    allocate( obj%ave(1:obj%nv) )
    allocate( obj%dns(1:obj%nv) )
    do ii=1,obj%no
      nn = obj%vo(ii,0)
      if (nn.ge.1) then
        wtv0 = rONE/nn
        do jj=1,nn
          iv = obj%vo(ii,jj)
          obj%ave(iv) = rZRO
          obj%wtv(iv) = wtv0
          obj%dns(iv) = rZRO
        enddo
      endif
    enddo
    obj%idat = 0
    obj%ntot = 0
    obj%istp = 0
  else
    if (.not.present(fileName)) then
      if (errru.ge.0) write(errru,*) 'ERROR in kaleu_mulcha: no fileName'
      stop
    endif
    iunit = deffile
    if (present(readUnit)) iunit = readUnit
    if (iunit.le.0) return
    obj%yes = .true.
    open(unit=iunit,file=trim(fileName),status='old',form='unformatted')
    read(iunit) obj%n1,obj%nv,obj%no,nn
    allocate( obj%ov(0:3,1:obj%nv) )
    allocate( obj%vo(0:obj%no,0:nn) )
    allocate( obj%frst1(1:obj%nv) )
    allocate( obj%wtv(1:obj%nv) )
    allocate( obj%dns(1:obj%nv) )
    read(iunit) obj%ov
    read(iunit) obj%vo
    read(iunit) obj%frst1
    read(iunit) obj%wtv
    close(iunit)
  endif
  end subroutine


  subroutine dump( obj ,writeUnit ,fileName )
!********************************************************************
!********************************************************************
  class(mulcha_type) :: obj
  integer,intent(in) :: writeUnit
  character(*),intent(in) :: fileName
  integer :: ii
  open(unit=writeUnit,file=trim(fileName),status='replace',form='unformatted')
  write(writeUnit) obj%n1,obj%nv,obj%no,ubound(obj%vo,2)
  write(writeUnit) obj%ov
  write(writeUnit) obj%vo
  write(writeUnit) obj%frst1
  write(writeUnit) obj%wtv
  close(writeUnit)
  end subroutine
 
 
  subroutine init_adapt( obj ,stats )
!********************************************************************
!********************************************************************
  class(mulcha_type) :: obj
  type(kaleu_stats_type),intent(in) :: stats
!
  if (stats%Nstep.le.0) then
    obj%yes = .false.
    if (allocated(obj%wtv)) deallocate( obj%wtv )
    if (allocated(obj%ave)) deallocate( obj%ave )
    if (allocated(obj%wtv)) deallocate( obj%dns )
  endif
!
  end subroutine


  subroutine dalloc( obj )
!********************************************************************
!********************************************************************
  class(mulcha_type) :: obj
  obj%yes = .false.
  obj%nv = 0
  obj%no = 0 
  obj%idat = 0
  obj%ntot = 0
  obj%istp = 0
  if (allocated(obj%ov   )) deallocate( obj%ov    )
  if (allocated(obj%vo   )) deallocate( obj%vo    )
  if (allocated(obj%frst1)) deallocate( obj%frst1 )
  if (allocated(obj%wtv)) deallocate( obj%wtv )
  if (allocated(obj%ave)) deallocate( obj%ave )
  if (allocated(obj%dns)) deallocate( obj%dns )
  end subroutine


  subroutine adapt( obj ,stats ,jremtot,nremtot )
!********************************************************************
!********************************************************************
  class(mulcha_type) :: obj
  type(kaleu_stats_type),intent(in) :: stats
  integer,intent(out) :: nremtot,jremtot(obj%nv)
  integer :: nn,kk,jj,ii,nrem
  integer :: jrem(obj%nv)
  !(realknd2!) :: factor,hsum,thrs
!
  nremtot = 0
  if (.not.obj%yes) return
  if (obj%istp.ge.stats%Nstep) return
! gather data
  if (stats%nonZeroWeight) then
    do ii=1,obj%no
      nn = obj%vo(ii,0)
      if (nn.gt.1) then
        factor = rZRO
        do jj=1,nn
          kk = obj%vo(ii,jj)
          factor = factor + obj%wtv(kk)*obj%dns(kk)
        enddo
        if (factor.ne.rZRO) factor = stats%sqrWeight/factor ! variance
!       if (factor.ne.rZRO) factor = stats%absWeight/factor ! entropy
        do jj=1,nn
          kk = obj%vo(ii,jj)
          obj%ave(kk) = obj%ave(kk) + factor*obj%dns(kk)
          obj%dns(kk) = rZRO
        enddo
      endif !(nn.gt.1)
    enddo !ii=1,obj%no
    obj%idat = obj%idat + 1
  endif
  obj%ntot = obj%ntot + 1
  if (obj%idat.eq.stats%Nbatch) then
! multi-channel step
    do ii=1,obj%no
      nn = obj%vo(ii,0)
      if (nn.gt.1) then
! Update channel weights
        hsum = rZRO
        do jj=1,nn
          kk = obj%vo(ii,jj)
          obj%ave(kk) = sqrt( obj%ave(kk)/obj%ntot ) ! variance
!         obj%ave(kk) = obj%ave(kk)/obj%ntot          ! entropy
          hsum = hsum + abs( obj%ave(kk) )
        enddo
        if (hsum.eq.rZRO) cycle
        hsum = rZRO
        do jj=1,nn
          kk = obj%vo(ii,jj)
          obj%wtv(kk) = obj%wtv(kk)*obj%ave(kk)
          obj%ave(kk) = rZRO
          hsum = hsum + obj%wtv(kk)
        enddo
        thrs = stats%ChThrs/nn
        nrem = 0
        if (hsum.eq.rZRO) hsum = rONE
! Normalized adapted channels
        do jj=1,nn
          kk = obj%vo(ii,jj)
          obj%wtv(kk) = obj%wtv(kk)/hsum
          if (obj%wtv(kk).lt.thrs) then
! Tag channel for removal
            nrem = nrem+1
            jrem(nrem) = jj
          endif
        enddo
! Remove tagged channels
        if (obj%istp.eq.stats%Nstep-1) then
        if (nrem.gt.0) then
          do jj=nrem,1,-1
            kk = obj%vo(ii,jrem(jj))
            nremtot = nremtot+1
            jremtot(nremtot) = kk
            obj%wtv(kk) = rZRO
            nn = nn-1
            do kk=jrem(jj),nn
              obj%vo(ii,kk) = obj%vo(ii,kk+1)
            enddo
          enddo
          obj%vo(ii,0) = nn 
! Re-normalize channels
          hsum = rZRO
          do jj=1,nn
            kk = obj%vo(ii,jj)
            hsum = hsum + obj%wtv(kk)
          enddo
          if (hsum.eq.rZRO) hsum = rONE
          do jj=1,nn
            kk = obj%vo(ii,jj)
            obj%wtv(kk) = obj%wtv(kk)/hsum
          enddo
        endif !(nrem.gt.0)
        endif !(obj%istp.eq.stats%Nstep-1)
      endif !(nn.gt.1)
    enddo !ii=1,obj%no
    obj%idat = 0
    obj%ntot = 0
    obj%istp = obj%istp+1
    if (obj%istp.eq.stats%Nstep) then
      deallocate(obj%ave)
      !call mulcha_print(obj) !DEBUG
    endif
  endif !(obj%idat.eq.stats%Nbatch)
  end subroutine


  subroutine mulcha_print( obj )
!********************************************************************
!********************************************************************
  type(mulcha_type) ,intent(inout) :: obj
  integer :: i0,jj,nn,ii
!
  if (messu.gt.0) then
    do i0=1,obj%no
      nn = obj%vo(i0,0)
      if (nn.gt.1) then
        write(messu,'(a17,i4)') 'avh_kaleu_mulcha:',i0
        do jj=1,nn
          ii = obj%vo(i0,jj)
          write(messu,'(a17,4x,i3,i4,e24.16)') &
            'avh_kaleu_mulcha:',jj,ii,obj%wtv(ii)
        enddo
      endif
    enddo
  endif
  end subroutine


  recursive subroutine vrtx( obj ,vtx,nn ,i0 ) 
!********************************************************************
!* Recursively create a list of splitting vertices
!********************************************************************
  class(mulcha_type),intent(in) :: obj
  integer :: nn,i0,vtx(obj%n1)
  integer :: iv,i1,i2,i3
  call mulcha_split( obj ,iv ,i0 )
  if (iv.ne.0) then
    nn = nn+1
    vtx(nn) = iv
    i1 = obj%ov(1,iv)
    i2 = obj%ov(2,iv)
    i3 = obj%ov(3,iv)
    if (i3.ne.0) i1 = i3 ! specific T-channel
    if (obj%frst1(iv)) then
      call obj%vrtx( vtx,nn ,i1 ) 
      call obj%vrtx( vtx,nn ,i2 ) 
    else
      call obj%vrtx( vtx,nn ,i2 ) 
      call obj%vrtx( vtx,nn ,i1 ) 
    endif
  endif
  end subroutine

  subroutine mulcha_split( obj ,iv ,i0 )
!*********************************************************************
!* Given  i0  choose random splitting vertex
!*********************************************************************
  type(mulcha_type) ,intent(in)  :: obj
  integer           ,intent(out) :: iv 
  integer           ,intent(in)  :: i0
  integer :: nn,ii
  !(realknd2!) :: xx,sumw
!
  nn = obj%vo(i0,0)
  if (nn.eq.0) then
! No splitting
    iv = 0
  elseif (nn.eq.1) then
! Only one splitting
    iv = obj%vo(i0,1)
  else
! Choose splitting
    xx = obj%rng()
    sumw = rZRO
    ii = 0
    if (obj%yes) then
      do while ( xx.gt.sumw .and. ii.lt.nn )
        ii = ii+1
        iv = obj%vo(i0,ii)
        sumw = sumw + obj%wtv(iv)
      enddo
      if (xx.gt.sumw) then
        if (errru.gt.0) write(errru,*) 'ERROR in Kaleu mulcha_split: ' &
          ,'no splitting chosen'
        stop
      endif
    else
      ii = 1 + int(nn*xx)
      iv = obj%vo(i0,ii)
    endif  
  endif
  end subroutine

end module


