module avh_kaleu_tree
  use avh_kskeleton
  use avh_prnt
  use avh_kaleu_units
  use avh_kaleu_model
!
  implicit none
  private ! Everything is private except the following list:
  public :: tree_type,tree_checkinput,tree_dress,tree_checkoutput
  public :: tree_nv,tree_copy
  public :: tree_check,printtree
!
! Realize that the object with the sizes defined below only exists
! at initialization-stage, and is not stored. So don't be afraid to
! put large numbers.
!
! Maximal number of vertices in a tree
  integer ,parameter :: size_nvx=30000 !SIZE_NVX
! Maximal number of "off-shell currents"
  integer ,parameter :: size_noc=2000 !SIZE_NOC
! Maximal number of vertices with the same 0-leg
  integer ,parameter :: size_nvo=2000 !SIZE_NVO
!
  type :: tree_type
    integer :: n1 ! number of external particles minus 1
    integer :: nv ! number of vertices
    integer :: no ! number of o(ff)s(hell)c(urrents)     
    integer :: ov(0:3,size_nvx)           ! osc as function of vertex   
    integer :: po(0:size_noc)             ! momentum as function of osc 
    integer :: fo(0:size_noc)             ! flavor as function of osc   
    integer :: vo(0:size_noc,0:size_nvo)  ! vertices as function of osc 
    integer :: typ(size_nvx)              ! type of vertex              
    integer :: zax(size_nvx)              ! momentum defining z-axis    
    logical :: frst1(size_nvx)            ! related to possible ordering
    integer :: nfl ! total number of flavors
  end type
 
contains


  function tree_nv( tree ) result(rslt)
!********************************************************************
!********************************************************************
  type(tree_type) ,intent(in) :: tree
  integer :: rslt
  rslt = tree%nv
  end function
!
!
  subroutine tree_checkinput( model ,process, nfinst ,label )
!********************************************************************
!********************************************************************
  type(kaleu_model_type),intent(in) :: model
  integer               ,intent(in) :: process(-2:17),nfinst
  character(*)          ,intent(in) :: label
  integer :: ii,jj,nn
  if (nfinst.lt.2) then
    if (errru.gt.0) write(errru,*) 'ERROR in Kaleu tree(',trim(label),'): ' &
      ,'nfinst =',nfinst,', but should be at least 2'
    stop
  endif
  nn = model%Npart()
  do ii=-2,nfinst
  if (ii.ne.0) then
    jj = process(ii)
    if (jj.lt.1) then
      if (errru.gt.0) write(errru,*) 'ERROR in Kaleu tree(',trim(label),'): ' &
        ,'process(',ii,') =',jj,', but should be larger than 0'
      stop
    endif
    if (jj.gt.nn) then
      if (errru.gt.0) write(errru,*) 'ERROR in Kaleu tree(',trim(label),'): ' &
        ,'process(',ii,') =',jj,', while nparticle =',nn
      stop
    endif
  endif
  enddo
  end subroutine
!
!
  subroutine tree_checkoutput( model ,tree ,process, nfinst ,cancel ,label )
!********************************************************************
!********************************************************************
  type(kaleu_model_type),intent(in) :: model
  type(tree_type)       ,intent(in) :: tree
  integer               ,intent(in) :: process(-2:17),nfinst
  logical               ,intent(out) :: cancel
  character(*)          ,intent(in) :: label
  integer :: ii
  character(160) :: line
!
  cancel = .false.
!
  if (tree_check(tree).ne.0) then
    line = 'WARNING from Kaleu tree('//trim(label)//'): the process '    &
         //' '//trim(model%symbol(process(-2)))        &
         //' '//trim(model%symbol(process(-1)))//' ->'
    do ii=1,nfinst
      line = trim(line)//' '//trim(model%symbol(process(ii)))
    enddo
    line = trim(line)//'  is not possible'
    write(*,*) trim(line)
    cancel = .true.
  else
    if (treeUnit.gt.0) then
      write(treeUnit,*) 'MESSAGE from Kaleu tree('//trim(label)//'): ' &
        ,'initializing for ',trim(model%process(nfinst,process))
    endif
    call printtree( tree ,model ) !DEBUG
  endif
  end subroutine
!
  function tree_check( tree ) result(rslt)
!********************************************************************
!********************************************************************
  type(tree_type) ,intent(in) :: tree
  integer :: rslt
  rslt = 0
  if (tree%nv.eq.0) then
    rslt = 1
  elseif (tree%po(tree%ov(0,tree%nv)).ne.base(tree%n1+1)-1) then
    rslt = 2
  endif
  end function
!
!
  subroutine tree_copy( copy ,tree )
!********************************************************************
!********************************************************************
  type(tree_type) ,intent(inout) :: copy
  type(tree_type) ,intent(in)    :: tree
  integer :: nn
  nn = maxval( tree%vo(:,0) )
  copy%n1 = tree%n1
  copy%nv = tree%nv
  copy%no = tree%no
  copy%ov(0:3,1:copy%nv)  = tree%ov(0:3,1:copy%nv)
  copy%po(0:copy%no)      = tree%po(0:copy%no)
  copy%fo(0:copy%no)      = tree%fo(0:copy%no)
  copy%vo(0:copy%no,0:nn) = tree%vo(0:copy%no,0:nn)
  copy%typ(1:copy%nv)     = tree%typ(1:copy%nv)
  copy%zax(1:copy%nv)     = tree%zax(1:copy%nv)
  copy%frst1(1:copy%nv)   = tree%frst1(1:copy%nv)
  end subroutine
!
!
  subroutine tree_dress( model ,tree ,process,nfinst ,ioption )
!********************************************************************
!* Dress up the  nkinv  kinematical vertices  kinv
!* to obtain  tree%nv  dressed vertices  tree%ov ,
!* i.e. vertices refering to flavored oc's following the rules
!* coming from  fusion .
!********************************************************************
  type(kaleu_model_type),intent(inout) :: model
  type(tree_type)       ,intent(  out) :: tree
  integer               ,intent(in)    :: process(-2:17),nfinst ,ioption
  character(72) ,parameter :: frmt= &
              "(' MESSAGE from Kaleu tree:  nvx =',i9,',  noc =',i9)"
!
  call dress0( model ,tree ,process,nfinst )
!
  if (ioption.eq.0) then
! Remove vertices with leg having momentum 1, except for the last oc
    call dress_rm_1( tree )
    if (sizeUnit.gt.0) write(sizeUnit,frmt) tree%nv,tree%no
  endif
! Re-find for each off-shell current the vertices it appears as 0-leg
  call dress_vo( tree )
  if (sizeUnit.gt.0) write(sizeUnit,frmt) tree%nv,tree%no
! Put the type of vertices: no_generation=0, t-channel=1, s-channel=2
  call dress_inittype( tree )
!
  end subroutine
 
 
  subroutine dress0( model ,tree ,process,nfinst )
!********************************************************************
!* Dress up the  nkinv  kinematical vertices  kinv
!* to obtain  tree%nv  dressed vertices  tree%ov ,
!* i.e. vertices refering to flavored oc's following the rules
!* coming from  fusion .
!********************************************************************
  type(kaleu_model_type),intent(inout) :: model
  type(tree_type)       ,intent(inout) :: tree
  integer               ,intent(in)    :: process(-2:17),nfinst
  integer,allocatable :: fusion(:,:,:)
  integer,allocatable :: o_p(:,:)
  integer :: nkinv,kinv(0:2,1:(3**size_leaves)/2)
  integer :: ii,p0,p1,p2,l1,l2,oc1,oc2,f1,f2,nfl,ifl,f0,jfl,oc0
  logical :: next
  character(80) ,parameter :: frmt1= &
    "(' ERROR in Kaleu tree: increase the parameter SIZE_NVX to at least',i9)"
  character(80) ,parameter :: frmt2=&
    "(' ERROR in Kaleu tree: increase the parameter SIZE_NOC to at least',i9)"
  character(80) ,parameter :: frmt3=&
    "(' MESSAGE from Kaleu tree:  nvx =',i9,',  noc =',i9)"
!
  tree%n1 = 0
  do p0=0,nfinst
    if (p0.eq.0) then
      ii = -1
    else
      ii = p0
    endif
    tree%n1 = tree%n1+1
    tree%po(tree%n1) = base(p0+1)
    tree%fo(tree%n1) = process(ii)
  enddo
!
  nfl = model%Npart()
  tree%nfl = nfl
!
  call model%fillFusion( fusion )
!
  allocate( o_p(1:2**tree%n1,0:nfl) )
  o_p(1:2**tree%n1,0:nfl) = 0
!
! Initialize tree%no
  tree%no = tree%n1
!
! Clean all oc's of level higher than 1
  tree%po(0) = 0
  tree%fo(0) = 0
  do ii=tree%no+1,size_noc
    tree%po(ii) = 0
    tree%fo(ii) = 0
  enddo
!
! Initialize list of vertices
  do ii=1,size_nvx
    tree%ov(0,ii) = 0
    tree%ov(1,ii) = 0
    tree%ov(2,ii) = 0
    tree%ov(3,ii) = 0
  enddo
! Initialize o_p
  do ii=1,tree%no
    o_p(tree%po(ii),0) = 1
    o_p(tree%po(ii),1) = ii
  enddo
!
  tree%nv = 0
!
  call kskeleton3( kinv,nkinv ,tree%n1 )
!
  do ii=1,nkinv
    p0 = kinv(0,ii)
    p1 = kinv(1,ii)
    p2 = kinv(2,ii)
!
    do l1=1,o_p(p1,0)
    do l2=1,o_p(p2,0)
      oc1 = o_p(p1,l1)
      oc2 = o_p(p2,l2)
      f1 = tree%fo(oc1)
      f2 = tree%fo(oc2)
      nfl = fusion( f1 ,f2 ,0 )
      if (nfl.ne.0) then
        do ifl=1,nfl
          f0 = fusion( f1 ,f2 ,ifl )
          jfl = 0
          next = .true.
          do while (next)
            jfl = jfl+1
            oc0 = o_p(p0,jfl)
            if ( f0.eq.tree%fo(oc0) ) then
              tree%nv = tree%nv+1
              if (tree%nv.gt.size_nvx) then
                if (errru.gt.0) write(errru,frmt1) tree%nv
                stop
              endif
              tree%ov(0,tree%nv) = oc0
              tree%ov(1,tree%nv) = oc1
              tree%ov(2,tree%nv) = oc2
              next = .false.
            endif
            if (next.and.(jfl.ge.o_p(p0,0))) then
              tree%no = tree%no+1
              if (tree%no.gt.size_noc) then
                if (errru.gt.0) write(errru,frmt2) tree%no
                stop
              endif
              tree%po(tree%no) = p0
              tree%fo(tree%no) = f0
              o_p(p0,0) = o_p(p0,0) + 1
              o_p(p0,o_p(p0,0)) = tree%no
              tree%nv = tree%nv+1
              if (tree%nv.gt.size_nvx) then
                if (errru.gt.0) write(errru,frmt1) tree%nv
                stop
              endif
              tree%ov(0,tree%nv) = tree%no
              tree%ov(1,tree%nv) = oc1
              tree%ov(2,tree%nv) = oc2
              next = .false.
            endif
          enddo
        enddo
      endif
    enddo
    enddo
!
  enddo
  if (sizeUnit.gt.0) write(sizeUnit,frmt3) tree%nv,tree%no
!
  deallocate( fusion )
  deallocate( o_p )
!
! Remove vertices with final momentum having the wrong flavor
  call dress_rm_last( tree ,process(-2) )
  if (sizeUnit.gt.0) write(sizeUnit,frmt3) tree%nv,tree%no
!
! Remove dead ends
  call dress_rm_dead( tree )
  if (sizeUnit.gt.0) write(sizeUnit,frmt3) tree%nv,tree%no
!
! Until here we have a list of vertices which would be sufficient for
! amplitude calculation. Now adjust for phase space generation
!
! Find for each off-shell current the vertices it appears as 0-leg
  call dress_vo( tree )
  if (sizeUnit.gt.0) write(sizeUnit,frmt3) tree%nv,tree%no
!
! Make sure the 1-leg of each vertex has the odd momentum
  call dress_odd( tree )
  if (sizeUnit.gt.0) write(sizeUnit,frmt3) tree%nv,tree%no
!
! Identify T-channel vertices, and increase the list of vertices
! corresponding to the different s-invariants
  call dress_tchan( tree )
  if (sizeUnit.gt.0) write(sizeUnit,frmt3) tree%nv,tree%no
!
  end subroutine
!
  subroutine dress_shup( tree ,ii )
!********************************************************************
!* Remove vertex ii and shift the ones below one up
!********************************************************************
  type(tree_type) ,intent(inout) :: tree
  integer ,intent(in) :: ii
  integer :: jj,kk
  tree%nv = tree%nv-1
  do jj=ii,tree%nv
    kk = jj+1
    tree%ov(0,jj) = tree%ov(0,kk)
    tree%ov(1,jj) = tree%ov(1,kk)
    tree%ov(2,jj) = tree%ov(2,kk)
    tree%ov(3,jj) = tree%ov(3,kk)
  enddo
  end subroutine
!
  subroutine dress_shdown( tree ,ii )
!********************************************************************
!* Shift all vertices below ii one down, and copy vertex ii one down
!********************************************************************
  type(tree_type) ,intent(inout) :: tree
  integer ,intent(in) :: ii
  integer :: jj,kk
  tree%nv = tree%nv+1
  if (tree%nv.gt.size_nvx) then
    if (errru.gt.0) write(errru,*) 'ERROR in Kaleu tree: ' &
      ,'increase the parameter SIZE_NVX to at least ',trim(prnt(tree%nv,'lft'))
    stop
  endif
  do jj=tree%nv,ii+1,-1
    kk = jj-1
    tree%ov(0,jj) = tree%ov(0,kk)
    tree%ov(1,jj) = tree%ov(1,kk)
    tree%ov(2,jj) = tree%ov(2,kk)
    tree%ov(3,jj) = tree%ov(3,kk)
  enddo
  end subroutine
!
  subroutine dress_rm_last( tree ,flast )
!********************************************************************
!* Remove vertices with final momentum having the wrong flavor
!********************************************************************
  type(tree_type) ,intent(inout) :: tree
  integer ,intent(in) :: flast
  integer :: p0,ii
  p0 = tree%po(tree%ov(0,tree%nv))
  ii = tree%nv
  do while ( tree%po(tree%ov(0,ii)).eq.p0 )
    if ( tree%fo(tree%ov(0,ii)).ne.flast ) call dress_shup( tree ,ii )
    ii = ii-1
  enddo
  end subroutine
!
  subroutine dress_rm_dead( tree )
!********************************************************************
!* Remove dead ends
!********************************************************************
  type(tree_type) ,intent(inout) :: tree
  logical :: isonlhs(0:tree%no),isonrhs(0:tree%no)
  integer :: ii,jj,oc0,oc1,oc2,oc3
  jj = 0
  do while (jj.ne.tree%nv)
    isonlhs(0:tree%no) = .false.
    isonrhs(0:tree%no) = .false.
    isonlhs(0:tree%n1) = .true.
    isonrhs(0:tree%n1) = .true.
    isonlhs(tree%ov(0,tree%nv)) = .true.
    isonrhs(tree%ov(0,tree%nv)) = .true.
    do ii=1,tree%nv
      isonlhs(tree%ov(0,ii)) = .true.
      isonrhs(tree%ov(1,ii)) = .true.
      isonrhs(tree%ov(2,ii)) = .true.
      isonrhs(tree%ov(3,ii)) = .true.
    enddo
    jj = tree%nv
    tree%nv = 0
    do ii=1,jj
      oc0 = tree%ov(0,ii)
      oc1 = tree%ov(1,ii)
      oc2 = tree%ov(2,ii)
      oc3 = tree%ov(3,ii)
      if (oc3.ne.0) oc1 = oc3
      if (     isonlhs(oc0).and.isonrhs(oc0) &
          .and.isonlhs(oc1).and.isonrhs(oc1) &
          .and.isonlhs(oc2).and.isonrhs(oc2) ) then
        tree%nv = tree%nv+1
        tree%ov(0:3,tree%nv) = tree%ov(0:3,ii)
        tree%typ(tree%nv)    = tree%typ(ii)
        tree%zax(tree%nv)    = tree%zax(ii)
        tree%frst1(tree%nv)  = tree%frst1(ii)
      endif
    enddo
  enddo
!  p0 = tree%po(tree%ov(0,tree%nv))
!  ii = tree%nv
!  do while (tree%po(tree%ov(0,ii)).eq.p0) 
!    ii = ii-1
!  enddo
!  do while( ii.ge.1 )
!    oc0 = tree%ov(0,ii)
!    jj = ii+1
!    do while ( jj.le.tree%nv &
!              .and.tree%ov(1,jj).ne.oc0 &
!              .and.tree%ov(2,jj).ne.oc0 &
!              .and.tree%ov(3,jj).ne.oc0 )
!      jj = jj+1
!    enddo
!    if (jj.gt.tree%nv) call dress_shup( tree ,ii )
!    ii = ii-1
!  enddo
  end subroutine
!
  subroutine dress_vo( tree )
!********************************************************************
!* Find for each off-shell current the vertices it appears as 0-leg
!********************************************************************
  type(tree_type) ,intent(inout) :: tree
  integer :: ii,oc0
  tree%vo(1:tree%no,0) = 0
  do ii=1,tree%nv
    oc0 = tree%ov(0,ii)
    tree%vo(oc0,0) = tree%vo(oc0,0)+1 ! the number of vertices
    if (tree%vo(oc0,0).gt.size_nvo) then
      if (errru.gt.0) write(errru,*) 'ERROR in Kaleu tree: ' &
        ,'increase the parameter SIZE_NVO to at least '&
        ,trim(prnt(tree%vo(oc0,0),'lft'))
      stop
    endif
    tree%vo(oc0,tree%vo(oc0,0)) = ii ! the vertex
  enddo
!
! Only external particles may occure in exactly 0 vertices 
  tree%vo(0,0) = -1
  do ii=1,tree%no
    if ( kskeleton_esab(tree%po(ii)).eq.0 &
             .and. tree%vo(ii,0).eq.0 ) tree%vo(ii,0) = -1
  enddo
  end subroutine
!
  subroutine dress_tchan( tree )
!********************************************************************
!* Identify T-channel vertices, and increase the list of vertices
!* corresponding to the different s-invariants
!********************************************************************
  type(tree_type) ,intent(inout) :: tree
  integer :: ii,oc0,p0,oc1,nn,l0,l1,p1,jj
  ii=tree%nv
  do while (ii.gt.0)
    oc0 = tree%ov(0,ii)
    p0 = tree%po(oc0)
    if (p0.ne.p0/2*2) then
      oc1 = tree%ov(1,ii)
      nn = tree%vo(oc1,0)
      l0 = 0
      l1 = ii-1
      do jj=1,nn
        p1 = tree%po(tree%ov(1,tree%vo(oc1,jj)))
        if (p1.eq.1.or.l0.eq.0) then
          l1 = l1+1
          if (l1.gt.ii) call dress_shdown( tree ,ii )
          if (p1.ne.1) then
            l0 = 1
            tree%ov(3,ii) = 0
          else
            tree%ov(3,ii) = tree%ov(2,tree%vo(oc1,jj))
          endif
        endif
      enddo
    endif
    ii = ii-1
  enddo
  end subroutine
!
  subroutine dress_odd( tree )
!********************************************************************
!* Make sure the 1-leg of each vertex has the odd momentum,
!* or else the smallest momentum
!********************************************************************
  type(tree_type) ,intent(inout) :: tree
  integer :: ii,p0,oc1,p1,oc2,p2
  do ii=1,tree%nv
    p0 = tree%po(tree%ov(0,ii))
    oc1 = tree%ov(1,ii)
    p1 = tree%po(oc1)
    if (p0.ne.p0/2*2) then
      if (p1.eq.p1/2*2) then
        oc1 = tree%ov(2,ii)
        tree%ov(2,ii) = tree%ov(1,ii)
        tree%ov(1,ii) = oc1
      endif
    else
      oc2 = tree%ov(2,ii)
      p2 = tree%po(oc2)
      if (p1.gt.p2) then
        oc1 = tree%ov(2,ii)
        tree%ov(2,ii) = tree%ov(1,ii)
        tree%ov(1,ii) = oc1
      endif
    endif
  enddo
  end subroutine
!
  subroutine dress_rm_1( tree )
!********************************************************************
!* Remove vertices with leg having momentum 1,
!* except for the last oc
!********************************************************************
  type(tree_type) ,intent(inout) :: tree
  integer :: ii,jj,p0
  p0 = tree%po(tree%ov(0,tree%nv))
  jj = 0
  do ii=1,tree%nv
    if (ii.eq.jj) cycle
    if (tree%po(tree%ov(0,ii)).eq.p0.or.tree%po(tree%ov(1,ii)).ne.1) then
      jj = jj+1
      tree%ov(0:3,jj) = tree%ov(0:3,ii)
      tree%typ(jj)    = tree%typ(ii)  
      tree%zax(jj)    = tree%zax(ii)  
      tree%frst1(jj)  = tree%frst1(ii)
    endif
  enddo
  tree%nv = jj
  end subroutine
!  
  subroutine dress_inittype ( tree )
!********************************************************************
!* put the type of vertex, zax, and frst1
!********************************************************************
  type(tree_type) ,intent(inout) :: tree
  integer :: ii,p0,p1,p2
  do ii=1,tree%nv
    p0 = tree%po(tree%ov(0,ii))
    p1 = tree%po(tree%ov(1,ii))
    p2 = tree%po(tree%ov(2,ii))
    tree%frst1(ii) = .true.
    if     (p1.eq.1.or.p2.eq.1) then
      tree%typ(ii) = 0
      tree%zax(ii) = 0
    elseif (p0.ne.p0/2*2) then
      tree%typ(ii) = 1
      tree%zax(ii) = 1
    else
      tree%typ(ii) = 2
      tree%zax(ii) = 0
    endif
  enddo
  end subroutine
   
   
  subroutine printtree( tree ,model )
!********************************************************************
!********************************************************************
  type(tree_type)       ,intent(in) :: tree
  type(kaleu_model_type),intent(in) :: model
  integer :: ii,o0,o1,o2,o3
  character(12) :: f0,f1,f2,f3
  character(134) :: frmt1,frmt2
  frmt1 = "(i5,a3 ,a1,i3,i4,1x,a2,1x,a1" &
           //",a7 ,a1,i3,i4,1x,a2,1x,a1" &
           //",a2 ,a1,i3,i4,1x,a2,1x,a1" &
           //",a2 ,a1,i3,i4,1x,a2,1x,a1)"  
  frmt2 = "(i5,a3 ,a1,i3,i4,1x,a2,1x,a1" &
           //",a7 ,a1,i3,i4,1x,a2,1x,a1" &
           //",a2 ,a1,i3,i4,1x,a2,1x,a1" &
           //",15x)"                       
  if (treeUnit.gt.0) then
  do ii=1,tree%nv
    if (tree%typ(ii).eq.1.or.tree%typ(ii).eq.3.or.tree%typ(ii).eq.5) then
      o0 = tree%ov(0,ii)
      o1 = tree%ov(1,ii)
      o2 = tree%ov(2,ii)
      o3 = tree%ov(3,ii)
      f0 = model%symbol(tree%fo(o0))
      f1 = model%symbol(tree%fo(o1))
      f2 = model%symbol(tree%fo(o2))
      f3 = model%symbol(tree%fo(o3))
      write(treeUnit,frmt1) &
         ii,':  ','[',o0,tree%po(o0),f0,']' &
       ,'  <--  ','[',o1,tree%po(o1),f1,']' &
       ,     '  ','[',o2,tree%po(o2),f2,']' &
       ,     '  ','(',o3,tree%po(o3),f3,')'
    else
      o0 = tree%ov(0,ii)
      o1 = tree%ov(1,ii)
      o2 = tree%ov(2,ii)
      f0 = model%symbol(tree%fo(o0))
      f1 = model%symbol(tree%fo(o1))
      f2 = model%symbol(tree%fo(o2))
      write(treeUnit,frmt2) &
         ii,':  ','[',o0,tree%po(o0),f0,']' &
       ,'  <--  ','[',o1,tree%po(o1),f1,']' &
       ,     '  ','[',o2,tree%po(o2),f2,']'
    endif
  enddo
  endif
  end subroutine
   
end module


