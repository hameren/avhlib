#!/usr/bin/env python
#begin header
import os,sys
pythonDir = ''
sys.path.append(pythonDir)
from avh import *
thisDir,packDir,avhDir = cr.dirsdown(2)
packageName = os.path.basename(thisDir)
buildDir = os.path.join(avhDir,'build')
#end header

date = '23-09-2018'

lines = ['! From here the package "'+packageName+'", last edit: '+date+'\n']
cr.addfile(os.path.join(thisDir,'kaleu_units.f90'),lines,delPattern="^.*")
cr.addfile(os.path.join(thisDir,'kaleu_model.f90'),lines,delPattern="^.*")
cr.addfile(os.path.join(thisDir,'kaleu_tree.f90'),lines,delPattern="^.*")
cr.addfile(os.path.join(thisDir,'kaleu_stats.f90'),lines,delPattern="^.*")
cr.addfile(os.path.join(thisDir,'kaleu_mulcha.f90'),lines,delPattern="^.*")
cr.addfile(os.path.join(thisDir,'kaleu_geninv.f90'),lines,delPattern="^.*")
cr.addfile(os.path.join(thisDir,'kaleu_inst.f90'),lines,delPattern="^.*")
cr.addfile(os.path.join(thisDir,'kaleu_base.f90'),lines,delPattern="^.*")
cr.addfile(os.path.join(thisDir,'kaleu_splvar.f90'),lines,delPattern="^.*")
cr.addfile(os.path.join(thisDir,'kaleu_ns.f90'),lines,delPattern="^.*")
cr.addfile(os.path.join(thisDir,'kaleu_cs.f90'),lines,delPattern="^.*")

fpp.incl(thisDir,lines)
fpp.case('splvar','both',lines)

cr.wfile(os.path.join(buildDir,packageName+'.f90'),lines)
