module avh_kaleu_inst
  use avh_iounits
  use avh_mathcnst
  use avh_ranvar
  use avh_grid
  use avh_kaleu_mulcha, only: rng_interface
  use avh_kaleu_stats
 
  implicit none
  private
  public :: kaleu_x1x2_type ,kaleu_inst_type ,ranKT_type
 
  type :: kaleu_x1x2_type
    private
    type(grid_type) :: grdT,grdU
    type(betaLike_type) :: betaObj
    !(realknd2!)    :: w,a,b,c,d,e
    integer         :: option
    procedure(rng_interface),pointer,nopass :: rng
  contains
    procedure :: init=>x1x2_init
    procedure :: gnrt=>x1x2_gnrt
    procedure :: wght=>x1x2_wght
    procedure :: adapt=>x1x2_adapt
    procedure :: plot=>x1x2_plot
  end type
 
  type :: ranKT_type
    type(grid_type) :: absGrid,phiGrid
    procedure(rng_interface),pointer,nopass :: rng
    !(realknd2!) :: wk
  contains
    procedure :: init=>kT_init
    procedure :: gnrt=>kT_gnrt
    procedure :: wght=>kT_wght
    procedure :: adapt=>kT_adapt
    procedure :: plot=>kT_plot
  end type

  type :: kaleu_inst_type
    private
    integer :: task,Nfinst
    type(kaleu_x1x2_type) :: X1X2
    type(ranKT_type)   :: KT1,KT2
    !(realknd2!)       :: w,Q0,x0,lam
    !(realknd2!)       :: Esq,Msq
  contains
    procedure :: init=>inst_init
    procedure :: gnrt=>inst_gnrt
    procedure :: wght=>inst_wght
    procedure :: adapt=>inst_adapt
    procedure :: plot=>inst_plot
  end type


contains

 
  subroutine x1x2_init( obj ,rng ,xmin ,exp1 ,exp2 ,option )
  class(kaleu_x1x2_type),intent(out) :: obj
  procedure(rng_interface) :: rng
  !(realknd2!),intent(in),optional :: xmin,exp1,exp2
  integer,intent(in),optional :: option
  !(realknd2!) :: tmin
!
  call init_ranvar

  obj%rng => rng
!
  call obj%grdU%init(nbatch=1000,nchmax=200,label='X1X2u')
  call obj%grdT%init(nbatch=1000,nchmax=200,label='X1X2t')
!
  tmin = 1d-6 ;if(present(xmin)) tmin=xmin**2
  obj%option = 1
  obj%a = 1-log(tmin)
  obj%b = 1/obj%a
  obj%c = tmin*obj%a
  if (present(xmin)) return
  if (present(exp1)) then
    if (exp1.le.-1d0) return
    obj%option = 2
    obj%a = exp1
    obj%c = 1/(1+obj%a)
  endif
  if (present(exp2)) then
    obj%option = 3
    obj%a =-0.5d0 ;if (present(exp1)) obj%a=exp1
    obj%b = exp2
    call obj%betaObj%init( 1+obj%b ,2+obj%a )
    obj%c = 1/(1+obj%a)
    obj%d = 1/(1+obj%b)
  endif
  if (present(option)) then
    if (option.eq.1) return
    obj%option = option
    obj%a =-0.5d0 ;if (present(exp1)) obj%a=exp1
    obj%b =-0.5d0 ;if (present(exp2)) obj%b=exp2
    obj%c = 1/(1+obj%a)
    obj%d = 1/(1+obj%b)
  endif
  end subroutine
 
  subroutine x1x2_gnrt( obj ,x1,x2 )
  class(kaleu_x1x2_type) :: obj
  intent(  out) :: x1,x2
  !(realknd2!) :: x1,x2 ,rr,tt,qq,hh,uu,wq,wr,wu,wt
  qq = obj%rng()
  rr = obj%rng()
  if (obj%option.eq.1) then
! Generate  uu=ln(x1)/(ln(x1)+ln(x2))  and  tt=x1*x2
! tt in [0,1] following  theta(tt<tmin)/tmin + theta(tt>tmin)/tt
    qq = obj%grdT%gnrt( qq ) ;wq = obj%grdT%wght()
    rr = obj%grdU%gnrt( rr ) ;wr = obj%grdU%wght()
    if (qq.le.obj%b) then
      obj%w = qq*obj%c
      qq = log(obj%w)
      obj%w = obj%c/obj%w
    else
      qq = (qq-1)*obj%a
      obj%w = obj%a
    endif
    x1 = exp( qq*(1-rr) )
    x2 = exp( qq*(  rr) )
    obj%w =-obj%w*wq*wr * qq *x1*x2
  elseif (obj%option.eq.2) then
! Generate  uu=ln(x1)/(ln(x1)+ln(x2))  and  tt=x1*x2
! tt in [0,1] following x^(obj%a)
    qq = obj%grdT%gnrt( qq ) ;wq = obj%grdT%wght()
    rr = obj%grdU%gnrt( rr ) ;wr = obj%grdU%wght()
    wt = qq**(-obj%a*obj%c)
      tt = qq*wt
      wt = wt*obj%c
    x2 = tt**rr
    x1 = tt/x2
    obj%w = wq*wr*wt*(-log(tt))
  elseif (obj%option.eq.3) then
! Generate  uu=|x1-x2|/2  and  tt=x1*x2
! uu in [-1/2,1/2] following (1-2*|uu|)**(1+obj%a) * |uu|**obj%b
    rr = obj%grdU%gnrt( rr ) ;wr = obj%grdU%wght()
    if (rr.lt.rHLF) then
!      wu = (1-2*rr)**(-obj%b*obj%d)
!      uu = (1-2*rr)*wu
!      wu = wu*obj%d
      call obj%betaObj%gnrt( uu,wu ,1-2*rr )
      uu =-uu/2
    elseif (rr.gt.rHLF) then
!      wu = (2*rr-1)**(-obj%b*obj%d)
!      uu = (2*rr-1)*wu
!      wu = wu*obj%d
      call obj%betaObj%gnrt( uu,wu ,2*rr-1 )
      uu = uu/2
    else
      uu = 0
      wu = 0
    endif
! tt in [0,1-2|uu|] following tt**obj%a
    hh = (1-2*abs(uu))**(1+obj%a)
    qq = obj%grdT%gnrt( qq ,rZRO,hh ) ;wq = obj%grdT%wght()
    wt = qq**(-obj%a*obj%c)
    tt = qq*wt
    wt = wt*obj%c
! construct x1,x2
    hh = sqrt(tt+uu*uu)
    x1 = hh+uu
    x2 = hh-uu
    obj%w = wq*wr*wu*wt/hh
  else
    qq = obj%grdT%gnrt( qq ) ;wq = obj%grdT%wght()
      wt = qq**(-obj%a*obj%c)
      tt = qq*wt
      wt = wt*obj%c
      x1 = tt
    rr = obj%grdU%gnrt( rr ) ;wr = obj%grdU%wght()
      wu = rr**(-obj%a*obj%c)
      uu = rr*wu
      wu = wu*obj%c
      x2 = uu
    obj%w = wq*wt*wr*wu
  endif
  end subroutine
 
  function x1x2_wght(obj) result(rslt)
  class(kaleu_x1x2_type) :: obj
  !(realknd2!) :: rslt
  rslt = obj%w
  end function
 
  subroutine x1x2_adapt( obj ,stats )
  class(kaleu_x1x2_type) :: obj
  type(kaleu_stats_type),intent(in) :: stats
  call obj%grdU%collect( stats%absWeight )
  call obj%grdT%collect( stats%absWeight )
  end subroutine
 
  subroutine x1x2_plot( obj ,iunit )
  class(kaleu_x1x2_type) :: obj
  intent(in) :: iunit
  integer :: iunit
  call obj%grdU%plot( iunit )
  call obj%grdT%plot( iunit )
  end subroutine


!***********************************************************************
!***********************************************************************

  subroutine kT_init( obj ,rng ,label )
  class(ranKT_type) :: obj
  procedure(rng_interface) :: rng
  intent(in) :: label
  character(*) :: label
  obj%rng => rng
  call obj%absGrid%init(nbatch=1000,nchmax=200,label='absKT'//label)
  call obj%phiGrid%init(nbatch=1000,nchmax=200,label='phiKT'//label)
  end subroutine

  subroutine kT_gnrt( obj ,k1,k2 )
  class(ranKT_type) :: obj
  intent(out  ) :: k1,k2
  !(realknd2!) :: k1,k2 ,kk,xx,yy
  xx = obj%absGrid%gnrt( obj%rng() )
  yy = obj%phiGrid%gnrt( obj%rng() )
!  call ranvar02_gnrt( kk,obj%wk ,xx ) ! x*exp(-x)
  call ranvar03_gnrt( kk,obj%wk ,xx ,2 ) ! theta(0<x<2)/4 + theta(2<x)/x^2
  kk = sqrt( kk )
  yy = r2PI*yy
  k1 = kk*cos(yy)
  k2 = kk*sin(yy)
  end subroutine

  function kT_wght( obj ) result(ww)
  class(ranKT_type) :: obj
  !(realknd2!) :: ww
  ww = r1PI*obj%wk*obj%absGrid%wght()*obj%phiGrid%wght()
  end function

  subroutine kT_adapt( obj ,stats )
  class(ranKT_type) :: obj
  type(kaleu_stats_type),intent(in) :: stats
  call obj%absGrid%collect( stats%absWeight )
!  call obj%phiGrid%collect( stats%absWeight )
  end subroutine

  subroutine kT_plot( obj ,iunit )
  class(ranKT_type) :: obj
  intent(in) :: iunit
  integer :: iunit
  call obj%absGrid%plot( iunit )
!  call obj%phiGrid%plot( iunit )
  end subroutine

!***********************************************************************
!***********************************************************************

  subroutine inst_init( obj ,ctask ,rng ,Nfinst ,Ecm ,Mass &
                       ,Q0 ,x0 ,lambda ,exp1 ,exp2 ,xmin )
  class(kaleu_inst_type) :: obj
  character(*),intent(in) :: ctask
  procedure(rng_interface) :: rng
  intent(in) :: Nfinst ,Ecm ,Mass ,Q0 ,x0 ,lambda ,exp1,exp2,xmin
  optional :: Q0 ,x0,lambda,exp1,exp2,xmin
  integer :: Nfinst
  !(realknd2!) :: Ecm,Mass,Q0,x0,lambda,exp1,exp2,expon1,expon2,xminus,xmin
  obj%Nfinst = Nfinst
  obj%Esq = Ecm*Ecm
  obj%Msq = Mass*Mass
  select case (ctask)
  case ('11')
    obj%task = 3
      obj%Q0 = 1.0d0 ;if (present(Q0)) obj%Q0=Q0*Q0
      obj%x0 = 0.5d0 ;if (present(x0)) obj%x0=x0
     obj%lam = 0.5d0 ;if (present(lambda)) obj%lam=lambda
      expon1 =-0.5d0 ;if (present(exp1)) expon1=exp1
      expon2 =-0.5d0 ;if (present(exp2)) expon2=exp2
    if (Nfinst.gt.1) then
      call obj%X1X2%init( rng ,exp1=expon1 ,exp2=expon2 )
      call obj%KT1%init( rng ,label='1' )
      call obj%KT2%init( rng ,label='2' )
    else
      call obj%X1X2%init( rng  ,xmin=max(1d-6,(Mass/Ecm)) )
      obj%KT1%rng => rng
      call obj%KT1%absGrid%init( nbatch=1000,nchmax=200,label='KT1' )
      obj%KT2%rng => rng
      call obj%KT2%absGrid%init( nbatch=1000,nchmax=200,label='KT2' )
    endif
  case ('10','01')
    obj%task = 2 ;if (ctask.eq.'01') obj%task = 1
      obj%Q0 = 1.0d0 ;if (present(Q0)) obj%Q0=Q0*Q0
      obj%x0 = 0.5d0 ;if (present(x0)) obj%x0=x0
     obj%lam = 0.5d0 ;if (present(lambda)) obj%lam=lambda
      expon1 =-0.5d0 ;if (present(exp1)) expon1=exp1
      expon2 =-0.5d0 ;if (present(exp2)) expon2=exp2
    if (Nfinst.gt.1) then
      call obj%X1X2%init( rng ,exp1=expon1 ,exp2=expon2 )
      call obj%KT1%init( rng ,label='1' )
    else
      xminus = 1d-6 ;if (present(xmin)) xminus=xmin
      call obj%X1X2%init( rng ,xmin=xminus ,exp1=expon1 ,exp2=expon2 ,option=2 )
    endif
  case ('00')
    obj%task = 0
    expon1 =-0.5d0 ;if (present(exp1)) expon1=exp1
    expon2 =-0.5d0 ;if (present(exp2)) expon2=exp2
    xminus = 1d-6  ;if (present(xmin)) xminus=xmin
    if (Nfinst.gt.1) then
      call obj%X1X2%init( rng ,xmin=xminus ,exp1=expon1 ,exp2=expon2 ,option=2 )
    else
      obj%X1X2%rng => rng
      call obj%X1X2%grdU%init( nbatch=1000,nchmax=200,label='X1X2u' )
    endif
  case default
    if (errru.ge.0) write(errru,*) 'ERROR in kaleu_inst: task not defined'
    stop
  end select
  end subroutine
 
 
  subroutine inst_gnrt( obj ,discard ,x1,kT1 ,x2,kT2 )
  class(kaleu_inst_type) :: obj
  intent(out) :: discard ,x1,kT1 ,x2,kT2
  logical :: discard
  !(realknd2!) :: x1,x2,kT1(1:2),kT2(1:2),Q1,Q2,phi,tt,cp,sp,ct,st,r1(1:2),r2(1:2)
  discard = .true.
  select case(obj%task)
  case (3)
    if (obj%Nfinst.gt.1) then
      call obj%X1X2%gnrt( x1,x2 )
      call obj%KT1%gnrt( kT1(1),kT1(2) )
      call obj%KT2%gnrt( kT2(1),kT2(2) )
      if (obj%lam.ne.rZRO) then
        Q1 = obj%Q0*(obj%x0/x1)**(obj%lam/2)
        Q2 = obj%Q0*(obj%x0/x2)**(obj%lam/2)
      else
        Q1 = obj%Q0
        Q2 = obj%Q0
      endif
      kT1 = Q1*kT1
      kT2 = Q2*kT2
      obj%w = Q1*Q2
      obj%w = obj%w*obj%w
    else
      call obj%X1X2%gnrt( x1,x2 )
      ct = x1*x2*obj%Esq-obj%Msq
      if (ct.le.rZRO) return
!
!      Q1 = sqrt(ct)
!      phi = r2PI*obj%KT1%rng()
!      r1(1:2) = [Q1*sin(phi),Q1*cos(phi)]
!      call ranvar03_gnrt( Q2 ,obj%KT2%wk ,obj%KT2%absGrid%gnrt(obj%KT2%rng()) ,2 )!obj%Esq )
!      Q2 = sqrt(Q2)
!      phi = r2PI*obj%KT2%rng()
!      r2(1:2) = [Q2*sin(phi),Q2*cos(phi)]
!      kT1 = (r1+r2)/2
!      kT2 = (r1-r2)/2
!
      call ranvar03_gnrt( Q1 ,obj%KT1%wk ,obj%KT1%absGrid%gnrt(obj%KT1%rng()) ,2 )!obj%Esq )
      Q1 = sqrt(Q1)
      call ranvar03_gnrt( Q2 ,obj%KT2%wk ,obj%KT2%absGrid%gnrt(obj%KT2%rng()) ,2 )!obj%Esq )
      Q2 = sqrt(Q2)
      ct = (ct - Q1*Q1 - Q2*Q2)/(2*Q1*Q2)
      if (abs(ct).gt.rONE) return
      st = sqrt(1-ct*ct)
      r1(1:2) = [rZRO,Q1]
      r2(1:2) = [st*Q2,ct*Q2]
      phi = r2PI*obj%X1X2%rng()
      cp = cos(phi)
      sp = sin(phi)
      kT1(1:2) = [         sp*r1(2),          cp*r1(2)]
      kT2(1:2) = [cp*r2(1)+sp*r2(2),-sp*r2(1)+cp*r2(2)]
      obj%w = 1/(st*Q1*Q2)
!
    endif
  case (2,1)
    if (obj%Nfinst.gt.1) then
      call obj%X1X2%gnrt( x1,x2 )
      call obj%KT1%gnrt( kT1(1),kT1(2) )
      if (obj%lam.ne.rZRO) then
        Q1 = obj%Q0*(obj%x0/x1)**(obj%lam/2)
      else
        Q1 = obj%Q0
      endif
      if (obj%task.eq.1) then
        kT1 = Q1*kT1
        kT2 = 0
      else
        kT2 = Q1*kT1
        kT1 = 0
      endif
      obj%w = Q1
      obj%w = obj%w*obj%w
    else
      call obj%X1X2%gnrt( x1,x2 )
      obj%w = x1*x2*obj%Esq-obj%Msq
      if (obj%w.le.rZRO) return
      obj%w = sqrt(obj%w)
      phi = r2PI*obj%X1X2%rng()
      if (obj%task.eq.1) then
        kT1 = [obj%w*cos(phi),obj%w*sin(phi)]
        kT2 = 0
      else
        kT1 = 0
        kT2 = [obj%w*cos(phi),obj%w*sin(phi)]
      endif
    endif
  case (0)
    if (obj%Nfinst.gt.1) then
      call obj%X1X2%gnrt( x1,x2 )
      kT1 = 0
      kT2 = 0
    else
      tt = obj%Msq/obj%Esq
      x2 = tt**obj%X1X2%grdU%gnrt( obj%X1X2%rng() )
      x1 = tt/x2
      obj%w =-log(tt)
      kT1 = 0
      kT2 = 0
    endif
  end select
  discard = .false.
  end subroutine


  function inst_wght( obj ) result(rslt)
  class(kaleu_inst_type) :: obj
  !(realknd2!) :: rslt
  select case (obj%task)
  case (3)
    if (obj%Nfinst.gt.1) then
      rslt = obj%w*obj%X1X2%wght()*obj%KT1%wght()*obj%KT2%wght()
    else
!      rslt = r1PI*r1PI/2*obj%X1X2%wght()*obj%KT2%wk*obj%KT2%absGrid%wght()
      rslt = obj%w * r1PI * obj%X1X2%wght() &
           * obj%KT1%wk*obj%KT1%absGrid%wght() &
           * obj%KT2%wk*obj%KT2%absGrid%wght()
    endif
  case (2,1)
    if (obj%Nfinst.gt.1) then
      rslt = obj%w*obj%X1X2%wght()*obj%KT1%wght()
    else
      rslt = r1PI * obj%X1X2%wght()
    endif
  case (0)
    if (obj%Nfinst.gt.1) then
      rslt = obj%X1X2%wght()
    else
      rslt = obj%w*obj%X1X2%grdU%wght()/obj%Esq
    endif
  end select
  end function


  subroutine inst_adapt( obj ,stats )
  class(kaleu_inst_type) :: obj
  type(kaleu_stats_type),intent(in) :: stats
  if (stats%zeroWeight) return
  select case (obj%task)
  case (3)
    if (obj%Nfinst.gt.1) then
      call obj%X1X2%adapt( stats )
      call obj%KT1%adapt( stats )
      call obj%KT2%adapt( stats )
    else
      call obj%X1X2%adapt( stats )
      call obj%KT1%absGrid%collect( stats%absWeight )
      call obj%KT2%absGrid%collect( stats%absWeight )
    endif
  case (2,1)
    if (obj%Nfinst.gt.1) then
      call obj%X1X2%adapt( stats )
      call obj%KT1%adapt( stats )
    else
      call obj%X1X2%adapt( stats )
    endif
  case (0)
    if (obj%Nfinst.gt.1) then
      call obj%X1X2%adapt( stats )
    else
      call obj%X1X2%grdU%collect( stats%absWeight )
    endif
  end select
  end subroutine


  subroutine inst_plot( obj ,iunit )
  class(kaleu_inst_type) :: obj
  integer,intent(in) ::iunit
  select case (obj%task)
  case (3)
    if (obj%Nfinst.gt.1) then
      call obj%X1X2%plot( iunit )
      call obj%KT1%plot( iunit )
      call obj%KT2%plot( iunit )
    else
      call obj%X1X2%plot( iunit )
      call obj%KT1%absGrid%plot( iunit )
      call obj%KT2%absGrid%plot( iunit )
    endif
  case (2,1)
    if (obj%Nfinst.gt.1) then
      call obj%X1X2%plot( iunit )
      call obj%KT1%plot( iunit )
    else
      call obj%X1X2%plot( iunit )
    endif
  case (0)
    if (obj%Nfinst.gt.1) then
      call obj%X1X2%plot( iunit )
    else
      call obj%X1X2%grdU%plot( iunit )
    endif
  end select
  end subroutine

 
end module


