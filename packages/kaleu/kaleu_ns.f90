module avh_kaleu_ns
  use avh_kaleu_units
  use avh_mathcnst
  use avh_prnt
  use avh_kaleu_base
  use avh_kaleu_model
  use avh_kaleu_mulcha, only: rng_interface
  use avh_kaleu_splvar !{splvar=both!}
  use avh_kaleu_stats
  use avh_grid
  use avh_kinematics
  use avh_kskeleton, only: base

  implicit none
  private
  public :: kaleu_ns_type ,mch_type

  type :: mch_type
    logical :: yes = .false.
    integer :: i0,i1
    !(realknd2!) ,allocatable :: wch(:),ave(:),dns(:)
    integer      :: Ibatch,Iincl0,Istep,Itot
    procedure(rng_interface),pointer,nopass :: rng
  contains
    procedure :: init=>mch_init
    procedure :: gnrt=>mch_gnrt
    procedure :: adapt=>mch_adapt
    procedure :: dalloc=>mch_dalloc
  end type

  type :: kaleu_ns_type
    private
    integer :: Ndip=0,Nfinst
    integer          ,allocatable :: i(:),j(:)
    type(kaleu_base_type) ,allocatable :: kaleu(:)
    type(splvar_type),allocatable :: splv(:) !{splvar=both!}
    type(grid_type)  ,allocatable :: grid(:)
    type(mch_type) :: mch
    integer,allocatable :: list(:)
    integer :: Idip=-1 ,idat=0 ,sourceDip=0
    integer :: NgridOpt,NchanOpt
    !(realknd2!) :: loopDns
    logical :: wght_not_calcd=.true.
    procedure(rng_interface),pointer,nopass :: rng
  contains
    procedure :: init
    procedure :: gnrt
    procedure :: exit
    procedure :: wght
    procedure :: adapt
    procedure :: dalloc
    procedure :: plotgrids
  end type


contains


  subroutine dalloc( obj )
!**********************************************************************
!**********************************************************************
  class(kaleu_ns_type) :: obj
  integer :: ii
  call obj%mch%dalloc
  do ii=0,obj%Ndip
    call obj%kaleu(ii)%dalloc
  enddo
  if (allocated( obj%i )    ) deallocate( obj%i )
  if (allocated( obj%j )    ) deallocate( obj%j )
  if (allocated( obj%kaleu )) deallocate( obj%kaleu )
  if (allocated( obj%splv ) ) deallocate( obj%splv ) !{splvar=both!}
  if (allocated( obj%grid ) ) deallocate( obj%grid )
  if (allocated( obj%list ) ) deallocate( obj%list )
  obj%Ndip = 0
  obj%Idip =-1
  obj%idat = 0
  obj%wght_not_calcd = .true.
  end subroutine


  subroutine init( obj ,mdl ,stats ,process ,Ecm ,Ecut ,rcg,rng ,dip,gluon )
!**********************************************************************
!* dip(1,l)=i, dip(2,l)=j
!**********************************************************************
  class(kaleu_ns_type) :: obj
  type(kaleu_model_type),intent(inout) :: mdl
  type(kaleu_stats_type),intent(in) :: stats
  type(process_type),intent(in) :: process
  !(realknd2!),intent(in) :: Ecm,Ecut
  procedure(rng_interface) :: rcg,rng
  integer,intent(in) :: dip(:,:),gluon ! expects dip(1:2,1:Ndip)
  type(process_type) :: tmpproc
  integer      :: kk,ll,mm,Nfinst
  logical      :: cancel
  character(6) :: lbl
  character(2) ,parameter :: symb(-2:9) = &
      (/'-2','-1','+0','+1','+2','+3','+4','+5','+6','+7','+8','+9'/)
  character(160) :: line
!
  call kaleu_hello
!
  obj%rng => rng
!
  if (size(dip,1).ne.2) then
    if (errru.ge.0) write(errru,*) 'ERROR in Kaleu NS init: '&
      ,'array dip has the wrong shape'
    stop
  endif
  obj%Ndip = size(dip,2)
!
  allocate( obj%i(0:obj%Ndip) )
  allocate( obj%j(0:obj%Ndip) )
  allocate( obj%kaleu(0:obj%Ndip) )
  allocate( obj%list(0:obj%Ndip) )
  obj%i = 0
  obj%j = 0
!
! The (n+1)-channel
  call obj%kaleu(0)%init( mdl ,process ,Ecm ,Ecut ,rcg,rng &
                         ,stats ,cancel=cancel,label='00' )
  if (cancel) then
    if (errru.ge.0) write(errru,*) 'ERROR in Kaleu NS: process not possible'
    stop
  endif
!
! Put mapped channels and find processes
  Nfinst = process%Nfinst
  kk = 0
  ll = 0
  do ;kk=kk+1 ;if(kk.gt.obj%Ndip)exit
    ll = ll+1
    obj%list(kk) = ll
    obj%i(kk) = dip(1,ll)
    obj%j(kk) = dip(2,ll)
!
    if (obj%i(kk).lt.0.and. &
        mdl%mass(mdl%getRef(process%infi(obj%j(kk)))).ne.rZRO) then
      if (warnu.gt.0) write(warnu,*) 'WARNING from Kaleu NS: ' &
                        ,'massive radiant from initial state, '&
                        ,'not added to the list of channels'
      kk = kk-1
      obj%Ndip = obj%Ndip-1
      cycle
    endif
!
    tmpproc = process
    if     (process%infi(obj%i(kk)).eq.gluon) then
      tmpproc%infi(obj%i(kk)) = process%infi(obj%j(kk))
    elseif (process%infi(obj%j(kk)).eq.gluon) then
      tmpproc%infi(obj%i(kk)) = process%infi(obj%i(kk))
    else
      tmpproc%infi(obj%i(kk)) = gluon
    endif
    tmpproc%infi(obj%j(kk):Nfinst-1) = tmpproc%infi(obj%j(kk)+1:Nfinst)
    tmpproc%Nfinst = tmpproc%Nfinst-1
    call tmpproc%complete_xtrn
!
    call obj%kaleu(kk)%init( mdl ,tmpproc ,Ecm ,Ecut ,rcg,rng &
                            ,stats ,cancel=cancel,label=prnt(kk,1,'0') )
    if (cancel) then
      if (warnu.gt.0) write(warnu,*) 'WARNING from Kaleu NS: ' &
        ,'process not possible, not added to the list of channels'
      call obj%kaleu(kk)%dalloc
      kk = kk-1
      obj%Ndip = obj%Ndip-1
      cycle
    endif
!
    if (nsUnit.gt.0) then
      line = 'MESSAGE from Kaleu NS:'//prnt(kk,2)//' =' &
        //' '//prnt(obj%i(kk),2)//mdl%symbol(mdl%getRef(process%infi(obj%i(kk)))) &
        //' '//prnt(obj%j(kk),1)//mdl%symbol(mdl%getRef(process%infi(obj%j(kk)))) 
      line = trim(line)//' , '//mdl%symbol(mdl%getRef(tmpproc%infi(-2)))        &
                         //' '//mdl%symbol(mdl%getRef(tmpproc%infi(-1)))//' ->'
      do mm=1,Nfinst-1
        line = trim(line)//' '//mdl%symbol(mdl%getRef(tmpproc%infi(mm)))
      enddo
      write(nsUnit,*) trim(line)
    endif
  enddo
!
! Initialize adaptive channels
  call obj%mch%init( 0,obj%Ndip ,rcg ,stats )
!
! Initialize grids
  allocate( obj%grid(1:2*obj%Ndip) )
  allocate( obj%splv(1:2*obj%Ndip) ) !{splvar=both!}
  lbl = 'D     '
  do kk=1,obj%Ndip
    lbl(2:6) = symb(obj%i(kk))//symb(obj%j(kk))//'x'
    call obj%grid(2*kk-1)%init( stats%Nbatch ,200 ,label=lbl )
    call obj%splv(2*kk-1)%init( Nbatch=stats%Nbatch ,label=lbl ) !{splvar=both!}
    lbl(6:6) = 'z'
    call obj%grid(2*kk  )%init( stats%Nbatch ,200 ,label=lbl )
    call obj%splv(2*kk  )%init( Nbatch=stats%Nbatch ,label=lbl ) !{splvar=both!}
  enddo
  end subroutine


  subroutine gnrt( obj ,discard ,psp1,psp ,pInst )
!*********************************************************************
!*********************************************************************
  class(kaleu_ns_type) :: obj
  type(psPoint_type),intent(inout) :: psp1,psp
  logical     ,intent(  out) :: discard
  !(realknd2!),intent(in   ) :: pInst(0:3,-2:-1)
  !(realknd2!) :: xx,zz,pTmp(0:3,-2:-1)
  integer      :: Idip,ii,jj
!
  call obj%mch%gnrt( Idip )
  obj%sourceDip = Idip
!
  if (Idip.eq.0) then
    call obj%kaleu(0)%gnrt( discard ,psp1 ,pInst ,[rZRO,rZRO] )
    if (discard) return
  else
    call obj%kaleu(0)%put_masses_to( psp1 )
    call psp1%put_inst( pInst ,[rZRO,rZRO] ,.true. )
    ii = obj%i(Idip)
    jj = obj%j(Idip)
    xx = obj%grid(2*Idip-1)%gnrt( obj%rng() )
    zz = obj%grid(2*Idip  )%gnrt( obj%rng() )
!{splvar=both
    xx = obj%splv(2*Idip-1)%gnrt( xx )
    zz = obj%splv(2*Idip  )%gnrt( zz )
!}splvar=both
    if (ii.gt.0) then
      call obj%kaleu(Idip)%gnrt( discard ,psp ,pInst ,[rZRO,rZRO] )
      if (discard) return
      call gnrt_final( discard ,psp1,psp ,xx,zz,obj%rng() ,ii,jj )
      if (discard) return
    else
      pTmp = pInst
      pTmp(0:3,ii) = (1-xx)*pTmp(0:3,ii) ! soft limit for x->0, not x->1
      call obj%kaleu(Idip)%gnrt( discard ,psp ,pTmp ,[rZRO,rZRO] )
      if (discard) return
      call gnrt_initial( discard ,psp1,psp ,xx,zz,obj%rng() ,ii,jj )
      if (discard) return
    endif
    call psp1%complete
  endif
!
  discard = .false.
!
  end subroutine 


  function exit( obj ,psp1 ,psp ,Ilist ,discard ,chWght ,chDnst ) result(rslt)
!*********************************************************************
! Usage:
!                   input       output
!                     |       /   |    \
!                     V      V    V     V
!  do ;if (obj%exit( psp1 ,psp ,Ilist ,discard )) exit
!    if (discard) cycle
!    if (Ilist.eq.0) then
!      value = amplitude_Nplus1( psp1 )
!      etc...
!    else
!      value = amplitude_N( Ilist ,psp )
!      etc...
!    endif
!  enddo
!
!  weight = obj%wght( psp1,psp )
!
! DO NOT CHANGE psp1 INSIDE THE LOOP
!*********************************************************************
  class(kaleu_ns_type) :: obj
  type(psPoint_type),intent(in   ):: psp1
  type(psPoint_type),intent(inout):: psp
  integer,intent(out) :: Ilist
  logical,intent(out) :: discard
  !(realknd2!),intent(out),optional :: chWght,chDnst
  logical :: rslt
  !(realknd2!) :: xx,zz,w1,w2,wx,wz,x0,z0
  integer :: Idip
!
  if (obj%Idip.lt.0) then
    obj%loopDns = 0
    obj%Idip =-1
  endif
!
  obj%Idip = obj%Idip+1
  Idip = obj%Idip
  discard = .true.
!
  rslt = (Idip.gt.obj%Ndip)
  if (rslt) then
    obj%wght_not_calcd = .false.
    obj%Idip =-1
    return
  endif
!
  Ilist = obj%list(Idip)
  obj%mch%dns(Idip) = 0
! 
  if (Idip.eq.0) then
    w1 = obj%kaleu(0)%wght( psp1 )
    if (w1.eq.rZRO) return
    obj%mch%dns(Idip) = 1/w1
  else
    call obj%kaleu(Idip)%put_masses_to( psp )
    if (obj%i(Idip).gt.0) then
      call wght_final( w1 ,psp1,psp ,xx,zz ,obj%i(Idip),obj%j(Idip) )
    else
      call wght_initial( w1 ,psp1,psp ,xx,zz ,obj%i(Idip),obj%j(Idip) )
    endif
    if (w1.eq.rZRO) return
    if (xx.le.rZRO.or.rONE.le.xx) return
    if (zz.le.rZRO.or.rONE.le.zz) return
    call psp%complete
    w2 = obj%kaleu(Idip)%wght( psp )
    if (w2.eq.rZRO) return
!{splvar=both
    call obj%splv(2*Idip-1)%wght( wx ,x0 ,xx )
    call obj%splv(2*Idip  )%wght( wz ,z0 ,zz )
    wx = wx * obj%grid(2*Idip-1)%wght( x0 )
    wz = wz * obj%grid(2*Idip  )%wght( z0 )
!}splvar=both
!{splvar=grid
    wx = obj%grid(2*Idip-1)%wght( xx )
    wz = obj%grid(2*Idip  )%wght( zz )
!}splvar=grid
    obj%mch%dns(Idip) = 1/(w1*w2*wx*wz)
  endif
! 
  obj%loopDns = obj%loopDns + obj%mch%wch(Idip)*obj%mch%dns(Idip)
!
  if (present(chWght)) chWght = obj%mch%wch(Idip)
  if (present(chDnst)) chDnst = obj%mch%dns(Idip)
!
  discard = .false.
!
  end function


  function wght( obj ,psp1,psp ) result(weight)
!*********************************************************************
!*********************************************************************
  class(kaleu_ns_type) :: obj
  type(psPoint_type),intent(in   ) :: psp1
  type(psPoint_type),intent(inout) :: psp
  !(realknd2!) :: weight
  integer :: Ilist
  logical :: discard
!
  if (obj%wght_not_calcd) then
    do ;if (obj%exit( psp1 ,psp ,Ilist ,discard )) exit
      if (discard) cycle
    enddo
  endif
!
  if (obj%loopDns.eq.rZRO) then
    weight = 0
  else
    weight = 1/obj%loopDns
  endif
  obj%wght_not_calcd = .true.
!
  end function


  subroutine adapt( obj ,stats )
!*********************************************************************
!*********************************************************************
  class(kaleu_ns_type) :: obj
  include 'kaleu_ns_adapt.h90'
  end subroutine


  subroutine plotgrids( obj ,iunit )
!*********************************************************************
!*********************************************************************
  class(kaleu_ns_type) :: obj
  integer,intent(in) :: iunit
  integer :: ii
  if (iunit.le.0) return
  do ii=1,obj%Ndip
    call obj%splv(2*ii-1)%plot( iunit ) !{splvar=both!}
    call obj%splv(2*ii  )%plot( iunit ) !{splvar=both!}
    call obj%grid(2*ii-1)%plot( iunit )
    call obj%grid(2*ii  )%plot( iunit )
  enddo
  end subroutine


!*********************************************************************
!*********************************************************************


  subroutine gnrt_final( discard ,psp1,psp ,xx,zz,phi ,ii,jj )
!*********************************************************************
!*********************************************************************
  logical           ,intent(inout) :: discard
  type(psPoint_type),intent(inout) :: psp1
  type(psPoint_type),intent(in   ) :: psp
  integer      ,intent(in) :: ii,jj
  !(realknd2!) ,intent(in) :: xx,zz,phi
  !(realknd2!) :: hh,vk,vi,zmin,zdif,yy &
                 ,pipj2,piK2,pjK2,abspj,absK,cosjk,sinjk
  integer :: ll,ij
  logical :: update
!
  ij = ii
  if (ij.gt.jj) ij=ij-1
  associate( pi=>psp1%p(psp1%b(ii)) &
            ,pj=>psp1%p(psp1%b(jj)) &
            , K=>psp1%p(psp1%AllFinal-psp1%b(ii)-psp1%b(jj)) &
            ,pij=>psp1%p(psp1%b(ii)+psp1%b(jj)) &
            , Q=>psp%p(psp%AllFinal) &
            ,Kt=>psp%p(psp%AllFinal-psp%b(ij)) )
!
  discard = .false.
!
  pi%S = pi%M**2
  pj%S = pj%M**2
  K%M = Kt%M
  K%S = Kt%S
!
  hh = ( Q%M - K%M )*( Q%M + K%M )
  pij%S = xx*( Q%M - K%M )**2 + (1-xx)*( pi%M + pj%M )**2
  pij%M = sqrt(abs(pij%S))
  vk = realMom_lambda(Q,K,pij)
  if (vk.lt.rZRO) then
    !if (errru.gt.0) write(errru,*) 'ERROR in Kaleu NS gnrt_final: ' &
    !  ,'vk<0, discard event',Q%M,K%M,pij%M-pi%M-pj%M
    discard = .true.
    return
  endif
  vk = sqrt( vk )/( hh - pij%S )
  vi = realMom_lambda(pij,pi,pj)
  if (vi.lt.rZRO) then
    !if (errru.gt.0) write(errru,*) 'ERROR in Kaleu NS gnrt_final: ' &
    !  ,'vi<0, discard event'
    discard = .true.
    return
  endif
  vi = sqrt( vi )/( pij%S + (pi%S - pj%S) )
  zmin = ( pij%S + (pi%S - pj%S) )/( 2*pij%S )
  zdif = zmin*vi*vk
  zmin = zmin-zdif
  zdif = 2*zdif
  yy = zdif*zz + zmin
!
  pipj2 = pij%S - pi%S - pj%S
  piK2 = yy*( hh - pij%S )
  pjK2 = (1-yy)*( hh - pij%S )
  pi%E = ( hh - pjK2 + (pi%S - pj%S) )/(2*Q%M)
  pj%E = ( hh - piK2 - (pi%S - pj%S) )/(2*Q%M)
  K%E = ( Q%S + K%S - pipj2 - pi%S - pj%S )/(2*Q%M)
  abspj = ( pj%E- pj%M )*( pj%E + pj%M )
  absK = ( K%E - K%M )*( K%E + K%M )
  if (abspj.lt.rZRO.or.absK.lt.rZRO) then
    if (errru.gt.0) write(errru,*) 'ERROR in Kaleu NS gnrt_final: ' &
      ,'abspj or absK <0, discard event'
    discard = .true.
    return
  endif
  abspj = sqrt(abspj)
  absK = sqrt(absK)
  cosjk = ( 2*pj%E*K%E - pjK2 )/(2*absK)
  sinjk = (abspj+cosjk)*(abspj-cosjk)
  if (sinjk.lt.rZRO) then
    if (errru.gt.0) write(errru,*) 'ERROR in Kaleu NS gnrt_final: ' &
      ,'|cosjk| >1, discard event'
    discard = .true.
    return
  endif
  sinjk = sqrt(sinjk)
  pj%V(1:3) = [ sinjk*cos(r2PI*phi) ,sinjk*sin(r2PI*phi) ,cosjk ]
  K%V(1:3) = [ rZRO ,rZRO ,absK ]
  pi%V(1:3) = -K%V(1:3) - pj%V(1:3)
  pi = pi%rotate_z(Kt%tsoob(Q))
  pj = pj%rotate_z()
  K = K%rotate_z()
  pi = pi%boost( Q )
  pj = pj%boost( Q )
  K = K%boost( Q )
!
  if (psp%Nfinst.ge.3) then
    update = .true.
    do ll=1,jj-1
      if (ll.eq.ii) cycle
      psp1%p(psp1%b(ll)) = psp%p(psp%b(ll))%boost( K ,Kt ,update )
      update = .false.
    enddo
    do ll=jj+1,psp1%Nfinst
      if (ll.eq.ii) cycle
      psp1%p(psp1%b(ll)) = psp%p(psp%b(ll-1))%boost( K ,Kt ,update )
      update = .false.
    enddo
  endif
!
  end associate
  end subroutine


  subroutine wght_final( weight ,psp1,psp ,xx,zz ,ii,jj )
!*********************************************************************
!*********************************************************************
  !(realknd2!),intent(out) :: weight ,xx,zz
  type(psPoint_type),intent(in   ) :: psp1
  type(psPoint_type),intent(inout) :: psp
  integer,intent(in) :: ii,jj
  !(realknd2!) :: vol,lam,vk,vi,rr
  integer :: ij,ll
  logical :: update
!
  weight = rZRO
!
  ij = ii
  if (ij.gt.jj) ij=ij-1
!
  associate(   Q=>psp1%p(psp1%AllFinal) &
             ,pi=>psp1%p(psp1%b(ii)) &
             ,pj=>psp1%p(psp1%b(jj)) &
              ,K=>psp1%p(psp1%AllFinal-psp1%b(ii)-psp1%b(jj)) &
            ,pij=>psp1%p(psp1%b(ii)+psp1%b(jj)) &
            ,pik=>psp1%p(psp1%AllFinal-psp1%b(jj)) &
            ,pit=>psp%p(psp%b(ij)) &
             ,Kt=>psp%p(psp%AllFinal-psp%b(ij)) )
!
  pit%S = pit%M**2
  Kt%M = K%M
  Kt%S = K%S
!
  lam = realMom_lambda(Q,pit,K)
  if (lam.le.rZRO) then
    if (errru.ge.0) write(errru,*) 'ERROR in Kaleu NS wght_final: ' &
      ,'lam<0, putting weight=0'
    return
  endif
  vk = realMom_lambda(Q,pij,K)
  if (vk.le.rZRO) then
    if (errru.ge.0) write(errru,*) 'ERROR in Kaleu NS wght_final: ' &
      ,'vk<0, putting weight=0'
    return
  endif
  vi = realMom_lambda(pij,pi,pj)
  if (vi.le.rZRO) then
    if (errru.ge.0) write(errru,*) 'ERROR in Kaleu NS wght_final: ' &
      ,'vi<0, putting weight=0'
    return
  endif
!
  vol = ((Q%M-K%M)-pi%M-pj%M)*((Q%M-K%M)+pi%M+pj%M)
  xx = ( pij%M - pi%M - pj%M )*( pij%M + pi%M + pj%M )/vol
!
  zz = ( (pik%M-K%M)*(pik%M+K%M) - pi%S )*( pij%S*2 ) &
     - ( (pij%M-pj%M)*(pij%M+pj%M) + pi%S )*( (Q%M-K%M)*(Q%M+K%M) - pij%S )
  zz = rHLF + zz/sqrt( 4*vk*vi )
!
  rr = sqrt(lam/vk)
  weight = r2PI*sqrt(vi)*vol/(4*rr*pij%S) ! extra factor (2pi)^3
!
  call psp%put_inst( psp1 )
!
  Kt%V = rr*K%V + ( (Q%S+K%S)*(1-rr) - (pit%S-rr*pij%S) )/(2*Q%S)*Q%V
  call Kt%complete
  pit%V = Q%V - Kt%V
  call pit%complete
!
  update = .true.
  if (psp%Nfinst.ge.3) then
    do ll=1,jj-1
      if (ll.eq.ij) cycle
      psp%p(psp%b(ll)) = psp1%p(psp1%b(ll))%boost( Kt ,K ,update )
      update = .false.
    enddo
    do ll=jj,psp%Nfinst
      if (ll.eq.ij) cycle
      psp%p(psp%b(ll)) = psp1%p(psp1%b(ll+1))%boost( Kt ,K ,update )
      update = .false.
    enddo
  endif
!
  end associate
  end subroutine


  subroutine gnrt_initial( discard ,psp1,psp ,xx,zz,phi ,aa,jj )
!********************************************************************
!* Soft limit when x->0, not x->1
!********************************************************************
  logical           ,intent(inout) :: discard
  type(psPoint_type),intent(inout) :: psp1
  type(psPoint_type),intent(in   ) :: psp
  integer      ,intent(in) :: aa,jj
  !(realknd2!) ,intent(in) :: xx,zz,phi
  !(realknd2!) :: yy,absK,cosKa,sinKa
  integer :: ll
  logical :: update
!
  discard = .false.
!
  associate( pa=>psp1%p(psp1%Nsize-psp1%b(aa)) & ! -p_a, so with positive energy
            ,pj=>psp1%p(psp1%b(jj)) &
            , K=>psp1%p(psp1%AllFinal-psp1%b(jj)) &
            , Q=>psp1%p(psp1%AllFinal) & ! -p_a-p_b should be defined
            ,Kt=>psp%p(psp%AllFinal) )
!
  pj%S = pj%M**2
  K%S = Kt%S
  K%M = Kt%M
!
  yy = xx*zz + (1-xx)
!
  pa = pa%tsoob(Q)
  absK = realMom_lambda(Q,K,pj)
  if (absK.lt.rZRO) then
    !if (errru.gt.0) write(errru,*) 'ERROR in Kaleu NS gnrt_initial: ' &
    !  ,'absK^2<0, discard event',Q%S,K%S,pj%S
    discard = .true.
    return
  endif
  absK = absK/(4*Q%S)
  K%E = sqrt( absK + K%S )
  absK = sqrt( absK )
  cosKa = K%E - yy*Q%S/(2*pa%E)
  sinKa = (absK+cosKa)*(absK-cosKa)
  if (sinKa.lt.rZRO) then
    !if (errru.gt.0) write(errru,*) 'ERROR in Kaleu NS gnrt_initial: ' &
    !  ,'|cosKa| >1, discard event'
    discard = .true.
    return
  endif
  sinKa = sqrt(sinKa)
  K%V = [ sinKa*cos(r2PI*phi) ,sinKa*sin(r2PI*phi) ,cosKa ]
  pj%E = Q%M - K%E
  pj%V(1:3) =-K%V(1:3)
  K = K%rotate_z(pa)
  pj = pj%rotate_z()
  K = K%boost( Q )
  pj = pj%boost( Q )
!
  update = .true.
  do ll=1,jj-1
    psp1%p(psp1%b(ll)) = psp%p(psp%b(ll))%boost( K ,Kt ,update )
    update = .false.
  enddo
  do ll=jj+1,psp1%Nfinst
    psp1%p(psp1%b(ll)) = psp%p(psp%b(ll-1))%boost( K ,Kt ,update )
    update = .false.
  enddo
!
  end associate
  end subroutine


  subroutine wght_initial( weight ,psp1,psp ,xx,zz ,aa,jj )
!********************************************************************
!* Soft limit when x->0, not x->1
!********************************************************************
  !(realknd2!)      ,intent(out)   :: weight ,xx,zz
  type(psPoint_type),intent(in   ) :: psp1
  type(psPoint_type),intent(inout) :: psp
  integer      ,intent(in)    :: aa,jj
  !(realknd2!) :: hh,pInst(0:3,-2:-1)
  integer :: ll
  logical :: update
!
  weight = rZRO
!
  associate( pa=>psp1%p(psp1%b(aa)) & ! p_a, with negative energy
            ,pj=>psp1%p(psp1%b(jj)) &
            , K=>psp1%p(psp1%AllFinal-psp1%b(jj)) &
            ,Ka=>psp1%p(psp1%AllFinal-psp1%b(jj)+psp1%b(aa)) & ! K+pa
            , Q=>psp1%p(psp1%AllFinal) &
            ,Kt=>psp%p(psp%AllFinal) )
!
  hh = (Q%M-K%M)*(Q%M+K%M)
  zz =-Ka%S/hh
  xx = hh/Q%S
  weight = r2PI*hh/4 ! extra factor (2pi)^3
!
  pInst(  0,-2) = psp1%p(psp1%b(-2))%E
  pInst(1:3,-2) = psp1%p(psp1%b(-2))%V(1:3)
  pInst(  0,-1) = psp1%p(psp1%b(-1))%E
  pInst(1:3,-1) = psp1%p(psp1%b(-1))%V(1:3)
  pInst(:,aa) = (K%S/Q%S)*pInst(:,aa) 
  call psp%put_inst( pInst ,[rZRO,rZRO] ,.true. )
!
  update = .true.
  do ll=1,jj-1
    psp%p(psp%b(ll)) = psp1%p(psp1%b(ll))%boost( Kt ,K ,update )
    update = .false.
  enddo
  do ll=jj,psp%Nfinst
    psp%p(psp%b(ll)) = psp1%p(psp1%b(ll+1))%boost( Kt ,K ,update )
    update = .false.
  enddo
!
  end associate
  end subroutine




  subroutine mch_init( obj ,i0,i1 ,rng ,stats )
!********************************************************************
!********************************************************************
  class(mch_type) ,intent(inout) :: obj
  type(kaleu_stats_type),intent(in) :: stats
  integer        ,intent(in)    :: i0,i1
  procedure(rng_interface) :: rng
!
  obj%i0 = min(i0,i1)
  obj%i1 = max(i0,i1)
  obj%yes = ( (stats%Nstep.gt.0) .and. (obj%i0.le.obj%i1) )
!
  if (.not.obj%yes) return
!
  obj%rng => rng
!
  allocate( obj%wch(obj%i0:obj%i1) )
  allocate( obj%ave(obj%i0:obj%i1) )
  allocate( obj%dns(obj%i0:obj%i1) )
!
!DEBUG  obj%Nbatch = 1000000 !DEBUG
  obj%Ibatch = 0
  obj%Iincl0 = 0
  obj%Istep = 0
  obj%Itot = 0
  obj%ave = rZRO
  obj%wch = rONE
  obj%dns = rZRO
  !obj%wch = 0 !DEBUG
  !obj%wch(0) = 0 !DEBUG
  obj%wch = obj%wch/sum( obj%wch )
  end subroutine


  subroutine mch_dalloc( obj )
!********************************************************************
!********************************************************************
  class(mch_type) ,intent(inout) :: obj
  obj%yes = .false.
  obj%Ibatch = 0
  obj%Iincl0 = 0
  obj%Istep = 0
  if (allocated(obj%wch)) deallocate( obj%wch )
  if (allocated(obj%ave)) deallocate( obj%ave )
  if (allocated(obj%dns)) deallocate( obj%dns )
  end subroutine


  subroutine mch_gnrt( obj ,ii )
!********************************************************************
!********************************************************************
  class(mch_type) ,intent(inout) :: obj
  integer        ,intent(out)   :: ii
  !(realknd2!) :: xx,sumw
  xx = obj%rng()
  sumw = rZRO
  ii = obj%i0-1
  if (obj%yes) then
    do while ( xx.gt.sumw .and. ii.lt.obj%i1 )
      ii = ii+1
      sumw = sumw + obj%wch(ii)
    enddo
    if (xx.gt.sumw) then
      if (errru.gt.0) write(errru,*) 'ERROR in Kaleu NS mch_gnrt: ' &
        ,'no channel chosen'
      stop
    endif
  else
    ii = 1 + int( (obj%i1-obj%i0+1)*xx )
  endif  
  end subroutine


  subroutine mch_adapt( obj ,stats )
!********************************************************************
!********************************************************************
  class(mch_type) ,intent(inout) :: obj
  type(kaleu_stats_type),intent(in) :: stats
  integer :: ii
  !(realknd2!) :: factor,hsum,thrs
!
  if (.not.obj%yes) return
  if (obj%Istep.ge.stats%Nstep) return
! gather data
  if (stats%nonZeroWeight) then
    factor = dot_product(obj%wch,obj%dns)    
    if (factor.ne.rZRO) factor = stats%sqrWeight/factor ! variance
!   if (factor.ne.rZRO) factor = stats%absWeight/factor ! entropy
    obj%ave = obj%ave + factor*obj%dns
    obj%dns = rZRO
    obj%Ibatch = obj%Ibatch + 1
    obj%Itot = obj%Itot + 1
  endif
  obj%Iincl0 = obj%Iincl0 + 1
  if (obj%Ibatch.eq.stats%Nbatch) then
    obj%ave = sqrt( obj%ave/obj%Iincl0 ) ! variance
!   obj%ave =       obj%ave/obj%Iincl0   ! entropy
    if (sum(obj%ave).eq.rZRO) return
    obj%wch = obj%wch*obj%ave
    obj%ave = rZRO
    hsum = sum(obj%wch)
    obj%wch = obj%wch/hsum
    if (obj%Istep.eq.stats%Nstep-1) then
      thrs = stats%ChThrs/(obj%i1-obj%i0+1)
      where (obj%wch.lt.thrs) obj%wch = rZRO
      hsum = sum(obj%wch)
      obj%wch = obj%wch/hsum
    endif
    obj%Ibatch = 0
    obj%Iincl0 = 0
    obj%Istep = obj%Istep+1
!    do ii=obj%i0,obj%i1                                   !DEBUG
!      if (obj%wch(ii).eq.rZRO) cycle                       !DEBUG
!      write(6,'(a31,i3,a3,e16.8)') &                      !DEBUG
!       ' MESSAGE from Kaleu CS/NS: wch(',ii,') =' & !DEBUG
!                                  ,obj%wch(ii)            !DEBUG
!    enddo                                                 !DEBUG
  endif !(obj%Ibatch.eq.stats%Nbatch)
  end subroutine


end module


