module avh_kskeleton
  !(usekindmod!)
  use avh_iounits
  use avh_doloops
  implicit none
  private
  public :: base
  public :: kskeleton3,kskeleton4,kskeleton5
  public :: kskeleton_esab,kskeleton_exp
  public :: kskeleton_isThere,kskeleton_jointwo,kskeleton_groups
  public :: size_leaves,size_xternal
  public :: momTree,update_momTree

! maxn: 3 ,4  ,5   ,6   ,7    ,8     ,9     ,10     ,11      ,12      ,13
! max3: 6 ,25 ,90  ,301 ,966  ,3025  ,9330  ,28501  ,86526   ,261625  ,788970
! max4: 7 ,35 ,155 ,651 ,2667 ,10795 ,43435 ,174251 ,698027  ,2794155 ,11180715 
! max5: 7 ,36 ,170 ,791 ,3717 ,17746 ,85960 ,420981 ,2077427 ,10302656
  integer,parameter :: kskeleton_maxn = 10
  integer,parameter :: kskeleton_max2 =  2**kskeleton_maxn
  integer,parameter :: kskeleton_max3 = (3**kskeleton_maxn)/2
  integer,parameter :: kskeleton_max4 = (4**kskeleton_maxn)/6
  integer,parameter :: kskeleton_max5 = (5**kskeleton_maxn+4**kskeleton_maxn)/24

  integer,parameter :: size_leaves = kskeleton_maxn
  integer,parameter :: size_xternal = size_leaves+1

  integer,parameter :: base(0:31)=&
      [0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768 &
      ,65536,131072,262144,524288,1048576,2097152,4194304,8388608,16777216 &
      ,33554432,67108864,134217728,268435456,536870912,1073741824] 
  
  integer,parameter :: momTreeSize(1:15)= &
    [1,1,1,6,10,35,56,154,246,627,1012,2497,4082,9893,16368]

  type :: momTree_type
    integer :: N,o
    !(integer1!),allocatable :: p1(:),p2(:)
  end type

  type(momTree_type),allocatable,protected,save :: momTree(:)
  integer,save :: NfinstMax=0

  interface kskeleton3
    module procedure kskeleton3,kskeleton3_i1
  end interface

  interface kskeleton4
    module procedure kskeleton4,kskeleton4_i1
  end interface

  interface kskeleton5
    module procedure kskeleton5,kskeleton5_i1
  end interface

  interface kskeleton_esab
    module procedure kskeleton_esab_1,kskeleton_esab_2
    module procedure kskeleton_esab_11,kskeleton_esab_22
  end interface

  interface kskeleton_exp
    module procedure kskeleton_exp,kskeleton_exp_n
  end interface


contains


  subroutine update_momTree( Nfinst )
  integer,intent(in) :: Nfinst
  integer :: ii
  if (allocated(momTree)) then
    if (Nfinst.le.NfinstMax) then
      return
    else
      deallocate(momTree)
    endif
  endif
  NfinstMax = Nfinst
  allocate(momTree(1:NfinstMax+2))
  do ii=3,Nfinst+2
    call fill_momTree( momTree(ii) ,ii )
  enddo
  end subroutine

  subroutine fill_momTree( t ,Nxtrn )
  type(momTree_type),intent(out) :: t
  integer           ,intent(in)  :: Nxtrn
  integer :: ii,p1,kk,p2
  integer :: aa(Nxtrn)
  type(grepen_type) :: loopa
  allocate(t%p1(1:momTreeSize(Nxtrn)))
  allocate(t%p2(1:momTreeSize(Nxtrn)))
  if (Nxtrn.le.3) then
    t%o = base(4)-1
    t%N = 1
    t%p1(1) = 1
    t%p2(1) = 2
  else
    t%o = base(Nxtrn+1)-1
    t%N = 0
    do ii=2,Nxtrn/2
      call loopa%init(ii,aa,1,Nxtrn)
      do
        p1 = 0
        kk = 0
        do ;kk=kk+1
          p1 = p1 + base(aa(kk))
          if (kk.eq.ii/2) exit
        enddo
        p2 = 0
        do ;kk=kk+1
          p2 = p2 + base(aa(kk))
          if (kk.eq.ii) exit
        enddo
        t%N = t%N+1
        t%p2(t%N) = p2
        t%p1(t%N) = p1
        if (loopa%exit(aa)) exit
      enddo
    enddo
  endif
  end subroutine


  subroutine kskeleton5( kinv,Nvrtx ,Nxtrn )
!*******************************************************************
! Create list of "kinematical vertices", including 5-point vertices
! kinv(0,ii) = kinv(1,ii) + kinv(2,ii) + kinv(3,ii) + kinv(4,ii)
!*******************************************************************
  intent(out) :: kinv,Nvrtx
  integer :: kinv(0:,1:)
  include 'kskeleton5.h90' 
    !execute xpnd: array | kinv(#1,#2)
  end subroutine

  subroutine kskeleton5_i1( kinv,Nvrtx ,Nxtrn )
  intent(out) :: kinv,Nvrtx
  !(integer1!) :: kinv(0:,1:)
  include 'kskeleton5.h90' 
    !execute xpnd: array | kinv(#1,#2)
  end subroutine

  subroutine kskeleton4( kinv,Nvrtx ,Nxtrn )
!*******************************************************************
! Create list of "kinematical vertices", including 4-point vertices
! kinv(0,ii) = kinv(1,ii) + kinv(2,ii) + kinv(3,ii)
!*******************************************************************
  intent(out) :: kinv,Nvrtx
  integer :: kinv(0:,1:)
  include 'kskeleton4.h90' 
    !execute xpnd: array | kinv(#1,#2)
  end subroutine

  subroutine kskeleton4_i1( kinv,Nvrtx ,Nxtrn )
  intent(out) :: kinv,Nvrtx
  !(integer1!) :: kinv(0:,1:)
  include 'kskeleton4.h90' 
    !execute xpnd: array | kinv(#1,#2)
  end subroutine

  subroutine kskeleton3( kinv,Nvrtx ,Nxtrn )
!*******************************************************************
! Create list of "kinematical vertices", just 3-point vertices
! kinv(0,ii) = kinv(1,ii) + kinv(2,ii)
!*******************************************************************
  intent(out) :: kinv,Nvrtx
  integer :: kinv(0:,1:)
  include 'kskeleton3.h90' 
    !execute xpnd: array | kinv(#1,#2)
  end subroutine

  subroutine kskeleton3_i1( kinv,Nvrtx ,Nxtrn )
  intent(out) :: kinv,Nvrtx
  !(integer1!) :: kinv(0:,1:)
  include 'kskeleton3.h90' 
    !execute xpnd: array | kinv(#1,#2)
  end subroutine

      
  function kskeleton_esab_2( ii ) result(jj)
!*******************************************************************
! Return "jj" if "ii=base(jj)", else return 0
!*******************************************************************
  integer ,intent(in) :: ii
  integer :: jj,jmax,jmin
  jmin = 0
  jmax = kskeleton_maxn+1
  do while (jmin.lt.jmax-1)
    jj = (jmin+jmax)/2
    if     (base(jj).eq.ii) then ;return
    elseif (base(jj).lt.ii) then ;jmin=jj
    else                         ;jmax=jj
    endif
  enddo
  jj = 0
  end function
  function kskeleton_esab_1( ii ) result(jj)
  !(integer1!) ,intent(in) :: ii
  integer :: jj,jmax,jmin
  jmin = 0
  jmax = kskeleton_maxn+1
  do while (jmin.lt.jmax-1)
    jj = (jmin+jmax)/2
    if     (base(jj).eq.ii) then ;return
    elseif (base(jj).lt.ii) then ;jmin=jj
    else                         ;jmax=jj
    endif
  enddo
  jj = 0
  end function

  function kskeleton_esab_22( ii ,nmax ) result(jj)
  integer ,intent(in) :: ii,nmax
  integer :: jj,jmax,jmin
  jmin = 0
  jmax = nmax+1
  do while (jmin.lt.jmax-1)
    jj = (jmin+jmax)/2
    if     (base(jj).eq.ii) then ;return
    elseif (base(jj).lt.ii) then ;jmin=jj
    else                         ;jmax=jj
    endif
  enddo
  jj = 0
  end function
  function kskeleton_esab_11( ii ,nmax ) result(jj)
  !(integer1!) ,intent(in) :: ii,nmax
  integer :: jj,jmax,jmin
  jmin = 0
  jmax = nmax+1
  do while (jmin.lt.jmax-1)
    jj = (jmin+jmax)/2
    if     (base(jj).eq.ii) then ;return
    elseif (base(jj).lt.ii) then ;jmin=jj
    else                         ;jmax=jj
    endif
  enddo
  jj = 0
  end function


  subroutine kskeleton_exp( aa,na,bb,nb ,ii )
!*********************************************************************
!                ii = base(aa(1)) + base(aa(2)) + ... + base(aa(na))
! base(nmax+1)-1-ii = base(bb(1)) + base(bb(2)) + ... + base(bb(nb))
!*********************************************************************
  integer ,intent(in)  :: ii
  integer ,intent(out) :: aa(kskeleton_maxn),na,bb(kskeleton_maxn),nb
  integer :: tmp,jj,dif,aao(kskeleton_maxn),bbo(kskeleton_maxn)
  tmp = ii
  na = 0
  nb = 0
  do jj=kskeleton_maxn-1,0,-1
    dif = tmp-base(jj+1)
    if (dif.ge.0) then
      na = na+1
      aao(na) = jj+1
      tmp = dif
    else
      nb = nb+1
      bbo(nb) = jj+1
    endif
  enddo
  do jj=1,na
    aa(jj) = aao(na-jj+1)
  enddo
  do jj=1,nb
    bb(jj) = bbo(nb-jj+1)
  enddo
  end subroutine

  subroutine kskeleton_exp_n( aa,na,bb,nb ,ii ,nmax )
!*********************************************************************
!                ii = base(aa(1)) + base(aa(2)) + ... + base(aa(na))
! base(nmax+1)-1-ii = base(bb(1)) + base(bb(2)) + ... + base(bb(nb))
!*********************************************************************
  integer ,intent(in)  :: ii,nmax
  integer ,intent(out) :: aa(nmax),na,bb(nmax),nb
  integer :: tmp,jj,dif,aao(nmax),bbo(nmax)
  tmp = ii
  na = 0
  nb = 0
  do jj=nmax-1,0,-1
    dif = tmp-base(jj+1)
    if (dif.ge.0) then
      na = na+1
      aao(na) = jj+1
      tmp = dif
    else
      nb = nb+1
      bbo(nb) = jj+1
    endif
  enddo
  do jj=1,na
    aa(jj) = aao(na-jj+1)
  enddo
  do jj=1,nb
    bb(jj) = bbo(nb-jj+1)
  enddo
  end subroutine


  subroutine kskeleton_isThere( aa ,pp ,nmax )
  integer,intent(in) :: pp,nmax
  logical,intent(out):: aa(nmax)
  integer :: tmp,dif,jj
  tmp = pp
  do jj=nmax,1,-1
    dif = tmp-base(jj)
    aa(jj) = (dif.ge.0)
    if (aa(jj)) tmp = dif
  enddo
  end subroutine
  

  subroutine kskeleton_jointwo( array ,n1,n2 ,nmax )
!***********************************************************************
! let  x(i) =base(i)           for i<n1 and n1<i<n2
!      x(n1)=base(n1)+base(n2)
!      x(i) =base(i+1)         for n2=<i<nmax
! let j = c(1)*base(1) + c(2)*base(2) +...+ c(nmax-1)*base(nmax-1)
! with  c(i)={0,1}
! then  array(j) = c(1)*x(1) + c(2)*x(2) +...+ c(nmax-1)*x(nmax-1)
!***********************************************************************
  integer,intent(in) :: n1,n2,nmax
  integer,intent(out) :: array(0:base(nmax)-1)
  integer :: p1,p2,ii,aa,bb,jj,dif
  p1 = n1
  p2 = n2
  if (p1.gt.p2) then
    p1 = n2
    p2 = n1
  endif
  array(0) = 0
  do ii=1,base(nmax)-1
    bb = ii
    aa = bb
    do jj=nmax-1,p2,-1
      dif = bb-base(jj)
      if (dif.ge.0) bb = dif
    enddo
    array(ii) = 2*(aa-bb)
    aa = bb
    do jj=p2-1,1,-1
      dif = bb-base(jj)
      if (dif.ge.0) then
        bb = dif
        if (jj.eq.p1) array(ii) = array(ii) + base(p2)
      endif
    enddo
    array(ii) = array(ii) + (aa-bb)
  enddo
  end subroutine
  

  subroutine kskeleton_groups( array ,groups ,Ngroups ,Nsize )
!***********************************************************************
! let  b(i) = sum( base(groups(j,i)) ,j=1..Nsize )  where  base(0)=0
! let n = c(1)*base(1) + c(2)*base(2) +...+ c(Ngroups)*base(Ngroups)
! with  c(i)={0,1}
! then  array(n) = c(1)*b(1) + c(2)*b(2) +...+ c(Ngroups)*b(Ngroups)
! Examples of groups with Nsize=5:
!   Ngroups=4 [ 1,0,0,0,0 ,2,4,0,0,0 ,3,0,0,0,0 ,5,0,0,0,0 ]
!   Ngroups=3 [ 1,5,0,0,0 ,2,4,0,0,0 ,3,0,0,0,0 ]
!   Ngroups=2 [ 1,5,0,0,0 ,2,3,4,0,0 ]
!***********************************************************************
  integer,intent(in) :: Ngroups,Nsize
  integer,intent(in) :: groups(Nsize,Ngroups)
  integer,intent(out) :: array(0:base(Ngroups+1)-1)
  integer :: ii,jj,bb(Nsize),tmp,dif,Ncount
  logical :: touched(Nsize)
  touched = .false.
  Ncount = 0
  do ii=1,Ngroups
    bb(ii) = 0
    do jj=1,Nsize
      if (touched(groups(jj,ii))) then
        if (errru.ge.0) write(errru,*) 'ERROR in kskeleton_groups: ' &
          ,'multiple occurences'
        stop
      else
        touched(groups(jj,ii)) = .true.
        Ncount = Ncount+1
      endif
      bb(ii) = bb(ii) + base(groups(jj,ii))
      if (groups(jj+1,ii).eq.0) exit
    enddo
  enddo
  if (Ncount.ne.Nsize) then
    if (errru.ge.0) write(errru,*) 'ERROR in kskeleton_groups: ' &
      ,'groups inconsistent with Nsize'
    stop
  endif
  array(0) = 0
  do ii=1,base(Ngroups+1)-1
    array(ii) = 0
    tmp = ii
    do jj=Ngroups,1,-1
      dif = tmp - base(jj)
      if (dif.ge.0) then
        tmp = dif
        array(ii) = array(ii) + bb(jj)
      endif
    enddo
  enddo
  end subroutine
 

end module


