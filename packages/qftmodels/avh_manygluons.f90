module avh_manygluons
  use avh_iounits
  use avh_prnt
  use avh_mathcnst
  use avh_kskeleton ,only: base
  use avh_lorentz ,only: POLARANG,XDEFINED,WARDTEST
  use avh_doloops
  use avh_colormatrix_ng
  use avh_colormatrix_qq
  implicit none

  private
  public :: init_manygluons ,fill_colormatrix
  public :: amplitude_qq ,amplitude_ng

  integer,parameter :: Nmax=8 ,inverse(8)=[8,7,6,5,4,3,2,1] ! (Nmax+1)-gluon amplitudes

  type :: permNode_type
    integer,allocatable :: dght(:)
    integer,allocatable :: next(:)
  end type

! tree is a structure to store data which is labeled by the permutations
! of (1,2,...,Ntot) and all permutations of all subsets of (1,2,...,Ntot).
  type(permNode_type),allocatable,save :: tree(:)
  integer,allocatable,save :: root(:)
  integer,save :: Ntot=0,iNode,Nnode

  !(complex2!),allocatable,save :: current(:,:)
  !(complex2!),save :: coup3,coup4

  integer,parameter :: ang1=0,ang2=1,sqr1=2,sqr2=3

contains


  subroutine init_manygluons( NgluonMax ,gQCD )
  integer,intent(in) :: NgluonMax
  !(realknd2!),intent(in) :: gQCD
  integer :: ii,rUnit
  if (Ntot.lt.NgluonMax-1) then
    Ntot = NgluonMax-1
    if (Ntot.gt.Nmax) then
      write(*,*) 'ERROR in manygluons_init: NgluonMax=',trim(prnt(NgluonMax,'lft')) &
                ,' must be at most ',trim(prnt(Nmax+1,'lft'))
      stop
    endif
!   Build the permutation tree
    if (allocated(root)) deallocate(root)
    allocate(root(Ntot))
    Nnode = 0
    iNode = 0
    if (allocated(tree)) deallocate(tree)
    do ii=1,Ntot
      call increase_iNode
      root(ii) = iNode
      call fill_tree( Ntot-1 ,[ii] )
    enddo
    call final_tree
!   Allocate currents
    if (allocated(current)) deallocate(current)
    allocate(current(0:3,1:Nnode))
  endif
!
! Set coupling
  coup3 = cIMA*gQCD/rSQRT2
  coup4 = cIMA*gQCD**2/2
!
  end subroutine


  subroutine amplitude_qq( amp ,Ngluon ,momentum ,momSqr ,qMass ,helicity ,xPolvec ,polang )
  integer,intent(in) :: Ngluon
  !(complex2!),intent(out) :: amp(igamm(Ngluon+1))
  !(complex2!),intent(in) :: momentum(0:3,*),momSqr(*),qMass
  integer,intent(in) :: helicity(*)
  !(realknd2!),intent(in) :: polang(*)
  !(complex2!),intent(in) :: xPolVec(0:3,*)
  !(complex2!) :: polvec(0:3,1:Nmax),qBra(0:3),qKet(0:3)
  integer :: Nleaf,Nperm,pp(Nmax),Icount,jj
  type(permut_type) :: permut
!
  do jj=1,Ngluon
    polvec(:,jj) = xVector( momentum(:,base(jj)) ,helicity(jj) ,xPolvec(:,jj) ,polang(jj) )
  enddo
  jj = Ngluon+1
  qKet = xSpinor( .true. ,momentum(:,base(jj)) ,qMass ,helicity(jj) ,xPolvec(:,jj) ,polang(jj) )
  jj = Ngluon+2
  qBra = xSpinor( .false. ,momentum(:,base(jj)) ,qMass ,helicity(jj) ,xPolvec(:,jj) ,polang(jj) )
  call calc_qq( Ngluon ,momentum ,momSqr ,polvec(0:3,1:Ngluon) ,qKet ,qMass )
  Icount = 0
  pp(Ngluon+1) = Ngluon+1
  call permut%init(Ngluon,pp(1:Ngluon))
  do ;Icount=Icount+1
    qKet(:) = current(:,find(pp(1:Ngluon+1)))
    amp(Icount) = qBra(ang1)*qKet(ang1) + qBra(ang2)*qKet(ang2) &
                + qBra(sqr1)*qKet(sqr1) + qBra(sqr2)*qKet(sqr2)
  if (permut%exit(pp(1:Ngluon))) exit
  enddo
!
  end subroutine


  subroutine amplitude_ng( amp ,Nxtrn ,momentum ,momSqr ,helicity ,xPolvec ,polang )
  integer,intent(in) :: Nxtrn
  !(complex2!),intent(out) :: amp(igamm(Nxtrn-1))
  !(complex2!),intent(in) :: momentum(0:3,*),momSqr(*)
  integer,intent(in) :: helicity(*)
  !(realknd2!),intent(in) :: polang(*)
  !(complex2!),intent(in) :: xPolVec(0:3,*)
  !(complex2!) :: polvec(0:3,1:Nmax),ee(0:3)
  integer :: Nleaf,Nperm,pp(Nmax),Icount,jj
  type(permut_type) :: permut
!
! Put external polarization vectors.
  do jj=1,Nxtrn
    polvec(:,jj) = xVector( momentum(:,base(jj)) ,helicity(jj) ,xPolvec(:,jj) ,polang(jj) )
  enddo
  Nleaf = Nxtrn-1
  Nperm = Nxtrn-2
  call calc_ng( Nleaf,Nleaf ,momentum ,momSqr ,polvec(0:3,1:Nleaf) )
  pp(Nleaf) = Nleaf
  Icount = 0
  call permut%init(Nperm,pp(1:Nperm))
  do ;Icount=Icount+1
    ee(:) = current(:,find(pp(1:Nleaf)))
    amp(Icount) = ee(0)*polvec(0,Nxtrn) - ee(1)*polvec(1,Nxtrn) &
                - ee(2)*polvec(2,Nxtrn) - ee(3)*polvec(3,Nxtrn)
  if (permut%exit(pp(1:Nperm))) exit
  enddo
!
  end subroutine

  subroutine increase_iNode
  integer :: oldsize
  type(permNode_type),allocatable :: tmp(:)
  iNode = iNode+1
  if (iNode.gt.Nnode) Nnode = iNode
  if (allocated(tree)) then
    oldsize = size(tree)
    if (Nnode.gt.oldsize) then
      if (allocated(tmp)) deallocate(tmp)
      allocate(tmp(oldsize))
      tmp(1:oldsize) = tree(1:oldsize)
      deallocate(tree)
      allocate(tree(2*Nnode))
      tree(1:oldsize) = tmp(1:oldsize)
      deallocate(tmp)
    endif
  else
    allocate(tree(Nnode))
  endif
  end subroutine

  recursive subroutine fill_tree( nn ,exclude )
  integer,intent(in) :: nn
  integer,intent(in) :: exclude(Ntot-nn)
  integer :: nextexcl(Ntot-nn+1),kk,ii,jj,jNode
  jNode = iNode
  if (allocated(tree(jNode)%dght)) deallocate(tree(jNode)%dght)
  if (allocated(tree(jNode)%next)) deallocate(tree(jNode)%next)
  allocate(tree(jNode)%dght(nn))
  allocate(tree(jNode)%next(nn))
  kk = 0
  do ii=1,Ntot
    if (any(ii.eq.exclude(1:Ntot-nn))) cycle
    kk = kk+1
    call increase_iNode
    tree(jNode)%next(kk) = ii
    tree(jNode)%dght(kk) = iNode
    if (nn.gt.1) then
      nextexcl(1:Ntot-nn) = exclude(1:Ntot-nn)
      nextexcl(Ntot-nn+1) = ii
      call fill_tree( nn-1 ,nextexcl )
    else
      if (allocated(tree(iNode)%dght)) deallocate(tree(iNode)%dght)
      if (allocated(tree(iNode)%next)) deallocate(tree(iNode)%next)
    endif
  enddo
  end subroutine

  subroutine final_tree
  type(permNode_type),allocatable :: tmp(:)
  if (allocated(tmp)) deallocate(tmp)
  allocate(tmp(Nnode))
  tmp(1:Nnode) = tree(1:Nnode)
  deallocate(tree)
  allocate(tree(Nnode))
  tree(1:Nnode) = tmp(1:Nnode)
  deallocate(tmp)
  end subroutine

  function find(per) result(jNode)
! Return the single-integer label associated with permutation per
  integer,intent(in) :: per(:)
  integer :: jNode ,ii,j0,j1,jj
  jNode = root(per(1))
  do ii=2,size(per,1)
    j0 = 1
    j1 = size(tree(jNode)%next)
    do while (j1-j0.gt.1)
      jj = (j0+j1)/2
      if (per(ii).le.tree(jNode)%next(jj)) then
        j1 = jj
      else
        j0 = jj
      endif
    enddo
    if (per(ii).eq.tree(jNode)%next(j0)) then
      jNode = tree(jNode)%dght(j0)
    else
      jNode = tree(jNode)%dght(j1)
    endif
  enddo
  end function




  function braSpinor( pp ) result(uu)
  !(complex2!),intent(in) :: pp(0:3)
  !(complex2!) :: uu(0:3),expiphi,pPLS,pMIN,pTRA,pTSR
  !(realknd2!) :: aPLS,aMIN
  pPLS = pp(0)+pp(3)
  pMIN = pp(0)-pp(3)
  pTRA = pp(1)+pp(2)*cIMA
  pTSR = pp(1)-pp(2)*cIMA
  aPLS = abs(pPLS)
  if (aPLS.le.rZRO) then
    aMIN = abs(pMIN)
    if (aMIN.le.rZRO) then
      uu = 0
    else ! consider momentum to be result of p1->0,p2=0
      aMIN = sqrt(aMIN)
      uu(sqr1) = 0
      uu(sqr2) = pMIN/aMIN
      uu(ang1) =-aMIN
      uu(ang2) = 0
    endif
  else
    expiphi = aPLS/pPLS
    aPLS = 1/sqrt( aPLS )
    uu(sqr1) =  pPLS*aPLS
    uu(sqr2) =  pTSR*aPLS
    uu(ang1) = -pTRA*expiphi*aPLS
    uu(ang2) =  uu(sqr1)*expiphi
  endif
  end function

  function ketFromBra( vv ) result(uu)
  !(complex2!),intent(in) :: vv(0:3)
  !(complex2!) :: uu(0:3)
  uu(sqr1) =-vv(sqr2)
  uu(sqr2) = vv(sqr1)
  uu(ang1) = vv(ang2)
  uu(ang2) =-vv(ang1)
  end function

  function braFromKet( vv ) result(uu)
  !(complex2!),intent(in) :: vv(0:3)
  !(complex2!) :: uu(0:3)
  uu(sqr1) = vv(sqr2)
  uu(sqr2) =-vv(sqr1)
  uu(ang1) =-vv(ang2)
  uu(ang2) = vv(ang1)
  end function

  function xVector( kk ,helicity ,xEntry ,polang ) result(ee)
  !(complex2!),intent(in) :: kk(0:3),xEntry(0:3)
  integer,intent(in) :: helicity
  !(realknd2!),intent(in) :: polang
  !(complex2!) :: ee(0:3),uu(0:3),vv(0:3),zz,e11,e12,e21,e22,expon
  select case (helicity)
  case (+1)
    uu = braSpinor(kk)
    vv = ketFromBra(braSpinor(xEntry))
    zz =-rSQRT2/( uu(ang1)*vv(ang1) + uu(ang2)*vv(ang2) )/2
    e11 = vv(ang1) * uu(sqr1) * zz
    e12 = vv(ang1) * uu(sqr2) * zz
    e21 = vv(ang2) * uu(sqr1) * zz
    e22 = vv(ang2) * uu(sqr2) * zz
    ee = [(e11+e22),(e12+e21),(e12-e21)*cIMA,(e11-e22)]
  case (-1)
    uu = ketFromBra(braSpinor(kk))
    vv = braSpinor(xEntry)
    zz =-rSQRT2/( vv(sqr1)*uu(sqr1) + vv(sqr2)*uu(sqr2) )/2
    e11 = uu(ang1) * vv(sqr1) * zz
    e12 = uu(ang1) * vv(sqr2) * zz
    e21 = uu(ang2) * vv(sqr1) * zz
    e22 = uu(ang2) * vv(sqr2) * zz
    ee = [(e11+e22),(e12+e21),(e12-e21)*cIMA,(e11-e22)]
  case (POLARANG)
    expon = exp(cIMA*r1PI*polang)
    uu = braSpinor(kk)
    vv = ketFromBra(braSpinor(xEntry))
    zz =-rSQRT2/( uu(ang1)*vv(ang1) + uu(ang2)*vv(ang2) )/2
    zz = zz*expon
    e11 = vv(ang1) * uu(sqr1) * zz
    e12 = vv(ang1) * uu(sqr2) * zz
    e21 = vv(ang2) * uu(sqr1) * zz
    e22 = vv(ang2) * uu(sqr2) * zz
    zz = rSQRT2/( uu(sqr1)*vv(sqr1) + uu(sqr2)*vv(sqr2) )/2
    zz = zz/expon
    e11 = e11 + vv(sqr2) * uu(ang2) * zz
    e12 = e12 - vv(sqr1) * uu(ang2) * zz
    e21 = e21 - vv(sqr2) * uu(ang1) * zz
    e22 = e22 + vv(sqr1) * uu(ang1) * zz
    ee = [(e11+e22),(e12+e21),(e12-e21)*cIMA,(e11-e22)]
  case (XDEFINED)
    ee = xEntry
  case (WARDTEST)
    ee = kk(:)/kk(0)
  end select
  end function

  function xSpinor( isKet ,kk ,mm ,helicity ,xEntry ,polang ) result(uu)
  logical,intent(in) :: isKet
  !(complex2!),intent(in) :: kk(0:3),mm,xEntry(0:3)
  integer,intent(in) :: helicity
  !(realknd2!),intent(in) :: polang
  !(complex2!) :: qq(0:3),uu(0:3),vv(0:3),ww(0:3),pp(0:3)
  !(complex2!) :: expon,zz,cIk2,cIq2
!
  if (mm.eq.cZRO) then
    select case (helicity)
    case (-1)
      uu = braSpinor(kk)
      uu(sqr1) = 0
      uu(sqr2) = 0
      if (isKet) uu = ketFromBra(uu)
    case (+1)
      uu = braSpinor(kk)
      uu(ang1) = 0
      uu(ang2) = 0
      if (isKet) uu = ketFromBra(uu)
    case (POLARANG)
      expon = exp(cIMA*r1PI*polang)
      uu = braSpinor(kk)
      uu(ang1) = uu(ang1)*expon
      uu(ang2) = uu(ang2)*expon
      uu(sqr1) = uu(sqr1)/expon
      uu(sqr2) = uu(sqr2)/expon
      if (isKet) uu = ketFromBra(uu)
    case (XDEFINED)
      uu(sqr1) = xEntry(sqr1)
      uu(sqr2) = xEntry(sqr2)
      uu(ang1) = xEntry(ang1)
      uu(ang2) = xEntry(ang2)
      if (.not.isKet) uu = braFromKet(uu)
    end select
  else
    qq = xEntry
    cIk2 = cIMA*kk(2)
    cIq2 = cIMA*qq(2)
    zz = ( kk(0)+kk(3) )*( qq(0)-qq(3) ) + ( kk(0)-kk(3) )*( qq(0)+qq(3) ) &
       - ( kk(1)+cIk2  )*( qq(1)-cIq2  ) - ( kk(1)-cIk2  )*( qq(1)+cIq2  )
    zz = mm*mm/zz
    qq = qq*zz
    pp = kk-qq
    select case (helicity)
    case (-1)
      vv = braSpinor(pp) 
      ww = braSpinor(qq)
      zz = ( vv(ang1)*ww(ang2) - ww(ang1)*vv(ang2) )/mm ! <v|w> / m
      uu(ang1) = vv(ang1)
      uu(ang2) = vv(ang2)
      uu(sqr1) = zz*ww(sqr1)
      uu(sqr2) = zz*ww(sqr2)
      if (isKet) uu = ketFromBra(uu)
    case (+1)
      vv = braSpinor(pp) 
      ww = braSpinor(qq)
      zz = ( ww(sqr1)*vv(sqr2) - vv(sqr1)*ww(sqr2) )/mm ! [v|w] / m
      uu(sqr1) = vv(sqr1)
      uu(sqr2) = vv(sqr2)
      uu(ang1) = zz*ww(ang1)
      uu(ang2) = zz*ww(ang2)
      if (isKet) uu = ketFromBra(uu)
    case (POLARANG)
      expon = exp(cIMA*r1PI*polang)
      vv = braSpinor(pp) 
      ww = braSpinor(qq)
      zz = ( vv(ang1)*ww(ang2) - ww(ang1)*vv(ang2) )/mm ! <v|w> / m
      uu(ang1) = vv(ang1)*expon
      uu(ang2) = vv(ang2)*expon
      uu(sqr1) = ww(sqr1)*zz*expon
      uu(sqr2) = ww(sqr2)*zz*expon
      zz = ( ww(sqr1)*vv(sqr2) - vv(sqr1)*ww(sqr2) )/mm ! [v|w] / m
      uu(ang1) = uu(ang1) + ww(ang1)*zz/expon
      uu(ang2) = uu(ang2) + ww(ang2)*zz/expon
      uu(sqr1) = uu(sqr1) + vv(sqr1)/expon
      uu(sqr2) = uu(sqr2) + vv(sqr2)/expon
      if (isKet) uu = ketFromBra(uu)
    case (XDEFINED)
      uu(sqr1) = xEntry(sqr1)
      uu(sqr2) = xEntry(sqr2)
      uu(ang1) = xEntry(ang1)
      uu(ang2) = xEntry(ang2)
      if (.not.isKet) uu = braFromKet(uu)
    end select
  endif
  end function
  



  subroutine calc_qq( NgluonTot ,momentum ,momSqr ,polvec ,qKet ,qMass )
  integer,intent(in) :: NgluonTot
  !(complex2!),intent(in) :: momentum(0:3,*),momSqr(*)
  !(complex2!),intent(in) :: polVec(0:3,1:NgluonTot),qKet(0:3),qMass
  !(complex2!) :: prop,uu(0:3),u1(0:3),u2(0:3)
  type(grepen_type) :: grepen
  type(permut_type) :: permut
  integer :: Ngluon,ii(Nmax),pp(Nmax),jj,kk,qBar,momLabel
  
!
  call calc_ng( NgluonTot,NgluonTot+1 ,momentum ,momSqr ,polvec(0:3,1:NgluonTot) )
  qBar = NgluonTot+1
  current(:,find([qBar])) = qKet(:)
!
  Ngluon = 1
  ii(2) = qBar
  do jj=1,NgluonTot ;ii(1)=jj
    uu = vertex( ii(1:1) ,ii(2:2) )
    if (Ngluon.lt.NgluonTot) uu = propagator( uu ,base(ii(1))+base(ii(2)) )
    current(:,find(ii([1,2]))) = uu(:)
  enddo
!
  Ngluon = 2
  if (Ngluon.le.NgluonTot) then
    ii(3) = qBar
    do jj=   1,NgluonTot-1 ;ii(1)=jj
    do kk=jj+1,NgluonTot   ;ii(2)=kk
      u1 = vertex( ii([1])   ,ii([2,3]) ) &
         + vertex( ii([1,2]) ,ii([3]) )
      u2 = vertex( ii([2])   ,ii([1,3]) ) &
         + vertex( ii([2,1]) ,ii([3]) )
      if (Ngluon.lt.NgluonTot) then
        momLabel = base(ii(1)) + base(ii(2)) + base(ii(3))
        u1 = propagator( u1 ,momLabel )
        u2 = propagator( u2 ,momLabel )
      endif
      current(:,find(ii([1,2,3]))) = u1(:)
      current(:,find(ii([2,1,3]))) = u2(:)
    enddo
    enddo
  endif
!
  do Ngluon=3,NgluonTot
    ii(Ngluon+1) = qBar
    pp(Ngluon+1) = Ngluon+1
    call grepen%init( Ngluon ,ii ,1,NgluonTot )
    do
      if (Ngluon.lt.NgluonTot) momLabel = sum(base(ii(1:Ngluon+1)))
      call permut%init(Ngluon,pp(1:Ngluon))
      do
        uu = 0
        do kk=1,Ngluon
          uu = uu + vertex( ii(pp(1:kk)) ,ii(pp(kk+1:Ngluon+1)) )
        enddo
        if (Ngluon.lt.NgluonTot) uu = propagator( uu ,momLabel )
        current(:,find(ii(pp(1:Ngluon+1)))) = uu(:)
      if (permut%exit(pp(1:Ngluon))) exit
      enddo
    if (grepen%exit(ii(1:Ngluon))) exit
    enddo
  enddo

  contains

    function propagator( uu ,momLabel ) result(rslt)
    !(complex2!),intent(in) :: uu(0:3)
    integer,intent(in) :: momLabel
    !(complex2!) :: rslt(0:3) ,cIp2,pPLS,pMIN,pTRA,pTSR
    cIp2 = cIMA*momentum(2,momLabel)
    pPLS = momentum(0,momLabel)+momentum(3,momLabel)
    pMIN = momentum(0,momLabel)-momentum(3,momLabel)
    pTRA = momentum(1,momLabel)+cIp2
    pTSR = momentum(1,momLabel)-cIp2
    rslt(sqr1) = pMIN*uu(ang1) - pTSR*uu(ang2)
    rslt(sqr2) =-pTRA*uu(ang1) + pPLS*uu(ang2)
    rslt(ang1) = pPLS*uu(sqr1) + pTSR*uu(sqr2)
    rslt(ang2) = pTRA*uu(sqr1) + pMIN*uu(sqr2)
    rslt = (uu*qMass - rslt)*(cIMA/(momSqr(momLabel)-qMass*qMass))
    end function
 
    function vertex( i1,i2 ) result(rslt)
    integer,intent(in) :: i1(:),i2(:)
    !(complex2!),dimension(0:3) :: rslt,ee,uu
    !(complex2!) :: cIe2,ePLS,eMIN,eTRA,eTSR
    ee(:) = current(:,find(i1))
    uu(:) = current(:,find(i2))
    cIe2 = cIMA*ee(2)
    ePLS = ee(0)+ee(3)
    eMIN = ee(0)-ee(3)
    eTRA = ee(1)+cIe2
    eTSR = ee(1)-cIe2
    rslt(sqr1) = ( eMIN*uu(ang1) - eTSR*uu(ang2) )*coup3
    rslt(sqr2) = (-eTRA*uu(ang1) + ePLS*uu(ang2) )*coup3
    rslt(ang1) = ( ePLS*uu(sqr1) + eTSR*uu(sqr2) )*coup3
    rslt(ang2) = ( eTRA*uu(sqr1) + eMIN*uu(sqr2) )*coup3
    end function

  end subroutine


  subroutine calc_ng( NleafTot,Nterminate ,momentum ,momSqr ,polvec )
  integer,intent(in) :: NleafTot,Nterminate
  !(complex2!),intent(in) :: momentum(0:3,*),momSqr(*)
  !(complex2!),intent(in) :: polVec(0:3,1:NleafTot)
  !(complex2!) :: prop,ee(0:3),e1(0:3),e2(0:3)
  type(grepen_type) :: grepen
  type(permut_type) :: permut
  integer :: Nleaf,Nperm,ii(Nmax),pp(Nmax),jj,kk,ll,sgn
!
  do jj=1,NleafTot
    current(:,find([jj])) = polvec(:,jj)
  enddo

  Nleaf = 2
  if (Nleaf.le.NleafTot) then
    do jj=   1,NleafTot-1 ;ii(1)=jj
    do kk=jj+1,NleafTot   ;ii(2)=kk
      ee = vertex3( ii(1:1) ,ii(2:2) )
      if (Nleaf.eq.Nterminate) then
        current(:,find(ii([1,2]))) = ee(:)
      else
        ee = ee*(-cIMA/momSqr(base(ii(1))+base(ii(2))))
        current(:,find(ii([1,2]))) = ee(:)
        if (ii(Nleaf).lt.Nterminate) then
          current(:,find(ii([2,1]))) =-ee(:)
        endif
      endif
    enddo
    enddo
  endif

  Nleaf = 3
  if (Nleaf.le.NleafTot) then
    do jj=   1,NleafTot-2 ;ii(1)=jj
    do kk=jj+1,NleafTot-1 ;ii(2)=kk
    do ll=kk+1,NleafTot   ;ii(3)=ll
      e1 = vertex3( ii([1])   ,ii([2,3])          ) &
         + vertex3( ii([1,2]) ,ii([3])            ) &
         + vertex4( ii([1])   ,ii([2])   ,ii([3]) )
      e2 = vertex3( ii([2])   ,ii([1,3])          ) &
         + vertex3( ii([2,1]) ,ii([3])            ) &
         + vertex4( ii([2])   ,ii([1])   ,ii([3]) )
      if (Nleaf.eq.Nterminate) then
        current(:,find(ii([1,2,3]))) = e1(:)
        current(:,find(ii([2,1,3]))) = e2(:)
      else
        prop =-cIMA/momSqr(base(ii(1))+base(ii(2))+base(ii(3)))
        e1 = e1*prop
        e2 = e2*prop
        current(:,find(ii([1,2,3]))) = e1(:)
        current(:,find(ii([2,1,3]))) = e2(:)
        if (ii(Nleaf).lt.Nterminate) then
          current(:,find(ii([3,2,1]))) = e1(:)
          current(:,find(ii([3,1,2]))) = e2(:)
          ee =-e1-e2
          current(:,find(ii([1,3,2]))) = ee(:)
          current(:,find(ii([2,3,1]))) = ee(:)
        endif
      endif
    enddo
    enddo
    enddo
  endif

  sgn = 1
  do Nleaf=4,NleafTot
    sgn =-sgn
    Nperm = Nleaf-1
    call grepen%init( Nleaf ,ii ,1,NleafTot )
    do
      if (Nleaf.lt.Nterminate) prop =-cIMA/momSqr(sum(base(ii(1:Nleaf))))
      pp(Nleaf) = Nleaf
      call permut%init(Nperm,pp(1:Nperm))
      do
        ee = 0
        do kk=1,Nperm
          ee = ee + vertex3( ii(pp(1:kk)) ,ii(pp(kk+1:Nleaf)) )
        enddo
        do kk=   1,Nperm-1
        do ll=kk+1,Nperm
          ee = ee + vertex4( ii(pp(1:kk)) ,ii(pp(kk+1:ll)) ,ii(pp(ll+1:Nleaf)) )
        enddo
        enddo
        if (Nleaf.eq.Nterminate) then
          current(:,find(ii(pp(1:Nleaf)))) = ee(:)
        else
          ee = ee*prop
          current(:,find(ii(pp(1:Nleaf)))) = ee(:)
          if (ii(Nleaf).lt.Nterminate) then
            current(:,find(ii(pp(inverse(Nmax-Nperm:Nmax))))) = sgn*ee(:)
          endif
        endif
      if (permut%exit(pp(1:Nperm))) exit
      enddo
      if (Nleaf.lt.Nterminate.and.ii(Nleaf).lt.Nterminate) then
        select case (Nleaf)
        case (4) ! for at-least-6-gluon amplitude
          call permut%init(Nperm,pp(1:Nperm))
          do
            current(:,find(ii(pp([1,4,2,3])))) = &
              +gete([1,3,2,4])+gete([3,1,2,4])+gete([3,2,1,4])
            current(:,find(ii(pp([1,2,4,3])))) = &
              -gete([1,2,3,4])-gete([1,3,2,4])-gete([3,1,2,4])
          if (permut%exit(pp(1:Nperm))) exit
          enddo
        case (5) ! for at-least-7-gluon amplitude
          call permut%init(Nperm,pp(1:Nperm))
          do
            current(:,find(ii(pp([1,5,2,3,4])))) = &
              -gete([1,4,3,2,5])-gete([4,1,3,2,5]) &
              -gete([4,3,1,2,5])-gete([4,3,2,1,5])
            current(:,find(ii(pp([1,2,5,3,4])))) = &
              +gete([1,2,4,3,5])+gete([1,4,2,3,5])+gete([1,4,3,2,5]) &
              +gete([4,1,2,3,5])+gete([4,1,3,2,5])+gete([4,3,1,2,5])
            current(:,find(ii(pp([1,2,3,5,4])))) = &
              -gete([1,2,3,4,5])-gete([1,2,4,3,5]) &
              -gete([1,4,2,3,5])-gete([4,1,2,3,5])
          if (permut%exit(pp(1:Nperm))) exit
          enddo
        case (6) ! for at-least-8-gluon amplitude
          call permut%init(Nperm,pp(1:Nperm))
          do
            current(:,find(ii(pp([1,6,2,3,4,5])))) = &
              +gete([1,5,4,3,2,6])+gete([5,1,4,3,2,6])+gete([5,4,1,3,2,6]) &
              +gete([5,4,3,1,2,6])+gete([5,4,3,2,1,6])
            current(:,find(ii(pp([1,2,6,3,4,5])))) = &
              -gete([1,2,5,4,3,6])-gete([1,5,2,4,3,6])-gete([1,5,4,2,3,6]) &
              -gete([1,5,4,3,2,6])-gete([5,1,2,4,3,6])-gete([5,1,4,2,3,6]) &
              -gete([5,1,4,3,2,6])-gete([5,4,1,2,3,6])-gete([5,4,1,3,2,6]) &
              -gete([5,4,3,1,2,6])
            current(:,find(ii(pp([1,2,3,6,4,5])))) = &
              +gete([1,2,3,5,4,6])+gete([1,2,5,3,4,6])+gete([1,2,5,4,3,6]) &
              +gete([1,5,2,3,4,6])+gete([1,5,2,4,3,6])+gete([1,5,4,2,3,6]) &
              +gete([5,1,2,3,4,6])+gete([5,1,2,4,3,6])+gete([5,1,4,2,3,6]) &
              +gete([5,4,1,2,3,6])
            current(:,find(ii(pp([1,2,3,4,6,5])))) = &
              -gete([1,2,3,4,5,6])-gete([1,2,3,5,4,6])-gete([1,2,5,3,4,6]) &
              -gete([1,5,2,3,4,6])-gete([5,1,2,3,4,6])
          if (permut%exit(pp(1:Nperm))) exit
          enddo
        case (7) ! for at-least-9-gluon amplitude
          call permut%init(Nperm,pp(1:Nperm))
          do
            current(:,find(ii(pp([1,7,2,3,4,5,6])))) = &
              -gete([1,6,5,4,3,2,7])-gete([6,1,5,4,3,2,7])-gete([6,5,1,4,3,2,7]) &
              -gete([6,5,4,1,3,2,7])-gete([6,5,4,3,1,2,7])-gete([6,5,4,3,2,1,7])
            current(:,find(ii(pp([1,2,7,3,4,5,6])))) = &
              +gete([1,2,6,5,4,3,7])+gete([1,6,2,5,4,3,7])+gete([1,6,5,2,4,3,7]) &
              +gete([1,6,5,4,2,3,7])+gete([1,6,5,4,3,2,7])+gete([6,1,2,5,4,3,7]) &
              +gete([6,1,5,2,4,3,7])+gete([6,1,5,4,2,3,7])+gete([6,1,5,4,3,2,7]) &
              +gete([6,5,1,2,4,3,7])+gete([6,5,1,4,2,3,7])+gete([6,5,1,4,3,2,7]) &
              +gete([6,5,4,1,2,3,7])+gete([6,5,4,1,3,2,7])+gete([6,5,4,3,1,2,7])
            current(:,find(ii(pp([1,2,3,7,4,5,6])))) = &
              -gete([1,2,3,6,5,4,7])-gete([1,2,6,3,5,4,7])-gete([1,2,6,5,3,4,7]) &
              -gete([1,2,6,5,4,3,7])-gete([1,6,2,3,5,4,7])-gete([1,6,2,5,3,4,7]) &
              -gete([1,6,2,5,4,3,7])-gete([1,6,5,2,3,4,7])-gete([1,6,5,2,4,3,7]) &
              -gete([1,6,5,4,2,3,7])-gete([6,1,2,3,5,4,7])-gete([6,1,2,5,3,4,7]) &
              -gete([6,1,2,5,4,3,7])-gete([6,1,5,2,3,4,7])-gete([6,1,5,2,4,3,7]) &
              -gete([6,1,5,4,2,3,7])-gete([6,5,1,2,3,4,7])-gete([6,5,1,2,4,3,7]) &
              -gete([6,5,1,4,2,3,7])-gete([6,5,4,1,2,3,7])
            current(:,find(ii(pp([1,2,3,4,7,5,6])))) = &
              +gete([1,2,3,4,6,5,7])+gete([1,2,3,6,4,5,7])+gete([1,2,3,6,5,4,7]) &
              +gete([1,2,6,3,4,5,7])+gete([1,2,6,3,5,4,7])+gete([1,2,6,5,3,4,7]) &
              +gete([1,6,2,3,4,5,7])+gete([1,6,2,3,5,4,7])+gete([1,6,2,5,3,4,7]) &
              +gete([1,6,5,2,3,4,7])+gete([6,1,2,3,4,5,7])+gete([6,1,2,3,5,4,7]) &
              +gete([6,1,2,5,3,4,7])+gete([6,1,5,2,3,4,7])+gete([6,5,1,2,3,4,7])
            current(:,find(ii(pp([1,2,3,4,5,7,6])))) = &
              -gete([1,2,3,4,5,6,7])-gete([1,2,3,4,6,5,7])-gete([1,2,3,6,4,5,7]) &
              -gete([1,2,6,3,4,5,7])-gete([1,6,2,3,4,5,7])-gete([6,1,2,3,4,5,7])
          if (permut%exit(pp(1:Nperm))) exit
          enddo
        end select
      endif
    if (grepen%exit(ii(1:Nleaf))) exit
    enddo
  enddo

  contains

    function vertex3( i1,i2 ) result(rslt)
    integer,intent(in) :: i1(:),i2(:)
    !(complex2!),dimension(0:3) :: rslt,e1,e2,q1,q2,p1,p2
    !(complex2!) :: cc(3)
    e1(:) = current(:,find(i1))
    e2(:) = current(:,find(i2))
    q1(:)= momentum(:,sum(base(i1)))
    q2(:)= momentum(:,sum(base(i2)))
    p2 = 2*q2+q1
    p1 = 2*q1+q2
    cc(3) = e1(0)*e2(0) - e1(1)*e2(1) - e1(2)*e2(2) - e1(3)*e2(3)
    cc(2) = e1(0)*p2(0) - e1(1)*p2(1) - e1(2)*p2(2) - e1(3)*p2(3)
    cc(1) = e2(0)*p1(0) - e2(1)*p1(1) - e2(2)*p1(2) - e2(3)*p1(3)
    cc = cc*coup3
    rslt = (q1-q2)*cc(3) + e2*cc(2) - e1*cc(1)
    end function

    function vertex4( i1,i2,i3 ) result(rslt)
    integer,intent(in) :: i1(:),i2(:),i3(:)
    !(complex2!),dimension(0:3) :: rslt,e1,e2,e3
    !(complex2!) :: cc(3)
    e1(:) = current(:,find(i1))
    e2(:) = current(:,find(i2))
    e3(:) = current(:,find(i3))
    cc(1) = e2(0)*e3(0) - e2(1)*e3(1) - e2(2)*e3(2) - e2(3)*e3(3)
    cc(2) = e3(0)*e1(0) - e3(1)*e1(1) - e3(2)*e1(2) - e3(3)*e1(3)
    cc(3) = e1(0)*e2(0) - e1(1)*e2(1) - e1(2)*e2(2) - e1(3)*e2(3)
    cc(1) =-cc(1)*coup4
    cc(2) = cc(2)*coup4*2
    cc(3) =-cc(3)*coup4
    rslt = e1*cc(1) + e2*cc(2) + e3*cc(3)
    end function

    function gete(per) result(ee)
    integer,intent(in) :: per(:)
    !(complex2!) :: ee(0:3)
    ee(:) = current(:,find(ii(pp(per))))
    end function

  end subroutine


  subroutine fill_colormatrix( matrix ,pattern ,Ngluon ,singleCor ,doubleCor )
  character(*),intent(in) :: pattern
  integer,intent(in) :: Ngluon
  integer,intent(in),optional :: singleCor(:),doubleCor(:)
  integer,allocatable,intent(out) :: matrix(:,:,:)
  integer :: jj,Nsize
  logical :: qqType
  qqType = (pattern.eq.'qq')
  Nsize = igamm(Ngluon-1)
  if (qqType) Nsize = igamm(Ngluon+1)
  if (present(singleCor)) then
    if (present(doubleCor)) then
      if (errru.ge.0) write(errru,*) 'ERROR in fill_colormatrix: '&
        ,'singelCor and doubleCor cannot be arguments simulateously.'
      stop
    endif
    allocate(matrix(Nsize,Nsize,0:size(singleCor,1)/2))
    if (qqType) then
      call fill_qq_square( Ngluon ,matrix(:,:,0) )
    else
      call fill_ng_square( Ngluon ,matrix(:,:,0) )
    endif
    do jj=1,size(singleCor,1)/2
      if (qqType) then
        call fill_qq_single( Ngluon ,singleCor(2*jj-1:2*jj) ,matrix(:,:,jj) )
      else
        call fill_ng_single( Ngluon ,singleCor(2*jj-1:2*jj) ,matrix(:,:,jj) )
      endif
    enddo
  elseif (present(doubleCor)) then
    if (size(doubleCor,1).ge.4) then
      allocate(matrix(Nsize,Nsize,size(doubleCor,1)/4))
      do jj=1,size(doubleCor,1)/4
        if (qqType) then
          call fill_qq_double( Ngluon ,doubleCor(4*jj-3:4*jj) ,matrix(:,:,jj) )
        else
          call fill_ng_double( Ngluon ,doubleCor(4*jj-3:4*jj) ,matrix(:,:,jj) )
        endif
      enddo
    endif
  else
    allocate(matrix(Nsize,Nsize,0:0))
    if (qqType) then
      call fill_qq_square( Ngluon ,matrix(:,:,0) )
    else
      call fill_ng_square( Ngluon ,matrix(:,:,0) )
    endif
  endif
  end subroutine


end module


