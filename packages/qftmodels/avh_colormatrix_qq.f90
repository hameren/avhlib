module avh_colormatrix_qq
  use avh_doloops
  implicit none
  private
  public :: fill_qq_square ,fill_qq_single ,fill_qq_double
  public :: tstring_type, Ncolor ,eval_tstring_ng


  type :: tstring_type
! Repesents a string of T-matrices
!   T{1}(a1,j1,i1) T{2}(a2,j2,i2) T{3}(a3,j3,i3) ... T{n}(a{n},j{n},i{n})
! Index contractions are encoded via the definition of connected neighbours.
! So  T{3}(a,j,i) T{5}(a,l,k)  is established as
!   T%adj(3)=5 ;T%adj(5)=3
! And  T{3}(a,j,i) T{5}(b,i,k)  is established as
!   T%rght(3)=5 ;T%left(5)=3
! In general we have 
!   T%adj(T%adj(i))=i , T%rght(T%left(i))=i , T%left(T%rght(i))=i 
! A string in which all indices are connected is evaluated by applying Fierz:
!   T(a,j,i)*T(a,l,k) = delta(i,l)*delta(k,j) - delta(i,j)*delta(k,l)/Nc
! The array ptr keeps track of which adjoint indices have not been Fierzed yet.
    integer :: Ntot,Nptr
    integer,allocatable :: ptr(:),adj(:),left(:),rght(:)
    integer :: factor
  contains 
    procedure :: dalloc
    procedure :: alloc
    procedure :: init
    procedure :: copy
    procedure :: prnt
    procedure :: splitRight
    procedure :: switch
    procedure :: connectAdj
    procedure :: insertFun
    procedure :: insertAdj
  end type

  integer,parameter :: Ncolor(0:14)=[1,3,9,27,81,243,729,2187,6561,19683 &
                                    ,59049,177147,531441,1594323,4782969]

  abstract interface
    subroutine dc_interface
    end subroutine
  end interface

contains

  subroutine dalloc( obj )
    class(tstring_type) :: obj
    if (allocated(obj%ptr)) deallocate(obj%ptr)
    if (allocated(obj%adj)) deallocate(obj%adj)
    if (allocated(obj%left)) deallocate(obj%left)
    if (allocated(obj%rght)) deallocate(obj%rght)
    obj%Ntot = 0
    obj%Nptr = 0
    obj%factor = 0
  end subroutine

  subroutine alloc( obj ,nn )
    class(tstring_type) :: obj
    integer,intent(in) :: nn
    call obj%dalloc
    allocate(obj%ptr(nn),obj%adj(nn),obj%left(nn),obj%rght(nn))
    obj%Ntot = nn
  end subroutine

  subroutine copy( obj ,xx )
    class(tstring_type),intent(in)  :: obj
    class(tstring_type),intent(out) :: xx
    call xx%alloc( obj%Ntot )
    xx%ptr  = obj%ptr  
    xx%adj  = obj%adj 
    xx%left = obj%left
    xx%rght = obj%rght
    xx%Nptr = obj%Nptr
    xx%factor = obj%factor
  end subroutine

  subroutine init( obj ,factor )
    class(tstring_type) :: obj
    integer,intent(in) :: factor
    integer :: ii
    obj%Nptr = obj%Ntot
    do ii=1,obj%Nptr
      obj%ptr(ii) = ii
    enddo
    obj%factor = factor
!   connect fundamental indices to a single trace 
!   T{1}(.,i{n},i1) T{2}(.,i1,i2) T{3}(.,i2,i3) ... T{n}(.,i{n-1},i{n})
    obj%left(1) = obj%Ntot
    do ii=2,obj%Ntot
      obj%rght(ii-1) = ii
      obj%left(ii  ) = ii-1
    enddo
    obj%rght(obj%Ntot) = 1
  end subroutine

  subroutine splitRight( obj ,ii )
! Replace
!   Tr(T{whatever}T{whatever+1}...T{ii}...T{Ntot})
! with
!   Tr(T{whatever}T{whatever+1}...T{ii}) Tr({ii+1}...T{Ntot})
    class(tstring_type) :: obj
    integer,intent(in) :: ii
    integer :: ri,rn
    ri = obj%rght(ii)
    rn = obj%rght(obj%Ntot)
    obj%rght(ii)=rn       ;obj%left(rn)=ii
    obj%rght(obj%Ntot)=ri ;obj%left(ri)=obj%Ntot
  end subroutine

  subroutine connectAdj( obj ,n1 ,n2 )
! Obvious
    class(tstring_type) :: obj
    integer,intent(in) :: n1,n2
    obj%adj(n1)=n2 ;obj%adj(n2)=n1
  end subroutine

  subroutine insertFun( obj ,n1 ,n2 )
! Replace  T%left(1)
!   ... T{n1}(.,.,j) T{r1}(.,j,.) ... T{l2}(.,.,i) T{n2}(.,i,.) ...
! with
!   ... T{n1}(.,.,j) T{r1}(.,i,.) ... T{l2}(.,.,i) T{n2}(.,j,.) ...
    class(tstring_type) :: obj
    integer,intent(in) :: n1,n2
    integer :: r1,l2
    r1 = obj%rght(n1)
    l2 = obj%left(n2)
    obj%rght(n1)=n2 ;obj%left(n2)=n1
    obj%left(r1)=l2 ;obj%rght(l2)=r1
  end subroutine
  
  subroutine insertAdj( obj ,n1 ,n2 )
! Replace
!   ... T{n1}(b,.,.) ... T{a1}(b,.,.) ... T{n2}(c,.,.) ... T{a2}(c,.,.)
! with
!   ... T{n1}(b,.,.) ... T{a1}(c,.,.) ... T{n2}(b,.,.) ... T{a2}(c,.,.)
    class(tstring_type) :: obj
    integer,intent(in) :: n1,n2
    integer :: a1,a2
    a1 = obj%adj(n1)
    a2 = obj%adj(n2)
    obj%adj(n1)=n2 ;obj%adj(n2)=n1
    obj%adj(a1)=a2 ;obj%adj(a2)=a1
  end subroutine


  subroutine switch( obj ,nn )
! Replace  T{nn}T{r1}T{r2}T{r3}  with  T{nn}T{r2}T{r1}T{r3}
! and put a minus sign
    class(tstring_type) :: obj
    integer,intent(in) :: nn
    integer :: r1,r2,r3
    r1 = obj%rght(nn)
    r2 = obj%rght(r1)
    r3 = obj%rght(r2)
    obj%left(r2)=nn ;obj%rght(nn)=r2
    obj%left(r1)=r2 ;obj%rght(r2)=r1
    obj%left(r3)=r1 ;obj%rght(r1)=r3
    obj%factor =-obj%factor
  end subroutine


  subroutine insertOne( obj ,Ntrm ,Nlast ,freeAdj ,ii ,Ngluon )
! Insert correlator, to be connected with another one via freeAdj
    type(tstring_type),intent(inout) :: obj(:)
    integer,intent(inout) :: Ntrm ,Nlast
    integer,intent(out) :: freeAdj
    integer,intent(in) :: ii,Ngluon
    integer :: hh
    if (ii.le.Ngluon) then ! gluon
      do hh=1,Ntrm
        associate( o=>obj(hh) ,t=>obj(Ntrm+hh) )
        call o%splitRight( Nlast+3 )
        call o%connectAdj( Nlast+1 ,Nlast+3 )
        call o%insertAdj( ii ,Nlast+1 )
        call o%copy(t)
        call t%switch( Nlast+1 )
        end associate
      enddo
      freeAdj = Nlast+2
      Ntrm = Ntrm*2
      Nlast = Nlast+3
    elseif (ii.eq.Ngluon+1) then ! anti-quark
      do hh=1,Ntrm
        call obj(hh)%splitRight( Nlast+1 )
        call obj(hh)%insertFun( Ngluon ,Nlast+1 )
      enddo
      freeAdj = Nlast+1
      Nlast = Nlast+1
    else ! quark
      do hh=1,Ntrm
        call obj(hh)%splitRight( Nlast+1 )
        call obj(hh)%insertFun( obj(hh)%left(1) ,Nlast+1 )
        obj(hh)%factor =-obj(hh)%factor
      enddo
      freeAdj = Nlast+1
      Nlast = Nlast+1
    endif
  end subroutine


  subroutine insertTwo( obj ,Ntrm ,Nlast ,freeAdj ,ii ,Ngluon )
! Insert two correlators for one parton, to be connected with two
! other ones via freeAdj(1:2)
    type(tstring_type),intent(inout) :: obj(:)
    integer,intent(inout) :: Ntrm ,Nlast
    integer,intent(out) :: freeAdj(2)
    integer,intent(in) :: ii,Ngluon
    integer :: hh
    if (ii.le.Ngluon) then ! gluon
      do hh=1,Ntrm
        associate( o0=>obj(hh),o1=>obj(Ntrm+hh),o2=>obj(2*Ntrm+hh),o3=>obj(3*Ntrm+hh) )
        call o0%splitRight( Nlast+3 )
        call o0%splitRight( Nlast+6 )
        call o0%connectAdj( Nlast+1 ,Nlast+6 )
        call o0%connectAdj( Nlast+3 ,Nlast+4 )
        call o0%insertAdj( ii ,Nlast+1 )
        call o0%copy(o1)
        call o0%copy(o2)
        call o0%copy(o3)
        call o1%switch( Nlast+1 )
        call o2%switch( Nlast+4 )
        call o3%switch( Nlast+1 )
        call o3%switch( Nlast+4 )
        end associate
      enddo
      freeAdj(1:2) = [Nlast+2,Nlast+5]
      Ntrm = Ntrm*4
      Nlast = Nlast+6
    elseif (ii.eq.Ngluon+1) then ! anti-quark
      do hh=1,Ntrm
        call obj(hh)%splitRight( Nlast+2 )
        call obj(hh)%insertFun( Ngluon ,Nlast+1 )
      enddo
      freeAdj(1:2) = [Nlast+1,Nlast+2]
      Nlast = Nlast+2
    else ! quark
      do hh=1,Ntrm
        call obj(hh)%splitRight( Nlast+2 )
        call obj(hh)%insertFun( obj(hh)%left(1) ,Nlast+2 )
      enddo
      freeAdj(1:2) = [Nlast+1,Nlast+2]
      Nlast = Nlast+2
    endif
  end subroutine


  subroutine fill_qq_square( Ngluon ,matrix )
    integer,intent(in) :: Ngluon
    integer,intent(out) :: matrix(igamm(Ngluon+1),igamm(Ngluon+1))
    type(permut_type) :: permut,qermut
    integer :: pCnt,qCnt,hh,pp(13),qq(13),invq(13)
    type(tstring_type) :: T
!  
    call T%alloc(Ngluon*2)
!  
    pCnt=0 ;call permut%init(Ngluon,pp) ;do ;pCnt=pCnt+1
    qCnt=0 ;call qermut%init(Ngluon,qq) ;do ;qCnt=qCnt+1
      do hh=1,Ngluon
        invq(qq(hh)) = hh
      enddo
      call T%init( Ncolor(Ngluon-1) )
      do hh=1,Ngluon
        call T%connectAdj( hh ,Ngluon+(Ngluon-invq(pp(hh))+1) )
      enddo
      matrix(pCnt,qCnt) = 0
      call eval_tstring( T ,matrix(pCnt,qCnt) )
    if (qermut%exit(qq)) exit ;enddo
    if (permut%exit(pp)) exit ;enddo
!  
    call T%dalloc
  end subroutine


  subroutine fill_qq_single( Ngluon ,corr ,matrix )
! Single color correlator. Anti-quark=Ngluon+1, quark=Ngluon+2
! For gluons, it applies f(a,b,c) = Tr(Ta*Tb*Tc-Ta*Tc*Tb) etc.
    integer,intent(in) :: Ngluon,corr(2)
    integer,intent(out) :: matrix(igamm(Ngluon+1),igamm(Ngluon+1))
    type(permut_type) :: permut,qermut
    integer :: pp(13),qq(13),invp(13),invq(13)
    integer :: pCnt,qCnt,twoN,hh,Nsiz,Ntrm,Nlast,free1,free2,term
    type(tstring_type) :: T(4)
!
    twoN = Ngluon*2
    Nsiz = twoN+2
    Ntrm = 1
    do hh=1,2
      if (corr(hh).le.Ngluon) then
        Nsiz = Nsiz+2
        Ntrm = Ntrm*2
      endif
    enddo
    invp(Ngluon+1:Ngluon+2) = [Ngluon+1,Ngluon+2]
!  
    do hh=1,Ntrm
      call T(hh)%alloc(Nsiz)
    enddo
!  
    pCnt=0 ;call permut%init(Ngluon,pp) ;do ;pCnt=pCnt+1
    qCnt=0 ;call qermut%init(Ngluon,qq) ;do ;qCnt=qCnt+1
      do hh=1,Ngluon
        invp(pp(hh)) = hh
        invq(qq(hh)) = hh
      enddo
      call T(1)%init( Ncolor(Ngluon) )
      call T(1)%splitRight( twoN )
      do hh=1,Ngluon
        call T(1)%connectAdj( invp(hh) ,Ngluon+(Ngluon-invq(hh)+1) )
      enddo
      Ntrm = 1
      Nlast = twoN
      call insertOne( T ,Ntrm ,Nlast ,free1 ,invp(corr(1)) ,Ngluon )
      call insertOne( T ,Ntrm ,Nlast ,free2 ,invp(corr(2)) ,Ngluon )
      matrix(pCnt,qCnt) = 0
      do hh=1,Ntrm
        call T(hh)%connectAdj( free1 ,free2 )
        term=0 ;call eval_tstring( T(hh) ,term )
        matrix(pCnt,qCnt) = matrix(pCnt,qCnt) + term
      enddo
    if (qermut%exit(qq)) exit ;enddo
    if (permut%exit(pp)) exit ;enddo
!  
    do hh=1,Ntrm
      call T(hh)%dalloc
    enddo
  end subroutine


  subroutine fill_qq_double( Ngluon ,corr ,matrix )
! Double color correlator. Anti-quark=Ngluon+1, quark=Ngluon+2
    integer,intent(in) :: Ngluon,corr(4)
    integer,intent(out) :: matrix(igamm(Ngluon+1),igamm(Ngluon+1))
    integer :: ii,jj,kk,ll,twoN,hh,Nsiz,Ntrm,Nlast,free1(2),free2(2),term
    type(permut_type) :: permut,qermut
    integer :: pCnt,qCnt,pp(13),qq(13),invp(13),invq(13)
    type(tstring_type) :: T(16)
!  
    twoN = Ngluon*2
    Nsiz = twoN+4
    Ntrm = 1
    do hh=1,4
      if (corr(hh).le.Ngluon) then
        Nsiz = Nsiz+2
        Ntrm = Ntrm*2
      endif
    enddo
    invp(Ngluon+1:Ngluon+2) = [Ngluon+1,Ngluon+2]
!  
    do hh=1,Ntrm
      call T(hh)%alloc(Nsiz)
    enddo
!  
    !ii=corr(1) ;jj=corr(2) ;kk=corr(3) ;ll=corr(4)
    ii=corr(3) ;jj=corr(4) ;kk=corr(1) ;ll=corr(2)
!  
    if (ii.eq.kk) then
      if (jj.eq.ll) then
        call eval_dc(f1212)
      else
        call eval_dc(f1213)
      endif
    elseif (ii.eq.ll) then
      hh=kk;kk=ll;ll=hh
      if (jj.eq.ll) then
        call eval_dc(f1212)
      else
        call eval_dc(f1213)
      endif
    elseif (jj.eq.kk) then
      hh=ii;ii=jj;jj=hh
      call eval_dc(f1213)
    elseif (jj.eq.ll) then
      hh=ii;ii=jj;jj=hh
      hh=kk;kk=ll;ll=hh
      call eval_dc(f1213)
    else
      call eval_dc(f1234)
    endif
!  
    do hh=1,Ntrm
      call T(hh)%dalloc
    enddo
!
  contains
!
    subroutine eval_dc(func)
      procedure(dc_interface) :: func
      pCnt=0 ;call permut%init(Ngluon,pp) ;do ;pCnt=pCnt+1
      qCnt=0 ;call qermut%init(Ngluon,qq) ;do ;qCnt=qCnt+1
        do hh=1,Ngluon
          invp(pp(hh)) = hh
          invq(qq(hh)) = hh
        enddo
        call T(1)%init( Ncolor(Ngluon+1) )
        call T(1)%splitRight( twoN )
        do hh=1,Ngluon
          call T(1)%connectAdj( invp(hh) ,Ngluon+(Ngluon-invq(hh)+1) )
        enddo
        Ntrm = 1
        Nlast = twoN
        call func
        matrix(pCnt,qCnt) = 0
        do hh=1,Ntrm
          call T(hh)%connectAdj( free1(1),free2(1) )
          call T(hh)%connectAdj( free1(2),free2(2) )
          term=0 ;call eval_tstring( T(hh) ,term )
          matrix(pCnt,qCnt) = matrix(pCnt,qCnt) + term
        enddo
      if (qermut%exit(qq)) exit ;enddo
      if (permut%exit(pp)) exit ;enddo
    end subroutine
!
    subroutine f1234
      call insertOne( T ,Ntrm ,Nlast ,free1(1) ,invp(ii) ,Ngluon )
      call insertOne( T ,Ntrm ,Nlast ,free2(1) ,invp(jj) ,Ngluon )
      call insertOne( T ,Ntrm ,Nlast ,free1(2) ,invp(kk) ,Ngluon )
      call insertOne( T ,Ntrm ,Nlast ,free2(2) ,invp(ll) ,Ngluon )
    end subroutine
!
    subroutine f1213
      call insertTwo( T ,Ntrm ,Nlast ,free1    ,invp(ii) ,Ngluon )
      call insertOne( T ,Ntrm ,Nlast ,free2(1) ,invp(jj) ,Ngluon )
      call insertOne( T ,Ntrm ,Nlast ,free2(2) ,invp(ll) ,Ngluon )
    end subroutine
!
    subroutine f1212
      call insertTwo( T ,Ntrm ,Nlast ,free1 ,invp(ii) ,Ngluon )
      call insertTwo( T ,Ntrm ,Nlast ,free2 ,invp(jj) ,Ngluon )
    end subroutine
!
  end subroutine


  recursive subroutine eval_tstring( Tin ,rslt )
    class(tstring_type),intent(in) :: Tin
    integer,intent(inout) :: rslt
    type(tstring_type) :: Ta,Tb
 
    call fierz_tstring( Tin ,Ta ,Tb )
    if (Ta%factor.ne.0) then
      if (Ta%Nptr.ge.2) then
        call eval_tstring( Ta ,rslt )
      else
        rslt = rslt + Ta%factor
      endif
    endif
    if (Tb%factor.ne.0) then
      if (Tb%Nptr.ge.2) then
        call eval_tstring( Tb ,rslt )
      else
        rslt = rslt + Tb%factor
      endif
    endif
  end subroutine


  subroutine fierz_tstring( Tin ,Ta ,Tb)
! Apply Fierz on the last entry of ptr and its adjointly connected partner.
    class(tstring_type),intent(in ) :: Tin
    class(tstring_type),intent(out) :: Ta,Tb
    integer :: ii,l1,n1,r1,l2,n2,r2
    n2 = Tin%ptr(Tin%Nptr)
    n1 = Tin%adj(n2)
    ii = 0
    do ;ii=ii+1 !;if (ii.gt.Tin%Nptr) exit
      if (Tin%ptr(ii).eq.n1) exit
    enddo
    l1 = Tin%left(n1)
    r1 = Tin%rght(n1)
    l2 = Tin%left(n2)
    r2 = Tin%rght(n2)
    if (r1.eq.n1.or.r2.eq.n2) then
      Ta%factor = 0
      Tb%factor = 0
    elseif (r1.eq.n2.and.r2.eq.n1) then
      call Tin%copy( Ta )
      Ta%factor = Ta%factor*(Ncolor(2)-1)
      Ta%ptr(ii:Ta%Nptr-2)=Ta%ptr(ii+1:Ta%Nptr-1) ;Ta%Nptr=Ta%Nptr-2
      Tb%factor = 0
    elseif (r1.eq.n2) then
      call Tin%copy( Ta )
      Ta%rght(l1)=r2 ;Ta%left(r2)=l1
      Ta%factor = Ta%factor/Ncolor(1)*(Ncolor(2)-1)
      Ta%ptr(ii:Ta%Nptr-2)=Ta%ptr(ii+1:Ta%Nptr-1) ;Ta%Nptr=Ta%Nptr-2
      Tb%factor = 0
    elseif (r2.eq.n1) then
      call Tin%copy( Ta )
      Ta%rght(l2)=r1 ;Ta%left(r1)=l2
      Ta%factor = Ta%factor/Ncolor(1)*(Ncolor(2)-1)
      Ta%ptr(ii:Ta%Nptr-2)=Ta%ptr(ii+1:Ta%Nptr-1) ;Ta%Nptr=Ta%Nptr-2
      Tb%factor = 0
    else
      call Tin%copy( Ta )
      Ta%rght(l1)=r2 ;Ta%left(r2)=l1
      Ta%rght(l2)=r1 ;Ta%left(r1)=l2
      Ta%ptr(ii:Ta%Nptr-2)=Ta%ptr(ii+1:Ta%Nptr-1) ;Ta%Nptr=Ta%Nptr-2
      call Tin%copy( Tb )
      Tb%rght(l1)=r1 ;Tb%left(r1)=l1
      Tb%rght(l2)=r2 ;Tb%left(r2)=l2
      Tb%factor =-Tb%factor/Ncolor(1)
      Tb%ptr(ii:Tb%Nptr-2)=Tb%ptr(ii+1:Tb%Nptr-1) ;Tb%Nptr=Tb%Nptr-2
    endif
  end subroutine


  subroutine eval_tstring_ng( T ,rslt )
! Evaluate tstring by only taking into account the contribution
!  delta(i,l)*delta(k,j)  and omitting  delta(i,j)*delta(k,l)/N
! in the Fierz identity.
    class(tstring_type),intent(inout) :: T
    integer,intent(out) :: rslt
    integer :: ii,l1,n1,r1,l2,n2,r2
    do while (T%Nptr.ge.2)
      n2 = T%ptr(T%Nptr)
      n1 = T%adj(n2)
      ii = 0
      do ;ii=ii+1 !;if (ii.gt.T%Nptr) exit
        if (T%ptr(ii).eq.n1) exit
      enddo
      l1 = T%left(n1)
      r1 = T%rght(n1)
      l2 = T%left(n2)
      r2 = T%rght(n2)
      if (r1.eq.n1.and.r2.eq.n2) then
        T%factor = T%factor*Ncolor(1)
      elseif (r1.eq.n1) then
        T%rght(l2)=r2 ;T%left(r2)=l2
      elseif (r2.eq.n2) then
        T%rght(l1)=r1 ;T%left(r1)=l1
      elseif (r1.eq.n2.and.r2.eq.n1) then
        T%factor = T%factor*Ncolor(2)
      elseif (r1.eq.n2) then
        T%rght(l1)=r2 ;T%left(r2)=l1
        T%factor = T%factor*Ncolor(1)
      elseif (r2.eq.n1) then
        T%rght(l2)=r1 ;T%left(r1)=l2
        T%factor = T%factor*Ncolor(1)
      else
        T%rght(l1)=r2 ;T%left(r2)=l1
        T%rght(l2)=r1 ;T%left(r1)=l2
      endif
      T%ptr(ii:T%Nptr-2) = T%ptr(ii+1:T%Nptr-1)
      T%Nptr = T%Nptr-2
    enddo
    rslt = T%factor
  end subroutine


  subroutine prnt( obj ,label ,writeUnit )
    class(tstring_type) :: obj
    character(*),intent(in) :: label
    integer,optional,intent(in) :: writeUnit
    integer :: wUnit,ii
    logical :: used(99)
    character(256) :: line,info
    wUnit = 6
    if (present(writeUnit)) wUnit = writeUnit
    if (wUnit.lt.0) return
    used(1:obj%Ntot) = .false.
    do ii=1,obj%Nptr
      used(obj%ptr(ii)) = .true.
    enddo
    line = ''
    do ii=1,obj%Ntot
      if (used(ii)) then
        if (ii.le.9) then
          write(info,'(i1)') ii
          line = trim(line)//'--'//trim(info)
        else
          write(info,'(i2)') ii
          line = trim(line)//'-'//trim(info)
        endif
      else
        line = trim(line)//'---'
      endif
    enddo
    write(info,*) obj%factor
    info = label//': factor='//trim(info)
    write(wUnit,'(A)') trim(info)
    write(wUnit,'(A)') trim(line)
    write(wUnit,'(99i3)') obj%adj(1:obj%Ntot)
    write(wUnit,'(99i3)') obj%left(1:obj%Ntot)
    write(wUnit,'(99i3)') obj%rght(1:obj%Ntot)
    write(wUnit,*)
  end subroutine


end module


