module avh_lagrangian
  !(usekindmod!)
  use avh_mathcnst
  use avh_othercnst
  use avh_iounits
  use avh_doloops ,only: perm_a_few
  use avh_prnt
  use avh_lorentz
  use avh_fermionlines
  use avh_fusiontools
  use avh_fill_colorflow ,only: tab_3g,tab_4g,tab_s3g,tab_s4g
  use avh_colorflow
  use avh_coloredlgn
  implicit none
  private

  integer,parameter,public :: eleNeu=1 , eleon=2 ,uQuark=3 ,dQuark=4
  integer,parameter,public ::  muNeu=5 ,  muon=6 ,cQuark=7 ,sQuark=8
  integer,parameter,public :: tauNeu=9 , tauon=10,tQuark=11,bQuark=12
  integer,parameter,public :: eikQ(14)=[13,14,15,16,17,18,19,20,21,22,23,24,25,26]
  integer,parameter,public :: Wboson=27,photon=28,Zboson=29,gluon=30,Higgs=31
  integer,parameter,public :: eikPh(1:2)=[32,33]
  integer,parameter,public :: gStar(1:2)=eikQ(1:2)
  integer,parameter,public :: uStar(1:2)=eikQ( 3:4 )
  integer,parameter,public :: dStar(1:2)=eikQ( 5:6 )
  integer,parameter,public :: cStar(1:2)=eikQ( 7:8 )
  integer,parameter,public :: sStar(1:2)=eikQ( 9:10)
  integer,parameter,public :: tStar(1:2)=eikQ(11:12)
  integer,parameter,public :: bStar(1:2)=eikQ(13:14)
  integer,parameter,public :: nulParticle=-28
  integer,parameter,public :: maxParticle= 33
  integer,parameter,public :: NeikFam=14,NeikDir=2

  type(fusionRules_type),save,public,protected :: lagrangianRules
  type(nonLocalRules_type),save,public,protected :: couplingPowerRules

  !(realknd2!),save,public,protected :: mass(1:maxParticle)
  !(realknd2!),save,public,protected :: width(1:maxParticle)
  !(complex2!),save,public,protected :: cmplxMass(1:maxParticle)
  logical     ,save,public,protected :: withQCD,withQED,withWeak,withHiggs,withCKM
  logical     ,save,public,protected :: withHG,withHA
  !(realknd2!),save,public,protected :: gQCD,gQED,gHG,gHA
  !(complex2!),save,public,protected :: gWeak,cosWein,sinWein

  integer,parameter,public :: helVal(3)=[-1,1,0]

!  logical,save,public,protected :: is_gluon(nulParticle:maxParticle)=.false.
  logical,save,public,protected :: is_quark(nulParticle:maxParticle)=.false.
  logical,save,public,protected :: is_antiq(nulParticle:maxParticle)=.false.
  logical,save,public,protected :: is_offsh(nulParticle:maxParticle)=.false.
  integer,save,public,protected :: NspinDof(nulParticle:maxParticle)=0
  integer,save,public,protected :: iEikVec( nulParticle:maxParticle)=0
  public :: get_anti,get_onshell,get_offshell
  public :: get_NspinDof
  public :: prnt_particle
  public :: get_symFac,fill_lagrangian,set_gauge
  public :: xCouplingPower
  public :: std_order

  public :: set_mass_and_width
  public :: set_gQCD ,set_gQED ,set_gHG ,set_gHA ,set_Gfermi
  public :: set_withQCD ,set_withQED ,set_withWeak
  public :: set_withHiggs ,set_withCKM ,set_withHG ,set_withHA
  public :: set_complex_mass_scheme
  public :: set_real_direction
  public :: set_complex_direction

  logical,save :: initz=.true.
  integer,save :: scheme=0
  type(lorentz_type),save :: eikVec(NeikDir) ,gaugeVec
  !(realknd2!),save :: gaugePar

  interface prnt_particle
    module procedure prnt_particle,prnt_particle_2
    module procedure prnt_particle_3,prnt_particle_4
  end interface

  interface get_offshell
    module procedure get_offshell1,get_offshell2
  end interface

contains


  subroutine init_lagrangian
!***********************************************************************
!***********************************************************************
  integer :: colorRepr(nulParticle:maxParticle)
  integer :: ii,jj
  !(realknd2!) :: keV
  initz = .false.
  call init_mathcnst
!
!  is_gluon( gluon)=.true.
  is_quark(uQuark)=.true. ;is_antiq(-uQuark)=.true.
  is_quark(dQuark)=.true. ;is_antiq(-dQuark)=.true.
  is_quark(cQuark)=.true. ;is_antiq(-cQuark)=.true.
  is_quark(sQuark)=.true. ;is_antiq(-sQuark)=.true.
  is_quark(tQuark)=.true. ;is_antiq(-tQuark)=.true.
  is_quark(bQuark)=.true. ;is_antiq(-bQuark)=.true.
  do ii=1,NeikFam
    is_quark(eikQ(ii))=.true. ;is_antiq(-eikQ(ii))=.true.
  enddo
!
  is_offsh = .false.
  do ii=1,NeikFam
    is_offsh( eikQ(ii)) = .true.
    is_offsh(-eikQ(ii)) = .true.
  enddo
  is_offsh(eikPh(1:NeikDir)) = .true.
!
  NspinDof(eleNeu)=2 ;NspinDof(-eleNeu)=2
  NspinDof( eleon)=2 ;NspinDof(- eleon)=2
  NspinDof(uQuark)=2 ;NspinDof(-uQuark)=2
  NspinDof(dQuark)=2 ;NspinDof(-dQuark)=2
  NspinDof( muNeu)=2 ;NspinDof(- muNeu)=2
  NspinDof(  muon)=2 ;NspinDof(-  muon)=2
  NspinDof(cQuark)=2 ;NspinDof(-cQuark)=2
  NspinDof(sQuark)=2 ;NspinDof(-sQuark)=2
  NspinDof(tauNeu)=2 ;NspinDof(-tauNeu)=2
  NspinDof( tauon)=2 ;NspinDof(- tauon)=2
  NspinDof(tQuark)=2 ;NspinDof(-tQuark)=2
  NspinDof(bQuark)=2 ;NspinDof(-bQuark)=2
  NspinDof(Wboson)=3 ;NspinDof(-Wboson)=3
  NspinDof(photon)=2
  NspinDof(Zboson)=3
  NspinDof( gluon)=2
  NspinDof( Higgs)=1
  do ii=1,NeikFam
    NspinDof( eikQ(ii))=2
    NspinDof(-eikQ(ii))=2
  enddo
  NspinDof(eikPh(1:NeikDir))=2
!
  colorRepr = 0
  colorRepr(eleNeu)=singlet ;colorRepr(-eleNeu)=singlet
  colorRepr( eleon)=singlet ;colorRepr( -eleon)=singlet
  colorRepr(uQuark)=fundame ;colorRepr(-uQuark)=antifun 
  colorRepr(dQuark)=fundame ;colorRepr(-dQuark)=antifun
  colorRepr( muNeu)=singlet ;colorRepr( -muNeu)=singlet
  colorRepr(  muon)=singlet ;colorRepr(  -muon)=singlet
  colorRepr(cQuark)=fundame ;colorRepr(-cQuark)=antifun
  colorRepr(sQuark)=fundame ;colorRepr(-sQuark)=antifun
  colorRepr(tauNeu)=singlet ;colorRepr(-tauNeu)=singlet
  colorRepr( tauon)=singlet ;colorRepr( -tauon)=singlet
  colorRepr(tQuark)=fundame ;colorRepr(-tQuark)=antifun
  colorRepr(bQuark)=fundame ;colorRepr(-bQuark)=antifun
  colorRepr(Wboson)=singlet ;colorRepr(-Wboson)=singlet
  colorRepr(photon)=singlet ;colorRepr( Zboson)=singlet
  colorRepr( gluon)=adjoint ;colorRepr(  Higgs)=singlet
  do ii=1,NeikFam
    colorRepr( eikQ(ii))=fundame
    colorRepr(-eikQ(ii))=antifun
  enddo
  colorRepr(eikPh(1:NeikDir))=singlet
  call fill_colorRepr( nulParticle,maxParticle,colorRepr )
!
  do jj=1,NeikFam/NeikDir
    do ii=1,NeikDir
      iEikVec( eikQ(NeikDir*(jj-1)+ii)) = ii
      iEikVec(-eikQ(NeikDir*(jj-1)+ii)) = ii
    enddo
  enddo
  do ii=1,NeikDir
    iEikVec(eikPh(ii)) = ii
  enddo
!
  mass = 0
  width = 0
  keV = rONE/1000000
  mass(Zboson)= 91187600*keV ;width(Zboson)=2495200*keV
  mass(Wboson)= 80385000*keV ;width(Wboson)=2085000*keV
  mass(Higgs )=125000000*keV ;width(Higgs )=   4210*keV
  mass(tQuark)=173340000*keV ;width(tQuark)=      0*keV
  eikVec(1)=momDef(rONE,rZRO,rZRO, rONE)
  eikVec(2)=momDef(rONE,rZRO,rZRO,-rONE)
  gQCD=sqrt(4*r1PI)
  gQED=sqrt(4*r1PI)
  gHG =sqrt(4*r1PI)
  gHA =sqrt(4*r1PI)
  withQCD  =.true.
  withQED  =.true.
  withWeak =.true.
  withHiggs=.false.
  withHG   =.false.
  withHA   =.false.
  withCKM  =.false.
!
  end subroutine


  subroutine set_mass_and_width( label ,rMass ,rWidth )
  integer,intent(in) :: label
  !(realknd2!),intent(in) :: rMass,rWidth
  if (initz) call init_lagrangian
  mass(label) = rMass
  width(label) = rWidth
  end subroutine

  subroutine set_complex_direction( direction ,eikVin ,vecPerm )
  integer,intent(in) :: direction
  !(complex2!),intent(in) :: eikVin(0:3)
  integer,optional,intent(in) :: vecPerm(3)
  if (initz) call init_lagrangian
  if (present(vecPerm)) then
    eikVec(direction) = momDef( eikVin ,vecPerm )
  else
    eikVec(direction) = momDef( eikVin(0),eikVin(1),eikVin(2),eikVin(3) )
  endif
  end subroutine

  subroutine set_real_direction( direction ,eikVin ,vecPerm )
  integer,intent(in) :: direction
  !(realknd2!),intent(in) :: eikVin(0:3)
  integer,optional,intent(in) :: vecPerm(3)
  if (initz) call init_lagrangian
  if (present(vecPerm)) then
    eikVec(direction) = momDef( eikVin ,vecPerm )
  else
    eikVec(direction) = momDef( eikVin(0),eikVin(1),eikVin(2),eikVin(3) )
  endif
  end subroutine

  subroutine set_gQCD( rValue )
  !(realknd2!),intent(in) :: rValue
  if (initz) call init_lagrangian
  gQCD = rValue
  end subroutine

  subroutine set_gQED( rValue )
  !(realknd2!),intent(in) :: rValue
  if (initz) call init_lagrangian
  gQED = rValue
  end subroutine

  subroutine set_gHG( rValue )
  !(realknd2!),intent(in) :: rValue
  if (initz) call init_lagrangian
  gHG = rValue
  end subroutine

  subroutine set_gHA( rValue )
  !(realknd2!),intent(in) :: rValue
  if (initz) call init_lagrangian
  gHA = rValue
  end subroutine

  subroutine set_complex_mass_scheme
  if (initz) call init_lagrangian
  scheme = 1
  end subroutine

  subroutine set_Gfermi( Gfermi )
  !(realknd2!),intent(in) :: Gfermi
  !(realknd2!) :: cW
  cW = mass(Wboson)/mass(Zboson)
  gQED = sqrt( 4*rSQRT2*Gfermi*(1-cW)*(1+cW) )*mass(Wboson)
  end subroutine

  subroutine set_withQCD( iValue )
  integer,intent(in) :: iValue
  if (initz) call init_lagrangian
  withQCD = (iValue.ne.0)
  end subroutine

  subroutine set_withQED( iValue )
  integer,intent(in) :: iValue
  if (initz) call init_lagrangian
  withQED = (iValue.ne.0)
  end subroutine

  subroutine set_withWeak( iValue )
  integer,intent(in) :: iValue
  if (initz) call init_lagrangian
  withWeak = (iValue.ne.0)
  end subroutine

  subroutine set_withHiggs( iValue )
  integer,intent(in) :: iValue
  if (initz) call init_lagrangian
  withHiggs = (iValue.ne.0)
  end subroutine

  subroutine set_withHG( iValue )
  integer,intent(in) :: iValue
  if (initz) call init_lagrangian
  withHG = (iValue.ne.0)
  end subroutine

  subroutine set_withHA( iValue )
  integer,intent(in) :: iValue
  if (initz) call init_lagrangian
  withHA = (iValue.ne.0)
  end subroutine

  subroutine set_withCKM( iValue )
  integer,intent(in) :: iValue
  if (initz) call init_lagrangian
  withCKM = (iValue.ne.0)
  end subroutine

  
  subroutine fill_lagrangian( offShFl )
!***********************************************************************
!***********************************************************************
  logical,intent(in):: offShFl
  type(interactionList_type) :: vertices 
  type(particleList_type) :: particles
  integer :: nul,ii,jj
  !(complex2!) :: h1l,h2l,h3l,h4l,h1r,h2r,h3r,h4r
  character(1),parameter :: dirS(2)=['A','B']
  character(2),parameter :: eikS(14)=&
    ['gA','gB','uA','uB','dA','dB','cA','cB','sA','sB','tA','tB','bA','bB']
  character(3),parameter :: antS(14)=&
    ['gA~','gB~','uA~','uB~','dA~','dB~','cA~','cB~','sA~','sB~','tA~','tB~','bA~','bB~']
!
  if (initz) call init_lagrangian
!
  select case (scheme)
  case default
    cosWein = mass(Wboson)/mass(Zboson)
    if ((real(cosWein).le.-rONE.or.real(cosWein).ge.rONE) &
        .and.aimag(cosWein).eq.rZRO) then
      cosWein = cosWein &
              - cIMA*(mass(Wboson)-mass(Zboson))/abs(mass(Zboson)) &
                    *epsilon(mass)
    endif
  case (1)
    cosWein = sqrt((mass(Wboson)*(mass(Wboson)-cIMA*width(Wboson))) &
                  /(mass(Zboson)*(mass(Zboson)-cIMA*width(Zboson))) )
  end select
!
  sinWein = sqrt((1-cosWein)*(1+cosWein))
  gWeak = gQED/sinWein
!
  do ii=1,maxParticle
    if (width(ii).eq.rZRO) then
      cmplxMass(ii) = mass(ii)
    else
      cmplxMass(ii) = sqrt(mass(ii)*(mass(ii)-cIMA*width(ii)))
    endif
  enddo
!
  call particles%add(  eleNeu ,'En' ,-eleNeu ,endTyp ,f_pgr ,spinor_xl )
  call particles%add( -eleNeu ,'EN' , eleNeu ,bgnTyp ,f_pgr ,spinor_xl )
  call particles%add(   eleon ,'E-' , -eleon ,endTyp ,f_pgr ,spinor_xl )
  call particles%add(  -eleon ,'E+' ,  eleon ,bgnTyp ,f_pgr ,spinor_xl )
  call particles%add(  uQuark ,'u ' ,-uQuark ,endTyp ,f_pgr ,spinor_xl )
  call particles%add( -uQuark ,'u~' , uQuark ,bgnTyp ,f_pgr ,spinor_xl ) 
  call particles%add(  dQuark ,'d ' ,-dQuark ,endTyp ,f_pgr ,spinor_xl )
  call particles%add( -dQuark ,'d~' , dQuark ,bgnTyp ,f_pgr ,spinor_xl )
  call particles%add(   muNeu ,'Mn' , -muNeu ,endTyp ,f_pgr ,spinor_xl )
  call particles%add(  -muNeu ,'MN' ,  muNeu ,bgnTyp ,f_pgr ,spinor_xl )
  call particles%add(    muon ,'M-' ,  -muon ,endTyp ,f_pgr ,spinor_xl )
  call particles%add(   -muon ,'M+' ,   muon ,bgnTyp ,f_pgr ,spinor_xl )
  call particles%add(  cQuark ,'c ' ,-cQuark ,endTyp ,f_pgr ,spinor_xl )
  call particles%add( -cQuark ,'c~' , cQuark ,bgnTyp ,f_pgr ,spinor_xl )
  call particles%add(  sQuark ,'s ' ,-sQuark ,endTyp ,f_pgr ,spinor_xl )
  call particles%add( -sQuark ,'s~' , sQuark ,bgnTyp ,f_pgr ,spinor_xl )
  call particles%add(  tauNeu ,'Tn' ,-tauNeu ,endTyp ,f_pgr ,spinor_xl )
  call particles%add( -tauNeu ,'TN' , tauNeu ,bgnTyp ,f_pgr ,spinor_xl )
  call particles%add(   tauon ,'T-' , -tauon ,endTyp ,f_pgr ,spinor_xl )
  call particles%add(  -tauon ,'T+' ,  tauon ,bgnTyp ,f_pgr ,spinor_xl )
  call particles%add(  tQuark ,'t ' ,-tQuark ,endTyp ,f_pgr ,spinor_xl )
  call particles%add( -tQuark ,'t~' , tQuark ,bgnTyp ,f_pgr ,spinor_xl )
  call particles%add(  bQuark ,'b ' ,-bQuark ,endTyp ,f_pgr ,spinor_xl )
  call particles%add( -bQuark ,'b~' , bQuark ,bgnTyp ,f_pgr ,spinor_xl )
  call particles%add(  Wboson ,'W+' ,-Wboson ,bosTyp ,v_pgr ,vector_xl )
  call particles%add( -Wboson ,'W-' , Wboson ,bosTyp ,v_pgr ,vector_xl )
  call particles%add(  photon ,'A ' , photon ,bosTyp ,g_pgr ,vector_xl )
  call particles%add(  Zboson ,'Z ' , Zboson ,bosTyp ,v_pgr ,vector_xl )
  call particles%add(   gluon ,'g ' ,  gluon ,bosTyp ,g_pgr ,vector_xl )
  call particles%add(   Higgs ,'H ' ,  Higgs ,bosTyp ,s_pgr ,scalar_xl )
  if (offShFl) then
    do ii=1,NeikFam
      call particles%add(  eikQ(ii) ,eikS(ii) ,-eikQ(ii) ,endTyp ,e_pgr ,eikQ_xl )
      call particles%add( -eikQ(ii) ,antS(ii) , eikQ(ii) ,bgnTyp ,e_pgr ,eikQ_xl )
    enddo
    do ii=1,NeikDir
      call particles%add( eikPh(ii) ,'ph'//dirS(ii) ,eikPh(ii) ,bosTyp ,v_pgr ,eikPh_xl)
    enddo
  endif
  call particles%set_shape( nulParticle+1,maxParticle )
!
  if (withQCD) then
  call vertices%add( [gluon,gluon,gluon,gluon] ,get_ref(cIMA*gQCD**2/2) ,gggg_vtx )
  call vertices%add( [ gluon, gluon ,gluon ] ,get_ref(cIMA*gQCD/rSQRT2) ,ggg_vtx )
  call vertices%add( [uQuark,-uQuark,gluon ] ,get_ref(cIMA*gQCD/rSQRT2) ,qqg_vtx )
  call vertices%add( [dQuark,-dQuark,gluon ] ,get_ref(cIMA*gQCD/rSQRT2) ,qqg_vtx )
  call vertices%add( [cQuark,-cQuark,gluon ] ,get_ref(cIMA*gQCD/rSQRT2) ,qqg_vtx )
  call vertices%add( [sQuark,-sQuark,gluon ] ,get_ref(cIMA*gQCD/rSQRT2) ,qqg_vtx )
  call vertices%add( [tQuark,-tQuark,gluon ] ,get_ref(cIMA*gQCD/rSQRT2) ,qqg_vtx )
  call vertices%add( [bQuark,-bQuark,gluon ] ,get_ref(cIMA*gQCD/rSQRT2) ,qqg_vtx )
  if (offShFl) then
    do ii=1,NeikFam
      call vertices%add( [eikQ(ii),-eikQ(ii),gluon] ,get_ref(cIMA*gQCD/rSQRT2) ,qqg_vtx )
    enddo
    do ii=1,NeikDir
      call vertices%add( [ uQuark,-uStar(ii),eikPh(ii)] ,get_ref(cIMA*gQCD/rSQRT2) ,ffA_vtx )
      call vertices%add( [-uQuark, uStar(ii),eikPh(ii)] ,get_ref(cIMA*gQCD/rSQRT2) ,ffA_vtx )
      call vertices%add( [ dQuark,-dStar(ii),eikPh(ii)] ,get_ref(cIMA*gQCD/rSQRT2) ,ffA_vtx )
      call vertices%add( [-dQuark, dStar(ii),eikPh(ii)] ,get_ref(cIMA*gQCD/rSQRT2) ,ffA_vtx )
      call vertices%add( [ cQuark,-cStar(ii),eikPh(ii)] ,get_ref(cIMA*gQCD/rSQRT2) ,ffA_vtx )
      call vertices%add( [-cQuark, cStar(ii),eikPh(ii)] ,get_ref(cIMA*gQCD/rSQRT2) ,ffA_vtx )
      call vertices%add( [ sQuark,-sStar(ii),eikPh(ii)] ,get_ref(cIMA*gQCD/rSQRT2) ,ffA_vtx )
      call vertices%add( [-sQuark, sStar(ii),eikPh(ii)] ,get_ref(cIMA*gQCD/rSQRT2) ,ffA_vtx )
      call vertices%add( [ bQuark,-bStar(ii),eikPh(ii)] ,get_ref(cIMA*gQCD/rSQRT2) ,ffA_vtx )
      call vertices%add( [-bQuark, bStar(ii),eikPh(ii)] ,get_ref(cIMA*gQCD/rSQRT2) ,ffA_vtx )
      call vertices%add( [ tQuark,-tStar(ii),eikPh(ii)] ,get_ref(cIMA*gQCD/rSQRT2) ,ffA_vtx )
      call vertices%add( [-tQuark, tStar(ii),eikPh(ii)] ,get_ref(cIMA*gQCD/rSQRT2) ,ffA_vtx )
    enddo
  endif
  endif
!
  if (withQED) then
  h2l =-3*gQED/3
  h3l = 2*gQED/3
  h4l =-1*gQED/3
  call vertices%add( [ eleon, -eleon,photon] ,get_ref(cIMA*h2l) ,ffA_vtx )
  call vertices%add( [uQuark,-uQuark,photon] ,get_ref(cIMA*h3l) ,ffA_vtx )
  call vertices%add( [dQuark,-dQuark,photon] ,get_ref(cIMA*h4l) ,ffA_vtx )
  call vertices%add( [  muon,  -muon,photon] ,get_ref(cIMA*h2l) ,ffA_vtx )
  call vertices%add( [cQuark,-cQuark,photon] ,get_ref(cIMA*h3l) ,ffA_vtx )
  call vertices%add( [sQuark,-sQuark,photon] ,get_ref(cIMA*h4l) ,ffA_vtx )
  call vertices%add( [ tauon, -tauon,photon] ,get_ref(cIMA*h2l) ,ffA_vtx )
  call vertices%add( [tQuark,-tQuark,photon] ,get_ref(cIMA*h3l) ,ffA_vtx )
  call vertices%add( [bQuark,-bQuark,photon] ,get_ref(cIMA*h4l) ,ffA_vtx )
  if (offShFl) then
    do ii=1,NeikDir
      call vertices%add( [ uStar(ii),-uStar(ii),photon] ,get_ref(cIMA*h3l) ,ffA_vtx )
      call vertices%add( [ dStar(ii),-dStar(ii),photon] ,get_ref(cIMA*h4l) ,ffA_vtx )
      call vertices%add( [ cStar(ii),-cStar(ii),photon] ,get_ref(cIMA*h3l) ,ffA_vtx )
      call vertices%add( [ sStar(ii),-sStar(ii),photon] ,get_ref(cIMA*h4l) ,ffA_vtx )
      call vertices%add( [ tStar(ii),-tStar(ii),photon] ,get_ref(cIMA*h3l) ,ffA_vtx )
      call vertices%add( [ bStar(ii),-bStar(ii),photon] ,get_ref(cIMA*h4l) ,ffA_vtx )
    enddo
  endif
  endif
! 
  if (withWeak) then
  h1l = ( 3             )/cosWein*gWeak/6  ;h1r =   0
  h2l = (-3+6*sinWein**2)/cosWein*gWeak/6  ;h2r = ( 6*sinWein**2)/cosWein*gWeak/6
  h3l = ( 3-4*sinWein**2)/cosWein*gWeak/6  ;h3r = (-4*sinWein**2)/cosWein*gWeak/6
  h4l = (-3+2*sinWein**2)/cosWein*gWeak/6  ;h4r = ( 2*sinWein**2)/cosWein*gWeak/6
  call vertices%add( [eleNeu,-eleNeu,Zboson] ,get_ref(cIMA*h1l,cIMA*h1r) ,ffV_vtx )
  call vertices%add( [eleon ,-eleon ,Zboson] ,get_ref(cIMA*h2l,cIMA*h2r) ,ffV_vtx )
  call vertices%add( [uQuark,-uQuark,Zboson] ,get_ref(cIMA*h3l,cIMA*h3r) ,ffV_vtx )
  call vertices%add( [dQuark,-dQuark,Zboson] ,get_ref(cIMA*h4l,cIMA*h4r) ,ffV_vtx )
  call vertices%add( [ muNeu, -muNeu,Zboson] ,get_ref(cIMA*h1l,cIMA*h1r) ,ffV_vtx )
  call vertices%add( [  muon,  -muon,Zboson] ,get_ref(cIMA*h2l,cIMA*h2r) ,ffV_vtx )
  call vertices%add( [cQuark,-cQuark,Zboson] ,get_ref(cIMA*h3l,cIMA*h3r) ,ffV_vtx )
  call vertices%add( [sQuark,-sQuark,Zboson] ,get_ref(cIMA*h4l,cIMA*h4r) ,ffV_vtx )
  call vertices%add( [tauNeu,-tauNeu,Zboson] ,get_ref(cIMA*h1l,cIMA*h1r) ,ffV_vtx )
  call vertices%add( [ tauon, -tauon,Zboson] ,get_ref(cIMA*h2l,cIMA*h2r) ,ffV_vtx )
  call vertices%add( [tQuark,-tQuark,Zboson] ,get_ref(cIMA*h3l,cIMA*h3r) ,ffV_vtx )
  call vertices%add( [bQuark,-bQuark,Zboson] ,get_ref(cIMA*h4l,cIMA*h4r) ,ffV_vtx )
  if (offShFl) then
    do ii=1,NeikDir
      call vertices%add( [ uStar(ii),-uStar(ii),Zboson] ,get_ref(cIMA*h3l,cIMA*h3r) ,ffV_vtx )
      call vertices%add( [ dStar(ii),-dStar(ii),Zboson] ,get_ref(cIMA*h4l,cIMA*h4r) ,ffV_vtx )
      call vertices%add( [ cStar(ii),-cStar(ii),Zboson] ,get_ref(cIMA*h3l,cIMA*h3r) ,ffV_vtx )
      call vertices%add( [ sStar(ii),-sStar(ii),Zboson] ,get_ref(cIMA*h4l,cIMA*h4r) ,ffV_vtx )
      call vertices%add( [ tStar(ii),-tStar(ii),Zboson] ,get_ref(cIMA*h3l,cIMA*h3r) ,ffV_vtx )
      call vertices%add( [ bStar(ii),-bStar(ii),Zboson] ,get_ref(cIMA*h4l,cIMA*h4r) ,ffV_vtx )
    enddo
  endif
!
  h1l = gWeak/rSQRT2
  call vertices%add( [ eleNeu, -eleon,-Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
  call vertices%add( [-eleNeu,  eleon, Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
  call vertices%add( [ uQuark,-dQuark,-Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
  call vertices%add( [-uQuark, dQuark, Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
  call vertices%add( [  muNeu,  -muon,-Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
  call vertices%add( [ -muNeu,   muon, Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
  call vertices%add( [ cQuark,-sQuark,-Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
  call vertices%add( [-cQuark, sQuark, Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
  call vertices%add( [ tauNeu, -tauon,-Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
  call vertices%add( [-tauNeu,  tauon, Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
  call vertices%add( [ tQuark,-bQuark,-Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
  call vertices%add( [-tQuark, bQuark, Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
  if (offShFl) then
    do ii=1,NeikDir
      call vertices%add( [ uStar(ii),-dStar(ii),-Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
      call vertices%add( [-uStar(ii), dStar(ii), Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
      call vertices%add( [ cStar(ii),-sStar(ii),-Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
      call vertices%add( [-cStar(ii), sStar(ii), Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
      call vertices%add( [ tStar(ii),-bStar(ii),-Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
      call vertices%add( [-tStar(ii), bStar(ii), Wboson] ,get_ref(cIMA*h1l,cZRO) ,ffV_vtx )
    enddo
  endif
!
  if (withCKM) then
  call vertices%add( [ uQuark,-sQuark,-Wboson] ,get_ref(cZRO) ,ffV_vtx )
  call vertices%add( [-uQuark, sQuark, Wboson] ,get_ref(cZRO) ,ffV_vtx )
  call vertices%add( [ uQuark,-bQuark,-Wboson] ,get_ref(cZRO) ,ffV_vtx )
  call vertices%add( [-uQuark, bQuark, Wboson] ,get_ref(cZRO) ,ffV_vtx )
  call vertices%add( [ cQuark,-dQuark,-Wboson] ,get_ref(cZRO) ,ffV_vtx )
  call vertices%add( [-cQuark, dQuark, Wboson] ,get_ref(cZRO) ,ffV_vtx )
  call vertices%add( [ cQuark,-bQuark,-Wboson] ,get_ref(cZRO) ,ffV_vtx )
  call vertices%add( [-cQuark, bQuark, Wboson] ,get_ref(cZRO) ,ffV_vtx )
  call vertices%add( [ tQuark,-dQuark,-Wboson] ,get_ref(cZRO) ,ffV_vtx )
  call vertices%add( [-tQuark, dQuark, Wboson] ,get_ref(cZRO) ,ffV_vtx )
  call vertices%add( [ tQuark,-sQuark,-Wboson] ,get_ref(cZRO) ,ffV_vtx )
  call vertices%add( [-tQuark, sQuark, Wboson] ,get_ref(cZRO) ,ffV_vtx )
  endif
!
  call vertices%add( [Wboson,-Wboson,Zboson]         ,get_ref( cIMA*gQED*cosWein/sinWein) ,VVV_vtx  )
  call vertices%add( [Wboson,-Wboson,photon]         ,get_ref( cIMA*gQED)                 ,VVV_vtx  )
  call vertices%add( [Wboson,-Wboson,Wboson,-Wboson] ,get_ref( cIMA*(gQED/sinWein)**2)         ,WWWW_vtx )
  call vertices%add( [Wboson,-Wboson,Zboson, Zboson] ,get_ref(-cIMA*(gQED*cosWein/sinWein)**2) ,WWVV_vtx )
  call vertices%add( [Wboson,-Wboson,photon, Zboson] ,get_ref(-cIMA*gQED**2*cosWein/sinWein)   ,WWVV_vtx )
  call vertices%add( [Wboson,-Wboson,photon, photon] ,get_ref(-cIMA*gQED**2)                   ,WWVV_vtx )
  endif
!!
  if (withHiggs) then
  call vertices%add( [Wboson,-Wboson,Higgs] ,get_ref(cIMA*gWeak*mass(Wboson))            ,VVH_vtx )
  call vertices%add( [Zboson, Zboson,Higgs] ,get_ref(cIMA*gWeak*mass(Wboson)/cosWein**2) ,VVH_vtx )
  call vertices%add( [Wboson,-Wboson,Higgs,Higgs] ,get_ref(cIMA*gWeak**2/2)              ,VVHH_vtx )
  h1l =-3*gWeak*mass(Higgs)**2/mass(Wboson)/2
  call vertices%add( [Higgs,Higgs,Higgs]       ,get_ref(cIMA*h1l)                ,HHH_vtx )
  call vertices%add( [Higgs,Higgs,Higgs,Higgs] ,get_ref(cIMA*h1l/mass(Wboson)/2) ,HHHH_vtx )
  h1l =-gWeak/mass(Wboson)/2
  if (mass(tQuark).ne.rZRO) call vertices%add( [tQuark,-tQuark,Higgs] ,get_ref(cIMA*h1l*mass(tQuark)) ,ffH_vtx )
  if (mass(bQuark).ne.rZRO) call vertices%add( [bQuark,-bQuark,Higgs] ,get_ref(cIMA*h1l*mass(bQuark)) ,ffH_vtx )
  if (mass(cQuark).ne.rZRO) call vertices%add( [cQuark,-cQuark,Higgs] ,get_ref(cIMA*h1l*mass(cQuark)) ,ffH_vtx )
  if (mass(sQuark).ne.rZRO) call vertices%add( [sQuark,-sQuark,Higgs] ,get_ref(cIMA*h1l*mass(sQuark)) ,ffH_vtx )
  if (mass( tauon).ne.rZRO) call vertices%add( [ tauon, -tauon,Higgs] ,get_ref(cIMA*h1l*mass( tauon)) ,ffH_vtx )
  if (mass(  muon).ne.rZRO) call vertices%add( [  muon,  -muon,Higgs] ,get_ref(cIMA*h1l*mass(  muon)) ,ffH_vtx )
  if (mass(dQuark).ne.rZRO) call vertices%add( [dQuark,-dQuark,Higgs] ,get_ref(cIMA*h1l*mass(dQuark)) ,ffH_vtx )
  if (mass(uQuark).ne.rZRO) call vertices%add( [uQuark,-uQuark,Higgs] ,get_ref(cIMA*h1l*mass(uQuark)) ,ffH_vtx )
  if (mass( eleon).ne.rZRO) call vertices%add( [eleon ,-eleon ,Higgs] ,get_ref(cIMA*h1l*mass(eleon )) ,ffH_vtx )
  endif
!
  if (withHG) then
  call vertices%add( [Higgs,gluon,gluon]             ,get_ref(cIMA*gHG)             ,H2g_vtx )
  call vertices%add( [Higgs,gluon,gluon,gluon]       ,get_ref(cIMA*gHG*gQCD/rSQRT2) ,H3g_vtx )
  call vertices%add( [Higgs,gluon,gluon,gluon,gluon] ,get_ref(cIMA*gHG*gQCD*gQCD/2) ,H4g_vtx )
  endif
!
  if (withHA) then
  call vertices%add( [Higgs,photon,photon] ,get_ref(cIMA*gHA) ,H2A_vtx )
  endif
!
  call lagrangianRules%fill( particles ,vertices )
  call couplingPowerRules%fill( CoPo_rule ,CoPo_anti ,0 )
!
  end subroutine


  subroutine set_gauge( vec ,par )
  type(lorentz_type),intent(in) :: vec
  !(realknd2!),intent(in) :: par
  gaugeVec = vec
  gaugePar = par
  end subroutine

  function spinor_xl( x ) result(rslt)
  type(lorentz_type) :: rslt
  type(xternalArgs_type),intent(in) :: x
  rslt = xSpinor( x%momentum ,mass(abs(x%identity)) ,x%auxVec ,x%helicity ,x%identity ,x%polarangle )
  end function

  function vector_xl( x ) result(rslt)
  type(lorentz_type) :: rslt
  type(xternalArgs_type),intent(in) :: x
  rslt = xVector( x%momentum ,mass(abs(x%identity)) ,x%auxVec ,x%helicity ,x%polarangle )
  end function

  function scalar_xl( x ) result(rslt)
  type(lorentz_type) :: rslt
  type(xternalArgs_type),intent(in) :: x
  rslt = xScalar()
  end function

  function eikQ_xl( x ) result(rslt)
  type(lorentz_type) :: rslt
  type(xternalArgs_type),intent(in) :: x
  rslt = xSpinor( eikVec(iEikVec(x%identity)) ,x%auxVec ,x%helicity ,x%identity ,x%polarangle )
  end function

  function eikPh_xl( x ) result(rslt)
  type(lorentz_type) :: rslt
  type(xternalArgs_type),intent(in) :: x
  rslt = xVector( eikVec(iEikVec(x%identity)) ,x%auxVec ,x%helicity ,x%polarangle )
  end function


  subroutine f_pgr &
  include 'propagator.h90'
  call fPropagator( u ,p ,cmplxMass(abs(i)) )
  end subroutine

  subroutine g_pgr &
  include 'propagator.h90'
  call gPropagator( u ,p ,gaugeVec ,gaugePar )
  end subroutine

  subroutine v_pgr &
  include 'propagator.h90'
  !call vPropagator( u ,p ,cmplxMass(abs(i)) ,gaugePar )
  call vPropagator( u ,p ,cmplxMass(abs(i)) ,2*rONE )
  end subroutine

  subroutine e_pgr &
  include 'propagator.h90'
  call ePropagator( u ,p ,eikVec(iEikVec(i)) )
  end subroutine

  subroutine s_pgr &
  include 'propagator.h90'
  call sPropagator( u ,p ,cmplxMass(abs(i)) )
  end subroutine


  function ggg_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  rslt = gggVertex( v%p0 ,v%p1,v%u1 ,v%p2,v%u2 &
                   ,v%Fsign*cplxcnst(1,v%cPrim) &
                    *( cplxcnst(perm_a_few(1,tab_3g(v%pSecn)),v%cSecn) &
                      -cplxcnst(perm_a_few(2,tab_3g(v%pSecn)),v%cSecn) ) )
  end function

  function gggg_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  !(complex2!) :: zz
  zz = v%Fsign*cplxcnst(1,v%cPrim)
  rslt = ggggVertex( v%u1 ,v%u2 ,v%u3 &
                    ,zz*cplxcnst(perm_a_few(1,tab_4g(v%pSecn)),v%cSecn) &
                    ,zz*cplxcnst(perm_a_few(2,tab_4g(v%pSecn)),v%cSecn) &
                    ,zz*cplxcnst(perm_a_few(3,tab_4g(v%pSecn)),v%cSecn) )
  end function

  function H2g_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  rslt = s2gVertex( v%p0 ,v%p1,v%u1 ,v%p2,v%u2 &
                   ,v%Fsign*cplxcnst(1,v%cPrim)*cplxcnst(1,v%cSecn) )
  end function

  function H3g_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  rslt = s3gVertex( v%p0 ,v%p1,v%u1 ,v%p2,v%u2 ,v%p3,v%u3 &
                   ,v%Fsign*cplxcnst(1,v%cPrim) &
                    *( cplxcnst(perm_a_few(1,tab_s3g(v%pSecn)),v%cSecn) &
                      -cplxcnst(perm_a_few(2,tab_s3g(v%pSecn)),v%cSecn) ) )
  end function

  function H4g_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  !(complex2!) :: zz
  zz = v%Fsign*cplxcnst(1,v%cPrim)
  rslt = s4gVertex( v%u1 ,v%u2 ,v%u3 ,v%u4 &
                   ,zz*cplxcnst(perm_a_few(1,tab_s4g(v%pSecn)),v%cSecn) &
                   ,zz*cplxcnst(perm_a_few(2,tab_s4g(v%pSecn)),v%cSecn) &
                   ,zz*cplxcnst(perm_a_few(3,tab_s4g(v%pSecn)),v%cSecn) )
  end function

  function H2A_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  rslt = s2gVertex( v%p0 ,v%p1,v%u1 ,v%p2,v%u2 ,v%Fsign*cplxcnst(1,v%cPrim) )
  end function

  function qqg_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  rslt = ffvVertex( v%u1 ,v%u2 ,v%Fsign*cplxcnst(1,v%cPrim)*cplxcnst(1,v%cSecn) )
  end function

  function ffA_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  rslt = ffvVertex( v%u1 ,v%u2 ,v%Fsign*cplxcnst(1,v%cPrim) )
  end function

  function ffV_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  rslt = ffvVertex( v%u1 ,v%u2 ,v%Fsign*cplxcnst(1,v%cPrim) &
                               ,v%Fsign*cplxcnst(2,v%cPrim) )
  end function

  function ffH_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  rslt = ffsVertex( v%u1 ,v%u2 ,v%Fsign*cplxcnst(1,v%cPrim) )
  end function

  function HHH_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  rslt = sssVertex( v%u1 ,v%u2 ,v%Fsign*cplxcnst(1,v%cPrim) )
  end function

  function HHHH_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  rslt = ssssVertex( v%u1 ,v%u2 ,v%u3 ,v%Fsign*cplxcnst(1,v%cPrim) )
  end function

  function VVH_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  rslt = vvsVertex( v%u1 ,v%u2 ,v%Fsign*cplxcnst(1,v%cPrim) )
  end function

  function VVHH_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  rslt = vvssVertex( v%u1 ,v%u2 ,v%u3 ,v%Fsign*cplxcnst(1,v%cPrim) )
  end function

  function VVV_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  !(complex2!) :: zz
  zz = v%Fsign*cplxcnst(1,v%cPrim)
  if     (v%e(0).eq. Wboson) then ;if (v%e(1).eq.-Wboson) zz =-zz
  elseif (v%e(0).eq.-Wboson) then ;if (v%e(2).eq. Wboson) zz =-zz
                             else ;if (v%e(1).eq. Wboson) zz =-zz
  endif
  rslt = gggVertex( v%p0 ,v%p1,v%u1 ,v%p2,v%u2 ,zz )
  end function

  function WWWW_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  if     (v%e(1).eq.v%e(2)) then
    rslt = vvvvVertex( v%u1 ,v%u2 ,v%u3 ,v%Fsign*cplxcnst(1,v%cPrim) )
  elseif (v%e(2).eq.v%e(3)) then
    rslt = vvvvVertex( v%u2 ,v%u3 ,v%u1 ,v%Fsign*cplxcnst(1,v%cPrim) )
  else
    rslt = vvvvVertex( v%u1 ,v%u3 ,v%u2 ,v%Fsign*cplxcnst(1,v%cPrim) )
  endif
  end function

  function WWVV_vtx( v ) result(rslt)
  type(vertexArgs_type),intent(in) :: v
  type(lorentz_type) :: rslt
  if     (abs(v%e(1)).eq.abs(v%e(2)).or.abs(v%e(0)).eq.abs(v%e(3))) then
    rslt = vvvvVertex( v%u1 ,v%u2 ,v%u3 ,v%Fsign*cplxcnst(1,v%cPrim) )
  elseif (abs(v%e(2)).eq.abs(v%e(3)).or.abs(v%e(0)).eq.abs(v%e(1))) then
    rslt = vvvvVertex( v%u2 ,v%u3 ,v%u1 ,v%Fsign*cplxcnst(1,v%cPrim) )
  else
    rslt = vvvvVertex( v%u1 ,v%u3 ,v%u2 ,v%Fsign*cplxcnst(1,v%cPrim) )
  endif
  end function

!  function _vtx( v ) result(rslt)
!  type(vertexArgs_type),intent(in) :: v
!  type(lorentz_type) :: rslt
!  rslt = Vertex(  )
!  end function
!


  function get_anti( ii ) result(rslt)
  integer,intent(in) :: ii
  integer :: rslt
  rslt = lagrangianRules%Particle(ii)%Anti
  end function

  function get_onshell( ii ) result(rslt)
  integer,intent(in) :: ii
  integer :: rslt
  select case (ii)
  case default ; rslt = ii
  case ( gStar(1), gStar(2)) ;rslt = gluon
  case ( uStar(1), uStar(2)) ;rslt = uQuark
  case (-uStar(1),-uStar(2)) ;rslt =-uQuark
  case ( dStar(1), dStar(2)) ;rslt = dQuark
  case (-dStar(1),-dStar(2)) ;rslt =-dQuark
  case ( sStar(1), sStar(2)) ;rslt = sQuark
  case (-sStar(1),-sStar(2)) ;rslt =-sQuark
  case ( cStar(1), cStar(2)) ;rslt = cQuark
  case (-cStar(1),-cStar(2)) ;rslt =-cQuark
  case ( bStar(1), bStar(2)) ;rslt = bQuark
  case (-bStar(1),-bStar(2)) ;rslt =-bQuark
  end select
  end function

  function get_offshell1( ii ,jj ) result(rslt)
  integer,intent(in) :: ii,jj
  integer :: rslt
  if (jj.eq.0) then
    rslt = ii
  else
    select case (ii)
    case default ;rslt = ii
    case ( gluon ) ;rslt = gStar(jj)
    case ( uQuark) ;rslt = uStar(jj)
    case (-uQuark) ;rslt =-uStar(jj)
    case ( dQuark) ;rslt = dStar(jj)
    case (-dQuark) ;rslt =-dStar(jj)
    case ( cQuark) ;rslt = cStar(jj)
    case (-cQuark) ;rslt =-cStar(jj)
    case ( sQuark) ;rslt = sStar(jj)
    case (-sQuark) ;rslt =-sStar(jj)
    case ( tQuark) ;rslt = tStar(jj)
    case (-tQuark) ;rslt =-tStar(jj)
    case ( bQuark) ;rslt = bStar(jj)
    case (-bQuark) ;rslt =-bStar(jj)
    end select
  endif
  end function

  function get_offshell2( ii ,task ) result(rslt)
  integer,intent(in) :: ii(-2:-1),task(-2:-1)
  integer :: rslt(-2:-1),i1,i2
  i2=ii(-2) ;if(task(-2).gt.0) i2=get_offshell1(ii(-2),2)
  i1=ii(-1) ;if(task(-1).gt.0) i1=get_offshell1(ii(-1),1)
  rslt(-2:-1) = [i2,i1]
  end function

  function get_NspinDof( Nxtrn,proc ) result(rslt)
  integer,intent(in) :: Nxtrn
  integer,intent(in) :: proc(1:Nxtrn)
  integer :: rslt(1:Nxtrn),ii
  do ii=1,Nxtrn
    rslt(ii) = NspinDof(proc(ii))
  enddo
  end function


  function prnt_particle(ii) result(rslt)
  integer,intent(in) :: ii
  character(3) :: rslt
  if (ii.le.nulParticle) then 
    rslt = ''
  else
    rslt = trim(lagrangianRules%Particle(ii)%Symbol)
  endif
  end function

  function prnt_particle_2(nn,array) result(rslt)
  integer,intent(in) :: nn
  integer,intent(in) :: array(nn)
  character(1+4*nn) :: rslt
  integer :: ii
  rslt = ''
  do ii=1,nn
    rslt = trim(rslt)//' '//trim(prnt_particle(array(ii)))
  enddo
  end function

  function prnt_particle_3(aa) result(rslt)
  integer,intent(in) :: aa(:)
  character(1+4*size(aa)) :: rslt
  integer :: ii
  rslt = ''
  do ii=1,size(aa)
    rslt = trim(rslt)//' '//trim(prnt_particle(aa(ii)))
  enddo
  end function

  function prnt_particle_4(aa,bb) result(rslt)
  integer,intent(in) :: aa(2),bb(:)
  character(1+11+4*size(bb)) :: rslt
  integer :: ii
  rslt = ''
  do ii=1,2
    rslt = trim(rslt)//' '//trim(prnt_particle(aa(ii)))
  enddo
  rslt = trim(rslt)//' ->'
  do ii=1,size(bb)
    rslt = trim(rslt)//' '//trim(prnt_particle(bb(ii)))
  enddo
  end function

  function get_symFac(Nfinst,process) result(rslt)
!***********************************************************************
!***********************************************************************
  use avh_doloops, only: igamm
  integer,intent(in) :: Nfinst
  integer,intent(in) :: process(Nfinst)
  integer :: rslt
  integer :: multiplicity(nulParticle:maxParticle),ii
  rslt = 1
  multiplicity = 0
  do ii=1,Nfinst
    multiplicity(process(ii)) = multiplicity(process(ii)) + 1
  enddo
  do ii=nulParticle+1,maxParticle
    if (multiplicity(ii).ne.0) rslt = rslt*igamm(1+multiplicity(ii))
  enddo
  end function

  function std_order(process) result(rslt)
!***********************************************************************
!***********************************************************************
  integer,intent(in) :: process(:)
  integer :: Nxtrn,ii,jj,nn ,rslt(size(process)) 
  integer :: amount(nulParticle+1:maxParticle)
  integer :: positn(nulParticle+1:maxParticle,13)
  Nxtrn = size(process,1)
  amount = 0
  do ii=1,Nxtrn
    jj = process(ii)
    amount(jj) = amount(jj) + 1
    positn(jj,amount(jj)) = ii
  enddo
  nn = 0
  call put(gluon)
  call put(-uQuark) ;call put(-dQuark)
  call put(-cQuark) ;call put(-sQuark)
  call put(-tQuark) ;call put(-bQuark)
  call put( uQuark) ;call put( dQuark)
  call put( cQuark) ;call put( sQuark)
  call put( tQuark) ;call put( bQuark)
  do ii=nulParticle+1,maxParticle
    call put(ii)
  enddo
  contains
    subroutine put(ii)
    integer,intent(in) :: ii
    integer :: jj
    do jj=1,amount(ii)
      nn = nn+1
      rslt(nn) = positn(ii,jj)
    enddo
    amount(ii) = 0
    end subroutine
  end function


!  function flavor_ordered( process ) result(rslt)
!  integer,intent(in) :: process(:)
!  integer :: rslt(size(process,1)) ,ii
!  integer :: Nflavor(             0:maxParticle)
!  integer :: Pflavor(size_xternal,0:maxParticle)
!  Nflavor = 0
!  do ii=1,size(process)
!    do jj=nulParticle+1,maxParticle
!      if (abs(process(ii)).eq.jj) then
!        Nflavor(jj) = Nflavor(jj)+1
!        Pflavor(Nflavor(jj),jj) = ii
!        exit
!      endif
!    enddo
!  enddo
!  oo = 0
!  do jj=0,maxParticle
!    if (Nflavor(jj).le.0) cycle
!    if (abs(process(Pflavor(1,jj))).lt.abs(nulParticle)) then
!      ii = 0
!      do ;ii=ii+1 ;if (ii.gt.Nflavor(jj)/2) exit
!        if (process(Pflavor(ii,jj)).gt.0) then
!          hh = Pflavor(ii,jj)
!          Pflavor(ii:Nflavor(jj)-1,jj) = Pflavor(ii+1:Nflavor(jj),jj)
!          Pflavor(Nflavor(jj),jj) = hh
!          ii = ii-1
!        endif
!      enddo
!    endif
!    rslt(oo+1:oo+Nflavor(jj)) = Pflavor(1:Nflavor(jj),jj)
!    oo = oo+Nflavor(jj)
!  enddo
!  end function


!***********************************************************************
! Vertex rule for power of coupling
!   QCD^a * EW^b * higgsGluon^c * higgsPhoton^d
! External process should be
!   xProcess=[1,1,...,1, 2**a * 3**b * 5**c * 7**d ]
! where  pNonQCD(1)=b  pNonQCD(2)=c  pNonQCD(3)=d
!***********************************************************************
  function xCouplingPower( pNonQCD ,Nxtrn ) result(rslt)
  integer,intent(in) :: pNonQCD(3) ,Nxtrn
  integer :: rslt(Nxtrn)
  if (any(pNonQCD.lt.0).or.Nxtrn-2.lt.sum(pNonQCD)) then
    if (errru.ge.0) write(errru,*) 'ERROR in put_xCoPo: ' &
      ,'pNotQCD = ',prnt(pNonQCD,'lft')
    stop
  endif
  rslt(1:Nxtrn-1) = 1
  rslt(Nxtrn) = 2**(Nxtrn-2-sum(pNonQCD)) &
              * 3**pNonQCD(1) * 5**pNonQCD(2) * 7**pNonQCD(3)
  end function

  subroutine CoPo_rule( p ,f ,ifus ,f0 ,discard )
  integer,intent(in   ) :: p(0:) ,f(1:)
  integer,intent(inout) :: ifus
  integer,intent(out  ) :: f0
  logical,intent(out  ) :: discard
  optional              :: f0,discard
  integer,parameter :: qcd=2,elwe=3,higl=5,hiph=7
  integer :: sizef,ii
  logical :: hasGluon,hasHiggs,hasPhoton
  if (present(f0)) then
    discard = .false.
    sizef = size(f)
    f0 = 1
    hasGluon = p(0).eq.gluon.or.any(p(0).eq.eikPh)
    hasHiggs = p(0).eq.Higgs
    hasPhoton = p(0).eq.Photon
    do ii=1,sizef
      f0 = f0*f(ii)
      hasGLuon = hasGluon.or.(p(ii).eq.gluon).or.any(p(ii).eq.eikPh)
      hasHiggs = hasHiggs.or.(p(ii).eq.Higgs)
      hasPhoton = hasPhoton.or.(p(ii).eq.Photon)
    enddo
    if (hasHiggs) then
      if (hasGluon) then
        f0 = f0*higl
        if (sizef.gt.2) f0 = f0*qcd
        if (sizef.gt.3) f0 = f0*qcd
      elseif (hasPhoton) then
        f0 = f0*hiph
        if (sizef.gt.2) f0 = f0*elwe
        if (sizef.gt.3) f0 = f0*elwe
      else
        f0 = f0*elwe
        if (sizef.gt.2) f0 = f0*elwe
      endif
    elseif (hasGluon) then
      f0 = f0*qcd
      if (sizef.gt.2) f0 = f0*qcd
    else
      f0 = f0*elwe
      if (sizef.gt.2) f0 = f0*elwe
    endif
  else
    ifus = 1
  endif
  end subroutine

!!***********************************************************************
!! Vertex rule for power of coupling  gQCD^a * gOTHER^b
!! External process should be  xProcess=[1,1,...,1, 2**a * 3**b ]
!!***********************************************************************
!  function xCouplingPower( pNotQCD ,Nxtrn ) result(rslt)
!  integer,intent(in) :: pNotQCD ,Nxtrn
!  integer :: rslt(Nxtrn)
!  if (pNotQCD.lt.0.or.Nxtrn-2.lt.pNotQCD) then
!    if (errru.ge.0) write(errru,*) 'ERROR in put_xCoPo: ' &
!      ,'pNotQCD = ',prnt(pNotQCD,'lft')
!    stop
!  endif
!  rslt(1:Nxtrn-1) = 1
!  rslt(Nxtrn) = 2**(Nxtrn-2-pNotQCD)*3**pNotQCD
!  end function
!
!  subroutine CoPo_rule( p ,f ,ifus ,f0 ,discard )
!  integer,intent(in   ) :: p(0:) ,f(1:)
!  integer,intent(inout) :: ifus
!  integer,intent(out  ) :: f0
!  logical,intent(out  ) :: discard
!  optional              :: f0,discard
!  integer,parameter :: qcd=2,other=3
!  integer :: sizef,ii
!  logical :: hasGluon,hasHiggs
!  if (present(f0)) then
!    discard = .false.
!    sizef = size(f)
!    f0 = 1
!    hasGluon = p(0).eq.gluon.or.any(p(0).eq.eikPh)
!    hasHiggs = p(0).eq.Higgs
!    do ii=1,sizef
!      f0 = f0*f(ii)
!      hasGLuon = hasGluon.or.(p(ii).eq.gluon).or.any(p(ii).eq.eikPh)
!      hasHiggs = hasHiggs.or.(p(ii).eq.Higgs)
!    enddo
!    if (hasHiggs) then
!      f0 = f0*other
!      if (hasGluon) then
!        if (sizef.gt.2) f0 = f0*qcd
!        if (sizef.gt.3) f0 = f0*qcd
!      else
!        if (sizef.gt.2) f0 = f0*other
!      endif
!    else
!      if (hasGluon) then
!        f0 = f0*qcd
!        if (sizef.gt.2) f0 = f0*qcd
!      else
!        f0 = f0*other
!        if (sizef.gt.2) f0 = f0*other
!      endif
!    endif
!  else
!    ifus = 1
!  endif
!  end subroutine

  function CoPo_anti( jj ) result(ii)
  integer,intent(in) :: jj
  integer :: ii
  ii = jj
  end function 


  subroutine resize( xx ,l1,u1 )
  !(realknd2!) &
  include '../arrays/resize1.h90'
  end subroutine

  subroutine enlarge( xx ,l1,u1 )
  integer &
  include '../arrays/enlarge1.h90'
  end subroutine
  
end module


