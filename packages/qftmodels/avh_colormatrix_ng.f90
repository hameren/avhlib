module avh_colormatrix_ng
  use avh_doloops
  use avh_colormatrix_qq
  implicit none
  private
  public :: fill_ng_square ,fill_ng_single ,fill_ng_double

  integer,parameter :: left(18)=[1,4,7,10,13,16,19,22,25,28,31,34,37,40,43,46,49,52]
  integer,parameter ::  mid(18)=[2,5,8,11,14,17,20,23,26,29,32,35,38,41,44,47,50,53]
  integer,parameter :: rght(18)=[3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54]

  type :: fstring_type
    integer :: Ntot
    integer,allocatable :: adj(:)
    integer :: factor
  contains 
    procedure :: dalloc
    procedure :: alloc
    procedure :: init
    procedure :: split 
    procedure :: copy
    procedure :: connectMid
    procedure :: insertMid
    procedure :: insertLeft
    procedure :: insertRght
  end type

  abstract interface
    subroutine dc_interface
    end subroutine
  end interface

contains

  subroutine dalloc( obj )
    class(fstring_type) :: obj
    if (allocated(obj%adj)) deallocate(obj%adj)
    obj%Ntot = 0
    obj%factor = 0
  end subroutine
  
  subroutine alloc( obj ,nn )
    class(fstring_type) :: obj
    integer,intent(in) :: nn
    call obj%dalloc
    allocate(obj%adj(3*nn))
    obj%Ntot = nn
  end subroutine
  
  subroutine copy( obj ,xx )
    class(fstring_type),intent(in)  :: obj
    class(fstring_type),intent(out) :: xx
    call xx%alloc( 3*obj%Ntot )
    xx%adj = obj%adj
    xx%factor = obj%factor
  end subroutine
  
  subroutine init( obj ,factor )
    class(fstring_type) :: obj
    integer,intent(in) :: factor
    integer :: ii
    obj%factor = factor
    obj%adj(left(1)) = rght(obj%Ntot)
    do ii=1,obj%Ntot-1
      obj%adj(rght(ii  )) = left(ii+1)
      obj%adj(left(ii+1)) = rght(ii  )
    enddo
    obj%adj(rght(obj%Ntot)) = left(1)
  end subroutine
  
  subroutine split( obj ,pp )
    class(fstring_type) :: obj
    integer,intent(in) :: pp(:)
    integer :: ii,nn,rr,jj
    nn = size(pp,1)
    jj = obj%adj(rght(pp(nn)))
    rr = obj%adj(rght(pp(1 )))
    obj%adj(jj)=rght(pp(1)) ;obj%adj(obj%adj(jj))=jj
    do ii=2,nn
      jj = rr
      rr = obj%adj(rght(pp(ii)))
      obj%adj(jj)=rght(pp(ii)) ;obj%adj(obj%adj(jj))=jj
    enddo
  end subroutine
  
  subroutine connectMid( obj ,n1 ,n2 )
    class(fstring_type) :: obj
    integer,intent(in) :: n1,n2
    obj%adj(mid(n1)) = mid(n2)
    obj%adj(mid(n2)) = mid(n1)
  end subroutine
  
  subroutine insertMid( obj ,n1 ,n2 )
    class(fstring_type) :: obj
    integer,intent(in) :: n1,n2
    integer :: aa
    aa = obj%adj(mid(n1))
    obj%adj(mid(n1))=left(n2) ;obj%adj(left(n2))=mid(n1) 
    obj%adj( aa    )=rght(n2) ;obj%adj(rght(n2))=aa
  end subroutine
  
  subroutine insertLeft( obj ,n1 ,n2 )
    class(fstring_type) :: obj
    integer,intent(in) :: n1,n2
    integer :: aa
    aa = obj%adj(left(n1))
    obj%adj(left(n1))=left(n2) ;obj%adj(left(n2))=left(n1) 
    obj%adj( aa     )=rght(n2) ;obj%adj(rght(n2))=aa
  end subroutine
  
  subroutine insertRght( obj ,n1 ,n2 )
    class(fstring_type) :: obj
    integer,intent(in) :: n1,n2
    integer :: aa
    aa = obj%adj(rght(n1))
    obj%adj(rght(n1))=left(n2) ;obj%adj(left(n2))=rght(n1) 
    obj%adj( aa     )=rght(n2) ;obj%adj(rght(n2))=aa
  end subroutine
  
  
  subroutine f_mult_eval( f ,rslt )
    class(fstring_type), intent(in) :: f
    integer, intent(out) ::  rslt
    type(tstring_type),allocatable :: t(:)
    integer :: Nt,nn,ii,term,tNtot,aa,bb
! separate symbol for the number of entries in each t-string
    tNtot = 3*f%Ntot
! allocate the t-strings
    allocate( t(2**f%Ntot) )
    do ii=1,2**f%Ntot
      call t(ii)%alloc( tNtot )
    enddo
! initialize only the first one
    call t(1)%init( f%factor )
! separate in groups of three
    do ii=1,f%Ntot
      t(1)%left(3*ii-2) = 3*ii
      t(1)%rght(3*ii  ) = 3*ii-2
    enddo
! put the adjoint indices
    do ii=1,3*f%Ntot
      t(1)%adj(ii) = f%adj(ii)
    enddo
! recursively fill up the other t-strings
    Nt = 1
    do nn=1,f%Ntot
!   copy the Nt existing ones on Nt new ones
      do ii=1,Nt
        call t(ii)%copy( t(Nt+ii) )
      enddo
!   in the new ones, switch last two adjoint indices and put minus
      do ii=Nt+1,2*Nt
        aa = t(ii)%adj(3*nn-1)
        bb = t(ii)%adj(3*nn  )
        t(ii)%adj(3*nn-1) = bb  ;t(ii)%adj(bb) = 3*nn-1
        t(ii)%adj(3*nn  ) = aa  ;t(ii)%adj(aa) = 3*nn
        t(ii)%factor =-t(ii)%factor
      enddo
      Nt = 2*Nt
    enddo
! evaluate all t-strings
    rslt = 0
    do ii=1,2**f%Ntot
      call eval_tstring_ng( t(ii) ,term )
      rslt = rslt+term
    enddo
    deallocate( t )
  end subroutine
  
  
  subroutine fill_ng_square( Ngluon ,matrix )
    integer,intent(in) :: Ngluon
    integer,intent(out) :: matrix(igamm(Ngluon-1),igamm(Ngluon-1))
    type(permut_type) :: permut,qermut
    integer :: NgMinus2,pCnt,qCnt,hh,pp(13),qq(13),invq(13)
    type(fstring_type) :: f
    NgMinus2 = Ngluon-2
    call f%alloc(NgMinus2*2)
!
    pCnt=0 ;call permut%init(NgMinus2,pp) ;do ;pCnt=pCnt+1
    qCnt=0 ;call qermut%init(NgMinus2,qq) ;do ;qCnt=qCnt+1
      do hh=1,NgMinus2
        invq(qq(hh)) = hh
      enddo
      call f%init( Ncolor(NgMinus2) )
      do hh=1,NgMinus2
        call f%connectMid( hh ,NgMinus2+(NgMinus2-invq(pp(hh))+1) )
      enddo
      call f_mult_eval( f ,matrix(pCnt,qCnt) )
      matrix(pCnt,qCnt) = matrix(pCnt,qCnt)/Ncolor(NgMinus2)
    if (qermut%exit(qq)) exit ;enddo
    if (permut%exit(pp)) exit ;enddo
    call f%dalloc()
  end subroutine
  
  
  subroutine fill_ng_single( Ngluon ,corr ,matrix )
    integer,intent(in) :: Ngluon,corr(2)
    integer,intent(out) :: matrix(igamm(Ngluon-1),igamm(Ngluon-1))
    type(permut_type) :: permut,qermut
    integer :: pp(13),qq(13),invp(13),invq(13)
    integer :: pCnt,qCnt,twoN,ii,jj,hh,NgMinus2
    type(fstring_type) :: f
!
    ii=corr(1) ;jj=corr(2)
!
    NgMinus2 = Ngluon-2
    twoN = NgMinus2*2
    call f%alloc( twoN+2 )
!
    pCnt=0 ;call permut%init(NgMinus2,pp) ;do ;pCnt=pCnt+1
    qCnt=0 ;call qermut%init(NgMinus2,qq) ;do ;qCnt=qCnt+1
      do hh=1,NgMinus2
        invp(pp(hh)) = hh
        invq(qq(hh)) = hh
      enddo
      call f%init( Ncolor(NgMinus2+1) )
      call f%split( [twoN,twoN+1,twoN+2] )
      do hh=1,NgMinus2
        call f%connectMid( invp(hh) ,NgMinus2+(NgMinus2-invq(hh)+1) )
      enddo
      call insert( ii ,twoN+1 )
      call insert( jj ,twoN+2 )
      call f%connectMid( twoN+1 ,twoN+2 )
      call f_mult_eval( f ,matrix(pCnt,qCnt) )
      matrix(pCnt,qCnt) = matrix(pCnt,qCnt)/Ncolor(NgMinus2+1)/2
    if (qermut%exit(qq)) exit ;enddo
    if (permut%exit(pp)) exit ;enddo
    call f%dalloc()
!
  contains
!
    subroutine insert( n1 ,n2 )
      integer,intent(in) :: n1,n2
      integer :: aa
      if (n1.le.NgMinus2) then
        call f%insertMid( invp(n1) ,n2 )
      elseif (n1.eq.NgMinus2+1) then
        call f%insertRght( NgMinus2 ,n2 )
      else!if(NgMinus2.eq.NgMinus2+2) then
        call f%insertLeft( 1 ,n2 )
      endif
    end subroutine
! 
  end subroutine
      
  
  subroutine fill_ng_double( Ngluon ,corr ,matrix )
    integer,intent(in) :: Ngluon,corr(4)
    integer,intent(out) :: matrix(igamm(Ngluon-1),igamm(Ngluon-1))
    type(permut_type) :: permut,qermut
    integer :: pp(13),qq(13),invp(13),invq(13)
    integer :: pCnt,qCnt,twoN,ii,jj,kk,ll,hh,NgMinus2
    type(fstring_type) :: f
!
    !ii=corr(1) ;jj=corr(2) ;kk=corr(3) ;ll=corr(4)
    ii=corr(3) ;jj=corr(4) ;kk=corr(1) ;ll=corr(2)
!
    NgMinus2 = Ngluon-2
    twoN = NgMinus2*2
    call f%alloc( twoN+4 )
!
    if (ii.eq.kk) then
      if (jj.eq.ll) then
        call eval_dc(g1212)
      else
        call eval_dc(g1213)
      endif
    elseif (ii.eq.ll) then
      hh=kk;kk=ll;ll=hh
      if (jj.eq.ll) then
        call eval_dc(g1212)
      else
        call eval_dc(g1213)
      endif
    elseif (jj.eq.kk) then
      hh=ii;ii=jj;jj=hh
      call eval_dc(g1213)
    elseif (jj.eq.ll) then
      hh=ii;ii=jj;jj=hh
      hh=kk;kk=ll;ll=hh
      call eval_dc(g1213)
    else
      call eval_dc(g1234)
    endif
!
    call f%dalloc()
!
  contains
!
    subroutine eval_dc(func)
      procedure(dc_interface) :: func
      pCnt=0 ;call permut%init(NgMinus2,pp) ;do ;pCnt=pCnt+1
      qCnt=0 ;call qermut%init(NgMinus2,qq) ;do ;qCnt=qCnt+1
        do hh=1,NgMinus2
          invp(pp(hh)) = hh
          invq(qq(hh)) = hh
        enddo
        call f%init( Ncolor(NgMinus2+2) )
        call f%split( [twoN,twoN+1,twoN+2,twoN+3,twoN+4] )
        do hh=1,NgMinus2
          call f%connectMid( invp(hh) ,NgMinus2+(NgMinus2-invq(hh)+1) )
        enddo
        call func
        call f%connectMid( twoN+1 ,twoN+2 )
        call f%connectMid( twoN+3 ,twoN+4 )
        call f_mult_eval( f ,matrix(pCnt,qCnt) )
        matrix(pCnt,qCnt) = matrix(pCnt,qCnt)/Ncolor(NgMinus2+2)/4
      if (qermut%exit(qq)) exit ;enddo
      if (permut%exit(pp)) exit ;enddo
    end subroutine
!
    subroutine g1234
      call insert( ii ,twoN+1 )
      call insert( jj ,twoN+2 )
      call insert( kk ,twoN+3 )
      call insert( ll ,twoN+4 )
    end subroutine
!
    subroutine g1213
      call insert( ii ,twoN+1 )
      call insert( jj ,twoN+2 )
      call f%insertRght( twoN+1 ,twoN+3 )
      call insert( ll ,twoN+4 )
    end subroutine
!
    subroutine g1212
      call insert( ii ,twoN+1 )
      call insert( jj ,twoN+2 )
      call f%insertRght( twoN+1 ,twoN+3 )
      call f%insertRght( twoN+2 ,twoN+4 )
    end subroutine
!
    subroutine insert( n1 ,n2 )
      integer,intent(in) :: n1,n2
      integer :: aa
      if (n1.le.NgMinus2) then
        call f%insertMid( invp(n1) ,n2 )
      elseif (n1.eq.NgMinus2+1) then
        call f%insertRght( NgMinus2 ,n2 )
      else!if(NgMinus2.eq.NgMinus2+2) then
        call f%insertLeft( 1 ,n2 )
      endif
    end subroutine
!
  end subroutine
    

end module


