!# Begin template input file.
!# Remove exclamation marks.
!# Lines starting with # or ! are skipped.
!# The order of items doesn't matter.
!# Empty items get default values.
!# The options for historgram format are
!#   'middle' or 'empty-bin' or 'full-bin'
!#
!        data file = 
!   histogram file = 
!       observable =
!      left border = 
!     right border = 
!   number of bins = 
!   overall factor = 
!        read unit = 
!       write unit = 
! histogram format = 
!#
!# End template input file.

program histogram
  use avh_mctools ,only: read_lines,extract_values

! The following module should provide  read_head  and  read_event
  use !(eventFileReader!) ,only: read_head,read_event

  implicit none
  integer,parameter :: lineLen=72
  integer,parameter :: stdin=5
  real(kind(1d0)),parameter :: nul=0d0
  character(lineLen),allocatable :: lines(:)
  character(lineLen) :: dataFile,histFile,hFormat,observable
  integer :: iFrst(0:lineLen),iLast(0:lineLen)
  real(kind(1d0)),allocatable :: bin(:,:)
  real(kind(1d0)) :: xLeft,xRight,factor,value,weight,step
  integer :: Nbins,rUnit,wUnit,ii,dataLen,histLen,hFmtLen,Nev
  integer :: obsLen,nWords
  logical :: eof

! Read input file
  call read_lines( lines ,lineLen ,stdin )

! default values
     xLeft = 0
    xRight = 1
     Nbins = 32
    factor = 1
  dataFile = 'datafile.dat' ;dataLen=12
  histFile = 'histfile.dat' ;histLen=12
   hFormat = 'empty-bin'    ;hFmtLen=9
     rUnit = 21
     wUnit = 21

! Parse input file
  do ii=1,ubound(lines,1)
    call extract_values( iFrst,iLast,nWords ,lines(ii) ,'=','#!' )
    if (iLast(0).le.0) cycle
    if (iLast(1).le.0) cycle
    select case (lines(ii)(iFrst(0):iLast(0)))
      case (   'left border') ;read(lines(iFrst(1):iLast(1)),*) xLeft
      case (  'right border') ;read(lines(iFrst(1):iLast(1)),*) xRight
      case ('number of bins') ;read(lines(iFrst(1):iLast(1)),*) Nbins
      case ('overall factor') ;read(lines(iFrst(1):iLast(1)),*) factor
      case (     'read unit') ;read(lines(iFrst(1):iLast(1)),*) rUnit
      case (    'write unit') ;read(lines(iFrst(1):iLast(1)),*) wUnit
      case ('data file') 
        dataLen = iLast(1)-iFrst(1)+1
        dataFile(1:dataLen) = lines(ii)(iFrst(1):iLast(1))
      case ('histogram file') 
        histLen = iLast(1)-iFrst(1)+1
        histFile(1:histLen) = lines(ii)(iFrst(1):iLast(1))
      case ('histogram format') 
        hFmtLen = iLast(1)-iFrst(1)+1
        hFormat(1:hFmtLen) = lines(ii)(iFrst(1):iLast(1))
      case ('observable')
        obsLen = iLast(1)-iFrst(1)+1
        observable(1:obsLen) = lines(ii)(iFrst(1):iLast(1))
    end select
  enddo

! Allocate bins and determine bin size
  allocate(bin(0:2,1:nBins))
  bin = 0
  step = (xRight-xLeft)/nBins

! Read data file
  open(rUnit,file=dataFile(1:dataLen))
  call read_head( rUnit )
  Nev = 0
  do
    call read_event( rUnit ,observable(1:obsLen) ,value,weight ,eof )
    if (eof) exit
    Nev = Nev+1
    if (value.lt.xLeft.or.xRight.le.value) cycle
    ii = 1+int((value-xLeft)/step)
    bin(0,ii) = bin(0,ii) + 1
    bin(1,ii) = bin(1,ii) + weight
    bin(2,ii) = bin(2,ii) + weight*weight
  enddo
  close(rUnit)

! Overall factor for the bins
  factor = factor/step

! Process bins
  do ii=1,nBins
    if (bin(0,ii).le.0) cycle
    bin(1,ii) = bin(1,ii)/Nev
    bin(2,ii) = bin(2,ii)/Nev - bin(1,ii)**2
    if (bin(2,ii).le.0.or.Nev.le.1) then
      bin(2,ii) = abs(bin(1,ii))
    else
      bin(2,ii) = sqrt( bin(2,ii)/(Nev-1) )
    endif
    bin(1:2,ii) = factor*bin(1:2,ii)
  enddo

! Write histogram file
  open(wUnit,file=histFile(1:histLen))
  if (hFormat(1:hFmtLen).eq.'empty-bin') write(wUnit,'(3e16.8)') xLeft,nul,nul
  select case (hFormat(1:hFmtLen))
    case ('middle')
      do ii=1,nBins
        write(wUnit,'(3e16.8)') xLeft+(2*ii-1)*step/2,bin(1:2,ii)
      enddo
    case ('empty-bin')
      do ii=1,nBins
        write(wUnit,'(3e16.8)') xLeft+(ii-1)*step,bin(1:2,ii)
        write(wUnit,'(3e16.8)') xLeft+(ii  )*step,bin(1:2,ii)
      enddo
    case ('full-bin')
      do ii=1,nBins
        write(wUnit,'(3e16.8)') xLeft+(ii-1)*step,nul,nul
        write(wUnit,'(3e16.8)') xLeft+(ii-1)*step,bin(1:2,ii)
        write(wUnit,'(3e16.8)') xLeft+(ii  )*step,bin(1:2,ii)
        write(wUnit,'(3e16.8)') xLeft+(ii  )*step,nul,nul
      enddo
  end select
  if (hFormat(1:hFmtLen).eq.'empty-bin') write(wUnit,'(3e16.8)') xRight,nul,nul
  close(wUnit)
      
end program
