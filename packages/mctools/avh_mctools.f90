module avh_mctools
  !(usekindmod!)
  use avh_random
  use avh_mathcnst
  use avh_iounits
  use avh_iotools
  use avh_prnt
  implicit none
  private
  public :: unweight_type,histogram_type
  public :: unweight,histogram,read_weight,get_sums

  integer,parameter :: nDec=8
  integer,parameter :: maxBatchSize=1024

  type :: unweight_type
    private
    character(72) :: label
    integer :: rUnit,wUnit,Nlist
    !(realknd2!) :: Nnext,Nmess
    !(realknd2!) :: Nzero,W0tot,W1tot,W2tot
    !(realknd2!),allocatable :: Wlist(:)
    logical :: firstWrite
  contains
    procedure ::       init=>unw_init
    procedure ::    prepare=>unw_prepare
    procedure ::    restart=>unw_restart
    procedure ::     accept=>unw_accept
    procedure ::     finish=>unw_finish
    procedure ::    average=>unw_average
    procedure :: prntResult=>unw_prntResult
    procedure ::    message=>unw_message
  end type

  type :: histogram_type
    private
    integer :: wUnit,Nbins,Nev
    !(realknd2!) :: xLeft,xRight,step,factor
    !(realknd2!),allocatable :: bin(:,:),tmp(:)
    character(72) :: fname,frmt
  contains
    procedure :: init=>hst_init
    procedure :: hst_collect_0
    procedure :: hst_collect_1
    procedure :: hst_collect_2
    generic :: collect=>hst_collect_0,hst_collect_1,hst_collect_2
    procedure :: write_file=>hst_write_file
  end type

  interface enlarge
    module procedure enlarge_i1,enlarge_r2
  end interface

  interface read_weight
    module procedure read_weight,read_weight_1
  end interface

  logical,save :: initz=.true.

contains

  subroutine init
  initz = .false.
  call init_mathcnst
  end subroutine


  subroutine unw_init( obj ,readUnit ,writeUnit ,fileName &
                      ,Nlist ,Nmess )
!***********************************************************************
!  obj%message(line) will be true every (i*Nmess)^2 i=1,2,3,... events
!***********************************************************************
  class(unweight_type) :: obj
  integer,intent(in) :: readUnit,writeUnit
  character(*),intent(in) :: fileName 
  integer,optional,intent(in) :: Nlist,Nmess
!
  if (initz) call init
!
  obj%rUnit = readUnit
  obj%wUnit = writeUnit
  obj%label = adjustl(fileName)
  obj%Nlist = 1024 ;if (present(Nlist)) obj%Nlist=Nlist
  obj%Nmess = 10 ;if (present(Nmess)) obj%Nmess = Nmess
  if (allocated(obj%Wlist)) deallocate(obj%Wlist)
  allocate(obj%Wlist(1:obj%Nlist)) ;obj%Wlist=0
  obj%Nzero = 0
  obj%W0tot = 0
  obj%W1tot = 0
  obj%W2tot = 0
  obj%Nnext = obj%Nmess**2
  obj%firstWrite = .true.
!
  open(obj%wUnit,file=trim(obj%label)//'.info',status='new')
  end subroutine

  
  subroutine unw_prepare( obj ,weight )
!***********************************************************************
!***********************************************************************
  class(unweight_type) :: obj
  !(realknd2!),intent(in) :: weight
  if (weight.le.rZRO) then
    obj%Nzero = obj%Nzero+1
    return
  endif
  obj%W0tot = obj%W0tot + 1
  obj%W1tot = obj%W1tot + weight
  obj%W2tot = obj%W2tot + weight*weight
  end subroutine 

 
  subroutine unw_restart( obj )
!***********************************************************************
!***********************************************************************
  class(unweight_type) :: obj
  !(realknd2!) :: ave,sig
!
  obj%Nnext = obj%Nmess**2
  obj%Nzero = 0
  obj%W0tot = 0
  obj%W1tot = 0
  obj%W2tot = 0
  end subroutine 


  function unw_accept( obj ,weight ,writeUnit ,efficiency ) result(rslt)
!***********************************************************************
!***********************************************************************
  class(unweight_type) :: obj
  !(realknd2!),intent(in) :: weight
  !(realknd2!),optional,intent(in) :: efficiency
  integer,optional,intent(out) :: writeUnit
  logical :: rslt
  !(realknd2!) :: xx,Wmax,rho,ave,sig
  integer :: ii,i0,i1
!
  rslt = .false.
  if (present(writeUnit)) writeUnit = obj%wUnit
!
  if (weight.le.rZRO) then
    obj%Nzero = obj%Nzero+1
    return
  endif
  obj%W0tot = obj%W0tot + 1
  obj%W1tot = obj%W1tot + weight
  obj%W2tot = obj%W2tot + weight*weight
!
  if (obj%Wlist(1).lt.weight) then
    if (obj%Wlist(obj%Nlist).lt.weight) then
      obj%Wlist(1:obj%Nlist-1) = obj%Wlist(2:obj%Nlist)
      obj%Wlist(obj%Nlist) = weight
    else
      i0 = 1
      i1 = obj%Nlist
      do while (i1-i0.gt.1)
        ii = (i0+i1)/2
        if (weight.le.obj%Wlist(ii)) then
          i1 = ii
        else
          i0 = ii
        endif
      enddo
      obj%Wlist(1:i0-1) = obj%Wlist(2:i0)
      obj%Wlist(i0) = weight
    endif
  endif
!
  call obj%average(ave,sig)
!
  if (present(efficiency)) then
    Wmax = min(obj%Wlist(obj%Nlist),ave/efficiency)
  else
    Wmax = min(obj%Wlist(obj%Nlist),ave*1024)
  endif
  rslt = (Wmax.le.weight)
  if (rslt) then
    Wmax = weight
  else
    !(rngenerator rho !)
    rslt = (rho*Wmax.le.weight)
  endif
!
  if (rslt) then
    if (obj%firstWrite) then
      obj%firstWrite = .false.
      close(obj%wUnit)
      open(obj%wUnit,file=trim(obj%label),status='new')
    endif
    write(obj%wUnit,'(a18,3e24.16)') 'avh_mctools_weight',Wmax,ave,sig
  endif
  end function

  subroutine read_weight( rUnit ,weight ,eof )
  integer         ,intent(in ) :: rUnit
  !(realknd2!)    ,intent(out) :: weight
  logical         ,intent(out) :: eof
  read(rUnit,'(18x,e24.16,48x)',end=002) weight
  001 eof = .false. ;return
  002 eof = .true.  ;return
  end subroutine
 
  subroutine read_weight_1( rUnit ,weighted )
  integer,intent(in ) :: rUnit
  logical,intent(out) :: weighted
  character(maxLineLen) :: line
  read(rUnit,'(A)') line
  weighted = (line(1:18).eq.'avh_mctools_weight') 
  end subroutine
 
 
  subroutine unw_finish( obj )
!***********************************************************************
!***********************************************************************
  class(unweight_type) :: obj
  character(maxLineLen),allocatable :: lines(:)
  !(realknd2!) :: ave,sig,Wsum,eps,epsMax
  integer :: ii,jj,Nlines
!
  close(obj%wUnit)
!
  call obj%average( ave,sig )
  epsMax = sig/ave
!
  open(obj%wUnit,file=trim(obj%label)//'.info',status='old',position='append')
  write(obj%wUnit,'(a21,e24.16,a4,e23.16,f8.4,a1)') &
    'avh_mctools average =',ave,' +/-',sig,100*sig/ave,'%'
  write(obj%wUnit,'(a31,f13.0,a8,f13.0)') &
    'avh_mctools Nevents = non-zero:',obj%W0tot,'  total:',obj%W0tot+obj%Nzero
  write(obj%wUnit,'(a18,3e23.16)') &
    'avh_mctools sums =',obj%W0tot+obj%Nzero ,obj%W1tot ,obj%W2tot
  Wsum = 0
  do ii=obj%Nlist,1,-1
    jj = obj%Nlist-ii
    eps = ( Wsum - jj*obj%Wlist(ii) )/obj%W1tot
    if (eps.gt.epsMax) exit
    Wsum = Wsum + obj%Wlist(ii)
    write(obj%wUnit,'(a18,i5,2e23.16)') &
      'avh_mctools tail =',jj+1,obj%Wlist(ii),eps
  enddo
  close(obj%wUnit)
  end subroutine


  subroutine get_sums( readUnit ,s0,s1,s2 )
  integer     ,intent(in ) :: readUnit
  !(realknd2!),intent(out) :: s0,s1,s2
  character(maxLineLen),allocatable :: lines(:)
  integer :: ii
  call read_lines( readUnit ,lines ,maxLineLen )
  do ii=1,size(lines,1)
    if (lines(ii)(1:18).eq.'avh_mctools sums =') read(lines(ii)(19:87),*) s0,s1,s2
  enddo
  end subroutine


  subroutine unw_average( obj ,ave ,sig )
!***********************************************************************
!***********************************************************************
  class(unweight_type) :: obj
  !(realknd2!),intent(out) :: ave,sig
  !(realknd2!) :: Ntot
  Ntot = obj%W0tot + obj%Nzero
  if (Ntot.le.rZRO.or.obj%W1tot.le.rZRO) then
    ave = 0
    sig = 0
  elseif (Ntot.le.rONE) then
    ave = obj%W1tot
    sig = ave
  else
    ave = obj%W1tot/Ntot
    sig = obj%W2tot/Ntot - ave*ave
    if (sig.le.rZRO) then
      sig = ave
    else
      sig = sqrt( sig/(Ntot-1) )
    endif
  endif
  end subroutine

  function unw_relErr( obj ) result(rslt)
!***********************************************************************
!***********************************************************************
  class(unweight_type) :: obj
  !(realknd2!) :: rslt ,ave,sig,Ntot
  Ntot = obj%W0tot + obj%Nzero
  if (Ntot.le.rONE.or.obj%W1tot.le.rZRO) then
    rslt = 1
  else
    ave = obj%W1tot/Ntot
    sig = obj%W2tot/Ntot - ave*ave
    if (sig.le.rZRO) then
      rslt = 1
    else
      rslt = sqrt( sig/(Ntot-1) )/ave
      rslt = max(rslt,rONE)
    endif
  endif
  end function

 
  function unw_message( obj ,line ) result(rslt)
!***********************************************************************
!***********************************************************************
  class(unweight_type) :: obj
!  character(44+2*nDec),intent(out) :: line
  character(*),intent(out) :: line
  logical :: rslt 
!
  rslt = .false.
  if (obj%W0tot.lt.obj%Nnext) return
  rslt = .true.
  obj%Nnext = (sqrt(obj%Nnext)+obj%Nmess)**2
!
  line = unw_prntResult( obj )
  end function

  function unw_prntResult( obj ) result(line)
!***********************************************************************
!***********************************************************************
  class(unweight_type) :: obj
  character(44+2*nDec) :: line
  integer :: i1,i2
  !(realknd2!) :: xx,yy,tt
  character(44+2*nDec) :: aa,bb,cc,ee
!
  call obj%average( xx,yy )
  write(aa,'(e23.16)') xx
  ee(1:2) = '1.'
  ee(3:6) = aa(20:23)
  read(ee(1:6),*) tt
  write(bb,'(f18.16)') abs(yy)/tt
  write(cc,'(f15.12)') abs(yy/xx*100)
  line = '('//aa(1:1)//aa(3:3+nDec)//'+/-'//bb(2:2+nDec)//')'//ee(3:6) &
       //' '//cc(1:6)//'%'
  write(aa,'(f13.0)') obj%W0tot
  write(bb,'(f13.0)') obj%W0tot+obj%Nzero
  if (aa(9:9).ne.' ') aa(1:9) = aa(2:9)//','
  if (aa(5:5).ne.' ') aa(1:5) = aa(2:5)//','
  if (bb(9:9).ne.' ') bb(1:9) = bb(2:9)//','
  if (bb(5:5).ne.' ') bb(1:5) = bb(2:5)//','
  line = trim(line)//aa(1:12)//bb(1:12)
  end function


  subroutine unweight_h( rUnit ,wUnit ,wMax ,Nacc )
!***********************************************************************
!***********************************************************************
  integer,intent(in) :: rUnit ,wUnit
  !(realknd2!),intent(in) :: wMax
  integer,intent(out) :: Nacc
  character(maxLineLen),allocatable :: lines(:),tmp(:)
  !(realknd2!) :: rho,weight,nextWeight
  integer :: jj,kk,Nlines
!
  allocate(lines(1))
  read(rUnit,'(A)') lines(1)
  if (lines(1)(1:18).ne.'avh_mctools_weight') then
    write(errru,*) 'ERROR in avh_mctools unweight: ' &
      ,'this does not seem to be an unweightable file.'
    return
  endif
  read(lines(1)(19:42),*) weight
!
  Nacc = 0
  Nlines = 0
  do
    jj = 0
    do ;jj=jj+1
      if (jj.gt.Nlines) then
        allocate(tmp(Nlines))
        tmp = lines
        deallocate(lines)
        allocate(lines(jj))
        lines(1:Nlines) = tmp(1:Nlines)
        Nlines = jj
        deallocate(tmp)
      endif
      read(rUnit,'(A)',end=111) lines(jj)
      if (lines(jj)(1:18).eq.'avh_mctools_weight') then
        read(lines(jj)(19:maxLineLen),*) nextWeight
        exit
      endif
    enddo
    if (wMax.le.weight) then
      Nacc = Nacc+1
      do kk=1,jj-1
        write(wUnit,'(A)') trim(lines(kk))
      enddo
    else
      !(rngenerator rho !)
      if (rho*wMax.le.weight) then
        Nacc = Nacc+1
        do kk=1,jj-1
          write(wUnit,'(A)') trim(lines(kk))
        enddo
      endif
    endif
    weight = nextWeight
  enddo
  111 continue
!
  end subroutine


  subroutine unweight( readUnit ,writeUnit ,inFiles ,outFile )
!***********************************************************************
! inFiles is a single character string that may contain several
! space-separated file names.
!***********************************************************************
  integer,intent(in) :: readUnit,writeUnit
  character(*),intent(in) :: inFiles,outFile
  type(charLine_type),allocatable :: lines(:)
  type(charLine_type) :: input
  character(maxLineLen) :: line
  !(realknd2!),allocatable :: s0(:),s1(:),s2(:)
  !(realknd2!),allocatable :: Wlist(:,:),eList(:,:)
  integer,allocatable :: Nlist(:)
  integer :: ii,Nfiles,jj,kk,Nacc,NaccTot
  !(realknd2!) :: ave,sig,Wmax,Wtmp,eTmp,s0tot,s1tot,s2tot
  input = put_charLine( inFiles )
  Nfiles = input%Nwords()
  allocate(s0(Nfiles),s1(Nfiles),s2(Nfiles),Nlist(Nfiles))
  Nlist = 0
  do ii=1,Nfiles
    open(readUnit,file=input%word(ii)//'.info',status='old')
    call read_charLines( readUnit ,lines )
    do jj=1,ubound(lines,1)
      if (lines(jj)%Nwords().le.1) cycle
      select case (lines(jj)%word(1,2))
        case ('avh_mctools sums')
          line=lines(jj)%word(4,6) ;read(line,*) s0(ii),s1(ii),s2(ii)
        case ('avh_mctools tail')
          line=lines(jj)%word(4,6) ;read(line,*) kk,Wtmp,eTmp
          call enlarge( Wlist, 1,kk ,1,Nfiles )
          call enlarge( eList, 1,kk ,1,Nfiles )
          Wlist(kk,ii) = Wtmp
          eList(kk,ii) = eTmp
          if (Nlist(ii).lt.kk) Nlist(ii) = kk
      end select
    enddo
    close(readUnit)
  enddo
  s0tot = sum(s0(1:Nfiles))
  s1tot = sum(s1(1:Nfiles))
  s2tot = sum(s2(1:Nfiles))
  ave = s1tot/s0tot
  sig = sqrt( (s2tot/s0tot-ave*ave)/(s0tot-1) )
  NaccTot = 0
  open(writeUnit,file=trim(outFile),status='new')
  do ii=1,Nfiles
    do kk=1,Nlist(ii)
      if (eList(kk,ii).gt.sig/ave) exit
    enddo
    if (messu.gt.0) write(messu,*) 'Including events from ',input%word(ii)
    if (messu.gt.0) write(messu,*) '    with wMax(',prnt(kk-1,2),')=',prnt(Wlist(kk-1,ii),8)
    open(readUnit,file=input%word(ii),status='old')
    call unweight_h( readUnit ,writeUnit ,Wlist(kk-1,ii) ,Nacc )
    if (messu.gt.0) write(messu,*) '    accepted',Nacc,' events'
    NaccTot = NaccTot + Nacc
    close(readUnit)
  enddo
  if (messu.gt.0) write(messu,*) 'Accepted',NaccTot,' events in total'
  close(writeUnit)
  open(writeUnit,file=trim(outFile)//'.info',status='new')
    write(writeUnit,'(a21,i12)') &
      'avh_mctools Nevents =',NaccTot
    write(writeUnit,'(a21,e24.16,a4,e23.16,f8.4,a1)') &
      'avh_mctools average =',ave,' +/-',sig,100*sig/ave,'%'
    write(writeUnit,'(a18,3e23.16)') &
      'avh_mctools sums =',s0tot,s1tot,s2tot
    write(writeUnit,'(A)') '#'
    write(writeUnit,'(A)') '# Below the content of the .info files coming with ' &
                           //'the event files from'
    write(writeUnit,'(A)') '# which '//trim(outFile)//' was created.'
    write(writeUnit,'(A)') '#'
    do ii=1,Nfiles
      open(readUnit,file=input%word(ii)//'.info',status='old')
      call read_charLines( readUnit ,lines )
      write(writeUnit,'(A)') '# '//input%word(ii)//'.info'
      do jj=1,ubound(lines,1)
        if (lines(jj)%word(1,1,11).eq.'avh_mctools') cycle
        write(writeUnit,'(A)') lines(jj)%prnt()
      enddo
    enddo
  close(writeUnit)
  end subroutine


  subroutine hst_init( obj ,fileName &
     ,leftBorder ,rightBorder ,numberOfBins ,overallFactor ,histFormat )
!***********************************************************************
! use avh_mctools
! type(histogram_type) :: hist
! call hist%init( fileName='histout'  &
!              ,leftBorder=0d0  &
!             ,rightBorder=1d0  &
!            ,numberOfBins=32  &
!           ,overallFactor=1d0  &
!              ,histFormat='left-right'  &
! )
! do iev=1,nev
!   call get_your_data_point( value,weight )
!   call hist%collect( value,weight )
! enddo
! call hist%write_file
!***********************************************************************
  class(histogram_type) :: obj
  character(*),intent(in) :: fileName
  !(realknd2!),optional,intent(in) :: leftBorder,rightBorder,overallFactor
  integer     ,optional,intent(in) :: numberOfBins
  character(*),optional,intent(in) :: histFormat
!
  if (initz) call init
!
  obj%fname = adjustl(fileName)
   obj%xLeft = 0  ;if (present(leftBorder   ))  obj%xLeft = leftBorder    
  obj%xRight = 1  ;if (present(rightBorder  )) obj%xRight = rightBorder   
   obj%Nbins = 32 ;if (present(numberOfBins ))  obj%Nbins = numberOfBins  
  obj%factor = 1  ;if (present(overallFactor)) obj%factor = overallFactor 
    obj%frmt = 'left-right'
                   if (present(histFormat)) obj%frmt = adjustl(histFormat)
  if (allocated(obj%bin)) deallocate(obj%bin)
  if (allocated(obj%tmp)) deallocate(obj%tmp)
  allocate(obj%bin(1:2,1:obj%Nbins),obj%tmp(1:obj%Nbins))
  obj%bin = 0
  obj%step = (obj%xRight-obj%xLeft)/obj%Nbins
  obj%Nev = 0
  end subroutine

  subroutine hst_collect_0( obj ,val )
!***********************************************************************
!***********************************************************************
  class(histogram_type) :: obj
  !(realknd2!),intent(in) :: val
  integer :: ii
  obj%Nev = obj%Nev+1
  if (val.lt.obj%xLeft.or.obj%xRight.le.val) return
  ii = 1+int((val-obj%xLeft)/obj%step)
  obj%bin(1,ii) = obj%bin(1,ii) + 1
  obj%bin(2,ii) = obj%bin(2,ii) + 1
  end subroutine

  subroutine hst_collect_1( obj ,val,weight )
!***********************************************************************
!***********************************************************************
  class(histogram_type) :: obj
  !(realknd2!),intent(in) :: val,weight
  integer :: ii
  obj%Nev = obj%Nev+1
  if (val.lt.obj%xLeft.or.obj%xRight.le.val) return
  ii = 1+int((val-obj%xLeft)/obj%step)
  obj%bin(1,ii) = obj%bin(1,ii) + weight
  obj%bin(2,ii) = obj%bin(2,ii) + weight*weight
  end subroutine

  subroutine hst_collect_2( obj ,values,weights )
!***********************************************************************
!***********************************************************************
  class(histogram_type) :: obj
  !(realknd2!),intent(in) :: values(:),weights(:)
  integer :: ii,batchSize,jj
  obj%Nev = obj%Nev+1
  obj%tmp = 0
  batchSize = size(values,1)
  if (batchSize.ne.size(weights,1)) then
    if (errru.ge.0) write(errru,*) 'ERROR in histogram collect: ' &
      ,'array sizes of  values  and  weights  are not equal'
    stop
  endif
  do ii=1,batchSize
    if (values(ii).lt.obj%xLeft.or.obj%xRight.le.values(ii)) cycle
    jj = 1+int((values(ii)-obj%xLeft)/obj%step)
    obj%tmp(jj) = obj%tmp(jj) + weights(ii)
  enddo
  do ii=1,obj%nBins
    obj%bin(1,ii) = obj%bin(1,ii) + obj%tmp(ii)
    obj%bin(2,ii) = obj%bin(2,ii) + obj%tmp(ii)*obj%tmp(ii)
  enddo
  end subroutine
  
  subroutine hst_write_file( obj ,writeUnit )
!***********************************************************************
!***********************************************************************
  class(histogram_type) :: obj
  integer,intent(in) :: writeUnit
  !(realknd2!) :: factor
  integer :: ii,wUnit
!
  wUnit = writeUnit
!
  factor = obj%factor/obj%step
!
  do ii=1,obj%Nbins
    obj%bin(1,ii) = obj%bin(1,ii)/obj%Nev
    obj%bin(2,ii) = obj%bin(2,ii)/obj%Nev - obj%bin(1,ii)**2
    if (obj%bin(2,ii).le.0.or.obj%Nev.le.1) then
      obj%bin(2,ii) = abs( obj%bin(1,ii) )
    else
      obj%bin(2,ii) = sqrt( obj%bin(2,ii)/(obj%Nev-1) )
    endif
    obj%bin(1:2,ii) = factor*obj%bin(1:2,ii)
  enddo
!
  open(wUnit,file=trim(obj%fname),status='replace')
!
  select case (trim(obj%frmt))
    case ('left-right')
      do ii=1,obj%Nbins
      write(wUnit,'(4e16.8)') obj%xLeft+(ii-1)*obj%step,obj%xLeft+ii*obj%step,obj%bin(1:2,ii)
      enddo
    case ('middle')
      do ii=1,obj%Nbins
      write(wUnit,'(3e16.8)') obj%xLeft+(2*ii-1)*obj%step/2,obj%bin(1:2,ii)
      enddo
    case ('empty-bin')
      write(wUnit,'(3e16.8)') obj%xLeft,rZRO,rZRO
      do ii=1,obj%Nbins
      write(wUnit,'(3e16.8)') obj%xLeft+(ii-1)*obj%step,obj%bin(1:2,ii)
      write(wUnit,'(3e16.8)') obj%xLeft+(ii  )*obj%step,obj%bin(1:2,ii)
      enddo
      write(wUnit,'(3e16.8)') obj%xRight,rZRO,rZRO
    case ('full-bin')
      do ii=1,obj%Nbins
      write(wUnit,'(3e16.8)') obj%xLeft+(ii-1)*obj%step,rZRO,rZRO
      write(wUnit,'(3e16.8)') obj%xLeft+(ii-1)*obj%step,obj%bin(1:2,ii)
      write(wUnit,'(3e16.8)') obj%xLeft+(ii  )*obj%step,obj%bin(1:2,ii)
      write(wUnit,'(3e16.8)') obj%xLeft+(ii  )*obj%step,rZRO,rZRO
      enddo
  end select
!
  close(wUnit)
!
  end subroutine


  subroutine histogram( read_event, observable ,readUnit ,histUnit &
     ,leftBorder ,rightBorder ,numberOfBins ,overallFactor ,histFormat )
!***********************************************************************
!
! subroutine read_event( observable ,readUnit ,batchSize,values,weights ,eof )
!   character(*)   ,intent(in ) :: observable
!   integer        ,intent(in ) :: readUnit(:)
!   integer        ,intent(out) :: batchSize
!   real(kind(1d0)),intent(out) :: values(:),weights(:)
!   logical        ,intent(out) :: eof
!   batchSize = 1
!   read(readUnit(1),*,end=999) data
!   read(readUnit(2),*,end=999) weight(1)
!   select case (observable)
!     case ('obs-1')
!       values(1) = function1(data)
!     case ('obs-2')
!       values(1) = function2(data)
!   end select
!   eof = .false.
!   return
!   999 eof = .true.
! end subroutine
!
!***********************************************************************
  character(*),intent(in) :: observable
  integer     ,intent(in) :: readUnit(:),histUnit
  !(realknd2!),optional,intent(in) :: leftBorder,rightBorder,overallFactor
  integer     ,optional,intent(in) :: numberOfBins
  character(*),optional,intent(in) :: histFormat
!
  !(realknd2!),allocatable :: bin(:,:),tmp(:)
  integer,allocatable :: iBin(:)
  !(realknd2!) :: xLeft,xRight,factor,step
  !(realknd2!) :: values(maxBatchSize),weights(maxBatchSize)
  character(24) :: hFormat
  integer :: Nbins,wUnit,ii,Nev,batchSize,jj
  logical :: eof
!
  interface
    subroutine read_event( observable ,readUnit ,batchSize,values,weights ,eof )
    character(*),intent(in ) :: observable
    integer     ,intent(in ) :: readUnit(:)
    integer     ,intent(out) :: batchSize
    !(realknd2!),intent(out) :: values(:),weights(:)
    logical     ,intent(out) :: eof
    end subroutine
  end interface
!
  if (initz) call init
!
  wUnit = histUnit
    xLeft = 0  ;if (present(leftBorder   ))  xLeft = leftBorder    
   xRight = 1  ;if (present(rightBorder  )) xRight = rightBorder   
    Nbins = 32 ;if (present(numberOfBins ))  Nbins = numberOfBins  
   factor = 1  ;if (present(overallFactor)) factor = overallFactor 
  hFormat = 'empty-bin'
                if (present(histFormat   )) hFormat = histFormat
!
! Allocate bins and determine bin size
  allocate(bin(1:2,1:nBins),tmp(1:nBins))
  bin = 0
  step = (xRight-xLeft)/nBins

! Read data file
  if (messu.gt.0) write(messu,*) 'MESSAGE from avh_mctools histogram ' &
    ,'for ',trim(observable),': reading data file'
  Nev = 0
  do
    call read_event( observable ,readUnit ,batchSize,values,weights ,eof )
    if (eof) exit
    Nev = Nev+1
    tmp = 0
    do ii=1,batchSize
      if (values(ii).lt.xLeft.or.xRight.le.values(ii)) cycle
      jj = 1+int((values(ii)-xLeft)/step)
      tmp(jj) = tmp(jj) + weights(ii)
    enddo
    do ii=1,nBins
      bin(1,ii) = bin(1,ii) + tmp(ii)
      bin(2,ii) = bin(2,ii) + tmp(ii)*tmp(ii)
    enddo
  enddo

! Overall factor for the bins
  factor = factor/step

! Process bins
  do ii=1,nBins
    bin(1,ii) = bin(1,ii)/Nev
    bin(2,ii) = bin(2,ii)/Nev - bin(1,ii)**2
    if (bin(2,ii).le.0.or.Nev.le.1) then
      bin(2,ii) = abs(bin(1,ii))
    else
      bin(2,ii) = sqrt( bin(2,ii)/(Nev-1) )
    endif
    bin(1:2,ii) = factor*bin(1:2,ii)
  enddo

! Write histogram file
  if (messu.gt.0) write(messu,*) 'MESSAGE from avh_mctools histogram ' &
    ,'for ',trim(observable),': writing histogram file'
  select case (trim(hFormat))
    case ('left-right')
      do ii=1,nBins
      write(wUnit,'(4e16.8)') xLeft+(ii-1)*step,xLeft+ii*step,bin(1:2,ii)
      enddo
    case ('middle')
      do ii=1,nBins
      write(wUnit,'(3e16.8)') xLeft+(2*ii-1)*step/2,bin(1:2,ii)
      enddo
    case ('empty-bin')
      write(wUnit,'(3e16.8)') xLeft,rZRO,rZRO
      do ii=1,nBins
      write(wUnit,'(3e16.8)') xLeft+(ii-1)*step,bin(1:2,ii)
      write(wUnit,'(3e16.8)') xLeft+(ii  )*step,bin(1:2,ii)
      enddo
      write(wUnit,'(3e16.8)') xRight,rZRO,rZRO
    case ('empty-bin-2')
      do ii=1,nBins
      write(wUnit,'(3e16.8)') xLeft+(ii-1)*step,bin(1,ii)-bin(2,ii),bin(1,ii)+bin(2,ii)
      write(wUnit,'(3e16.8)') xLeft+(ii  )*step,bin(1,ii)-bin(2,ii),bin(1,ii)+bin(2,ii)
      enddo
    case ('full-bin')
      do ii=1,nBins
      write(wUnit,'(3e16.8)') xLeft+(ii-1)*step,rZRO,rZRO
      write(wUnit,'(3e16.8)') xLeft+(ii-1)*step,bin(1:2,ii)
      write(wUnit,'(3e16.8)') xLeft+(ii  )*step,bin(1:2,ii)
      write(wUnit,'(3e16.8)') xLeft+(ii  )*step,rZRO,rZRO
      enddo
  end select
  if (messu.gt.0) write(messu,*) 'MESSAGE from avh_mctools histogram ' &
    ,'for ',trim(observable),': finished'
  end subroutine


!  subroutine parse_file( readUnit ,parser ,comment ,separator )
!!***********************************************************************
!! To parse a file like
!!
!!
!!# this is a comment in the file to be parsed
!!   something = 1e0
!! something else = yes 3
!!# another comment
!!
!!
!! you should put  comment='#'  separator='='
!! and the external routine  parser  should look as follows:
!!
!! subroutine parser( keyword ,values )
!!   character(*),intent(in) :: keyword
!!   character(*),intent(in) :: values(:)
!!   select case (keyword)
!!     case ('something')
!!       read(values(1),*) aRealVariable
!!     case ('something else')
!!       read(values(1),*) aCharacterVariable
!!       read(values(2),*) anIntegerVariable
!!   end select
!! end subroutine
!!
!!***********************************************************************
!  integer,intent(in)      :: readUnit
!  external                :: parser
!  character(*),intent(in) :: comment,separator
!  character(maxLineLen),allocatable :: lines(:)
!  character(maxLineLen) :: keyword,values(0:maxLineLen)
!  integer :: ii,frst(0:maxLineLen),last(0:maxLineLen),nWords,jj
!  interface
!    subroutine parser( keyword ,values )
!    character(*),intent(in) :: keyword
!    character(*),intent(in) :: values(:)
!    end subroutine
!  end interface
!  call read_lines( readUnit ,lines ,maxLineLen )
!  do ii=1,ubound(lines,1)
!    call extract_values( frst,last,nWords ,lines(ii) ,separator,comment )
!    if (last(0).le.0) cycle
!    if (last(1).le.0) cycle
!    keyword(1:last(0)-frst(0)+1) = lines(ii)(frst(0):last(0))
!    do jj=1,nWords
!      values(jj)(1:last(jj)-frst(jj)+1) = lines(ii)(frst(jj):last(jj))
!    enddo
!    call parser( keyword ,values )
!  enddo
!  end subroutine
! 
! 
!  subroutine read_lines( rUnit ,lines ,lineLen )
!!***********************************************************************
!!***********************************************************************
!  integer,intent(in) :: lineLen,rUnit
!  character(lineLen),allocatable,intent(out) :: lines(:)
!  character(lineLen),allocatable :: tmp(:)
!  integer :: ii,Nline
!  ii = 16
!  allocate(tmp(1:ii))
!  Nline = 0
!  do ;Nline=Nline+1
!    if (Nline.gt.ii) then
!      allocate(lines(1:ii))
!      lines(1:ii) = tmp(1:ii)
!      deallocate(tmp)
!      allocate(tmp(1:2*ii))
!      tmp(1:ii) = lines(1:ii)
!      ii = 2*ii
!      deallocate(lines)
!    endif
!    read(rUnit,'(A)',end=111) tmp(Nline)
!  enddo
!  111 continue
!  Nline = Nline-1
!  allocate(lines(1:Nline))
!  lines(1:Nline) = tmp(1:Nline)
!  deallocate(tmp)
!  end subroutine
! 
!
!  subroutine get_keyword( readUnit ,keyword ,values,Nwords ,separate,comment )
!  integer     ,intent(in ) :: readUnit
!  character(*),intent(in ) :: keyword,separate,comment
!  character(*),intent(out) :: values(:)
!  integer     ,intent(out) :: Nwords
!  character(maxLineLen),allocatable :: lines(:)
!  integer :: frst(0:maxLineLen),last(0:maxLineLen),nWords
!  integer :: ii
!  call read_lines( readUnit ,lines ,maxLineLen )
!  do ii=1,size(lines,1)
!    call extract_values( frst,last,nWords ,lines(ii) ,separate,comment )
!      if (last(0).le.0) cycle
!      if (last(1).le.0) cycle
!      if (lines(ii)(frst(0):last(0)).eq.keyword) then
!        values = lines(ii)(frst(1):last(nWords))
!      endif
!  enddo
!  end subroutine
!  
!
!  subroutine extract_values( frst,last,nWords,line,separate,comment )
!!***********************************************************************
!!***********************************************************************
!  character(*),intent(in)  :: line,separate,comment
!  integer     ,intent(out) :: frst(0:),last(0:)
!  integer     ,intent(out) :: nWords
!  character(1) :: frstChar
!  integer :: pos,lineLen
!  frst(0) = 0
!  last(0) = 0
!  frst(1) = 0
!  last(1) = 0
!  frstChar = adjustl(line)
!  if (verify(frstChar,comment).eq.0) return
!  lineLen = len(line)
!  pos = scan(line,separate)
!  if (pos.le.0) return
!  last(0) = len_trim(line(1:pos-1))
!  frst(0) = last(0) - len_trim(adjustl(line(1:pos-1))) + 1
!  call extract_words( frst(1:),last(1:),nWords,line(pos+1:lineLen),' ' )
!  frst(1:nWords) = frst(1:nWords) + pos
!  last(1:nWords) = last(1:nWords) + pos
!  end subroutine
!
!  subroutine extract_words( frst,last,nWords ,line ,separate )
!!**********************************************************************
!! Extract all words from <line> separated by any (and any number) of
!! the characters in <separate> . The result is given as the positions
!! of the first and the last character of each word.
!! Example:
!!   character(72)   :: line,quantity,eUnit
!!   real(kind(1d0)) :: value
!!   integer         :: frst(72),last(72),nWords
!!   line = " mass: 173d0 GeV "
!!   call extract( frst,last,nWords ,line ," :" )
!!   read(line(frst(1):last(1)),*) quantity
!!   read(line(frst(2):last(2)),*) value
!!   read(line(frst(3):last(3)),*) eUnit
!!   write(*,'(A)') quantity
!!   write(*,'(e15.8)') value
!!   write(*,'(A)') eUnit 
!! will write the lines
!!   mass
!!    0.17300000E+03
!!   GeV
!!**********************************************************************
!  character(*),intent(in)  :: line,separate 
!  integer     ,intent(out) :: frst(:),last(:)
!  integer     ,intent(out) :: nWords
!  integer :: ii,trimLen
!  nWords = 0
!  if (line.eq.'') return
!  trimLen = len_trim(line)
!  ii = 0
!  do ;if (ii.gt.trimLen) exit
!    do ;ii=ii+1 ;if (ii.gt.trimLen) exit
!      if (scan(separate,line(ii:ii)).eq.0) then
!        nWords = nWords+1
!        frst(nWords) = ii
!        exit
!      endif
!    enddo
!    last(nWords) = ii
!    do ;ii=ii+1 ;if (ii.gt.trimLen) exit
!      if (verify(line(ii:ii),separate).eq.0) exit
!      last(nWords) = ii
!    enddo
!  enddo
!  end subroutine
!
!
!  function date_time() result(rslt)
!  character(12) :: rslt,date,time
!  call date_and_time(date=date,time=time)
!  rslt = date(3:8)//time(1:6)
!  end function
!
!
!  subroutine command_arguments(args)
!  character(*),allocatable,intent(out) :: args(:)
!  character(maxLineLen) :: arg
!  integer :: ii,Nargs
!  Nargs = command_argument_count()
!  allocate(args(1:Nargs))
!  do ii=1,Nargs
!    call get_command_argument(ii, arg)
!    args(ii) = adjustl(arg)
!  enddo
!  end subroutine


  subroutine enlarge_i1( xx ,l1,u1 )
  integer &
  include '../arrays/enlarge1.h90'
  end subroutine

  subroutine enlarge_r2( xx ,l1,u1 ,l2,u2 )
  !(realknd2!) &
  include '../arrays/enlarge2.h90'
  end subroutine

end module


