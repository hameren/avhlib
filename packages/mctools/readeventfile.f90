module readeventfile
  implicit none

!  integer,save :: nMomenta

contains

  subroutine read_head( rUnit )
  integer,intent(in) :: rUnit
! read(rUnit,*) nMomenta
  end subroutine

  subroutine read_event( rUnit ,observable ,value,weight ,eof )
  integer        ,intent(in ) :: rUnit
  character(*)   ,intent(in ) :: observable
  real(kind(1d0)),intent(out) :: value,weight
  logical        ,intent(out) :: eof
!  read(rUnit,*,end=111) weight
!  do ii=1,nMomenta
!    read(rUnit,*,end=111) pp(0:3,ii)
!  enddo
!  select case (observable)
!    case ('pT3')
!      value = sqrt(pp(1,3)**2 + pp(2,3)**2)
!    case ('rapidity4')
!      value = log( (pp(0,4)-pp(3,4))/(pp(0,4)+pp(3,4)) )/2
!  end select
!
!  eof = .false.
!  return
!  111 continue
!  eof = .true.
!  return
  end subroutine

end module
