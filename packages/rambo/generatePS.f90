program generatePS
  use avh_prnt
  use rambo_module
  implicit none
  type(rambo_type) :: obj
  integer :: Nev,Iev,ii,Nfinst
  real(kind(1d0)) :: pp(0:3,-2:17),hh(0:3)
  logical :: discard

  Nev = 5
  Nfinst = 2
  call rambo_init( obj ,Nfinst ,option='inin' )

  pp(0:3,-2) = (/-0.5d0,-0.11d0, 0.12d0,-0.5d0/)
  pp(0:3,-1) = (/-0.5d0, 0.09d0, 0.08d0, 0.5d0/)

  do Iev=1,Nev
    call rambo_gnrt( obj ,discard ,pp )
    if (discard) cycle

    write(*,'(A)') 'next'
    write(*,*) prnt(2+Nfinst,2)
    do ii=Nfinst,-2,-1 ;if(ii.eq.0)cycle
      write(*,*) prnt(pp(0:3,ii)),' ',prnt(ii,2)
    enddo

  enddo

end program
