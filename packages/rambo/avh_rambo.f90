module rambo_module
  !(usekindmod!)
  use avh_random

  private
  public :: rambo_type,rambo_init,rambo_gnrt,rambo_wght

  integer ,parameter :: nmax=17

  type :: rambo_type
    private
    !(realknd2!) :: w=0
    !(realknd2!) :: m(-2:nmax)=0,summ=0
    !(realknd2!) :: e=0
    integer         :: n=0
    integer         :: iw(5)=0
    logical         :: pdf=.false.
    logical         :: inin=.false.
  end type
    

contains


  subroutine rambo_init( obj ,nfinst ,ecm ,masses ,option )
  implicit none
  intent(inout) :: obj
  intent(in   ) :: nfinst,ecm,option,masses
  optional :: ecm,option,masses
  type(rambo_type) :: obj
  integer          :: nfinst
  character(*)     :: option
  !(realknd2!)  :: ecm,masses(-2:)
  integer :: ii
  logical :: firstcall=.true.
  if (firstcall) then
    firstcall = .false.
    write(*,'(a72)') '########################################################################'
    write(*,'(a72)') '#                                                                      #'
    write(*,'(a72)') '#                         You are using RAMBO                          #'
    write(*,'(a72)') '#                                                                      #'
    write(*,'(a72)') '#                     for phase space generation                       #'
    write(*,'(a72)') '#                                                                      #'
    write(*,'(a72)') '# AUTHORS:  S.D. ELLIS,  R. KLEISS,  W.J. STIRLING                     #'
    write(*,'(a72)') '# THIS IS VERSION 1.0 -  WRITTEN BY R. KLEISS                          #'
    write(*,'(a72)') '# Wrapped by A. van Hameren                                            #'
    write(*,'(a72)') '#                                                                      #'
    write(*,'(a72)') '########################################################################'
  endif
  obj%n = nfinst
  obj%e = 1
    if (present(ecm)) obj%e=ecm
  obj%m = 0
  obj%summ = 0
    if (present(masses)) then
      do ii=-2,nfinst
        if (ii.eq.0) cycle
        obj%m(ii) = masses(ii)
      enddo
    endif
    obj%summ = sum(obj%m(1:obj%n))
  obj%pdf = .false.
    if (present(option)) obj%pdf=(option(1:3).eq.'pdf')
  obj%inin = .false.
    if (present(option)) obj%inin=(option.eq.'inin')
  end subroutine
!  
!
  subroutine rambo_gnrt( obj, discard ,pkaleu ,x1,x2 )
  implicit none
  type(rambo_type) ,intent(inout) :: obj
  logical         ,intent(out) :: discard
  !(realknd2!) ,intent(out) :: pkaleu(0:3,-2:17)
  !(realknd2!) ,intent(out),optional :: x1,x2
  !(realknd2!) &
    :: ehat,stot,s1,s2,wx1x2,pp(4,nmax),hh(4),aa,bb
  integer :: ii
  !(realknd2!) :: rho(2)
  discard = .true.
  ehat = obj%e
  if (obj%inin) then
    hh(4)   =-pkaleu(0  ,-1)-pkaleu(0  ,-2)
    hh(1:3) =-pkaleu(1:3,-1)-pkaleu(1:3,-2)
    ehat = (hh(4)+hh(3))*(hh(4)-hh(3)) - hh(1)*hh(1) - hh(2)*hh(2)
    if (ehat.le.0) then
      write(*,*) 'ERROR in rambo: squared energy too small'
      stop
    endif
    ehat = sqrt(ehat)
    call rambo( discard ,obj%n ,ehat ,obj%m(1:nmax) ,pp ,obj%w ,obj%iw )
    if (discard) return
    do ii=1,obj%n
      aa = hh(4)*pp(4,ii)+hh(1)*pp(1,ii)+hh(2)*pp(2,ii)+hh(3)*pp(3,ii)
      aa = aa/ehat
      bb = (pp(4,ii)+aa)/(hh(4)+ehat)
      pkaleu(0  ,ii) = aa
      pkaleu(1:3,ii) = pp(1:3,ii)+hh(1:3)*bb
    enddo
  else
    wx1x2 = 1
    if (present(x1).or.present(x2)) then
      if (.not.present(x1).or..not.present(x2)) then
        write(*,*) 'ERROR in rambo: x1,x2 must both be present or not'
      endif
      if (obj%pdf) then
        call rangen(rho,2); x1=rho(1); x2=rho(2)
        wx1x2 = 1/(x1*x2)
        ehat = sqrt(x1*x2)*obj%e
        if (ehat.le.obj%summ) return
      else
        x1=1 ;x2=1
      endif
    endif
    stot = ehat*ehat
    s1 = obj%m(-1)**2
    s2 = obj%m(-2)**2
    pkaleu(0,-1) = -( stot + s1 - s2 ) / (2*ehat)
    pkaleu(3,-1) = -sqrt( pkaleu(0,-1)**2 - s1 )
    pkaleu(2,-1) =  0
    pkaleu(1,-1) =  0
    pkaleu(0,-2) = -( stot + s2 - s1 ) / (2*ehat)
    pkaleu(3,-2) = -pkaleu(3,-1)
    pkaleu(2,-2) = -pkaleu(2,-1)
    pkaleu(1,-2) = -pkaleu(1,-1)
!  
    call rambo( discard ,obj%n ,ehat ,obj%m(1:nmax) ,pp ,obj%w ,obj%iw )
    if (discard) return
    obj%w = wx1x2*obj%w
!  
    pkaleu(  0,1:obj%n) = pp(  4,1:obj%n)
    pkaleu(1:3,1:obj%n) = pp(1:3,1:obj%n)
  endif
  discard = .false.
  end subroutine  


  function rambo_wght( obj ) result(weight)
  implicit none
  type(rambo_type) ,intent(in) :: obj
  !(realknd2!) :: weight
  weight = obj%w
  end function


      SUBROUTINE RAMBO(discard,N,ET,XM,P,WT,IWARN)
!------------------------------------------------------
!
!                       RAMBO
!
!    RA(NDOM)  M(OMENTA)  B(EAUTIFULLY)  O(RGANIZED)
!
!    A DEMOCRATIC MULTI-PARTICLE PHASE SPACE GENERATOR
!    AUTHORS:  S.D. ELLIS,  R. KLEISS,  W.J. STIRLING
!    THIS IS VERSION 1.0 -  WRITTEN BY R. KLEISS
!
!    N  = NUMBER OF PARTICLES (>1, IN THIS VERSION <101)
!    ET = TOTAL CENTRE-OF-MASS ENERGY
!    XM = PARTICLE MASSES ( DIM=nmax )
!    P  = PARTICLE MOMENTA ( DIM=(4,nmax) )
!    WT = WEIGHT OF THE EVENT
!
!------------------------------------------------------
      IMPLICIT !(realknd2!) (A-H,O-Z)
      DIMENSION XM(nmax),P(4,nmax),Q(4,nmax),Z(nmax),R(4) &
        ,B(3),P2(nmax),XM2(nmax),E(nmax),V(nmax),IWARN(5) &
        ,RN(4)
      SAVE ACC,ITMAX,IBEGIN,Z,TWOPI,PI2LOG
      DATA ACC/1.D-14/,ITMAX/6/,IBEGIN/0/!,IWARN/5*0/
      logical discard
!------------------------------------------------------
!       VERSION DEFINITION
!------------------------------------------------------
!      DATA IVERSION/0/
!      IF(IVERSION.EQ.0)PRINT*,'VERSION OF 13/12/94'
!      IVERSION=1
!------------------------------------------------------
!         IBEGIN=0
! INITIALIZATION STEP: FACTORIALS FOR THE PHASE SPACE WEIGHT
      discard = .false.
      IF(IBEGIN.NE.0) GOTO 103
      IBEGIN=1
      TWOPI=8.D0*ATAN(1.D0)
      PI2LOG=LOG(TWOPI/4.D0)
      Z(2)=PI2LOG
      DO 101 K=3,nmax!100
  101 Z(K)=Z(K-1)+PI2LOG-2.D0*LOG(DBLE(K-2))
      DO 102 K=3,nmax!100
  102 Z(K)=(Z(K)-LOG(DBLE(K-1)))
!
! CHECK ON THE NUMBER OF PARTICLES
  103 IF(N.GT.1.AND.N.LT.101) GOTO 104
      PRINT 1001,N
      STOP
!
! CHECK WHETHER TOTAL ENERGY IS SUFFICIENT; COUNT NONZERO MASSES
  104 XMT=0.D0
      NM=0
      DO 105 I=1,N
       IF((XM(I).GT.0.D0).OR.(XM(I).LT.0.D0)) NM=NM+1
  105 XMT=XMT+ABS(XM(I))
      IF(XMT.LE.ET) GOTO 201
!      PRINT 1002,XMT,ET
!      STOP
      discard = .true.
      return
!
! THE PARAMETER VALUES ARE NOW ACCEPTED
!
! GENERATE N MASSLESS MOMENTA IN INFINITE PHASE SPACE
 201  CONTINUE
      DO 202 I=1,N
        call rangen(RN,4)
      C=2.D0*RN(1)-1.D0
      S=SQRT(1.D0-C*C)
      F=TWOPI*RN(2)
      Q(4,I)=-LOG(RN(3)*RN(4))
      Q(3,I)=Q(4,I)*C
      Q(2,I)=Q(4,I)*S*SIN(F)
  202 Q(1,I)=Q(4,I)*S*COS(F)
!
! CALCULATE THE PARAMETERS OF THE CONFORMAL TRANSFORMATION
      DO 203 I=1,4
  203 R(I)=0.D0
      DO 204 I=1,N
      DO 204 K=1,4
  204 R(K)=R(K)+Q(K,I)
      RMAS=SQRT(R(4)**2-R(3)**2-R(2)**2-R(1)**2)
      DO 205 K=1,3
  205 B(K)=-R(K)/RMAS
      G=R(4)/RMAS
      A=1.D0/(1.D0+G)
      X=ET/RMAS
!
! TRANSFORM THE Q'S CONFORMALLY INTO THE P'S
      DO 207 I=1,N
      BQ=B(1)*Q(1,I)+B(2)*Q(2,I)+B(3)*Q(3,I)
      DO 206 K=1,3
  206 P(K,I)=X*(Q(K,I)+B(K)*(Q(4,I)+A*BQ))
  207 P(4,I)=X*(G*Q(4,I)+BQ)
!
! CALCULATE WEIGHT AND POSSIBLE WARNINGS
      WT=PI2LOG
      IF(N.NE.2) WT=(2.D0*N-4.D0)*LOG(ET)+Z(N)
      IF(WT.GE.-180.D0) GOTO 208
      IF(IWARN(1).LE.5) PRINT 1004,WT
      IWARN(1)=IWARN(1)+1
  208 IF(WT.LE. 174.D0) GOTO 209
      IF(IWARN(2).LE.5) PRINT 1005,WT
      IWARN(2)=IWARN(2)+1
!
! RETURN FOR WEIGHTED MASSLESS MOMENTA
  209 IF(NM.NE.0) GOTO 210
      WT=EXP(WT)
      RETURN
!
! MASSIVE PARTICLES: RESCALE THE MOMENTA BY A FACTOR X
  210 XMAX=SQRT(1.D0-(XMT/ET)**2)
      DO 301 I=1,N
      XM2(I)=XM(I)**2
  301 P2(I)=P(4,I)**2
      ITER=0
      X=XMAX
      ACCU=ET*ACC
  302 F0=-ET
      G0=0.D0
      X2=X*X
      DO 303 I=1,N
      E(I)=SQRT(XM2(I)+X2*P2(I))
      F0=F0+E(I)
  303 G0=G0+P2(I)/E(I)
      IF(ABS(F0).LE.ACCU) GOTO 305
      ITER=ITER+1
      IF(ITER.LE.ITMAX) GOTO 304
      PRINT 1006,ITMAX
      GOTO 305
  304 X=X-F0/(X*G0)
      GOTO 302
  305 DO 307 I=1,N
      V(I)=X*P(4,I)
      DO 306 K=1,3
  306 P(K,I)=X*P(K,I)
  307 P(4,I)=E(I)
!
! CALCULATE THE MASS-EFFECT WEIGHT FACTOR
      WT2=1.D0
      WT3=0.D0
      DO 308 I=1,N
      WT2=WT2*V(I)/E(I)
  308 WT3=WT3+V(I)**2/E(I)
      WTM=(2.D0*N-3.D0)*LOG(X)+LOG(WT2/WT3*ET)
!
! RETURN FOR  WEIGHTED MASSIVE MOMENTA
      WT=WT+WTM
      IF(WT.GE.-180.D0) GOTO 309
      IF(IWARN(3).LE.5) PRINT 1004,WT
      IWARN(3)=IWARN(3)+1
  309 IF(WT.LE. 174.D0) GOTO 310
      IF(IWARN(4).LE.5) PRINT 1005,WT
      IWARN(4)=IWARN(4)+1
  310 WT=EXP(WT)
      RETURN
!
 1001 FORMAT(' RAMBO FAILS: # OF PARTICLES =',I5,' IS NOT ALLOWED')
 1002 FORMAT(' RAMBO FAILS: TOTAL MASS =',D15.6,' IS NOT', &
       ' SMALLER THAN TOTAL ENERGY =',D15.6)
 1004 FORMAT(' RAMBO WARNS: WEIGHT = EXP(',F20.9,') MAY UNDERFLOW')
 1005 FORMAT(' RAMBO WARNS: WEIGHT = EXP(',F20.9,') MAY  OVERFLOW')
 1006 FORMAT(' RAMBO WARNS:',I3,' ITERATIONS DID NOT GIVE THE', &
       ' DESIRED ACCURACY =',D15.6)
      END subroutine
!
end module
