module avh_iotools
  use avh_iounits
  implicit none
  private
  public :: charLine_type,commandArgs_type
  public :: put_charLine,read_charLines,read_lines
  public :: command_arguments,date_time
  public :: maxLineLen

  integer,parameter :: maxLineLen=!(maxLineLen!)

  integer,parameter :: carLen=64

  integer :: frst(maxLineLen),last(maxLineLen)


  type :: charTrain_type
    character(carLen),allocatable :: car(:)
    integer :: Ncars,Nletters,Nlast
  contains
    procedure :: cht_chr
    procedure :: cht_chr_ii
    generic   :: chr=>cht_chr,cht_chr_ii
    procedure :: put=>cht_put
  end type


  type :: charLine_type
    private
    type (charTrain_type) :: t
!    character(maxLineLen) :: Chr
    integer :: Nword,trimLen,stripLen
    integer,allocatable :: Frst(:),Last(:)
  contains
    procedure :: identify_words
    procedure :: chl_word
    procedure :: chl_word_1
    procedure :: chl_word_2
    generic :: word=>chl_word,chl_word_1,chl_word_2
    procedure :: Nwords=>chl_Nwords
    procedure :: prnt=>chl_prnt
    procedure :: chl_read_i2
    procedure :: chl_read_r2
    generic :: read=>chl_read_i2,chl_read_r2
  end type

  type :: commandArgs_type
    private
    character(maxLineLen),allocatable :: Alist(:)
    integer,allocatable :: leng(:)
    integer :: Narg
  contains
    procedure :: grab=>cda_grab
    procedure :: erase=>cda_erase
    procedure :: Nargs=>cda_Nargs
    procedure :: nr=>cda_nr
    procedure :: find_char=>cda_find_char
    procedure :: find_int=>cda_find_int
    procedure :: find_real=>cda_find_real
  end type

  interface read_charLines
    module procedure read_charLines,read_charLines_ii
  end interface

contains


  function cht_chr( t ) result(rslt)
!***********************************************************************
!***********************************************************************
  class(charTrain_type),intent(in) :: t
  character(t%Nletters) :: rslt
  integer :: ii
  rslt = ''
  do ii=1,t%Ncars
    rslt = trim(rslt)//t%car(ii)
  enddo
  end function

  function cht_chr_ii( t ,ii,jj ) result(rslt)
  class(charTrain_type),intent(in) :: t
  integer              ,intent(in) :: ii,jj
  character(jj-ii+1   ) :: rslt
  character(t%Nletters) :: tmp
  tmp = t%chr()
  rslt = tmp(ii:jj)
  end function


  subroutine cht_put( t ,line )
!***********************************************************************
!***********************************************************************
  class(charTrain_type) :: t
  character(*),intent(in) :: line
  integer :: nn,ii,oo
  nn = len(line)/carLen
  oo = len(line)-nn*carLen
  if (allocated(t%car)) deallocate(t%car)
  allocate( t%car(1:nn+1) )
  do ii=1,nn
    t%car(ii)(1:carLen) = line((ii-1)*carLen+1:ii*carLen)
  enddo
  t%car(nn+1) = ''
  t%car(nn+1)(1:oo) = line(nn*carLen+1:nn*carLen+oo)
  t%Ncars    = nn+1
  t%Nletters = len(line)
  t%Nlast    = oo
  end subroutine


  subroutine copy_charTrain( aa ,bb )
!***********************************************************************
!***********************************************************************
  type(charTrain_type),intent(out) :: aa
  type(charTrain_type),intent(in ) :: bb
  if (allocated(aa%car)) deallocate(aa%car)
  allocate( aa%car(lbound(bb%car,1):ubound(bb%car,1)) )
  aa%car = bb%car
  aa%Ncars    = bb%Ncars
  aa%Nletters = bb%Nletters
  aa%Nlast    = bb%Nlast
  end subroutine
  
   

  

  function chl_word( l ,ii ) result(rslt)
!***********************************************************************
! returns word nr. ii, the whole word
!***********************************************************************
  class(charLine_type),intent(in) :: l
  integer,intent(in) :: ii
  character(l%Last(ii)-l%Frst(ii)+1) :: rslt
  if (ii.lt.1.or.l%Nword.lt.ii) then
    rslt = ''
  else
    rslt = l%t%Chr(l%Frst(ii),l%Last(ii))
  endif
  end function

  function chl_word_1( l ,ii ,j0,j1 ) result(rslt)
!***********************************************************************
! returns character j0 to j1 of word nr. ii
!***********************************************************************
  class(charLine_type),intent(in) :: l
  integer,intent(in) :: ii,j0,j1
  character(j1-j0+1) :: rslt
  if (ii.lt.1.or.l%Nword.lt.ii) then
    rslt = ''
  else
    rslt = l%t%Chr(l%Frst(ii)+j0-1,l%Frst(ii)+j1-1)
  endif
  end function

  function chl_word_2( l ,ii,jj ) result(rslt)
!***********************************************************************
! returns words nr. ii to jj separated by a single space
!***********************************************************************
  class(charLine_type),intent(in) :: l
  integer,intent(in) :: ii,jj
  integer :: kk
  character(sum(l%Last(ii:jj))-sum(l%Frst(ii:jj))+2*jj-2*ii+1) :: rslt
  if (ii.lt.1.or.l%Nword.lt.ii.or.jj.lt.ii) then
    rslt = ''
  else
    rslt = l%t%Chr(l%Frst(ii),l%Last(ii))
    do kk=ii+1,jj
      rslt = trim(rslt)//' '//l%t%Chr(l%Frst(kk),l%Last(kk))
    enddo
  endif
  end function


  function chl_Nwords( l ) result(rslt)
  class(charLine_type),intent(in):: l
  integer :: rslt
  rslt = l%Nword
  end function


  function chl_prnt( l ) result(rslt)
  class(charLine_type),intent(in) :: l
  character(l%trimLen) :: rslt
  rslt = trim(l%t%Chr())
  end function


  subroutine chl_read_i2( l ,ii ,xx )
  class(charLine_type),intent(in ) :: l
  integer             ,intent(in ) :: ii
  integer             ,intent(out) :: xx
  character(maxLineLen) :: aa
  aa = l%word(ii)
  read(aa,*) xx
  end subroutine
  subroutine chl_read_r2( l ,ii ,xx )
  class(charLine_type),intent(in ) :: l
  integer             ,intent(in ) :: ii
  !(realknd2!)        ,intent(out) :: xx
  character(maxLineLen) :: aa
  aa = l%word(ii)
  read(aa,*) xx
  end subroutine
 

  function put_charLine( line ) result(rslt)
!***********************************************************************
!***********************************************************************
  character(*),intent(in) :: line
  type(charLine_type) :: rslt
  call rslt%t%put( line )
  call rslt%identify_words
  end function


  subroutine read_charLines( rUnit ,lines )
!***********************************************************************
!***********************************************************************
  integer,intent(in) :: rUnit
  type( charLine_type),allocatable,intent(out) :: lines(:)
  type(charTrain_type),allocatable :: tmp(:)
  character(maxLineLen) :: tmpLine
  integer :: ii,Nline,jj
  ii = 16
  allocate(tmp(1:ii))
  Nline = 0
  do ;Nline=Nline+1
    if (Nline.gt.ii) then
      allocate(lines(1:ii))
      do jj=1,ii
        call copy_charTrain( lines(jj)%t ,tmp(jj) )
      enddo
      deallocate(tmp)
      allocate(tmp(1:2*ii))
      do jj=1,ii
        call copy_charTrain( tmp(jj) ,lines(jj)%t )
      enddo
      ii = 2*ii
      deallocate(lines)
    endif
    read(rUnit,'(A)',end=111) tmpLine
    call tmp(Nline)%put(tmpLine)
  enddo
  111 continue
  Nline = Nline-1
  allocate(lines(1:Nline))
  do jj=1,Nline
    call copy_charTrain( lines(jj)%t ,tmp(jj) )
    call lines(jj)%identify_words
  enddo
  deallocate(tmp)
  end subroutine
 

  subroutine read_charLines_ii( rUnit ,lines ,frstLine,lastLine )
!***********************************************************************
!***********************************************************************
  integer,intent(in) :: rUnit,frstLine,lastLine
  type( charLine_type),allocatable,intent(out) :: lines(:)
  character(maxLineLen) :: tmpLine
  integer :: Nline,jj
  allocate(lines(1:lastLine-frstLine+1))
  Nline = 0
  do ;Nline=Nline+1
    if (Nline.gt.lastLine) exit
    read(rUnit,'(A)',end=111) tmpLine
    if (Nline.lt.frstLine) cycle
    call lines(Nline-frstLine+1)%t%put(tmpLine)
  enddo
  111 continue
  do jj=1,lastLine-frstLine+1
    call lines(jj)%identify_words
  enddo
  end subroutine
 

  subroutine identify_words( l )
  class(charLine_type) :: l
  character(l%t%Nletters) :: l_Chr
  integer :: ii
  l_Chr = l%t%chr()
  l%Nword = 0
  if (l_Chr.ne.'') then
    l%trimLen = len_trim(l_Chr)
    l%stripLen = len_trim(adjustl(l_Chr))
    ii = 0
    do ;if (ii.gt.l%trimLen) exit
      do ;ii=ii+1 ;if (ii.gt.l%trimLen) exit
        if (l_Chr(ii:ii).ne.' ') then
          l%Nword = l%Nword+1
          frst(l%Nword) = ii
          exit
        endif
      enddo
      last(l%Nword) = ii
      do ;ii=ii+1 ;if (ii.gt.l%trimLen) exit
        if (l_Chr(ii:ii).eq.' ') exit
        last(l%Nword) = ii
      enddo
    enddo
  endif
  if (allocated(l%Frst)) deallocate(l%Frst,l%Last)
  if (l%Nword.le.0) then
    allocate(l%Frst(1:1),l%Last(1:1))
    l%Frst(1) = 0
    l%Last(1) = 0
  else
    allocate(l%Frst(1:l%Nword),l%Last(1:l%Nword))
    l%Frst(1:l%Nword) = frst(1:l%Nword)
    l%Last(1:l%Nword) = last(1:l%Nword)
  endif
  end subroutine


  subroutine cda_grab( obj )  
  class(commandArgs_type),intent(out) :: obj
  integer :: ii
  obj%Narg = command_argument_count()
  allocate(obj%Alist(1:obj%Narg))
  allocate(obj%leng(1:obj%Narg))
  do ii=1,obj%Narg
    call get_command_argument( ii ,obj%Alist(ii) )
    obj%leng(ii) = len_trim(obj%Alist(ii))
  enddo
  end subroutine

  subroutine cda_erase( obj )  
  class(commandArgs_type),intent(out) :: obj
  integer :: ii
  obj%Narg = 0
  if (allocated(obj%Alist)) deallocate(obj%Alist)
  end subroutine

  function cda_Nargs( obj ) result(rslt)
  class(commandArgs_type),intent(in):: obj
  integer :: rslt
  rslt = obj%Narg
  end function

  function cda_nr( obj ,ii ) result(rslt)
  class(commandArgs_type),intent(in):: obj
  integer,intent(in) :: ii
  character(len_trim(adjustl(obj%Alist(ii)))) :: rslt
  rslt = trim(adjustl(obj%Alist(ii)))
  end function

  function cda_find_char( obj ,pattern ) result(rslt)
  class(commandArgs_type),intent(in):: obj
  character(*),intent(in) :: pattern
  character(maxLineLen) :: rslt,patt
  integer :: ll,ii
  rslt = '0'
  patt = adjustl(pattern)
  ll = len_trim(patt)
  do ii=1,obj%Narg
    if (obj%Alist(ii)(1:ll).eq.trim(patt)) then
      rslt = obj%Alist(ii)(ll+1:maxLineLen)
      exit
    endif
  enddo
  end function
  
  function cda_find_int( obj ,pattern ) result(rslt)
! find command line term with the shape  patternINT, where pattern
! is a string, like eg. "value=", and INT is an integer, so terms like
!    value=1234
! INT may contain commas, they are ignored.
  class(commandArgs_type),intent(in):: obj
  character(*),intent(in) :: pattern
  integer :: rslt ,ll,ii,jj,nn
  character(maxLineLen) :: patt,string
  rslt = 0
  patt = adjustl(pattern)
  ll = len_trim(patt)
  do ii=1,obj%Narg
    if (obj%Alist(ii)(1:ll).eq.trim(patt)) then
      string = obj%Alist(ii)(ll+1:obj%leng(ii))
      nn = obj%leng(ii)-ll
      jj = 0
      do while (jj.le.nn)
        jj = jj+1
        if (string(jj:jj).eq.',') then 
          string(jj:nn-1) = string(jj+1:nn)
          nn = nn-1
        endif
      enddo
      read( string(1:nn) ,* ) rslt
      exit
    endif
  enddo
  end function
  
  function cda_find_real( obj ,pattern ) result(rslt)
! find command line term with the shape  patternREAL, where pattern
! is a string, like eg. "value=", and REAL is a real, so terms like
!    keyword=1.345
  class(commandArgs_type),intent(in):: obj
  character(*),intent(in) :: pattern
  !(realknd2!) :: rslt
  integer :: ll,ii,jj,nn
  character(maxLineLen) :: patt,string
  rslt = 0
  patt = adjustl(pattern)
  ll = len_trim(patt)
  do ii=1,obj%Narg
    if (obj%Alist(ii)(1:ll).eq.trim(patt)) then
      string = obj%Alist(ii)(ll+1:obj%leng(ii))
      nn = obj%leng(ii)-ll
      read( string(1:nn) ,* ) rslt
      exit
    endif
  enddo
  end function
  
  
  subroutine command_arguments(args)
  character(*),allocatable,intent(out) :: args(:)
  character(maxLineLen) :: arg
  integer :: ii,Narg
  Narg = command_argument_count()
  allocate(args(1:Narg))
  do ii=1,Narg
    call get_command_argument(ii, arg)
    args(ii) = adjustl(arg)
  enddo
  end subroutine


  subroutine read_lines( rUnit ,lines ,lineLen )
  integer,intent(in) :: lineLen,rUnit
  character(lineLen),allocatable,intent(out) :: lines(:)
  character(lineLen),allocatable :: tmp(:)
  integer :: ii,Nline
  ii = 16
  allocate(tmp(1:ii))
  Nline = 0
  do ;Nline=Nline+1
    if (Nline.gt.ii) then
      allocate(lines(1:ii))
      lines(1:ii) = tmp(1:ii)
      deallocate(tmp)
      allocate(tmp(1:2*ii))
      tmp(1:ii) = lines(1:ii)
      ii = 2*ii
      deallocate(lines)
    endif
    read(rUnit,'(A)',end=111) tmp(Nline)
  enddo
  111 continue
  Nline = Nline-1
  allocate(lines(1:Nline))
  lines(1:Nline) = tmp(1:Nline)
  deallocate(tmp)
  end subroutine
 

  function date_time() result(rslt)
  character(12) :: rslt,date,time
  call date_and_time(date=date,time=time)
  rslt = date(3:8)//time(1:6)
  end function


end module


