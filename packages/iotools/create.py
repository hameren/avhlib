#!/usr/bin/env python
#begin header
import os,sys
pythonDir = ''
sys.path.append(pythonDir)
from avh import *
thisDir,packDir,avhDir = cr.dirsdown(2)
packageName = os.path.basename(thisDir)
buildDir = os.path.join(avhDir,'build')
#end header

date = '04-12-2013'

lines = ['! From here the package "'+packageName+'", last edit: '+date+'\n']
cr.addfile(os.path.join(thisDir,'avh_iotools.f90'),lines,delPattern="^.*")

fpp.xpnd('maxLineLen','288',lines)

cr.wfile(os.path.join(buildDir,packageName+'.f90'),lines)
