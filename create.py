#!/usr/bin/env python
import os,sys,re
pythonDir = ''
sys.path.append(pythonDir)
from avh import *
thisDir = cr.dirsdown(0)[0]
buildDir = os.path.join(thisDir,'build')
srcDir = os.path.join(thisDir,'packages')

if len(sys.argv)==1:
   cr.prnt('')
   cr.prnt('Example of use just to create  build/avhlib.f90  containing')
   cr.prnt('the packages  kaleu  and  skeletools  plus depencencies:')
   cr.prnt('python create.py kaleu skeletools')
   cr.prnt('')
   cr.prnt('Example of use to create  build/libavhlib.a  containing')
   cr.prnt('the packages  kaleu  and  skeletools  plus depencencies:')
   cr.prnt('python create.py kaleu skeletools compiler=gfortran,-std=f2003')
   cr.prnt('')
   cr.prnt('Example of use to create  libavhlib.a  containing')
   cr.prnt('the packages  kaleu  and  skeletools  plus depencencies')
   cr.prnt('in a specified directory:')
   cr.prnt('python create.py kaleu skeletools \\')
   cr.prnt('                 compiler=gfortran build=/home/user/project/')
   cr.prnt('This is independent from where you execute create.py.')
   cr.prnt('')
   sys.exit()

sysargv = sys.argv
FCF = []
options = []

ii = 0
nn = len(sysargv)-1
while ii<=nn:
    rhs = re.sub('^.*?=','',sysargv[ii])
    lhs = re.sub('='+rhs+'$','',sysargv[ii])
    if lhs!=rhs:
        if lhs=='compiler':
            FCF = rhs.split(',')
        elif lhs=='build':
            buildDir = rhs
        elif lhs=='QP':
            if rhs=='yes': options.append('-QP')
        else:
            options.append(sysargv[ii])
        del sysargv[ii:ii+1]
        nn = nn-1
    elif sysargv[ii]=='-FC' or sysargv[ii]=='-compiler':
        FCF = sysargv[ii+1].split()
        del sysargv[ii:ii+2]
        nn = nn-2
    elif sysargv[ii]=='-QP':
        options.append('-QP')
        del sysargv[ii:ii+1]
        nn = nn-1
    elif sysargv[ii]=='-build':
        buildDir = sysargv[ii+1]
        del sysargv[ii:ii+2]
        nn = nn-2
    elif sysargv[ii]=='iounits':
        cr.prnt('ERROR: iounits is included automatically and must not be an argument')
        sys.exit()
    else:
        ii = ii+1

print(buildDir)

avhlib = os.path.join(buildDir,'avhlib.f90')
avhtbl = os.path.join(buildDir,'avhlib.tbl')

def isthere(term,words):
    for word in words:
        if term==word: return True
    return False

def addsource(package,done,lines):
    deps = []
    cr.addlines(os.path.join(srcDir,package,'dependencies'),deps)
    for line in deps:
        for word in line.split():
            if isthere(word,done): pass
            elif word=='usekindmod': pass
            else: addsource(word,done,lines)
    cr.addavh(thisDir,package,options,lines)
    done.append(package)

def addtables(package,done,lines):
    deps = []
    cr.addlines(os.path.join(srcDir,package,'dependencies'),deps)
    for line in deps:
        for word in line.split():
            if isthere(word,done): pass
            elif word=='usekindmod': pass
            else: addtables(word,done,lines)
    tables = os.path.join(thisDir,'packages',package,'tables.tbl')
    if os.path.exists(tables): cr.addfile(tables,lines)
    done.append(package)

lines = []
done = []
for ii in range(1,len(sys.argv)):
  addtables(sys.argv[ii],done,lines)
if len(lines)>0: cr.wfile(avhtbl,lines)

lines = []
done = []
addsource('iounits',done,lines)
for ii in range(1,len(sys.argv)):
  addsource(sys.argv[ii],done,lines)

fpp.xpnd('path_tbldir',buildDir+os.sep,lines)
fpp.xpnd('rngenerator','call rangen(#1)',lines)
fpp.xpnd('rngmodule','avh_random',lines)
fpp.xpnd('realknd2','real(kind(1d0))',lines)
fpp.xpnd('realknd1','real(kind(1e0))',lines)
fpp.xpnd('complex2','complex(kind(1d0))',lines)
fpp.xpnd('integer2','integer',lines)
fpp.xpnd('integer1','integer(selected_int_kind(4))',lines)
fpp.xpnd('integer0','integer(selected_int_kind(2))',lines)
cr.putdate(lines)
cr.wfile(avhlib,lines)
if len(FCF)>0:
    cr.lib('avhlib',FCF,[avhlib],buildDir)
    lines = []
    lines.append('#\n')
    lines.append('# usage: python compile.py file1.f90 file2.f90\n')
    lines.append('#\n')
    lines.append('import sys,subprocess\n')
    lines.append('sys.path.append(\''+pythonDir+'\')\n')
    lines.append('import avh\n')
    lines.append('files = sys.argv\n')
    lines.append('del files[0]\n')
    lines.append('FCF = '+str(FCF)+'\n')
    lines.append('avhDir = \''+thisDir+'\'\n')
    lines.append('includePaths = [\'-I'+buildDir+'\']\n')
    lines.append('libraryPaths = [\'-L'+buildDir+'\', \'-lavhlib\']\n')
    lines.append('command = FCF + includePaths + files + libraryPaths\n')
    lines.append('print(\'executing: \'+\' \'.join(command))\n')
    lines.append('subprocess.call(command)\n')
    cr.wfile(os.path.join(buildDir,'compile.py'),lines)
