#!/usr/bin/env python
import os,sys
from os.path import join as jpth
thisDir = os.path.dirname(os.path.abspath(sys.argv[0]))
avhDir = thisDir
pythonDir = jpth(avhDir,'python')
sys.path.append(pythonDir)
from avh import *

sysarg1 = ''
if len(sys.argv)>1: sysarg1=sys.argv[1]

packages = jpth(thisDir,'packages')

def setPaths(avhValue,pythonValue):
    edpy.set_value( 'pythonDir' ,pythonValue ,jpth(thisDir,'create.py') )
    
    for name in os.listdir(packages):
        theFile = jpth(packages,name,'create.py')
        if os.path.isfile(theFile):
            edpy.set_value( 'pythonDir' ,pythonValue ,theFile )
    
    edpy.set_value( 'pythonDir' ,pythonValue ,jpth(packages,'putheader.py') )
    edpy.set_value( 'pythonDir' ,pythonValue ,jpth(thisDir,'applications','kaleu4helac','create.py'))


if sysarg1=='reset':

    if os.path.exists(jpth(thisDir,'build')): pfd.rmr(jpth(thisDir,'build'))
    setPaths('','')

else:
    if not os.path.exists(jpth(thisDir,'build')): pfd.mkdirp(jpth(thisDir,'build'))
    setPaths(avhDir,pythonDir)

