      module kaleu4helac_mod
        use avh_kaleu_stats
        use avh_kaleu_base
        use avh_kaleu_ns
        use avh_kaleu_cs
        use avh_kaleu_inst
        use avh_kinematics
        use avh_random, only: rangen_init,ranfun
        type(kaleu_stats_type),save :: stats
        type(kaleu_base_type),save :: kaleu
        type(kaleu_ns_type),save :: kaleu_ns
        type(kaleu_cs_type),save :: kaleu_cs
        type(psPoint_type),save :: psPoint,psPoint0
        type(kaleu_x1x2_type),save :: kaleu_x1x2
        real(kind(1d0)),save :: ecm,sInst(-2:-1),x1x2
        logical,save :: pdfyes=.false.,NSdip=.false.,CSdip=.false.
        integer,allocatable,save :: dip(:,:)

      contains

        subroutine kaleu4helac_getdip( ijk ,ndip ,mxdip ,dipType )
!     ***************************************************************
!     * Turn helac-array "ijk" of dipoles into "dip" which will be  *
!     * input for kaleu_ns or kaleu_cs                              *
!     * This routine is inside the module "kaleu4helac_mod" because *
!     * it needs to set the allocatable array "dip".                *
!     ***************************************************************
        use avh_arrays
        implicit none
        integer,intent(in) :: ndip,mxdip,dipType
        integer,intent(in) :: ijk(mxdip,3)
        integer :: ii,jj
!     The routine "find_or_add" is defined in the module "avh_arrays".
!     It takes a 2-dim allocatable array "dip", and a 1-dim array, say "x",
!     as input, and checks if there is any "j" such that "dip(:,j)=x".
!     If yes, it returns "j", if not, it adds "x" to "dip", and returns
!     the, new, last postion as "j".
        if (dipType.eq.0) then
          CSdip = .true.
          do ii=1,ndip
            call find_or_add( jj ,dip ,translate(ijk(ii,1:3),3) ) 
          enddo
        elseif (dipType.eq.1) then
          NSdip = .true.
          do ii=1,ndip
            call find_or_add( jj ,dip ,translate(ijk(ii,1:2),2) )
          enddo
        endif
        end subroutine

        function translate(aa,nn) result(rslt)
        integer,intent(in) :: nn
        integer,intent(in) :: aa(nn)
        integer :: rslt(nn) ,ii
        integer,parameter :: tt(14)=[-2,-1,1,2,3,4,5,6,7,8,9,10,11,12]
        do ii=1,nn
          rslt(ii) = tt(aa(ii))
        enddo
        end function

      end module


      subroutine kaleu4helac_init( ecut,nbatch,nstep,nbatch_g,nstep_g )
!   ********************************************************************
!   * Input needed by Kaleu only is input of this routine. Other input *
!   * needed both by Kaleu and Helac comes in via common blocks.       *
!   ********************************************************************
      use avh_kaleu_model
      use kaleu4helac_mod
      include 'declare.h'
!       
      include 'common_flags.h'
      include 'common_int.h'
      include 'common_mom.h'
      include 'common_masses.h'
      include 'common_phegas_mom.h'
      include 'common_norm.h'
      include 'common_strf.h'
!      include 'common_cuts.h'
      include 'common_qcdrun.h'
!
      dimension p(2,4)
      type(kaleu_model_type) :: model
      type(process_type) :: process
      integer :: iproc(20)
!
! Where do messages go?
      call kaleu_set_unit( 'model' ,77 )
      call kaleu_set_unit( 'tree' ,77 )
      call kaleu_set_unit( 'input' ,6 )
      call kaleu_set_unit( 'NS' ,6 )
!
! Put the model for Kaleu
! Anti-particles are replaced by their particles in a process
! W-(=particle 34) is replaced by W+(=particle 33) in a process
      call model%addParticle(  1 ,'Ne' ,parmas( 1) ,parwid( 1) )
      call model%addParticle(  2 ,'el' ,parmas( 2) ,parwid( 2) )
      call model%addParticle(  3 ,'_u' ,parmas( 3) ,parwid( 3) )
      call model%addParticle(  4 ,'_d' ,parmas( 4) ,parwid( 4) )
      call model%addParticle(  5 ,'Nm' ,parmas( 5) ,parwid( 5) )
      call model%addParticle(  6 ,'mu' ,parmas( 6) ,parwid( 6) )
      call model%addParticle(  7 ,'_c' ,parmas( 7) ,parwid( 7) )
      call model%addParticle(  8 ,'_s' ,parmas( 8) ,parwid( 8) )
      call model%addParticle(  9 ,'Nt' ,parmas( 9) ,parwid( 9) )
      call model%addParticle( 10 ,'ta' ,parmas(10) ,parwid(10) )
      call model%addParticle( 11 ,'_t' ,parmas(11) ,parwid(11) )
      call model%addParticle( 12 ,'_b' ,parmas(12) ,parwid(12) )
      call model%addParticle( 31 ,'_A' ,parmas(31) ,parwid(31) )
      call model%addParticle( 32 ,'_Z' ,parmas(32) ,parwid(32) )
      call model%addParticle( 33 ,'_W' ,parmas(33) ,parwid(33) )
      call model%addParticle( 35 ,'_g' ,parmas(35) ,parwid(35) )
      call model%addParticle( 41 ,'_H' ,parmas(41) ,parwid(41) )
      if (withqcd.or.onlyqcd) then
        call model%addVertex( 35,35,35 )
        call model%addVertex( 3,3,35 )
        call model%addVertex( 4,4,35 )
        call model%addVertex( 7,7,35 )
        call model%addVertex( 8,8,35 )
        call model%addVertex( 11,11,35 )
        call model%addVertex( 12,12,35 )
      endif
      if (.not.onlyqcd) then
        call model%addVertex( 3,3,31 )
        call model%addVertex( 4,4,31 )
        call model%addVertex( 7,7,31 )
        call model%addVertex( 8,8,31 )
        call model%addVertex( 11,11,31 )
        call model%addVertex( 12,12,31 )
        call model%addVertex( 33,33,31 )
        call model%addVertex( 2,2,31 )
        call model%addVertex( 6,6,31 )
        call model%addVertex( 10,10,31 )
      endif
      if (.not.onlyqcd) then
        call model%addVertex( 3,3,32 )
        call model%addVertex( 4,4,32 )
        call model%addVertex( 7,7,32 )
        call model%addVertex( 8,8,32 )
        call model%addVertex( 11,11,32 )
        call model%addVertex( 12,12,32 )
!      
        call model%addVertex( 3,4,33 )
        call model%addVertex( 7,8,33 )
        call model%addVertex( 11,12,33 )
!      
        call model%addVertex( 33,33,32 )
!      
        call model%addVertex( 2,2,32 )
        call model%addVertex( 1,1,32 )
        call model%addVertex( 2,1,33 )
!      
        call model%addVertex( 6,6,32 )
        call model%addVertex( 5,5,32 )
        call model%addVertex( 6,5,33 )
!      
        call model%addVertex( 10,10,32 )
        call model%addVertex( 9,9,32 )
        call model%addVertex( 10,9,33 )
      endif
      if (.not.onlyqcd.and.ihiggs.ne.0) then
        call model%addVertex( 41,41,41 )
        call model%addVertex( 32,32,41 )
        call model%addVertex( 33,33,41 )
        call model%addVertex( 11,11,41 )
        call model%addVertex( 12,12,41 )
        call model%addVertex( 7,7,41 )
        call model%addVertex( 8,8,41 )
        call model%addVertex( 10,10,41 )
        call model%addVertex(  6, 6,41 )
!        call model%addVertex( 4,4,41 )
!        call model%addVertex( 3,3,41 )
!        call model%addVertex(  2, 2,41 )
      endif
!
! Initializations for Helac
      parmas(0)=0
      parwid(0)=-1
!
      gevnb=dnou(38937966)/100
      print*,'gevnb:',gevnb
      read*,ecm
      print*,'energy:',ecm
      read*,istruc
      print*,'istruc:',istruc
!
      xp1 = 1
      xp2 = 1
!
      call readcuts 
!
! Set the squares of the initial-state momenta. Kaleu asks for them
! seperately. Kaleu_ns and kaleu_cs don't, because they can only be
! used for massless intial states.
      sInst(-2:-1) = [ parmas(ifl(2))**2 ,parmas(ifl(1))**2 ]
!
! Initialize initial-state momenta for Helac
      p(1,4)=( ecm**2 + sInst(-1) - sInst(-2) )/ecm/2
      p(1,3)=-p(1,4)*sqrt( 1 - sInst(-1)/p(1,4)**2 )
      p(1,2)=0
      p(1,1)=0
      p(2,4)=( ecm**2 + sInst(-2) - sInst(-1) )/ecm/2
      p(2,3)=p(2,4)*sqrt( 1 - sInst(-2)/p(2,4)**2 )
      p(2,2)=0
      p(2,1)=0
      do i=1,2
        pt=sqrt(p(i,1)**2+p(i,2)**2)
        pq=sqrt(pt**2+p(i,3)**2)
        zq(i,1)= cmplx(p(i,4)+p(i,3),p(i,3) ,kind(zq))
        zq(i,2)= cmplx(p(i,4)-p(i,3),pt     ,kind(zq))
        zq(i,3)= cmplx(p(i,1), p(i,2)       ,kind(zq))
        zq(i,4)= cmplx(p(i,1),-p(i,2)       ,kind(zq))
        zq(i,5)= cmplx( parmas(ifl(i)) , pq ,kind(zq))
        pmom(i,1:4)=p(i,1:4)
        pmom(i,5)=parmas(ifl(i))
      enddo  
!
! Constant factor in weight       
      call mypi(pifac)
      pifac = (2*pifac)**(4-3*(n-2))
      wrest=4*sqrt( scalar_product(p(1,1:4),p(2,1:4))**2 !&
     &              -sInst(-1)*sInst(-2) )
      wrest=gevnb/wrest * pifac
!
! Set seed for random number generator
      open(77,file='seed.input',status='old')
        read(77,*) iseed
        call rangen_init(iseed)
      close(77)
!
! Initialize psPoint
      call psPoint%alloc( n-2 )
      if (NSdip.or.CSdip) call psPoint0%alloc( n-3 )
!
! Initialize generation of x1 and x2
      pdfyes = (istruc.eq.1)
      if (pdfyes) then
!   xmin is a soft cut-off, corrected by an adaptive grid.
        call kaleu_x1x2%init( ranfun ,xmin=(n-2)*ecut/ecm )
      endif
!
! Put the process to the process_type
      do ii=1,n
        iproc(ii) = abs(ifl(ii))
        if (iproc(ii).eq.34) iproc(ii)=33
      enddo             ! initial states    final states
      call process%put( [iproc(2),iproc(1)] ,iproc(3:n) )
!
! Initialize Kaleu
! Global parameters
      call stats%init( ChThrs=1d-3 ,Nbatch=nbatch ,Nstep=nstep !&
     &                ,NbatchGrid=nbatch_g ,NstepGrid=nstep_g )
!   Kaleu_ns and kaleu_cs need to know the identifier for the gluon,
!   which is 35
      if (NSdip) then
        call kaleu_ns%init( model ,stats ,process ,ecm ,ecut !&
     &                     ,ranfun ,ranfun ,dip ,35 )
        deallocate( dip )
      elseif (CSdip) then
        call kaleu_cs%init( model ,stats ,process ,ecm ,ecut !&
     &                     ,ranfun ,ranfun ,dip ,35 )
        deallocate( dip )
      else
        call kaleu%init( model ,stats ,process ,ecm ,ecut !&
     &                     ,ranfun ,ranfun )
      endif
      end subroutine


      subroutine kaleu4helac_gnrt( discard )
      use kaleu4helac_mod
      include 'declare.h'
      include 'common_int.h'
      include 'common_mom.h'
      include 'common_masses.h'
      include 'common_phegas_mom.h'
      include 'common_strf.h'
      dimension pinst(0:3,-2:-1)
      logical discard
!       
! generate x1 and x2
      if (pdfyes) then
        call kaleu_x1x2%gnrt( xp1 ,xp2 )
        x1x2 = xp1*xp2
        ehat = sqrt(x1x2)*ecm
      else
        x1x2 = 1
        ehat = ecm
      endif
!
! construct initial-state momenta
      pInst(0,-1)=-( ehat**2 + sInst(-1) - sInst(-2) )/ehat/2
      pInst(3,-1)= pInst(0,-1)*sqrt( 1 - sInst(-1)/pInst(0,-1)**2 )
      pInst(2,-1)=0
      pInst(1,-1)=0
      pInst(0,-2)=-( ehat**2 + sInst(-2) - sInst(-1) )/ehat/2
      pInst(3,-2)=-pInst(0,-2)*sqrt( 1 - sInst(-2)/pInst(0,-2)**2 )
      pInst(2,-2)=0
      pInst(1,-2)=0
!
! generate final-state momenta
      if (NSdip) then
        call kaleu_ns%gnrt( discard ,psPoint ,psPoint0 ,pInst )
      elseif (CSdip) then
        call kaleu_cs%gnrt( discard ,psPoint ,psPoint0 ,pInst )
      else
        call kaleu%gnrt( discard ,psPoint ,pInst ,sInst ) 
      endif
      if (discard) return
!
! put initial-state momenta to helac
      if (pdfyes) then
       wjac = 1 ! wjac is already in the weight of x1x2
       do ii=1,2
         jj = -ii
         pt = pInst(1,jj)**2 + pInst(2,jj)**2
         pq = sqrt( pt + pInst(3,jj)**2 )
         pt = sqrt( pt )
         zq(ii,1)=cmplx(-pInst(0,jj) -pInst(3,jj),-pInst(3,jj),kind(zq))
         zq(ii,2)=cmplx(-pInst(0,jj) +pInst(3,jj),pt          ,kind(zq))
         zq(ii,3)=cmplx(-pInst(1,jj),-pInst(2,jj)             ,kind(zq))
         zq(ii,4)=cmplx(-pInst(1,jj),+pInst(2,jj)             ,kind(zq))
         zq(ii,5)=cmplx( parmas(ifl(ii)) , pq                 ,kind(zq))
         pmom(ii,4) =-pInst(0,jj)
         pmom(ii,1) =-pInst(1,jj)
         pmom(ii,2) =-pInst(2,jj)
         pmom(ii,3) =-pInst(3,jj)
         pmom(ii,5) = parmas(ifl(ii))
       enddo
      endif
!
! put final-state momenta to helac. The encoding is:
! (final-state momentum i)=(helac momentum i+2) -> psPoint%b(i)=2^(i-1)
      do ii=3,n
        jj = psPoint%b(ii-2)
        pmom(ii,4)   = psPoint%p(jj)%E
        pmom(ii,1:3) = psPoint%p(jj)%V(1:3)
        pmom(ii,5)   = psPoint%p(jj)%M
        pt = sqrt( psPoint%p(jj)%Tsq )
        pq = psPoint%p(jj)%AV
        zq(ii,1)= cmplx( pmom(ii,4) +pmom(ii,3) ,pmom(ii,3) ,kind(zq) )
        zq(ii,2)= cmplx( pmom(ii,4) -pmom(ii,3) ,pt         ,kind(zq) ) 
        zq(ii,3)= cmplx( pmom(ii,1), pmom(ii,2)             ,kind(zq) )
        zq(ii,4)= cmplx( pmom(ii,1),-pmom(ii,2)             ,kind(zq) )
        zq(ii,5)= cmplx( pmom(ii,5), pq                     ,kind(zq) )
      enddo
      end subroutine


      subroutine kaleu4helac_wght( wght )
      use kaleu4helac_mod
      real(kind(1d0)),intent(out) :: wght
      if (NSdip) then
        wght = kaleu_ns%wght( psPoint,psPoint0 )
      elseif (CSdip) then
        wght = kaleu_cs%wght( psPoint,psPoint0 )
      else
        wght = kaleu%wght( psPoint )
      endif
      if (pdfyes) wght = wght*kaleu_x1x2%wght()/x1x2
      end subroutine


      subroutine kaleu4helac_collect( wght )
      use kaleu4helac_mod
      real(kind(1d0)),intent(in) :: wght
      call stats%collect( wght )
      if (NSdip) then
        call kaleu_ns%adapt( stats )
      elseif (CSdip) then
        call kaleu_cs%adapt( stats )
      else
        call kaleu%adapt( stats )
      endif
      if (pdfyes) call kaleu_x1x2%adapt( stats )
      end subroutine


      subroutine kaleu4helac_plotgrids( iunit )
      use kaleu4helac_mod
      integer,intent(in) :: iunit
      if (NSdip) then
        call kaleu_ns%plotgrids( iunit )
      elseif (CSdip) then
        call kaleu_cs%plotgrids( iunit )
      else
        call kaleu%plotgrids( iunit )
      endif
      if (pdfyes) call kaleu_x1x2%plot( iunit )
      end subroutine


      subroutine printmomenta
      include 'declare.h'
      include 'common_int.h'
      include 'common_phegas_mom.h'
      do ii=1,n
        write(6,'(i2,5d24.16)') !&
     &  ii,pmom(ii,1),pmom(ii,2),pmom(ii,3),pmom(ii,4),pmom(ii,5)
      enddo
      end


