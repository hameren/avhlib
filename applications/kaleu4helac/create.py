#!/usr/bin/env python
#begin header
import os,sys
thisDir = os.path.dirname(os.path.abspath(sys.argv[0]))
pythonDir = ''
sys.path.append(pythonDir)
from avh import *
#end header

from os.path import join as jpth

def createmain(directory,task):
    lines = []
    lines.extend(open('main_kaleu_base.f').readlines())
    fpp.case('diptask',task,lines)
    cr.wfile(jpth(directory,'main_kaleu_'+task+'.f'),lines)

createmain('helac','born')
createmain(jpth('helac','dipoles'),'dip')
createmain(jpth('helac','dipoles'),'I'  )
createmain(jpth('helac','dipoles'),'KP' )
