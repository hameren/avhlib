from __future__ import print_function
import os,sys,re,subprocess,platform

def prnt(message,delPattern=''):
    if not delPattern=='^.*': print(re.sub(delPattern,'',message))

def dirsdown(Ndir):
    dir0 = os.path.abspath(sys.argv[0])
    if os.path.isfile(dir0):
        dirs = [os.path.dirname(dir0)]
    else:
        dirs = [dir0]
    nn = 0
    while nn<Ndir:
        dirs.append( os.path.dirname(dirs[nn]) )
        nn = nn+1
    return dirs

def prnt_list(array,delPattern=''):
    message = ''
    for item in array:
        message = message+' '+item
    prnt(re.sub(r'^ ','',message),delPattern)

def prnt_1(message,old,new):
    prnt(re.sub(old,new,message))

def wfile(filename,lines,delPattern='^.*'):
    prnt('writing file '+filename,delPattern)
    iofile = open(filename,'w')
    iofile.writelines(lines)
    iofile.close()

def execute(command,delPattern='^.*',options=[]):
    prnt_list(['executing:']+command,delPattern)
    if 'onlyPrint' in options:
        pass
    elif 'shell' in options:
        subprocess.call(command,shell=True)
    else:
        subprocess.call(command)

def addlines(filename,lines):
    lines.extend(open(filename).readlines())

def addfile(filename,lines,delPattern=''):
    prnt('adding file '+filename,delPattern)
    lines.extend(open(filename).readlines())

def addavh(avhDir,packName,options,lines,delPattern=''):
    """
    options  is a list of (optional) arguments given to
    the  create.py  of  packName
    """
    packDir = os.path.join(avhDir,'packages',packName)
    buildDir = os.path.join(avhDir,'build')
    prnt('adding avh package '+packDir)
    execute(['python',os.path.join(packDir,'create.py')]+options,delPattern='^.*')
    addfile(os.path.join(buildDir,packName+'.f90'),lines,delPattern='^.*')
    spaces = ' '*(67-len(packName))
    nn = -1
    for line in lines:
        nn = nn+1
        if re.match(r'^!IncludingTheModulesBeforeThisLine',lines[nn]):
            newline = re.sub(r'\s*#\'',' '+packName,lines[nn-1].strip('\n'))
            if packName=='iounits':
                lines.insert(nn,'  write(*,\'(a72)\') \'#   '+packName+spaces+'#\'\n')
            elif (len(newline)>90):
                lines.insert(nn,'  write(*,\'(a72)\') \'#   '+packName+spaces+'#\'\n')
            else:
                spaces = ' '*(91-len(newline))
                lines[nn-1] = newline+spaces+'#\'\n'
            break
    if os.path.exists(os.path.join(packDir,'reference.h')):
        reference = []
        addfile(os.path.join(packDir,'reference.h'),reference,delPattern='^.*')
        nn = -1
        for line in lines:
            nn = nn+1
            if re.match(r'^!TheFirstReferenceIsNotYetHere',lines[nn]):
                lines[nn] = '  write(*,\'(a72)\') \'# Please cite'+' '*58+'#\'\n'
                lines.insert(nn,'  write(*,\'(a72)\') \'#'+' '*70+'#\'\n')
                lines.insert(nn+3,'  write(*,\'(a72)\') \'# in publications with '
                          +'results obtained with the help of this program. #\'\n')
            elif re.match(r'^!IncludingReferencesBeforeThisLine',lines[nn]):
                for ref in reference:
                   lines.insert(nn,ref)
                   nn = nn+1
                break

def moduleDir(fc):
    if fc=='gfortran': return '-J'
    elif fc=='nagfor': return '-mdir'
    else: return '-module'

def lib(name,fc,files,build,delPattern=''):
    """
    fc and files must be lists
    """
    libName = 'lib'+name+'.a'
    libFile = os.path.join(build,libName)
    objects = []
    for item in files:
        dirName,objct = os.path.split(item)
        objct = re.sub('\.f$','.o',objct)
        objct = re.sub('\.f90$','.o',objct)
        objct = os.path.join(build,objct)
        execute(fc+['-I',build,'-c',item,'-o',objct,moduleDir(fc[0]),build],delPattern)
        objects = objects+[objct]
    execute(['ar','cru',libFile]+objects,delPattern)
    execute(['ranlib',libFile],delPattern)

def modlib(name,fc,lines,build,linkmethod='dynamic',linker='ld',delPattern='^.*',options=[]):
    """
    Creates a library form fortran source contained in <lines>.
    The fortran source must consist of modules only, and in the
    right dependency order. All input is of type string, and
    <fc> are and <lines> lists of strings.
     name: library name
       fc: fortran compiler and flags
    build: directory
    lines: lines containing the full source
    """
    objNames = []
    objects = []
    while re.sub(r'^ *','',lines[0])[:7]!='module ': del lines[0] 
    while len(lines)!=0:
        modName = re.sub(r'^ *module ','',lines[0]).rstrip('\n')
        modName = re.sub(r'!.*$','',modName)
        modName = re.sub(r' .*$','',modName)
        srcFile = os.path.join(build,'mod_'+modName+'.f90')
        objFile = os.path.join(build,'mod_'+modName+'.o')
        objNames.append('mod_'+modName+'.o')
        objects.append(objFile)
        nn = 1
        while re.sub(r'^ *','',lines[nn])[:10]!='end module': nn = nn+1
        nn = nn+1
        if (os.path.exists(objFile) and os.path.exists(srcFile)):
            oldLines = []
            addlines(srcFile,oldLines)
            if len(oldLines)==nn:
                ii = 0
                while oldLines[ii]==lines[ii]:
                    ii = ii+1
                    if (ii>nn or ii+1>len(oldLines)): break
                if ii==nn: update = False
                else: update = True
            else: update = True
        else: update = True
        if update:
            wfile(srcFile,lines[0:nn],delPattern)
            if linkmethod=='static':
                execute(fc+['-I',build,'-c',srcFile,'-o',objFile,moduleDir(fc[0]),build],delPattern,options)
            else:
                execute(fc+['-I',build,'-fPIC','-c',srcFile,'-o',objFile,moduleDir(fc[0]),build],delPattern,options)
        del lines[0:nn]
        while re.sub(r'^ *','',lines[0])[:7]!='module ':
            del lines[0]
            if len(lines)==0: break
    if linkmethod=='static':
        libFile = os.path.join(build,'lib'+name+'.a')
        execute(['ar','cru',libFile]+objects,delPattern,options)
        execute(['ranlib',libFile],delPattern,options)
    else:
        if platform.system()=='Darwin':
            libFile = os.path.join(build,'lib'+name+'.dylib')
            execute([linker,'-dynamiclib','-o',libFile]+objects,delPattern,options)
        else:
            libFile = os.path.join(build,'lib'+name+'.so')
            execute([linker,'-shared','-o',libFile]+objects,delPattern,options)

def lhs(lhs,separator,line):
    return re.match(r'^ *'+lhs+' *'+separator,line)

def rhs(separator,line):
    tmpline = re.sub(r'^.*?'+separator,'',line)
    return tmpline.rstrip('\n').strip()

def putdate(lines):
    maxdate = '00-00-0000'
    maxnumdate = 0
    for line in lines:
        if re.match('^!.*last edit:',line):
            date = re.sub(' *\n','',re.sub('^.*last edit: ','',line))
            numdate = int(date[0:2])+100*int(date[3:5])+10000*int(date[6:10])
            if numdate>maxnumdate:
              maxdate = date
              maxnumdate = numdate
    nn = 0
    for line in lines:
        if re.match(r'^ .*#   date:',line):
            lines[nn] = re.sub(r'date: .{10}','date: '+maxdate,line)
            break
        nn = nn+1

def putprogname(progname,lines):
    nbefore = (70-len(progname))/2
    nafter = nbefore
    if nbefore+nafter+len(progname)<70: nafter=nafter+1
    nn = 0
    for line in lines:
        if re.match(r'!IncludeProgramNameBeforeThisLine',line):
            lines.insert(nn,'  write(*,\'(a72)\') \'# composing'+' '*60+'#\'\n')
            lines.insert(nn+1,'  write(*,\'(a72)\') \'#'+' '*nbefore+progname.title()+' '*nafter+'#\'\n')
            del lines[nn+2]
            break
        nn = nn+1


def testcall(command):
    code = 0
    try:
        stdout_string = subprocess.check_output(command,stderr=subprocess.STDOUT)
        #print(stdout_string)
    except subprocess.CalledProcessError as cpe:
        code = 1
        #print(cpe.returncode)
        print(cpe.output)
    except OSError as e:
        if e.errno == os.errno.ENOENT:
            code = 2
            print(e)
        else:
            code = 3
            print(e)
    return code

