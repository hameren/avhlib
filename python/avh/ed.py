import os,sys,re

def lines_from_file(theFile):
    lines = []
    lines.extend(open(theFile).readlines())
    return lines

def file_from_lines(theFile,lines):
    iofile = open(theFile,'w')
    iofile.writelines(lines)
    iofile.close()

def replace_block(pattern,newLines,lines,Nblock=999999):
    switch = False
    ii = 0
    nn = 0
    while nn<len(lines) and ii<Nblock:
        if re.match( pattern[0], lines[nn] ):
            switch = True
        elif re.match( pattern[1] ,lines[nn] ):
            switch = False
            ii = ii+1
            lines[nn:nn] = newLines
            nn = nn + len(newLines)
        elif switch:
            del lines[nn]
            nn = nn-1
        nn = nn+1

def get_block(pattern,lines,Iblock=1):
    block = []
    switch = False
    ii = 0
    nn = 0
    while nn<len(lines):
        if re.match( pattern[0], lines[nn] ):
            switch = True
            ii = ii+1
        elif re.match( pattern[1] ,lines[nn] ):
            switch = False
            if ii==Iblock: return block
        elif (switch and ii==Iblock):
            block.append(lines[nn])
        nn = nn+1

def replace_when_match(pattern,oldString,newString,lines):
    nn = 0
    while nn<len(lines):
        if re.search( pattern, lines[nn] ):
            lines[nn] = re.sub(oldString,newString,lines[nn])
        nn = nn+1

def replace_string(oldString,newString,lines):
    replace_when_match(oldString,oldString,newString,lines)

def case(pattern,variable,value,lines,delete=False):
    """
    Switch on lines between lines starting with
      pattern[0]variable=value  and  pattern[1]variable,
    and switch off lines between lines starting with
      pattern[0]variable=<othervalue>  and  pattern[1]variable.
    Switching on-off happens by un/out-commenting using pattern[2].
    Also switch on/off lines containing  pattern[0]variable=value pattern[1]
    Lines are 'search'ed for  value  so it may be a list or string of values:
      pattern[i]variable=value1,value2 value3value4
    will trigger  case(pattern,variable,valuej,lines)  for every j={1,2,3,4}. 
    The opposite happens with lines containing  variable!=value  etc.
    If  delete=True  then lines that are switched off are deleted and the
    case for this variable cannot be changed anymore.
    Spaces at the beginning and end of variable, value, and all entries of
    pattern are ignored.
    """
    strpVari = ' *'+variable.strip()
    bgnBlock = pattern[0].strip()+strpVari+' *'
    endBlock = ' *'+pattern[1].strip()
    outCmmnt = pattern[2].strip()
    strpValu = value.strip()
    if delete:
        extra='delete'
    else:
        extra=''
    uncom = False
    outcom = False
    nn = 0
    for line in lines:
        if re.match( '^ *'+bgnBlock+'=', line ):
            cutline = re.sub( '^'+bgnBlock+'=', '', line )
            uncom = re.search( strpValu, cutline )
            outcom = not uncom
            if delete: lines[nn] = re.sub( '^' ,outCmmnt+extra ,line )
        elif re.match( '^ *'+bgnBlock+'!=', line ):
            cutline = re.sub( '^'+bgnBlock+'!=', '', line )
            outcom = re.search( strpValu, cutline )
            uncom = not uncom
            if delete: lines[nn] = re.sub( '^' ,outCmmnt+extra ,line )
        elif re.match( '^'+endBlock+strpVari, line ):
            uncom = False
            outcom = False
            if delete: lines[nn] = re.sub( '^' ,outCmmnt+extra ,line )
        elif re.search( bgnBlock+'=', line ):
            searchRslt = re.search( bgnBlock+'= *'+'(.*?)'+endBlock ,line )
            if re.search( strpValu, searchRslt.group(1) ):
                lines[nn] = re.sub('^'+outCmmnt, '', line)
            else:
                if not re.match('^'+outCmmnt,line):
                    lines[nn] = re.sub('^', outCmmnt+extra, line)
                else:
                    lines[nn] = re.sub('^'+outCmmnt, outCmmnt+extra, line)
        elif re.search( bgnBlock+'!=', line ):
            searchRslt = re.search( bgnBlock+'!= *'+'(.*?)'+endBlock ,line )
            if not re.search( strpValu, searchRslt.group(1) ):
                lines[nn] = re.sub('^'+outCmmnt, '', line)
            else:
                if not re.match('^'+outCmmnt,line):
                    lines[nn] = re.sub('^', outCmmnt+extra, line)
                else:
                    lines[nn] = re.sub('^'+outCmmnt, outCmmnt+extra, line)
        else:
            if uncom:
                lines[nn] = re.sub('^'+outCmmnt, '', line)
            elif outcom:
                if not re.match('^'+outCmmnt,line):
                    lines[nn] = re.sub('^', outCmmnt+extra, line)
                else:
                    lines[nn] = re.sub('^'+outCmmnt, outCmmnt+extra, line)
        nn = nn+1
    if delete:
        nn = len(lines)-1
        while (nn >= 0):
            if re.match('^'+outCmmnt+extra,lines[nn]):
                del lines[nn]
            else:
                lines[nn] = re.sub( bgnBlock+'.*?'+endBlock ,'' ,lines[nn] )
            nn = nn-1  

def xpnd(pattern,variable,value,lines):
    """
    Replace occurences of
      pattern[0]variable arg1 arg2 ... pattern[1]
    with value, and then in value replace
      pattern[2]1  with  arg1,  pattern[2]2  with  arg2  etc.
    There must be a space between the last arg and pattern[1].
    For example
      xpnd( ['!(','!)','#'] ,'rangen' ,'call ranlux(#1,#2)' ,lines)
    replaces occurences of
      !(rangen xx 1 !)  with  call ranlux(xx,1)
    Spaces at the beginning and end of variable, value, and all
    entries of pattern are ignored.
    """
    bgnBlock = pattern[0].strip()+' *'+variable.strip()
    endBlock = ' *'+pattern[1].strip()
    argNumbr = pattern[2].strip()
    strpValu = value.strip()
    nn = 0
    for line in lines:
        lines[nn] = re.sub( bgnBlock+endBlock ,strpValu ,line )
        nn = nn+1
    nn = 0
    for line in lines:
        for phrase in re.findall( bgnBlock+' +'+'(.*?)'+endBlock ,line ):
            words = phrase.split()
            newPhrase = strpValu
            for ii in range(0,len(words)):
                newPhrase = re.sub( argNumbr+str(ii+1) ,words[ii] ,newPhrase )
            lines[nn] = re.sub( bgnBlock+' +'+phrase+endBlock ,newPhrase ,line ,1 )
        nn = nn+1
            
def incl(pattern,srcDir,lines,casePattern=['','',''],xpndPattern=['','','']):
    """
    Find lines starting with  pattern[0]FILENAMEpattern[1], then
    on the lines in FILENAME execute the instances of case and xpnd
    indicated by the following lines starting with
      pattern[2]case  or  pattern[2]xpnd
    For example  incl( [ 'include \'' , '\'' , '!execute ' , '|' ] ,'sources' ,lines )
    may act on
      include 'stuff.h90'
      !execute case !{ | !} | !# | pdf | cteq
    where  stuff.h90  is found in  sources
    Spaces at the beginning and end of all entries of pattern are ignored.
    """
    bgnBlock = ' *'+pattern[0].strip()+' *'
    endBlock = ' *'+pattern[1].strip()
    execute  = ' *'+pattern[2].strip()+' *'
    separate = pattern[3].strip()
    noCASEpattern = casePattern[0]+casePattern[1]+casePattern[2]==''
    noXPNDpattern = xpndPattern[0]+xpndPattern[1]+xpndPattern[2]==''
    nn = 0
    for line in lines:
        if re.match( '^'+bgnBlock, line ):
            searchRslt = re.search( '^'+bgnBlock+'(.*?)'+endBlock ,line )
            iofile = open(os.path.join(srcDir,searchRslt.group(1)))
            inclines = iofile.readlines()
            del lines[nn]
            while nn<=len(lines)-1:
                line = lines[nn]
                if re.match('^'+execute+'case:',line):
                    line = re.sub('^'+execute+'case: *','',line)
                    line = re.sub(' *\n','',line)
                    words = line.split(separate)
                    if noCASEpattern:
                        case(words[0:3],words[3],words[4],inclines,delete=True)
                    else:
                        ll = len(words)-1
                        case(casePattern[0:3],words[ll-1],words[ll],inclines,delete=True)
                    del lines[nn]
                elif re.match('^'+execute+'xpnd:',line):
                    line = re.sub('^'+execute+'xpnd: *','',line)
                    line = re.sub(' *\n','',line)
                    words = line.split(separate)
                    if noXPNDpattern:
                        xpnd(words[0:3],words[3],words[4],inclines)
                    else:
                        ll = len(words)-1
                        xpnd(xpndPattern[0:3],words[ll-1],words[ll],inclines)
                    del lines[nn]
                else:
                   break
            jj = 0
            for ljne in inclines:
               lines.insert(nn+jj,ljne)
               jj = jj+1
        nn = nn+1


def set_value(sep,variable,value,theFile):
    """
    set  '.*?'  to  value  in lines containing
      sep[0]+variable+sep[1]+sep[2]+'.*?'+sep[3]
    If only  sep[0]+variable+sep[1]  matches, the line is replaced by
      sep[0]+variable+sep[1]+sep[2]+value+sep[3]+'\n'
    The values of sep[2:3] must be non-empty, eg. for lines of the type
      variable=value
    use sep=['^ *','','=','$']
    """
    lines = lines_from_file(theFile)
    nn = 0
    pattern = sep[0]+variable+sep[1]
    for line in lines:
        searchRslt = re.search( '('+pattern+')' ,line )
        if searchRslt:
            lines[nn] = searchRslt.group(1)+sep[2]+value+sep[3]+'\n'
            tail = re.sub( pattern ,'' ,line ).rstrip('\n')
            if tail=='' or not re.match( sep[2]+'.*?'+sep[3] ,tail ):
                lines[nn] = searchRslt.group(1)+sep[2]+value+sep[3]+'\n'
            elif sep[3]=='$':
                lines[nn] = re.sub( sep[2]+'.*?'+sep[3] ,sep[2]+value ,line )
            else:
                lines[nn] = re.sub( sep[2]+'.*?'+sep[3] ,sep[2]+value+sep[3] ,line )
        nn = nn+1
    file_from_lines(theFile,lines)

def assure_newline(lines):
    outLines = lines
    nn = 0
    for line in outLines:
        if not re.search( '\n$' ,line ):
            outLines[nn] = re.sub( '$' ,'\n' ,line )
        nn = nn+1
    return outLines

def set_block(pattern,newLines,theFile):
    lines = lines_from_file(theFile)
    replace_block(pattern,assure_newline(newLines),lines)
    file_from_lines(theFile,lines)

