from __future__ import absolute_import
import sys
from . import ed

def set_pythonVersion(theFile):
    lines = ed.lines_from_file(theFile)
    ed.case(['#{','#}','#ppp'],'pythonVersion',sys.version[0],lines)
    ed.file_from_lines(theFile,lines)

def set_value(variable,value,theFile):
    ed.set_value( ['^ *',' *= *','\'','\''] ,variable,value,theFile )

