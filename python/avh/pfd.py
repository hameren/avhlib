import subprocess,sys

def rm(arg):
    subprocess.call(['rm',arg])

def rmdirstar(arg):
    subprocess.call('rm '+arg+'/*',shell=True)

def rmr(arg):
    subprocess.call(['rm','-r',arg])

def mkdir(arg):
    #subprocess.call(['mkdir',arg])
    p = subprocess.Popen(['mkdir',arg],stderr=subprocess.PIPE)
    std_err = p.communicate()
    if p.returncode!=0: sys.exit(std_err[1])

def mkdirp(arg):
    subprocess.call(['mkdir','-p',arg])

def cp(arg1,arg2):
    subprocess.call(['cp',arg1,arg2])
    
def cpr(arg1,arg2):
    subprocess.call(['cp','-r',arg1,arg2])
    
def mv(arg1,arg2):
    subprocess.call(['mv',arg1,arg2])

