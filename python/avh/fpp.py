from __future__ import absolute_import
from . import ed

def case(pattern,value,lines):
    ed.case(['!{','!}','!fpp'],pattern,value,lines,delete=True)

def xpnd(pattern,value,lines):
    ed.xpnd(['!\(','!\)','#'],pattern,value,lines) 

def incl(srcdir,lines):
    ed.incl(['include \'','\'','!execute','|'],srcdir,lines
           ,casePattern=['!{','!}','!fpp'],xpndPattern=['!\(','!\)','#'])

def blck(pattern,block,lines):
    ed.replace_block(['!\['+pattern,'!\]'+pattern],block,lines)
